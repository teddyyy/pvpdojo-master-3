/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.menus;

import java.util.Set;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.bukkit.util.GuiUtil;
import com.pvpdojo.hub.market.PrivateTrade;
import com.pvpdojo.hub.market.PrivateTrade.TradePlayer;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.menus.AbilitiesMenu;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class TradeMenu extends SingleInventoryMenu {

    private Player trader;
    private PrivateTrade session;

    public TradeMenu(Player player, Player trader, PrivateTrade session) {
        super(player, CC.AQUA + "Trade Menu", 54);
        setCloseable(false);
        setCancelCloseMessage(null);
        this.trader = trader;
        this.session = session;
    }

    @Override
    public void redraw() {
        super.redraw();

        InventoryPage inv = getCurrentPage();

        User user = User.getUser(getPlayer());
        inv.setItem(0, getTradePlayerIcon(getPlayer(), session), null);
        inv.setItem(8, getTradePlayerIcon(trader, session), null);
        TradePlayer tp = session.getTradePlayer(getPlayer());
        ItemStack status = new ItemBuilder(Material.EMERALD).name(CC.GREEN + "Finish Editing").lore(CC.GRAY + "Click to finish editing your trade").build();

        if (tp.isLocked() && !session.isTradeLocked()) {
            status = new ItemBuilder(Material.REDSTONE).name(CC.GREEN + "Continue Editing").lore(CC.GRAY + "Click to edit the trade again").build();
        }

        if (session.isTradeLocked()) {
            status = new ItemBuilder(Material.DIAMOND).name(CC.DARK_RED + "Finalize Trade").lore(CC.GRAY + "Click to finalize the trade").build();
        }

        if (tp.isFinalized()) {
            status = new ItemBuilder(Material.COMPASS).name(CC.RED + "Waiting on " + trader.getNick()).lore(
                    CC.GRAY + trader.getNick() + " must finalize before", CC.GRAY + "the trade can be completed").build();
        }

        inv.setItem(4, status, clickType -> {
            if (tp.isLocked() && !session.isTradeLocked()) {
                session.lockTrade(getPlayer(), false);
            } else if (session.isTradeLocked()) {
                if (!tp.isFinalized()) {
                    session.finalizeTrade(getPlayer());
                    inv.getMenu().setCloseable(true);
                }
            } else {
                session.lockTrade(getPlayer(), true);
            }
        });
        inv.setItem(2, new ItemBuilder(Material.ENDER_CHEST).name(CC.AQUA + "Add Items To Trade").lore(CC.GRAY + "Click to add items", CC.GRAY + "to the current trade").build(),
                clickType -> {
                    if (user.getPersistentData().getAbilities().isEmpty()) {
                        user.sendOverlay(MessageKeys.NO_ABILITIES);
                        return;
                    }
                    if (session.isTradeLocked() || session.getTradePlayer(getPlayer()).isLocked()) {
                        user.sendOverlay(MessageKeys.TRADE_LOCKED);
                        return;
                    }
                    new AbilitiesMenu(getPlayer(), user.getPersistentData().getAbilities(), (ability, ignore) -> {
                        if (session.getTradePlayer(user.getPlayer()).getItems().size() >= 16) {
                            user.sendOverlay(MessageKeys.TRADE_TOO_MANY_ITEMS);
                            return;
                        }
                        session.getTradePlayer(user.getPlayer()).addAbility(ability.getAbilityUUID());
                        redrawAndOpen();
                    }, ability -> session.getTradePlayer(user.getPlayer()).containsAbility(ability.getAbilityUUID())).open();
                });
        inv.setItem(6, new ItemBuilder(Material.ENDER_CHEST).name(CC.AQUA + "View Trader's Items").lore(
                CC.GRAY + "Click to view " + trader.getNick() + "'s", CC.GRAY + "current items they can trade").build(), clickType -> {
            new AbilitiesMenu(getPlayer(), User.getUser(trader).getPersistentData().getAbilities(),
                    (ability, ignore) -> {}, null).open();
        });
        inv.setItem(13, new ItemBuilder(Material.STAINED_GLASS_PANE).name(CC.RED + "Cancel Trade").lore(CC.GRAY + "Click to cancel the trade").durability((short) 14).build(), clickType -> {
            inv.getMenu().setCloseable(true);
            session.cancelTrade();
        });

        GuiUtil.fillRow(inv, GuiUtil.getBlankItem(), 1, 4, 2);
        GuiUtil.fillRow(inv, GuiUtil.getBlankItem(), 6, 9, 2);
        inv.setItem(22, GuiUtil.getBlankItem(), null);
        inv.setItem(31, GuiUtil.getBlankItem(), null);
        inv.setItem(40, GuiUtil.getBlankItem(), null);
        inv.setItem(49, GuiUtil.getBlankItem(), null);

        setTradeAbilities(tp, session, inv, true);
        setTradeAbilities(session.getTradePlayer(trader), session, inv, false);

    }

    private static void setTradeAbilities(TradePlayer player, PrivateTrade session, InventoryPage inv, boolean slot1) {
        Set<TradePlayer.TradeItem> tradeItems = player.getItems();

        inv.setItem(slot1 ? 11 : 15, player.getMoneyTradeItem().getIcon(), slot1 ? type -> {
            if (session.isTradeLocked() || player.isLocked()) {
                player.getPlayer().sendMessage(CC.RED + "Cannot edit trade while locked in");
                return;
            }

            int change = 0;
            switch (type) {
                case SHIFT_LEFT:
                    change += 90;
                case LEFT:
                    change += 10;
                    break;
                case SHIFT_RIGHT:
                    change -= 90;
                case RIGHT:
                    change -= 10;
                    break;
            }

            if (change != 0 && player.getMoneyTradeItem().get().get() + change >= 0) {
                if (User.getUser(player.getPlayer()).getPersistentData().getMoney() >= player.getMoneyTradeItem().get().get() + change) {
                    player.getMoneyTradeItem().get().addAndGet(change);
                    session.updateTradeMenu();
                } else {
                    player.getPlayer().sendMessage(CC.RED + "Not enough money");
                }
            }

        } : null);


        int i = 23;
        for (TradePlayer.TradeItem tradeItem : tradeItems) {
            inv.setItem(slot1 ? i - 5 : i, tradeItem.getIcon(), slot1 ? clickType -> {
                if (clickType == ClickType.SHIFT_LEFT || clickType == ClickType.SHIFT_RIGHT) {
                    if (session.isTradeLocked() || player.isLocked()) {
                        player.getPlayer().sendMessage(CC.RED + "Cannot edit trade while locked in");
                        return;
                    }
                    session.removeTradeItem(player.getPlayer(), tradeItem);
                }
            } : null);
            i++;
            if (i % 9 == 0) {
                i += 5;
            }
        }
    }

    private static ItemStack getTradePlayerIcon(Player player, PrivateTrade session) {
        TradePlayer trader = session.getTradePlayer(player);
        boolean locked = trader.isLocked();
        boolean finalized = trader.isFinalized();
        short skull_id = 1;
        String status = "still Trading";
        if (locked) {
            skull_id = 0;
            status = "locked In";
        }
        if (finalized) {
            skull_id = 3;
            status = "finalized";
        }
        return new ItemBuilder(Material.SKULL_ITEM).name(CC.GRAY + player.getNick() + " is " + status).durability(skull_id).build();
    }

}
