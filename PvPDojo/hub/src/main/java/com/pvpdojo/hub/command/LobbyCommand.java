/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.command;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.hub.lobby.Lobby;
import com.pvpdojo.hub.lobby.LobbyHotbarGUI;
import com.pvpdojo.hub.lobby.LobbyManager;
import com.pvpdojo.hub.menus.HubHotbarGUI;
import com.pvpdojo.hub.menus.LobbyCreationMenu;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.StringUtils;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Single;
import co.aikar.commands.annotation.Subcommand;

@CommandAlias("lobby|join")
public class LobbyCommand extends DojoCommand {

    public void onLobby(User user, UUID host) {

        if (LobbyManager.inst().getLobbies().stream().anyMatch(lobby -> lobby.getParticipants().contains(user.getUUID()))) {
            user.sendMessage(MessageKeys.WARNING, "{text}", "You are already in another lobby");
            return;
        }

        if (LobbyManager.inst().hasLobby(host)) {
            Lobby lobby = LobbyManager.inst().getLobby(host);

            if (lobby.addParticipant(user.getPlayer())) {
                lobby.broadcast(CC.BLUE + user.getName() + CC.GRAY + " joined the lobby " + CC.GREEN + "[" + lobby.getParticipants().size() + "]");
                user.setHotbarGui(LobbyHotbarGUI.get());
            } else {
                user.sendMessage(MessageKeys.WARNING, "{text}", "This lobby is not available");
            }
        } else {
            user.sendMessage(MessageKeys.WARNING, "{text}", "This lobby is not available");
        }
    }

    @CatchUnknown
    @Default
    public void onLobby(User user, @Single String host) {
        if (StringUtils.UUID.matcher(host).matches()) {
            onLobby(user, UUID.fromString(host));
        } else if (Bukkit.getPlayer(host) != null) {
            onLobby(user, Bukkit.getPlayer(host).getUniqueId());
        }
    }

    @Subcommand("leave")
    public void onLeave(User user) {
        LobbyManager.inst().disconnectPlayer(user.getPlayer());
        user.setHotbarGui(HubHotbarGUI.get(user.getPlayer()));
        user.getPlayer().sendMessage(CC.GREEN + "You have been removed from all lobbies");
    }

    @CommandPermission("staff")
    @Subcommand("unlock")
    public void onKill(User user, UUID host) {
        if (LobbyManager.inst().hasLobby(host)) {
            Lobby lobby = LobbyManager.inst().getLobby(host);
            lobby.stop();
            LobbyManager.inst().removeLobby(lobby);
            user.sendMessage("Lobby removed");
        } else {
            user.sendMessage("Not found");
        }
    }

    @CommandPermission("staff")
    @Subcommand("setcountdown")
    public void onCountdown(User user, Integer countdown) {
        if (LobbyManager.inst().hasLobby(user.getUUID()) && LobbyManager.inst().getLobby(user.getUUID()).getCurrentCountdown() != null) {
            LobbyManager.inst().getLobby(user.getUUID()).getCurrentCountdown().setCurrentCount(countdown).start();
        }
    }

    @Subcommand("create")
    public void onCreate(Player player) {
        if (hasRank(player, Rank.FIRST_DAN)) {
            new LobbyCreationMenu(player).open();
        }
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
