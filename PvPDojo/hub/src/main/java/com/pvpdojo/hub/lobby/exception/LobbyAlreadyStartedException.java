/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.lobby.exception;

public class LobbyAlreadyStartedException extends RuntimeException {}
