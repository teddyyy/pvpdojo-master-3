/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.listeners;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.hub.market.PrivateTrade;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.Request;
import com.pvpdojo.userdata.User;

public class HubPlayerClickEntityListener implements DojoListener {

    @EventHandler
    public void onInteractEntity(PlayerInteractEntityEvent e) {
        Player player = e.getPlayer();
        User user = User.getUser(player);

        if (user.isAdminMode()) {
            return;
        }
        if (!player.getItemInHand().getType().equals(Material.BLAZE_ROD))
            return;
        if (!(e.getRightClicked() instanceof Player))
            return;

        User target = User.getUser((Player) e.getRightClicked());
        if (user.getRequests().containsKey(target.getUUID())) {
            Request request = user.getRequests().remove(target.getUUID());
            if (request instanceof PrivateTrade.TradeRequest) {
                request.accept(user);
                return;
            }
        }
        String fail = target.makeRequest(user.getUUID(), new PrivateTrade.TradeRequest(user));
        if (fail == null) {
            user.sendOverlay(MessageKeys.INVITE_TRADE, "{target}", target.getName());
            target.sendOverlay(MessageKeys.INVITED_TRADE, "{inviter}", user.getName());
        } else {
            user.sendOverlay(MessageKeys.WARNING, "{text}", fail);
        }
    }

}
