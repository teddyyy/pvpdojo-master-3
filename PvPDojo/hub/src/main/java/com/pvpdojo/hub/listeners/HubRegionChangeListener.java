/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.RegionEnterEvent;
import com.pvpdojo.bukkit.events.RegionLeaveEvent;
import com.pvpdojo.hub.Hub;
import com.pvpdojo.hub.userdata.HubUser;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class HubRegionChangeListener implements Listener {

    public static void register() {
        Bukkit.getPluginManager().registerEvents(new HubRegionChangeListener(), PvPDojo.get());
    }

    @EventHandler
    public void onRegionEnter(RegionEnterEvent e) {
        if (User.getUser(e.getPlayer()).isAdminMode()) {
            return;
        }
        Inventory inv = e.getPlayer().getInventory();
        if (e.getRegion().getId().startsWith("market")) {
            User.getUser(e.getPlayer()).sendMessage(MessageKeys.MARKET_WARNING);
            User.getUser(e.getPlayer()).setHotbarGui(null);
            inv.clear();
            inv.setItem(4, new ItemBuilder(Material.BLAZE_ROD).name(CC.DARK_AQUA + "Trade Abilities").build());
        }
    }

    @EventHandler
    public void onRegionLeave(RegionLeaveEvent e) {

        HubUser user = HubUser.getUser(e.getPlayer());

        if (user.isAdminMode()) {
            return;
        }

        if (e.getRegion().getId().startsWith("kitcreation")) {
            if (user.getCurrentlyEditing() != null) {
                user.getPlayer().performCommand("save");
            }
            Hub.applyHubItems(e.getPlayer());
        }

        if (e.getRegion().getId().startsWith("market")) {
            Hub.applyHubItems(e.getPlayer());
        }
    }
}
