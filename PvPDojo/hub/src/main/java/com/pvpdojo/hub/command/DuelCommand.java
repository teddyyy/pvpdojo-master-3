/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.command;

import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.hub.queue.QueueHandler;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.network.SessionRequest;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.Request;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.StringUtils;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Flags;

@CommandAlias("duel")
public class DuelCommand extends DojoCommand {

    @CatchUnknown
    @CommandCompletion("@players")
    public void onDuel(User user, @Flags("other") Player target) {
        User targetUser = User.getUser(target);

        if (user.getParty() != null && !user.getParty().getLeader().equals(user.getUUID())) {
            user.sendMessage(MessageKeys.PARTY_LEADER_REQUIRED);
            return;
        }

        if (targetUser.getParty() != null && !targetUser.getParty().getLeader().equals(targetUser.getUUID())) {
            PvPDojo.schedule(() -> user.sendMessage(PvPDojo.WARNING + "Please duel the party leader: "
                    + CC.BLUE + PvPDojo.getOfflinePlayer(targetUser.getParty().getLeader()).getName())).createAsyncTask();
            return;
        }

        if (user.getRequests().containsKey(targetUser.getUUID())) {
            Request request = user.getRequests().get(targetUser.getUUID());
            if (request instanceof DuelRequest) {
                request.accept(user);
                return;
            }
        }
        String failReason = targetUser.makeRequest(user.getUUID(), new DuelRequest(user));
        if (failReason == null) {
            user.sendOverlay(MessageKeys.INVITE_DUEL, "{target}", targetUser.getName());
            targetUser.sendMessage(StringUtils.createComponent(PvPDojo.LANG.formatMessage(targetUser, MessageKeys.INVITED_DUEL, "{inviter}", user.getName()),
                    CC.GREEN + "accept", "accept " + user.getUUID().toString()));
        } else {
            user.sendOverlay(MessageKeys.WARNING, "{text}", failReason);
        }
    }

    private class DuelRequest extends Request {

        private User requester;

        public DuelRequest(User requester) {
            this.requester = requester;
        }

        @Override
        public void accept(User acceptor) {

            if (requester.isLoggedOut()) {
                acceptor.sendMessage(MessageKeys.COULD_NOT_FIND_PLAYER);
                return;
            }

            if (requester.getParty() == acceptor.getParty()) {
                QueueHandler.inst().submitDeathmatchRequest(new SessionRequest().type(SessionType.DUEL_CASUAL).players(requester.getUUID(), acceptor.getUUID()));
            } else if (requester.getParty() == null) {
                acceptor.sendMessage(PvPDojo.WARNING + "The requesting user is not party while you are");
                return;
            } else if (acceptor.getParty() == null) {
                acceptor.sendMessage(PvPDojo.WARNING + "The requesting user is in a party while you are not. Their party size is " + requester.getParty().size()
                        + ". It is not necessary to have a matching party size");
                return;
            } else if (!acceptor.getParty().getLeader().equals(acceptor.getUUID())) {
                acceptor.sendMessage(PvPDojo.WARNING + "You are not the leader of your party");
                return;
            } else {
                QueueHandler.inst().submitDeathmatchRequest(new SessionRequest().type(SessionType.TEAM_DUEL)
                                                                                .parties(requester.getParty().getPartyId(), acceptor.getParty().getPartyId())
                                                                                .players(requester.getParty().getUsers())
                                                                                .players(acceptor.getParty().getUsers()));
            }

            QueueHandler.inst().setLastDuel(requester.getUniqueId(), acceptor.getUniqueId());
        }

        @Override
        public void deny(User deniedBy) {}

        @Override
        public int getExpirationSeconds() {
            return 60;
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof DuelRequest;
        }

    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
