/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.menus;

import static com.pvpdojo.util.bukkit.ItemUtil.CLICK_ITEM_POOL;
import static com.pvpdojo.util.StreamUtils.negate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.DeveloperSettings;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilityinfo.AbilityType;
import com.pvpdojo.abilityinfo.AbilityUpgrade;
import com.pvpdojo.abilityinfo.KitAttributeArray.KitAttribute;
import com.pvpdojo.achievement.Achievement;
import com.pvpdojo.achievement.AchievementManager;
import com.pvpdojo.bukkit.util.GuiItem;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.gui.InputGUI;
import com.pvpdojo.hub.userdata.HubUser;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.menu.SortType;
import com.pvpdojo.menu.SortedInventoryMenu;
import com.pvpdojo.menu.SortedItem;
import com.pvpdojo.menus.AbilitiesMenu;
import com.pvpdojo.userdata.AbilityData;
import com.pvpdojo.userdata.KitAbilityData;
import com.pvpdojo.userdata.KitData;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.ItemEditor;

public class KitMenu {

    public static class KitCustomizeMenu extends SingleInventoryMenu {

        private HubUser user;
        private KitData kit;

        public KitCustomizeMenu(HubUser user, KitData kit) {
            super(user.getPlayer(), CC.GRAY + "Editor: " + CC.GREEN + kit.getName(), 27);
            this.user = user;
            this.kit = kit;
            page.setHasBar(true);
        }

        @Override
        public void redraw() {
            InventoryPage inv = getCurrentPage();

            inv.setItem(8, new ItemBuilder(Material.STONE_SWORD).name(CC.GREEN + "Edit Inventory").build(), clickType -> {
                closeSafely();
                user.sendMessage(PvPDojo.PREFIX + "Use " + CC.GREEN + "/save or leave the kit creation area " + CC.GRAY + "to save your kit");
                user.setHotbarGui(null);
                AbilityPlayer.get(getPlayer()).selectChampion(kit.getId());
                user.setCurrentlyEditing(kit);
            });

            inv.setItem(11, new ItemBuilder(Material.ENDER_CHEST).name(CC.GREEN + "Add Ability").lore(
                    CC.GRAY + "Click to add an ability to your kit", getPointsDescription(kit.getPoints())).build(), p -> {
                if (user.getPersistentData().countAbilities() == 0) {
                    user.sendOverlay(MessageKeys.NO_ABILITIES);
                    return;
                }

                Collection<AbilityData> available = DeveloperSettings.freeAbilities(user)
                        ? AbilityLoader.getAbilities().stream()
                                       .map(ab -> new AbilityData(null, null, ab.getId()))
                                       .collect(Collectors.toList())
                        : user.getPersistentData().getAbilities().stream()
                              .map(AbilityData::getId)
                              .distinct()
                              .map(i -> new AbilityData(null, null, i))
                              .collect(Collectors.toList());

                new AbilitiesMenu(getPlayer(), available, (data, type) -> {
                    new AbilityCustomizeMenu(user.getPlayer(), kit, data.getLoader(), new KitAbilityData(data.getLoader().getId())).open();
                }, abilityData -> kit.containsAbility(abilityData.getId())).highestGrade().open();

            });

            inv.setItem(15, new ItemBuilder(Material.CHEST).name(CC.GOLD + "Current Abilities").lore(
                    CC.GRAY + "Click to view or edit the current", CC.GRAY + "abilities for this kit", getPointsDescription(kit.getPoints())).build(),
                    p -> new CurrentAbilitiesMenu(user.getPlayer(), kit).open());

            inv.setItem(18, new ItemBuilder(Material.ANVIL).name(CC.GREEN + "Rename Your Kit").build(), clickType -> {
                new InputGUI(CC.GREEN + "Please type your a new name for your kit", input -> {
                    if (!input.matches("^[A-Za-z0-9_]+$") || input.length() > 20) {
                        user.sendOverlay(MessageKeys.INVALID_CHARACTERS);
                        open();
                        return;
                    }
                    kit.setName(input);
                    saveChanges(user, kit);
                    inv.setTitle(CC.GRAY + "Editor: " + CC.GREEN + kit.getName());
                    open();
                }, this::open).open(getPlayer());
            });

            inv.setItem(13, new ItemBuilder(kit.getPlayStyle().getIcon())
                    .name(CC.AQUA + "Change PlayStyle")
                    .lore(CC.GRAY + "Click to change the kit playstyle", CC.GRAY + "Current PlayStyle: " + CC.GREEN + kit.getPlayStyle().getName())
                    .build(), p -> {
                kit.setPlayStyle(kit.getPlayStyle().next());
                saveChanges(user, kit);
                redrawAndOpen();
            });

            inv.setItem(26, new ItemBuilder(Material.TNT).name(CC.DARK_RED + "Reset Your Kit").build(), clickType -> {
                kit.reset();
                saveChanges(user, kit);
                inv.setTitle(CC.GRAY + "Editor: " + CC.GREEN + kit.getName());
                redrawAndOpen();
            });

            inv.fillBlank();

            if (!kit.getAbilities().isEmpty()) {
                AchievementManager.inst().trigger(Achievement.CREATOR, getPlayer());
            }
        }

    }

    private static class CurrentAbilitiesMenu extends SingleInventoryMenu {

        private KitData kit;

        public CurrentAbilitiesMenu(Player player, KitData kit) {
            super(player, CC.GREEN + "Current Abilities", 27);
            this.kit = kit;
            page.setHasBar(true);
        }

        @Override
        public void redraw() {

            InventoryPage inv = getCurrentPage();
            HubUser user = HubUser.getUser(getPlayer());

            int i = 0;
            for (KitAbilityData ability : kit.getAbilities()) {
                i++;
                AbilityLoader abilityData = AbilityLoader.getAbilityData(ability.getId());
                if (abilityData == null)
                    continue;
                List<String> lore = abilityData.fixAbilityDescription(user, kit.getAbility(abilityData.getId()).getUpgrades());

                lore.add(CC.RED + "Shift click to remove ability");
                inv.setItem(9 + i, new ItemEditor(abilityData.getAbilityIcon()).lore(lore).build(), clickType -> {
                    if (clickType == ClickType.SHIFT_RIGHT || clickType == ClickType.SHIFT_LEFT) {
                        kit.removeAbility(abilityData.getId());
                        user.sendOverlay(MessageKeys.KIT_ABILITY_REMOVED, "{ability}", abilityData.getRarity().getColor() + abilityData.getName());
                        saveChanges(user, kit);
                        inv.clear();
                        redrawAndOpen();
                        return;
                    }
                    new AbilityCustomizeMenu(user.getPlayer(), kit, abilityData, ability.cloneAbility()).open();
                });
            }

            inv.fillBlank();
        }

    }

    private static class AbilityListMenu extends SortedInventoryMenu {

        private KitData kit;
        private boolean keyStone;

        public AbilityListMenu(Player player, KitData kit, boolean keyStone) {
            super(player, "Abilities", 54, SortType.ALPHABETICAL, SortType.RARITY);
            this.kit = kit;
            this.keyStone = keyStone;
        }

        @Override
        public void redraw() {
            super.redraw();

            HubUser user = HubUser.getUser(getPlayer());

            for (AbilityLoader loader : AbilityLoader.getAbilities()) {
                if (kit.containsAbility(loader.getId()))
                    continue;
                if (!keyStone) {
                    if (!DeveloperSettings.freeAbilities(user)) {
                        if (!user.getPersistentData().hasAbility(loader.getId())) {
                            continue;
                        }
                    }
                }

                if (keyStone)
                    if (loader.getId() > 0)
                        continue;
                if (!keyStone)
                    if (loader.getId() < 0)
                        continue;

                ItemStack item = loader.getAbilityIcon();
                List<String> lore = GuiItem.getLore(item);
                new ItemEditor(item).lore(lore).build();

                SortedItem sort = new SortedItem(item, clickType -> {
                    if (!DeveloperSettings.freeAbilities(user)) {
                        if (!keyStone) {
                            if (!user.getPersistentData().hasAbility(loader.getId())) {
                                user.sendMessage(CC.RED + "You do not own " + loader.getName());
                                return;
                            }
                        }
                    }

                    KitAbilityData abilityData = new KitAbilityData(loader.getId());

                    if (keyStone) {
                        for (int i = kit.getAbilities().size() - 1; i >= 0; i--) {
                            KitAbilityData loop = kit.getAbilities().get(i);
                            if (loop.getId() < 0) {
                                kit.removeAbility(loop.getId());
                            }
                        }
//                        user.sendOverlay(CC.GRAY + "Selected " + loader.getRarity().getColor() + loader.getName() + " Keystone" + CC.GRAY + " for current kit");
                        kit.addAbility(abilityData);
                        saveChanges(user, kit);
                    }

                    new AbilityCustomizeMenu(user.getPlayer(), kit, loader, abilityData).open();
                });
                sort.setSortName(loader.getName());
                sort.setSortValue(loader.getRarity().ordinal());
                addSortItem(sort);

            }
            if (sortcache.size() == 0) {
                user.sendOverlay(MessageKeys.NO_ABILITIES);
            }
        }

    }

    private static class AbilityCustomizeMenu extends SingleInventoryMenu {

        private KitData toClone;
        private AbilityLoader loader;
        private KitAbilityData ability;

        public AbilityCustomizeMenu(Player player, KitData toClone, AbilityLoader loader, KitAbilityData ability) {
            super(player, CC.GREEN + "Customize Ability", 27);
            this.toClone = toClone;
            this.loader = loader;
            this.ability = ability;
            page.setHasBar(true);
        }

        @Override
        public void redraw() {

            super.redraw();

            KitData data = toClone.clone();
            HubUser user = HubUser.getUser(getPlayer());
            InventoryPage inv = getCurrentPage();

            int i = 9;
            for (AbilityUpgrade upgrade : loader.getAbilityUpgrades()) {

                Consumer<ClickType> clickProcessor = type -> {

                    boolean containsUpgrade = ability.getUpgrades().contains(upgrade.upgrade_number);
                    if (!containsUpgrade) {
                        ability.getUpgrades().add(upgrade.upgrade_number);
                    } else {
                        //noinspection UnnecessaryBoxing
                        ability.getUpgrades().remove(Integer.valueOf(upgrade.upgrade_number)); // Without boxing it removes according to index but we wanna remove the Object
                    }
                    new ItemEditor(getInventory().getItem(i + upgrade.upgrade_number)).durability((containsUpgrade ? DyeColor.RED : DyeColor.LIME).getWoolData()).build();
                    new ItemEditor(getInventory().getItem(8))
                            .lore(CC.GRAY + "Click to finalize your upgrades and", CC.GRAY + "add the ability to your kit", getPointsDescription(getAddedPoints(data, ability)))
                            .build();
                };

                boolean contains_upgrade = ability.getUpgrades().contains(upgrade.upgrade_number);
                List<String> description = fixUpgradeDescription(upgrade);

                if (contains_upgrade) {
                    ItemStack it = new ItemBuilder(Material.STAINED_GLASS_PANE).name(CC.AQUA + upgrade.name + " Upgrade").durability((short) 5).lore(description).build();
                    inv.setItem(i + upgrade.upgrade_number, it, clickProcessor);
                    continue;
                }


                inv.setItem(i + upgrade.upgrade_number,
                        new ItemBuilder(Material.STAINED_GLASS_PANE)
                                .name(CC.AQUA + upgrade.name + " Upgrade")
                                .durability(DyeColor.RED.getWoolData())
                                .lore(description)
                                .build(),
                        clickProcessor);
            }

            if (loader.isActive()) {
                inv.setItem(3, ability.getAbilityType().getIcon(), invClickType -> {
                    AbilityType newType = AbilityType.CLICK;
                    AbilityType type = ability.getAbilityType() == null ? AbilityType.CLICK : ability.getAbilityType();

                    if (type.equals(AbilityType.CLICK)) {
                        newType = AbilityType.SHIFT;
                    } else if (type.equals(AbilityType.SHIFT)) {
                        newType = AbilityType.BLOCK;
                    }
                    ability.setAbilityType(newType);
                    toClone.setInventory(null);
                    redrawAndOpen();
                });

                if (ability.getAbilityType() == AbilityType.CLICK) {

                    Material clickItem = ability.getClickItem();
                    if (clickItem == null) {
                        Set<Material> used = data.getAbilities().stream().map(KitAbilityData::getClickItem).collect(Collectors.toSet());
                        if (used.contains(loader.getAbilityIcon().getType()) || !CLICK_ITEM_POOL.contains(loader.getAbilityIcon().getType())) {
                            //noinspection OptionalGetWithoutIsPresent
                            clickItem = CLICK_ITEM_POOL.stream().filter(negate(used::contains)).findAny().get(); // We cannot run out of items
                        } else {
                            clickItem = loader.getAbilityIcon().getType();
                        }
                    }
                    ability.setClickItem(clickItem);
                    inv.setItem(5, new ItemBuilder(clickItem).name(CC.GREEN + "Change click item").build(), clickType -> new AbilityClickItemMenu(getPlayer(), data, ability).open());
                }
            }

            inv.setItem(8,
                    new ItemBuilder(Material.EMERALD)
                            .name(CC.GREEN + "Finalize Ability")
                            .lore(CC.GRAY + "Click to finalize your upgrades and", CC.GRAY + "add the ability to your kit", getPointsDescription(getAddedPoints(data, ability)))
                            .build(),
                    clickType -> {
                        double points = getAddedPoints(data, ability);
                        if (points > DeveloperSettings.MAX_KIT_POINTS) {
                            user.sendOverlay(MessageKeys.KIT_POINTS_EXCEEDED);
                            return;
                        }
                        if (!data.containsAbility(loader.getId())) {
                            user.sendMessage(CC.GRAY + "Added " + loader.getRarity().getColor() + loader.getName() + " Ability" + CC.GRAY + " to current kit");
                            data.addAbility(ability);
                        } else {
                            data.replaceAbility(ability);
                            user.sendMessage(CC.GRAY + "Updated " + loader.getRarity().getColor() + loader.getName() + " Ability" + CC.GRAY + " for current kit");
                        }

                        saveChanges(user, data);
                        new KitCustomizeMenu(user, data).open();
                    }
            );
            inv.fillBlank();
        }

    }

    private static class AbilityClickItemMenu extends SortedInventoryMenu {

        private Predicate<Material> filter;
        private final KitData data;
        private final KitAbilityData ability;

        public AbilityClickItemMenu(Player player, KitData data, KitAbilityData ability) {
            super(player, CC.GREEN + "Choose click item", 54, SortType.NONE);
            this.data = data;
            this.ability = ability;
            Set<Material> used = data.getAbilities().stream().map(KitAbilityData::getClickItem).collect(Collectors.toSet());
            filter = negate(used::contains);
        }

        @Override
        public void redraw() {
            super.redraw();

            CLICK_ITEM_POOL.stream().filter(filter).forEach(material -> {
                SortedItem item = new SortedItem(new ItemStack(material), type -> {
                    ability.setClickItem(material);
                    data.setInventory(null); // Reset custom inventory
                    getPreviousMenu().redrawAndOpen();
                });
                item.setSortName(material.name());
                addSortItem(item);
            });

        }
    }

    private static void saveChanges(User user, KitData kitData) {
        double points = kitData.getPoints();
        if (points > DeveloperSettings.MAX_KIT_POINTS) {
            user.getPlayer().sendMessage(CC.GRAY + "You may not have more than " + DeveloperSettings.MAX_KIT_POINTS + CC.GRAY + " points worth of abilities selected");
            return;
        }
        user.getPersistentData().updateKit(kitData);
    }

    private static List<String> fixUpgradeDescription(AbilityUpgrade upgrade) {
        ArrayList<String> lines = new ArrayList<>();
        for (String line : upgrade.description.split(", ")) {
            for (KitAttribute stat : upgrade.stats.getAttributeArray()) {
                if (line.contains("~" + stat.getAttributeName())) {
                    line = line.replace("~" + stat.getAttributeName(), AbilityLoader.getColorByAttribute(stat) + Math.abs(stat.getAttributeValue()));
                    if (line.contains("(hearts)")) {
                        line = line.replace("(hearts)", CC.GRAY + "(" + CC.RED + (stat.getAttributeValue() / (DeveloperSettings.HEALTH_CONVERTER * 2)) + " ❤'s" + CC.GRAY + ")");
                    }
                }

            }
            lines.add(CC.GRAY + line);
        }
        lines.add(CC.GRAY + "Point Cost: " + CC.GOLD + upgrade.points + " point" + (upgrade.points > 1 ? "s" : ""));

        return lines;
    }

    private static String getPointsDescription(double currentPoints) {
        double max_points = DeveloperSettings.MAX_KIT_POINTS;
        boolean over = currentPoints > max_points;
        String color = over ? CC.RED.toString() : CC.GREEN.toString();
        return CC.GRAY + "Current Points " + color + currentPoints + CC.GRAY + "/" + color + max_points;
    }

    public static double getAddedPoints(KitData data, KitAbilityData added) {

        double points = 0;
        for (KitAbilityData ability : data.getAbilities()) {
            if (ability.getId() == added.getId()) {
                continue;
            }
            points += KitData.getPointsForAbility(ability);
        }
        return points + KitData.getPointsForAbility(added);
    }

}
