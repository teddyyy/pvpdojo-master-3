/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.command;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilityinfo.AbilityCollection;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.command.staff.ServerPurchaseCommand;
import com.pvpdojo.mysql.SQLBuilder;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.bukkit.CC;

import java.sql.SQLException;
import java.util.UUID;

@CommandAlias("migrate")
public class TestCommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onTest(User user) {
        PvPDojo.schedule(() -> {
            long millisStart = System.currentTimeMillis();
            user.sendMessage(CC.GREEN + "STARTING MIGRATION");

            try (SQLBuilder sql = new SQLBuilder()) {
                sql.select(Tables.USERS, "uuid").executeQuery();

                while (sql.next()) {
                    UUID uuid = UUID.fromString(sql.getString("uuid"));
					for (int i = 0; i < 5; i++) {
						ServerPurchaseCommand.addCollection(uuid.toString(), AbilityCollection.BENDER);
					}
                    BungeeSync.updateUser(uuid);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            Log.info("done");
            user.sendMessage(CC.GREEN + "MIGRATION FINISHED IN " + (System.currentTimeMillis() - millisStart) / 1000 + " seconds");

        }).createAsyncTask();
    }

    @Override
    public Rank getRank() {
        return Rank.SERVER_ADMINISTRATOR;
    }
}
