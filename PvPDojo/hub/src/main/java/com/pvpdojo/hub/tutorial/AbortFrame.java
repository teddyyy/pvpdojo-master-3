/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.tutorial;

import org.bukkit.Bukkit;

import com.pvpdojo.hub.listeners.HubJoinListener;
import com.pvpdojo.hub.userdata.HubUser;
import com.pvpdojo.userdata.User;

import co.aikar.taskchain.TaskChain;

public class AbortFrame implements Frame {

    @Override
    public void apply(TaskChain<HubUser> chain) {

        chain.sync(user -> {
            if (user.isLoggedOut()) {
                return user;
            }

            if (user.getCurrentTutorial() == null) {
                user.setVanish(false);
                Bukkit.getOnlinePlayers().stream().map(User::getUser).forEach(online -> online.updateUserFor(user));
                user.setFrozen(false);
                user.detachHologram();

                HubJoinListener.spawnPlayer(user.getPlayer());
            }
            return user;

        });
        chain.abortIf(User::isLoggedOut);
        chain.abortIf(hubUser -> hubUser.getCurrentTutorial() == null);
    }

    @Override
    public void accept(User user) {}

}
