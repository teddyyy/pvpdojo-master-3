/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.market;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.DBCommon;
import com.pvpdojo.Discord;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilityinfo.AbilityCollection;
import com.pvpdojo.abilityinfo.AbilityLog;
import com.pvpdojo.achievement.Achievement;
import com.pvpdojo.achievement.AchievementManager;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.hub.market.PrivateTrade.TradePlayer.AbilityTradeItem;
import com.pvpdojo.hub.market.PrivateTrade.TradePlayer.MoneyTradeItem;
import com.pvpdojo.hub.market.PrivateTrade.TradePlayer.TradeItem;
import com.pvpdojo.hub.menus.ChestMenu;
import com.pvpdojo.hub.menus.TradeMenu;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.menu.InventoryMenu;
import com.pvpdojo.userdata.AbilityData;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.Request;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.Holder;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.Log;

public class PrivateTrade {

    private TradePlayer player1;
    private TradePlayer player2;

    public PrivateTrade(Player player1, Player player2) {
        this.player1 = new TradePlayer(player1);
        this.player2 = new TradePlayer(player2);
    }

    public static PrivateTrade startTradeSession(User player1, User player2) {
        if ((player1.getMenu() != null || player1.getInputGui() != null) || (player2.getMenu() != null || player2.getInputGui() != null)) {
            player1.sendMessage(PvPDojo.WARNING + "Player is currently in a menu");
            return null;
        }

        return new PrivateTrade(player1.getPlayer(), player2.getPlayer());
    }

    public void removeTradeItem(Player player, TradePlayer.TradeItem item) {
        TradePlayer tp = getTradePlayer(player);
        tp.removeTradeItem(item);
        this.updateTradeMenu();
    }

    public TradePlayer getTradePlayer(Player player) {
        if (player1.getPlayer().equals(player)) {
            return player1;
        }
        return player2;
    }

    public TradePlayer getOtherTradePlayer(Player player) {
        if (player1.getPlayer().equals(player)) {
            return player2;
        }
        return player1;
    }

    public List<TradePlayer> getTradePlayers() {
        return Arrays.asList(player1, player2);
    }

    public boolean isEmpty() {
        return getTradePlayers().stream().flatMap(tradePlayer -> tradePlayer.getItems().stream()).count() <= 0;
    }

    public void completeTrade() {
        this.closeTradeSession();

        if (isEmpty()) {
            sendMessage(CC.RED + "Trade did not perform any action. It was empty");
            return;
        }
        for (TradePlayer tradePlayer : getTradePlayers()) {
            User.getUser(tradePlayer.getPlayer()).getPersistentData().getAbilities().clear();
            AchievementManager.inst().trigger(Achievement.NEGOTIATOR, tradePlayer.getPlayer());
            if (tradePlayer.getItems().size() == 0) {
                Achievement.GENEROSITY.trigger(getOtherTradePlayer(tradePlayer.getPlayer()).getPlayer());
            }
        }
        PvPDojo.schedule(() -> {
            getTradePlayers().stream().flatMap(tradePlayer -> tradePlayer.getItems().stream()).forEach(TradePlayer.TradeItem::performTransaction);
            getTradePlayers().stream().map(TradePlayer::getMoneyTradeItem).forEach(MoneyTradeItem::performTransaction);
            getTradePlayers().forEach(tradePlayer -> User.getUser(tradePlayer.getPlayer()).getPersistentData().loadData(true));
            sendMessage(CC.GREEN + "Trade was successful");

            // Let's try to catch some foolish trades
            try {
                List<UUID> grid = DBCommon.getAccountGrid(getTradePlayers().get(0).getPlayer().getUniqueId());
                if (grid.contains(getTradePlayers().get(1).getPlayer().getUniqueId())) {
                    Discord.text().sendMessage("Report: " + getTradePlayers().get(0).getPlayer().getName() + " and "
                            + getTradePlayers().get(1).getPlayer().getName() + " Direct Account Grid Trade").complete();
                    return;
                }
            } catch (SQLException e) {
                Log.exception("", e);
            }

            for (TradePlayer tradePlayer : getTradePlayers()) {
                for (TradeItem tradeItem : tradePlayer.getItems()) {
                    if (tradeItem instanceof AbilityTradeItem) {
                        List<AbilityLog> abilityLogs = AbilityLog.getLogChain(((AbilityTradeItem) tradeItem).getValue());
                        UUID firstOwner = abilityLogs.get(0).getPreviousOwner();

                        if (!firstOwner.equals(getOtherTradePlayer(tradePlayer.getPlayer()).getPlayer().getUniqueId())) {
                            try {
                                List<UUID> sourceGrid = DBCommon.getAccountGrid(firstOwner);

                                // Ouch
                                if (sourceGrid.contains(getOtherTradePlayer(tradePlayer.getPlayer()).getPlayer().getUniqueId())) {
                                    Discord.text().sendMessage("Report: " + getTradePlayers().get(0).getPlayer().getName() + " and "
                                            + getTradePlayers().get(1).getPlayer().getName() + " Man in the middle").complete();

                                    break;
                                }
                            } catch (SQLException e) {
                                Log.exception("", e);
                            }
                        }
                    }
                }
            }

        }).createAsyncTask();
    }

    public void updateTradeMenu() {
        if (User.getUser(player1.getPlayer()).getMenu() instanceof TradeMenu) {
            User.getUser(player1.getPlayer()).getMenu().redrawAndOpen();
        }
        if (User.getUser(player2.getPlayer()).getMenu() instanceof TradeMenu) {
            User.getUser(player2.getPlayer()).getMenu().redrawAndOpen();
        }
    }

    public boolean isTradeLocked() {
        return player1.isLocked() && player2.isLocked();
    }

    public void lockTrade(Player player, boolean val) {
        this.getTradePlayer(player).setLocked(val);
        this.updateTradeMenu();
    }

    public void finalizeTrade(Player player) {
        this.getTradePlayer(player).setFinalized();
        this.updateTradeMenu();
        if (this.player1.isFinalized() && this.player2.isFinalized()) {
            this.completeTrade();
        }
    }

    public void sendMessage(String message) {
        this.player1.getPlayer().sendMessage(message);
        this.player2.getPlayer().sendMessage(message);
    }

    public void cancelTrade() {
        this.sendMessage(CC.RED + "The trade was cancelled by a player");
        this.closeTradeSession();
    }

    public void closeTradeSession() {
        InventoryMenu menu1 = User.getUser(player1.getPlayer()).getMenu();
        if (menu1 != null) {
            menu1.closeSafely();
        }
        InventoryMenu menu2 = User.getUser(player2.getPlayer()).getMenu();
        if (menu2 != null) {
            menu2.closeSafely();
        }
    }

    public static class TradeRequest extends Request {

        private User requester;

        public TradeRequest(User requester) {
            this.requester = requester;
        }

        @Override
        public void accept(User acceptor) {

            if (acceptor.getPlayer().getAddress().getHostString().equals(requester.getPlayer().getAddress().getHostString())) {
                acceptor.sendMessage(MessageKeys.WARNING, "{text}", "You cannot trade with that player");
                return;
            }

            PrivateTrade trade = startTradeSession(acceptor, requester);
            if (trade != null) {
                new TradeMenu(acceptor.getPlayer(), requester.getPlayer(), trade).open();
                new TradeMenu(requester.getPlayer(), acceptor.getPlayer(), trade).open();
            }
        }

        @Override
        public void deny(User deniedBy) {}

        @Override
        public int getExpirationSeconds() {
            return 20;
        }

        public boolean equals(Object obj) {
            return obj instanceof TradeRequest;
        }
    }

    public class TradePlayer {

        private boolean locked = false;
        private boolean finalized = false;
        private boolean confirmed = false;
        private Player player;
        private Set<TradeItem> items = new HashSet<>();
        private MoneyTradeItem moneyTradeItem = new MoneyTradeItem(new AtomicInteger());

        public TradePlayer(Player player) {
            this.player = player;
        }

        public Player getPlayer() {
            return player;
        }

        private void setLocked(boolean locked) {
            this.locked = locked;
        }

        public boolean isLocked() {
            return locked;
        }

        private void confirm() {
            this.confirmed = true;
        }

        public boolean isConfirmed() {
            return this.confirmed;
        }

        private void setFinalized() {
            this.finalized = true;
        }

        public boolean isFinalized() {
            return finalized;
        }

        public void addAbility(UUID abilityUUID) {
            addTradeItem(new AbilityTradeItem(abilityUUID));
        }

        private void addTradeItem(TradeItem item) {
            this.items.add(item);
            updateTradeMenu();
        }

        public boolean containsAbility(UUID abilityUUID) {
            return items.contains(new AbilityTradeItem(abilityUUID));
        }

        private void removeTradeItem(TradeItem item) {
            this.items.remove(item);
        }

        public Set<TradeItem> getItems() {
            return items;
        }

        public MoneyTradeItem getMoneyTradeItem() {
            return moneyTradeItem;
        }

        public abstract class TradeItem<T> extends Holder<T> {

            public TradeItem(T initialItem) {
                super(initialItem);
            }

            public abstract ItemStack getIcon();

            public abstract void performTransaction();

        }

        public class AbilityTradeItem extends TradeItem<UUID> {

            public AbilityTradeItem(UUID value) {
                super(value);
            }

            @Override
            public ItemStack getIcon() {
                AbilityData ability = User.getUser(player).getPersistentData().getAbility(getValue());
                ItemStack item = AbilityLoader.getAbilityData(ability.getId()).getAbilityIcon();
                List<String> lore = ability.getLoader().fixAbilityDescription(User.getUser(player), null);
                new ItemBuilder.ItemEditor(item).lore(lore).build();
                return item;
            }

            @Override
            public void performTransaction() {
                PersistentData.updateAbilityOwnership(getOtherTradePlayer(player).getPlayer().getUniqueId(), getValue(), "Trade");
            }

        }

        public class CollectionTradeItem extends AbilityTradeItem {

            public CollectionTradeItem(UUID value) {
                super(value);
            }

            @Override
            public ItemStack getIcon() {
                return ChestMenu.getItemFromChest(
                        Objects.requireNonNull(AbilityCollection.getCollectionById(User.getUser(player)
                                                                                       .getPersistentData()
                                                                                       .getAbility(getValue())
                                                                                       .getId())),
                        User.getUser(player).getPersistentData().getAbilities());
            }
        }

        public class MoneyTradeItem extends TradeItem<AtomicInteger> {

            public MoneyTradeItem(AtomicInteger value) {
                super(value);
            }

            @Override
            public ItemStack getIcon() {
                return new ItemBuilder(Material.GOLD_INGOT)
                        .amount(getValue().get() / 100)
                        .name(CC.GREEN + "Credits: " + CC.YELLOW + getValue())
                        .build();
            }

            @Override
            public void performTransaction() {
                User.getUser(player).getPersistentData().incrementMoney(-getValue().get());
                User.getUser(getOtherTradePlayer(player).getPlayer()).getPersistentData().incrementMoney(getValue().get());
            }
        }
    }


}
