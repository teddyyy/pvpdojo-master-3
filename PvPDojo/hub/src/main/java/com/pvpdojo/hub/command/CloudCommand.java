/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.hub.Hub;
import com.pvpdojo.hub.lobby.cloud.CTMCloud;
import com.pvpdojo.hub.lobby.cloud.Cloud;
import com.pvpdojo.redis.ServerInfo;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Subcommand;

@CommandAlias("cloud")
public class CloudCommand extends DojoCommand {

    @Subcommand("enable")
    public class CloudEnable extends DojoCommand {

        @Subcommand("ctm")
        public void onCTM(CommandSender sender) {
            enable(sender, CTMCloud.inst());
        }

        public void enable(CommandSender sender, Cloud cloud) {
            cloud.start();
            Hub.get().getConfig().set(cloud.getClass().getSimpleName(), true);
            Hub.get().saveConfig();
            sender.sendMessage(PvPDojo.PREFIX + CC.GREEN + "Enabled " + cloud.getClass().getSimpleName());
        }

        @Override
        public Rank getRank() {
            return Rank.SERVER_ADMINISTRATOR;
        }
    }

    @Subcommand("disable")
    public class CloudDisable extends DojoCommand {

        @Subcommand("ctm")
        public void onCTM(CommandSender sender) {
            disable(sender, CTMCloud.inst());
        }

        public void disable(CommandSender sender, Cloud cloud) {
            cloud.shutdown();
            Hub.get().getConfig().set(cloud.getClass().getSimpleName(), false);
            Hub.get().saveConfig();
            sender.sendMessage(PvPDojo.PREFIX + CC.RED + "Disabled " + cloud.getClass().getSimpleName());
        }

        @Override
        public Rank getRank() {
            return Rank.SERVER_ADMINISTRATOR;
        }
    }

    @Subcommand("info")
    public class CloudInfo extends DojoCommand {

        @Subcommand("ctm")
        public void onCTM(CommandSender sender) {
            send(sender, CTMCloud.inst());
        }

        public void send(CommandSender sender, Cloud cloud) {
            sender.sendMessage(PvPDojo.PREFIX + CC.GREEN + cloud.getClass().getSimpleName() + CC.GRAY + " is currently " + (cloud.isEnabled() ? CC.GREEN + "enabled" : CC.RED + "disabled"));
            if (cloud.isEnabled()) {
                for (ServerInfo info : cloud.getAvailableServers()) {
                    sender.sendMessage(info.getName() + ": " + info.getPlayers());
                }
            }
        }

        @Override
        public Rank getRank() {
            return Rank.SERVER_ADMINISTRATOR;
        }
    }

    @Subcommand("connect|join")
    public class CloudConnect extends DojoCommand {

        @Subcommand("ctm")
        public void onCTM(Player player) {
            connect(player, CTMCloud.inst());
        }

        public void connect(Player player, Cloud cloud) {
            cloud.connectPlayer(player);
        }

        @Override
        public Rank getRank() {
            return Rank.DEFAULT;
        }
    }

    @Override
    public Rank getRank() {
        return Rank.SERVER_ADMINISTRATOR;
    }
}
