/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.menus;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilityinfo.AbilityRarity;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.menus.AbilitiesMenu;
import com.pvpdojo.menus.ChallengeMenu;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.settings.FastDB;
import com.pvpdojo.userdata.AbilityData;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.WoolBuilder;

public class SalesmanMenu extends SingleInventoryMenu {

    private int transactionIndex;
    private List<AbilityData> input = new ArrayList<>();

    public SalesmanMenu(Player player) {
        super(player, "PvPDojo", 18);
        transactionIndex = User.getUser(player).getFastDB().getCurrentSalesmanTransaction();
    }

    @Override
    public void redraw() {
        super.redraw();

        page.setItem(3, new ItemBuilder(Material.PAPER).name(CC.GREEN + "Salesman").lore(
                "",
                CC.GRAY + "1st and 2nd Transaction:",
                CC.GRAY + "2 Blue abilities = 1 Chest",
                "",
                CC.GRAY + "3rd and 4th Transaction:",
                CC.GRAY + "1 Purple + 2 Blue abilities = 2 Chests",
                "",
                CC.GRAY + "5th and 6th Transaction:",
                CC.GRAY + "1 Purple + 3 Blue abilities = 3 Chests",
                "",
                CC.GRAY + "7th Transaction:",
                CC.GRAY + "1 Red ability = 5 chests",
                "",
                CC.LIGHT_PURPLE + "Cycle restarts after 7th transaction",
                CC.LIGHT_PURPLE + "Your current transaction: " + transactionIndex
        ).build(), null);

        for (int i = 0; i < input.size(); i++) {
            final int index = i;
            AbilityLoader loader = input.get(i).getLoader();
            page.setItem(9 + i, new ItemBuilder(loader.getAbilityIcon()).lore(loader.fixAbilityDescription(User.getUser(getPlayer()), null)).build(), type -> {
                input.remove(index);
                redrawAndOpen();
            });
        }

        switch (transactionIndex) {
            case 1:
            case 2:
                if (input.stream().filter(ability -> ability.getLoader().getRarity() == AbilityRarity.BLUE).count() == 2) {
                    finishButton(1);
                } else {
                    addAbilityButton();
                }
                break;
            case 3:
            case 4:
                if (input.stream().filter(ability -> ability.getLoader().getRarity() == AbilityRarity.BLUE).count() == 2
                        && input.stream().anyMatch(ability -> ability.getLoader().getRarity() == AbilityRarity.PURPLE)) {
                    finishButton(2);
                } else {
                    addAbilityButton();
                }
                break;
            case 5:
            case 6:
                if (input.stream().filter(ability -> ability.getLoader().getRarity() == AbilityRarity.BLUE).count() == 3
                        && input.stream().anyMatch(ability -> ability.getLoader().getRarity() == AbilityRarity.PURPLE)) {
                    finishButton(3);
                } else {
                    addAbilityButton();
                }
                break;
            case 7:
                if (input.stream().filter(ability -> ability.getLoader().getRarity() == AbilityRarity.RED).count() == 1) {
                    finishButton(5);
                } else {
                    addAbilityButton();
                }
                break;
            default:
                throw new IllegalStateException();
        }

        page.fillBlank();
    }

    private void addAbilityButton() {
        page.setItem(5, new ItemBuilder(Material.ENDER_CHEST).name(CC.GREEN + "Add ability").build(), type -> {
            new AbilitiesMenu(getPlayer(), User.getUser(getPlayer()).getPersistentData().getAbilities(), (abilityData, ignore) -> {
                input.add(abilityData);
                redrawAndOpen();
            }, abilityData -> {
                if (transactionIndex != 7 && abilityData.getLoader().getRarity().ordinal() > AbilityRarity.PURPLE.ordinal()) {
                    return true;
                } 
                if(transactionIndex == 7 && abilityData.getLoader().getRarity().ordinal() != AbilityRarity.RED.ordinal())
                	return true;

                if (input.stream().anyMatch(abilityData::equals)) {
                    return true;
                }

                switch (transactionIndex) {
                    case 1:
                    case 2:
                        return abilityData.getLoader().getRarity() == AbilityRarity.PURPLE
                                || (input.stream().filter(ability -> ability.getLoader().getRarity() == AbilityRarity.BLUE).count() == 2 && abilityData.getLoader().getRarity() == AbilityRarity.BLUE);
                    case 3:
                    case 4:
                        return (input.stream().filter(ability -> ability.getLoader().getRarity() == AbilityRarity.BLUE).count() == 2 && abilityData.getLoader().getRarity() == AbilityRarity.BLUE)
                                || (abilityData.getLoader().getRarity() == AbilityRarity.PURPLE && input.stream().anyMatch(ability -> ability.getLoader().getRarity() == AbilityRarity.PURPLE));
                    case 5:
                    case 6:
                        return  (abilityData.getLoader().getRarity() == AbilityRarity.BLUE && input.stream().filter(ability -> ability.getLoader().getRarity() == AbilityRarity.BLUE).count() == 3)
                                || (input.stream().anyMatch(ability -> ability.getLoader().getRarity() == AbilityRarity.PURPLE) && abilityData.getLoader().getRarity() == AbilityRarity.PURPLE);
                
                    case 7:
                    	 return (input.stream().filter(ability -> ability.getLoader().getRarity() == AbilityRarity.RED).count() == 1
                                 && abilityData.getLoader().getRarity() == AbilityRarity.RED);
                        
                    default:
                        throw new IllegalStateException();
                }
            }).open();
        });
    }

    private void finishButton(int reward) {
        page.setItem(5, new WoolBuilder(DyeColor.GREEN).name(CC.GREEN + "Finish").build(), type -> {
            closeSafely();

            PvPDojo.schedule(() -> {
                FastDB fastDB = User.getUser(getPlayer()).getFastDB();
                fastDB.setCurrentSalesmanTransaction(++transactionIndex > 7 ? 1 : transactionIndex);
                fastDB.save(getPlayer().getUniqueId());
                for (AbilityData abilityData : input) {
                    PersistentData.updateAbilityOwnership(null, abilityData.getAbilityUUID(), "Salesman");
                }
                ChallengeMenu.rewardCollections(User.getUser(getPlayer()), reward);
                BungeeSync.updateUser(getPlayer().getUniqueId());
            }).createAsyncTask();

        });
    }

}
