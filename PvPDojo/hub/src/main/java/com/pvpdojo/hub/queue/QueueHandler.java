/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.queue;

import static com.pvpdojo.util.StreamUtils.negate;
import static com.pvpdojo.util.StringUtils.PIPE;
import static com.pvpdojo.util.StringUtils.getEnumName;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.libs.com.google.gson.reflect.TypeToken;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import com.google.common.collect.Table;
import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.clan.Clan;
import com.pvpdojo.hub.menus.HubHotbarGUI;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.party.Party;
import com.pvpdojo.party.PartyList;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.network.SessionRequest;
import com.pvpdojo.session.network.component.ClanComponent;
import com.pvpdojo.session.network.component.PokerComponent;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.PlayStyle;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.ExpireHashMap;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.bukkit.CC;

import redis.clients.jedis.JedisPubSub;

public class QueueHandler extends JedisPubSub {

    private static QueueHandler instance = new QueueHandler();

    public static QueueHandler inst() {
        return instance;
    }

    // Stores player count information session specific which is automatically sent by online deathmatch servers
    // Mapping <ServerName, SessionType, PlayerCount>
    private Table<String, SessionType, Integer> playerCount = HashBasedTable.create();
    // Parameter shit for GSON
    private static Type playerCountMapType = new TypeToken<Map<SessionType, Integer>>() {}.getType();

    private ExpireHashMap<UUID, UUID> lastDuel = new ExpireHashMap<>(10, TimeUnit.MINUTES);
    private ExpireHashMap<UUID, QueueItem> lastQueue = new ExpireHashMap<>(10, TimeUnit.MINUTES, (uuid, session) -> {
        Player expired = Bukkit.getPlayer(uuid);
        if (expired != null && User.getUser(expired).getHotbarGui() == HubHotbarGUI.get(expired)) {
            expired.getInventory().setItem(2, new ItemStack(Material.AIR));
        }
    });

    private Set<UUID> requestLock = new HashSet<>();
    private Multimap<SessionType, QueueItem> sessionQueue = HashMultimap.create();
    private Multimap<SessionType, UUID> partyQueue = HashMultimap.create();
    private Multimap<SessionType, Party> partySessionQueue = HashMultimap.create();
    private List<ClanQueueItem> clanQueue = new ArrayList<>();

    @Override
    public void onMessage(String channel, String message) {
        try {

            if (Redis.get().isTest()) {
                channel = channel.replaceFirst("test_", "");
            }

            if (Redis.get().isUsProxy()) {
                channel = channel.replaceFirst(BungeeSync.US_PROXY_PREFIX, "");
            }

            if (channel.equals(BungeeSync.SESSION)) {
                String[] split = PIPE.split(message);
                int packetId = Integer.parseInt(split[0]);

                if (packetId == BungeeSync.PLAYERCOUNT) {
                    Map<String, Integer> serverPlayerCount = BungeeSync.getServerPlayerCount();
                    serverPlayerCount.values().removeIf(BungeeSync.OFFLINE); // We need to check against online servers

                    PvPDojo.schedule(() -> {
                        playerCount.rowKeySet().removeIf(negate(serverPlayerCount.keySet()::contains)); // Clear dead servers
                        playerCount.row(split[1]).putAll(DBCommon.GSON.fromJson(split[2], playerCountMapType)); // Finally process the fresh data set
                    }).sync();
                }
            }
        } catch (Throwable t) {
            Log.exception("Handling Redis Packet ch: " + channel + " m: " + message, t);
        }
    }

    public UUID getLastDuel(UUID uuid) {
        return lastDuel.get(uuid);
    }

    public UUID popLastDuel(UUID uuid) {
        return lastDuel.remove(uuid);
    }

    public QueueItem popLastQueue(UUID uuid) {
        return lastQueue.remove(uuid);
    }

    public QueueItem getLastQueue(UUID uuid) {
        return lastQueue.get(uuid);
    }

    public void setLastDuel(UUID uuid, UUID opponent) {
        lastDuel.put(uuid, opponent);
        lastDuel.put(opponent, uuid);
    }

    public int getPlayerInGame(SessionType type) {
        if (playerCount.containsColumn(type)) {
            return playerCount.columnMap().get(type).values().stream().mapToInt(Integer::intValue).sum();
        }
        return 0;
    }

    public int getPlayersInQueue(QueueItem item) {
        SessionType type = item.getSessionType();
        return (int) (sessionQueue.get(type).stream().filter(item::sameQueue).count() + partySessionQueue.get(type).stream().mapToInt(Party::size).sum() + partyQueue.get(type).size());
    }

    /**
     * @param uuid The player to enter the queue
     * @param item {@link QueueItem} containing information about the queue request
     * @return returns <code>null</code> if players has been queued otherwise a user friendly message is returned
     */
    public String queue(UUID uuid, QueueItem item) {

        SessionType type = item.getSessionType();

        if (sessionQueue.get(type).contains(item) || requestLock.contains(uuid)) {
            return PvPDojo.LANG.createDynamicKey(MessageKeys.QUEUE_ALREADY_QUEUED);
        }

        SessionRequest request = new SessionRequest().type(type).playStyle(item.getStyle());

        switch (type) {
            case BOT_DUEL:
                submitDeathmatchRequest(request.players(uuid));
                break;
            case POKER:
                PokerQueueItem pokerMatch = (PokerQueueItem) sessionQueue.get(type).stream().filter(item::sameQueue).findAny().orElse(null);

                if (pokerMatch != null) {
                    Map<UUID, Integer> kitMap = new HashMap<>();
                    kitMap.put(pokerMatch.getUniqueId(), pokerMatch.getKit());
                    kitMap.put(item.getUniqueId(), ((PokerQueueItem) item).getKit());
                    submitDeathmatchRequest(request.special(new PokerComponent(kitMap)).players(uuid, pokerMatch.getUniqueId()));
                } else {
                    sessionQueue.put(type, item);
                }
                break;
            case DUEL_CASUAL_NO_ABILITY:
            case DUEL_CASUAL:
                QueueItem match = sessionQueue.get(type).stream().filter(item::sameQueue).findAny().orElse(null);

                if (match != null) {
                    submitDeathmatchRequest(request.players(uuid, match.getUniqueId()).playStyle(item.getStyle()));
                } else {
                    sessionQueue.put(type, item);
                }
                break;
            case DUEL_RANKED:
                match = sessionQueue.get(type)
                                    .stream()
                                    .filter(item::sameQueue)
                                    .filter(queueItem -> Math.abs(User.getUser(queueItem.getUniqueId()).getElo() - User.getUser(uuid).getElo()) < 350)
                                    .filter(queueItem -> !User.getUser(uuid).getCurrentAccountGrid().contains(queueItem.getUniqueId()))
                                    .findAny().orElse(null);

                if (match != null) {
                    submitDeathmatchRequest(request.players(uuid, match.getUniqueId()));
                } else {
                    sessionQueue.put(type, item);
                }
                break;
            case FEAST_FIGHT:
                if (sessionQueue.get(type).size() == 3) {
                    submitDeathmatchRequest(request.playStyle(PlayStyle.SOUP)
                                                   .players(uuid)
                                                   .players(sessionQueue.values().stream().map(QueueItem::getUniqueId).toArray(UUID[]::new)));
                } else {
                    sessionQueue.put(type, item);
                }
                break;
            case TEAM_DUEL:
            case TEAM_DUEL_RANKED:
                return queue(User.getUser(uuid).getParty(), uuid, type);
            default:
                throw new IllegalStateException("Queue not recognized");

        }

        return null;
    }

    /**
     * @param party The party to enter the queue
     * @param uuid  The player that is requesting their party to queue up
     * @param type  {@link SessionType} of the queue to enter
     * @return returns <code>null</code> if players has been queued otherwise a user friendly message is returned
     */
    public String queue(Party party, UUID uuid, SessionType type) {
        if (party == null) { // Player joined a team queue as a single (no friends?)
            if (partyQueue.get(type).contains(uuid)) {
                return PvPDojo.LANG.createDynamicKey(MessageKeys.QUEUE_ALREADY_QUEUED);
            }
            UUID otherForParty = Iterables.getFirst(partyQueue.get(type), null);
            if (otherForParty != null) { // Found another single (parship)
                unqueue(otherForParty);

                // Let's fire a new party onto the network and queue them up
                PvPDojo.newChain()
                       .asyncFirst(() -> PartyList.createParty(otherForParty, Collections.singletonList(uuid), true))
                       .syncLast(newParty -> queue(newParty, otherForParty, type))
                       .execute();

                return null;
            } else {
                partyQueue.put(type, uuid);
                return null;
            }
        }

        // Prevent shit
        if (!party.getLeader().equals(uuid)) {
            return "You must be the leader of the party";
        }
        if (party.getUsers().size() <= 1) {
            return "Your party is invalid";
        }
        if (partySessionQueue.get(type).contains(party)) {
            return PvPDojo.LANG.createDynamicKey(MessageKeys.QUEUE_ALREADY_QUEUED);
        }

        Party opponent = null;
        party.channelMessage("Entered the " + CC.GREEN + getEnumName(type) + CC.GRAY + " queue");
        switch (type) {
            case TEAM_DUEL:
                Iterator<Party> iterator = partySessionQueue.get(type).iterator();
                while (iterator.hasNext()) {
                    Party queued = iterator.next();
                    if (!queued.isValid()) { // Clear dead parties
                        iterator.remove();
                        continue;
                    }
                    if (queued.getUsers().size() == party.getUsers().size()) {
                        opponent = queued;
                    }
                }
                if (opponent != null) {
                    SessionRequest request = new SessionRequest();
                    submitDeathmatchRequest(request.type(type)
                                                   .parties(party.getPartyId(), opponent.getPartyId())
                                                   .players(party.getUsers())
                                                   .players(opponent.getUsers()));
                } else {
                    partySessionQueue.put(type, party);
                }
                break;
            default:
                throw new IllegalStateException("Queue not recognized");
        }
        return null;
    }

    public boolean isQueued(UUID uuid, QueueItem item) {
        return sessionQueue.containsEntry(item.getSessionType(), item)
                || partySessionQueue.entries().stream().anyMatch(entry -> entry.getKey() == item.getSessionType() && entry.getValue().getLeader().equals(uuid))
                || partyQueue.containsEntry(item.getSessionType(), uuid);
    }

    public boolean unqueue(UUID uuid, QueueItem item) {
        return sessionQueue.remove(item.getSessionType(), item)
                || partySessionQueue.entries().removeIf(entry -> entry.getKey() == item.getSessionType() && entry.getValue().getLeader().equals(uuid))
                || partyQueue.remove(item.getSessionType(), uuid);
    }

    public void unqueue(UUID uuid) {
        partySessionQueue.values().removeIf(party -> uuid.equals(party.getLeader()));
        sessionQueue.values().removeIf(queueItem -> queueItem.getUniqueId().equals(uuid));
        partyQueue.values().removeIf(uuid::equals);
    }

    public void queueClan(ClanQueueItem clanQueueItem) {
        ClanQueueItem opponent = Iterables.getFirst(clanQueue, null);
        if (opponent != null) {
            clanQueue.remove(opponent);
            Clan lowerRated = clanQueueItem.getClan().getElo() <= opponent.getClan().getElo() ? clanQueueItem.getClan() : opponent.getClan();
            ClanComponent clanComponent = new ClanComponent(lowerRated != opponent.getClan() ? opponent.getClan() : clanQueueItem.getClan(), lowerRated);

            clanComponent.getGameModesToPlay().add(lowerRated == opponent.getClan() ? opponent.getDesiredGameModes().poll() : clanQueueItem.getDesiredGameModes().poll());
            clanComponent.getGameModesToPlay().add(lowerRated != opponent.getClan() ? opponent.getDesiredGameModes().poll() : clanQueueItem.getDesiredGameModes().poll());
            clanComponent.getGameModesToPlay().add(lowerRated == opponent.getClan() ? opponent.getDesiredGameModes().poll() : clanQueueItem.getDesiredGameModes().poll());
            clanComponent.getGameModesToPlay().add(lowerRated != opponent.getClan() ? opponent.getDesiredGameModes().poll() : clanQueueItem.getDesiredGameModes().poll());

            clanComponent.setLineup(opponent.getClan(), opponent.getLineup().stream().map(DojoOfflinePlayer::getUniqueId).collect(Collectors.toSet()));
            clanComponent.setLineup(clanQueueItem.getClan(), clanQueueItem.getLineup().stream().map(DojoOfflinePlayer::getUniqueId).collect(Collectors.toSet()));

            clanComponent.sendMessage("Get ready clan fight begins in 10 seconds");
            clanComponent.sendCurrentScore();
            clanComponent.createParties();
            clanComponent.advance(10);

        } else {
            clanQueue.add(clanQueueItem);
            clanQueueItem.getClan().sendMessage("Queued for a clan fight");
        }
    }

    public void unlock(UUID uuid) {
        requestLock.remove(uuid);
    }

    public boolean lock(UUID uuid) {
        return requestLock.add(uuid);
    }

    public void submitDeathmatchRequest(SessionRequest request) {

        // Throw them out of the system they're going on deathmatch vacation
        request.getPlayers().forEach(this::unqueue);
        for (UUID cur : request.getPlayers()) {

            if (!lock(cur)) { // Cancel request if there is a player already awaiting their match
                return;
            }

            // Store their last request type for quick access after their return
            lastQueue.put(cur, new QueueItem(cur, request.getPlayStyle(), request.getType()));
        }

        request.sendToServer(ServerType.DEATHMATCH, () -> request.getPlayers().forEach(this::unlock));

    }

}
