/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.command;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Conditions;
import co.aikar.commands.annotation.Default;

@CommandAlias("clanfight")
public class ClanFightCommand extends DojoCommand {

    @Default
    @CatchUnknown
    @Conditions("hasclan|clanleader")
    public void onClanFight(User user) {
//        PvPDojo.newChain().asyncFirst(() -> user.getClan().getAllMembers().stream().map(PvPDojo::getOfflinePlayer).collect(Collectors.toSet())).syncLast(input -> {
//            new ClanFightMenus.ChooseLineupMenu(user.getPlayer(), input).open();
//        }).execute();
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
