/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

import com.pvpdojo.PvPDojo;

public class HubPlayerDropListener implements Listener {

    public static void register() {
        Bukkit.getPluginManager().registerEvents(new HubPlayerDropListener(), PvPDojo.get());
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        e.setCancelled(true);
    }

}
