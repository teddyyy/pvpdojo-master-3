/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.lobby;

import static com.pvpdojo.util.StreamUtils.negate;
import static com.pvpdojo.util.StringUtils.MOJANG_UUID;
import static com.pvpdojo.util.StringUtils.PIPE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.base.Preconditions;
import com.pvpdojo.DBCommon;
import com.pvpdojo.Gamemode;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.settings.GameSettings;
import com.pvpdojo.hub.Hub;
import com.pvpdojo.hub.lobby.exception.AlreadyHostingException;
import com.pvpdojo.hub.lobby.exception.LobbyAlreadyStartedException;
import com.pvpdojo.hub.menus.HubHotbarGUI;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.BroadcastUtil;
import com.pvpdojo.util.CompBuilder;
import com.pvpdojo.util.Countdown;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.bukkit.CC;

import net.md_5.bungee.api.ChatColor;
import redis.clients.jedis.JedisPubSub;


public class LobbyManager extends JedisPubSub {

    public static final String POOL_CONFIG_KEY = "serverpool";
    public static final String RUNNING_SERVERS = "runningservers"; // This is a config list to keep track of running servers after restarting the Hub server
    public static final List<UUID> SERVER_POOL = Hub.get().getConfig().getStringList(POOL_CONFIG_KEY).stream().map(UUID::fromString).collect(Collectors.toList());

    private static final LobbyManager INST = new LobbyManager();

    public static LobbyManager inst() {
        return INST;
    }

    private Map<UUID, Lobby> activeLobbies = new ConcurrentHashMap<>();

    {
        // Load in
        activeLobbies.putAll(Hub.get().getConfig().getStringList(RUNNING_SERVERS).stream()
                                .map(json -> DBCommon.GSON.fromJson(json, Lobby.class))
                                .filter(lobby -> BungeeSync.getServerPlayerCount().containsKey(lobby.getNetworkIdentifier()))
                                .collect(Collectors.toMap(lobby -> lobby.getInfo().getHost(), Function.identity())));
    }

    @Override
    public void onMessage(String channel, String message) {
        try {
            if (Redis.get().isTest()) {
                channel = channel.replaceFirst("test_", "");
            }

            switch (channel) {
                case BungeeSync.REGISTER_SERVER:
                    String identifier = "game_" + PIPE.split(message)[0];
                    Log.info(identifier);
                    activeLobbies.values().stream().filter(lobby -> lobby.getNetworkIdentifier().equals(identifier)).findFirst().ifPresent(lobby -> {
                        lobby.setRunning(true);
                        Log.info("Server started for " + lobby.getInfo().getHost());
                        if (Bukkit.getPlayer(lobby.getInfo().getHost()) != null) {
                            Bukkit.getPlayer(lobby.getInfo().getHost()).sendMessage(CC.GREEN + "Booting..."); // Register packet occurs before server is loaded, tell them its booting
                        }
                    });
                    break;
                case BungeeSync.UNREGISTER_SERVER:
                    Iterator<Lobby> itr = activeLobbies.values().iterator();

                    while (itr.hasNext()) {
                        Lobby lobby = itr.next();

                        if (lobby.getNetworkIdentifier().equals(message)) {
                            itr.remove();
                            Log.info(lobby.getInfo().getHost() + " stopped: Removing lobby");
                            if (MOJANG_UUID.matcher(lobby.getInfo().getHost().toString()).matches()) {
                                BungeeSync.sendMessage(lobby.getInfo().getHost(), PvPDojo.PREFIX + CC.GREEN + CC.BOLD + "Your Lobby has been closed");
                            }
                            break;
                        }
                    }
                    break;
            }
        } catch (Throwable throwable) {
            Log.exception(throwable);
        }
    }

    public <T extends GameSettings> Lobby<T> createLobby(Gamemode type, T settings) throws AlreadyHostingException {
        Log.info(activeLobbies.keySet().toString());

        Optional<UUID> consoleHost = SERVER_POOL.stream().filter(negate(activeLobbies::containsKey)).findAny();
        UUID newHost = UUID.randomUUID();

        if (!consoleHost.isPresent()) {

            SERVER_POOL.add(newHost);
            Hub.get().getConfig().set(POOL_CONFIG_KEY, SERVER_POOL.stream().map(UUID::toString).collect(Collectors.toList()));
            Hub.get().saveConfig();

        }

        Lobby<T> lobby = createLobby(consoleHost.orElse(newHost), type, settings);
        activeLobbies.put(lobby.getInfo().getHost(), lobby);

        return lobby;
    }

    public <T extends GameSettings> Lobby<T> createLobby(UUID host, Gamemode type, T settings) throws AlreadyHostingException {
        Preconditions.checkNotNull(host);
        Preconditions.checkNotNull(type);

        if (activeLobbies.containsKey(host)) {
            throw new AlreadyHostingException(activeLobbies.get(host));
        }

        return new Lobby<>(new LobbyInfo<>(host, type, settings));
    }

    public boolean publishLobby(CommandSender sender, Lobby<?> lobby) {

        if (lobby.isStarted() || lobby.isPublished()) {
            throw new LobbyAlreadyStartedException();
        }

        lobby.setPublished(true);

        if (!lobby.isPublicLobby()) {
            User user;
            if (sender instanceof Player) {
                if ((user = User.getUser((Player) sender)).getParty() != null) {
                    lobby.getParticipants().addAll(user.getParty().getUsers());
                    if (checkStartCondition(lobby)) {
                        lobby.start(sender);
                    }
                    return true;
                } else {
                    sender.sendMessage(CC.RED + "You must be in a party for a private game");

                }
            } else if (checkStartCondition(lobby)) {
                lobby.start(sender);
            } else {
                Log.warn("Could not start lobby " + lobby.toString());
            }
        } else {
            String readableHost = LobbyManager.SERVER_POOL.contains(lobby.getInfo().getHost()) ? "Console" : Bukkit.getPlayer(lobby.getInfo().getHost()).getNick();
            lobby.setCurrentCountdown(new Countdown(lobby.getCountdownTime())
                    .userCounter(i -> {
                        if (i % 5 == 0) {
                            BroadcastUtil.global(ChatChannel.GLOBAL,
                                    new CompBuilder(PvPDojo.PREFIX)
                                            .append(lobby.getInfo().getType().name())
                                            .color(ChatColor.GOLD)
                                            .append(" lobby is starting in ")
                                            .color(ChatColor.GRAY)
                                            .append(String.valueOf(i))
                                            .color(ChatColor.RED)
                                            .append(" seconds hosted by " + readableHost + " ")
                                            .color(ChatColor.GRAY)
                                            .append("[Join]")
                                            .color(ChatColor.GREEN)
                                            .command("lobby " + lobby.getInfo().getHost())
                                            .create());
                        }
                    })
                    .finish(() -> {
                        if (checkStartCondition(lobby)) {
                            lobby.start(sender);
                        }
                    })
                    .start());
            return true;
        }
        return false;
    }

    private boolean checkStartCondition(Lobby<?> lobby) {
        if (lobby.getParticipants().size() < lobby.getInfo().getSettings().getMinPlayers()) {
            LobbyManager.inst().removeLobby(lobby);
            lobby.broadcast(CC.RED + "Not enough players joined the game");

            for (UUID uuid : lobby.getParticipants()) {
                if (Bukkit.getPlayer(uuid) != null) {
                    User.getUser(uuid).setHotbarGui(HubHotbarGUI.get(Bukkit.getPlayer(uuid)));
                }
            }
            return false;
        }
        return true;
    }

    public void addLobby(Lobby lobby) {
        activeLobbies.put(lobby.getInfo().getHost(), lobby);
    }

    public void removeLobby(Lobby lobby) {
        activeLobbies.remove(lobby.getInfo().getHost());
    }

    public Lobby getLobby(UUID host) {
        return activeLobbies.get(host);
    }

    public boolean hasLobby(UUID host) {
        return activeLobbies.containsKey(host);
    }

    public Collection<Lobby> getLobbies() {
        return activeLobbies.values();
    }

    public void disconnectPlayer(Player player) {
        for (Lobby lobby : activeLobbies.values()) {
            if (lobby.getParticipants() != null && lobby.getParticipants().contains(player.getUniqueId())) {
                if (lobby.removeParticipant(player)) {
                    lobby.broadcast(CC.BLUE + player.getNick() + CC.GRAY + " left the lobby " + CC.RED + "[" + lobby.getParticipants().size() + "]");
                }
            }
        }
    }

    public void shutdown() {
        unsubscribe();

        List<String> runningLobbies = new ArrayList<>();
        for (Lobby lobby : activeLobbies.values()) {
            if (lobby.isRunning()) {
                if (BungeeSync.getServerPlayerCount().get(lobby.getNetworkIdentifier()) > 0) {
                    runningLobbies.add(DBCommon.GSON.toJson(lobby));
                } else {
                    lobby.stop();
                }
            }
        }

        Hub.get().getConfig().set(RUNNING_SERVERS, runningLobbies);
        Hub.get().saveConfig();
    }

}
