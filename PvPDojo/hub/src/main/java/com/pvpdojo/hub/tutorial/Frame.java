/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.tutorial;

import com.pvpdojo.hub.userdata.HubUser;
import com.pvpdojo.userdata.User;

import co.aikar.taskchain.TaskChain;

public interface Frame {

    default void apply(TaskChain<HubUser> chain) {
        chain.sync(input -> {
            accept(input);
            return input;
        });
    }

    void accept(User user);

}
