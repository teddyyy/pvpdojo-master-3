/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.menus;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.pvpdojo.hub.Hub;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class LobbyWarpMenu extends SingleInventoryMenu {

    public LobbyWarpMenu(Player player) {
        super(player, CC.DARK_GREEN + "Lobby Warps", 9);
        setCloseOnClick(true);
    }

    @Override
    public void redraw() {
        InventoryPage inv = getCurrentPage();

        inv.setItem(0, new ItemBuilder(Material.REDSTONE).name(CC.RED + "Spawn").build(), clickType -> getPlayer().teleport(Hub.get().getSpawn()));
        inv.setItem(2, new ItemBuilder(Material.ANVIL).name(CC.RED + "Kit Creator").build(), clickType -> getPlayer().teleport(Hub.get().getKitcreation()));
        inv.setItem(4, new ItemBuilder(Material.PISTON_BASE).name(CC.RED + "Market").build(), clickType -> getPlayer().teleport(Hub.get().getMarket()));
        inv.setItem(6, new ItemBuilder(Material.SKULL_ITEM).name(CC.RED + "Staff").build(), clickType -> getPlayer().teleport(Hub.get().getStaff()));
        inv.setItem(8, new ItemBuilder(Material.BOOK).name(CC.RED + "Leaderboard").build(), clickType -> getPlayer().teleport(Hub.get().getLeaderboard()));

        inv.fillBlank();
    }

}
