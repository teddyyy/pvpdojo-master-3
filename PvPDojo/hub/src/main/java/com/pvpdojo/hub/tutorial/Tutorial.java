/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.tutorial;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.hub.userdata.HubUser;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.taskchain.TaskChain;

public class Tutorial {

    public static final Tutorial MARKET_TUTORIAL = new Tutorial()
            .freeze()
            .teleport(PvPDojo.get().getStandardMap().getLocation("market_tut_0"))
            .text(CC.GOLD + "The Market", CC.GRAY + " - ", CC.GREEN + "A place to sell, trade and upgrade")
            .delay(3 * 20)
            .text(CC.GOLD + "Trading")
            .delay(2 * 20)
            .text(CC.GREEN + "You can trade your abilities with other players",
                    CC.GREEN + "by right clicking them with the blaze rod.")
            .delay(5 * 20)
            .text(CC.GOLD + "Trade Menu:",
                    CC.AQUA + "- " + CC.GREEN + "Add Abilities by clicking the " + CC.YELLOW + "left enderchest",
                    CC.AQUA + "- " + CC.GREEN + "Add credits by clicking the " + CC.YELLOW + "gold ingot",
                    CC.AQUA + "- " + CC.GREEN + "You can revoke your input by " + CC.YELLOW + "shift " + CC.GREEN + "clicking")
            .delay(16 * 20)
            .text(CC.GREEN + "When both players click the emerald",
                    CC.GREEN + "you can no longer edit the trade.",
                    CC.GREEN + "To accept the trade you will be prompted",
                    CC.GREEN + "with several confirmation screens.",
                    " ",
                    CC.DARK_RED + "ALWAYS double check your trade before accepting.",
                    CC.DARK_RED + "Be aware of scammers.")
            .delay(20 * 20)
            .text(CC.GOLD + "Market")
            .delay(2 * 20)
            .text(CC.GREEN + "You can add abilities to the market by",
                    CC.GREEN + "clicking on the enderchest in the market area.")
            .delay(5 * 20)
            .teleport(PvPDojo.get().getStandardMap().getLocation("market_tut_1"))
            .text(CC.GREEN + "You can access the market by clicking on the " + CC.YELLOW + "piston blocks.",
                    CC.GREEN + "You can now search, sort, browse and buy offers.")
            .delay(11 * 20)
            .text(CC.GOLD + "Trade-Up")
            .teleport(PvPDojo.get().getStandardMap().getLocation("market_tut_0"))
            .delay(2 * 20)
            .text(CC.GREEN + "You can trade-up your abilities of the",
                    CC.GREEN + "same rarity to a higher rarity.")
            .delay(6 * 20)
            .text(CC.GREEN + "In order to do that you need to click on the workbench",
                    CC.GREEN + "and " + CC.YELLOW + "add 6 abilities " + CC.GREEN + "of the same rarity.",
                    CC.GREEN + "Once accepted you will be granted " + CC.YELLOW + "1 higher ability" + CC.GREEN + ".")
            .delay(10 * 20)
            .end();

    public static final Tutorial KIT_CREATOR_TUTORIAL = new Tutorial()
            .freeze()
            .teleport(PvPDojo.get().getStandardMap().getLocation("kit_tut_0"))
            .text(CC.GOLD + "The Kit Creator", CC.GREEN + "A place to create your custom kits")
            .delay(3 * 20)
            .text(CC.GREEN + "You can create your own kits by clicking an anvil.")
            .delay(4 * 20)
            .text(CC.GOLD + "Kit creation menu",
                    " ",
                    CC.GREEN + "- " + CC.YELLOW + "Add " + CC.GREEN + "abilities by clicking the enderchest",
                    CC.GREEN + "- " + CC.YELLOW + "Edit " + CC.GREEN + "your current abilities by clicking the normal chest",
                    CC.GREEN + "  -> " + CC.YELLOW + "Remove " + CC.GREEN + "abilities by shift clicking")
            .delay(15 * 20)
            .text(CC.GREEN + "Every ability has its own individual qualities which",
                    CC.GREEN + "can be upgraded by using ability points.")
            .delay(8 * 20)
            .text(CC.RED + "Make sure not to exceed your maximum", CC.RED + "amount of points which is 25.")
            .delay(5 * 20)
            .text(CC.GREEN + "You are also able to choose whether you would like",
                    CC.GREEN + "to activate your ability by",
                    CC.GREEN + "sneaking, blocking or clicking an customizable click item.")
            .delay(12 * 20)
            .end();

    private List<Frame> frames = new ArrayList<>();

    public Tutorial apply(Frame frame) {
        frames.add(new AbortFrame());
        frames.add(frame);
        return this;
    }

    public Tutorial delay(int delay) {
        for (int i = 0; i < delay; i++) {
            apply(new DelayFrame());
        }
        return this;
    }

    public Tutorial text(String... text) {
        return apply(new TextFrame(Arrays.asList(text)));
    }

    public Tutorial text(int distance, String... text) {
        return apply(new TextFrame(Arrays.asList(text), distance));
    }

    public Tutorial freeze() {
        return apply(new FreezeFrame(true));
    }

    public Tutorial unfreeze() {
        return apply(new FreezeFrame(false));
    }

    public Tutorial teleport(Location location) {
        return apply(new TeleportFrame(location));
    }

    public Tutorial end() {
        return apply(new EndFrame());
    }

    public void start(HubUser user) {

        for (Player player : Bukkit.getOnlinePlayers()) {
            user.getPlayer().hidePlayer(player);
        }

        user.setCurrentTutorial(this);
        user.setVanish(true);

        TaskChain<HubUser> chain = PvPDojo.newChain().syncFirst(() -> user);
        frames.forEach(frame -> frame.apply(chain));
        chain.execute();

    }

}
