/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.menus;

import org.bukkit.entity.Player;

import com.pvpdojo.Gamemode;
import com.pvpdojo.ServerType;
import com.pvpdojo.hub.lobby.cloud.CTMCloud;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class GamemodeMenu extends SingleInventoryMenu {

    public GamemodeMenu(Player player) {
        super(player, CC.BLUE + "Select Gamemode", 9);
        setUpdateInterval(20);
    }

    @Override
    public void redraw() {
        InventoryPage inv = getCurrentPage();

        setItem(1, Gamemode.FFA, CC.GRAY + "Fighting area for every playstyle, teaming forbidden.");
//        setItem(2, Gamemode.CTM, CC.GRAY + "Capture The Mushroom cow (CTM) is a gamemode that is mainly based on Capture The Flag but also " +
//                "implements certain kits that can help you capture the opponents' cows. Of course, both SoupPvP and team chemistry play an important role too!");
        setItem(3, Gamemode.HG, CC.GRAY + "Classic Hardcore Games with new original kits and concepts that build off of the features of the original gamemode.");
        setItem(5, Gamemode.CHALLENGES, CC.GRAY + "Offers a vast variety of challenges to train your souping skills. Suitable for every player, from soup beginner to veteran.");
        setItem(7, Gamemode.CLASSIC, CC.GRAY + "Fighting area with early HG equipment, teaming forbidden. " + CC.RED + "Soup only");

        inv.fillBlank();
    }

    private void setItem(int slot, Gamemode gamemode, String customDescription) {
        page.setItem(slot,
                new ItemBuilder(gamemode.getIcon())
                        .amount(Math.min(gamemode.getPlayerCount(), 64))
                        .lore("", customDescription, "", CC.GRAY + "Player Count: " + CC.WHITE + gamemode.getPlayerCount()).build(), type -> {
                    if (gamemode == Gamemode.DEATHMATCH) {
                        new QueueMenu(getPlayer()).open();
                    } else if (gamemode == Gamemode.HG) {
                        new HGMenu(getPlayer()).open();
                    } else if (gamemode == Gamemode.CTM) {
                        CTMCloud.inst().connectPlayer(getPlayer());
                    } else {
                        ServerType.valueOf(gamemode.getName().toUpperCase()).connect(getPlayer());
                        closeSafely();
                    }
                });
    }

}
