package com.pvpdojo.hub.command;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Subcommand;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.ParticleEffects;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

//Cosmetics that I want to play with.
@CommandAlias("teddyyy")
public class TeddyyyCommand extends DojoCommand {

    private static final String META = "flexer";

    @Subcommand("start")
    @CatchUnknown
    @Default
    public void onCosmeticEffect(Player player) {

        if (player.hasMetadata(META)) {
            return;
        }

        player.sendMessage(PvPDojo.PREFIX + "Starting...");
        BukkitTask run = new BukkitRunnable() {

            int loop = 0;
            float increasement = 0.1f;
            float r = 1.75f;

            @Override
            public void run() {

                if (!player.isOnline()) {
                    cancel();
                    return;
                }

                Location loc = player.getLocation().add(0, 0.75, 0);

                loop += 2.0;
                double x = Math.cos(loop * -increasement) * r;
                double z = Math.sin(loop * -increasement) * r;

                for (int i = 0; i < 5; i++) {
                    loc.add(x, x, z);
                    ParticleEffects.SPELL.sendToLocation(loc, 0, 0, 0, 0, 1);
                    loc.subtract(x, x, z);
                }

                for (int i = 0; i < 20; i++) {
                    loc.add(-x, x, z);
                    loc.getWorld().playEffect(loc, Effect.TILE_BREAK, 3);
                    loc.subtract(-x, x, z);
                }

                loc.add(x, 0, z);
                ParticleEffects.DRIP_WATER.sendToLocation(loc, 0, 0, 0, 0, 1);
                loc.subtract(x, 0, z);

                loc.add(x, z, 0);
                ParticleEffects.FIRE.sendToLocation(loc, 0, 0, 0, 0, 1);
                loc.subtract(x, z, 0);
            }

            @Override
            public synchronized void cancel() throws IllegalStateException {
                super.cancel();
                player.removeMetadata(META, PvPDojo.get());
            }
        }.runTaskTimer(PvPDojo.get(), 0, 1);
        player.setMetadata(META, new FixedMetadataValue(PvPDojo.get(), run));
    }

    @Subcommand("cancel")
    public void onCancel(Player player) {

        if (player.hasMetadata(META)) {
            BukkitTask task = (BukkitTask) player.getMetadata(META).get(0).value();
            task.cancel();
            player.sendMessage(PvPDojo.PREFIX + "Cancelled.");
        }
    }

    @Subcommand("cancelall")
    public void onCancelAll() {

        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.hasMetadata(META)) {
                BukkitTask task = (BukkitTask) player.getMetadata(META).get(0).value();
                task.cancel();
            }
        }

    }

    @Subcommand("colorize")
    public void onColorize(Player player) {}

    @Override
    public Rank getRank() {
        return Rank.DEVELOPER;
    }
}
