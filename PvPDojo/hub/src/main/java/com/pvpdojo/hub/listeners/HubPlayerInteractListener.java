/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.BotInteractEvent;
import com.pvpdojo.bukkit.events.DojoEvent;
import com.pvpdojo.bukkit.events.DojoPlayerClickBlockEvent;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.gui.InputGUI;
import com.pvpdojo.hub.Hub;
import com.pvpdojo.hub.market.Market;
import com.pvpdojo.hub.market.MarketCache;
import com.pvpdojo.hub.menus.KitMenu.KitCustomizeMenu;
import com.pvpdojo.hub.menus.MarketMenu;
import com.pvpdojo.hub.menus.SalesmanMenu;
import com.pvpdojo.hub.menus.TradeUpMenu;
import com.pvpdojo.hub.queue.PokerQueueItem;
import com.pvpdojo.hub.queue.QueueHandler;
import com.pvpdojo.hub.tutorial.Tutorial;
import com.pvpdojo.hub.userdata.HubUser;
import com.pvpdojo.menus.AbilitiesMenu;
import com.pvpdojo.menus.KitSelectorMenu;
import com.pvpdojo.util.MathUtil;
import com.pvpdojo.util.bukkit.CC;

public class HubPlayerInteractListener implements Listener {

    public static void register() {

        DojoEvent.register(new DojoPlayerClickBlockEvent() {

            @Override
            public void execute() {
                Player player = this.player;
                HubUser user = HubUser.getUser(player);
                boolean market = user.getApplicableRegions().stream().findAny()
                                     .filter(region -> region.getId().startsWith("market")).isPresent();

                if (market) {
                    if (block.getType() == Material.ENDER_CHEST) {
                        this.setCancelled(true);
                        if (user.getPersistentData().countAbilities() == 0) {
                            player.sendMessage(PvPDojo.PREFIX + CC.RED
                                    + "Seems like you have no abilities. Open some chests first!");
                            return;
                        }
                        AbilitiesMenu.getAbilitiesMenu(player, ability -> {
                            new InputGUI(CC.RED + "Insert your price for "
                                    + AbilityLoader.getAbilityData(ability.getId()).getRarity().getColor()
                                    + AbilityLoader.getAbilityData(ability.getId()).getName(), input -> {
                                if (MathUtil.isInteger(input)) {
                                    int price = Integer.parseInt(input);
                                    if (price < 1 || price > 10000000) {
                                        user.sendMessage(CC.RED + "The price must be between 1 and 10 million");
                                        return;
                                    }
                                    MarketCache add = new MarketCache(player.getUniqueId(), price,
                                            System.currentTimeMillis(), ability.getAbilityUUID(),
                                            ability.getId());
                                    Market.get().transerItemToMarket(user, add);
                                } else {
                                    player.sendMessage(PvPDojo.WARNING + "Input must be a number");
                                }
                            }).open(player);
                        }).open();
                    } else if (block.getType() == Material.PISTON_BASE) {
                        new MarketMenu(player, null).open();
                    } else if (block.getType() == Material.WORKBENCH) {
                        new TradeUpMenu(player).open();
                        setCancelled(true);
                    }
                }
                if (block.getType() == Material.ANVIL) {
                    if (!QueueHandler.inst().isQueued(player.getUniqueId(),
                            new PokerQueueItem(player.getUniqueId(), 0))) {
                        Hub.applyHubItems(user.getPlayer());
                        user.setCurrentlyEditing(null);
                        new KitSelectorMenu(player, true, kit -> new KitCustomizeMenu(user, kit).open()).open();
                    } else {
                        player.sendMessage(PvPDojo.WARNING + "You cannot edit kits while in poker queue");
                    }
                    setCancelled(true);
                }
            }
        });
        Bukkit.getPluginManager().registerEvents(new HubPlayerInteractListener(), PvPDojo.get());
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        e.setCancelled(true);
        if (e.getCause() == DamageCause.VOID && e.getEntity() instanceof Player) {
            HubJoinListener.spawnPlayer((Player) e.getEntity());
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (e.getItem() != null && e.getItem().getType() == Material.ENDER_PEARL) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBotInteract(BotInteractEvent e) {
        if (e.getHuman().getHuman().getProfile().getName().equals("Steve")) {
            e.getPlayer().sendMessage(CC.GRAY + "Join " + CC.RED + "pvpdojo.com/discord " + CC.GRAY + "for " + CC.GREEN
                    + "staff application" + CC.GRAY + " details");
        } else if (e.getHuman().getHuman().getProfile().getName().equals("Salesman")) {
            new SalesmanMenu(e.getPlayer()).open();
        } else if (e.getHuman().getCustomName().equals("Gabik21_tutorial")) {
            Tutorial.MARKET_TUTORIAL.start(HubUser.getUser(e.getPlayer()));
        } else if (e.getHuman().getCustomName().equals("schmockyyy_tutorial")) {
            Tutorial.KIT_CREATOR_TUTORIAL.start(HubUser.getUser(e.getPlayer()));
        }
    }

}
