/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub;

import com.pvpdojo.hub.command.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.pvpdojo.Discord;
import com.pvpdojo.PaymentProcessor;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.UserList;
import com.pvpdojo.abilities.Abilities;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.command.lib.DojoCommandManager;
import com.pvpdojo.extension.DojoServer;
import com.pvpdojo.hub.listeners.HubInventoryClickListener;
import com.pvpdojo.hub.listeners.HubJoinListener;
import com.pvpdojo.hub.listeners.HubPlayerClickEntityListener;
import com.pvpdojo.hub.listeners.HubPlayerDropListener;
import com.pvpdojo.hub.listeners.HubPlayerInteractListener;
import com.pvpdojo.hub.listeners.HubRegionChangeListener;
import com.pvpdojo.hub.lobby.LobbyManager;
import com.pvpdojo.hub.lobby.cloud.CTMCloud;
import com.pvpdojo.hub.market.Market;
import com.pvpdojo.hub.menus.HubHotbarGUI;
import com.pvpdojo.hub.queue.QueueHandler;
import com.pvpdojo.hub.userdata.HubUser;
import com.pvpdojo.map.DojoMap;
import com.pvpdojo.map.MapLoader;
import com.pvpdojo.map.MapType;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.userdata.KitData;
import com.pvpdojo.util.Log;

import co.aikar.commands.ConditionFailedException;
import org.bukkit.inventory.ItemStack;

public class Hub extends DojoServer<HubUser> {

    private static Hub instance;
    private boolean loaded;

    public static Hub get() {
        return instance;
    }

    private Thread redisQueueThread;
    private Thread redisLobbyThread;

    @Override
    public void onLoad() {
        deleteFolder("world");
        if (!PvPDojo.get().isTest()) {
            PvPDojo.schedule(() -> Discord.getJda().addEventListener(new AppealHandler())).createAsyncTask();
            PvPDojo.schedule(() -> Discord.getJda().addEventListener(new BastiSubHandler())).createAsyncTask();
        }
        super.onLoad();
    }

    @Override
    public ServerType getServerType() {
        return loaded ? ServerType.HUB : null;
    }

    public void start() {
        instance = this;
        loaded = true;
        PvPDojo.SERVER_TYPE = ServerType.HUB;

        if (!PvPDojo.get().isTest()) {
            PaymentProcessor.proccessPayments();
        }

        Bukkit.setSaveBandwidth(true);
        loadListeners();

        this.redisQueueThread = new Thread(() -> Redis.getResource().subscribe(QueueHandler.inst(), Redis.get().getCh(BungeeSync.SESSION)));
        redisQueueThread.setName("Redis-QueueHandler-Thread");
        redisQueueThread.start();

        this.redisLobbyThread = new Thread(() -> Redis.getResource().subscribe(LobbyManager.inst(), Redis.get().getCh(BungeeSync.UNREGISTER_SERVER),
                Redis.get().getCh(BungeeSync.REGISTER_SERVER)));
        redisLobbyThread.setName("Redis-LobbyHandler-Thread");
        redisLobbyThread.start();

        if (getConfig().getBoolean(CTMCloud.class.getSimpleName())) {
            CTMCloud.inst().start();
        }
    }

    @Override
    public void postLoad() {
        Abilities.get().start();
        registerCommands();
        Market.get().initMarket();
        EloLeaderBoard.inst().setLocation(getSoloLeaderBoardHolo());
        EloLeaderBoard.inst().track();
        HGLeaderBoard.inst().setLocation(getHGLeaderBoardHolo());
        HGLeaderBoard.inst().track();

    }

    @Override
    public void onDisable() {
        super.onDisable();

        CTMCloud.inst().shutdown();
        QueueHandler.inst().unsubscribe();
        LobbyManager.inst().shutdown();

        try {
            redisQueueThread.join();
            redisLobbyThread.join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            Log.exception(e);
        }

    }

    @Override
    public DojoMap setupMap() {

        DojoMap map = MapLoader.getMap(MapType.DEFAULT, "hub");
        PvPDojo.get().setStandardMap(map);
        if (map == null) {
            return null;
        }
        map.setWorld(Bukkit.getWorlds().get(0));
        map.shift(-map.getLocations()[0].getBlockX(), 0, -map.getLocations()[0].getBlockZ()); // Move spawn to 0,0
        map.setup(0);
        map.placeBlocks(false);
        return map;
    }

    @Override
    public UserList<HubUser> createUserList() {
        return new UserList<>(HubUser::new);
    }

    public Location getSpawn() {
        return PvPDojo.get().getStandardMap().getLocations()[0];
    }

    public Location getMarket() {
        return PvPDojo.get().getStandardMap().getLocations()[1];
    }

    public Location getKitcreation() {
        return PvPDojo.get().getStandardMap().getLocations()[2];
    }

    public Location getStaff() {
        return PvPDojo.get().getStandardMap().getLocations()[3];
    }

    public Location getLeaderboard() {
        return PvPDojo.get().getStandardMap().getLocations()[4];
    }

    public Location getSoloLeaderBoardHolo() {
        return PvPDojo.get().getStandardMap().getLocations()[5];
    }

    public Location getHGLeaderBoardHolo() {
        return PvPDojo.get().getStandardMap().getLocations()[6];
    }

    public static void applyHubItems(Player player) {
        HubUser user = HubUser.getUser(player);
        AbilityPlayer.get(player).die();
        user.setCurrentlyEditing(null);
        user.setHotbarGui(HubHotbarGUI.get(player));
    }

    private void loadListeners() {
        new HubJoinListener().register();
        new HubInventoryClickListener().register();
        HubPlayerInteractListener.register();
        new HubPlayerClickEntityListener().register();
        HubRegionChangeListener.register();
        HubPlayerDropListener.register();
    }

    private void registerCommands() {
        DojoCommandManager.get().getCommandContexts().registerIssuerOnlyContext(HubUser.class, c -> HubUser.getUser(c.getPlayer()));
        DojoCommandManager.get().getCommandContexts().registerIssuerAwareContext(KitData.class, c -> HubUser.getUser(c.getPlayer()).getCurrentlyEditing());
        DojoCommandManager.get().getCommandConditions().addCondition(HubUser.class, "kitsave", (c, ec, user) -> {
            if (user.getCurrentlyEditing() == null) {
                throw new ConditionFailedException("You are not editing a kit");
            }
        });
        DojoCommandManager.get().registerCommand(new ChestCommand());
        DojoCommandManager.get().registerCommand(new SpawnCommand());
        DojoCommandManager.get().registerCommand(new DuelCommand());
        DojoCommandManager.get().registerCommand(new SaveCommand());
        DojoCommandManager.get().registerCommand(new ClanFightCommand());
        DojoCommandManager.get().registerCommand(new TestCommand());
        DojoCommandManager.get().registerCommand(new DiscordCommand());
        DojoCommandManager.get().registerCommand(new CloudCommand());
        DojoCommandManager.get().registerCommand(new CTMDuelCommand());
        DojoCommandManager.get().registerCommand(new LobbyCommand());
        DojoCommandManager.get().registerCommand(new TeddyyyCommand());
    }

}
