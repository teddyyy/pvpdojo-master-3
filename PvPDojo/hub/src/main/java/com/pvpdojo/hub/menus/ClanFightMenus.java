/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.menus;

import static com.pvpdojo.util.StringUtils.getEnumName;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.pvpdojo.hub.queue.ClanQueueItem;
import com.pvpdojo.hub.queue.QueueHandler;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.session.network.component.ClanComponent.ClanGame;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.impl.DojoOfflinePlayerImpl;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.SkullBuilder;

public class ClanFightMenus {

    public static class ChooseLineupMenu extends SingleInventoryMenu {

        private Set<DojoOfflinePlayer> chosen = new HashSet<>();
        private Set<DojoOfflinePlayer> available;

        public ChooseLineupMenu(Player player, Set<DojoOfflinePlayer> available) {
            super(player, "Select lineup", 9);
            this.available = available;
            this.chosen.add(new DojoOfflinePlayerImpl(User.getUser(getPlayer())));
        }

        @Override
        public void redraw() {
            super.redraw();

            InventoryPage inv = getCurrentPage();
            inv.setSizeForNeededSlots(available.size() + 1);

            int i = 0;
            for (DojoOfflinePlayer member : available) {
                if (!member.equals(getPlayer().getUniqueId())) {
                    inv.setItem(i, new SkullBuilder(member.getName()).name(CC.BLUE + member.getName()).glow(chosen.contains(member)).build(), type -> {
                        chosen.add(member);
                        redrawAndOpen();
                    });
                    i++;
                }
            }

            inv.setItem(inv.getSize() - 1, new ItemBuilder(Material.EMERALD).name(CC.GREEN + "Finish").build(), type -> {
                ClanQueueItem queueItem = new ClanQueueItem(User.getUser(getPlayer()).getClan(), chosen);
                new ChooseModesMenu(getPlayer(), queueItem).open();
            });

            inv.fillBlank();
        }
    }

    public static class ChooseModesMenu extends SingleInventoryMenu {

        private Set<ClanGame> chosen = new HashSet<>();
        private ClanQueueItem queueItem;

        public ChooseModesMenu(Player player, ClanQueueItem queueItem) {
            super(player, "Select preferred game modes", 9);
            this.queueItem = queueItem;
        }

        @Override
        public void redraw() {
            super.redraw();

            InventoryPage inv = getCurrentPage();

            int i = 0;
            for (ClanGame game : ClanGame.values()) {
                if (!chosen.contains(game)) {
                    inv.setItem(i++, new ItemBuilder(Material.STONE).name(getEnumName(game)).build(), type -> {
                        chosen.add(game);
                        queueItem.applyGameMode(game);
                        if (queueItem.getDesiredGameModes().size() >= 2) {
                            QueueHandler.inst().queueClan(queueItem);
                            closeSafely();
                        } else {
                            redrawAndOpen();
                        }
                    });
                }
            }

            inv.fillBlank();
        }
    }

}
