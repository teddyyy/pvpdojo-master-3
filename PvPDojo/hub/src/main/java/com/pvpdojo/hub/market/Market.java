/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.market;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.achievement.Achievement;
import com.pvpdojo.achievement.AchievementManager;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.mysql.SQL;
import com.pvpdojo.mysql.SQLBuilder;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.userdata.AbilityData;
import com.pvpdojo.userdata.KitData;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.Log;

public class Market {

    private static final Market instance = new Market();
    private final List<MarketCache> cache = new CopyOnWriteArrayList<>();

    public static Market get() {
        return instance;
    }

    public void initMarket() {
        try (SQLBuilder builder = new SQLBuilder()) {
            builder.setStatement(
                    "SELECT market.price, market.date, market.owner, abilities.ability_id, abilities.ability_uuid, abilities.ability_grade FROM abilities, market WHERE market.ability_uuid = abilities.ability_uuid;");
            builder.executeQuery();
            cache.clear();
            while (builder.next()) {
                UUID original_owner = UUID.fromString(builder.getString("market.owner"));
                int price = builder.getInteger("market.price");
                long date = builder.getResultSet().getTimestamp("market.date").getTime();

                UUID ability_uuid = UUID.fromString(builder.getString("abilities.ability_uuid"));
                int ability_id = builder.getInteger("abilities.ability_id");
                int grade = builder.getInteger("abilities.ability_grade");

                MarketCache marketCache = new MarketCache(original_owner, price, date, ability_uuid, ability_id);
                cache.add(marketCache);
            }
        } catch (SQLException e) {
            Log.exception("Loading Market", e);
        }

        PvPDojo.schedule(() -> cache.forEach(MarketCache::updateOwnerName)).createTimerAsync(2 * 60 * 20, -1);
    }

    public List<MarketCache> getMarketCache() {
        return cache;
    }

    public void transerItemToMarket(User user, MarketCache add) {

        PvPDojo.newChain().asyncFirst(() -> {
            try {
                AbilityData abilityData = user.getPersistentData().readAbility(add.getAbilityUUID());

                SQL.insert(Tables.MARKET, "'" + abilityData.getAbilityUUID() + "', 0, '" + user.getPlayer().getUniqueId().toString() + "', " + add.getPrice() + ", NOW()");
                add.updateOwnerName();
                PersistentData.updateAbilityOwnership(PvPDojo.SERVER_UUID, abilityData.getAbilityUUID(), add.getPrice() + "$");
                user.getPersistentData().readAbilities(null);

                return abilityData;
            } catch (SQLException e) {
                Log.exception(e);
                user.getPlayer().sendMessage(CC.RED + "An error occurred while transferring to market");
            }
            return null;
        }).abortIfNull().syncLast(input -> {
            cache.add(add);
            AbilityLoader loader = AbilityLoader.getAbilityData(input.getId());

            boolean removedFromKit = false;
            if (loader == null) {
                for (KitData kit : new ArrayList<>(user.getPersistentData().getKits())) {
                    if (kit.containsAbility(add.getAbilityId())) {
                        removedFromKit = true;
                        kit.removeAbility(add.getAbilityId());
                        user.getPersistentData().updateKit(kit);
                    }
                }
            }

            if (removedFromKit) {
                user.sendMessage(PvPDojo.PREFIX + "At least one of your kits have been modified due to selling an ability used in them");
            }
            user.sendMessage(PvPDojo.PREFIX + "Successfully added " + loader.getRarity().getColor() + loader.getName() + " Ability" + CC.GRAY + " for " + CC.RED
                    + add.getPrice() + CC.GRAY + " to the market");
            AchievementManager.inst().trigger(Achievement.SALESMAN, user.getPlayer());
        }).execute();
    }

    public void transerItemFromMarket(final MarketCache cache, final User buyer) {
        if (!this.cache.remove(cache)) {
            buyer.sendMessage(PvPDojo.WARNING + "Item not found");
            return;
        }

        PvPDojo.schedule(() -> {
            int price = cache.getPrice();
            if (buyer.getPlayer().getUniqueId().equals(cache.getOriginalOwner())) {
                price = 0;
            }

            try {
                SQL.remove(Tables.MARKET, "ability_uuid = ?", cache.getAbilityUUID().toString());
            } catch (SQLException e) {
                Log.exception("Removing from market", e);
                buyer.sendMessage(MessageKeys.WARNING, "{text}", "An error occurred in this transaction, please report this");
                return;
            }

            buyer.getPersistentData().incrementMoney(-price);
            new PersistentData(cache.getOriginalOwner()).incrementMoney(price);
            PersistentData.updateAbilityOwnership(buyer.getUUID(), cache.getAbilityUUID(), cache.getPrice() + "$");

            buyer.getPersistentData().loadData(true);


            AbilityLoader loader = AbilityLoader.getAbilityData(cache.getAbilityId());
            buyer.getPlayer().sendMessage(CC.GRAY + "Successfully purchased " + loader.getRarity().getColor() + loader.getName() + " Ability" + CC.GRAY + " for " + CC.RED + cache.getPrice());

            PvPDojo.schedule(() -> AchievementManager.inst().trigger(Achievement.CUSTOMER, buyer.getPlayer())).sync();

            User user = User.getUser(cache.getOriginalOwner());
            String msg = PvPDojo.PREFIX + "Your market offer for the " + loader.getRarity().getColor() + loader.getName() + " Ability " + CC.GRAY
                    + "was bought and you received " + CC.GREEN + cache.getPrice() + CC.GRAY + " dojo credits";
            if (user == null || user.isLoggedOut()) {
                DBCommon.sendOfflineMessage(cache.getOriginalOwner(), msg);
                return;
            }
            user.getPersistentData().readMoney();
            user.sendMessage(msg);
        }).createAsyncTask();
    }

}
