/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.tutorial;

import com.pvpdojo.hub.listeners.HubJoinListener;
import com.pvpdojo.userdata.User;

public class EndFrame implements Frame {

    @Override
    public void accept(User user) {
        user.setFrozen(false);
        user.detachHologram();
        HubJoinListener.spawnPlayer(user.getPlayer());
    }

}
