/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.tutorial;

import com.pvpdojo.userdata.User;

import co.aikar.taskchain.TaskChain;

public class DelayFrame implements Frame {


    public DelayFrame() {}

    @Override
    public void apply(TaskChain chain) {
        chain.delay(1);
    }

    @Override
    public void accept(User user) {}

}
