/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.lobby.cloud;

import java.util.function.Predicate;

import com.pvpdojo.Gamemode;
import com.pvpdojo.game.ctm.settings.CTMSettings;
import com.pvpdojo.redis.ServerInfo;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.session.SessionType;

public class CTMCloud extends Cloud {

    private static final CTMCloud INST = new CTMCloud();

    public static CTMCloud inst() {
        return INST;
    }

    private CTMCloud() {
        super(CTMSettings::new, Gamemode.CTM, SessionType.C_T_M);
    }

    @Override
    public Predicate<ServerInfo> getCustomFilter() {
        return serverInfo -> serverInfo.getSessionInfo().getSessionState() != SessionState.FINISHED;
    }
}
