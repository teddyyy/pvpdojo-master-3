/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.queue;

import java.util.UUID;

import com.pvpdojo.session.SessionType;

import lombok.Getter;

public class PokerQueueItem extends QueueItem {

    @Getter
    private final int kit;

    public PokerQueueItem(UUID uuid, int kit) {
        super(uuid, null, SessionType.POKER);
        this.kit = kit;
    }

}
