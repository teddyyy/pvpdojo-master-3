/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.lobby;

import java.util.UUID;

import com.pvpdojo.Gamemode;
import com.pvpdojo.game.settings.GameSettings;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LobbyInfo<T extends GameSettings> {

    private final UUID host;
    private final Gamemode type;
    private final transient T settings;

}
