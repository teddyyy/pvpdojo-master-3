/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.pvpdojo.Discord;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.PunishmentManager;
import com.pvpdojo.util.PunishmentManager.Punishment;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class AppealHandler extends ListenerAdapter {

    private Pattern pattern = Pattern.compile("!appeal [0-9a-f]+");
    private Map<String, Long> lastAppeal = new ConcurrentHashMap<>();
    private Cache<String, Integer> currentAppeals = CacheBuilder.newBuilder().expireAfterWrite(2, TimeUnit.HOURS).build();

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (event.getAuthor().isBot()) {
            return;
        }

        if (event.isFromType(ChannelType.PRIVATE)) {

            if (event.getAuthor().getMutualGuilds().isEmpty()) {
                return;
            }

            if (lastAppeal.containsKey(event.getAuthor().getId()) && lastAppeal.get(event.getAuthor().getId()) > System.currentTimeMillis()) {
                event.getPrivateChannel().sendMessage("You have already sent an appeal").queue();
                return;
            }

            if (event.getMessage().getContentRaw().startsWith("!appeal")) {
                if (event.getMessage().getContentRaw().split(" ").length != 2) {
                    event.getPrivateChannel().sendMessage("Please use !appeal [id] before writing your appeal").queue();
                } else {
                    try {
                        int id = Integer.valueOf(event.getMessage().getContentRaw().split(" ")[1], 16);
                        try {
                            Punishment punishment = PunishmentManager.getPunishment(id);
                            if (punishment != null && punishment.isValid() && !punishment.isExpired()) {
                                currentAppeals.put(event.getAuthor().getId(), id);
                                event.getPrivateChannel().sendMessage("IGN: " + PvPDojo.getOfflinePlayer(punishment.getPunished()).getName()
                                        + "\nReason: " + punishment.getOriginalReason()
                                        + "\n\nYou may now write your appeal in English or German"
                                        + "\nNote that you CANNOT split your appeal into multiple messages").queue();
                            } else {
                                event.getPrivateChannel().sendMessage("404 - Not Found").queue();
                            }
                        } catch (SQLException e) {
                            Log.exception("Load punishment for appeal", e);
                            event.getPrivateChannel().sendMessage("An error occurred").queue();
                        }
                    } catch (NumberFormatException e) {
                        event.getPrivateChannel().sendMessage("Please use !appeal [id] before writing your appeal").queue();
                    }
                }
            } else {
                Integer id = currentAppeals.getIfPresent(event.getAuthor().getId());
                if (id != null) {
                    lastAppeal.put(event.getAuthor().getId(), System.currentTimeMillis() + 3 * 60 * 60 * 1000);

                    try {
                        Punishment punishment = PunishmentManager.getPunishment(id);
                        if (punishment != null) {
                            Discord.appeals().sendMessage(event.getAuthor().getAsMention() + " appealing"
                                    + "\nIGN: " + PvPDojo.getOfflinePlayer(punishment.getPunished()).getName()
                                    + "\nReason: " + punishment.getOriginalReason()
                                    + "\nId: " + punishment.getId()
                                    + "\n\n" + event.getMessage().getContentRaw())
                                   .queue();

                            event.getPrivateChannel().sendMessage("Your appeal has been sent. Editing your appeal has NO EFFECT.\nWe will answer your appeal via Discord").queue();
                        } else {
                            event.getPrivateChannel().sendMessage("Could not send appeal").queue();
                        }
                    } catch (SQLException e) {
                        Log.exception("Load punishment for appeal", e);
                    }
                } else {
                    event.getPrivateChannel().sendMessage("Please use '!appeal [id]' to appeal\nuse '!register [ign]' to link accounts").queue();
                }
            }

        } else if (pattern.matcher(event.getMessage().getContentRaw()).matches()) {
            event.getMessage().delete().queue();
            event.getChannel().sendMessage(new MessageBuilder().append(event.getAuthor()).append(" Please send me a private message with your code").build()).queue();
        } else if (event.getMessage().getContentRaw().contains("soup.gg") || event.getMessage().getContentRaw().contains("soupgg")) {
            event.getMessage().delete().queue();
        }

    }

}
