/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.menus;

import static com.pvpdojo.util.StringUtils.getEnumName;

import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.Gamemode;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.gui.HotbarGUI;
import com.pvpdojo.hub.queue.QueueHandler;
import com.pvpdojo.hub.queue.QueueItem;
import com.pvpdojo.menus.ProfileMenu;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.ItemEditor;
import com.pvpdojo.util.bukkit.ItemBuilder.SkullBuilder;

public class HubHotbarGUI extends HotbarGUI {

    private static final HubHotbarGUI DEFAULT = new HubHotbarGUI(false);
    private static final HubHotbarGUI LOBBY_CREATION = new HubHotbarGUI(false);

    public static HubHotbarGUI get(Player player) {
        if (User.getUser(player).getRank().inheritsRank(Rank.BLACKBELT)) {
            return LOBBY_CREATION;
        } else {
            return DEFAULT;
        }
    }

    private HubHotbarGUI(boolean lobbyCreation) {
        setItem(1, new ItemBuilder(Material.ENCHANTMENT_TABLE).name(CC.DARK_GREEN + "PvPDojo Lobby Warps").build(), onRightClick(p -> new LobbyWarpMenu(p).open()));

        setItem(lobbyCreation ? 3 : 4, new ItemStack(Material.STONE_SWORD), onRightClick(p -> new QueueMenu(p).open()));
        setItem(lobbyCreation ? 3 : 4, new ItemBuilder(Material.STONE_SWORD).name(CC.DARK_AQUA + "Deathmatch").unbreakable().build(), (player, action, entity) -> {
            if (action == Action.LEFT_CLICK_AIR && entity instanceof Player) {
                player.performCommand("duel " + ((Player) entity).getNick());
            }
        });

        PvPDojo.schedule(() -> {
            int players = Gamemode.DEATHMATCH.getPlayerCount();
            for (User user : Bukkit.getOnlinePlayers().stream().map(User::getUser).collect(Collectors.toList())) {
                if (user.getHotbarGui() == this) {
                    ItemBuilder editor = new ItemEditor(user.getPlayer().getInventory().getItem(lobbyCreation ? 3 : 4))
                            .amount(Math.max(1, players)).name(CC.DARK_AQUA + "Deathmatch" + CC.GRAY + " - " + CC.WHITE + players);
                    user.getPlayer().getInventory().setItem(lobbyCreation ? 3 : 4, editor.build());
                }
            }
        }).createTimer(0, 20, -1);

        if (lobbyCreation) {
            setItem(5, new ItemBuilder(Material.NOTE_BLOCK).name(CC.DARK_AQUA + "Lobby Creation").build(), onRightClick(player -> new LobbyCreationMenu(player).open()));
        }

        setItem(2, Material.EMERALD, p -> {
            UUID lastDuel = QueueHandler.inst().getLastDuel(p.getUniqueId());
            Player lastDuelPlayer = Bukkit.getPlayer(lastDuel);

            QueueItem lastQueue = QueueHandler.inst().getLastQueue(p.getUniqueId());

            if (lastQueue != null && lastQueue.getSessionType() == SessionType.POKER) {
                return null;
            }

            if (lastDuel != null || lastQueue != null) {
                PvPDojo.schedule(() -> {
                    if (p.isOnline()) {
                        QueueHandler.inst().popLastDuel(p.getUniqueId());
                        QueueHandler.inst().popLastQueue(p.getUniqueId());
                        p.getInventory().setItem(2, null);
                    }
                }).createTask(30 * 20);
            }

            if (lastDuel != null) {
                if (lastDuelPlayer == null) {
                    PvPDojo.newChain()
                           .asyncFirst(() -> PvPDojo.getOfflinePlayer(lastDuel).getName())
                           .syncLast(name -> p.getInventory().setItem(2, new ItemBuilder(Material.EMERALD).name(CC.GREEN + "Rematch: " + CC.BLUE + name).build()))
                           .execute();
                    return null;
                } else {
                    return new ItemBuilder(Material.EMERALD).name(CC.GREEN + "Rematch: " + CC.BLUE + lastDuelPlayer.getNick()).build();
                }
            }
            return lastQueue != null ? new ItemBuilder(Material.EMERALD).name(CC.GREEN + "Requeue: " + getEnumName(lastQueue.getSessionType(), true)).build() : null;
        }, onRightClick(player -> {
            if (QueueHandler.inst().getLastDuel(player.getUniqueId()) != null) {
                player.performCommand("duel " + QueueHandler.inst().popLastDuel(player.getUniqueId()));
                QueueHandler.inst().popLastQueue(player.getUniqueId());
            } else if (QueueHandler.inst().getLastQueue(player.getUniqueId()) != null) {
                QueueMenu.CLICK_HANDLER.accept(User.getUser(player), player.isSneaking() ? ClickType.SHIFT_LEFT : ClickType.LEFT, QueueHandler.inst().popLastQueue(player.getUniqueId()));
            }
        }));

        setItem(0, new ItemBuilder(Material.COMPASS).name(CC.GREEN + "PvPDojo Warps").build(), onRightClick(p -> new GamemodeMenu(p).open()));

        setItem(7, new ItemBuilder(Material.CHEST).name(CC.DARK_AQUA + "Open Ability Collection").build(), onRightClick(p -> new ChestMenu(p).open()));

        setItem(8, Material.SKULL_ITEM, player -> new SkullBuilder(player.getName()).name(CC.DARK_AQUA + "Your Profile").build(), onRightClick(p -> new ProfileMenu(p).open()));
    }

}
