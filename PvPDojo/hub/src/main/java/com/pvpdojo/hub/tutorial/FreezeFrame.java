/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.tutorial;

import com.pvpdojo.userdata.User;

public class FreezeFrame implements Frame {

    private boolean freeze;

    public FreezeFrame(boolean freeze) {
        this.freeze = freeze;
    }

    @Override
    public void accept(User user) {
        user.setFrozen(freeze);
    }

}
