/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.lobby;

import java.util.ArrayList;
import java.util.List;

import com.pvpdojo.settings.Settings;

import lombok.Data;

@Data
public class LobbyPresets implements Settings, Cloneable {
    private List<Lobby<?>> lobbyPresets = new ArrayList<>();
}
