/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.command;

import java.sql.SQLException;

import com.pvpdojo.DBCommon;
import com.pvpdojo.Discord;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.hub.BastiSubHandler;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("discord")
public class DiscordCommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onDiscordConnect(User user) {

        PvPDojo.schedule(() -> {
            try {
                int id = DBCommon.getUserId(user.getUUID()).orElseThrow(SQLException::new);
                String discordUUID = BastiSubHandler.REQUESTS.getIfPresent(user.getUUID());

                if (discordUUID != null && Discord.get().canRegister(id, discordUUID)) {
                    BastiSubHandler.REQUESTS.invalidate(user.getUUID());
                    Discord.get().register(id, discordUUID);
                    user.sendMessage(MessageKeys.DISCORD_REGISTER_SUCCESS);
                } else {
                    user.sendMessage(MessageKeys.DISCORD_ALREADY_REGISTERED);
                }
            } catch (SQLException e) {
                Log.exception(e);
                user.sendMessage(CC.RED + "ERROR");
            }
        }).createAsyncTask();
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
