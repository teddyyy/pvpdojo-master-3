/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.command;

import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.pvpdojo.Gamemode;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.game.GameProperty;
import com.pvpdojo.game.ctm.settings.CTMSettings;
import com.pvpdojo.hub.lobby.Lobby;
import com.pvpdojo.hub.lobby.LobbyManager;
import com.pvpdojo.hub.lobby.exception.AlreadyHostingException;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.Request;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Flags;

@CommandAlias("ctmduel")
public class CTMDuelCommand extends DojoCommand {

    public static final int LEAST_PLAYERS = 2;

    @CommandCompletion("@players")
    @CatchUnknown
    public void onCTMDuel(User user, @Flags("other") Player target) {
        User targetUser = User.getUser(target);

        if (targetUser.getParty() == null || user.getParty() == null) {
            user.sendMessage(PvPDojo.WARNING + "Both players must be in party");
            return;
        }

        if (!user.getParty().getLeader().equals(user.getUUID())) {
            user.sendMessage(MessageKeys.PARTY_LEADER_REQUIRED);
            return;
        }

        if (!targetUser.getParty().getLeader().equals(targetUser.getUUID())) {
            PvPDojo.schedule(() -> user.sendMessage(PvPDojo.WARNING + "Please duel the party leader: "
                    + CC.BLUE + PvPDojo.getOfflinePlayer(targetUser.getParty().getLeader()).getName())).createAsyncTask();
            return;
        }

        if (targetUser.getParty().size() < LEAST_PLAYERS && user.getParty().size() < LEAST_PLAYERS) {
            user.sendMessage(PvPDojo.WARNING + "Both parties need at least 4 players");
            return;
        }

        String failReason = targetUser.makeRequest(user.getUUID(), new CTMDuelRequest(user));
        if (failReason == null) {
            user.sendOverlay(MessageKeys.INVITE_DUEL, "{target}", targetUser.getName());
            targetUser.sendMessage(StringUtils.createComponent(PvPDojo.LANG.formatMessage(targetUser, MessageKeys.INVITED_DUEL, "{inviter}", user.getName()),
                    CC.GREEN + "accept", "accept " + user.getUUID().toString()));
        } else {
            user.sendOverlay(MessageKeys.WARNING, "{text}", failReason);
        }

    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

    private class CTMDuelRequest extends Request {

        private final User requester;

        private CTMDuelRequest(User requester) {
            this.requester = requester;
        }

        @Override
        public void accept(User acceptor) {

            if (requester.isLoggedOut()) {
                acceptor.sendMessage(MessageKeys.COULD_NOT_FIND_PLAYER);
                return;
            }

            if (acceptor.getParty() == null || requester.getParty() == null) {
                acceptor.sendMessage(PvPDojo.WARNING + "Both players must be in party");
                return;
            }

            if (acceptor.getParty().size() < LEAST_PLAYERS && requester.getParty().size() < LEAST_PLAYERS) {
                acceptor.sendMessage(PvPDojo.WARNING + "Both parties need at least 4 players");
                return;
            }

            try {
                Lobby lobby = LobbyManager.inst().createLobby(Gamemode.CTM, new CTMSettings());
                lobby.setPublicLobby(false);
                lobby.getProperties().setProperty(GameProperty.TEAM_RED, requester.getParty().getUsers().stream().map(UUID::toString).collect(Collectors.joining("|")));
                lobby.getProperties().setProperty(GameProperty.TEAM_BLUE, acceptor.getParty().getUsers().stream().map(UUID::toString).collect(Collectors.joining("|")));

                lobby.start(Bukkit.getConsoleSender());
            } catch (AlreadyHostingException e) {
                Log.exception(e);
            }
        }

        @Override
        public void deny(User deniedBy) {}

        @Override
        public int getExpirationSeconds() {
            return 120;
        }
    }

}
