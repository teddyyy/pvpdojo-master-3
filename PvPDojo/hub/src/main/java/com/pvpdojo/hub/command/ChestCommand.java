/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.command;

import org.bukkit.entity.Player;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.hub.menus.ChestMenu;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("chest")
public class ChestCommand extends DojoCommand {

    @Default
    public void onCommand(Player player) {
        new ChestMenu(player).open();
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
