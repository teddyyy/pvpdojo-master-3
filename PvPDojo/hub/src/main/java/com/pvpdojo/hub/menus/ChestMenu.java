/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.menus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilityinfo.AbilityCollection;
import com.pvpdojo.abilityinfo.AbilityRarity;
import com.pvpdojo.abilityinfo.RandomAbility;
import com.pvpdojo.bukkit.util.GuiUtil;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.menus.AchievementMenu;
import com.pvpdojo.menus.ChallengeMenu;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.userdata.AbilityData;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.BroadcastUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.Log;

public class ChestMenu extends SingleInventoryMenu {

    public ChestMenu(Player player) {
        super(player, CC.DARK_AQUA + "Select Collection", 27);

        PersistentData data = User.getUser(player).getPersistentData();

        setDefaultActions(inv -> {
            //inv.setItem(0, new ItemBuilder(Material.BOOK_AND_QUILL).name(CC.GREEN + "Challenges").build(), type -> ChallengeMenu.getChallengeMenu(getPlayer()).open());
            inv.setItem(3, new ItemBuilder(Material.DIAMOND_BLOCK).name(CC.AQUA + "Achievements").build(), type -> new AchievementMenu(getPlayer()).open());

            boolean canAfford = data.getAchievementPoints() >= AchievementMenu.CHEST_COST;
            CC color = canAfford ? CC.GREEN : CC.RED;
            String canAffordMessage = canAfford ? "You can afford this" : "You cannot afford this";
            inv.setItem(5, new ItemBuilder(Material.CHEST)
                            .name(color + "Chest")
                            .lore(CC.GRAY + "Chests are used by users to unlock abilities. Click here to purchase.", "",
                                    CC.GRAY + "Cost: " + color + AchievementMenu.CHEST_COST + " Points", "",
                                    color + "[    " + canAffordMessage + "     ]")
                            .build(),
                    type -> {
                        if (canAfford) {
                            data.incrementAchievementPoints(-AchievementMenu.CHEST_COST);
                            ChallengeMenu.rewardCollections(User.getUser(player), 1);
                            redrawAndOpen();
                        }
                    });

            inv.setItem(8, new ItemBuilder(Material.SIGN).name(CC.DARK_AQUA + "Ways to get chests").lore("", CC.GRAY + "shop.pvpdojo.com").build(), null);
        });
    }

    @Override
    public void redraw() {
        User user = User.getUser(getPlayer());
        InventoryPage inv = getCurrentPage();

        inv.setHasBar(true);

        Set<Integer> containsCollection = new HashSet<>();
        for (AbilityData data : user.getPersistentData().getAbilities()) {
            if (data.getId() < 0) {
                containsCollection.add(data.getId());
            }
        }
        int i = 9;
        for (int x : containsCollection) {
            i++;
            if (i == 17) {
                i = 19;
            }
            AbilityCollection collection = AbilityCollection.getCollectionById(x);
            inv.setItem(i, getItemFromChest(collection, user.getPersistentData().getAbilities()), clickType -> new ChestUnlockMenu(user.getPlayer(), collection).open());
        }
        if (containsCollection.isEmpty()) {
            user.sendMessage(CC.RED + "You dont have any collections to open");
        }
        inv.fillBlank();
    }

    private static class ChestUnlockMenu extends SingleInventoryMenu {

        private AbilityCollection collection;

        public ChestUnlockMenu(Player player, AbilityCollection collection) {
            super(player, CC.DARK_AQUA + "Opening Collection", 27);
            this.collection = collection;
            setCloseable(false);
        }

        @Override
        public void redraw() {
            InventoryPage inv = getCurrentPage();
            User user = User.getUser(getPlayer());

            user.getPersistentData().hasAbility(collection.getId(), (hasCollection, thrown) -> {
                if (thrown != null) {
                    user.getPlayer().sendMessage(CC.RED + "Failed to open collection, try again");
                    return;
                }
                if (hasCollection) {
                    AbilityRarity rarity = RandomAbility.getRandomRarity(true);
                    int abilityId = RandomAbility.getAbilityFromRarity(rarity, collection);

                    UUID abilityUUID = null;
                    for (AbilityData data : user.getPersistentData().getAbilities()) {
                        if (data.getId() == collection.getId())
                            abilityUUID = data.getAbilityUUID();
                    }
                    if (abilityUUID == null) {
                        user.getPlayer().sendMessage(CC.RED + "failed to open collection, try again");
                        throw new IllegalStateException("unable to add ability for player " + user.getPlayer().getName());
                    }
                    user.getPersistentData().updateAbility(abilityUUID, abilityId);

                    PvPDojo.schedule(() -> {
                        ItemStack blankRed = new ItemBuilder(GuiUtil.getBlankItem()).durability(DyeColor.RED.getWoolData()).build();
                        inv.setItem(4, blankRed, null);
                        inv.setItem(22, blankRed, null);
                        startAnimation(user, inv, collection, AbilityLoader.getAbilityData(abilityId));
                    }).sync();

                }
            });

            inv.fillBlank();
        }

        int iterations = 0;
        int skip = 0;
        int runnable;

        private void startAnimation(User user, InventoryPage page, AbilityCollection collection, AbilityLoader reward) {
            final int[] map = new int[100];
            final int endSlot = getRandomSlotCount();
            final Player player = user.getPlayer();
            for (int i = 0; i < 100; i++) {
                AbilityRarity color = RandomAbility.getRandomRarity(false);
                int abilityId = RandomAbility.getAbilityFromRarity(color, collection);
                AbilityLoader loader = AbilityLoader.getAbilityData(abilityId);
                map[i] = abilityId;

                if (i < 7) {
                    page.setItem(10 + i, new ItemBuilder(loader.getAbilityIcon()).lore(loader.fixAbilityDescription(user, null)).build(), null);
                }
            }
            map[endSlot] = reward.getId();

            runnable = PvPDojo.schedule(() -> {

                if (skip > 0) {
                    skip--;
                    return;
                }

                if (iterations > 30) {
                    skip = (iterations - 30) / 3;
                }

                iterations++;

                if ((iterations + 2) == endSlot) {
                    user.sendMessage(CC.GRAY + "Congratulations, you unlocked " + reward.getRarity().getColor() + reward.getName() + " Ability");
                    Log.info(reward.getRarity().name());
                    if (reward.getRarity() == AbilityRarity.GOLD || reward.getRarity() == AbilityRarity.LEGENDARY) {
                        Log.info("Broadcast");
                        BroadcastUtil.global(ChatChannel.GLOBAL, CC.GRAY + player.getName() + " unlocked " + reward.getRarity().getColor() + reward.getName() + " Ability");
                    }

                    Bukkit.getScheduler().cancelTask(runnable);

                    PvPDojo.schedule(() -> {
                        page.getMenu().setCloseable(true);
                        player.closeInventory();
                    }).createTask(30);
                    return;
                }
                player.playSound(player.getLocation(), Sound.CLICK, .3f, 1);
                for (int i = 0; i < 7; i++) {
                    int abilityId = map[i + iterations];
                    AbilityLoader loader = AbilityLoader.getAbilityData(abilityId);
                    page.setItem(10 + i, new ItemBuilder(loader.getAbilityIcon()).lore(loader.fixAbilityDescription(user, null)).build(), null);
                }
                open();
            }).createTimer(1, -1);
        }
    }

    private static int getRandomSlotCount() {
        return PvPDojo.RANDOM.nextInt(15) + 40;
    }

    public static ItemStack getItemFromChest(AbilityCollection collection, Collection<AbilityData> data) {
        int collection_id = collection.getId();
        ArrayList<String> lore = new ArrayList<>();
        for (AbilityLoader ability : AbilityCollection.getAbilitiesInCollection(collection)) {
            lore.add(ability.getRarity().getColor() + ability.getName() + " Ability");
        }
        int amount = 0;
        for (AbilityData ad : data) {
            if (ad.getId() == collection_id) {
                amount++;
            }
        }
        return new ItemBuilder(collection.getItem()).amount(amount).lore(lore).build();
    }
}
