/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.market;

import java.util.UUID;

import com.pvpdojo.PvPDojo;

public class MarketCache {

    private UUID ownerUUID;
    private String ownerName;
    private int price;
    private long date;

    private UUID ability_uuid;
    private int ability_id;

    public MarketCache(UUID original_owner, int price, long date, UUID ability_uuid, int ability_id) {
        this.ownerUUID = original_owner;
        this.price = price;
        this.date = date;
        this.ability_uuid = ability_uuid;
        this.ability_id = ability_id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public UUID getOriginalOwner() {
        return ownerUUID;
    }

    public long getDate() {
        return date;
    }

    public UUID getAbilityUUID() {
        return ability_uuid;
    }

    public int getAbilityId() {
        return ability_id;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public void updateOwnerName() {
        ownerName = PvPDojo.getOfflinePlayer(ownerUUID).getName();
    }
}
