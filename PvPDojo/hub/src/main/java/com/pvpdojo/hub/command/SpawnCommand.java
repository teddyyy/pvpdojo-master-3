/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.command;

import org.bukkit.entity.Player;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.hub.listeners.HubJoinListener;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("spawn")
public class SpawnCommand extends DojoCommand {

    @Default
    public void onSpawn(Player player) {
        HubJoinListener.spawnPlayer(player);
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
