/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.lobby;

import org.bukkit.Material;

import com.pvpdojo.gui.HotbarGUI;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class LobbyHotbarGUI extends HotbarGUI {

    private static final LobbyHotbarGUI INST = new LobbyHotbarGUI();

    public static LobbyHotbarGUI get() {
        return INST;
    }

    private LobbyHotbarGUI() {
        setItem(8, new ItemBuilder(Material.REDSTONE).name(CC.RED + "Leave Lobby").build(), onRightClick(player -> player.performCommand("lobby leave")));
    }

}
