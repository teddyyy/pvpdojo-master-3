/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.lobby.cloud;

import static com.pvpdojo.redis.BungeeSync.REQUEST;
import static com.pvpdojo.redis.BungeeSync.RESPONSE;

import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.pvpdojo.Gamemode;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.cloud.CloudUtil;
import com.pvpdojo.game.settings.GameSettings;
import com.pvpdojo.hub.lobby.Lobby;
import com.pvpdojo.hub.lobby.LobbyManager;
import com.pvpdojo.hub.lobby.exception.AlreadyHostingException;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.redis.ServerInfo;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.PlayerUtil;

import lombok.Getter;
import redis.clients.jedis.JedisPubSub;

@Getter
public abstract class Cloud extends JedisPubSub {

    protected final Map<UUID, CompletableFuture<Boolean>> CONNECTION_REQUESTS = new ConcurrentHashMap<>();

    private final Supplier<GameSettings> settingsSupplier;
    private final Gamemode gamemode;
    private final SessionType sessionType;

    protected List<ServerInfo> availableServers;
    protected Lobby nextServer;

    private Thread jedisThread;
    private int tickTask;

    protected Cloud(Supplier<GameSettings> settingsSupplier, Gamemode gamemode, SessionType sessionType) {
        this.settingsSupplier = settingsSupplier;
        this.gamemode = gamemode;
        this.sessionType = sessionType;
    }

    public void start() {
        if (!isEnabled()) {
            tick();

            // Messaging for request system
            jedisThread = new Thread(() -> Redis.get().subscribe(this, CloudUtil.getChannel(sessionType)));
            jedisThread.start();

            tickTask = PvPDojo.schedule(this::tick).createTimer(20, -1);
        }
    }

    public void shutdown() {
        if (isEnabled()) {
            Bukkit.getScheduler().cancelTask(tickTask);
            unsubscribe();
            try {
                if (jedisThread != null) {
                    jedisThread.join();
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                Log.exception(e);
            }
            if (nextServer != null && !nextServer.isStarted()) {
                LobbyManager.inst().removeLobby(nextServer);
            }
        }
    }

    public boolean isEnabled() {
        return isSubscribed();
    }

    // Keeps track of currently connecting players so we do not queue them twice
    private final Set<UUID> PROCESSING = Collections.newSetFromMap(new ConcurrentHashMap<>());

    public void connectPlayer(Player player) {
        if (PROCESSING.add(player.getUniqueId())) {
            User user = User.getUser(player);

            PvPDojo.schedule(() -> {
                try {
                    if (!isEnabled()) {
                        player.sendMessage(PvPDojo.WARNING + "This service is currently disabled");
                        return;
                    }

                    if (CONNECTION_REQUESTS.containsKey(player.getUniqueId())) {
                        player.sendMessage(PvPDojo.WARNING + "Your request is pending");
                        return;
                    }

                    if (availableServers.isEmpty()) {
                        player.sendMessage(PvPDojo.WARNING + "Servers are currently booting");
                        return;
                    }

                    Collection<UUID> connecting = Collections.singletonList(player.getUniqueId());

                    if (user.getParty() != null && user.getParty().getLeader().equals(user.getUUID())) {
                        connecting = user.getParty().getUsers();
                    }

                    for (ServerInfo serverInfo : availableServers) {
                        boolean canJoin = true;

                        for (UUID uuid : connecting) {
                            if (!tryConnectToServer(uuid, serverInfo.getName())) {
                                canJoin = false;
                                break;
                            }
                        }

                        if (canJoin) {
                            for (UUID uuid : connecting) {
                                PlayerUtil.sendToServer(uuid, serverInfo.getName());
                            }
                            return;
                        }

                    }

                    player.sendMessage(PvPDojo.WARNING + "Could not find a suitable server");
                } finally {
                    PROCESSING.remove(player.getUniqueId());
                }
            }).createNonMainThreadTask();
        }
    }

    /*
     * We cannot fully rely on server playercount as it updates too slowly and does not take connecting players
     * into account. We first contact the server if there is space for our player to join. If this is the case
     * upon the next request the contacted server can take sum up their player count including their currently connecting
     * players.
     */
    public boolean tryConnectToServer(UUID uuid, String serverName) {
        CompletableFuture<Boolean> future = new CompletableFuture<>();
        BungeeSync.addTimeOut(future, Duration.ofSeconds(3));
        CONNECTION_REQUESTS.put(uuid, future);

        Redis.get().publish(CloudUtil.getChannel(sessionType), REQUEST + "|" + serverName + "|" + uuid);
        try {
            Boolean accept = future.get();
            if (accept != null && accept) {
                return true;
            }
        } catch (InterruptedException | ExecutionException e) {
            Log.exception(e);
        } finally {
            CONNECTION_REQUESTS.remove(uuid);
        }
        return false;
    }

    public void ensureNextServer() {
        if (nextServer == null || (nextServer.isRunning() && availableServers.stream().anyMatch(info -> info.getName().equals(nextServer.getNetworkIdentifier())))) {
            try {
                nextServer = LobbyManager.inst().createLobby(gamemode, settingsSupplier.get());
            } catch (AlreadyHostingException e) {
                Log.exception(e);
            }
        }
        if (availableServers.isEmpty() && !nextServer.isStarted()) {
            nextServer.start(Bukkit.getConsoleSender());
        }
    }

    public void tick() {
        availableServers = generateAvailableServers();
        ensureNextServer();
    }

    protected List<ServerInfo> generateAvailableServers() {
        return BungeeSync.getServerInfo().stream()
                         .filter(info -> info.getSessionInfo() != null)
                         .filter(info -> info.getSessionInfo().getSessionType() == sessionType)
                         .filter(getCustomFilter())
                         .filter(info -> info.getPlayers() < info.getSessionInfo().getMaxPlayers())
                         .sorted((serverInfo1, serverInfo2) -> Integer.compare(serverInfo2.getPlayers(), serverInfo1.getPlayers()))
                         .collect(Collectors.toList());
    }

    @Override
    public void onMessage(String channel, String message) {
        try {

            String[] split = StringUtils.PIPE.split(message);

            if (Integer.valueOf(split[0]) == RESPONSE) {
                CompletableFuture<Boolean> future = CONNECTION_REQUESTS.remove(UUID.fromString(split[1]));
                if (future != null) {
                    future.complete(Boolean.valueOf(split[2]));
                }
            }

        } catch (Throwable throwable) {
            Log.exception(throwable);
        }
    }

    public abstract Predicate<ServerInfo> getCustomFilter();

}
