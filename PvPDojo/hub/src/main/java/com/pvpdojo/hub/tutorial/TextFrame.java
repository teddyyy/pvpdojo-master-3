/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.tutorial;

import java.util.List;

import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.Hologram;

public class TextFrame implements Frame {

    private List<String> text;
    private int distance;

    public TextFrame(List<String> text) {
        this(text, 3);
    }

    public TextFrame(List<String> text, int distance) {
        this.text = text;
        this.distance = distance;
    }

    @Override
    public void accept(User user) {
        user.attachHologram(new Hologram(text), distance, Integer.MAX_VALUE);
    }

}
