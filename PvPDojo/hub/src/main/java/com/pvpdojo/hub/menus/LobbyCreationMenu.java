/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.menus;

import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.pvpdojo.Gamemode;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.ctm.settings.CTMSettings;
import com.pvpdojo.game.hg.settings.HGSettings;
import com.pvpdojo.game.lms.settings.LMSSettings;
import com.pvpdojo.game.settings.GameSettings;
import com.pvpdojo.game.settings.GameSettingsMenu;
import com.pvpdojo.hub.lobby.Lobby;
import com.pvpdojo.hub.lobby.LobbyManager;
import com.pvpdojo.hub.lobby.exception.AlreadyHostingException;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.WoolBuilder;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.TimeUtil;

public class LobbyCreationMenu extends SingleInventoryMenu {

    private CreationState state = CreationState.LOBBY_MODE;
    private Lobby lobby;
    private boolean publicLobby;

    public LobbyCreationMenu(Player player) {
        super(player, "Lobby Creation", 27);
    }

    private enum CreationState {
        GAME_MODE, LOBBY_MODE, GAME_SETTINGS, FINISHED
    }

    @Override
    public void redraw() {
        super.redraw();

        switch (state) {
            case GAME_MODE:
                if (!publicLobby || User.getUser(getPlayer()).getRank().inheritsRank(Rank.DEVELOPER)) {
                    page.setItem(9, Gamemode.HG.getIcon(), type -> selectGamemode(Gamemode.HG));
                }
                page.setItem(10, Gamemode.CTM.getIcon(), type -> selectGamemode(Gamemode.CTM));
                page.setItem(11, Gamemode.LMS.getIcon(), type -> selectGamemode(Gamemode.LMS));
                break;
            case LOBBY_MODE:
                page.setItem(12, new ItemBuilder(Material.GLOWSTONE_DUST).name(CC.GREEN + "Public").build(), type -> selectPrivacy(true));
                page.setItem(14, new ItemBuilder(Material.SULPHUR).name(CC.GREEN + "Private").build(), type -> selectPrivacy(false));
                break;
        }

        page.fillBlank();

    }

    public void selectGamemode(Gamemode gamemode) {
        GameSettings settings;

        switch (gamemode) {
            case HG:
                settings = new HGSettings();
                break;
            case CTM:
                settings = new CTMSettings();
                break;
            case LMS:
                settings = new LMSSettings();
                break;
            default:
                throw new IllegalStateException("Cannot select " + gamemode + " for a lobby");
        }

        try {
            this.lobby = LobbyManager.inst().createLobby(getPlayer().getUniqueId(), gamemode, settings);
            this.lobby.setPublicLobby(publicLobby);
            this.state = CreationState.GAME_SETTINGS;

            GameSettingsMenu<?> menu = lobby.getInfo().getSettings().getMenu(getPlayer());
            menu.getCurrentPage().setHasBar(true);
            menu.setBackFunction(null);
            menu.setDefaultActions(inventoryPage -> {
                inventoryPage.setItem(25, new ItemBuilder(Material.WATCH).name(CC.GRAY + "Countdown" + PvPDojo.POINTER + lobby.getCountdownTime()).build(),
                        menu.editNumber("Enter your countdown", countdown -> {
                            if (countdown >= 120 && countdown <= 300) {
                                lobby.setCountdownTime(countdown);
                            } else {
                                getPlayer().sendMessage(CC.RED + "Countdown must be between 120 and 300 seconds");
                            }
                        }));
                inventoryPage.setItem(26,
                        new WoolBuilder(DyeColor.GREEN).name(CC.GREEN + "START LOBBY")
                                                       .lore(settings.runChecks().stream().map(check -> CC.RED + check.getMessage()).collect(Collectors.toList())).build(), type -> {
                            User user = User.getUser(getPlayer());
                            if (!user.getRank().inheritsRank(Rank.DEVELOPER) && !TimeUtil.hasPassed(user.getCooldownSettings().getLastLobbyStart(), TimeUnit.HOURS, 1)) {
                                user.sendMessage(CC.RED + "You are on lobby creation cooldown");
                                return;
                            }

                            if (!settings.runChecks().isEmpty()) {
                                user.sendMessage(CC.RED + "Please resolve the listed settings problems");
                                return;
                            }

                            state = CreationState.FINISHED;
                            LobbyManager.inst().addLobby(lobby);
                            if (!LobbyManager.inst().publishLobby(getPlayer(), lobby)) {
                                LobbyManager.inst().removeLobby(lobby);
                            }
                            menu.closeSafely();
                        });
            });
            menu.open();

        } catch (AlreadyHostingException e) {
            Log.warn(e.getMessage());
            getPlayer().sendMessage(PvPDojo.WARNING + "You are already hosting");
            closeSafely();
        }

    }

    public void selectPrivacy(boolean publicLobby) {
        this.publicLobby = publicLobby;
        this.state = CreationState.GAME_MODE;
        this.redrawAndOpen();
    }

}
