/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.menus;

import static com.pvpdojo.redis.BungeeSync.getServerInfo;

import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.jetbrains.annotations.NotNull;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.ServerInfo;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.network.SessionInfo;
import com.pvpdojo.settings.ServerSwitchSettings;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Countdown;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.PlayerUtil;

import lombok.Data;
import lombok.EqualsAndHashCode;

public class HGMenu extends SingleInventoryMenu {

    public HGMenu(Player player) {
        super(player, "HG Menu", 9);
        setUpdateInterval(20);
    }

    @Override
    public void redraw() {
        super.redraw();
        User user = User.getUser(getPlayer());
        InventoryPage inv = getCurrentPage();
        List<HGServer> servers = generateServerList();

        inv.setSizeForNeededSlots(servers.size());

        for (int i = 0; i < servers.size(); i++) {
            HGServer server = servers.get(i);
            inv.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE)
                            .durability(server.getDyeColor().getWoolData())
                            .name(CC.GRAY + BungeeSync.getSubdomainById(server.getSessionInfo().getGameId()) + ".mc-hg.eu")
                            .lore(CC.GRAY + "Players" + PvPDojo.POINTER + server.getColor() + server.getSessionInfo().getSessionPlayerCount() + "/" + server.getSessionInfo().getMaxPlayers(),
                                    CC.GRAY + "Time" + PvPDojo.POINTER + server.getColor() + LocaleMessage.of(Countdown.getUserFriendlyNumber(server.getSessionInfo().getTime())).get(user)[0],
                                    "", CC.GOLD + "Right " + CC.GRAY + "click to queue"
                            ).build(),
                    type -> {
                        ServerSwitchSettings switchSettings = new ServerSwitchSettings(user);
                        if (type == ClickType.RIGHT || type == ClickType.SHIFT_RIGHT) {
                            switchSettings.addCommand("queue");
                        }
                        PlayerUtil.sendToServer(getPlayer(), server.getName(), switchSettings);
                    });
        }
        inv.fillBlank();

    }

    public static List<HGServer> generateServerList() {
        return getServerInfo().stream()
                              .filter(server -> server.getSessionInfo() != null)
                              .filter(server -> server.getSessionInfo().getSessionType() == SessionType.HG)
                              .map(server -> new HGServer(server.getSessionInfo(), server.getName(), server.getPlayers()))
                              .sorted().collect(Collectors.toList());
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class HGServer extends ServerInfo implements Comparable<HGServer> {

        HGServer(SessionInfo info, String name, int players) {
            super(name, players, null);
            setSessionInfo(info);
        }

        DyeColor getDyeColor() {
            return getSessionInfo().getSessionState() == SessionState.PREGAME ? DyeColor.LIME : getSessionInfo().getSessionState() == SessionState.INVINCIBILITY ?
                    DyeColor.YELLOW : getSessionInfo().getSessionState() == SessionState.STARTED ? DyeColor.ORANGE : DyeColor.RED;
        }

        CC getColor() {
            return getSessionInfo().getSessionState() == SessionState.PREGAME ? CC.GREEN : getSessionInfo().getSessionState() == SessionState.INVINCIBILITY ?
                    CC.YELLOW : getSessionInfo().getSessionState() == SessionState.STARTED ? CC.GOLD : CC.RED;
        }

        @Override
        public int compareTo(@NotNull HGServer hgServer) {
            if (getSessionInfo().getSessionState().ordinal() < hgServer.getSessionInfo().getSessionState().ordinal()) {
                return -1;
            } else if (getSessionInfo().getSessionState().ordinal() > hgServer.getSessionInfo().getSessionState().ordinal()) {
                return 1;
            }
            if (getSessionInfo().getSessionState() == SessionState.STARTED) {
                return getSessionInfo().getTime() - hgServer.getSessionInfo().getTime();
            }
            int playerSort = hgServer.getSessionInfo().getSessionPlayerCount() - getSessionInfo().getSessionPlayerCount();
            return playerSort == 0 ? getSessionInfo().getTime() - hgServer.getSessionInfo().getTime() : playerSort;
        }
    }

}
