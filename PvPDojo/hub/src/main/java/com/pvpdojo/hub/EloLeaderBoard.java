/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_7_R4.CraftWorld;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_7_R4.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.DBCommon;
import com.pvpdojo.DeveloperSettings;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.achievement.Achievement;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.mysql.SQLBuilder;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.TimeUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.Hologram;
import com.pvpdojo.util.bukkit.Human;

import net.minecraft.server.v1_7_R4.EntityBat;
import net.minecraft.server.v1_7_R4.EntityItem;
import net.minecraft.server.v1_7_R4.PacketPlayOutAttachEntity;
import net.minecraft.server.v1_7_R4.PacketPlayOutEntityMetadata;
import net.minecraft.server.v1_7_R4.PacketPlayOutSpawnEntity;
import net.minecraft.server.v1_7_R4.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_7_R4.PlayerConnection;
import net.minecraft.server.v1_7_R4.World;
import net.minecraft.util.com.mojang.authlib.GameProfile;

public class EloLeaderBoard extends Hologram {

    private static final EloLeaderBoard instance = new EloLeaderBoard();
    private Human human;
    private EntityItem right, left;

    public EloLeaderBoard() {
        super(Arrays.asList("Loading...", ".", ".", ".", ".", ".", ".", ".", ".", "."));
        PvPDojo.schedule(() -> {
            if (getLocation() != null) {
                this.human = new Human(getLocation().clone().add(0, 1.8, 0), null, new GameProfile(UUID.randomUUID(), "Loading..."));
                this.human.spawn();
                this.human.track();
            }
        }).nextTick();
        PvPDojo.schedule(() -> {
            if (getLocation() != null) {
                if (human == null) {
                    this.human = new Human(getLocation().clone().add(0, 1.8, 0), null, DBCommon.getProfile(PvPDojo.getOfflinePlayer("Buffer").getUniqueId()));
                    this.human.track();
                }
                PvPDojo.schedule(() -> {
                    List<String> lines = new ArrayList<>();
                    int i = 1;


                    lines.add(CC.RED + "Top 10 players on " + CC.DARK_PURPLE + "PvPDojo");
                    lines.add(CC.GRAY + "" + CC.STRIKETHROUGH + "                          ");
                    try (SQLBuilder rs = new SQLBuilder()) {
                        rs.select(Tables.USERS, "name, uuid, " + DeveloperSettings.SEASON.getName() + ".elo, rank")
                          .join(DeveloperSettings.SEASON, "id = userid")
                          .orderBy(DeveloperSettings.SEASON.getName() + ".elo", false)
                          .limit(10)
                          .executeQuery();

                        while (rs.next()) {
                            UUID uuid = UUID.fromString(rs.getString("uuid"));

                            DojoOfflinePlayer offlinePlayer = PvPDojo.getOfflinePlayer(uuid);

                            if (offlinePlayer.getCurrentBan() != null && TimeUtil.hasPassed(offlinePlayer.getCurrentBan().getTimestamp(), TimeUnit.DAYS, 5)) {
                                DeveloperSettings.SEASON.resetElo(offlinePlayer.getUniqueId());
                            }

                            PvPDojo.schedule(() -> {
                                User user = User.getUser(uuid);
                                if (user != null && user.isOnline()) {
                                    Achievement.THE_BIG_LEAGUE.trigger(Bukkit.getPlayer(uuid));
                                }
                            }).sync();

                            Rank rank = Rank.valueOf(rs.getString("rank"));
                            if (i == 1) {
                                Location looking = getLocation().clone();
                                looking.add(0, 1.8, 0);

                                GameProfile profile = DBCommon.getProfile(uuid);

                                PvPDojo.schedule(() -> {
                                    Human first = new Human(looking, null, profile);
                                    if (!human.getHuman().getProfile().equals(profile) || !human.getHuman().getProfile().getProperties().equals(profile.getProperties())) {
                                        this.human.untrack();
                                        this.human = first;
                                        first.track();
                                    }
                                }).sync();

                            }
                            lines.add(getColor(i) + "#" + i++ + " " + rank.getPrefix() + rs.getString("name") + CC.GREEN + " - " + rs.getInteger("elo"));
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    PvPDojo.schedule(() -> change(lines)).sync();
                }).createAsyncTask();
            }
        }).createTimer(140, 20 * 60, -1);
    }

    public static EloLeaderBoard inst() {
        return instance;
    }

    private CC getColor(int i) {
        switch (i) {
            case 1:
                return CC.GOLD;
            case 2:
                return CC.GRAY;
            case 3:
                return CC.DARK_GRAY;
            default:
                return CC.GREEN;
        }
    }

    @Override
    public void createFor(Player player) {
        super.createFor(player);
        spawnSwords(player);
    }

    @Override
    public void destroyFor(Player player) {
        super.destroyFor(player);

        if (right != null && left != null) {
            NMSUtils.get(player).removeQueue.add(left.getId());
            NMSUtils.get(player).removeQueue.add(right.getId());
        }
    }

    private void spawnSwords(Player player) {
        if (getLocation() != null) {
            if (right == null || left == null) {
                right = new EntityItem(((CraftWorld) getLocation().getWorld()).getHandle());
                left = new EntityItem(((CraftWorld) getLocation().getWorld()).getHandle());
                right.setItemStack(CraftItemStack.asNMSCopy(new ItemStack(Material.DIAMOND_SWORD)));
                left.setItemStack(CraftItemStack.asNMSCopy(new ItemStack(Material.DIAMOND_SWORD)));
                right.setLocation(getLocation().getX(), getLocation().getY(), getLocation().getZ(), 0, 0);
                left.setLocation(getLocation().getX(), getLocation().getY(), getLocation().getZ(), 0, 0);
            }
            spawnItem(player, getLocation().clone().add(0, 1.5, 1), right);
            spawnItem(player, getLocation().clone().add(0, 1.5, -1), left);
        }
    }

    private void spawnItem(Player player, Location loc, EntityItem item) {
        World world = ((CraftWorld) loc.getWorld()).getHandle();

        EntityBat bat = new EntityBat(world);
        bat.setLocation(loc.getX(), loc.getY(), loc.getZ(), 0, 0);
        bat.setInvisible(true);

        PacketPlayOutSpawnEntityLiving packetBat = new PacketPlayOutSpawnEntityLiving(bat);
        PacketPlayOutSpawnEntity packetItem = new PacketPlayOutSpawnEntity(item, 2);
        PacketPlayOutEntityMetadata itemMeta = new PacketPlayOutEntityMetadata(item.getId(), item.getDataWatcher(), true);
        PacketPlayOutAttachEntity attachpacket = new PacketPlayOutAttachEntity(0, item, bat);

        PlayerConnection nmsConnection = ((CraftPlayer) player).getHandle().playerConnection;
        nmsConnection.sendPacket(packetBat);
        nmsConnection.sendPacket(packetItem);
        nmsConnection.sendPacket(itemMeta);
        nmsConnection.sendPacket(attachpacket);
    }

}
