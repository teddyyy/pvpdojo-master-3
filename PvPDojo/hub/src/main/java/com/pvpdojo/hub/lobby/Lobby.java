/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.lobby;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.util.HashSet;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.GameProperty;
import com.pvpdojo.game.settings.GameSettings;
import com.pvpdojo.hub.lobby.exception.LobbyAlreadyStartedException;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.settings.CooldownSettings;
import com.pvpdojo.settings.SettingsType;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Countdown;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;

import lombok.Data;

@Data
public class Lobby<T extends GameSettings> {

    public static final String PREFIX = CC.DARK_GRAY + "[" + CC.DARK_PURPLE + "Lobby" + CC.DARK_GRAY + "] " + CC.GRAY;

    private final LobbyInfo<T> info;
    private boolean publicLobby = true;
    private int countdownTime = 120;

    private transient final Properties properties = new Properties();
    private transient final Set<UUID> participants = new HashSet<>();

    private int serverId = -1;
    private transient boolean published, started, running;
    private transient Countdown currentCountdown;

    public Lobby(LobbyInfo<T> info) {
        this.info = info;
    }

    public boolean addParticipant(Player participant) {
        return !started && publicLobby && participants.size() < info.getSettings().getMaxPlayers() && participants.add(participant.getUniqueId());
    }

    public boolean removeParticipant(Player participant) {
        return !started && participants != null && participants.remove(participant.getUniqueId());
    }

    public void broadcast(String m) {
        String message = PREFIX + m;
        if (!participants.contains(info.getHost())) {
            if (Bukkit.getPlayer(info.getHost()) != null) {
                Bukkit.getPlayer(info.getHost()).sendMessage(message);
            } else {
                Log.info(message);
            }
        }
        participants.stream()
                    .map(Bukkit::getPlayer)
                    .filter(Objects::nonNull)
                    .forEach(player -> player.sendMessage(message));
    }

    public void start(CommandSender sender) {
        if (started) {
            throw new LobbyAlreadyStartedException();
        }

        broadcast(CC.GREEN + "Starting Game...");
        started = true;

        // If Lobby is started by a player set their cooldown
        if (sender instanceof Player) {
            User user = User.getUser((Player) sender);
            if (user != null) {
                user.getCooldownSettings().setLastLobbyStart(System.currentTimeMillis());
                user.getCooldownSettings().save(user.getUUID());
            } else {
                PvPDojo.schedule(() -> {
                    CooldownSettings cooldownSettings = DBCommon.loadSettings(SettingsType.COOLDOWN, getInfo().getHost());
                    cooldownSettings.setLastLobbyStart(System.currentTimeMillis());
                    cooldownSettings.save(getInfo().getHost());
                }).createAsyncTask();
            }
        }

        new LobbyCreationThread(sender).start();
    }

    public void fillProperties() {

        properties.setProperty(GameProperty.SERVER_ID, String.valueOf(serverId));
        properties.setProperty(GameProperty.HOST, info.getHost().toString());
        properties.setProperty(GameProperty.PRIVATE, String.valueOf(!publicLobby));
        properties.setProperty(GameProperty.GAME, info.getType().toString());
        properties.setProperty(GameProperty.SETTINGS, DBCommon.GSON.toJson(info.getSettings()));
        if (!participants.isEmpty()) {
            properties.setProperty(GameProperty.PARTICIPANTS, StringUtils.getStringFromArray(participants, UUID::toString, "|"));
        }

    }

    public String getNetworkIdentifier() {
        return "game_" + serverId;
    }

    public void stop() {
        PvPDojo.schedule(() -> Redis.get().publish(BungeeSync.SHUTDOWN, getNetworkIdentifier())).createNonMainThreadTask();
    }

    private class LobbyCreationThread extends Thread {

        private CommandSender sender;

        public LobbyCreationThread(CommandSender sender) {
            this.sender = sender;
            setName("Lobby-Creation");
        }

        @Override
        public void run() {

            serverId = BungeeSync.getNextServerId();

            if (sender != null) {
                sender.sendMessage(CC.GREEN + "Copying files...");
            }

            fillProperties();

            File serverFolder = new File("../" + info.getHost().toString());

            try {
                if (!serverFolder.exists()) {
                    try {
                        new ProcessBuilder()
                                .directory(new File("../")) // Go to main network directory
                                .redirectErrorStream(true) // Log failures as IOException
                                .redirectOutput(Redirect.INHERIT)
                                .command("sh", "createserver.sh", "game", info.getHost().toString())
                                .start()
                                .waitFor();
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }

                try {
                    new ProcessBuilder()
                            .directory(serverFolder)
                            .redirectErrorStream(true) // Log failures as IOException
                            .redirectOutput(Redirect.INHERIT)
                            .command("sh", "update.sh")
                            .start()
                            .waitFor();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }

                // Write properties
                properties.store(new FileOutputStream(new File(serverFolder, GameProperty.FILE_NAME)), "");

                if (sender != null) {
                    sender.sendMessage(CC.GREEN + "Initializing server...");
                }

                new ProcessBuilder()
                        .directory(serverFolder)
                        .redirectErrorStream(true)
                        .command("sh", "start.sh")
                        .start();

            } catch (IOException e) {
                Log.exception(e);
                if (sender != null) {
                    sender.sendMessage(CC.RED + "Fail");
                }
            }

        }
    }

}
