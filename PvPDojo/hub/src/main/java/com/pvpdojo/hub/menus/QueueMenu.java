/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.menus;

import static com.pvpdojo.util.StringUtils.getEnumName;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.gui.ConfirmInputGUI;
import com.pvpdojo.hub.queue.PokerQueueItem;
import com.pvpdojo.hub.queue.QueueHandler;
import com.pvpdojo.hub.queue.QueueItem;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.menus.KitSelectorMenu;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.settings.BotSettings;
import com.pvpdojo.settings.BotSettings.Difficulty;
import com.pvpdojo.settings.SettingsType;
import com.pvpdojo.userdata.KitAbilityData;
import com.pvpdojo.userdata.PlayStyle;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.TriConsumer;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.SkullBuilder;

public class QueueMenu extends SingleInventoryMenu {

    public static final TriConsumer<User, ClickType, QueueItem> CLICK_HANDLER = (user, clickType, queueItem) -> {

        String queueName = getEnumName(queueItem.getSessionType(), true) + (queueItem.getStyle() != null ? " (" + getEnumName(queueItem.getStyle()) + ")" : "");

        if (clickType == ClickType.SHIFT_LEFT || clickType == ClickType.SHIFT_RIGHT) {
            if (QueueHandler.inst().unqueue(user.getUniqueId(), queueItem)) {
                user.sendMessage(MessageKeys.QUEUE_LEAVE, "{queue}", queueName);
                if (user.getMenu() != null) {
                    user.getMenu().redrawAndOpen();
                }
            } else {
                user.sendMessage(MessageKeys.QUEUE_NOT_QUEUED, "{queue}", queueName);
            }
        } else {
            String failMessage = QueueHandler.inst().queue(user.getUniqueId(), queueItem);
            if (failMessage == null) {
                user.sendMessage(MessageKeys.QUEUE_JOIN, "{queue}", queueName);
                if (user.getMenu() != null) {
                    user.getMenu().redrawAndOpen();
                }
            } else {
                user.sendMessage(MessageKeys.WARNING, "{text}", failMessage);
            }
        }
    };

    public QueueMenu(Player player) {
        super(player, CC.BLUE + "Deathmatch queues", 27);
        setUpdateInterval(10);
        page.setHasBar(true);
    }

    @Override
    public void redraw() {
        InventoryPage inv = getCurrentPage();

        // Always set back button to make it always symetric
        inv.setItem(0, new ItemBuilder(Material.REDSTONE).name(CC.DARK_RED + ">       Back       <").build(), getBackFunction());

        applyQueue(inv, 10, new QueueItem(getPlayer().getUniqueId(), null, SessionType.DUEL_CASUAL), getPlayer());
        applyQueue(inv, 12, new QueueItem(getPlayer().getUniqueId(), null, SessionType.DUEL_RANKED), getPlayer());
        applyQueue(inv, 16, new QueueItem(getPlayer().getUniqueId(), null, SessionType.TEAM_DUEL), getPlayer());
        applyQueue(inv, 14, new QueueItem(getPlayer().getUniqueId(), null, SessionType.DUEL_CASUAL_NO_ABILITY), getPlayer());
        applyQueue(inv, 4, new QueueItem(getPlayer().getUniqueId(), null, SessionType.FEAST_FIGHT), getPlayer());

        applyQueue(inv, 21, new QueueItem(getPlayer().getUniqueId(), PlayStyle.SOUP, SessionType.DUEL_RANKED), getPlayer());
//        applyQueue(inv, 19, new QueueItem(getPlayer().getUniqueId(), PlayStyle.SURVIVALIST, SessionType.DUEL_CASUAL), getPlayer());
//        applyQueue(inv, 20, new QueueItem(getPlayer().getUniqueId(), PlayStyle.POTTER, SessionType.DUEL_CASUAL), getPlayer());

//        applyQueue(inv, 22, new QueueItem(getPlayer().getUniqueId(), PlayStyle.SURVIVALIST, SessionType.DUEL_RANKED), getPlayer());
//        applyQueue(inv, 23, new QueueItem(getPlayer().getUniqueId(), PlayStyle.POTTER, SessionType.DUEL_RANKED), getPlayer());

        QueueItem pokerDummy = new QueueItem(getPlayer().getUniqueId(), null, SessionType.POKER);
        int pokerQueued = QueueHandler.inst().getPlayersInQueue(pokerDummy);
        int pokerIngame = pokerDummy.getStyle() == null ? QueueHandler.inst().getPlayerInGame(pokerDummy.getSessionType()) : 0;

        inv.setItem(23, new ItemBuilder(Material.GOLD_INGOT)
                        .name(CC.RED + "Poker Queue")
                        .lore(CC.GRAY + "Gamble your active abilities in a breathtaking duel")
                        .amount(pokerIngame + pokerQueued)
                        .appendLore(generateLore(pokerDummy, pokerQueued, pokerIngame))
                        .build(),
                type -> {
                    if (User.getUser(getPlayer()).getStats().getKills() >= 20) {
                        if (type == ClickType.SHIFT_LEFT || type == ClickType.SHIFT_RIGHT) {
                            CLICK_HANDLER.accept(User.getUser(getPlayer()), type, new PokerQueueItem(getPlayer().getUniqueId(), 0));
                        } else {
                            new KitSelectorMenu(getPlayer(), true, kitData -> {
                                User user = User.getUser(getPlayer());
                                user.getMenu().closeSafely();

                                boolean notOwning = false;

                                for (KitAbilityData kit : kitData.getAbilities()) {
                                    if (!user.getPersistentData().hasAbility(kit.getId())) {
                                        notOwning = true;
                                    }
                                }

                                if (notOwning) {
                                    getPlayer().sendMessage(CC.RED + "This kit contains abilities you are not owning please reset it");
                                    return;
                                }

                                if (kitData.getAbilities().size() >= 2) {
                                    PvPDojo.schedule(() -> new ConfirmInputGUI(getPlayer(),
                                            CC.RED + "By losing a match you also LOSE ONE OF YOUR PLAYED ABILITIES",
                                            () -> {
                                                CLICK_HANDLER.accept(User.getUser(getPlayer()), type,
                                                        new PokerQueueItem(getPlayer().getUniqueId(), kitData.getId()));
                                            }).open()).nextTick();
                                } else {
                                    getPlayer().sendMessage(CC.RED + "You must choose a kit with at least two abilities");
                                }
                            }).open();
                        }
                    } else {
                        getPlayer().sendMessage(PvPDojo.WARNING + "You need at least 20 kills to join poker");
                    }
                });

        PvPDojo.newChain().asyncFirst(() -> DBCommon.loadSettings(SettingsType.BOT, getPlayer().getUniqueId())).syncLast(loadedSettings -> {
            BotSettings settings = loadedSettings != null ? loadedSettings : new BotSettings();
            int ingame = QueueHandler.inst().getPlayerInGame(SessionType.BOT_DUEL);
            inv.setItem(8,
                    new SkullBuilder(getPlayer().getName())
                            .name(CC.DARK_PURPLE + "DojoBot")
                            .amount(ingame)
                            .lore(CC.GRAY + "Soups" + PvPDojo.POINTER + CC.BLUE + settings.getSoups(),
                                    CC.GRAY + "Difficulty" + PvPDojo.POINTER + settings.getDifficulty().getColor() + StringUtils.getEnumName(settings.getDifficulty()),
                                    CC.GRAY + "Old Bot" + PvPDojo.POINTER + (settings.isOldBot() ? CC.GREEN + "Enabled" : CC.RED + "Disabled"),
                                    "",
                                    CC.YELLOW + "" + ingame + " players in game", "", CC.GOLD + "Left " + CC.GRAY + "click to play", CC.GOLD + "Right " + CC.GRAY + "click to edit")
                            .build(),
                    clickType -> {
                        if (clickType == ClickType.LEFT) {
                            closeSafely();
                            QueueHandler.inst().queue(getPlayer().getUniqueId(), new QueueItem(getPlayer().getUniqueId(), null, SessionType.BOT_DUEL));
                        } else if (clickType == ClickType.RIGHT) {
                            new BotEditMenu(getPlayer(), settings).open();
                        }
                    });
            open();
        }).execute();

        inv.fillBlank();
    }

    public static void applyQueue(InventoryPage page, int slot, QueueItem item, Player player) {
        int queued = QueueHandler.inst().getPlayersInQueue(item);
        int ingame = item.getStyle() == null ? QueueHandler.inst().getPlayerInGame(item.getSessionType()) : 0;
        page.setItem(slot, new ItemBuilder(item.getStyle() != null ? item.getStyle().getIcon() : item.getSessionType().getIcon())
                        .name(CC.RED + getEnumName(item.getSessionType(), true) + (item.getStyle() != null ? " (" + getEnumName(item.getStyle()) + ")" : ""))
                        .glow(QueueHandler.inst().isQueued(player.getUniqueId(), item))
                        .amount(ingame + queued)
                        .lore(generateLore(item, queued, ingame))
                        .build(),
                clickType -> CLICK_HANDLER.accept(User.getUser(player), clickType, item));
    }

    public static List<String> generateLore(QueueItem item, int queued, int ingame) {
        List<String> lore = new ArrayList<>();
        lore.add("");
        if (item.getStyle() == null) {
            lore.add(CC.YELLOW + "" + ingame + " players in game");
        }
        lore.add(CC.YELLOW + "" + queued + " players in queue");
        lore.add("");
        lore.add(CC.GOLD + "Left " + CC.GRAY + "click to queue");
        lore.add(CC.GOLD + "Shift " + CC.GRAY + "click to leave queue");
        return lore;
    }

    private class BotEditMenu extends SingleInventoryMenu {

        private BotSettings settings;

        public BotEditMenu(Player player, BotSettings settings) {
            super(player, CC.BLUE + "Edit bot", 9);
            this.settings = settings;
            page.setHasBar(true);
        }

        @Override
        public void redraw() {
            InventoryPage inv = getCurrentPage();

            inv.setItem(2,
                    new ItemBuilder(Material.MUSHROOM_SOUP)
                            .name(CC.GRAY + "Soups" + PvPDojo.POINTER + CC.BLUE + settings.getSoups())
                            .lore("", CC.GOLD + "Left " + CC.GRAY + "click to add one soup", CC.GOLD + "Shift left " + CC.GRAY + " to add 10 soups",
                                    CC.GOLD + "Right " + CC.GRAY + "click to remove one soup", CC.GOLD + "Shift right " + CC.GRAY + " to remove 10 soups")
                            .build(),
                    clickType -> {
                        if (clickType == ClickType.SHIFT_LEFT) {
                            settings.addSoups(10);
                        } else if (clickType == ClickType.SHIFT_RIGHT) {
                            settings.addSoups(-10);
                        } else if (clickType == ClickType.LEFT) {
                            settings.addSoups(1);
                        } else if (clickType == ClickType.RIGHT) {
                            settings.addSoups(-1);
                        }
                        new ItemBuilder.ItemEditor(getInventory().getItem(2)).name(CC.GRAY + "Soups" + PvPDojo.POINTER + CC.BLUE + settings.getSoups()).build();
                    });
            inv.setItem(4, new ItemBuilder(Material.SKULL_ITEM).durability((short) 3).name(CC.GRAY + "Old Bot" + PvPDojo.POINTER + (settings.isOldBot() ? CC.GREEN + "Enabled" : CC.RED + "Disabled")).build(),
                    clickType -> {
                        settings.setOldBot(!settings.isOldBot());
                        new ItemBuilder.ItemEditor(getInventory().getItem(4))
                                .name(CC.GRAY + "Old Bot" + PvPDojo.POINTER + (settings.isOldBot() ? CC.GREEN + "Enabled" : CC.RED + "Disabled"))
                                .build();
                    });
            inv.setItem(6,
                    new ItemBuilder(Material.LAVA_BUCKET)
                            .name(CC.GRAY + "Difficulty" + PvPDojo.POINTER + settings.getDifficulty().getColor() + StringUtils.getEnumName(settings.getDifficulty())).build(),
                    clickType -> {
                        settings.setDifficulty(Difficulty.getNext(settings.getDifficulty()));
                        new ItemBuilder.ItemEditor(getInventory().getItem(6))
                                .name(CC.GRAY + "Difficulty" + PvPDojo.POINTER + settings.getDifficulty().getColor() + StringUtils.getEnumName(settings.getDifficulty()))
                                .build();
                    });
            inv.setItem(8, new ItemBuilder(Material.EMERALD).name(CC.GREEN + "Save").build(), clickType -> {
                inv.clear();
                PvPDojo.newChain().async(() -> settings.save(getPlayer().getUniqueId())).sync(() -> new QueueMenu(getPlayer()).open()).execute();
            });
        }

    }

}
