/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.pvpdojo.DBCommon;
import com.pvpdojo.Discord;
import com.pvpdojo.settings.FastDB;
import com.pvpdojo.settings.SettingsType;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.CompBuilder;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.bukkit.CC;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRoleAddEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRoleRemoveEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.bukkit.Bukkit;

import java.sql.SQLException;
import java.util.OptionalInt;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class BastiSubHandler extends ListenerAdapter {

    public static final Cache<UUID, String> REQUESTS = CacheBuilder.newBuilder().expireAfterWrite(30, TimeUnit.MINUTES).build();

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {

        if (event.getAuthor().getMutualGuilds().isEmpty()) {
            return;
        }

        if (event.getChannelType() == ChannelType.PRIVATE) {
            if (event.getMessage().getContentRaw().startsWith("!register") && event.getMessage().getContentRaw().split(" ").length == 2) {
                String ign = event.getMessage().getContentRaw().split(" ")[1];
                User user = User.getUser(Bukkit.getPlayerExact(ign));
                if (user != null) {
                    REQUESTS.put(user.getUUID(), event.getAuthor().getId());
                    user.sendMessage(new CompBuilder(CC.RED + "Discord account link request: Click this message to accept").command("discord").create());
                }
            }
        }
    }

    @Override
    public void onGuildMemberRoleAdd(GuildMemberRoleAddEvent event) {
        Log.info("Role Add");
        if (event.getRoles().contains(event.getGuild().getRoleById(Discord.BASTI_SUB))) {
            Log.info("Caught Basti Sub added");
            updateLinked(event.getMember(), fastDB -> fastDB.setBastiSub(true));
        } else if (event.getRoles().contains(event.getGuild().getRoleById(Discord.DOJO_SUB))) {
            Log.info("Caught Dojo Sub added");
            updateLinked(event.getMember(), fastDB -> fastDB.setDojoSub(true));
        }
    }

    @Override
    public void onGuildMemberRoleRemove(GuildMemberRoleRemoveEvent event) {
        Log.info("Role Remove");
        if (event.getRoles().contains(event.getGuild().getRoleById(Discord.BASTI_SUB))) {
            Log.info("Caught Basti Sub removed");
            updateLinked(event.getMember(), fastDB -> fastDB.setBastiSub(false));
        } else if (event.getRoles().contains(event.getGuild().getRoleById(Discord.DOJO_SUB))) {
            Log.info("Caught Dojo Sub remove");
            updateLinked(event.getMember(), fastDB -> fastDB.setDojoSub(false));
        }
    }

    private void updateLinked(Member member, Consumer<FastDB> action) {
        OptionalInt userId = Discord.get().getUserLink(member.getUser().getId());
        if (userId.isPresent()) {
            try {
                UUID uuid = DBCommon.getUserById(userId.getAsInt()).orElseThrow(IllegalStateException::new);
                FastDB fastDB = DBCommon.loadSettings(SettingsType.FAST_DB, uuid);
                action.accept(fastDB);
                fastDB.save(uuid);
            } catch (SQLException e) {
                Log.exception(e);
            }
        } else {
            Log.info("No Link found");
        }
    }

}
