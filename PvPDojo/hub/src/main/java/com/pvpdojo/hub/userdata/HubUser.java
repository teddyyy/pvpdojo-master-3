/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.userdata;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import com.pvpdojo.ServerType;
import com.pvpdojo.Warp;
import com.pvpdojo.hub.Hub;
import com.pvpdojo.hub.tutorial.Tutorial;
import com.pvpdojo.userdata.EloRank;
import com.pvpdojo.userdata.KitData;
import com.pvpdojo.userdata.User;
import com.pvpdojo.userdata.impl.UserImpl;
import com.pvpdojo.util.bukkit.CC;

public class HubUser extends UserImpl {

    public static HubUser getUser(Player player) {
        return Hub.get().getUser(player);
    }

    private KitData currentlyEditing;
    private Tutorial currentTutorial;

    public HubUser(UUID uuid) {
        super(uuid);
    }

    public KitData getCurrentlyEditing() {
        return currentlyEditing;
    }

    public void setCurrentlyEditing(KitData currentlyEditing) {
        this.currentlyEditing = currentlyEditing;
    }

    public Tutorial getCurrentTutorial() {
        return currentTutorial;
    }

    public void setCurrentTutorial(Tutorial currentTutorial) {
        this.currentTutorial = currentTutorial;
    }

    @Override
    public void updateUserFor(User td, boolean tracker) {
        super.updateUserFor(td, tracker);
        if (tracker || getPlayer().isTrackedBy(td.getPlayer()) || td == this) {

            Scoreboard scoreboard = td.getPlayer().getScoreboard();
            Objective undername = scoreboard.getObjective("undername");

            if (undername == null) {
                undername = scoreboard.registerNewObjective("undername", "dummy");
                undername.setDisplayName(CC.GRAY + "Elo");
            }

            undername.getScore(getPlayer().getNick()).setScore(getElo());

            if (undername.getDisplaySlot() != DisplaySlot.BELOW_NAME) {
                undername.setDisplaySlot(DisplaySlot.BELOW_NAME);
            }

        }

    }

    @Override
    public StringBuilder generatePrefix(StringBuilder prefix, User td) {
        if (td.getSettings().isShowElo()) {
            EloRank eloRank = EloRank.getRank(getElo());
            if (eloRank.ordinal() <= EloRank.TRAINEE.ordinal()) {
                prefix.append(eloRank.getColor()).append("[").append(eloRank.getName()).append("]").append(" ");
            }
        }
        return super.generatePrefix(prefix, td);
    }

    @Override
    public StringBuilder generateSuffix(StringBuilder suffix, User td) {
        if (getClan() != null && !getPlayer().isNicked() && td.getSettings().isShowClan()) {
            suffix.append(CC.DARK_GRAY).append(" [").append(CC.AQUA).append(getClan().getName()).append(CC.DARK_GRAY).append("]");
        }
        return super.generateSuffix(suffix, td);
    }

    @Override
    public void handleWarp(Warp warp) {
        super.handleWarp(warp);
        if (warp.getServer() == ServerType.HUB) {
            switch (warp) {
                case SPAWN:
                    teleport(Hub.get().getSpawn());
                    break;
                case MARKET:
                    teleport(Hub.get().getMarket());
                    break;
                case KITCREATOR:
                    teleport(Hub.get().getKitcreation());
                    break;
                case STAFF:
                    teleport(Hub.get().getStaff());
                    break;
                case LEADERBOARD:
                    teleport(Hub.get().getLeaderboard());
                    break;
                default:
                    break;
            }
        }
    }

    private boolean vanishedAfk;

    @Override
    public void setVanish(boolean vanish) {
        super.setVanish(vanish);
        vanishedAfk = false;
    }

    @Override
    public void addNoMoveSecond() {
        super.addNoMoveSecond();
        if (isAFK() && Bukkit.getOnlinePlayers().size() > 75) {
            if (!isVanish()) {
                setVanish(true);
                vanishedAfk = true;
            }
        } else if (isVanish() && vanishedAfk) {
            setVanish(false);
        }
    }
}
