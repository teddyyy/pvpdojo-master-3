/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.command;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.hub.Hub;
import com.pvpdojo.hub.userdata.HubUser;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.KitData;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Conditions;
import co.aikar.commands.annotation.Default;

@CommandAlias("save")
public class SaveCommand extends DojoCommand {

    @Default
    public void onSave(@Conditions("kitsave") HubUser user, KitData kit) {
        kit.setInventory(user.getPlayer().getInventory().getContents());
        user.getPersistentData().updateKit(kit);
        user.sendOverlay(MessageKeys.CUSTOM_KIT_SAVED, "{kit}", kit.getName());
        user.getPlayer().getInventory().setArmorContents(null);
        Hub.applyHubItems(user.getPlayer());
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
