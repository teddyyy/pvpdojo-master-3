/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.menus;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilityinfo.AbilityCollection;
import com.pvpdojo.abilityinfo.AbilityLog;
import com.pvpdojo.abilityinfo.AbilityRarity;
import com.pvpdojo.abilityinfo.RandomAbility;
import com.pvpdojo.achievement.Achievement;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.menu.CraftingInventoryMenu;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menus.AbilitiesMenu;
import com.pvpdojo.mysql.SQL;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.userdata.AbilityData;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.Log;

public class TradeUpMenu extends CraftingInventoryMenu {

    List<AbilityData> currentAbilites = new ArrayList<>(6);

    public TradeUpMenu(Player player) {
        super(player, "Tradeup Menu");
    }

    @Override
    public void redraw() {
        super.redraw();

        InventoryPage inv = getCurrentPage();
        inv.setHasBar(false);
        Collection<AbilityData> abilities = User.getUser(getPlayer()).getPersistentData().getAbilities();

        inv.setItem(2, new ItemBuilder(Material.ENDER_CHEST).name(CC.GREEN + "Add Ability").build(), type -> {
            if (currentAbilites.size() < 6) {
                new AbilitiesMenu(getPlayer(), abilities, (abilityData, ignore) -> {
                    currentAbilites.add(abilityData);
                    redrawAndOpen();
                }, this::cannotApply).open();
            }
        });

        for (int i = 0; i < 6; i++) {
            if (i < currentAbilites.size()) {
                int index = i;
                AbilityLoader loader = currentAbilites.get(i).getLoader();
                inv.setItem(i + 4, new ItemBuilder(loader.getAbilityIcon()).lore(loader.fixAbilityDescription(User.getUser(getPlayer()), null)).build(), type -> {
                    currentAbilites.remove(index);
                    redrawAndOpen();
                });
            }
        }

        if (currentAbilites.size() == 6) {
            inv.setItem(0, new ItemBuilder(Material.EMERALD).name(CC.GREEN + "Finalize Trade Up").build(), type -> {
                User user = User.getUser(getPlayer());
                user.getPersistentData().getAbilities().clear();

                Achievement.RECYCLE.trigger(getPlayer());

                PvPDojo.schedule(() -> {
                    UUID newUUID = UUID.randomUUID();

                    for (AbilityData abilityData : currentAbilites) {
                        PersistentData.updateAbilityOwnership(null, abilityData.getAbilityUUID(), "Tradeup: " + newUUID);
                    }

                    AbilityRarity newRarity = getTradeUpRarity().next();
                    AbilityCollection collection = AbilityCollection.getRandomCollection();

                    while (AbilityCollection.getAbilitiesInCollection(collection).stream().noneMatch(abilityLoader -> abilityLoader.getRarity() == newRarity)) {
                        collection = AbilityCollection.getRandomCollection();
                    }

                    int newAbilityId = RandomAbility.getAbilityFromRarity(newRarity, collection);
                    try {
                        SQL.insert(Tables.ABILITIES, "?,?,?,?,?", user.getUUID().toString(), 0, newUUID.toString(), newAbilityId, 1);
                    } catch (SQLException e) {
                        Log.exception("Tradeup: new ability insert failed", e);
                    }
                    new AbilityLog(newUUID, user.getUUID(), user.getUUID(), "Tradeup").save();

                    user.getPersistentData().readAbilities(null);

                    AbilityData newAbility = new AbilityData(user.getUUID(), newUUID, newAbilityId);
                    user.sendMessage(MessageKeys.TRADEUP, "{ability}", newAbility.getLoader().getDisplayName() + " Ability");

                }).createAsyncTask();

                closeSafely();
            });
        }

        inv.fillBlank();
    }

    private boolean cannotApply(AbilityData data) {
        if (currentAbilites.isEmpty()) {
            return data.getLoader().getRarity().ordinal() >= AbilityRarity.GOLD.ordinal();
        }
        return currentAbilites.contains(data) || getTradeUpRarity() != data.getLoader().getRarity();
    }

    private AbilityRarity getTradeUpRarity() {
        return currentAbilites.get(0).getLoader().getRarity();
    }

}
