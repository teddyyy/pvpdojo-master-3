/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.lobby.exception;

import com.pvpdojo.hub.lobby.Lobby;

public class AlreadyHostingException extends Exception {

    public AlreadyHostingException(Lobby lobby) {
        super(lobby.getInfo().getHost() + " is already hosting " + lobby.toString());
    }
}
