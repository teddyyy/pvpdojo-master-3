/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.tutorial;

import org.bukkit.Location;

import com.pvpdojo.userdata.User;

public class TeleportFrame implements Frame {

    private Location location;

    public TeleportFrame(Location location) {
        this.location = location;
    }

    @Override
    public void accept(User user) {
        user.teleport(location);
    }

}
