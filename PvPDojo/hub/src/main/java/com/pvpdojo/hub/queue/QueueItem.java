/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.queue;

import java.util.Objects;
import java.util.UUID;

import com.pvpdojo.session.SessionType;
import com.pvpdojo.userdata.PlayStyle;

public class QueueItem {

    private final UUID uuid;
    private final PlayStyle style;
    private final SessionType sessionType;

    public QueueItem(UUID uuid, PlayStyle style, SessionType sessionType) {
        this.uuid = uuid;
        this.style = style;
        this.sessionType = sessionType;
    }

    public UUID getUniqueId() {
        return uuid;
    }

    public PlayStyle getStyle() {
        return style;
    }

    public SessionType getSessionType() {
        return sessionType;
    }

    public boolean sameQueue(QueueItem other) {
        return sessionType == other.sessionType && style == other.style;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o instanceof QueueItem) {
            QueueItem queueItem = (QueueItem) o;
            return Objects.equals(uuid, queueItem.uuid) && style == queueItem.style && sessionType == queueItem.sessionType;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, style, sessionType);
    }
}
