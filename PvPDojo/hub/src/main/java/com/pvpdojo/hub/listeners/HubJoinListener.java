/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.listeners;

import static com.pvpdojo.util.StringUtils.getLS;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;

import com.pvpdojo.Discord;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.DataLoadedEvent;
import com.pvpdojo.bukkit.events.DataUpdateEvent;
import com.pvpdojo.bukkit.events.DojoPlayerJoinEvent;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.hub.Hub;
import com.pvpdojo.hub.lobby.LobbyManager;
import com.pvpdojo.hub.menus.HGMenu;
import com.pvpdojo.hub.menus.HGMenu.HGServer;
import com.pvpdojo.hub.queue.QueueHandler;
import com.pvpdojo.hub.userdata.HubUser;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.menus.ChallengeMenu;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.userdata.Request;
import com.pvpdojo.userdata.User;
import com.pvpdojo.userdata.stats.PersistentHGStats;
import com.pvpdojo.util.BroadcastUtil;
import com.pvpdojo.util.CompBuilder;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.TimeUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.PlayerUtil;

import net.md_5.bungee.api.ChatColor;

public class HubJoinListener implements DojoListener {

    public static void spawnPlayer(Player player) {
        if (BukkitUtil.countNearbyEntities(Hub.get().getSpawn(), 2, 2, 2) < 30) {
            player.teleport(Hub.get().getSpawn());
        } else if (BukkitUtil.countNearbyEntities(Hub.get().getSpawn().clone().add(0, 0, -20), 2, 2, 2) < 30) {
            player.teleport(Hub.get().getSpawn().clone().add(0, 0, -20));
            player.sendMessage(CC.RED + "Repositioned due to high load");
        } else {
            player.teleport(Hub.get().getKitcreation());
            player.sendMessage(CC.RED + "Repositioned due to high load");
        }

        PlayerUtil.resetPlayer(player);
        player.closeInventory();
        Hub.applyHubItems(player);
        HubUser user = HubUser.getUser(player);

        user.setCurrentTutorial(null);
    }

    @EventHandler
    public void onDataLoaded(DataLoadedEvent e) {
        User user = User.getUser(e.getPlayer());

        PvPDojo.schedule(() -> {
            if (!user.getFastDB().checkNameMC(user.getUUID())) {
                user.sendMessage(MessageKeys.NAMEMC_NOT_VOTED);
            }

            Runnable subReward = () -> ChallengeMenu.rewardCollections(user, 3);
            if (TimeUtil.hasPassed(user.getFastDB().getLastBastiReward(), TimeUnit.DAYS, 30)) {
                try {
                    String discordUUID = Discord.get().getDiscordLink(user.getUUID());
                    if (discordUUID != null) {
                        if (user.getFastDB().checkBastiSub(user.getUUID(), discordUUID)) {
                            user.getFastDB().setLastBastiReward(System.currentTimeMillis());
                            user.getFastDB().save(user.getUUID());

                            PvPDojo.schedule(subReward).sync();
                            new PersistentHGStats(user.getUUID()).addMoney(750);
                            user.sendMessage(MessageKeys.SUB_REWARD, "{streamer}", "BastiGHG");
                        }
                    }
                } catch (SQLException ex) {
                    Log.exception(ex);
                }
            }

            if (TimeUtil.hasPassed(user.getFastDB().getLastDojoReward(), TimeUnit.DAYS, 30)) {
                try {
                    String discordUUID = Discord.get().getDiscordLink(user.getUUID());
                    if (discordUUID != null) {
                        if (user.getFastDB().checkDojoSub(user.getUUID(), discordUUID)) {
                            user.getFastDB().setLastDojoReward(System.currentTimeMillis());
                            user.getFastDB().save(user.getUUID());

                            PvPDojo.schedule(subReward).sync();
                            new PersistentHGStats(user.getUUID()).addMoney(750);
                            user.sendMessage(MessageKeys.SUB_REWARD, "{streamer}", "PvPDojo");
                        }
                    }
                } catch (SQLException ex) {
                    Log.exception(ex);
                }
            }
        }).createAsyncTask();
        

        if (!user.isAdminMode()) {
            Hub.applyHubItems(e.getPlayer());
            if (user.getFastDB().isWonHG()) {
                user.getFastDB().setWonHG(false);
                user.getFastDB().save(user.getUniqueId());
                user.teleport(PvPDojo.get().getStandardMap().getLocation("hg_win"));
                BroadcastUtil.global(ChatChannel.GLOBAL, PvPDojo.PREFIX + CC.BLUE + user.getName() + CC.GREEN + " just won a round of Hardcore Games");
            }
            if (user.getFastDB().isLostHG()) {
                user.getFastDB().setLostHG(false);
                user.getFastDB().save(user.getUniqueId());
                if (user.getSettings().getHGSettings().isRejoin()) {
                    List<HGServer> serverList = HGMenu.generateServerList();
                    if (serverList.isEmpty() || serverList.get(0).getSessionInfo().getSessionState() != SessionState.PREGAME) {
                        user.sendMessage(PvPDojo.WARNING + "Could not find a suitable HG server to rejoin");
                    } else {
                        PlayerUtil.sendToServer(e.getPlayer(), serverList.get(0).getName());
                    }
                }
            }
        }
    }

    @EventHandler
    public void onDataUpdate(DataUpdateEvent e) {
        User user = User.getUser(e.getPlayer());

        if (user.getSettings().isScoreboard()) {
            user.getSidebar().setTitle(CC.DARK_PURPLE.toString() + CC.BOLD + "PvPDojo.com");
            user.getSidebar().set(12, "");
            user.getSidebar().set(11, CC.GRAY + "Kills");
            user.getSidebar().set(10, PvPDojo.POINTER + CC.GREEN + e.getData().getKills());
            user.getSidebar().set(9, "");
            user.getSidebar().set(8, CC.GRAY + "Deaths");
            user.getSidebar().set(7, PvPDojo.POINTER + CC.RED + e.getData().getDeaths());
            user.getSidebar().set(6, "");
            user.getSidebar().set(5, CC.GRAY + "Elo");
            user.getSidebar().set(4, PvPDojo.POINTER + CC.WHITE + e.getData().getElo());
            user.getSidebar().set(3, "");
            user.getSidebar().set(2, CC.GRAY + "Credits");
            user.getSidebar().set(1, PvPDojo.POINTER + CC.GOLD + e.getData().getMoney());

        }
    }

    @EventHandler
    public void onJoin(DojoPlayerJoinEvent e) {
        spawnPlayer(e.getPlayer());

        PvPDojo.newChain().delay(20).asyncFirst(() -> Redis.get().getValue(BungeeSync.RECONNECT + e.getPlayer().getUniqueId().toString())).syncLast(input -> {
            if (input != null) {
                User user = User.getUser(e.getPlayer());
                user.getRequests().put(PvPDojo.SERVER_UUID, new ReconnectRequest(input));
                e.getPlayer().sendMessage(CC.DARK_GRAY + getLS());
                e.getPlayer().spigot().sendMessage(new CompBuilder("Your game is still running ")
                        .color(ChatColor.RED)
                        .bold(true)
                        .append("[Reconnect]")
                        .color(ChatColor.GREEN)
                        .bold(false)
                        .command("accept " + PvPDojo.SERVER_UUID.toString())
                        .create());
                e.getPlayer().sendMessage(CC.DARK_GRAY + getLS());
            }
        }).execute();

    }

    private class ReconnectRequest extends Request {

        private String server;

        private ReconnectRequest(String server) {
            this.server = server;
        }

        @Override
        public void accept(User acceptor) {
            PlayerUtil.sendToServer(acceptor.getPlayer(), server);
        }

        @Override
        public void deny(User deniedBy) {}

        @Override
        public int getExpirationSeconds() {
            return Integer.MAX_VALUE;
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        QueueHandler.inst().unqueue(e.getPlayer().getUniqueId());
        QueueHandler.inst().unlock(e.getPlayer().getUniqueId());
        LobbyManager.inst().disconnectPlayer(e.getPlayer());
    }

}
