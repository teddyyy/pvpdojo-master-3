/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;

import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.hub.userdata.HubUser;

public class HubInventoryClickListener implements DojoListener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        HubUser user = HubUser.getUser((Player) e.getWhoClicked());
        if (!user.isAdminMode() && user.getCurrentlyEditing() == null) {
            e.setCancelled(true);
        }
    }

}
