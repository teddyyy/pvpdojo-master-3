/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.menus;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.bukkit.util.GuiItem;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.gui.ConfirmInputGUI;
import com.pvpdojo.hub.market.Market;
import com.pvpdojo.hub.market.MarketCache;
import com.pvpdojo.menu.SortType;
import com.pvpdojo.menu.SortedInventoryMenu;
import com.pvpdojo.menu.SortedItem;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.ItemEditor;
import com.pvpdojo.util.StringUtils;

public class MarketMenu extends SortedInventoryMenu {

    private Predicate<MarketCache> filter;

    public MarketMenu(Player player, Predicate<MarketCache> filter) {
        super(player, "Market", 54, SortType.ALPHABETICAL, SortType.RARITY);
        this.filter = filter;
        setDefaultActions(page -> page.setItem(4, new ItemBuilder(Material.BOOK).name(CC.RED + "Your offers").build(),
                click -> new MarketMenu(player, cache -> cache.getOriginalOwner().equals(player.getUniqueId())).open()));
    }

    @Override
    public void redraw() {
        super.redraw();

        List<MarketCache> data = Market.get().getMarketCache();
        if (filter != null) {
            data = data.stream().filter(filter).collect(Collectors.toList());
        }

        for (AbilityLoader loader : AbilityLoader.getAbilities()) {
            List<String> lore = loader.fixAbilityDescription(User.getUser(getPlayer()), null);
            int amount = (int) data.stream().filter(cache -> cache.getAbilityId() == loader.getId()).count();
            if (amount == 0) {
                continue;
            }
            lore.add(0, "");
            lore.add(0, CC.GRAY + "Amount for sale: " + (amount > 0 ? CC.GREEN : CC.RED) + amount);
            ItemStack item = new ItemBuilder(loader.getAbilityIcon()).lore(lore).amount(amount).build();

            SortedItem sort = new SortedItem(item, clickType -> new MarketBuyMenu(getPlayer(), loader, filter).open());
            sort.setSortName(GuiItem.getDisplayNameStripped(item));
            sort.setSortValue(loader.getRarity().ordinal());
            addSortItem(sort);
        }

    }

    private class MarketBuyMenu extends SortedInventoryMenu {

        private AbilityLoader loader;
        private Predicate<MarketCache> filter;

        public MarketBuyMenu(Player player, AbilityLoader loader, Predicate<MarketCache> filter) {
            super(player, "Market", 54, SortType.LOWEST_HIGHEST_PRICE, SortType.HIGHEST_LOWEST_PRICE);
            this.loader = loader;
            this.filter = filter;
        }

        @Override
        public void redraw() {
            super.redraw();

            User user = User.getUser(getPlayer());
            List<MarketCache> data = Market.get().getMarketCache();

            if (filter != null) {
                data = data.stream().filter(filter).collect(Collectors.toList());
            }

            for (MarketCache ability : data) {
                if (ability.getAbilityId() == loader.getId()) {
                    ItemStack item = loader.getAbilityIcon().clone();
                    List<String> lore = new ArrayList<>();
                    lore.add(CC.GRAY + "Offered by: " + CC.GOLD + ability.getOwnerName());
                    lore.add(CC.GRAY + "Price: " + CC.DARK_GREEN + ability.getPrice());
                    lore.add(CC.GRAY + "Date added: " + CC.LIGHT_PURPLE + StringUtils.getDateFromMillis(ability.getDate()));

                    if (ability.getOriginalOwner().equals(user.getUUID())) {
                        GuiItem.addGlow(item);
                        lore.add("");
                        lore.add(CC.GREEN + "This is your own market offer");
                    }
                    new ItemEditor(item).lore(lore).build();
                    SortedItem sort = new SortedItem(item, clickType -> {
                        if (user.getPersistentData().getMoney() < ability.getPrice() && !user.getUUID().equals(ability.getOriginalOwner())) {
                            user.getPlayer().sendMessage(CC.RED + "Not enough money to buy this ability, you need $" + CC.UNDERLINE
                                    + (ability.getPrice() - user.getPersistentData().getMoney()) + " more");
                            return;
                        }
                        new ConfirmInputGUI(getPlayer(), "Buy for " + CC.RED + ability.getPrice() + "$", () -> Market.get().transerItemFromMarket(ability, user)).open();
                    });
                    sort.setSortName(GuiItem.getDisplayNameStripped(item));
                    sort.setSortValue(ability.getPrice());
                    addSortItem(sort);
                }
            }
        }

    }

}
