/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.hub.queue;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import com.pvpdojo.clan.Clan;
import com.pvpdojo.session.network.component.ClanComponent.ClanGame;
import com.pvpdojo.userdata.DojoOfflinePlayer;

public class ClanQueueItem {

    private Clan clan;
    private Set<DojoOfflinePlayer> lineup;
    private Queue<ClanGame> desiredGameModes = new LinkedList<>();

    public ClanQueueItem(Clan clan, Set<DojoOfflinePlayer> lineup) {
        this.clan = clan;
        this.lineup = lineup;
    }

    public void applyGameMode(ClanGame type) {
        desiredGameModes.add(type);
    }

    public Queue<ClanGame> getDesiredGameModes() {
        return desiredGameModes;
    }

    public Set<DojoOfflinePlayer> getLineup() {
        return lineup;
    }

    public Clan getClan() {
        return clan;
    }
}
