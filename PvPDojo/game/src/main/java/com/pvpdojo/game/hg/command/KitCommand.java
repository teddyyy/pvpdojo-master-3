/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.command;

import co.aikar.commands.annotation.*;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.kits.menu.KitSelectorMenu;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

@CommandAlias("kit")
public class KitCommand extends DojoCommand {

    @CommandCompletion("@kits")
    @CatchUnknown
    @Default
    public void onKit(User u, @Optional HGKit kit) {
        HGUser user = (HGUser) u;

        // Retry if not loaded
        if (!user.getPersistentKits().isLoaded()) {
            PvPDojo.schedule(() -> onKit(u, kit)).createTask(5);
        }

        if (user.getSession() != null && (user.getSession().getState() == SessionState.PREGAME || user.getSession().getState() == SessionState.INVINCIBILITY
                || (user.getKit() != null && user.getKitHolder().getKitProvider() == HGKit.BACKUP))) {
            if (user.getSession().getState() == SessionState.INVINCIBILITY && user.getKit() != null && user.getKitHolder().getKitProvider() != HGKit.BACKUP) {
                user.sendMessage(CC.RED + "You have already chosen your kit");
                return;
            }

            if (kit != null && kit.isMainKit()) {
                if (!user.getPersistentKits().getAvailableKits().contains(kit)) {
                    user.sendMessage(CC.RED + "You do not own " + kit.getName());
                    return;
                }
                user.setKit(kit);

                if (user.getSession().getState() != SessionState.PREGAME) {
                    KitUtil.applyKitItems(user, user.getKit());
                }
                user.sendMessage(CC.GREEN + "You are now " + StringUtils.getEnumName(kit));
            } else {
                new KitSelectorMenu(u.getPlayer()).open();
            }

        } else {
            user.sendMessage(CC.RED + "You can't choose a kit after the game has started");
        }
    }

    @Subcommand("bonus")
    public void onKitBonus(User u) {
        onKit(u, HG.inst().getSession().getBonusKit(u.getUniqueId()));
    }

    @CommandAlias("bonuskit|bonus")
    @Default
    public void onShowBonusKit(Player player) {
        player.sendMessage(CC.GREEN + "Your bonus kit is " + StringUtils.getEnumName(HG.inst().getSession().getBonusKit(player.getUniqueId())));
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
