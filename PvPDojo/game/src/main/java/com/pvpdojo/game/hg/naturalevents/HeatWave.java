/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.naturalevents;

import java.util.EnumSet;
import java.util.Set;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Biome;

import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemUtil;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class HeatWave extends BiomeSpecificEvent {

    @Override
    Set<Biome> getBiomes() {
        return EnumSet.of(Biome.DESERT, Biome.DESERT_HILLS, Biome.DESERT_MOUNTAINS, Biome.JUNGLE, Biome.JUNGLE_EDGE, Biome.JUNGLE_EDGE_MOUNTAINS, Biome.JUNGLE_HILLS,
                Biome.JUNGLE_MOUNTAINS, Biome.MESA, Biome.MESA_PLATEAU, Biome.MESA_BRYCE, Biome.MESA_PLATEAU_FOREST, Biome.MESA_PLATEAU_FOREST_MOUNTAINS, Biome.MESA_PLATEAU_MOUNTAINS,
                Biome.HELL, Biome.SAVANNA, Biome.SAVANNA_MOUNTAINS, Biome.SAVANNA_PLATEAU, Biome.SAVANNA_PLATEAU_MOUNTAINS);
    }

    @Override
    public void start() {
    }

    @Override
    public void tick() {
        TrackerUtil.getIngame().forEach(player -> {
            if (getBiomes().contains(player.getLocation().getBlock().getBiome())) {
                player.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 40, 2), true);

/*                player.setFireTicks(2 * 20);
                BlockUtil.circle(player.getLocation(), 10, 10, false, true, 0).stream().map(Location::getBlock)
                         .filter(block -> ItemUtil.isWater(block.getType())).forEach(block -> {
                    block.getWorld().playEffect(block.getLocation(), Effect.LARGE_SMOKE, 0);
                    block.getWorld().playSound(block.getLocation(), Sound.FIZZ, 0.5F, 2.7F);
                    block.setType(Material.AIR);
                });

 */
            }
        });
    }

    @Override
    public void end() {

    }

    @Override
    public int getDuration() {
        return 90;
    }

    @Override
    public LocaleMessage getMessage() {
        return LocaleMessage.of("The HG Gods are not satisfied with this game, you feel an incredible heat overcoming you... " + CC.GOLD + "[Drought]");
    }
}
