/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.session;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.lang.HGMessageKeys;
import com.pvpdojo.game.hg.util.FeastUtil;
import com.pvpdojo.game.session.StageHandler;
import com.pvpdojo.game.session.StagedSession;
import com.pvpdojo.util.StringUtils;
import lombok.Getter;
import org.bukkit.Location;

public class BonusFeast extends Feast {

    @Getter
    private String biome;
    @Getter
    private boolean spawned = true;

    public BonusFeast(StagedSession session) {
        super(session);
    }

    @Override
    public StageHandler init() {
        setTimeSeconds(HG.inst().getSettings().getSecondsTillBonusFeast());
        return super.init();
    }

    @Override
    public void spawnFeast() {
        if (HG.inst().getSession().getInitialPlayerCount() < 20 || HG.inst().getSession().getPlayersLeft() < 2) {
            spawned = false;
            return;
        }

        Location feastLocation = FeastUtil.findFeastLocation(session.getArena().getWorld(), HG.inst().getSettings().getBorder() - 50);
        FeastUtil.spawnFeastPlatform(feastLocation);
        PvPDojo.schedule(() -> FeastUtil.spawnFeastChests(feastLocation)).createTask(5 * 20);
        biome = StringUtils.getEnumName(feastLocation.getBlock().getBiome(), true);
        session.broadcast(HGMessageKeys.BONUSFEAST, "{biome}", biome);
    }

    @Override
    public void run() {
        super.run();

        if (getTimeSeconds() >= HG.inst().getSettings().getSecondsTillPit()) {
            advance();
        }
    }

    @Override
    public StageHandler getNextHandler() {
        return new Pit(session);
    }
}
