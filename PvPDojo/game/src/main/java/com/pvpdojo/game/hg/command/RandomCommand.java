/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.command;

import java.util.List;

import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("random")
public class RandomCommand extends DojoCommand {

    @CatchUnknown
    @Default
    public void onRandom(Player player) {
        HGUser user = HGUser.getUser(player);
        List<HGKit> available = user.getPersistentKits().getAvailableKits();
        player.performCommand("kit " + available.get(PvPDojo.RANDOM.nextInt(available.size())).toString());
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
