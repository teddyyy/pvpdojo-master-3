/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Sound;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.userdata.kit.KitGameUser;
import com.pvpdojo.session.SessionState;

public class Werewolf extends Kit {

    private boolean night;
    private int loop;

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {
        if (night) {
            e.setDamage(e.getDamage() + 1.5);
        }
    }

    @Override
    public void onTick(KitGameUser user) {
        if (HG.inst().getSession().getState() == SessionState.STARTED) {
            if (user.getPlayer() != null) {
                if (user.getPlayer().getWorld().getTime() >= 13000) {
                    night = true;
                    user.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 6000, 0));
                    if (++loop > 180 || loop == 1) {
                        user.getPlayer().getWorld().playSound(user.getPlayer().getLocation(), Sound.WOLF_HOWL, 1.0F, 0.9F);
                        if (loop > 180)
                            loop = 2;
                    }
                } else if (user.getPlayer().getWorld().getTime() < 13000 && night) {
                    night = false;
                }
            }
        }
    }

}
