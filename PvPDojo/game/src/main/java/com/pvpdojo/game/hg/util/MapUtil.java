/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.util;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;

import com.boydti.fawe.FaweAPI;
import com.boydti.fawe.object.schematic.Schematic;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.map.DojoMap;
import com.pvpdojo.util.Log;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.math.transform.AffineTransform;

public class MapUtil {

    public static void spawnBorder() {
        File borderFile = new File(DojoMap.MAP_SCHEMATICS, "hg_border.schematic");
        File cornerFile = new File(DojoMap.MAP_SCHEMATICS, "hg_corner.schematic");
        File middleFile = new File(DojoMap.MAP_SCHEMATICS, "hg_middle.schematic");

        int border = HG.inst().getSettings().getBorder() + 1;
        try {
            Schematic cornerSchematic = ClipboardFormat.SCHEMATIC.load(cornerFile);
            Schematic wallSchematic = ClipboardFormat.SCHEMATIC.load(borderFile);
            Schematic middleSchematic = ClipboardFormat.SCHEMATIC.load(middleFile);

            rotationPaste(cornerSchematic, new Vector(border, 30, border), 0);
            rotationPaste(cornerSchematic, new Vector(-border, 30, border), 90);
            rotationPaste(cornerSchematic, new Vector(-border, 30, -border), 180);
            rotationPaste(cornerSchematic, new Vector(border, 30, -border), 270);

            for (int secondPart = border - 51; secondPart > -border + 51; secondPart -= 50) {
                int zeroExclude = secondPart <= 0 ? secondPart - 1 : secondPart;

                rotationPaste(wallSchematic, new Vector(zeroExclude, 30, border), 0);
                rotationPaste(wallSchematic, new Vector(zeroExclude - 49, 30, -border), 180);

                rotationPaste(wallSchematic, new Vector(border, 30, zeroExclude - 49), 270);
                rotationPaste(wallSchematic, new Vector(-border, 30, zeroExclude), 90);
            }

            rotationPaste(middleSchematic, new Vector(0, 30, border), 0);
            rotationPaste(middleSchematic, new Vector(-border, 30, 0), 90);
            rotationPaste(middleSchematic, new Vector(0, 30, -border), 180);
            rotationPaste(middleSchematic, new Vector(border, 30, 0), 270);

        } catch (IOException e) {
            Log.exception(e);
        }
    }

    private static void rotationPaste(Schematic schematic, Vector to, int rotation) {
        schematic.paste(FaweAPI.getWorld(Bukkit.getWorlds().get(0).getName()), to, false, false, rotation != 0 ? new AffineTransform().rotateY(-rotation) : null);
    }

    public static void pregenerateSpawnChunks() {
        for (int x = -12; x < 12; x++) {
            for (int z = -12; z < 12; z++) {
                Bukkit.getWorlds().get(0).getChunkAt(x, z).load(true);
            }
        }
    }

}
