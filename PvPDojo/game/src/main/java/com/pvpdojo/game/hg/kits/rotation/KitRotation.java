/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits.rotation;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.util.Log;

public class KitRotation extends TimerTask {

    public static final String KEY = "hgkitrotation";

    public static void scheduleRotation() {
        Timer timer = new Timer();
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime rotation = now.with(TemporalAdjusters.next(DayOfWeek.FRIDAY)).withHour(0).withMinute(2).withSecond(0);
        Duration dur = Duration.between(now, rotation);
        Log.info(String.valueOf(dur.getSeconds()));
        timer.schedule(new KitRotation(), dur.getSeconds() * 1000);
    }

    public static KitRotationData getCurrentRotation() {
        return DBCommon.GSON.fromJson(Redis.get().getValue(KitRotation.KEY), KitRotationData.class);
    }

    @Override
    public void run() {
        Log.info("Rotating");
        KitRotationData data = new KitRotationData();

        List<HGKit> kits = new ArrayList<>();
        kits.add(HGKit.SURPRISE);
        kits.add(HGKit.PACIFIST);
        kits.add(HGKit.CAPITALIST);

        while (kits.size() < 13) {
            HGKit kit = HGKit.values()[PvPDojo.RANDOM.nextInt(HGKit.values().length)];
            if (!kits.contains(kit)) {
                kits.add(kit);
            }
        }

        data.setKits(kits);
        Redis.get().setValue(KEY, DBCommon.GSON.toJson(data));
    }
}