/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;

public class Hulk extends Kit {

    @Override
    public void onInteractEntity(PlayerInteractEntityEvent e) {

        if (HG.inst().getSession().getState() != SessionState.STARTED) {
            return;
        }

        if (e.getPlayer().getItemInHand().getType() == Material.AIR && e.getPlayer().isEmpty() && e.getRightClicked().getPassenger() != e.getPlayer()) {

            Block block = e.getPlayer().getEyeLocation().getBlock();

            if (block.getRelative(BlockFace.UP).getType() != Material.AIR || block.getLocation().add(0, 2, 0).getBlock().getType() != Material.AIR) {
                e.getPlayer().sendMessage(CC.RED + "There has to be 2 blocks of air above you to pickup your target");
                return;
            }

            if (getKitCooldown().isOnCooldown()) {
                getKitCooldown().sendCooldown(User.getUser(e.getPlayer()));
                return;
            }
            getKitCooldown().use();

            if (e.getRightClicked() instanceof Player) {
                if (!HGUser.getUser((Player) e.getRightClicked()).isIngame()) {
                    return;
                }
            }

            e.getPlayer().setPassenger(e.getRightClicked());
        }

    }

    @Override
    public void onInteract(PlayerInteractEvent e) {

        Player p = e.getPlayer();

        if ((e.getAction() == Action.LEFT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_AIR) && !p.isEmpty()) {

            Entity t = p.getPassenger();

            p.eject();

            Vector handle = p.getEyeLocation().getDirection();
            handle.setY(0.4D);
            t.setVelocity(handle);

        }
    }

    /*
     * Hot-Fix for 1.8 clients
     */
    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {
        Player p = (Player) e.getDamager();
        if (!p.isEmpty() && p.getPassenger() == e.getEntity()) {
            p.eject();
        }
    }

    @Override
    public int getCooldown() {
        return 5_000;
    }

}
