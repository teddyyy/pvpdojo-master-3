/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits.sidekits;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class Grandpa extends Kit {
    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] { new ItemBuilder(Material.STICK).enchant(Enchantment.KNOCKBACK, 2).build() };
    }
}
