/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.userdata.kit;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.google.common.collect.Lists;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.kits.KitHolder;
import com.pvpdojo.game.kits.KitProvider;
import com.pvpdojo.game.userdata.GameUserImpl;

public abstract class KitGameUserImpl<P extends KitProvider> extends GameUserImpl implements KitGameUser<P> {

    private List<KitHolder<P>> kitHolders = new ArrayList<>();
    private List<Kit> kits = Lists.transform(kitHolders, KitHolder::getKit);

    public KitGameUserImpl(UUID uuid) {
        super(uuid);
    }

    @Override
    public void run() {
        getKits().forEach(kit -> kit.onTick(this));
    }

    @Override
    public List<Kit> getKits() {
        return kits;
    }

    @Override
    public List<Kit> getKitsDirect() {
        return kits;
    }

    @Override
    public List<KitHolder<P>> getKitHolders() {
        return kitHolders;
    }

    @Override
    public void addKit(P kitProvider) {
        kitHolders.add(new KitHolder<>(kitProvider, kitProvider.getSupplier().get()));
    }

    @Override
    public void setSpectator(boolean spectator) {
        super.setSpectator(spectator);
        if (spectator) {
            kits.forEach(kit -> kit.onLeave(getPlayer()));
            kits.clear();
        }
    }
}
