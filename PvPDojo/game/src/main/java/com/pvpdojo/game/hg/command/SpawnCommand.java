/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.command;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import co.aikar.taskchain.TaskChain;

@CommandAlias("spawn")
public class SpawnCommand extends DojoCommand {

    private Set<UUID> cooldown = new HashSet<>();

    @Default
    @CatchUnknown
    public void onSpawn(Player player) {
        if (HG.inst().getSession().getState() == SessionState.PREGAME && HG.inst().getSession().getCurrentHandler().getTimeSeconds() > 15) {
            if (cooldown.add(player.getUniqueId())) {
                int border = HG.inst().getSettings().getBorder();
                int x = PvPDojo.RANDOM.nextInt(border * 2) - border, z = PvPDojo.RANDOM.nextInt(border * 2) - border;

                int previous = player.spigot().getViewDistance();
                player.spigot().setViewDistance(3);

                player.teleport(BlockUtil.getHighestBlock(player.getWorld(), x, z).getLocation().add(0.5, 1, 0.5));

                TaskChain chain = PvPDojo.newChain();
                for (int i = 3; i <= previous; i++) {
                    final int currentView = i;
                    chain.delay(i).sync(() -> player.spigot().setViewDistance(currentView));
                }
                chain.sync(() -> cooldown.remove(player.getUniqueId())).execute();
            } else {
                player.sendMessage(CC.RED + "You are using this command too fast");
            }
        } else {
            player.sendMessage(CC.RED + "You cannot use this command at this stage");
        }
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
