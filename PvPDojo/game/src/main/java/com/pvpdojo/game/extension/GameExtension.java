/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.extension;

import com.pvpdojo.extension.DojoExtension;
import com.pvpdojo.extension.DojoServer;
import com.pvpdojo.game.session.StagedSession;
import com.pvpdojo.game.settings.GameSettings;
import com.pvpdojo.game.userdata.GameUser;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class GameExtension<SESSION extends StagedSession, SETTINGS extends GameSettings, T extends GameUser> extends DojoExtension<T> {

    private SESSION session;
    private SETTINGS settings;

    public GameExtension(DojoServer<T> server, SETTINGS settings) {
        super(server);
        this.settings = settings;
    }

    public abstract SESSION createSession();

    public abstract void preLoad();

    @Override
    public void postLoad() {
        session = createSession();
    }
}
