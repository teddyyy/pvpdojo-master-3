/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.kits;

import org.bukkit.event.player.PlayerInteractEvent;

public interface BlockInteractingKit extends ClickKit<PlayerInteractEvent> {
    default void onRightClick(PlayerInteractEvent event) {}
    default void onLeftClick(PlayerInteractEvent event) {}
}
