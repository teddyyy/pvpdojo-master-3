/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import com.pvpdojo.game.hg.kits.list.HGKit;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.ServerSettings.CustomRecipeModule;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.kits.KitHolder;
import com.pvpdojo.game.userdata.kit.KitGameUser;

public class Camel extends ComplexKit {

    private boolean hasCamelSpeed = false;

    @Override
    public void onMove(PlayerMoveEvent e) {
        if (e.getFrom().getBlock().equals(e.getTo().getBlock())) {
            Biome biome = e.getTo().getBlock().getBiome();
            if (biome == Biome.DESERT || biome == Biome.DESERT_HILLS || biome == Biome.DESERT_MOUNTAINS || biome.name().contains("MESA")) {
                if (!e.getPlayer().hasPotionEffect(PotionEffectType.SPEED) || hasCamelSpeed) {
                    hasCamelSpeed = true;
                    e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 5 * 20, 0), true);
                }
            } else {
                hasCamelSpeed = false;
            }
        }
    }

    @EventHandler
    public void onPrepareCraft(PrepareItemCraftEvent e) {
        KitGameUser<?> user = KitGameUser.getUser((Player) e.getView().getPlayer());

        if (e.getInventory().contains(Material.CACTUS) && !e.getInventory().contains(Material.BOWL)
                && user.getKitHolders().stream().map(KitHolder::getKitProvider).noneMatch(HGKit.CAMEL::equals)) {
            e.getInventory().setResult(null);
        }
    }

    @Override
    public void register() {
        super.register();
        ShapelessRecipe camelrecipe = new ShapelessRecipe(new ItemStack(Material.MUSHROOM_SOUP));
        camelrecipe.addIngredient(1, Material.CACTUS);
        new CustomRecipeModule(camelrecipe).register();
    }
}
