package com.pvpdojo.game.hg.kits.sidekits;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.ItemUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class Oasis extends Kit {

    private BukkitRunnable run;

    @Override
    public void onMove(PlayerMoveEvent e) {

        Material fromMat = e.getFrom().getBlock().getType();
        Material toMat = e.getTo().getBlock().getType();

        boolean preWater = (fromMat == Material.WATER || fromMat == Material.STATIONARY_WATER);
        boolean postWater = (toMat == Material.WATER || toMat == Material.STATIONARY_WATER);

        if (!preWater && postWater) {
            run = new BukkitRunnable() {
                @Override
                public void run() {
                    ItemUtil.addItemWhenFree(e.getPlayer(), new ItemStack(Material.BOWL));
                    ItemUtil.addItemWhenFree(e.getPlayer(), new ItemStack(Material.BROWN_MUSHROOM));
                    ItemUtil.addItemWhenFree(e.getPlayer(), new ItemStack(Material.RED_MUSHROOM));
                }
            };
            run.runTaskTimer(PvPDojo.get(), 3 * 20, 3 * 20);
            return;
        }
        if (preWater && !postWater) {
            run.cancel();
        }
    }

}
