/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.kits.defaults;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class Archer extends Kit {

    public static final String ARCHER_META = "archer_meta";

    @Override
    public void onBowShoot(EntityShootBowEvent e) {
        e.getProjectile().setMetadata(ARCHER_META, new FixedMetadataValue(PvPDojo.get(), null));
    }

    @Override
    public void onProjectileHit(EntityDamageByEntityEvent e) {

        if (!e.isCancelled() && e.getDamager().hasMetadata(ARCHER_META)) {
            ((Player) ((Projectile) e.getDamager()).getShooter()).getInventory().addItem(new ItemStack(Material.ARROW));
        }

    }

    @Override
    public void onKill(PlayerDeathEvent e) {
        e.getEntity().getKiller().getInventory().addItem(new ItemStack(Material.ARROW, 5));
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] { new ItemBuilder(Material.BOW).enchant(Enchantment.ARROW_KNOCKBACK, 1).build(), new ItemStack(Material.ARROW, 10) };
    }
}
