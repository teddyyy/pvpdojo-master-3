/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.naturalevents;

import java.util.EnumSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Biome;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.CC;

public class ThunderStorm extends BiomeExcludingEvent {

    private int tickCount;

    @Override
    public void start() {
        PvPDojo.get().getServerSettings().setRain(true);
        Bukkit.getWorlds().get(0).setThundering(true);
    }

    @Override
    public void tick() {

        if (++tickCount % 6 == 0) {
            TrackerUtil.getIngame().forEach(player -> {
                Vector randomVector = Vector.getRandom().multiply(3).setY(0);
                if (PvPDojo.RANDOM.nextBoolean()) {
                    randomVector.multiply(-1);
                }
                Location location = BlockUtil.getHighestBlock(player.getLocation().add(randomVector)).getLocation().add(0, 1, 0);
                if (KitUtil.hasKit(HGUser.getUser(player), HGKit.THOR)) {
                    player.getWorld().strikeLightningEffect(location);
                } else {
                    player.getWorld().strikeLightning(location);
                }
            });
        }

    }

    @Override
    public void end() {
        PvPDojo.get().getServerSettings().setRain(false);
        Bukkit.getWorlds().get(0).setThundering(false);
        Bukkit.getWorlds().get(0).setStorm(false);
    }

    @Override
    public int getDuration() {
        return 70;
    }

    @Override
    public LocaleMessage getMessage() {
        return LocaleMessage.of("The HG Gods are not satisfied with this game, prepare for their wrath. " + CC.GOLD + "[Thunder Storm]");
    }

    @Override
    Set<Biome> getExcludedBiomes() {
        return EnumSet.of(Biome.DESERT, Biome.DESERT_HILLS, Biome.DESERT_MOUNTAINS);
    }
}
