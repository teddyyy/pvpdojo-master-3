/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.naturalevents;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.hg.util.EquipmentUtil;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.util.VectorUtil;
import com.pvpdojo.util.bukkit.CC;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BeastAttack implements NaturalEvent {

    @Override
    public void start() {
        List<Player> playerList = new ArrayList<>();
        TrackerUtil.getIngame().filter(player -> 40 <= player.getLocation().getY() && player.getLocation().getY() <= 100).forEach(playerList::add);

        playerList.sort(Comparator.comparingDouble(EquipmentUtil::getEquipmentPoints));//Sorting to who has the lowest points
        Collections.reverse(playerList);//Reverse the list to who has the highest.

        for (int i = 0; i < 3; i++) {//Spawn to the 'i' highest equipment points players.
            Player player = playerList.get(i);

            for (int j = 0; j < 3; j++) {
                Wolf wolf = player.getWorld().spawn(player.getLocation(), Wolf.class);
                wolf.setVelocity(VectorUtil.getRandomVelocity());
                wolf.setAngry(true);
                wolf.setMaxHealth(12D);
                wolf.setHealth(wolf.getMaxHealth());
                PvPDojo.schedule(() -> {
                    wolf.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1));
                    wolf.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 2));
                    wolf.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 2));
                }).nextTick();

                if (!KitUtil.hasKit(HGUser.getUser(player), HGKit.BEASTMASTER)) {
                    wolf.setTarget(player);
                }
            }
        }
    }

    @Override
    public void tick() {

    }

    @Override
    public void end() {

    }

    @Override
    public int getDuration() {
        return 60;
    }

    @Override
    public LocaleMessage getMessage() {
        return LocaleMessage.of("The HG Gods are not satisfied with this game, powerful creatures were released upon all players. " + CC.GOLD + "[Beast Attack]");
    }
}
