/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class Rogue extends Kit implements InteractingKit {

    @Override
    public void activate(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        List<HGUser> tracked = TrackerUtil.getNearbyIngame(player, 25, 25, 25).map(HGUser::getUser).collect(Collectors.toList());

        if (tracked.isEmpty()) {
            player.sendMessage(CC.RED + "No players nearby");
            return;
        }

        player.sendMessage(CC.GREEN + "You disabled the abilities of every kit around you!");

        getKitCooldown().use();

        for (HGUser user : tracked) {
            user.sendMessage(CC.RED + "A Rogue disabled your kit");
            user.getKits().forEach(kit -> kit.onLeave(user.getPlayer()));
            user.disableKits(25);
        }
    }

    @Override
    public void click(Player player) {}

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemBuilder(Material.STICK).enchant(Enchantment.DURABILITY, 1).build();
    }

    @Override
    public int getCooldown() {
        return 45_000;
    }
}
