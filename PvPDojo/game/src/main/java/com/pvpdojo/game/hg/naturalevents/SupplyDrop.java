/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.naturalevents;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.game.hg.util.FeastUtil;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.util.bukkit.BlockUtil;

public class SupplyDrop implements NaturalEvent, DojoListener {

    private Chicken chicken;
    private FallingBlock fallingBlock;
    private int counter;
    private int fireWork;
    private final int coordX = PvPDojo.RANDOM.nextInt(400) - 200, coordZ = PvPDojo.RANDOM.nextInt(400) - 200;

    @Override
    public void start() {
        TrackerUtil.getIngame().forEach(player -> player.playSound(player.getLocation(), Sound.FIREWORK_LAUNCH, 1, 1));
    }

    @Override
    public void tick() {
        if (++counter == 30) {
            register();

            Location location = new Location(Bukkit.getWorlds().get(0), coordX, BlockUtil.getHighestBlock(Bukkit.getWorlds().get(0), coordX, coordZ).getY() + 50, coordZ);
            fallingBlock = location.getWorld().spawnFallingBlock(location, Material.PISTON_BASE, (byte) 6);
            fallingBlock.setDropItem(false);

            chicken = location.getWorld().spawn(location, Chicken.class);
            chicken.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 0));
            chicken.setPassenger(fallingBlock);

            new BukkitRunnable() {
                @Override
                public void run() {
                    if (chicken.isOnGround() || chicken.getLocation().getBlock().isLiquid()) {
                        cancel();
                        chicken.remove();
                        fallingBlock.remove();

                        FeastUtil.setChest(chicken.getLocation().getBlock(), FeastUtil.FEAST, 5);
                    } else {
                        for (int i = 0; i < 3; i++) {
                            chicken.getWorld().playEffect(chicken.getLocation(), Effect.CLOUD, 0);
                            chicken.getWorld().playEffect(chicken.getLocation().add(0, 1, 0), Effect.SMOKE, i);
                        }
                        fallingBlock.setTicksLived(5);

                        if (++fireWork % 3 == 0) {
                            BukkitUtil.launchFireworkEffect(chicken.getLocation().add(0, 1, 0), FireworkEffect.builder().with(FireworkEffect.Type.BURST)
                                                                                                              .withColor(Color.RED)
                                                                                                              .withColor(Color.WHITE)
                                                                                                              .withFlicker()
                                                                                                              .withTrail().build());
                        }
                    }
                }
            }.runTaskTimer(PvPDojo.get(), 1, 1);
        }
    }

    @Override
    public void end() {
        unregister();
    }

    @Override
    public int getDuration() {
        return 120;
    }

    @Override
    public LocaleMessage getMessage() {
        return LocaleMessage.of("A crate filled with supplies will drop from the sky in 1 minute at (x: " + coordX + " and z: " + coordZ + ")");
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (e.getEntity() == chicken || e.getEntity() == fallingBlock) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onChunkUnload(ChunkUnloadEvent e) {
        for (Entity entity : e.getChunk().getEntities()) {
            if (entity == chicken || entity == fallingBlock) {
                e.setCancelled(true);
                break;
            }
        }
    }
}
