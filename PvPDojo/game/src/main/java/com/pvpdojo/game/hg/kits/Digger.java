/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.kits.BlockInteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.util.bukkit.CC;

public class Digger extends Kit implements BlockInteractingKit {

    @Override
    public void activate(PlayerInteractEvent e) {
        if (Game.inst().getGame().getSession().getState() != SessionState.INVINCIBILITY) {
            if (e.getClickedBlock() != null && e.getPlayer().getItemInHand() != null && e.getPlayer().getItemInHand().getType() == getClickItem().getType()) {
                Player player = e.getPlayer();
                kitCooldown.use();
                player.sendMessage(CC.GREEN + "Digging hole...");
                if (player.getItemInHand().getAmount() > 1) {
                    player.getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
                } else {
                    player.setItemInHand(null);
                }
                Location mid = e.getClickedBlock().getLocation();
                PvPDojo.schedule(() -> {
                    for (int x = -2; x <= 2; x++) {
                        for (int z = -2; z <= 2; z++) {
                            for (int y = 0; y >= -5; y--) {
                                Block block = mid.clone().add(x, y, z).getBlock();
                                if (block.getType() != Material.BEDROCK) {
                                    BlockBreakEvent event = new BlockBreakEvent(block, player);
                                    Bukkit.getPluginManager().callEvent(event);
                                    if (!event.isCancelled()) {
                                        block.getWorld().playEffect(block.getLocation(), Effect.STEP_SOUND, block.getType());
                                        block.setType(Material.AIR);
                                    }
                                }
                            }
                        }
                    }
                }).createTask(20);
            }
        }
    }

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.DRAGON_EGG, 20);
    }

    @Override
    public int getCooldown() {
        return 5000;
    }

}
