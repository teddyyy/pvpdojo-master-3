/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.util;

import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.entity.TeamArenaEntity;

public class TeamUtil {

    public static void removeFromTeam(ArenaEntity user) {
        if (user.getTeam() != null) {
            user.getTeam().removeMember(user);
        }
    }

    public static void balanceTeams(TeamArenaEntity stackedTeam, TeamArenaEntity minorTeam) {
        int diff = stackedTeam.getMembers().size() - minorTeam.getMembers().size() - 1;
        for (int i = 0; i < diff; i++) {
            ArenaEntity stackedMember = stackedTeam.getMembers().get(0);
            removeFromTeam(stackedMember);
            minorTeam.addMember(stackedMember);
        }
    }

}
