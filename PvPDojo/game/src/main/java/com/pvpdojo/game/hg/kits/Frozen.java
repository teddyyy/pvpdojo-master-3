package com.pvpdojo.game.hg.kits;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.kits.util.KitUtil;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.EnumSet;

public class Frozen extends ComplexKit {

    private static final EnumSet<Material> SPEED_BLOCKS = EnumSet.of(Material.SNOW, Material.SNOW_BLOCK, Material.ICE,
            Material.PACKED_ICE);

    @Override
    public void onMove(PlayerMoveEvent e) {

        Player player = e.getPlayer();
        Location loc = player.getLocation();

        for (Material material : SPEED_BLOCKS) {
            if (loc.getBlock().getType() == material
                    || loc.getBlock().getRelative(BlockFace.DOWN).getType() == material) {
                player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 5, 0), true);
            }
        }

        Biome biome = loc.getBlock().getBiome();
        if (biome.equals(Biome.ICE_PLAINS) || biome.equals(Biome.COLD_TAIGA) || biome.equals(Biome.ICE_PLAINS_SPIKES)) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 5, 0), true);
        }

        if (player.getItemInHand().getType() == Material.SNOW_BALL) {
            Block legs = loc.getBlock();
            Block below = legs.getRelative(BlockFace.DOWN);
            Block below2 = below.getRelative(BlockFace.DOWN);

            if (legs.isEmpty() && below.isEmpty() && below2.isEmpty() || legs.getType() == Material.PACKED_ICE
                    || below.getType() == Material.PACKED_ICE || below2.getType() == Material.PACKED_ICE) {
                createFieldAt(player, below.getLocation(), true, false);
            }


        }

    }

    @EventHandler
    public void onProjectileHit(ProjectileHitEvent e) {
        if (e.getEntity() instanceof Snowball) {
            if (e.getEntity().getShooter() instanceof Player) {
                Player player = (Player) e.getEntity().getShooter();
                if (KitUtil.hasKit(HGUser.getUser(player), HGKit.FROZEN)) {
                    createFieldAt(player, e.getEntity().getLocation(), false, true);
                }
            }
        }
    }

    //Remake this because it's aids to look at
    private void createFieldAt(Player player, Location loc, boolean canRemove, boolean replaceWater) {
        boolean yeet = false;
        float radius = replaceWater ? 3 : 1.5f;

        for (float x = -radius; x <= radius; x++) {
            for (float z = -radius; z <= radius; z++) {

                Location l = loc.clone().add(x + 0.5, 0, z + 0.5);
                Block block = l.getBlock();
                boolean check = !replaceWater ? block.getType() == Material.AIR && block.getRelative(BlockFace.UP).isEmpty() : l.getBlock().getType() == Material.STATIONARY_WATER
                        || l.getBlock().getType() == Material.WATER;

                if (check) {
                    l.getBlock().setType(Material.PACKED_ICE);
                    PvPDojo.schedule(() -> l.getBlock().setType(Material.AIR)).createTask(20 * 30);

                    if (canRemove) {
                        yeet = true;
                    }

                }

            }
        }
        if (yeet) {
            if (player.getItemInHand().getAmount() > 1) {
                player.getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
            } else {
                player.setItemInHand(null);
            }
        }
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[]{new ItemStack(Material.SNOW_BALL, 64)};
    }
}
