/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Horse.Variant;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.util.bukkit.CC;

import net.minecraft.server.v1_7_R4.GenericAttributes;

public class Rider extends ComplexKit implements InteractingKit {

    private static final String META = "rider_meta";

    private List<ItemStack> lastDrops;
    private Horse horse = null;

    @Override
    public void click(Player player) {
        if (horse == null || horse.isDead() || !horse.isValid()) {
            horse = player.getWorld().spawn(player.getLocation(), Horse.class);
            horse.setMetadata(META, new FixedMetadataValue(Game.inst(), player.getUniqueId()));
            horse.setJumpStrength(0.85);
            horse.setAdult();
            horse.setVariant(Variant.HORSE);
            horse.setCarryingChest(true);
            horse.setMaxHealth(60);
            horse.setHealth(60);
            horse.setCanPickupItems(true);
            horse.setTamed(true);
            horse.setPassenger(player);
            horse.setOwner(player);
            horse.setVariant(Variant.SKELETON_HORSE);
            horse.getInventory().setSaddle(new ItemStack(Material.SADDLE));
//            horse.getInventory().setArmor(new ItemStack(Material.IRON_BARDING));
            NMSUtils.get(horse).getAttributeInstance(GenericAttributes.d).setValue(0.25);
            if (lastDrops != null) {
                lastDrops.forEach(drop -> horse.getInventory().addItem(drop));
                lastDrops = null;
            }
            player.sendMessage(CC.GREEN + "Your horse has been spawned");
        } else if (player.getLocation().distance(horse.getLocation()) >= 10) {
            horse.teleport(player.getLocation());
            player.sendMessage(CC.GREEN + "Your horse has been teleported to you");
        }
    }

    @EventHandler
    public void onDeath(EntityDeathEvent e) {
        if (e.getEntity().hasMetadata(META)) {
            e.getDrops().removeIf(item -> item.getType() == Material.SADDLE || item.getType() == Material.IRON_BARDING);
            lastDrops = new ArrayList<>(e.getDrops());
            e.getDrops().clear();

            Player owner = Bukkit.getPlayer((UUID) e.getEntity().getMetadata(META).get(0).value());
            if (owner != null) {
                HGUser.getUser(owner).getKit().getKitCooldown().use();
            }
        }
    }

    @EventHandler
    public void onMount(VehicleEnterEvent e) {
        if (e.getVehicle().hasMetadata(META) && Bukkit.getPlayer((UUID) e.getVehicle().getMetadata(META).get(0).value()) != e.getEntered()) {
            e.setCancelled(true);
        }
    }

    @Override
    public void activate(PlayerInteractEvent e) {}

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.SADDLE);
    }

    @Override
    public int getCooldown() {
        return 90000;
    }

}
