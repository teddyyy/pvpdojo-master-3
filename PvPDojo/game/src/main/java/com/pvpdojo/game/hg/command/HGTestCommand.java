/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.command;

import org.bukkit.entity.Player;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.game.hg.util.EquipmentUtil;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("hgtest")
public class HGTestCommand extends DojoCommand {

    @CatchUnknown
    @Default
    public void onHGTest(Player player) {
        player.sendMessage(String.valueOf(EquipmentUtil.getEquipmentPoints(player)));
    }

    @Override
    public Rank getRank() {
        return Rank.SERVER_ADMINISTRATOR;
    }
}
