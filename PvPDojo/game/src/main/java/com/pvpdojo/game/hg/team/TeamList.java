package com.pvpdojo.game.hg.team;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.session.HGRelogData;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.session.entity.ArenaEntity;

public class TeamList {
    // copied and adapted from com.pvpdojo.party.PartyList, parts are unnecessary as teams are only on one server

    private static Map<UUID, HGTeam> loadedTeams = new ConcurrentHashMap<>();

    public static Collection<HGTeam> getTeams() {
        return loadedTeams.values();
    }

    public static boolean contains(UUID party) {
        return loadedTeams.containsKey(party);
    }

    public static void addTeam(HGTeam team) {
        if (!loadedTeams.containsKey(team.getPartyId())) {
            loadedTeams.put(team.getPartyId(), team);
        }
    }

    public static void removeTeam(UUID partyUUID) {
        HGTeam team = loadedTeams.remove(partyUUID);
        if (team != null) {
            team.getUsers().stream().map(HG.inst().getSession()::getParticipant).filter(Objects::nonNull).forEach(user -> {
                if (user instanceof HGUser) {
                    ((HGUser) user).setHGTeam(null);
                    ((HGUser) user).updateForEveryOne();
                } else {
                    ((HGRelogData) user).setHGTeam(null);
                }
            });
        }
    }

    public static HGTeam getTeamById(UUID teamId) {
        if (teamId == null) {
            return null;
        }
        return loadedTeams.get(teamId);
    }

    public static HGTeam getTeamForUser(UUID uuid) {
        for (HGTeam team : loadedTeams.values()) {
            if (team.getUsers().contains(uuid)) {
                return team;
            }
        }
        return null;
    }

    public static boolean registerTeam(HGTeam team) {
        addTeam(team);
        return team.isValid();
    }

    public static HGTeam createTeam(UUID leader) {
        return createTeam(leader, Collections.emptyList());
    }

    public static HGTeam createTeam(UUID leader, Iterable<UUID> users) {
        return createTeam(leader, users, false);
    }

    public static HGTeam createTeam(UUID leader, Iterable<UUID> users, boolean temporary) {
        HGTeam team = new HGTeam(UUID.randomUUID(), getTeamCount() + 1);
        team.addUser(leader);
        team.setLeader(leader);
        team.setTemporary(temporary);
        for (UUID user : users) {
            team.addUser(user);
        }

        registerTeam(team);

        PvPDojo.schedule(() -> {
            for (UUID uuid : team.getUsers()) {
                ArenaEntity ae = HG.inst().getSession().getParticipant(uuid);
                if (ae != null) {
                    if (ae instanceof HGUser) {
                        ((HGUser) ae).setHGTeam(team);
                        ((HGUser) ae).updateForEveryOne();
                    } else {
                        ((HGRelogData) ae).setHGTeam(team);
                    }
                }
            }
            team.sendMessage("Team has been created");
        }).sync();

        return team;
    }

    public static void addTeamUser(HGTeam team, UUID userUUID) {
        synchronized (team) {
            if (team.addUser(userUUID)) {
                PvPDojo.schedule(() -> {
                    ArenaEntity ae = HG.inst().getSession().getParticipant(userUUID);
                    if (ae instanceof HGUser) {
                        ((HGUser) ae).setHGTeam(team);
                        ((HGUser) ae).updateForEveryOne();
                    } else {
                        ((HGRelogData) ae).setHGTeam(team);
                    }
                }).sync();
            }
        }
    }

    public static void removeTeamUser(HGTeam team, UUID userUUID) {
        synchronized (team) {
            if (team.removeUser(userUUID)) {
                PvPDojo.schedule(() -> {
                    ArenaEntity ae = HG.inst().getSession().getParticipant(userUUID);
                    if (ae instanceof HGUser) {
                        ((HGUser) ae).setHGTeam(null);
                        ((HGUser) ae).updateForEveryOne();
                    } else {
                        ((HGRelogData) ae).setHGTeam(null);
                    }
                }).sync();
                if (team.getLeader().equals(userUUID)) {
                    if (team.getUsers().isEmpty()) {
                        removeTeam(team.getPartyId());
                    } else {
                        team.setLeader(team.getUsers().iterator().next());
                    }
                }
            }
        }
    }

    synchronized public static int getTeamCount() {
        return loadedTeams.size();
    }
}
