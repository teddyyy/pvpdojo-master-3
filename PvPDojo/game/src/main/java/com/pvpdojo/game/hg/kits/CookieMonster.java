package com.pvpdojo.game.hg.kits;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.kits.ComplexKit;

public class CookieMonster extends ComplexKit {

    private int cookies;

    @Override
    public void onBreak(BlockBreakEvent e) {
        if (!e.isCancelled() && e.getBlock().getType() == Material.LONG_GRASS) {
            Player player = e.getPlayer();
            int cookies = player.getInventory().all(Material.COOKIE).size();
            double dropChance = (cookies < 32) ? 35 : 20;
            boolean seedsDrop = e.getBlock().getDrops().stream().anyMatch(items -> items.getType() == Material.SEEDS);

            if (PvPDojo.RANDOM.nextInt(100) < dropChance || seedsDrop) {
                e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(Material.COOKIE));
            }
            e.getBlock().setType(Material.AIR);
        }
    }

    @Override
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            ItemStack item = player.getItemInHand();
            int slot = player.getInventory().getHeldItemSlot();
            if (item.getType() == Material.COOKIE) {
                if (player.getHealth() == player.getMaxHealth()) {
                    cookies++;
                } else if (player.getHealth() < player.getMaxHealth()) {
                    player.setHealth(Math.min(player.getHealth() + 3, player.getMaxHealth()));
                    cookies = 0;
                } else if (player.getFoodLevel() < 20) {
                    player.setFoodLevel(Math.min(player.getFoodLevel() + 7, 20));
                    cookies = 0;
                }
                if (cookies >= 20) {
                    cookies = 0;
                    player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20 * 20, 1), true);
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 20, 0), true);
                    player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * 20, 1), true);
                    player.sendMessage(ChatColor.GREEN + "OMNOMOMOM");
                }

                if (item.getAmount() > 1) {
                    item.setAmount(player.getItemInHand().getAmount() - 1);
                } else
                    PvPDojo.schedule(() -> player.getInventory().setItem(slot, null)).nextTick();
            }
        }

    }

}