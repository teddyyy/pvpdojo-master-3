package com.pvpdojo.game.hg.kits.sidekits;

import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.ClickKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.util.bukkit.ItemUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;

import java.util.EnumSet;
import java.util.List;
import java.util.Map;

public class Salvager extends Kit {

    @Override
    public void onInteract(PlayerInteractEvent e) {
        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (e.getClickedBlock().getType() == Material.ANVIL && e.getPlayer().isSneaking()) {
                HGUser user = HGUser.getUser(e.getPlayer());
                if (user.getKit() != null) {
                    if (user.getKit() instanceof ClickKit) {
                        if (e.getPlayer().getItemInHand().getType() == ((ClickKit) user.getKit()).getClickItem().getType()) {
                            e.setCancelled(true);
                            return;
                        }
                    }
                    if (user.getKit().getStartItems().length != 0) {
                        for (ItemStack startItem : user.getKit().getStartItems()) {
                            if (e.getPlayer().getItemInHand().isSimilar(startItem)) {
                                e.setCancelled(true);
                                return;
                            }
                        }
                    }
                }

                ItemStack itemInHand = e.getPlayer().getItemInHand();
                itemInHand.setDurability((short) 0);

                EnumSet<Material> ARMOR = EnumSet.of(Material.IRON_HELMET, Material.IRON_CHESTPLATE, Material.IRON_LEGGINGS, Material.IRON_BOOTS, Material.DIAMOND_HELMET, Material.DIAMOND_CHESTPLATE, Material.DIAMOND_LEGGINGS, Material.DIAMOND_BOOTS);
                if (ItemUtil.SWORDS.contains(itemInHand.getType()) || ItemUtil.TOOLS.contains(itemInHand.getType()) || ARMOR.contains(itemInHand.getType()) || itemInHand.getType() == Material.MUSHROOM_SOUP) {
                    e.setCancelled(true);

                    if (itemInHand.getAmount() > 1) {
                        itemInHand.setAmount(itemInHand.getAmount() - 1);
                    } else {
                        e.getPlayer().setItemInHand(null);
                    }

                    Recipe recipe = Bukkit.getRecipesFor(itemInHand).get(0);
                    if (recipe instanceof ShapelessRecipe) {
                        List<ItemStack> ingredients = ((ShapelessRecipe) recipe).getIngredientList();
                        for (ItemStack itemStack : ((ShapelessRecipe) recipe).getIngredientList()) {
                            ItemUtil.addItemWhenFree(e.getPlayer(), itemStack);
                        }
                        return;
                    }
                    if (recipe instanceof ShapedRecipe) {
                        Map<Character, ItemStack> ingredients = ((ShapedRecipe) recipe).getIngredientMap();
                        for (ItemStack itemStack : ingredients.values()) {
                            ItemUtil.addItemWhenFree(e.getPlayer(), itemStack);
                        }
                        return;
                    }
                }
            }
        }
        if (e.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
            if (e.getClickedBlock().getType() == Material.ANVIL) {
                e.getClickedBlock().setType(Material.AIR);
                ItemUtil.addItemWhenFree(e.getPlayer(), new ItemStack(Material.ANVIL));
            }
        }
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[]{new ItemStack(Material.ANVIL)};
    }
}
