/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.game.kits.Kit;

public class Tank extends Kit {

    @Override
    public void onKillEntity(EntityDeathEvent e) {
        float power = 2F;

        if (e.getEntity() instanceof Player) {
            e.getEntity().getKiller().addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 5 * 20, 0));
            power = 4.0F;
        }

        List<ItemStack> drops = new ArrayList<>(e.getDrops());
        e.getDrops().clear();

        World w = e.getEntity().getWorld();
        Location loc = e.getEntity().getLocation();

        BukkitUtil.createExplosionByEntity(e.getEntity().getKiller(), loc, power, true, false);

        PvPDojo.schedule(() -> drops.stream().filter(Objects::nonNull).filter(stack -> stack.getType() != Material.AIR)
                                    .forEach(stack -> w.dropItemNaturally(loc, stack))).createTask(30);
    }

    @Override
    public void onDamage(EntityDamageEvent e) {
        if (e.getCause() == DamageCause.ENTITY_EXPLOSION || e.getCause() == DamageCause.BLOCK_EXPLOSION) {
            e.setCancelled(true);
        }
    }

}
