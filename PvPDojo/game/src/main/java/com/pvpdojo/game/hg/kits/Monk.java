/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.kits.EntityInteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.CC;

public class Monk extends Kit implements EntityInteractingKit {

    private boolean monkedArmour = false;

    @Override
    public void activate(PlayerInteractEntityEvent e) {
        Player target = (Player) e.getRightClicked();

        if (monkedArmour) {
            monkedArmour = false;
        }

        if (monkArmour(target)) {
            monkedArmour = true;
        }

        int index = PvPDojo.RANDOM.nextInt(27) + 9;
        ItemStack random = target.getInventory().getItem(index);

        target.getInventory().setItem(index, target.getItemInHand());
        target.getInventory().setItemInHand(random);

        e.getPlayer().sendMessage(CC.GREEN + "Monked!");
        target.sendMessage(CC.RED + "You have been monked!");
        kitCooldown.use();
    }

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.BLAZE_ROD);
    }

    @Override
    public int getCooldown() {
        if (monkedArmour) {
            return 55_000;
        }
        return 11_000;
    }

    private boolean monkArmour(Player target) {
        int size = getArmourSize(target);
        int validSpot = getValidSpot(target);

        if(size == 0) {
            return false;
        }

        for (ItemStack item : target.getInventory().getArmorContents()) {
            if (item == null || item.getType() == Material.ANVIL) {
                continue;
            }

            int random = PvPDojo.RANDOM.nextInt(size) + 1;
            int index = validSpot + random;

            ItemStack monkedItem = target.getInventory().getItem(index);

            target.getInventory().setItem(index, target.getItemInHand());
            target.getInventory().setItemInHand(monkedItem);

            break;
        }
        return size > 0;
    }

    private int getArmourSize(Player player) {
        int size = 0;

        for (ItemStack item : player.getInventory().getArmorContents()) {
            if (item == null || item.getType() == Material.AIR || item.getType() == Material.ANVIL) {
                continue;
            }
            size++;
        }
        return size;
    }

    private int getValidSpot(Player player) {
        int spot = 35;
        EntityEquipment equip = player.getEquipment();

        if (equip.getBoots() != null) return 35;
        if (equip.getLeggings() != null) return 36;
        if (equip.getChestplate() != null && equip.getChestplate().getType() != Material.ANVIL) return 37;
        if (equip.getHelmet() != null && equip.getHelmet().getType() != Material.ANVIL) return 38;

        return spot;
    }

}
