/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.session;

import com.pvpdojo.game.hg.team.HGTeam;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.session.KitGameRelogData;
import com.pvpdojo.userdata.User;

public class HGRelogData extends KitGameRelogData {

    private int kills;
    private boolean respawned;
    private HGTeam team;

    public HGRelogData(HGUser user) {
        super(user);
        this.kills = user.getKills();
        this.respawned = user.isRespawned();
        this.team = user.getHGTeam();
    }

    @Override
    public void apply(User user) {
        super.apply(user);
        ((HGUser) user).setKills(kills);
        ((HGUser) user).setRespawned(respawned);
        ((HGUser) user).setHGTeam(team);
    }

    public int getKills() {
        return kills;
    }

    public HGTeam getHGTeam() {
        return team;
    }

    public void setHGTeam(HGTeam team) {
        this.team = team;
    }
}
