/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.session;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.lang.HGMessageKeys;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.hg.util.FeastUtil;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.game.session.StageHandler;
import com.pvpdojo.game.session.StagedSession;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.util.Countdown;
import com.pvpdojo.util.bukkit.BlockUtil;

import lombok.Getter;

public class DefaultFeast extends Feast {

    @Getter
    private Countdown feastCountdown;

    public DefaultFeast(StagedSession session) {
        super(session);
    }

    @Override
    public StageHandler init() {
        setTimeSeconds(HG.inst().getSettings().getSecondsTillFeast());
        return super.init();
    }

    @Override
    public void run() {
        super.run();

        if (getTimeSeconds() >= HG.inst().getSettings().getSecondsTillBonusFeast()) {
            advance();
        }
    }

    @Override
    public StageHandler getNextHandler() {
        return new BonusFeast(session);
    }

    @Override
    public void spawnFeast() {
        Location feastLocation = FeastUtil.findFeastLocation(session.getArena().getWorld(), 100);
        feastRegion = FeastUtil.spawnFeastPlatform(feastLocation);
        HG.inst().setFeastLocation(feastLocation);

        TrackerUtil.getIngame().filter(players -> KitUtil.hasKit(HGUser.getUser(players), HGKit.ANCHOR)).forEach(players -> {
        	players.getInventory().setHelmet(null);
        	players.getInventory().setChestplate(null);
        });
        
        feastCountdown = new Countdown(300)
                .userCounter(i -> session.broadcast(HGMessageKeys.FEAST, "{time}", Countdown.getUserFriendlyNumber(i), "{location}", BlockUtil.asString(feastLocation.getBlock())))
                .finish(() -> {
                    FeastUtil.spawnFeastChests(feastLocation);
                    // Allow building again
                    feastRegion = null;
                }).start();

    }
}
