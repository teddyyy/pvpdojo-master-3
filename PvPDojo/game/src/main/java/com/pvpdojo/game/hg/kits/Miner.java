/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

public class Miner extends Kit {

    public static final EnumSet<Material> ORES = EnumSet.of(Material.COAL_ORE, Material.IRON_ORE, Material.GOLD_ORE, Material.REDSTONE_ORE, Material.DIAMOND_ORE, Material.LAPIS_ORE, Material.EMERALD_ORE);

    private static final EnumSet<BlockFace> FACES = EnumSet.of(BlockFace.SOUTH, BlockFace.NORTH, BlockFace.EAST, BlockFace.WEST, BlockFace.SOUTH_EAST,
            BlockFace.SOUTH_WEST, BlockFace.NORTH_EAST, BlockFace.NORTH_WEST, BlockFace.DOWN, BlockFace.UP);

    private boolean inProgress;

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[]{new ItemBuilder(Material.STONE_PICKAXE).enchant(Enchantment.DIG_SPEED, 3).unbreakable().build(), new ItemStack(Material.APPLE, 5)};
    }

    @Override
    public void onBreak(BlockBreakEvent e) {
        if (!inProgress && ORES.contains(e.getBlock().getType())) {
            inProgress = true;
            recursiveBreak(e.getBlock(), e.getPlayer(), new HashSet<>());
            inProgress = false;
        }
    }

    @Override
    public void onConsume(PlayerItemConsumeEvent e) {
        if (e.getItem().getType() == Material.APPLE) {
            e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 90*20, 1));
        }
    }

    public void recursiveBreak(Block block, Player player, Set<Block> broken) {
        if (broken.contains(block)) {
            return;
        }

        broken.add(block);
        if (ORES.contains(block.getType())) {
            breakBlock(block, player);
            for (BlockFace face : FACES) {
                recursiveBreak(block.getRelative(face), player, broken);
            }
        }
    }

    public void breakBlock(Block block, Player player) {
        BlockBreakEvent event = new BlockBreakEvent(block, player);
        Bukkit.getPluginManager().callEvent(event);
        if (!event.isCancelled()) {
            block.breakNaturally();
        }
    }
}

