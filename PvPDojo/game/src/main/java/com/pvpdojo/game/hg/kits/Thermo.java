/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;

import java.util.EnumMap;
import java.util.HashSet;
import java.util.Set;

public class Thermo extends Kit {

    private static final EnumMap<Material, Material> LIQUID_SWITCH = new EnumMap<>(Material.class);

    static {
        LIQUID_SWITCH.put(Material.WATER, Material.LAVA);
        LIQUID_SWITCH.put(Material.STATIONARY_WATER, Material.STATIONARY_LAVA);
        LIQUID_SWITCH.put(Material.LAVA, Material.WATER);
        LIQUID_SWITCH.put(Material.STATIONARY_LAVA, Material.STATIONARY_WATER);
        LIQUID_SWITCH.put(Material.WATER_BUCKET, Material.LAVA_BUCKET);
        LIQUID_SWITCH.put(Material.LAVA_BUCKET, Material.WATER_BUCKET);
    }

    @Override
    public void onPlace(BlockPlaceEvent e) {
        if (e.getBlockPlaced().getType() == getStartItems()[0].getType()) {
            e.setCancelled(true);

            if (LIQUID_SWITCH.containsKey(e.getBlockReplacedState().getType())) {
                if (kitCooldown.isOnCooldown()) {
                    kitCooldown.sendCooldown(User.getUser(e.getPlayer()));
                    return;
                }

                Set<BlockState> workSet = new HashSet<>();

                floodFill(e.getBlockReplacedState(), workSet);

                if (workSet.size() > 400) {
                    e.getPlayer().sendMessage(CC.RED + "This is too much! It can only change up to 400 blocks at a time");
                    return;
                }

                kitCooldown.use();

                for (BlockState block : workSet) {
                    block.getBlock().setType(Material.AIR);
                }

                for (BlockState block : workSet) {
                    byte data = block.getRawData();
                    block.setType(LIQUID_SWITCH.get(block.getType()));
                    block.setRawData(data);
                    block.update(true);
                }

            }
        }
    }

    @Override
    public void onInteractEntity(PlayerInteractEntityEvent e) {

        if (!(e.getRightClicked() instanceof Player)) {
            return;
        }

        Player target = (Player) e.getRightClicked();

        if (e.getPlayer().getItemInHand().getType() != getStartItems()[0].getType()) {
            return;
        }

        if (kitCooldown.isOnCooldown()) {
            kitCooldown.sendCooldown(User.getUser(e.getPlayer()));
            return;
        }

        if (replaceBuckets(target)) {
            kitCooldown.use();
        }
    }

    public void floodFill(BlockState block, Set<BlockState> workSet) {
        if (workSet.contains(block) || !LIQUID_SWITCH.containsKey(block.getType())) {
            return;
        }

        if (workSet.size() > 401) {
            return;
        }

        workSet.add(block);

        floodFill(block.getBlock().getRelative(BlockFace.NORTH).getState(), workSet);
        floodFill(block.getBlock().getRelative(BlockFace.SOUTH).getState(), workSet);
        floodFill(block.getBlock().getRelative(BlockFace.EAST).getState(), workSet);
        floodFill(block.getBlock().getRelative(BlockFace.WEST).getState(), workSet);
        floodFill(block.getBlock().getRelative(BlockFace.UP).getState(), workSet);
        floodFill(block.getBlock().getRelative(BlockFace.DOWN).getState(), workSet);

    }

    public boolean replaceBuckets(Player player) {
        boolean bool = false;

        for (ItemStack items : player.getInventory().getContents()) {
            if (items == null || items.getType() == Material.AIR)
                continue;
            if (items.getType() == Material.WATER_BUCKET || items.getType() == Material.LAVA_BUCKET) {
                items.setType(LIQUID_SWITCH.get(items.getType()));
                bool = true;
            }
        }
        return bool;
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[]{new ItemStack(Material.DAYLIGHT_DETECTOR)};
    }

    @Override
    public int getCooldown() {
        return 8000;
    }
}
