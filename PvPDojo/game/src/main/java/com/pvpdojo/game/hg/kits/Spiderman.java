/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.BlockUtil;

public class Spiderman extends ComplexKit {

    private static final String SPIDER_WEB = "spider_web";
    private boolean spiderManFly;

    @Override
    public void onInteract(PlayerInteractEvent e) {
        if (e.getItem() != null && e.getItem().getType() == Material.EGG && (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK)) {
            if (getKitCooldown().isOnCooldown()) {
                getKitCooldown().sendCooldown(User.getUser(e.getPlayer()));
                e.setCancelled(true);
            }
        }

    }

    @Override
    public void onProjectileLaunch(ProjectileLaunchEvent e) {
        if (e.getEntity().getType() == EntityType.EGG) {
            e.getEntity().setMetadata(SPIDER_WEB, new FixedMetadataValue(Game.inst(), null));
            getKitCooldown().use();
        }
    }

    @Override
    public void onMove(PlayerMoveEvent e) {
        Location sub = e.getTo().clone().subtract(e.getFrom());
        if (NMSUtils.touches(e.getTo().getWorld(), NMSUtils.get(e.getPlayer()).boundingBox.clone().d(sub.getX(), sub.getY(), sub.getZ()), net.minecraft.server.v1_7_R4.Material.WEB)) {
            if (!spiderManFly) {
                e.getPlayer().setAllowFlight(true);
                e.getPlayer().setFlying(true);
                spiderManFly = true;
            }
        } else if (spiderManFly) {
            spiderManFly = false;
            e.getPlayer().setAllowFlight(false);
        }
    }

    @Override
    public void onLeave(Player player) {
        if (spiderManFly) {
            spiderManFly = false;
            player.setAllowFlight(false);
        }
    }

    @EventHandler
    public void onSpiderWeb(ProjectileHitEvent e) {
        if (e.getEntity().hasMetadata(SPIDER_WEB) && Game.inst().getGame().getSession().getState() != SessionState.INVINCIBILITY) {
            BlockUtil.getNearbyBlocks(e.getEntity().getLocation(), 3).stream()
                     .filter(Block::isEmpty)
                     .forEach(block -> {
                         block.setType(Material.WEB);
                         PvPDojo.schedule(() -> {
                             if (block.getType() == Material.WEB) {
                                 block.setType(Material.AIR);
                             }
                         }).createTask(10 * 20);
                     });
        }
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] { new ItemStack(Material.EGG, 20) };
    }

    @Override
    public int getCooldown() {
        return 30_000;
    }
}
