/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.util;

import static com.pvpdojo.util.StreamUtils.negate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.ctm.CTM;
import com.pvpdojo.game.ctm.kits.list.CTMKit;
import com.pvpdojo.game.ctm.menu.CTMTeamSelectionMenu;
import com.pvpdojo.game.ctm.session.CTMSession;
import com.pvpdojo.game.ctm.userdata.CTMUser;
import com.pvpdojo.game.kits.KitHolder;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.game.util.TeamUtil;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.entity.TeamArenaEntity;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.PlayerUtil;


public class CTMUtil {

    public static final double MAX_DAMAGE = 50D;

    private static final ItemStack[] CTM_ARMOR = new ItemStack[] { new ItemStack(Material.IRON_BOOTS), new ItemStack(Material.IRON_LEGGINGS), new ItemStack(Material.IRON_CHESTPLATE),
            new ItemStack(Material.IRON_HELMET) };
    public static final ItemStack[] CTM_CONTENTS = new ItemStack[36];

    private static final List<ItemStack> ITEMS = new ArrayList<>();

    static {

        CTM_CONTENTS[0] = new ItemBuilder(Material.IRON_SWORD).enchant(Enchantment.DAMAGE_ALL, 1).build();
        CTM_CONTENTS[1] = new ItemStack(Material.LOG, 64);
        CTM_CONTENTS[8] = new ItemStack(Material.WATER_BUCKET);
        CTM_CONTENTS[16] = new ItemStack(Material.IRON_AXE);
        CTM_CONTENTS[17] = new ItemStack(Material.IRON_PICKAXE);

        ITEMS.add(new ItemBuilder(Material.DIAMOND_HELMET).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build());
        ITEMS.add(new ItemBuilder(Material.DIAMOND_CHESTPLATE).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build());
        ITEMS.add(new ItemBuilder(Material.DIAMOND_LEGGINGS).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build());
        ITEMS.add(new ItemBuilder(Material.DIAMOND_BOOTS).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build());

        ITEMS.add(new ItemStack(Material.BOW));
        ITEMS.add(new ItemStack(Material.ARROW, 7));
        ITEMS.add(new ItemStack(Material.TNT, 4));

        ITEMS.add(new ItemStack(Material.MUSHROOM_SOUP, 6));
        ITEMS.add(new ItemStack(Material.ENDER_PEARL));
        ITEMS.add(new ItemBuilder(Material.DIAMOND_SWORD).enchant(Enchantment.DAMAGE_ALL, 1).build());

        Potion poison = new Potion(PotionType.POISON, 2);
        poison.setSplash(true);

        Potion damage = new Potion(PotionType.INSTANT_DAMAGE, 2);
        damage.setSplash(true);

        ITEMS.add(poison.toItemStack(1));
        ITEMS.add(damage.toItemStack(1));
    }

    public static void refillMinifeasts(Collection<Location> miniFeasts) {
        for (Location feast : miniFeasts) {
            feast.getBlock().setType(Material.DROPPER);
            InventoryHolder dropper = (InventoryHolder) feast.getBlock().getState();
            dropper.getInventory().clear();
            for (int i = 0; i < dropper.getInventory().getSize(); i++) {
                dropper.getInventory().setItem((PvPDojo.RANDOM.nextInt(dropper.getInventory().getSize())), ITEMS.get(PvPDojo.RANDOM.nextInt(ITEMS.size())));
            }
        }
    }

    public static void applyItems(Player player) {
        player.getInventory().setContents(CTM_CONTENTS);
        player.getInventory().setArmorContents(CTM_ARMOR);

        KitUtil.applyKitItems(CTMUser.getUser(player));

        PlayerUtil.refillSoup(player, 31);
    }

    public static boolean isCapturing(UUID uuid) {
        return CTM.inst().getSession().getRunningCaptures().containsKey(uuid);
    }

    public static double getDamage(UUID uuid) {
        Map<UUID, Double> damageMap = CTM.inst().getSession().getDamageMap();
        return damageMap.getOrDefault(uuid, 0D);
    }

    public static void addDamage(EntityDamageEvent e, Player damager) {
        Map<UUID, Double> damageMap = CTM.inst().getSession().getDamageMap();

        if (damageMap.containsKey(damager.getUniqueId())) {
            CTMUser user = CTMUser.getUser(damager);

            if (user.getKitHolders().stream().map(KitHolder::getKitProvider).anyMatch(CTMKit.BOOSTER::equals)) {

                for (ArenaEntity entity : user.getTeam().getMembers()) {
                    if (entity != user && entity.getPlayer() != null
                            && CTMUser.getUser(entity.getPlayer()).getKitHolders().stream().map(KitHolder::getKitProvider).anyMatch(negate(CTMKit.BOOSTER::equals))) {
                        addDamage(new EntityDamageEvent(e.getEntity(), e.getCause(), e.getFinalDamage() / 2), entity.getPlayer());
                    }
                }
                return;
            }

            double damage = Math.min(e.getFinalDamage(), ((Player) e.getEntity()).getHealth());
            damageMap.put(damager.getUniqueId(), Math.min(MAX_DAMAGE, damageMap.get(damager.getUniqueId()) + damage / 2));

            // Update their scoreboard immediately
            CTMUser.getUser(damager).updateScoreBoard();
        }
    }

    public static void removeDamage(UUID uuid, double remove) {
        double previous = getDamage(uuid);
        if (remove > previous) {
            resetDamage(uuid);
        } else {
            CTM.inst().getSession().getDamageMap().put(uuid, previous - remove);
        }
        CTMUser.getUser(uuid).updateScoreBoard();
    }

    public static void resetDamage(UUID uuid) {
        CTM.inst().getSession().getDamageMap().put(uuid, 0D);
        CTMUser.getUser(uuid).updateScoreBoard();
    }

    public static void handleTeamSelection(Player player) {
        User user = User.getUser(player);
        CTMSession session = CTM.inst().getSession();

        new CTMTeamSelectionMenu(player, selection -> {

            Collection<User> toAdd = Collections.singleton(user);
            TeamArenaEntity team = selection.getTeam().get();

            // Let's handle party shit
            if (user.getParty() != null && user.getParty().getLeader().equals(user.getUUID())) {
                List<User> partyUsers = user.getParty().getUsers().stream().map(User::getUser).filter(Objects::nonNull).collect(Collectors.toList());

                if (partyUsers.stream().map(User::getRank).allMatch(rank -> rank.inheritsRank(Rank.GREENBELT))) {
                    int maxPerTeam = CTM.inst().getSession().getArena().getMaxPlayers() / 2;

                    if (team.getMembers().size() + partyUsers.size() <= maxPerTeam) {
                        toAdd = partyUsers;
                        user.getParty().sendMessage(CC.GREEN + "We could assign all your party members to this team");
                    } else {
                        user.getParty().sendMessage(CC.RED + "This team would be overfilled by the party");
                    }
                } else {
                    user.getParty().sendMessage(CC.RED + "All players must have at least Greenbelt to join a team as a party");
                }
            }

            toAdd.forEach(TeamUtil::removeFromTeam);
            toAdd.forEach(team::addMember);
            toAdd.forEach(session::prepareEntity);
        }).open();
    }

}
