package com.pvpdojo.game.hg.kits.sidekits;

import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.kits.Kit;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;


public class Sonic extends Kit {

    private Player lastDamaged;
    private int lastHit = 0;
    private boolean hasSonicSpeed = false;

    @Override
    public void onMove(PlayerMoveEvent e) {
        int range = 30;

        Vector handle = e.getPlayer().getEyeLocation().getDirection();
        handle.multiply(1.25F);

        if (lastDamaged != null && e.getPlayer().getLocation().distance(lastDamaged.getLocation()) <= range) {
            if (e.getPlayer().getLocation().distanceSquared(lastDamaged.getLocation()) >= e.getPlayer().getLocation().add(handle.clone().multiply(5)).distanceSquared(lastDamaged.getLocation())) {
                if (HG.inst().getSession().getCurrentHandler().getTimeSeconds() - lastHit >= 5) {
                    if (!e.getPlayer().hasPotionEffect(PotionEffectType.SPEED) || hasSonicSpeed) {
                        hasSonicSpeed = true;
                        e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 5 * 20, 2), true);
                        return;
                    }
                }
            }
        }

        hasSonicSpeed = false;
        e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 0, 2), true);
    }

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player) {
            lastDamaged = (Player) e.getEntity();
        }
    }

    @Override
    public void onPlayerDamagedByPlayer(EntityDamageByEntityEvent e) {
        if ((Player) e.getDamager() == lastDamaged) {
            lastHit = HG.inst().getSession().getCurrentHandler().getTimeSeconds();
        }
    }
}
