/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game;

import co.aikar.commands.ConditionFailedException;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.UserList;
import com.pvpdojo.command.general.NullSeeKitCommand;
import com.pvpdojo.command.lib.DojoCommandManager;
import com.pvpdojo.extension.DojoServer;
import com.pvpdojo.game.cloud.GenericCloudResponder;
import com.pvpdojo.game.command.GameSettingsCommand;
import com.pvpdojo.game.command.GameTimeCommand;
import com.pvpdojo.game.command.SeeKitCommand;
import com.pvpdojo.game.extension.GameExtension;
import com.pvpdojo.game.kits.listener.KitListener;
import com.pvpdojo.game.listener.DefaultListener;
import com.pvpdojo.game.session.StagedSession;
import com.pvpdojo.game.settings.GameSettings;
import com.pvpdojo.game.userdata.GameUser;
import com.pvpdojo.map.DojoMap;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.network.SessionInfo;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.PlayerUtil;
import org.bukkit.Bukkit;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

public class Game<T extends GameUser> extends DojoServer<T> {

    private static Game instance;

    public static Game<?> inst() {
        return instance;
    }

    private GameExtension<? extends StagedSession, ? extends GameSettings, T> game;
    private GameProperties gameProperties = new GameProperties();
    private int gameId;

    private KitListener kitListener = new KitListener();
    private Set<UUID> playerToForceJoin = new HashSet<>();
    private Set<UUID> participants = new HashSet<>();

    @Override
    public ServerType getServerType() {
        return ServerType.GAME;
    }

    @Override
    public void onLoad() {
        instance = this;
        PvPDojo.LANG.addBundleClassLoader(getClassLoader());
        PvPDojo.LANG.addMessageBundles("pvpdojo-game");
        gameId = BungeeSync.getNextGameId();
        deleteFolder("world");
        readGameProperties();
        game.preLoad();
        super.onLoad();
    }

    @Override
    public void start() {
        new DefaultListener().register();
        clearRegions(Bukkit.getWorlds().get(0));
        game.start();
    }

    @Override
    public void postLoad() {
        loadCommands();
        game.postLoad();
        game.getSession().start();

        GenericCloudResponder.inst().start();

        Iterator<UUID> itr = playerToForceJoin.iterator();
        if (itr.hasNext()) {
            PvPDojo.schedule(() -> PlayerUtil.sendHere(itr.next())).createTimer(1, playerToForceJoin.size());
        }
    }

    @Override
    public void onDisable() {
        if (game.getSession().getState() != SessionState.FINISHED) {
            game.getSession().end(ArenaEntity.EVERYONE);
        }
        GenericCloudResponder.inst().shutdown();
    }

    @Override
    public DojoMap setupMap() {
        return game.setupMap();
    }

    @Override
    public UserList<T> createUserList() {
        return game.createUserList();
    }

    public GameExtension getGame() {
        return game;
    }

    public boolean isPrivate() {
        return gameProperties.getHost() != null || gameProperties.isPrivateGame();
    }

    public UUID getHost() {
        return gameProperties.getHost();
    }

    public void setGame(GameExtension<? extends StagedSession, ? extends GameSettings, T> game) {
        this.game = game;
    }

    public GameProperties getGameProperties() {
        return gameProperties;
    }

    public void enableKits() {
        kitListener.register(this);
        DojoCommandManager.get().registerCommand(new SeeKitCommand(), true);
    }

    public void disableKits() {
        kitListener.unregister();
        DojoCommandManager.get().registerCommand(new NullSeeKitCommand(), true);
    }

    public void loadCommands() {
        DojoCommandManager.get().registerCommand(new GameTimeCommand());
        DojoCommandManager.get().registerCommand(new GameSettingsCommand());

        DojoCommandManager.get().getCommandConditions().addCondition("state_pregame", context -> {
            if (game.getSession().getState() != SessionState.PREGAME) {
                throw new ConditionFailedException("Game already started");
            }
        });
        DojoCommandManager.get().getCommandConditions().addCondition(User.class, "game_settings", (context, execContext, value) -> {
            if (!value.getUUID().equals(getHost()) && !value.getRank().inheritsRank(Rank.TRIAL_MODERATOR)) {
                throw new ConditionFailedException("Insufficient permissions");
            }
        });
    }

    public void readGameProperties() {
        gameProperties.readProperties(this);
    }

    public void addPlayer(UUID uuid) {
        playerToForceJoin.add(uuid);
    }

    public void addParticipant(UUID uuid) {
        addPlayer(uuid);
        participants.add(uuid);
    }

    public Set<UUID> getParticipants() {
        return participants;
    }

    @Override
    public String getSubdomain() {
        return BungeeSync.getSubdomainById(gameId);
    }

    public SessionInfo createSessionInfo() {
        StagedSession sess = getGame().getSession();
        GameSettings settings = getGame().getSettings();
        return new SessionInfo(sess.getType(), sess.getState(), sess.getCurrentHandler().getTimeSeconds(), settings.getMaxPlayers(), sess.getPlayersLeft(), sess.getArena().getName(), gameId);
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }
}
