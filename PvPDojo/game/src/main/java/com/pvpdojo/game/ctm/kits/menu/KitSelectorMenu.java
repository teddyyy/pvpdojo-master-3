/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.kits.menu;

import org.bukkit.entity.Player;

import com.pvpdojo.game.ctm.CTM;
import com.pvpdojo.game.ctm.kits.list.CTMKit;
import com.pvpdojo.game.ctm.userdata.CTMUser;
import com.pvpdojo.game.ctm.util.CTMUtil;
import com.pvpdojo.menu.SortType;
import com.pvpdojo.menu.SortedInventoryMenu;
import com.pvpdojo.menu.SortedItem;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class KitSelectorMenu extends SortedInventoryMenu {

    public KitSelectorMenu(Player player) {
        super(player, "Choose your kit", 54, SortType.ALPHABETICAL);
        setCloseable(false);
        setUpdateInterval(1);
    }

    @Override
    public void redraw() {
        super.redraw();

        CTMUser user = CTMUser.getUser(getPlayer());

        for (CTMKit kit : CTMKit.values()) {

            boolean occupied = !CTM.inst().getSession().canSelectKit(user.getTeam(), kit);
            ItemBuilder itemBuilder = new ItemBuilder(kit.getIcon(user));
            if (occupied) {
                itemBuilder.appendLore("", CC.RED + "[TAKEN]");
            }

            SortedItem item = new SortedItem(itemBuilder.build(), occupied ? null : type -> {
                if (CTM.inst().getSession().canSelectKit(user.getTeam(), kit)) {
                    CTM.inst().getSession().getOccupiedKits().get(user.getTeam()).add(kit);
                    user.setKit(kit);
                    if (CTM.inst().getSession().getState() == SessionState.STARTED) {
                        CTMUtil.applyItems(getPlayer());
                    }
                    closeSafely();
                }
            });
            item.setSortName(kit.name());
            addSortItem(item);
        }

    }
}
