/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.BlockIterator;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.kits.Kit;

public class Blink extends Kit implements InteractingKit {

    private int uses;

    @Override
    public void activate(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        BlockIterator iterator;
        try {
            iterator = new BlockIterator(player, 7);
        } catch (IllegalStateException ex) {
            // What the fack?
            return;
        }

        Block lastBlock = null;

        while (iterator.hasNext()) {
            lastBlock = iterator.next();
        }

        if (lastBlock == null) {
            return;
        }

        Block finalBlock = lastBlock;

        if (lastBlock.getType() == Material.AIR) {
            lastBlock.setType(Material.LEAVES);
            PvPDojo.schedule(() -> finalBlock.setType(Material.AIR)).createTask(10);
        }

        Location location = lastBlock.getLocation().add(0, 2, 0);
        location.setYaw(player.getLocation().getYaw());
        location.setPitch(player.getLocation().getPitch());

        player.teleport(location);

        if (++uses > 2) {
            uses = 0;
            kitCooldown.use();
        }
    }

    @Override
    public void click(Player player) {}

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.NETHER_STAR);
    }

    @Override
    public int getCooldown() {
        return 25000;
    }
}
