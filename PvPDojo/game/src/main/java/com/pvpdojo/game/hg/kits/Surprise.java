/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.kits.KitHolder;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.game.userdata.kit.KitGameUser;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.StringUtils;

public class Surprise extends Kit {

    private boolean kitChosen;

    @Override
    public void onTick(KitGameUser user) {
        if (!kitChosen && HG.inst().getSession().getState() != SessionState.PREGAME) {
            HGKit kit = HGKit.values()[PvPDojo.RANDOM.nextInt(HGKit.values().length)];
            if (kit != HGKit.SURPRISE && forbiddenKits(kit)) {
                kitChosen = true;
                user.sendMessage(CC.GREEN + "You are now " + StringUtils.getEnumName(kit));
                user.sendMessage(LocaleMessage.of(kit.getDescription()).transformer(str -> CC.GREEN + CC.stripColor(str)));
                user.getKitHolders().add(0, new KitHolder<>(kit, kit.getSupplier().get()));
                KitUtil.applyKitItems(user);
            }
        }
    }

    private boolean forbiddenKits(HGKit kit) {
        return kit.isEnabled() && kit != HGKit.PACIFIST && kit != HGKit.CAPITALIST;
    }
}
