package com.pvpdojo.game.hg.kits.sidekits;

import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.ItemUtil;
import org.bukkit.Material;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

public class Noob extends Kit {

    private boolean invOpen = false;

    @Override
    public void onDrop(PlayerDropItemEvent e) {
        Material material = e.getItemDrop().getItemStack().getType();
        if (ItemUtil.SWORDS.contains(material)) {
            int swords = 0;
            for (ItemStack itemStack : e.getPlayer().getInventory()) {
                if (itemStack != null) {
                    if (ItemUtil.SWORDS.contains(itemStack.getType())) {
                        swords++;
                    }
                }
            }

            if (!e.getPlayer().isSneaking() && swords < 2) {
                e.setCancelled(true);
            }
        }
    }

    @Override
    public void onDamage(EntityDamageEvent e) {
        if (invOpen) {
            e.setDamage(e.getDamage() * 0.5);
        }
    }

    @Override
    public void onInvOpen(InventoryOpenEvent e) {
        invOpen = true;
    }

    @Override
    public void onInvClose(InventoryCloseEvent e) {
        invOpen = false;
    }
}
