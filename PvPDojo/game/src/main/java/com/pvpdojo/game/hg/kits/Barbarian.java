/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.IntSet;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemUtil;

public class Barbarian extends Kit {

    private static final IntSet UPGRADE = new IntSet();

    static {
        UPGRADE.addAll(1, 2, 4, 5, 7, 8, 10, 12);
    }

    private int kills;

    @Override
    public void onKill(PlayerDeathEvent e) {
        ItemStack inhand = e.getEntity().getKiller().getItemInHand();

        if (ItemUtil.SWORDS.contains(inhand.getType())) {
            if (inhand.getItemMeta().spigot().isUnbreakable()) {
                if (UPGRADE.contains(++kills)) {
                    e.getEntity().getKiller().getInventory().setItemInHand(getNextSword(inhand));
                }
            }
        }
    }

    @Override
    public void onDrop(PlayerDropItemEvent e) {
        if (ItemUtil.SWORDS.contains(e.getItemDrop().getItemStack().getType()) && e.getItemDrop().getItemStack().hasItemMeta()
                && e.getItemDrop().getItemStack().getItemMeta().spigot().isUnbreakable()) {
            e.setCancelled(true);
        }
    }

    @Override
    public void onDeath(PlayerDeathEvent e) {
        e.getDrops().removeIf(stack -> ItemUtil.SWORDS.contains(stack.getType()) && stack.hasItemMeta() && stack.getItemMeta().spigot().isUnbreakable());
    }

    public ItemStack getNextSword(ItemStack currentSword) {
        Material material = currentSword.getType();

        switch (material) {
            case WOOD_SWORD:
                if (currentSword.getEnchantments().containsKey(Enchantment.DAMAGE_ALL)) {
                    material = Material.IRON_SWORD;
                    currentSword.removeEnchantment(Enchantment.DAMAGE_ALL);
                } else {
                    currentSword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
                }
                break;
            case STONE_SWORD:
                if (currentSword.getEnchantments().containsKey(Enchantment.DAMAGE_ALL)) {
                    material = Material.DIAMOND_SWORD;
                    currentSword.removeEnchantment(Enchantment.DAMAGE_ALL);
                }
                break;
            case IRON_SWORD:
                if (currentSword.getEnchantments().containsKey(Enchantment.DAMAGE_ALL)) {
                    material = Material.DIAMOND_SWORD;
                } else {
                    material = Material.STONE_SWORD;
                    currentSword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
                }
                break;
            case DIAMOND_SWORD:
                if (currentSword.getEnchantments().containsKey(Enchantment.DAMAGE_ALL)) {
                    currentSword.addEnchantment(Enchantment.FIRE_ASPECT, 1);
                } else {
                    material = Material.IRON_SWORD;
                    currentSword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
                }
                break;
        }

        return new ItemBuilder(currentSword).unbreakable().type(material).build();
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] { new ItemBuilder(Material.WOOD_SWORD).enchant(Enchantment.DURABILITY, 3).unbreakable().build() };
    }
}
