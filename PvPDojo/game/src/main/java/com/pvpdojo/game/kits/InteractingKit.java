/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.kits;

import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;

public interface InteractingKit extends ClickKit<PlayerInteractEvent> {
    default void click(Player player) {}
}
