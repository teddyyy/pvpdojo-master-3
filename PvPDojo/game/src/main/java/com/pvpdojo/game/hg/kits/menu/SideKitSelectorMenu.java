package com.pvpdojo.game.hg.kits.menu;

import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.menu.SortType;
import com.pvpdojo.menu.SortedInventoryMenu;
import com.pvpdojo.menu.SortedItem;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class SideKitSelectorMenu extends SortedInventoryMenu {

    public SideKitSelectorMenu(Player player) {
        super(player, "Choose your side kit", 54, SortType.ALPHABETICAL);
    }

    @Override
    public void redraw() {
        super.redraw();
        HGUser user = HGUser.getUser(getPlayer());
        for (HGKit kit : HGKit.values()) {
            if (kit.isSideKit() && kit.isEnabled()) {
                List<String> lore = new ArrayList<>();
                lore.add(CC.GREEN + "[Owned]");
                SortedItem item = new SortedItem(new ItemBuilder(kit.getIcon(user)).appendLore(lore).build(), type -> {
                    getPlayer().performCommand("sidekit " + kit);
                    closeSafely();
                });
                item.setSortName(kit.name());
                addSortItem(item);
            }
        }
    }

}
