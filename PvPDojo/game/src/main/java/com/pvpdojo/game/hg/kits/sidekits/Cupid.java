/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits.sidekits;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.DojoPlayerSoupEvent;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.lang.HGKitMessageKeys;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.kits.EntityInteractingKit;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.TimeUtil;

import co.aikar.locales.MessageKeyProvider;
import lombok.Data;

public class Cupid extends ComplexKit implements EntityInteractingKit {

    private static final Map<UUID, RelationShip> RELATION_SHIPS = new HashMap<>();

    private Player lastClicked;

    @Override
    public void activate(PlayerInteractEntityEvent e) {
        if (e.getRightClicked() instanceof Player) {

            User user = User.getUser(e.getPlayer());

            if (RELATION_SHIPS.containsKey(e.getPlayer().getUniqueId())) {
                user.sendMessage(HGKitMessageKeys.CUPID_RELATION_LIMIT);
                return;
            }

            if (lastClicked != null && lastClicked != e.getRightClicked() && lastClicked.isOnline() && HGUser.getUser(lastClicked).isIngame()) {
                Player clicked = (Player) e.getRightClicked();
                HG.inst().getSession().broadcast(HGKitMessageKeys.CUPID_RELATION_START, "{first}", clicked.getNick(), "{second}", lastClicked.getNick());

                RelationShip relationShip = new RelationShip(clicked.getUniqueId(), lastClicked.getUniqueId());
                RELATION_SHIPS.put(e.getPlayer().getUniqueId(), relationShip);
                relationShip.setTaskId(PvPDojo.schedule(relationShip).createTimer(10, -1));
                lastClicked = null;
            } else {
                lastClicked = (Player) e.getRightClicked();
                user.sendMessage(HGKitMessageKeys.CUPID_PICK);
            }
        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        new ArrayList<>(RELATION_SHIPS.values()).stream().filter(relationShip -> relationShip.getPartners().contains(e.getEntity().getUniqueId())).forEach(RelationShip::end);
    }

    @EventHandler
    public void onSoup(DojoPlayerSoupEvent e) {
        if (e.getPlayer().getHealth() < 16) { // Prevent spam

            for (RelationShip relationShip : RELATION_SHIPS.values()) {
                if (relationShip.getPartners().contains(e.getPlayer().getUniqueId())) {

                    @SuppressWarnings("OptionalGetWithoutIsPresent")
                    Player target = Bukkit.getPlayer(relationShip.getPartners().stream().filter(uuid -> !uuid.equals(e.getPlayer().getUniqueId())).findAny().get());

                    if (target != null) {
                        int slot = target.getInventory().first(Material.MUSHROOM_SOUP);
                        if (slot != -1) {
                            target.getInventory().setItem(slot, null);
                        }
                    }
                }
            }

        }
    }

    @EventHandler
    public void onLoverAttack(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player || (e.getDamager() instanceof Projectile && ((Projectile) e.getDamager()).getShooter() instanceof Player)) {
            for (RelationShip relationShip : RELATION_SHIPS.values()) {
                relationShip.handleHit(e.getDamager() instanceof Projectile ? ((Player) ((Projectile) e.getDamager()).getShooter()).getUniqueId() : e.getDamager().getUniqueId());
            }
        }
    }

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {
        if (RELATION_SHIPS.containsKey(e.getDamager().getUniqueId())) {
            RELATION_SHIPS.get(e.getDamager().getUniqueId()).setLastHit(System.currentTimeMillis());
        }
    }

    @Override
    public void onLeave(Player player) {
        if (RELATION_SHIPS.containsKey(player.getUniqueId())) {
            RELATION_SHIPS.get(player.getUniqueId()).end();
        }
    }

    @Data
    private class RelationShip implements Runnable {

        private UUID first, second;
        private long firstLastHit = System.currentTimeMillis(), secondLastHit = System.currentTimeMillis();
        private long lastHit = System.currentTimeMillis();
        private int taskId;

        RelationShip(UUID first, UUID second) {
            this.first = first;
            this.second = second;
        }

        void handleHit(UUID uuid) {
            if (uuid.equals(first)) {
                firstLastHit = System.currentTimeMillis();
            } else if (uuid.equals(second)) {
                secondLastHit = System.currentTimeMillis();
            }
        }

        List<UUID> getPartners() {
            return Arrays.asList(first, second);
        }

        @Override
        public void run() {
            if (TimeUtil.hasPassed(lastHit, TimeUnit.SECONDS, 10) || TimeUtil.hasPassed(firstLastHit, TimeUnit.SECONDS, 45)
                    || TimeUtil.hasPassed(secondLastHit, TimeUnit.SECONDS, 45)) {
                end();
                return;
            }

            getPartners().stream().map(Bukkit::getPlayer).filter(Objects::nonNull).forEach(player -> player.getWorld().playEffect(player.getEyeLocation(), Effect.HEART, 0));
        }

        void end() {
            sendMessage(HGKitMessageKeys.CUPID_RELATION_END);
            getKitCooldown().use();
            RELATION_SHIPS.values().remove(this);
            Bukkit.getScheduler().cancelTask(taskId);
        }

        void sendMessage(MessageKeyProvider key, String... replacements) {
            getPartners().stream().map(User::getUser).filter(Objects::nonNull).forEach(player -> player.sendMessage(key, replacements));
        }

    }

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.ARROW);
    }

    @Override
    public int getCooldown() {
        return 0;
    }
}
