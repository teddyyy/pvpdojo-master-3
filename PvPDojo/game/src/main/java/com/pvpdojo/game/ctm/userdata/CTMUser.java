/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.userdata;

import java.util.UUID;

import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.game.ctm.CTM;
import com.pvpdojo.game.ctm.kits.list.CTMKit;
import com.pvpdojo.game.ctm.lang.CTMMessageKeys;
import com.pvpdojo.game.ctm.session.CTMSession;
import com.pvpdojo.game.ctm.session.CaptureRegion;
import com.pvpdojo.game.ctm.util.CTMUtil;
import com.pvpdojo.game.lang.GameMessageKeys;
import com.pvpdojo.game.userdata.kit.SingleKitGameUserImpl;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.userdata.User;
import com.pvpdojo.userdata.sidebar.SidebarModule;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;

import net.minecraft.util.com.mojang.authlib.GameProfile;

public class CTMUser extends SingleKitGameUserImpl<CTMKit> {

    public static final String SYMBOL_COW_INCOMPLETE = CC.WHITE + "\u2b1c"; // ⬜
    public static final String SYMBOL_COW_TOUCHED = CC.YELLOW + "\u2592"; // ▒
    public static final String SYMBOL_COW_COMPLETE = CC.GREEN + "\u2b1b"; // ⬛

    public static CTMUser getUser(Player player) {
        return CTM.inst().getUserList().getUser(player);
    }

    public static CTMUser getUser(UUID uuid) {
        return CTM.inst().getUserList().getUser(uuid);
    }

    public CTMUser(UUID uuid) {
        super(uuid);
    }

    @Override
    public void run() {
        super.run();
        updateScoreBoard();

        if (hasKit() && getKit().getKitCooldown().isOnCooldown()) {
            getPlayer().setExp(getKit().getKitCooldown().getCooldownRatio());
        } else if (getPlayer().getExp() != 0) {
            getPlayer().setExp(0);
        }
    }

    @Override
    public StringBuilder generatePrefix(StringBuilder prefix, User td) {
        if (getTeam() != null) {
            return prefix.append(getTeam().getColor());
        }
        return super.generatePrefix(prefix, td);
    }

    @Override
    public void updateTablist() {
        if (getTeam() == null) {
            super.updateTablist();
            return;
        }
        String tabname = BukkitUtil.getTabName(getTeam().getColor().toString(), getPlayer().getNick());
        getPlayer().setPlayerListName(tabname);
        if (getPlayer().isNicked()) {
            GameProfile nickProfile = NMSUtils.getCraftPlayer(getPlayer()).nickGameProfile;
            PvPDojo.schedule(() -> Redis.get().publish(BungeeSync.TABLIST, getPlayer().getUniqueId() + "|" + getSpoofedRank().getPrefix() + getPlayer().getNick()
                    + (nickProfile != null ? "|" + nickProfile.getId().toString() : ""))).createAsyncTask();
        }
    }

    @Override
    public void updateUserFor(User td, boolean tracker) {
        super.updateUserFor(td, true); // We use this for tablist stuff, always track scoreboard teams
    }

    private SidebarModule sidebarModule = new SidebarModule();

    public void updateScoreBoard() {

        CTMSession session = CTM.inst().getSession();
        getSidebar().setTitle(CC.DARK_PURPLE + CC.BOLD.toString() + "CTM");

        sidebarModule.append(LocaleMessage.of(session.getState() == SessionState.PREGAME ? GameMessageKeys.SIDEBAR_STARTING : GameMessageKeys.SIDEBAR_GAME_TIME)
                                          .transformer(str -> CC.YELLOW + str).get(this))
                     .append(StringUtils.getReadableSeconds(session.getCurrentHandler().getTimeSeconds()))
                     .append("");

        if (getSession() != null) {
            sidebarModule.append(LocaleMessage.of(CTMMessageKeys.SIDEBAR_DAMAGE).get(this));
            Double damage = session.getDamageMap().get(getPlayer().getUniqueId());
            boolean notReady = damage != null && damage < 50D;

            sidebarModule.append((notReady ? "" : CC.GREEN.toString()) + String.format("%.2f", damage) + "/" + CTMUtil.MAX_DAMAGE)
                         .append("");

        }

        sidebarModule.append(CC.RED + session.getTeamRed().getName() + (session.getTeamRed() == getTeam() ? CC.GOLD + " (Your):" : ":"));
        for (CaptureRegion region : session.getCaptureRegions().get(session.getTeamRed())) {
            sidebarModule.append((session.getCaptures().get(session.getTeamRed()).contains(region.getRegion()) ? SYMBOL_COW_COMPLETE :
                    session.getRunningCaptures().values().contains(region) ? SYMBOL_COW_TOUCHED : SYMBOL_COW_INCOMPLETE) + " "
                    + StringUtils.getEnumName(region.getDirection()) + " cow");
        }

        sidebarModule.append("")
                     .append(CC.BLUE + session.getTeamBlue().getName() + (session.getTeamBlue() == getTeam() ? CC.GOLD + " (Your):" : ":"));
        for (CaptureRegion region : session.getCaptureRegions().get(session.getTeamBlue())) {
            sidebarModule.append((session.getCaptures().get(session.getTeamBlue()).contains(region.getRegion()) ? SYMBOL_COW_COMPLETE :
                    session.getRunningCaptures().values().contains(region) ? SYMBOL_COW_TOUCHED : SYMBOL_COW_INCOMPLETE) + " "
                    + StringUtils.getEnumName(region.getDirection()) + " cow");
        }

        getSidebar().set(sidebarModule);

    }

    @Override
    public boolean canBuild() {
        return super.canBuild() || (!isSpectator() && !isAdminMode());
    }

    @Override
    public void setSpectator(boolean spectator) {
        super.setSpectator(spectator);
        if (spectator && !CTM.inst().getSession().isPreparedTeamsOnly() && CTM.inst().getSession().getState() != SessionState.FINISHED) {
            setHotbarGui(CTMSpectatorHotbar.inst());
        } else if (CTMSpectatorHotbar.inst().equals(getHotbarGui())) {
            setHotbarGui(null);
        }
    }
}
