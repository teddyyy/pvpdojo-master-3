/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.userdata.kit;

import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.kits.KitHolder;
import com.pvpdojo.game.kits.KitProvider;

public interface SingleKitGameUser<P extends KitProvider> extends KitGameUser<P> {

    KitHolder getKitHolder();

    Kit getKit();

    void setKit(P kitProvider);

    boolean hasKit();
}
