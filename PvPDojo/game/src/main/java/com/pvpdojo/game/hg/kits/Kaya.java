/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.util.Vector;

import com.google.common.collect.Sets;
import com.pvpdojo.ServerSettings.CustomRecipeModule;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.game.userdata.kit.KitGameUser;

public class Kaya extends ComplexKit {

    private static final Set<Block> KAYA = new HashSet<>();

    @Override
    public void onPlace(BlockPlaceEvent e) {
        if (e.getBlockPlaced().getType() == Material.GRASS) {
            KAYA.add(e.getBlockPlaced());
        }
    }

    @EventHandler
    public void onMoveKaya(PlayerMoveEvent e) {
        if (KAYA.isEmpty()) {
            return;
        }

        HGUser user = HGUser.getUser(e.getPlayer());

        if (user.isIngame() && !KitUtil.hasKit(user, HGKit.KAYA)) {
            Vector diff = e.getTo().toVector().subtract(e.getFrom().toVector());
            for (Block block : Sets.union(NMSUtils.getBlocksBelow(e.getPlayer(), 0.0001, diff), NMSUtils.getBlocksBelow(e.getPlayer(), 1.0001, diff))) {
                if (KAYA.remove(block)) {
                    block.setType(Material.AIR);
                    if (!e.getPlayer().isOnGround() && e.isOnGround()) {
                        e.getPlayer().setVelocity(new Vector());
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPreventKayaCraft(PrepareItemCraftEvent e) {
        KitGameUser<?> user = KitGameUser.getUser((Player) e.getView().getPlayer());

        if (e.getInventory().contains(Material.DIRT) && e.getInventory().contains(Material.SEEDS) && !KitUtil.hasKit(user, HGKit.KAYA)) {
            e.getInventory().setResult(null);
        }
    }

    @Override
    public void register() {
        super.register();
        ShapelessRecipe kayaRecipe = new ShapelessRecipe(new ItemStack(Material.GRASS));
        kayaRecipe.addIngredient(1, Material.DIRT);
        kayaRecipe.addIngredient(1, Material.SEEDS);
        new CustomRecipeModule(kayaRecipe).register();
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] { new ItemStack(Material.GRASS, 25) };
    }
}
