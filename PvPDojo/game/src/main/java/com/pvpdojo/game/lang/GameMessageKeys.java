/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.lang;

import co.aikar.locales.MessageKey;
import co.aikar.locales.MessageKeyProvider;

public enum GameMessageKeys implements MessageKeyProvider {
    COUNTDOWN_START, COUNTDOWN_INVINCIBILITY,
    SIDEBAR_STARTING, SIDEBAR_INVINCIBLE, SIDEBAR_GAME_TIME,
    COOLDOWN;

    private final MessageKey key = MessageKey.of("pvpdojo-game." + name().toLowerCase());

    @Override
    public MessageKey getMessageKey() {
        return key;
    }
}
