package com.pvpdojo.game.hg.kits.sidekits;

import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemUtil;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Burrower extends Kit implements InteractingKit {

    @Override
    public void activate(PlayerInteractEvent e) {

        if (e.getPlayer().getLocation().getY() <= 80 && e.getPlayer().getLocation().getY() >= 50) {

            Block block1 = e.getPlayer().getWorld().getBlockAt(e.getPlayer().getLocation().getBlockX() - 3, e.getPlayer().getLocation().getBlockY() - 30, e.getPlayer().getLocation().getBlockZ() - 3);
            Block block2 = e.getPlayer().getWorld().getBlockAt(e.getPlayer().getLocation().getBlockX() + 3, e.getPlayer().getLocation().getBlockY() - 24, e.getPlayer().getLocation().getBlockZ() + 3);
            Block block3 = e.getPlayer().getWorld().getBlockAt(e.getPlayer().getLocation().getBlockX() - 2, e.getPlayer().getLocation().getBlockY() - 29, e.getPlayer().getLocation().getBlockZ() - 2);
            Block block4 = e.getPlayer().getWorld().getBlockAt(e.getPlayer().getLocation().getBlockX() + 2, e.getPlayer().getLocation().getBlockY() - 25, e.getPlayer().getLocation().getBlockZ() + 2);



            spawnCuboid(block1, block2, Material.COBBLESTONE);
            spawnCuboid(block3, block4, Material.AIR);

            e.getPlayer().teleport(new Location(e.getPlayer().getWorld(), block3.getX() + 2, block3.getY(), block3.getZ() + 3));
            ItemUtil.throwItem(e.getPlayer(), new ItemStack(Material.STONE_PICKAXE), true, false);
            e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 30 * 20, 1));

            kitCooldown.use();

        } else {
            e.getPlayer().sendMessage(CC.RED + "You can only use this kit while being between y: 50 and y: 80");
        }

    }

    public void spawnCuboid(Block block1, Block block2, Material material) {
        for (int k = block1.getY(); k <= block2.getY(); k++) {
            for (int j = block1.getZ(); j <= block2.getZ(); j++) {
                for (int i = block1.getX(); i <= block2.getX(); i++) {
                    block1.getWorld().getBlockAt(i, k, j).setType(material);
                }
            }
        }
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.GOLD_SPADE);
    }

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public int getCooldown() {
        return 60000;
    }
}
