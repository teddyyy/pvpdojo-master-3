package com.pvpdojo.game.hg.command;

import co.aikar.commands.annotation.*;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.kits.menu.KitSelectorMenu;
import com.pvpdojo.game.hg.kits.menu.SideKitSelectorMenu;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.KitHolder;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;

@CommandAlias("sidekit")
public class SideKitCommand extends DojoCommand {

    @CommandCompletion("@kits")
    @CatchUnknown
    @Default
    public void onKit(User u, @Optional HGKit kit) {
        HGUser user = (HGUser) u;

        if (user.getSession() != null && (user.getSession().getState() == SessionState.PREGAME || user.getSession().getState() == SessionState.INVINCIBILITY)) {
            if (user.getSession().getState() == SessionState.INVINCIBILITY && user.getSideKit() != null) {
                user.sendMessage(CC.RED + "You have already chosen your side kit");
                return;
            }

            if (kit != null && kit.isSideKit()) {
                user.setSideKit(new KitHolder<HGKit>(kit, kit.getSupplier().get()));
                if (user.getSession().getState() != SessionState.PREGAME) {
                    KitUtil.applyKitItems(user, user.getSideKit().getKit());
                }
                user.sendMessage(CC.GREEN + "You are now " + StringUtils.getEnumName(kit));
            } else {
                new SideKitSelectorMenu(u.getPlayer()).open();
            }

        } else {
            user.sendMessage(CC.RED + "You can't choose a kit after the game has started");
        }
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
