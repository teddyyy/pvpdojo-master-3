package com.pvpdojo.game.hg.kits;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.DojoPlayerSoupEvent;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder.ItemEditor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_7_R4.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Nightlock extends ComplexKit {

    private int duration = 20 * 10;
    private int hunger;


    @EventHandler
    public void onSoup(DojoPlayerSoupEvent e) {
        Player player = e.getPlayer();
        double currentHealth = player.getHealth();
        ItemStack item = player.getItemInHand();
        if (item.getItemMeta() == null || item.getItemMeta().getDisplayName() == null
                || !item.getItemMeta().getDisplayName().endsWith(ChatColor.DARK_GREEN + "")) {
            return;
        }
        if (KitUtil.hasKit(HGUser.getUser(player), HGKit.NIGHTLOCK)) {
            return;
        }
        if ((player.getHealth() < player.getMaxHealth()) && (player.getHealth() > 0)) {
            if (player.getHealth() <= player.getMaxHealth() - 2) {
                player.setHealth(currentHealth);
                player.setHealth(player.getHealth() + 2);
                soup(player);
            } else if ((player.getHealth() < player.getMaxHealth())
                    && (player.getHealth() > player.getMaxHealth() - 2)) {
                player.setHealth(player.getMaxHealth());
                soup(player);
            }

        }

    }

    private void soup(Player p) {
        if (hunger == 0) {
            hunger = p.getFoodLevel();
        }
        p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, duration, 0), true);
        p.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, duration, 1), true);
        p.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, duration, 39));// ok chris.
        p.sendMessage(CC.RED + "Agh! A Nightlock gave you a poisonous soup!");

        PvPDojo.schedule(() -> {
            p.setFoodLevel(hunger);
            hunger = 0;
        }).createTask(duration + 1);

        Bukkit.getPluginManager().callEvent(new PlayerItemConsumeEvent(p, p.getItemInHand()));
        new ItemEditor(p.getItemInHand()).name(null).type(Material.BOWL).build();
    }

    @EventHandler
    public void onInventoryClick(CraftItemEvent e) {
        Player player = (Player) e.getWhoClicked();
        HGUser user = HGUser.getUser(player);
        if (!KitUtil.hasKit(user, HGKit.NIGHTLOCK)) {
            return;
        }
        if (e.isShiftClick()) {
            if (e.getInventory().getResult() != null
                    && e.getInventory().getResult().getType() == Material.MUSHROOM_SOUP) {

                ItemStack[] preInv = player.getInventory().getContents();
                PvPDojo.schedule(() -> {
                    final ItemStack[] postInv = player.getInventory().getContents();

                    for (int i = 0; i < preInv.length; i++) {
                        if (preInv[i] != postInv[i]) {
                            if (postInv[i].getType() == Material.MUSHROOM_SOUP) {
                                ItemMeta meta = postInv[i].getItemMeta();
                                meta.setDisplayName((meta.getDisplayName() == null
                                        ? ChatColor.RESET + getDefaultItemName(postInv[1])
                                        : meta.getDisplayName()) + ChatColor.DARK_GREEN + "");
                                postInv[i].setItemMeta(meta);
                            }
                        }
                    }
                }).nextTick();
            }
        } else {
            if (e.getInventory().getResult() != null
                    && e.getInventory().getResult().getType() == Material.MUSHROOM_SOUP) {
                ItemMeta meta = e.getInventory().getResult().getItemMeta();
                meta.setDisplayName((meta.getDisplayName() == null
                        ? ChatColor.RESET + getDefaultItemName(e.getInventory().getResult())
                        : meta.getDisplayName()) + ChatColor.DARK_GREEN + "");
                e.getInventory().getResult().setItemMeta(meta);
            }

        }
    }

    public String getDefaultItemName(ItemStack item) {
        net.minecraft.server.v1_7_R4.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        return nmsStack.getItem().n(nmsStack);
    }

}
