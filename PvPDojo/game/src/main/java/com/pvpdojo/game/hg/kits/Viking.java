/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import java.util.EnumSet;

import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class Viking extends Kit {

    private static final EnumSet<Material> AXES = EnumSet.of(Material.WOOD_AXE, Material.STONE_AXE, Material.IRON_AXE, Material.DIAMOND_AXE);

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {
        Player attacker = (Player) e.getDamager();
        if (attacker.getItemInHand() != null && AXES.contains(attacker.getItemInHand().getType()) && PlayerUtil.isRealHit((LivingEntity) e.getEntity())) {
            e.setDamage(e.getDamage() + 1.5);
            attacker.getItemInHand().setDurability((short) (attacker.getItemInHand().getDurability() - 1));
        }
    }
}
