/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.DojoPlayerSoupEvent;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.kits.KitHolder;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class Parasite extends Kit implements InteractingKit {

    private boolean activated;
    private Player lastDamaged;

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {
        if (!e.isCancelled()) {
            lastDamaged = (Player) e.getEntity();
        }
    }

    @Override
    public void onSoup(DojoPlayerSoupEvent e) {
        if (activated) {
            if (PvPDojo.RANDOM.nextInt(8) == 0) {
                int slot = lastDamaged.getInventory().first(Material.MUSHROOM_SOUP);
                if (slot != -1) {
                    lastDamaged.getInventory().setItem(slot, null);
                }
            }
        }
    }

    @Override
    public void onLeave(Player player) {
        deactivate(HGUser.getUser(player));
    }

    @Override
    public void activate(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        if (lastDamaged != null && !activated) {

            // Assign Kit
            HGUser victim = HGUser.getUser(lastDamaged);
            HGUser user = HGUser.getUser(player);

            if (victim != null) {
                getKitCooldown().use();

                victim.sendMessage(CC.RED + "A Parasite has infiltrated your body and copied your properties");
                KitUtil.removeKitItems(user);
                activated = true;

                if (victim.getKitHolder() == null || victim.getKitHolder().getKitProvider() == HGKit.PARASITE || victim.getKitHolder().getKitProvider() == HGKit.BACKUP) {
                    user.getKitHolders().add(0, new KitHolder<>(HGKit.SURPRISE, HGKit.SURPRISE.getSupplier().get()));
                } else {
                    user.getKitHolders().add(0, new KitHolder<>((HGKit) victim.getKitHolder().getKitProvider(), victim.getKitHolder().getKitProvider().getSupplier().get()));
                    if (victim.getKitHolder().getKitProvider() != HGKit.URGAL || victim.getKitHolder().getKitProvider() != HGKit.MONK
                            || victim.getKitHolder().getKitProvider() != HGKit.REDSTONER) {
                        KitUtil.applyKitItems(user);
                    }
                    player.updateInventory();
                    user.sendMessage(CC.GREEN + "You are now " + StringUtils.getEnumName((HGKit) victim.getKitHolder().getKitProvider()));
                }

                PvPDojo.schedule(() -> deactivate(user)).createTask(40 * 20);

            }
        }
    }

    private void dropPortal(HGUser user) {
        Kit kit = user.getKit();

        if (kit instanceof Portal) {
            Portal portal = (Portal) kit;

            portal.destroyGates(true);
            portal.destroyGates(false);
        }
    }

    public void deactivate(HGUser user) {
        if (user.isIngame() && activated) {
            activated = false;
            dropPortal(user);
            user.getKits().forEach(kit -> kit.onLeave(user.getPlayer()));
            KitUtil.removeKitItems(user);
            while (user.getKitHolders().size() > 1) {
                user.getKitHolders().remove(0);
            }
            KitUtil.applyKitItems(user);
        }
    }

    @Override
    public void click(Player player) {
    }

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        if (activated) {
            // Stupid hack to prevent NPE and represent a non-existent item
            return new ItemStack(Material.REDSTONE_LAMP_ON);
        }
        return new ItemStack(Material.FERMENTED_SPIDER_EYE);
    }

    @Override
    public int getCooldown() {
        return 60_000;
    }
}
