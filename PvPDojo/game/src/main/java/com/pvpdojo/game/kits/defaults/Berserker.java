/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.kits.defaults;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.game.kits.Kit;

public class Berserker extends Kit {

    @Override
    public void onKillEntity(EntityDeathEvent e) {
        boolean playerKill = e.getEntity() instanceof Player;

        e.getEntity().getWorld().playSound(e.getEntity().getKiller().getLocation(), Sound.WOLF_HOWL, 1, 1);
        e.getEntity().getKiller().addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, playerKill ? 12 * 20 : 4 * 20, 0), true);
        e.getEntity().getKiller().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, playerKill ? 8 * 20 : 6 * 20, 1), true);
    }
}
