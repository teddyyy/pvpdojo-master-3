/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.ShapelessRecipe;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerSettings;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.kits.KitHolder;
import com.pvpdojo.game.userdata.kit.KitGameUser;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class Magma extends ComplexKit {

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {
        if (!e.isCancelled() && PlayerUtil.isRealHit((LivingEntity) e.getEntity()) && PvPDojo.RANDOM.nextInt(3) == 2) {
            if (e.getEntity().getFireTicks() < 3 * 20) {
                e.getEntity().setFireTicks(3 * 20);
            }
        }
    }

    @Override
    public void onDamage(EntityDamageEvent e) {
        if (e.getCause() == DamageCause.FIRE || e.getCause() == DamageCause.FIRE_TICK) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPrepareCraft(PrepareItemCraftEvent e) {
        KitGameUser<?> user = KitGameUser.getUser((Player) e.getView().getPlayer());

        if (e.getInventory().contains(Material.NETHER_STALK) && e.getInventory().contains(Material.BOWL)
                && user.getKitHolders().stream().map(KitHolder::getKitProvider).noneMatch(HGKit.MAGMA::equals)) {
            e.getInventory().setResult(null);
        }
    }

    @Override
    public void register() {
        super.register();
        ShapelessRecipe magmarecipe = new ShapelessRecipe(new ItemBuilder(Material.MUSHROOM_SOUP).name("Wart Syrup").build());
        magmarecipe.addIngredient(2, Material.NETHER_STALK);
        magmarecipe.addIngredient(1, Material.BOWL);
        new ServerSettings.CustomRecipeModule(magmarecipe).register();
    }
}
