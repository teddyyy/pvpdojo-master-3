/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class Lumberjack extends Kit {

    private static final EnumSet<BlockFace> FACES = EnumSet.of(BlockFace.SOUTH, BlockFace.NORTH, BlockFace.EAST, BlockFace.WEST, BlockFace.SOUTH_EAST,
            BlockFace.SOUTH_WEST, BlockFace.NORTH_EAST, BlockFace.NORTH_WEST, BlockFace.DOWN, BlockFace.UP);

    private boolean inProgress;

    @Override
    public void onBreak(BlockBreakEvent e) {
        if (!inProgress && (e.getBlock().getType() == Material.LOG || e.getBlock().getType() == Material.LOG_2) && e.getPlayer().getItemInHand().getType() == getStartItems()[0].getType()) {
            inProgress = true;
            recursiveBreak(e.getBlock(), e.getPlayer(), new HashSet<>());
            inProgress = false;
        }
    }

    public void recursiveBreak(Block block, Player player, Set<Block> broken) {
        if (broken.contains(block)) {
            return;
        }

        broken.add(block);
        if (block.getType() == Material.LOG || block.getType() == Material.LOG_2) {
            breakBlock(block, player);
            for (BlockFace face : FACES) {
                recursiveBreak(block.getRelative(face), player, broken);
            }
        }
    }

    public void breakBlock(Block block, Player player) {
        BlockBreakEvent event = new BlockBreakEvent(block, player);
        Bukkit.getPluginManager().callEvent(event);
        if (!event.isCancelled()) {
            block.breakNaturally();
        }
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] { new ItemBuilder(Material.WOOD_AXE).enchant(Enchantment.DIG_SPEED, 2).unbreakable().build() };
    }
}
