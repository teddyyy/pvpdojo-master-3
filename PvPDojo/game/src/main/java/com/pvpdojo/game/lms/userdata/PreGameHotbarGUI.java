/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.lms.userdata;

import org.bukkit.Material;

import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.game.lms.LMS;
import com.pvpdojo.gui.HotbarGUI;
import com.pvpdojo.menus.KitSelectorMenu;
import com.pvpdojo.userdata.KitAbilityData;
import com.pvpdojo.userdata.KitData;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.StringUtils;

public class PreGameHotbarGUI extends HotbarGUI {

    private static final PreGameHotbarGUI inst = new PreGameHotbarGUI();

    public static PreGameHotbarGUI get() {
        return inst;
    }

    private PreGameHotbarGUI() {
        if (LMS.inst().getSettings().isAbilities()) {
            setItem(0, new ItemBuilder(Material.ANVIL).name(CC.GREEN + "Kit Selector").build(), onRightClick(player -> {
                new KitSelectorMenu(player, true, kit -> {
                    LMSUser.getUser(player).setKitCache(kit);
                    player.sendMessage(CC.GRAY + StringUtils.getLS());
                    player.sendMessage(CC.GRAY + "You selected: " + CC.GREEN + kit.getName());
                    player.sendMessage(CC.GRAY + "Playstyle: " + CC.DARK_PURPLE + kit.getPlayStyle().getName());
                    player.sendMessage(CC.GRAY + "Abilities: " + listAbilities(kit));
                    player.sendMessage(CC.GRAY + StringUtils.getLS());
                    player.closeInventory();
                }).open();
            }));
        }
    }

    private static String listAbilities(KitData data) {
        StringBuilder abilities = new StringBuilder();
        for (KitAbilityData ability : data.getAbilities()) {
            AbilityLoader loader = AbilityLoader.getAbilityData(ability.getId());
            abilities.append(loader.getRarity().getColor()).append(loader.getName()).append(", ");
        }
        if (abilities.length() != 0)
            return abilities.substring(0, abilities.length() - 2);
        return abilities.toString();
    }

}
