/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits.sidekits;

import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import com.pvpdojo.game.kits.Kit;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Worm extends Kit {

    @Override
    public void onBlockDamage(BlockDamageEvent e) {
        if (e.getBlock().getType() == Material.DIRT || e.getBlock().getType() == Material.GRAVEL || e.getBlock().getType() == Material.SAND) {
            Player player = e.getPlayer();
            player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 20 * 5, 0));
            player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * 5, 0));
            player.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION, 20 * 5, 0));
            e.setInstaBreak(true);
        }
    }

    @Override
    public void onDamage(EntityDamageEvent e) {
        if (e.getCause() == EntityDamageEvent.DamageCause.FALL && e.getEntity().getLocation().getBlock().getRelative(BlockFace.DOWN).getType() == Material.DIRT) {
            e.setDamage(1);
        }
    }
}
