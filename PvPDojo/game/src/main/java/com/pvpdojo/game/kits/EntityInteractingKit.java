/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.kits;

import org.bukkit.event.player.PlayerInteractEntityEvent;

public interface EntityInteractingKit extends ClickKit<PlayerInteractEntityEvent> {}
