/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.pvpdojo.game.hg.kits.list.HGKit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.common.util.concurrent.AtomicDouble;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.lang.HGKitMessageKeys;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.kits.KitHolder;
import com.pvpdojo.game.kits.KitProvider;
import com.pvpdojo.game.userdata.kit.KitGameUser;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.util.TimeUtil;

public class Madman extends ComplexKit {

    public static final Table<UUID, UUID, AtomicInteger> TIME_MAP = HashBasedTable.create();
    public static final Table<UUID, UUID, AtomicDouble> PERCENTAGE_MAP = HashBasedTable.create();
    public static final Map<UUID, Long> LAST_MESSAGE = new HashMap<>();

    @Override
    public void onTick(KitGameUser<? extends KitProvider> user) {

        if (HG.inst().getSession().getState() != SessionState.STARTED) {
            return;
        }

        Set<Player> nearBy = TrackerUtil.getNearbyIngame(user.getPlayer(), 20, 20, 20)
                                        .filter(player -> {
                                            HGUser target = HGUser.getUser(player);
                                            KitHolder holder = target.getKitHolder();
                                            return target.getKit() == null || holder.getKitProvider() != HGKit.MADMAN;
                                        })
                                        .collect(Collectors.toSet());

        TrackerUtil.getIngame().forEach(player -> {
            HGUser target = HGUser.getUser(player);
            int time = 0;

            if (nearBy.contains(player) && nearBy.size() >= 2) {
                if (TIME_MAP.contains(user.getUUID(), player.getUniqueId())) {
                    time = TIME_MAP.get(user.getUUID(), player.getUniqueId()).addAndGet(nearBy.size());
                } else {
                    time = nearBy.size();
                    TIME_MAP.put(user.getUUID(), player.getUniqueId(), new AtomicInteger(nearBy.size()));
                }
            } else {
                if (TIME_MAP.contains(user.getUUID(), player.getUniqueId())) {
                    time = TIME_MAP.get(user.getUUID(), player.getUniqueId()).addAndGet(-5);
                }
            }

            if (time > 0) {
                if (!PERCENTAGE_MAP.contains(user.getUUID(), player.getUniqueId())) {
                    target.sendMessage(HGKitMessageKeys.MADMAN_EFFECT);
                    LAST_MESSAGE.put(player.getUniqueId(), System.currentTimeMillis());
                }
                PERCENTAGE_MAP.put(user.getUUID(), player.getUniqueId(), new AtomicDouble(calcPercentage(time)));
                if (TimeUtil.hasPassed(LAST_MESSAGE.get(player.getUniqueId()), TimeUnit.SECONDS, 7)) {
                    PvPDojo.schedule(() -> {
                        LAST_MESSAGE.put(player.getUniqueId(), System.currentTimeMillis());
                        int percent = (int) PERCENTAGE_MAP.column(player.getUniqueId()).values().stream().mapToDouble(AtomicDouble::get).sum();
                        target.sendMessage(HGKitMessageKeys.MADMAN_DAMAGE, "{percent}", String.valueOf(percent));
                    }).nextTick();
                }
            } else {
                TIME_MAP.remove(user.getUUID(), player.getUniqueId());
                if (PERCENTAGE_MAP.remove(user.getUUID(), player.getUniqueId()) != null && PERCENTAGE_MAP.column(player.getUniqueId()).values().isEmpty()) {
                    target.sendMessage(HGKitMessageKeys.MADMAN_NO_EFFECT);
                }
            }
        });
    }

    public double calcPercentage(double time) {
        if (time > 120) {
            return 50 + (time - 100) / 5;
        }
        return time / 2;
    }

    public static double getMultiplier(UUID uuid) {
        return 1 + (PERCENTAGE_MAP.column(uuid).values().stream().mapToDouble(AtomicDouble::get).sum() / 100);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        e.setDamage(e.getDamage() * getMultiplier(e.getEntity().getUniqueId()));
    }

    @Override
    public void onLeave(Player player) {
        TIME_MAP.rowMap().remove(player.getUniqueId());
        PERCENTAGE_MAP.rowMap().remove(player.getUniqueId());
    }


}
