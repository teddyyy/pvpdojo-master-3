/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.util;

import java.lang.reflect.Field;
import java.util.Calendar;

import org.bukkit.craftbukkit.v1_7_R4.entity.CraftAmbient;
import org.bukkit.craftbukkit.v1_7_R4.util.UnsafeList;
import org.bukkit.entity.Ambient;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.game.ctm.session.CaptureRegion;

import net.minecraft.server.v1_7_R4.*;

/*
 * This class is a copy of EntityBat modified to follow their owner
 */
public class CTMBat extends EntityAmbient {

    public static Ambient spawn(Player owner) {
        Ambient ambient = (CraftAmbient) new CTMBat(NMSUtils.get(owner.getWorld()), owner).getBukkitEntity();
        ambient.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 0));
        ambient.setMetadata(CaptureRegion.CAPTURE_ENTITY, new FixedMetadataValue(PvPDojo.get(), null));
        return ambient;
    }

    EntityPlayer owner;
    private ChunkCoordinates h;

    public CTMBat(World paramWorld) {
        super(paramWorld);
    }

    public CTMBat(World world, Player owner) {
        super(world);
        this.owner = NMSUtils.get(owner);
        setLocation(owner.getLocation().getX(), owner.getLocation().getY(), owner.getLocation().getZ(), 0, 0);
        try {
            Field bField = PathfinderGoalSelector.class.getDeclaredField("b");
            bField.setAccessible(true);
            Field cField = PathfinderGoalSelector.class.getDeclaredField("c");
            cField.setAccessible(true);
            bField.set(goalSelector, new UnsafeList<PathfinderGoalSelector>());
            bField.set(targetSelector, new UnsafeList<PathfinderGoalSelector>());
            cField.set(goalSelector, new UnsafeList<PathfinderGoalSelector>());
            cField.set(targetSelector, new UnsafeList<PathfinderGoalSelector>());

        } catch (Exception exc) {
            exc.printStackTrace();
        }
        world.addEntity(this);
    }

    protected void c() {
        super.c();

        this.datawatcher.a(16, (byte) 0);
    }

    protected float bf() {
        return 0.10F;
    }

    protected float bg() {
        return (super.bg() * 0.949999990F);
    }

    protected String t() {
        return null;
    }

    protected String aT() {
        return "mob.bat.hurt";
    }

    protected String aU() {
        return "mob.bat.death";
    }

    public boolean S() {
        return false;
    }

    protected void o(Entity paramEntity) {}

    protected void bo() {}

    protected void aD() {
        super.aD();

        getAttributeInstance(GenericAttributes.maxHealth).setValue(6.0D);
    }

    public boolean isAsleep() {
        return ((this.datawatcher.getByte(16) & 0x1) != 0);
    }

    public void setAsleep(boolean paramBoolean) {
        int i = this.datawatcher.getByte(16);
        if (paramBoolean)
            this.datawatcher.watch(16, Byte.valueOf((byte) (i | 0x1)));
        else
            this.datawatcher.watch(16, Byte.valueOf((byte) (i & 0xFFFFFFFE)));
    }

    protected boolean bk() {
        return true;
    }

    private Entity ctmPassenger;

    public void h() {
        super.h();

        if (passenger != null) {
            this.ctmPassenger = passenger;
        }

        if (owner != null && ticksLived % 20 == 0) {
            if (owner.getBukkitEntity().isProtocolHack()) {
                for (Object oplayer : ((EntityTrackerEntry) ((WorldServer) world).getTracker().trackedEntities.get(getId())).trackedPlayers) {
                    if (oplayer != owner) {
                        ((EntityPlayer) oplayer).playerConnection.sendPacket(new PacketPlayOutAttachEntity(0, this, this.owner));
                    }
                }
            } else if (vehicle == null) {
                owner.getBukkitEntity().setPassenger(getBukkitEntity());
            } else if (passenger == null) {
                if (ctmPassenger != null) {
                    ctmPassenger.mount(this);
                }
            }
        }

        if (owner != null && getBukkitEntity().getLocation().distanceSquared(owner.getBukkitEntity().getLocation()) > 81) {
            setLocation(owner.locX, owner.locY + 5, owner.locZ, 0, 0);
        }

        if (isAsleep()) {
            this.motX = (this.motY = this.motZ = 0.0D);
            this.locY = (MathHelper.floor(this.locY) + 1.0D - this.length);
        } else {
            this.motY *= 0.600000023841857910D;
        }
    }

    protected void bn() {
        super.bn();
        if (owner != null) {
            Vector vec = new Vector(owner.locX - locX, owner.locY + 3 - locY, owner.locZ - locZ);
            // vec.normalize();
            this.motX = vec.getX();
            this.motY = vec.getY();
            this.motZ = vec.getZ();

        } else {
            if (isAsleep()) {
                if (!(this.world.getType(MathHelper.floor(this.locX), (int) this.locY + 1, MathHelper.floor(this.locZ)).r())) {
                    setAsleep(false);
                    this.world.a(null, 1015, (int) this.locX, (int) this.locY, (int) this.locZ, 0);
                } else {
                    if (this.random.nextInt(200) == 0) {
                        this.aO = this.random.nextInt(360);
                    }

                    if (this.world.findNearbyPlayer(this, 4.0D) != null) {
                        setAsleep(false);
                        this.world.a(null, 1015, (int) this.locX, (int) this.locY, (int) this.locZ, 0);
                    }
                }
            } else {
                if ((this.h != null) && (((!(this.world.isEmpty(this.h.x, this.h.y, this.h.z))) || (this.h.y < 1)))) {
                    this.h = null;
                }
                if ((this.h == null) || (this.random.nextInt(30) == 0) || (this.h.e((int) this.locX, (int) this.locY, (int) this.locZ) < 4.0F)) {
                    this.h = new ChunkCoordinates((int) this.locX + this.random.nextInt(7) - this.random.nextInt(7),
                            (int) this.locY + this.random.nextInt(6) - 2, (int) this.locZ + this.random.nextInt(7) - this.random.nextInt(7));
                }

                double d1 = this.h.x + 0.50D - this.locX;
                double d2 = this.h.y + 0.100000000000000010D - this.locY;
                double d3 = this.h.z + 0.50D - this.locZ;

                this.motX += (Math.signum(d1) * 0.50D - this.motX) * 0.100000001490116120D;
                this.motY += (Math.signum(d2) * 0.699999988079071040D - this.motY) * 0.100000001490116120D;
                this.motZ += (Math.signum(d3) * 0.50D - this.motZ) * 0.100000001490116120D;

                float f1 = (float) (Math.atan2(this.motZ, this.motX) * 180.0D / Math.PI) - 90.0F;
                float f2 = MathHelper.g(f1 - this.yaw);
                this.be = 0.50F;
                this.yaw += f2;

                if ((this.random.nextInt(100) == 0) && (this.world.getType(MathHelper.floor(this.locX), (int) this.locY + 1, MathHelper.floor(this.locZ)).r()))
                    setAsleep(true);
            }
        }
    }

    protected boolean g_() {
        return false;
    }

    protected void b(float paramFloat) {}

    protected void a(double paramDouble, boolean paramBoolean) {}

    public boolean az() {
        return true;
    }

    public boolean damageEntity(DamageSource paramDamageSource, float paramFloat) {
        if (isInvulnerable())
            return false;
        if ((!(this.world.isStatic)) && (isAsleep())) {
            setAsleep(false);
        }

        return super.damageEntity(paramDamageSource, paramFloat);
    }

    public void a(NBTTagCompound paramNBTTagCompound) {
        super.a(paramNBTTagCompound);

        this.datawatcher.watch(16, Byte.valueOf(paramNBTTagCompound.getByte("BatFlags")));
    }

    public void b(NBTTagCompound paramNBTTagCompound) {
        super.b(paramNBTTagCompound);

        paramNBTTagCompound.setByte("BatFlags", this.datawatcher.getByte(16));
    }

    public boolean canSpawn() {
        int i = MathHelper.floor(this.boundingBox.b);
        if (i >= 63)
            return false;

        int j = MathHelper.floor(this.locX);
        int k = MathHelper.floor(this.locZ);

        int l = this.world.getLightLevel(j, i, k);
        int i1 = 4;
        Calendar localCalendar = this.world.V();

        if (((localCalendar.get(2) + 1 == 10) && (localCalendar.get(5) >= 20)) || ((localCalendar.get(2) + 1 == 11) && (localCalendar.get(5) <= 3)))
            i1 = 7;
        else if (this.random.nextBoolean()) {
            return false;
        }

        if (l > this.random.nextInt(i1))
            return false;

        return super.canSpawn();
    }

}