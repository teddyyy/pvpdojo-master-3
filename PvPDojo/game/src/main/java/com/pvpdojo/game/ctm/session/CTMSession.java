/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.session;

import static com.pvpdojo.util.StreamUtils.negate;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.MushroomCow;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.ctm.kits.list.CTMKit;
import com.pvpdojo.game.ctm.kits.menu.KitSelectorMenu;
import com.pvpdojo.game.ctm.lang.CTMMessageKeys;
import com.pvpdojo.game.ctm.userdata.CTMUser;
import com.pvpdojo.game.ctm.util.CTMUtil;
import com.pvpdojo.game.session.StageHandler;
import com.pvpdojo.game.session.StagedSession;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.session.RelogSession;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.arena.CTMArena;
import com.pvpdojo.session.arena.CTMArena.CTMDirection;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.entity.ClanArenaTeam;
import com.pvpdojo.session.entity.PreparedArenaTeam;
import com.pvpdojo.session.entity.RankedArenaEntity;
import com.pvpdojo.session.entity.TeamArenaEntity;
import com.pvpdojo.settings.ServerSwitchSettings;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.PlayerUtil;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.md_5.bungee.api.chat.TextComponent;

public class CTMSession extends StagedSession implements RelogSession<CTMRelogData, CTMUser> {

    private TreeMultimap<TeamArenaEntity, CaptureRegion> captureRegions = TreeMultimap.create();
    private Multimap<TeamArenaEntity, ProtectedRegion> captures = HashMultimap.create();
    private Multimap<TeamArenaEntity, CTMKit> occupiedKits = HashMultimap.create();
    private Map<UUID, CaptureRegion> runningCaptures = new HashMap<>();
    private Map<UUID, Double> damageMap = new HashMap<>();

    private Map<UUID, PreparedArenaTeam> userToTeam = new HashMap<>();

    public CTMSession(CTMArena arena, SessionType type, TeamArenaEntity teamRed, TeamArenaEntity teamBlue) {
        super(arena, type, teamRed, teamBlue);

        teamRed.setColor(CC.RED);
        teamBlue.setColor(CC.BLUE);

        if (teamRed instanceof PreparedArenaTeam) {
            ((PreparedArenaTeam) teamRed).getPotentialPlayers().forEach(uuid -> userToTeam.put(uuid, (PreparedArenaTeam) teamRed));
        }

        if (teamBlue instanceof PreparedArenaTeam) {
            ((PreparedArenaTeam) teamBlue).getPotentialPlayers().forEach(uuid -> userToTeam.put(uuid, (PreparedArenaTeam) teamBlue));
        }

        if (teamBlue instanceof ClanArenaTeam && teamRed instanceof ClanArenaTeam) {
            setRanked(true);
        }

        for (ProtectedRegion region : arena.getCaptureZonesRed()) {

            CTMDirection direction = getArena().getDirectionMap().inverse().get(region);
            Location spawn = getArena().getLocation(CTMArena.COW_SPAWN_RED + direction.name().toLowerCase());
            Location location = getArena().getLocation(CTMArena.COW_HOME_RED);

            captureRegions.put(teamRed, new CaptureRegion(region, direction, location, spawn));
        }

        for (ProtectedRegion region : arena.getCaptureZonesBlue()) {

            CTMDirection direction = getArena().getDirectionMap().inverse().get(region);
            Location spawn = getArena().getLocation(CTMArena.COW_SPAWN_BLUE + direction.name().toLowerCase());
            Location location = getArena().getLocation(CTMArena.COW_HOME_BLUE);

            captureRegions.put(teamBlue, new CaptureRegion(region, direction, location, spawn));
        }

    }

    @Override
    public void addParticipant(ArenaEntity ae) {
        if (!(ae instanceof TeamArenaEntity)) {

            CTMUtil.resetDamage(ae.getUniqueId());

            if (isPreparedTeamsOnly() && !userToTeam.containsKey(ae.getUniqueId())) {
                return;
            }

            if (userToTeam.containsKey(ae.getUniqueId())) {
                userToTeam.get(ae.getUniqueId()).addMember(ae);
            } else {
                PvPDojo.schedule(() -> CTMUtil.handleTeamSelection(ae.getPlayer())).nextTick();
            }

        }
        super.addParticipant(ae);
    }

    public TeamArenaEntity getAutoAssign() {
        TeamArenaEntity team;

        long teamRed = getTeamRed().getMembers().stream().filter(ArenaEntity::isOnline).count();
        long teamBlue = getTeamBlue().getMembers().stream().filter(ArenaEntity::isOnline).count();

        if (teamRed < teamBlue) {
            team = getTeamRed();
        } else if (teamBlue < teamRed) {
            team = getTeamBlue();
        } else {
            if (PvPDojo.RANDOM.nextBoolean()) {
                team = getTeamBlue();
            } else {
                team = getTeamRed();
            }
        }
        return team;
    }

    public boolean isPreparedTeamsOnly() {
        return getTeamRed() instanceof PreparedArenaTeam && getTeamBlue() instanceof PreparedArenaTeam;
    }

    @Override
    public StageHandler getFirstStageHandler() {
        return new PreGame(this);
    }

    @Override
    public CTMArena getArena() {
        return (CTMArena) super.getArena();
    }

    public TeamArenaEntity getTeamRed() {
        return (TeamArenaEntity) getParticipants().get(0);
    }

    public TeamArenaEntity getTeamBlue() {
        return (TeamArenaEntity) getParticipants().get(1);
    }

    public TeamArenaEntity getOtherTeam(TeamArenaEntity team) {
        return team == getTeamRed() ? getTeamBlue() : getTeamRed();
    }

    public Map<UUID, Double> getDamageMap() {
        return damageMap;
    }

    public Map<UUID, CaptureRegion> getRunningCaptures() {
        return runningCaptures;
    }

    public Multimap<TeamArenaEntity, CTMKit> getOccupiedKits() {
        return occupiedKits;
    }

    public boolean canSelectKit(TeamArenaEntity team, CTMKit kit) {
        if (isRanked() || Game.inst().isPrivate()) {
            return !occupiedKits.get(team).contains(kit);
        }
        return true;
    }

    public Multimap<TeamArenaEntity, ProtectedRegion> getCaptures() {
        return captures;
    }

    public TreeMultimap<TeamArenaEntity, CaptureRegion> getCaptureRegions() {
        return captureRegions;
    }

    @Override
    public void end(ArenaEntity... losers) {
        playSound(Sound.ENDERDRAGON_DEATH);
        super.end(losers);
        getPlayers().stream().map(User::getUser).forEach(user -> {
            user.setSpectator(true);
            user.sendMessage(CC.GREEN + "Reconnecting to next game in 8 seconds...");
            PvPDojo.schedule(() -> {
                if (user.isOnline()) {
                    ServerSwitchSettings settings = new ServerSwitchSettings(user);
                    settings.addCommand("cloud connect ctm");
                    PlayerUtil.sendToServer(user.getPlayer(), ServerType.HUB, settings);
                }
            }).createTask(8 * 20);
        });
    }

    @Override
    public void endRanked(RankedArenaEntity winner, ArenaEntity... losers) {
        RankedArenaEntity loser = (RankedArenaEntity) losers[0];

        int eloChange = calcEloChange(winner.getElo(), loser.getElo());
        winner.addElo(eloChange);
        loser.addElo(-eloChange);

        broadcast(LocaleMessage.of(CC.GREEN + winner.getName() + " (" + winner.getElo() + ") (+" + eloChange + ") " + CC.GRAY + "defeated " + CC.RED + loser.getName() + " ("
                + loser.getElo() + ") (-" + eloChange + ")"));
    }

    public static int calcEloChange(int winnerElo, int loserElo) {

        final int oldElo = winnerElo;
        final double kFactor = 20;
        final double winEstimated = 1D / (1D + Math.pow(10, ((loserElo - winnerElo) / 400D)));
        final double eloChange = kFactor * (1 - winEstimated);

        winnerElo += (int) eloChange;

        return (winnerElo - oldElo) > 0 ? (winnerElo - oldElo) : 1;
    }

    @Override
    public void onLose(ArenaEntity loser) {}

    @Override
    public void onWin(ArenaEntity winner) {
        broadcast(MessageKeys.DEATHMATCH_WON, "{winner}", winner.getLongName(), "{color}", winner.getTeam().getColor().toString());
    }

    @Override
    public void handleMatchDetails(TextComponent tc) {
        getParticipants().forEach(entity -> entity.sendMessage(tc));
    }

    @Override
    public void prepareEntity(ArenaEntity entity) {
        if (getState() == SessionState.STARTED) {
            entity.getPlayers().stream().map(User::getUser).forEach(user -> {
                user.setSpectator(false);
                user.updateTablist();
            });
            entity.getPlayers().forEach(CTMUtil::applyItems);
            entity.getPlayers().stream().map(CTMUser::getUser).filter(negate(CTMUser::hasKit)).map(User::getPlayer).forEach(player -> new KitSelectorMenu(player).open());
            entity.teleport(getSpawnLocation(entity));
        }
    }

    @Override
    public void death(ArenaEntity entity, Event parentEvent) {
        super.death(entity, parentEvent);
        getCurrentlyDead().remove(entity); // Make Relog work
    }


    @Override
    public void postDeath(ArenaEntity entity) {
        super.postDeath(entity);
        getCurrentlyDead().remove(entity); // Make Relog work
    }

    @Override
    public boolean checkEndCondition() {
        return captures.values().containsAll(getCaptureRegions().get(getTeamRed()).stream().map(CaptureRegion::getRegion).collect(Collectors.toList()))
                || captures.values().containsAll(getCaptureRegions().get(getTeamBlue()).stream().map(CaptureRegion::getRegion).collect(Collectors.toList()));
    }

    @Override
    public boolean isPersistent() {
        return false;
    }

    @Override
    public CTMRelogData createRelogData(CTMUser user) {
        return new CTMRelogData(user);
    }

    @Override
    public void onLogout(CTMUser user) {}

    @Override
    public void onTimeout(CTMUser user) {}

    @Override
    public int getRelogTimeout() {
        return -1;
    }

    @Override
    public boolean hasNPC() {
        return false;
    }

    @Override
    public boolean isAllowCombatLog() {
        return false;
    }

    public void playSound(Sound sound) {
        executeForPlayers(player -> player.playSound(player.getLocation(), sound, 100, 1));
    }

    public void tryCapture(Player player) {
        if (runningCaptures.containsKey(player.getUniqueId())) {
            User user = User.getUser(player);
            CaptureRegion region = runningCaptures.remove(player.getUniqueId());

            captures.get(user.getTeam()).add(region.getRegion());
            region.capture();

            player.removePotionEffect(PotionEffectType.SLOW);
            CTMUtil.resetDamage(player.getUniqueId());

            playSound(Sound.ENDERDRAGON_GROWL);
            CC color = user.getTeam().getColor();
            broadcast(CTMMessageKeys.COW_CAPTURE, "{team}", color + "[" + user.getTeam().getName() + "]", "{player}", color + player.getNick());
            executeForPlayers(all -> CTMUser.getUser(all).updateScoreBoard());

            triggerEnd(getOtherTeam(user.getTeam()));
        }
    }

    public void tryTakeCow(Player player, CaptureRegion region, MushroomCow cow) {
        User user = User.getUser(player);

        if (runningCaptures.containsKey(player.getUniqueId())) {
            player.sendMessage(CC.RED + "Cannot take two cows at the same time");
        } else if (runningCaptures.values().contains(region)) {
            player.sendMessage(CC.YELLOW + "Another player of your team has the cow at the moment");
        } else if (captures.get(user.getTeam()).contains(region.getRegion())) {
            player.sendMessage(CC.GREEN + "This cow is already captured");
        } else if (damageMap.get(player.getUniqueId()) < 50D) {
            player.sendMessage(CC.RED + "You need to deal more damage in order to take a cow");
        } else {
            region.takeCow(player, cow);
            runningCaptures.put(player.getUniqueId(), region);

            player.getInventory().addItem(new ItemStack(Material.MUSHROOM_SOUP), new ItemStack(Material.MUSHROOM_SOUP), new ItemStack(Material.MUSHROOM_SOUP), new ItemStack(Material.MUSHROOM_SOUP),
                    new ItemStack(Material.MUSHROOM_SOUP));
            player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 0));
            player.sendMessage(CC.GREEN + "You received 5 soups");

            CC color = user.getTeam().getColor();
            broadcast(CTMMessageKeys.COW_PICKUP, "{team}", color + "[" + user.getTeam().getName() + "]", "{player}", color + player.getNick());
            playSound(Sound.PISTON_EXTEND);

            executeForPlayers(all -> CTMUser.getUser(all).updateScoreBoard());
        }
    }

    public void removeCowFromPlayer(Player player) {
        if (runningCaptures.containsKey(player.getUniqueId())) {
            runningCaptures.remove(player.getUniqueId()).respawn();
            CC color = User.getUser(player).getTeam().getColor();
            broadcast(CTMMessageKeys.COW_DROP, "{team}", color + "[" + User.getUser(player).getTeam().getName() + "]", "{player}", player.getNick());
        }
    }

    public void handleRespawn(Player player) {
        // This is a workaround to hide an EyeHawk check from them cheaters
        ItemBuilder sword = new ItemBuilder(CTMUtil.CTM_CONTENTS[0]);
        sword.removeEnchantment(Enchantment.DAMAGE_ALL);
        sword.unbreakable();
        player.getInventory().setItem(0, sword.build());
        player.setFrozen(true);
        User.getUser(player).sendMessage(CTMMessageKeys.RESPAWN);
        PvPDojo.schedule(() -> {
            player.setFrozen(false);
            player.getInventory().setItem(0, CTMUtil.CTM_CONTENTS[0]);
        }).createTask(8 * 20);

    }

    public void handleCowClick(PlayerInteractEntityEvent e) {
        Player p = e.getPlayer();
        User user = User.getUser(p);

        getCaptureRegions().get(user.getTeam()).stream()
                           .filter(captureRegion -> captureRegion.getCow().equals(e.getRightClicked()))
                           .findFirst()
                           .ifPresent(captureRegion -> tryTakeCow(p, captureRegion, captureRegion.getCow()));

    }


}
