/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.game.kits.Kit;

public class Poseidon extends Kit {


    @Override
    public void onMove(PlayerMoveEvent e) {
        boolean water = NMSUtils.isInWater(e.getPlayer());
        if (water) {
            e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 2 * 20, 0), true);
        }
        if (water || e.getPlayer().getWorld().hasStorm()) {
            e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 2 * 20, 1), true);
        }
    }

}
