package com.pvpdojo.game.hg.kits.sidekits;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ParticleEffects;
import com.pvpdojo.util.bukkit.PlayerUtil;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class Titan extends Kit implements InteractingKit {

    public boolean titan;

    @Override
    public void click(Player player) {

        if (titan) {
            titan = false;
        }

        titan = true;

        ParticleEffects.FIRE.sendToLocation(player.getLocation().add(0, 1, 0), 0, 0, 0, 0.1f, 10);
        player.getWorld().playSound(player.getLocation(), Sound.BLAZE_HIT, 1.0F, 1.0F);

        PvPDojo.schedule(() -> {
            titan = false;

            player.getWorld().playSound(player.getLocation(), Sound.BLAZE_DEATH, 1.0F, 1.0F);
            ParticleEffects.LAVA_SPARK.sendToLocation(player.getLocation().add(0, 1, 0), 0, 0, 0, 0, 10);

        }).createTask(20 * 7);

        kitCooldown.use();
    }

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {

        if (e.isCancelled()) {
            return;
        }

        if (PlayerUtil.isRealHit((LivingEntity) e.getDamager())) {
            if (titan) {
                e.setDamage(e.getDamage() * 0.5D);
            }
        }
    }

    @Override
    public void onDamage(EntityDamageEvent e) {

        if (titan) {
            e.setCancelled(true);
        }
    }

    @Override
    public void onLeave(Player player) {
        titan = false;
    }

    @Override
    public void activate(PlayerInteractEvent event) {
    }

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public int getCooldown() {
        return 60_000;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemBuilder(Material.BEDROCK).name(CC.GREEN + "Titan").build();
    }
}
