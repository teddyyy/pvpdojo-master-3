/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.lang;

import co.aikar.locales.MessageKey;
import co.aikar.locales.MessageKeyProvider;

public enum CTMMessageKeys implements MessageKeyProvider {

    REFILL_MINIFEAST, REFILL_MINIFEAST_COUNTDOWN,
    END_ALL_LEFT,
    COW_CAPTURE, COW_PICKUP, COW_DROP,
    SIDEBAR_DAMAGE,
    RESPAWN;

    private final MessageKey key = MessageKey.of("pvpdojo-ctm." + name().toLowerCase());

    public MessageKey getMessageKey() {
        return key;
    }
}
