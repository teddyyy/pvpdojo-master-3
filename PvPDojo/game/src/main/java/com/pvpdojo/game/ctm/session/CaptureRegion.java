/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.session;

import java.util.Collections;

import org.bukkit.Location;
import org.bukkit.entity.Ambient;
import org.bukkit.entity.MushroomCow;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.jetbrains.annotations.NotNull;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.ctm.util.CTMBat;
import com.pvpdojo.session.arena.CTMArena.CTMDirection;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.Hologram;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class CaptureRegion implements Comparable<CaptureRegion> {

    public static final String CAPTURE_ENTITY = "capture_entity";

    private ProtectedRegion region;
    private MushroomCow cow;
    private Ambient bat;
    private Hologram holo;
    private CTMDirection direction;
    private Location cage, spawn;

    public CaptureRegion(ProtectedRegion region, CTMDirection direction, Location cage, Location spawn) {
        this.region = region;
        this.direction = direction;
        this.cage = cage;
        this.spawn = spawn;
    }

    public void takeCow(Player player, MushroomCow cow) {

        this.cow = cow;
        this.bat = CTMBat.spawn(player);

        bat.setPassenger(cow);

        if (!player.isProtocolHack()) {
            player.setPassenger(bat);
        }

        holo = new Hologram(CC.YELLOW + "Cow already taken");
        holo.show(spawn);
        holo.track();

    }

    public void capture() {
        holo.change(Collections.singletonList(CC.GREEN + "This cow is already captured"));
        if (cow.getVehicle() != null) {
            cow.getVehicle().eject();
        }
        cow.teleport(cage);
        bat.remove();
    }

    public void respawn() {

        if (cow != null) {
            cow.remove();
        }

        if (bat != null) {
            bat.remove();
            bat = null;
        }

        if (holo != null) {
            holo.destroy();
            holo = null;
        }

        cow = spawn.getWorld().spawn(spawn, MushroomCow.class);
        cow.setMetadata(CAPTURE_ENTITY, new FixedMetadataValue(PvPDojo.get(), null));

    }

    public MushroomCow getCow() {
        return cow;
    }

    public ProtectedRegion getRegion() {
        return region;
    }

    public CTMDirection getDirection() {
        return direction;
    }

    @Override
    public int compareTo(@NotNull CaptureRegion o) {
        if (o.getDirection() == null || direction == null) {
            return 1;
        }
        return Integer.compare(direction.ordinal(), o.getDirection().ordinal());
    }

}
