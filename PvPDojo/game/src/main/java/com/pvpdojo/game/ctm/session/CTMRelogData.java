/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.session;

import com.pvpdojo.game.ctm.CTM;
import com.pvpdojo.game.ctm.userdata.CTMUser;
import com.pvpdojo.game.session.KitGameRelogData;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;

public class CTMRelogData extends KitGameRelogData {

    public CTMRelogData(CTMUser user) {
        super(user);
    }

    @Override
    public void apply(User user) {
        CTMSession session = CTM.inst().getSession();

        if (getTeam().getMembers().stream().filter(ArenaEntity::isOnline).count() < session.getArena().getMaxPlayers() / 2) {
            super.apply(user);

            session.prepareEntity(user);
            session.handleRespawn(user.getPlayer());
        } else {
            user.sendMessage(CC.RED + "Could not reconnect, your team filled up");
        }
    }

}
