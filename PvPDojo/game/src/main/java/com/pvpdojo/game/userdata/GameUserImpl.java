/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.userdata;

import java.util.UUID;

import com.pvpdojo.game.session.StagedSession;
import com.pvpdojo.userdata.impl.UserImpl;

public abstract class GameUserImpl extends UserImpl implements GameUser {


    public GameUserImpl(UUID uuid) {
        super(uuid);
    }

    @Override
    public StagedSession getSession() {
        return (StagedSession) super.getSession();
    }

    /**
     * This method is called on every game tick
     */
    public abstract void run();

}
