package com.pvpdojo.game.hg.kits;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.userdata.User;

public class Tarzan extends Kit {

	private int uses;
	private int maxUses = 8;
	private ArrayList<Location> TARZAN = new ArrayList<Location>();

	private static final int vinesRadius = 2;
	private static final EnumSet<Material> IGNORED_MATERIALS = EnumSet.of(Material.VINE, Material.DEAD_BUSH,
			Material.RED_MUSHROOM, Material.BROWN_MUSHROOM, Material.BROWN_MUSHROOM, Material.LONG_GRASS,
			Material.SNOW);

	@Override
	public void onToggleSneak(PlayerToggleSneakEvent e) {
		Player player = e.getPlayer();
		User user = User.getUser(player);
		Block block = checkforWall(player);

		if (kitCooldown.isOnCooldown()) {
			kitCooldown.sendCooldown(user);
			return;
		}

		if (uses >= maxUses) {
			kitCooldown.use();
			uses = 0;
			return;
		}
		if (block != null && e.isSneaking()) {
			Vector vec = player.getLocation().getDirection().multiply(2.1).setY(1.2);
			player.setVelocity(vec);
			player.getWorld().playEffect(block.getLocation(), Effect.STEP_SOUND, block.getType());
			// user.cancelNextFall();
			uses++;
		}

	}

	private Block checkforWall(Player item) {

		Block itemb = item.getLocation().getBlock();
		Block cur;
		BlockFace[] faces = new BlockFace[] { BlockFace.EAST, BlockFace.WEST, BlockFace.SOUTH, BlockFace.NORTH };

		for (BlockFace face : faces) {
			cur = itemb.getRelative(face);
			if (cur.getType().isSolid() || cur.getType() == Material.GLASS) {
				return cur;
			}
		}

		return null;
	}

	@Override
	public void onDamage(EntityDamageEvent e) {
		if (e.getCause() == DamageCause.FALL) {
			e.setDamage(e.getDamage() / 1.6);
		}
	}

	@Override
	public void onMove(PlayerMoveEvent e) {
		Player player = e.getPlayer();

		if (!TARZAN.isEmpty()) {

			Iterator<Location> iterator = TARZAN.iterator();
			while (iterator.hasNext()) {

				Location loc = iterator.next();
				boolean close = false;

				if (player.getLocation().distance(loc) <= vinesRadius) {
					close = true;
				}
				if (!close) {
					loc.getBlock().setType(Material.AIR);
					iterator.remove();
					break;
				}
			}
		}

		if (player.getItemInHand().getType() == Material.VINE) {

			if (kitCooldown.isOnCooldown()) {
				// kitCooldown.sendCooldown(HGUser.getUser(player));
				return;
			}

			Location nextLoc = e.getTo();
			for (int x = -vinesRadius; x <= vinesRadius; x++) {
				for (int y = -vinesRadius; y <= vinesRadius; y++) {
					for (int z = -vinesRadius; z <= vinesRadius; z++) {
						Location loc = nextLoc.clone().add(x, y, z);

						if (loc.getBlock().getType() != Material.AIR)
							continue;

						Block north = loc.getBlock().getRelative(BlockFace.NORTH);
						Block east = loc.getBlock().getRelative(BlockFace.EAST);
						Block south = loc.getBlock().getRelative(BlockFace.SOUTH);
						Block west = loc.getBlock().getRelative(BlockFace.WEST);
						Block up = loc.getBlock().getRelative(BlockFace.UP);

						// byte data shit starts.
						byte data = 0;

						if (!north.isEmpty() && !north.isLiquid() && !ignoredBlock(north))
							data |= 0x4;

						if (!east.isEmpty() && !east.isLiquid() && !ignoredBlock(east))
							data |= 0x8;

						if (!south.isEmpty() && !south.isLiquid() && !ignoredBlock(south))
							data |= 0x1;

						if (!west.isEmpty() && !west.isLiquid() && !ignoredBlock(west))
							data |= 0x2;

						// byte data shit ends.
						if (data == 0 && up.getType() == Material.VINE) {
							TARZAN.add(loc);
							loc.getBlock().setTypeIdAndData(Material.VINE.getId(), up.getData(), false);
						}
						if (data > 0) {
							TARZAN.add(loc);
							loc.getBlock().setTypeIdAndData(Material.VINE.getId(), data, false);
						}

					}
				}
			}
		}
	}

	@Override
	public void onInteract(PlayerInteractEvent e) {
		if (e.getItem() != null && e.getItem().getType() == Material.VINE)
			e.setCancelled(true);
	}

	private boolean ignoredBlock(Block block) {
		return IGNORED_MATERIALS.contains(block.getType());
	}

	@Override
	public int getCooldown() {
		return 25000;
	}

	@Override
	public ItemStack[] getStartItems() {
		return new ItemStack[] { new ItemStack(Material.VINE) };
	}

}
