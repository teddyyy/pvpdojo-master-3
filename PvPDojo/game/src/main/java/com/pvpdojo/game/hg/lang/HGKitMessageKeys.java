/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.lang;

import co.aikar.locales.MessageKey;
import co.aikar.locales.MessageKeyProvider;

public enum HGKitMessageKeys implements MessageKeyProvider {

    KANGAROO_COMBAT_UP, KANGAROO_COMBAT_DIRECTION,
    FLASH_NO_BLOCK,
    MADMAN_EFFECT, MADMAN_NO_EFFECT, MADMAN_DAMAGE,
    JOKER_SHUFFLE, JOKER_SHUFFLED,
    CUPID_RELATION_START, CUPID_RELATION_END, CUPID_RELATION_LIMIT, CUPID_PICK,
    SOULSTEALER_MODE,
    HORCRUX_DESTROY, HORCRUX_DESTROY_DEATH, HORCRUX_DESTROY_ALL, HORCRUX_RESPAWN, NEO_CANNOT_GLADIATOR, NEO_DODGE,
    GLADIATOR_TOO_MANY_EP;

    private final MessageKey key = MessageKey.of("pvpdojo-hg-kits." + name().toLowerCase());

    @Override
    public MessageKey getMessageKey() {
        return key;
    }
}
