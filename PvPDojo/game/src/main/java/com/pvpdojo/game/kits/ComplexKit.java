/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.kits;

import java.util.HashSet;
import java.util.Set;

import com.pvpdojo.bukkit.listeners.DojoListener;

public abstract class ComplexKit extends Kit implements DojoListener {

    private static Set<Class<? extends ComplexKit>> enabled = new HashSet<>();

    public ComplexKit() {
        if (enabled.add(getClass())) {
            register();
        }
    }

}
