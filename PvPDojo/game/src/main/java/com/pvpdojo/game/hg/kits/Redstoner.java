/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.game.kits.Kit;

public class Redstoner extends Kit {

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] { new ItemStack(Material.PISTON_BASE, 8), new ItemStack(Material.REDSTONE, 64), new ItemStack(Material.DROPPER),
                new ItemStack(Material.PISTON_STICKY_BASE, 8), new ItemStack(Material.DIODE, 4), new ItemStack(Material.REDSTONE_COMPARATOR, 4), new ItemStack(Material.TNT, 4), new ItemStack(Material.REDSTONE_TORCH_ON, 4),
                new ItemStack(Material.TRIPWIRE_HOOK, 4), new ItemStack(Material.STRING, 4), new ItemStack(Material.HOPPER, 4), new ItemStack(Material.DISPENSER) };
    }

}
