package com.pvpdojo.game.hg.kits;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.kits.util.KitUtil;

public class Jumper extends ComplexKit implements InteractingKit {

	private double maxBlocks = 100.0D;

	@EventHandler
	public void onProjectileLaunch(ProjectileLaunchEvent e) {
		if (e.getEntity() instanceof EnderPearl && e.getEntity().getShooter() instanceof Player) {
			Player player = (Player) e.getEntity().getShooter();
			EnderPearl pearl = (EnderPearl) e.getEntity();
			if (KitUtil.hasKit(HGUser.getUser(player), HGKit.JUMPER)) {
				pearl.setPassenger(player);
				new BukkitRunnable() {

					int checkTimer = 20 * 10;
					boolean triggerBedRock = false;
					Location loc = player.getLocation();

					public void run() {
						if (player.isDead()) {
							this.cancel();
							return;
						}
						if (--checkTimer <= 0) {
							triggerBedRock = true;
							this.cancel();
						}
						if (pearl.getLocation().distance(loc) >= maxBlocks) {
							triggerBedRock = true;
							this.cancel();
						}
						if (pearl.isDead() || pearl.getPassenger() == null || pearl.isOnGround()) {
							Location teleport = pearl.getLocation().getBlock().getRelative(BlockFace.UP).getLocation()
									.add(0, 3.0, 0);
							teleport.setYaw(player.getLocation().getYaw());
							teleport.setPitch(player.getLocation().getPitch());
							player.teleport(teleport);
							triggerBedRock = true;
							this.cancel();
						}
						if (triggerBedRock) {
							PvPDojo.schedule(() -> bedrock(
									pearl.getLocation().getBlock().getRelative(BlockFace.UP).getLocation())).nextTick();

						}
					}

					@Override
					public void cancel() {
						pearl.remove();
						super.cancel();
					}

				}.runTaskTimer(Game.inst(), 0, 1);
			}
		}
	}

	@Override
	public void activate(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		player.launchProjectile(EnderPearl.class);
		event.setCancelled(true);
		player.updateInventory();
		kitCooldown.use();
	}

	@Override
	public void onInteract(PlayerInteractEvent e) {
		if (e.getItem() != null && e.getItem().getType() == Material.ENDER_PEARL) {
			if (getKitCooldown().isOnCooldown()) {
				e.setCancelled(true);
				e.getPlayer().updateInventory();
			}
		}

	}

	private void bedrock(Location loc) {
		for (int x = -2; x <= 2; x++) {
			for (int z = -2; z <= 2; z++) {
				final Block block = loc.getBlock().getRelative(x, 0, z);
				if (block.getType() == Material.AIR) {
					block.setType(Material.BEDROCK);
					PvPDojo.schedule(() -> block.setType(Material.AIR)).createTask(20 * 10);
				}
			}
		}
	}

	@Override
	public boolean isSendCooldown() {
		return true;
	}

	@Override
	public ItemStack getClickItem() {
		return new ItemStack(Material.ENDER_PEARL);
	}

	@Override
	public int getCooldown() {
		return 45_000;
	}
}
