/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.kits;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.game.ctm.CTM;
import com.pvpdojo.game.ctm.DamageConsumingKit;
import com.pvpdojo.game.ctm.lang.CTMKitMessageKeys;
import com.pvpdojo.game.ctm.session.CTMSession;
import com.pvpdojo.game.ctm.userdata.CTMUser;
import com.pvpdojo.game.ctm.util.CTMUtil;
import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.util.TrackerUtil;

public class Blindwave extends Kit implements InteractingKit, DamageConsumingKit {

    @Override
    public void activate(PlayerInteractEvent e) {}

    @Override
    public void click(Player player) {
        CTMUser user = CTMUser.getUser(player);
        CTMSession session = CTM.inst().getSession();

        if (consumeDamage(player)) {
            session.broadcast(CTMKitMessageKeys.BLINDWAVE_SUMMON, "{player}", user.getTeam().getColor() + player.getNick());
            TrackerUtil.getPlayerOpponents(user)
                       .forEach(all -> all.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20 * 10, 6)));
        }

    }

    @Override
    public boolean isSendCooldown() {
        return false;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.GOLDEN_CARROT);
    }

    @Override
    public double getDamageNeeded() {
        return CTMUtil.MAX_DAMAGE;
    }
}
