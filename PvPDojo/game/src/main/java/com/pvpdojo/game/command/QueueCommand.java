/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.command;

import com.pvpdojo.ServerType;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.game.kits.KitProvider;
import com.pvpdojo.game.session.QueueableStage;
import com.pvpdojo.game.session.StagedSession;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Optional;

@CommandAlias("queue|q")
public class QueueCommand extends DojoCommand {

    @CatchUnknown
    @Default
    @CommandCompletion("@kits")
    public void onQueue(User user, @Optional KitProvider kitProvider) {
        if (kitProvider != null) {
            user.getPlayer().performCommand("kit " + kitProvider.name());
        }

        if (user.getSession() instanceof StagedSession && ((StagedSession) user.getSession()).getCurrentHandler() instanceof QueueableStage) {
            QueueableStage stage = (QueueableStage) ((StagedSession) user.getSession()).getCurrentHandler();

            if (stage.queue(user)) {
                user.sendMessage(CC.GREEN + "Queued! You may now play other game modes. You will be teleported 30 seconds before the game starts");
                ServerType.HUB.connect(user.getPlayer());
            } else {
                user.sendMessage(CC.RED + "You cannot enter the queue right now");
            }
        }

    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
