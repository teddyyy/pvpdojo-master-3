/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits.rotation;

import java.util.ArrayList;
import java.util.List;

import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.userdata.Rank;

import lombok.Data;

@Data
public class KitRotationData {

    private List<HGKit> kits = new ArrayList<>();

    public List<HGKit> getByRank(Rank rank, int extra) {
        switch (rank) {
            case BLACKBELT:
                return kits.subList(0, 11 + extra);
            case BLUEBELT:
                return kits.subList(0, 10 + extra);
            case GREENBELT:
                return kits.subList(0, 9 + extra);
            case DEFAULT:
                return kits.subList(0, 8 + extra);
        }
        return kits;
    }

}
