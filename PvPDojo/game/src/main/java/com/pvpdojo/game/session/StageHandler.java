/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.session;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.userdata.GameUser;
import com.pvpdojo.util.DojoRunnable;

public abstract class StageHandler<T extends StagedSession> implements Listener, Runnable {

    protected T session;
    protected int timeSeconds;
    protected DojoRunnable timerTask;

    public StageHandler(T session) {
        this.session = session;
    }

    public StageHandler init() {
        Bukkit.getPluginManager().registerEvents(this, PvPDojo.get());
        timerTask = PvPDojo.schedule(this);
        timerTask.createTimer(20, 20, -1);
        return this;
    }

    public abstract boolean isCountdown();

    public boolean shouldCountDown() {
        return true;
    }

    /**
     * This method is called every 20 ticks ( 1 second )
     */
    @Override
    public void run() {
        if (isCountdown()) {
            if (shouldCountDown() && --timeSeconds == 0) {
                advance();
            }
        } else {
            timeSeconds++;
        }

        // Run tick method for every user
        Bukkit.getOnlinePlayers().stream().map(GameUser::getUser).forEach(GameUser::runSafely);
    }

    public void advance() {
        HandlerList.unregisterAll(this);
        timerTask.cancel();
        if (getNextHandler() != null) {
            session.setCurrentHandler(getNextHandler().init());
        }
    }

    public int getTimeSeconds() {
        return timeSeconds;
    }

    public void setTimeSeconds(int timeSeconds) {
        this.timeSeconds = timeSeconds;
    }

    public abstract StageHandler getNextHandler();

    @EventHandler
    public void onListPing(ServerListPingEvent e) {
        if (!Game.inst().isPrivate()) {
            e.setMotd(DBCommon.GSON.toJson(Game.inst().createSessionInfo()));
        }
    }

}
