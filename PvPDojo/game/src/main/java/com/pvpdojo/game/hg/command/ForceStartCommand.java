/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.command;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.lang.HGMessageKeys;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("forcestart|fs")
public class ForceStartCommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onForceStart() {
        if (HG.inst().getSession().getState() == SessionState.PREGAME && HG.inst().getSession().getCurrentHandler().getTimeSeconds() > 15) {
            HG.inst().getSession().getCurrentHandler().setTimeSeconds(15);
            HG.inst().getSession().broadcast(HGMessageKeys.FORCE_START);
        }
    }

    @Override
    public Rank getRank() {
        return Rank.YOUTUBER;
    }
}
