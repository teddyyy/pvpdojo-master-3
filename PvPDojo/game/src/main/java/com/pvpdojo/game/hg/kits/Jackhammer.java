/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class Jackhammer extends Kit {

    private int uses;
    private boolean inProgress;

    @Override
    public void onBreak(BlockBreakEvent e) {
        if (!inProgress && HG.inst().getSession().getState() == SessionState.STARTED && e.getPlayer().getItemInHand().getType() == getStartItems()[0].getType() && !e.isCancelled()) {
            if (!kitCooldown.isOnCooldown()) {
                Block block = e.getBlock();
                BlockFace face = e.getPlayer().getLocation().getPitch() < 0 ? BlockFace.UP : BlockFace.DOWN;
                inProgress = true;
                while ((block = block.getRelative(face)).getType() != Material.BEDROCK && block.getY() > 0) {
                    BlockBreakEvent event = new BlockBreakEvent(block, e.getPlayer());
                    Bukkit.getPluginManager().callEvent(event);
                    if (!event.isCancelled()) {
                        block.setType(Material.AIR);
                    }
                }
                if (++uses > 5) {
                    uses = 0;
                    kitCooldown.use();
                }
                inProgress = false;
            } else {
                kitCooldown.sendCooldown(User.getUser(e.getPlayer()));
            }
        }
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] { new ItemBuilder(Material.STONE_AXE).unbreakable().build() };
    }

    @Override
    public int getCooldown() {
        return 15_000;
    }
}
