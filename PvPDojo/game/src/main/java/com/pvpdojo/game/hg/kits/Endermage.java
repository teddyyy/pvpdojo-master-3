/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.kits.sidekits.Titan;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.BlockInteractingKit;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.DojoRunnable;
import com.pvpdojo.util.LimitedQueue;
import com.pvpdojo.util.TimeUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemUtil;
import net.minecraft.server.v1_7_R4.Entity;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.entity.FishHook;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.pvpdojo.util.StreamUtils.negate;

public class Endermage extends ComplexKit implements BlockInteractingKit {

    private static final List<EndermagePortal> latestPortals = new ArrayList<>();
    private LimitedQueue<Long> lastUses = new LimitedQueue<>(5);

    @Override
    public void activate(PlayerInteractEvent event) {
    }

    @Override
    public void onRightClick(PlayerInteractEvent e) {
        Block block = e.getClickedBlock();

        if (!getKitCooldown().isOnCooldown()) {
            if (lastUses.isFull() && !TimeUtil.hasPassed(lastUses.getFirst(), TimeUnit.SECONDS, 25)) {
                getKitCooldown().use();
                lastUses.clear();
            }
        }

        if (Gladiator.isInGladiator(e.getPlayer())) {
            return;
        }

        if (block.getRelative(BlockFace.UP).getType() != Material.AIR || block.getLocation().add(0, 2, 0).getBlock().getType() != Material.AIR) {
            e.getPlayer().sendMessage(CC.RED + "There has to be 2 blocks of air above your portal");
            return;
        }

        if (block.getRelative(BlockFace.UP).getType() != Material.AIR || block.getLocation().add(0, 2, 0).getBlock().getType() != Material.AIR) {
            e.getPlayer().sendMessage(CC.RED + "There has to be 2 blocks of air above your portal");
            return;
        }

        if (block.getType() == Material.ENDER_PORTAL_FRAME || block.getType() == Material.BEDROCK
                || block.getType() == Material.WOODEN_DOOR || block.getType() == Material.IRON_DOOR_BLOCK
                || block.getType() == Material.PISTON_BASE || block.getType() == Material.PORTAL) {
            return;
        }

        Titan kit = null;
        if (HGUser.getUser(e.getPlayer()).getSideKit().getKit() instanceof Titan) {
            kit = (Titan) HGUser.getUser(e.getPlayer()).getSideKit().getKit();
        }
        if (kit != null && !kit.getKitCooldown().isOnCooldown()) {
            kit.getKitCooldown().use();
            e.getPlayer().sendMessage(CC.RED + "Your Titan ability has been temporarily disabled");
        }

        e.getPlayer().getInventory().remove(getClickItem().getType());
        EndermagePortal portal = new EndermagePortal(HGUser.getUser(e.getPlayer()), block);
        latestPortals.add(portal);
        portal.createTimer(1, -1);
        e.setCancelled(true); // Bypass block glitching is not allowed

    }


    private class EndermagePortal extends DojoRunnable implements Runnable {

        private HGUser user;
        private Block block;
        private BlockState previousState;

        private int counter;

        public EndermagePortal(HGUser user, Block block) {
            this.user = user;
            this.previousState = block.getState();
            this.block = block;
            block.setType(Material.ENDER_PORTAL_FRAME);

            this.runnable = this;
        }

        @Override
        public void run() {

            if (++counter >= 100) {
                clean();
                cancel();
                return;
            }

            List<Player> nearbyPlayers = TrackerUtil.getNearbyIngame(BukkitUtil.getNearbyEntities(block.getLocation(), 4, 256, 4))
                    .filter(negate(user.getPlayer()::equals))
                    .map(HGUser::getUser)
                    .filter(user -> !KitUtil.hasKit(user, HGKit.ENDERMAGE) && !KitUtil.hasKit(user, HGKit.NEO))
                    .map(User::getPlayer)
                    .filter(negate(Gladiator::isInGladiator))
                    .filter(player -> Math.abs(player.getLocation().getY() - block.getLocation().getY()) >= 5)
                    .collect(Collectors.toList());

            if (!nearbyPlayers.isEmpty()) {
                nearbyPlayers.add(user.getPlayer()); // Readd mage to teleport them too
                mage(nearbyPlayers);
                cancel();
            }

        }

        @Override
        public void cancel() {
            super.cancel();
            latestPortals.remove(this);
        }

        public void mage(List<Player> toMage) {
            lastUses.add(System.currentTimeMillis());
            clean();

            Location teleport = block.getRelative(BlockFace.UP).getLocation().add(0.5, 0, 0.5);

            // Don't make stupid stuck traps
            teleport.getBlock().setType(Material.AIR);
            teleport.getBlock().getRelative(BlockFace.UP).setType(Material.AIR);

            for (Player player : toMage) {
                player.getNearbyEntities(3, 3, 3).stream().filter(entity -> entity instanceof FishHook).map(NMSUtils::get).forEach(Entity::die);
                HGUser.getUser(player).setInvincibleSeconds(5);
                player.teleport(teleport);
                player.sendMessage(CC.RED + "You have been teleported by an Endermage. You are invincible for 5 seconds.");
            }
        }

        public void clean() {
            ItemUtil.addItemWhenFree(user.getPlayer(), getClickItem());
            previousState.update(true, false);
        }

    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (latestPortals.stream().anyMatch(endermagePortal -> endermagePortal.block.equals(e.getBlock()))) {
            e.setCancelled(true);
        }
    }

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.PORTAL);
    }

    @Override
    public int getCooldown() {
        return 15_000;
    }
}
