package com.pvpdojo.game.hg.kits;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.kits.util.KitUtil;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;
import org.spigotmc.event.entity.EntityDismountEvent;

public class Glider extends ComplexKit {

    private static final String fall = "FALL";

    @Override
    public void onMove(PlayerMoveEvent e) {
        Player player = e.getPlayer();
        if (player.getItemInHand().getType() == getStartItems()[0].getType()) {

            if (player.getLocation().subtract(0, 1, 0).getBlock().getType() == Material.AIR
                    && player.getLocation().subtract(0, 2, 0).getBlock().getType() == Material.AIR) {

                double glide = 0.76;
                double y = -0.03;
                Vector vec = player.getLocation().clone().getDirection()
                        .multiply(HGUser.getUser(player).isInCombat() ? glide / 2.5 : glide).setY(y);
                player.setVelocity(vec);

            } else {
                if (!player.isOnGround()) {
                    player.setMetadata(fall, new FixedMetadataValue(PvPDojo.get(), true));
                    if (player.getPassenger() != null) {
                        player.getPassenger().setMetadata(fall, new FixedMetadataValue(PvPDojo.get(), true));
                    }
                } else {
                    if (player.getPassenger() != null) {
                        player.getPassenger().setMetadata(fall, new FixedMetadataValue(PvPDojo.get(), true));
                    }
                    player.removeMetadata(fall, PvPDojo.get());
                }
            }
        }
    }


    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (e.getCause() == DamageCause.FALL && e.getEntity().hasMetadata(fall)) {
            e.setCancelled(true);
            e.getEntity().removeMetadata(fall, PvPDojo.get());
        }
    }

    @EventHandler
    public void onPlayerInteractGlider(PlayerInteractEntityEvent e) {
        Player player = e.getPlayer();
        if (e.getRightClicked() instanceof Player) {
            Player clicked = (Player) e.getRightClicked();
            if (KitUtil.hasKit(HGUser.getUser(clicked), HGKit.GLIDER)) {
                if (clicked.getItemInHand().getType() == getStartItems()[0].getType()) {
                    if (HGUser.getUser(player).isIngame()) {
                        clicked.setPassenger(player);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onDismount(EntityDismountEvent e) {
        if (e.getEntity() instanceof Player && e.getDismounted() instanceof Player) {
            Player dismount = (Player) e.getDismounted();
            if (KitUtil.hasKit(HGUser.getUser(dismount), HGKit.GLIDER)) {
                if (e.getEntity().hasMetadata(fall)) {
                    e.getEntity().removeMetadata(fall, PvPDojo.get());
                }
            }
        }
    }

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {
        Player p = (Player) e.getDamager();
        if (!p.isEmpty() && p.getPassenger() == e.getEntity()) {
            p.eject();
        }
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[]{new ItemStack(Material.FEATHER)};
    }
}
