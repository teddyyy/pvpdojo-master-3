/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.kits;

import org.bukkit.Material;
import org.bukkit.entity.FishHook;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.ctm.lang.CTMKitMessageKeys;
import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.userdata.User;

public class Achilles extends Kit implements InteractingKit {

    private boolean activated;

    @Override
    public void activate(PlayerInteractEvent event) {
        getKitCooldown().use();
        activated = true;
        User user = User.getUser(event.getPlayer());
        user.sendMessage(CTMKitMessageKeys.ACHILLES_ACTIVATE);
        PvPDojo.schedule(() -> {
            user.sendMessage(CTMKitMessageKeys.ACHILLES_EXPIRE);
            activated = false;
        }).createTask(8 * 20);
    }

    @Override
    public void onDamage(EntityDamageEvent e) {
        if (activated) {
            if (e instanceof EntityDamageByEntityEvent && (e.getCause() == DamageCause.ENTITY_ATTACK
                    || (e.getCause() == DamageCause.PROJECTILE && ((EntityDamageByEntityEvent) e).getDamager() instanceof FishHook))) {
                EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
                if (event.getDamager() instanceof Player) {
                    User.getUser((Player) event.getDamager()).sendMessage(CTMKitMessageKeys.ACHILLES_EFFECT);
                }
                e.setCancelled(true);
            } else {
                e.setDamage(1000);
            }
        }
    }

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.COAL);
    }

    @Override
    public int getCooldown() {
        return 60_000;
    }
}
