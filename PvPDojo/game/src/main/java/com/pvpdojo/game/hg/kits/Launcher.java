/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.game.kits.Kit;

public class Launcher extends Kit {

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] { new ItemStack(Material.SPONGE, 20) };
    }
}
