/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits.persistent;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.mysql.SQLBuilder;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.bukkit.CC;
import lombok.Getter;
import org.bukkit.Bukkit;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PersistentKits {

    private PersistentData db;
    @Getter
    private List<HGKit> ownedKits = new ArrayList<>();
    @Getter
    private boolean loaded;

    public PersistentKits(PersistentData db) {
        this.db = db;
    }

    public List<HGKit> getAvailableKits() {
        if (db.getRank().inheritsRank(Rank.SENSEI) || HG.inst().getSettings().isFreekits()) {
            return Stream.of(HGKit.values()).filter(HGKit::isEnabled).collect(Collectors.toList());
        }
        return Stream.concat(
                Stream.of(HG.inst().getCurrentRotation().getByRank(db.getRank(), getExtraRotation()), ownedKits)
                     .flatMap(Collection::stream),
                     Stream.of(HGUser.getUser(db.getUUID()).getBonusKit()))
                     .filter(Objects::nonNull)
                     .distinct()
                     .filter(HGKit::isEnabled)
                     .collect(Collectors.toList());
    }

    public int getExtraRotation() {
        int extraRotation = 0;
        User user = User.getUser(db.getUUID());
        if (user != null ) {
            if ( user.getFastDB().isBastiSub()) {
                extraRotation++;
            }
            if (user.getFastDB().isDojoSub()) {
                extraRotation++;
            }
        }
        return extraRotation;
    }

    public void addKit(HGKit kit) throws SQLException {
        if (ownedKits.contains(kit)) {
            return;
        }
        try (SQLBuilder sql = new SQLBuilder()) {
            int affected = sql.insert(Tables.HG_KITS, "uuid, kit").values("?,?").executeUpdate(db.getUUID().toString(), kit.toString());
            if (affected == 0) {
                throw new SQLException("No row affected");
            }
            // Concurrency
            List<HGKit> kits = new ArrayList<>(ownedKits);
            kits.add(kit);
            this.ownedKits = kits;
        }
    }

    public void pull() {
        try (SQLBuilder sql = new SQLBuilder()) {
            sql.select(Tables.HG_KITS, "kit").where("uuid = ?").executeQuery(db.getUUID().toString());
            List<HGKit> kits = new ArrayList<>();
            while (sql.next()) {
                String kitString = sql.getString("kit");
                try {
                    HGKit kit = HGKit.valueOf(kitString);
                    kits.add(kit);
                } catch (IllegalArgumentException ex) {
                    Log.exception(db.getUUID() + " has an invalid kit: " + kitString, ex);
                }
            }
            this.ownedKits = kits;
            this.loaded = true;
        } catch (SQLException ex) {
            Log.exception(ex);
            if (Bukkit.getPlayer(db.getUUID()) != null) {
                Bukkit.getPlayer(db.getUUID()).sendMessage(PvPDojo.WARNING + CC.DARK_RED + "An error occurred within your user data report this issue to a staff member");
            }
        }
    }

}
