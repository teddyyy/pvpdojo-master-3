package com.pvpdojo.game.hg.kits;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimaps;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.hg.util.FeastUtil;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.userdata.User;
import net.minecraft.server.v1_7_R4.TileEntityChest;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_7_R4.block.CraftChest;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Consumer;

public class Gravedigger extends ComplexKit {

    private ListMultimap<UUID, Location> CHESTS = Multimaps.newListMultimap(new HashMap<>(), ArrayList::new);
    private static final EnumSet<BlockFace> FACES = EnumSet.of(BlockFace.SOUTH, BlockFace.NORTH, BlockFace.EAST, BlockFace.WEST, BlockFace.SOUTH_EAST,
            BlockFace.SOUTH_WEST, BlockFace.NORTH_EAST, BlockFace.NORTH_WEST);

    private boolean delay;

    @Override
    public void onKill(PlayerDeathEvent e) {
        Player target = e.getEntity();
        Player player = target.getKiller();

        List<ItemStack> loot = new ArrayList<>(e.getDrops());

        Block block = target.getLocation().getBlock();
        Block block2 = block.getRelative(getFace(block));

        while (!block.isEmpty() && block.getType() == Material.BEDROCK) {
            block = block.getRelative(BlockFace.UP);
        }

        while (block.getState() instanceof InventoryHolder || block2.getState() instanceof InventoryHolder ||
                block.getRelative(BlockFace.DOWN).getState() instanceof InventoryHolder ||
                block2.getRelative(BlockFace.DOWN).getState() instanceof InventoryHolder) {

            block = block2;
            block2 = block.getRelative(getFace(block));
        }

        block.setType(Material.CHEST);

        InventoryHolder chest = (InventoryHolder) block.getState();
        InventoryHolder chest2 = chest;

        if (e.getDrops().size() >= 27) {//Spawn double chest
            block2.setType(Material.CHEST);
            chest2 = (InventoryHolder) block2.getState();
        }


        InventoryHolder finalChest = chest2;
        HGUser user = HGUser.getUser(target);

        if (user.getKit() instanceof SoulStealer) {
            loot.clear();

            SoulStealer kit = (SoulStealer) user.getKit();
            loot.addAll(Arrays.asList(kit.items));
            loot.addAll(Arrays.asList(kit.armor));

        }

        for (int i = 0; i <= 27 && i < e.getDrops().size(); i++) {
            chest.getInventory().addItem(loot.get(i));
        }

        for (int i = 28; i < e.getDrops().size(); i++) {
            finalChest.getInventory().addItem(loot.get(i));
        }

        for (int i = 0; i < 3; i++) {
            chest.getInventory().addItem(FeastUtil.MINI_FEAST.generateRandomItem());
        }

        CHESTS.put(player.getUniqueId(), block.getLocation());

        if (e.getDrops().size() >= 27) {
            CHESTS.put(player.getUniqueId(), block2.getLocation());
        }

        HGUser.getUser(player.getUniqueId()).setInvincibleSeconds(15);

        setName(block, target.getNick() + "'s Grave");

        createGrave(player, block.getLocation().subtract(0, 1, 0).getBlock(), true);

        if (block2.getType() == Material.CHEST) {
            createGrave(player, block2.getLocation().subtract(0, 1, 0).getBlock(), false);
        }


        e.getDrops().clear();

    }

    private BlockFace getFace(Block block) {
        for (BlockFace face : FACES) {
            if (block.getRelative(face).getType() == Material.AIR) {
                return face;
            }
        }
        return BlockFace.EAST;
    }

    private void createGrave(Player player, Block block, boolean explode) {

        if (block.getType().isSolid() && block.getType() != Material.OBSIDIAN || block.getType() != Material.BEDROCK) {
            block.setType(Material.GRAVEL);
        }

        int min = 25, max = 65;
        int expireTime = (int) ((Math.random() * ((max - min) + 1)) + min) * 20;

        if (explode) {
            PvPDojo.schedule(() -> {

                BukkitUtil.createExplosionByEntity(player, block.getLocation().add(0, 1, 0), 2f, true, false);

                block.setType(Material.AIR);

                if (block.getLocation().add(0, 1, 0).getBlock().getType() == Material.CHEST) {
                    block.getLocation().add(0, 1, 0).getBlock().setType(Material.AIR);
                }

                block.getWorld().strikeLightningEffect(block.getLocation());

                Zombie soul = (Zombie) block.getWorld().spawnEntity(block.getLocation().add(0, 1, 0), EntityType.ZOMBIE);

                soul.setBaby(false);
                soul.setMaxHealth(25);
                soul.setHealth(25);
                soul.getEquipment().setHelmet(new ItemStack(Material.PUMPKIN));


            }).createTask(expireTime);
        } else {//Chest won't explode but we need to remove it *if it still exists*

            PvPDojo.schedule(() -> {
                if (block.getType() == Material.CHEST) {
                    block.setType(Material.AIR);
                }
            }).createTask(expireTime);
        }
    }


    private void setName(Block block, String to) {
        CraftChest chest = (CraftChest) block.getState();

        try {
            Field inventoryField = chest.getClass().getDeclaredField("chest");
            inventoryField.setAccessible(true);
            TileEntityChest teChest = ((TileEntityChest) inventoryField.get(chest));
            teChest.a(to); //Changes title of the chest.
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {

        if (e.isCancelled()) {
            return;
        }

        if (HG.inst().getSession().getState() == SessionState.PREGAME) {
            return;
        }

        if (e.getClickedBlock() == null) {
            return;
        }

        if (e.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }

        if (e.getClickedBlock().getType() == Material.CHEST) {

            streamThingy(e.getPlayer(), e.getClickedBlock().getLocation(), players -> {

                if (!delay) {
                    BukkitUtil.createExplosionByEntity(players, e.getClickedBlock().getLocation(), 2F, false, false);
                    delay = true;
                    PvPDojo.schedule(() -> delay = false).createTask(40);
                }

                e.setCancelled(true);
            });

        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        streamThingy(e.getPlayer(), e.getBlock().getLocation(), player -> e.setCancelled(true));
    }

    private void streamThingy(Player player, Location loc, Consumer<Player> consumer) {
        TrackerUtil.getIngame()
                .filter(target -> target != player)
                .filter(target -> CHESTS.containsEntry(target.getUniqueId(), loc))
                .filter(target -> loc.getBlock().getType() == Material.CHEST)
                .map(HGUser::getUser)
                .filter(user -> !user.isAdminMode() && !user.isSpectator() && !user.isVanish() && !user.isFrozenByStaff())
                .map(User::getPlayer)
                .forEach(consumer);
    }

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {

        HGUser user = HGUser.getUser((Player) e.getDamager());

        if (user.isInvincible()) {
            user.setInvincibleSeconds(1);
        }
    }

}
