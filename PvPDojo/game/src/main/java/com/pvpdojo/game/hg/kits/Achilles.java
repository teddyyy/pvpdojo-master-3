/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.CC;

public class Achilles extends Kit {

    @Override
    public void onPlayerDamagedByPlayer(EntityDamageByEntityEvent e) {
        Player damager = (Player) e.getDamager();
        switch (damager.getItemInHand().getType()) {
            case GOLD_SWORD:
            case STONE_SWORD:
            case IRON_SWORD:
            case DIAMOND_SWORD:
                e.setDamage(e.getDamage() * 0.3);
                damager.sendMessage(CC.RED + "Huh, the " + CC.ITALIC + "wood " + CC.RED + "handle did more damage than the blade to this guy...");
                break;
            case WOOD_SWORD:
            case WOOD_AXE:
            case WOOD_SPADE:
                e.setDamage(e.getDamage() * 2);
                if (PvPDojo.RANDOM.nextBoolean()) {
                    damager.getItemInHand().setDurability((short) (damager.getItemInHand().getDurability() - 1));
                }
                break;
        }
    }
}
