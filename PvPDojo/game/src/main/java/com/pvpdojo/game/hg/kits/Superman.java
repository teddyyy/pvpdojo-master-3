/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ParticleEffects;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Superman extends Kit implements InteractingKit {

    private ItemStack supermanClickItem = new ItemBuilder(Material.REDSTONE).name(CC.RED + "Heat Vision").build();
    private ItemStack seekerClickItem = new ItemBuilder(Material.EYE_OF_ENDER).name(CC.BLUE + "X-Ray Vision").build();


    private static final EnumSet SEEKER_EYE = EnumSet.of(Material.NETHERRACK, Material.ICE, Material.PACKED_ICE, Material.SNOW, Material.SNOW_BLOCK, Material.GRASS, Material.DIRT,
            Material.GRAVEL, Material.SAND, Material.SANDSTONE, Material.MYCEL, Material.STONE, Material.STAINED_CLAY, Material.HARD_CLAY);

    private boolean viewing;

    @Override
    public void onInteractEntity(PlayerInteractEntityEvent e) {

        if (!(e.getRightClicked() instanceof Player)) {
            return;
        }

        Player target = (Player) e.getRightClicked();

        if (!e.getPlayer().getItemInHand().isSimilar(seekerClickItem)) {
            return;
        }

        if (checkUsage(this, User.getUser(e.getPlayer()))) {
            viewing = true;
            e.getPlayer().openInventory(target.getInventory());
            e.getPlayer().sendMessage(CC.GRAY + "Viewing inventory of " + CC.BLUE + target.getNick());
            getKitCooldown().use();
        }

    }

    @Override
    public void onInvClose(InventoryCloseEvent e) {
        if (viewing) {
            viewing = false;
        }
    }

    @Override
    public void onInvClick(InventoryClickEvent e) {
        if (viewing) {
            e.setCancelled(true);
        }
    }

    @Override
    public void onLeave(Player player) {
        if (viewing) {
            player.closeInventory();
            viewing = false;
        }
    }

    @Override
    public void onInteract(PlayerInteractEvent e) {

        Player player = e.getPlayer();

        if (player.getItemInHand().isSimilar(seekerClickItem)) {

            e.setCancelled(true);

            if (checkUsage(this, User.getUser(player))) {
                if (e.getAction() != Action.RIGHT_CLICK_BLOCK) {
                    return;
                }
                if (viewing) {
                    return;
                }
                onSeeker(player);

                getKitCooldown().use();
            }

        }


    }

    private static final HashSet<Byte> ALL_MATERIALS = new HashSet<>();

    static {
        for (Material material : Material.values()) {
            ALL_MATERIALS.add((byte) material.getId());
        }
    }

    private void onSeeker(Player player) {

        List<Block> blocks = player.getLineOfSight(ALL_MATERIALS, 70);
        Iterator<Block> preIt = blocks.iterator();
        Set<Block> addedBlocks = new HashSet<>();
        Set<Block> glowStone = new HashSet<>();

        int radius = 5;

        while (preIt.hasNext()) {

            Block block = preIt.next();

            for (int x = -radius + 1; x < radius; x++) {
                for (int y = -radius + 1; y < radius; y++) {
                    for (int z = -radius + 1; z < radius; z++) {
                        Block relative = block.getRelative(x, y, z);
                        addedBlocks.add(relative);
                    }
                }
            }
        }

        preIt = blocks.iterator();
        int border = 6;


        while (preIt.hasNext()) {

            Block block = preIt.next();

            for (int x = -(border); x <= border; x++) {
                for (int z = -(border); z <= border; z++) {
                    Block relative = block.getRelative(x, 0, z);
                    if (Math.abs(z) == (border - 2) && Math.abs(x) == (border - 1)) {
                        glowStone.add(relative);
                    } else if (Math.abs(z) == (border - 1) && Math.abs(x) == (border - 2)) {
                        glowStone.add(relative);
                    }
                }
            }
        }

        blocks.addAll(addedBlocks);
        Iterator<Block> it = blocks.iterator();

        while (it.hasNext()) {
            final Block block = it.next();
            graduateBlockChangeForAll(block, Material.GLASS);
        }

        it = glowStone.iterator();

        while (it.hasNext()) {
            final Block block = it.next();
            if (blocks.contains(block)) {
                continue;
            }
            graduateBlockChangeForAll(block, Material.GLOWSTONE);
        }
    }

    private static final int CONVERSION_SLOW = 4;

    private void graduateBlockChangeForAll(Block block, Material material) {
        if (!SEEKER_EYE.contains(block.getType())) {
            return;
        }

        for (Player players : Bukkit.getOnlinePlayers()) {
            double distance = block.getLocation().distance(players.getLocation());

            if (distance < 120) {
                PvPDojo.schedule(() -> players.sendBlockChange(block.getLocation(), material, (byte) 0))
                        .createTask((int) (distance * CONVERSION_SLOW));
                PvPDojo.schedule(() -> players.sendBlockChange(block.getLocation(), block.getType(), block.getData()))
                        .createTask((int) ((-distance * CONVERSION_SLOW) + (64 * CONVERSION_SLOW) + (64 * CONVERSION_SLOW) + 100));
            }
        }
    }

    //Different from KitPvP.
    private void onSuperman(Player player) {

        player.getLocation().getWorld().playSound(player.getLocation(), Sound.EXPLODE, 2.0F, -1.0F);
        Location eyeLoc = player.getEyeLocation().add(player.getEyeLocation().getDirection().multiply(1.0));
        ParticleEffects.LARGE_EXPLODE.sendToLocation(eyeLoc, 0, 0, 0, 0, 1);

        for (int i = 0; i < 25; i++) {
            Location location = player.getTargetBlock(null, i).getLocation();

            location.getWorld().playEffect(location, Effect.STEP_SOUND, Material.REDSTONE_BLOCK);
            ParticleEffects.LAVA_SPARK.sendToLocation(location, 0, 0, 0, 0, 1);

            TrackerUtil.getIngame()
                    .filter(target -> target != player)
                    .filter(target -> target.getLocation().distance(location) <= 1.65)
                    .forEach(target -> {
                        NMSUtils.damageEntity(target, player, EntityDamageEvent.DamageCause.FIRE, 4);
                        target.setFireTicks(20 * 5);
                    });
        }
    }

    @Override
    public int getCooldown() {
        return 45_000;
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[]{seekerClickItem};
    }

    @Override
    public void activate(PlayerInteractEvent e) {
        onSuperman(e.getPlayer());
        getKitCooldown().use();
    }

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        return supermanClickItem;
    }
}
