/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.command;

import static com.pvpdojo.util.StringUtils.getReadableSeconds;

import org.bukkit.command.CommandSender;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.game.Game;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("gametime")
public class GameTimeCommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onGameTime(CommandSender sender, int time) {
        if (Game.inst().getGame().getSession().getCurrentHandler().isCountdown()) {
            time++;
        }
        Game.inst().getGame().getSession().getCurrentHandler().setTimeSeconds(Math.abs(time));
        sender.sendMessage(PvPDojo.PREFIX + "Game time changed to " + CC.GREEN + getReadableSeconds(Math.abs(time)));
    }

    @Override
    public Rank getRank() {
        return Rank.DEVELOPER;
    }
}
