/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerVelocityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class Anchor extends ComplexKit {

    private static final Set<UUID> CANCEL_NEXT_VELOCITY = new HashSet<>();

    @Override
    public void onEntityAttack(EntityDamageByEntityEvent e) {
        anchor(e);
    }

    @Override
    public void onDamage(EntityDamageEvent e) {
        if (e instanceof EntityDamageByEntityEvent) {
            if (((EntityDamageByEntityEvent) e).getDamager() instanceof Player &&
                    KitUtil.hasKit(HGUser.getUser((Player) ((EntityDamageByEntityEvent) e).getDamager()), HGKit.NEO)) {
                return;
            }
            anchor((EntityDamageByEntityEvent) e);
        }
    }

    @EventHandler
    public void onVelocity(PlayerVelocityEvent e) {
        if (CANCEL_NEXT_VELOCITY.remove(e.getPlayer().getUniqueId())) {
            e.setVelocity(new Vector());
        }
    }

    private void anchor(EntityDamageByEntityEvent e) {
        if ((e.getEntity() instanceof LivingEntity && !PlayerUtil.isRealHit((LivingEntity) e.getEntity()))
                || e.isCancelled()) {
            return;
        }

        if (e.getEntity() instanceof Player) {
            if (KitUtil.hasKit(HGUser.getUser((Player) e.getEntity()), HGKit.NEO)) {
                return;
            }
            CANCEL_NEXT_VELOCITY.add(e.getEntity().getUniqueId());
        } else {
            PvPDojo.schedule(() -> e.getEntity().setVelocity(new Vector())).nextTick();
        }

        if (e.getEntity() instanceof Player) {
            ((Player) e.getEntity()).playSound(e.getEntity().getLocation(), Sound.ANVIL_LAND, 0.1F, 1F);
        }
        if (e.getDamager() instanceof Player) {
            ((Player) e.getDamager()).playSound(e.getEntity().getLocation(), Sound.ANVIL_LAND, 0.1F, 1F);
        }
    }

    @Override
    public void onInvClick(InventoryClickEvent e) {
        if (e.getWhoClicked() instanceof Player) {
            if (e.getCurrentItem() != null && e.getSlotType() == SlotType.ARMOR && (e.getRawSlot() == 5 || e.getRawSlot() == 6)) {// Helmet RawSlot = 5, Slot = 39
                if (HG.inst().getSession().getCurrentHandler().getTimeSeconds() <= HG.inst().getSettings().getSecondsTillFeast()) {
                    e.setCancelled(true);
                }
            }

        }
    }

    @Override
    public void onDeath(PlayerDeathEvent e) {
        e.getDrops().removeIf(item -> item.getType() == Material.ANVIL);
    }

    @Override
    public ItemStack[] getStartArmor() {
        return new ItemStack[] { null, null, new ItemStack(Material.ANVIL), new ItemStack(Material.ANVIL) };// Boots, Leggings, Chestplate, Helmet
    }
}
