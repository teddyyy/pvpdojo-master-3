/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.naturalevents;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.util.TimeUtil;
import com.pvpdojo.util.bukkit.CC;

public class NaturalEventManager {

    private static final NaturalEventManager INST = new NaturalEventManager();

    public static NaturalEventManager inst() {
        return INST;
    }

    private final List<Supplier<NaturalEvent>> naturalEvents;
    private NaturalEvent currentEvent;
    private long lastKill;

    NaturalEventManager() {
        naturalEvents = Arrays.asList(AcidRain::new, SnowStorm::new, HeatWave::new, ThunderStorm::new, Storm::new, BeastAttack::new, MeteorShower::new, EarthQuake::new, SupplyDrop::new);
        reset();
    }

    private int nextMinute;
    private int probability;
    private int currentEventDuration;

    public void tick() {
        if (currentEvent != null) {
            currentEvent.tick();
            if (++currentEventDuration >= currentEvent.getDuration()) {
                cancelEvent();
            }
        } else if (TimeUtil.hasPassed(lastKill, TimeUnit.MINUTES, 3) && HG.inst().getSession().getCurrentHandler().getTimeSeconds() > 300) {
            if (nextMinute <= 0 && PvPDojo.RANDOM.nextInt(100) < probability) {
                while (true) {
                    if (spawnEvent(naturalEvents.get(PvPDojo.RANDOM.nextInt(naturalEvents.size())).get())) {
                        break;
                    }
                }
            } else if (nextMinute <= 0) {
                probability += 12;
                nextMinute = 60;
            } else {
                nextMinute--;
            }
        }
    }

    private void reset() {
        currentEvent = null;
        probability = 0;
        nextMinute = 0;
        currentEventDuration = 0;
    }

    public void updateLastKill() {
        lastKill = System.currentTimeMillis();
        if (currentEvent == null) {
            reset();
        }
    }

    public List<Supplier<NaturalEvent>> getNaturalEvents() {
        return naturalEvents;
    }

    public boolean spawnEvent(NaturalEvent event) {
        if (currentEvent == null) {
            if (event instanceof BiomeSpecificEvent || event instanceof BiomeExcludingEvent) {
                Stream<Biome> biomeStream = TrackerUtil.getIngame()
                                                       .map(Entity::getLocation)
                                                       .map(Location::getBlock)
                                                       .map(Block::getBiome);
                if (event instanceof BiomeSpecificEvent && biomeStream.noneMatch(((BiomeSpecificEvent) event).getBiomes()::contains)) {
                    return false;
                } else if (event instanceof BiomeExcludingEvent && biomeStream.allMatch(((BiomeExcludingEvent) event).getExcludedBiomes()::contains)) {
                    return false;
                }
            }

            if (event instanceof Storm && !((Storm)event).canSpawn()) {
                return false;
            }

            currentEvent = event;
            currentEvent.start();
            HG.inst().getSession().broadcast(currentEvent.getMessage().transformer(str -> CC.RED + str));
            TrackerUtil.getIngame().forEach(player -> player.playSound(player.getLocation(), Sound.WITHER_SPAWN, 1.5F, 1));
            return true;
        }
        return false;
    }

    public boolean cancelEvent() {
        if (currentEvent != null) {
            currentEvent.end();
            reset();
            HG.inst().getSession().broadcast(LocaleMessage.of(CC.GREEN + "End of Natural Event"));
            return true;
        }
        return false;
    }
}
