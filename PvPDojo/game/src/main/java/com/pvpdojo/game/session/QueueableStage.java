/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.session;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import com.pvpdojo.bukkit.events.DojoPlayerJoinEvent;
import com.pvpdojo.bukkit.events.ProxyQuitEvent;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.session.Reloggable;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.PlayerUtil;

public abstract class QueueableStage<T extends Reloggable, U extends User, S extends StagedSession> extends StageHandler<S> {

    private final Map<UUID, T> queuedPlayers = new ConcurrentHashMap<>();
    private boolean pulled;

    public QueueableStage(S session) {
        super(session);
    }

    public abstract T createQueued(U user);

    public abstract boolean canQueue(U user);

    public boolean queue(U user) {
        boolean canQueue = canQueue(user);
        if (canQueue) {
            queuedPlayers.put(user.getUUID(), createQueued(user));
        }
        return canQueue;
    }

    public Map<UUID, T> getQueuedPlayers() {
        return queuedPlayers;
    }

    public int getPlayerCountWithQueue() {
        return queuedPlayers.size() + session.getParticipants().size();
    }

    public void pullQueued() {
        if (!pulled) {
            queuedPlayers.keySet().forEach(PlayerUtil::sendHere);
        }
        pulled = true;
    }

    public void resetQueue() {
        pulled = false;
        queuedPlayers.clear();
    }

    public void broadcastQueued(String message) {
        queuedPlayers.keySet().forEach(uuid -> BungeeSync.sendMessage(uuid, message));
    }

    @EventHandler
    public void onQueueProxyQuit(ProxyQuitEvent e) {
        queuedPlayers.remove(e.getUniqueId());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onQueueJoin(DojoPlayerJoinEvent e) {
        Reloggable reloggable = queuedPlayers.remove(e.getPlayer().getUniqueId());
        if (reloggable != null) {
            reloggable.apply(User.getUser(e.getPlayer()));
        }
    }

}
