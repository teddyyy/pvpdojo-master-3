/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.userdata.stats;

import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.userdata.stats.PersistentHGStats;

public class HGStats extends PersistentHGStats {

    private HGUser user;
    private long gameStart = -1;

    public HGStats(HGUser user) {
        super(user.getUUID());
        this.user = user;
        if (HG.inst().getSession().getState() != SessionState.PREGAME) {
            gameStart = System.currentTimeMillis();
        }
    }

    public void startGame() {
        gameStart = System.currentTimeMillis();
        addGamePlayed();
    }

    @Override
    public int getKillstreak() {
        return user.getKills();
    }

    @Override
    public int incrKillstreak() {
        return getKillstreak();
    }

    @Override
    public long getCurrentPlayTime() {
        return gameStart != -1 ? System.currentTimeMillis() - gameStart : 0;
    }

}
