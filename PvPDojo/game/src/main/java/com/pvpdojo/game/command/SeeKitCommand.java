/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.command;

import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.game.userdata.kit.KitGameUser;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Flags;

@CommandAlias("seekit|ckit|showkit")
public class SeeKitCommand extends DojoCommand {

    @Default
    @CatchUnknown
    @CommandCompletion("@players")
    public void onSeekit(Player player, @Flags("other") Player target) {
        KitGameUser user = KitGameUser.getUser(target);
        if (user instanceof HGUser) {
            player.sendMessage(CC.GRAY + "Kills" + PvPDojo.POINTER + CC.GREEN + ((HGUser) user).getKills());
        }
        player.sendMessage(CC.GRAY + "Kits" + PvPDojo.POINTER + CC.GREEN + KitUtil.getKitList(user));
    }

    @Override
    public Rank getRank() {
        return Rank.TRIAL_MODERATOR;
    }
}
