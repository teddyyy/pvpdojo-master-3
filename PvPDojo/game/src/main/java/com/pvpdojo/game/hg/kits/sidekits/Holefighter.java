package com.pvpdojo.game.hg.kits.sidekits;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.kits.EntityInteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.util.bukkit.CC;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;

public class Holefighter extends Kit implements EntityInteractingKit {

    @Override
    public void activate(PlayerInteractEntityEvent e) {
        if (e.getRightClicked() instanceof Player) {
            int playerCount = (int) TrackerUtil.getNearbyIngame(e.getPlayer(), 10, 10, 10).count();

            if (playerCount >= 2) {
                Player clickedPlayer = (Player) e.getRightClicked();
                double x = clickedPlayer.getLocation().getX();
                double y = clickedPlayer.getLocation().getY();
                double z = clickedPlayer.getLocation().getZ();
                if (y >= 5 && y <= 80) {
                    clickedPlayer.getWorld().getBlockAt(new Location(clickedPlayer.getWorld(), x, y - 1, z)).setType(Material.AIR);
                    clickedPlayer.getWorld().getBlockAt(new Location(clickedPlayer.getWorld(), x, y - 2, z)).setType(Material.AIR);
                    clickedPlayer.getWorld().getBlockAt(new Location(clickedPlayer.getWorld(), x, y - 3, z)).setType(Material.AIR);

                    Material prevMaterial = clickedPlayer.getWorld().getBlockAt(new Location(clickedPlayer.getWorld(), x, y - 4, z)).getType();
                    clickedPlayer.getWorld().getBlockAt(new Location(clickedPlayer.getWorld(), x, y - 4, z)).setType(Material.BEDROCK);
                    PvPDojo.schedule(() -> {
                        clickedPlayer.getWorld().getBlockAt(new Location(clickedPlayer.getWorld(), x, y - 4, z)).setType(prevMaterial);
                    }).createTask(20 * 20);

                    clickedPlayer.teleport(new Location(clickedPlayer.getWorld(), x, y - 2, z));
                    kitCooldown.use();
                } else {
                    e.getPlayer().sendMessage(CC.RED + "You can only use this kit between y: 5 and y: 80");
                }
            } else {
                e.getPlayer().sendMessage(CC.RED + "You can only use this kit when two or more players are nearby");
            }
        }
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.WOOD_SPADE);
    }

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public int getCooldown() {
        return 60000;
    }
}
