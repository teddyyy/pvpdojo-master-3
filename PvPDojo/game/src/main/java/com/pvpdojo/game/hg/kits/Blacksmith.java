/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.ItemUtil;

public class Blacksmith extends Kit {

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {
        if (!e.isCancelled()) {
            Player victim = (Player) e.getEntity();
            for (ItemStack armor : victim.getInventory().getArmorContents()) {
                if (PvPDojo.RANDOM.nextBoolean()) {
                    armor.setDurability((short) (armor.getDurability() + 1));
                }
            }
        }
    }

    @Override
    public void onPlayerDamagedByPlayer(EntityDamageByEntityEvent e) {
        if (!e.isCancelled()) {
            Player attacker = (Player) e.getDamager();
            if (attacker.getItemInHand() != null) {
                if (ItemUtil.TOOLS.contains(attacker.getItemInHand().getType())) {
                    if (PvPDojo.RANDOM.nextBoolean()) {
                        attacker.getItemInHand().setDurability((short) (attacker.getItemInHand().getDurability() + 1));
                    }
                }
            }
        }
    }
}
