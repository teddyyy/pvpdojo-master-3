package com.pvpdojo.game.hg.command;

import co.aikar.commands.annotation.*;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.KitHolder;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

@CommandAlias("setkit")
public class SetKitCommand extends DojoCommand {

    @CatchUnknown
    @Default
    @CommandCompletion("@allplayers @kits")
    public void onSetKit(CommandSender sender, @Single String target, HGKit kit) {

        //Shouldn't work if the game didn't start yet.
        if (HG.inst().getSession().getState() == SessionState.PREGAME) {
            return;
        }

        boolean applyItems = HG.inst().getSession().getState() != SessionState.PREGAME;
        HGUser user = HGUser.getUser(Bukkit.getPlayer(target));

        if (applyItems) {//Remove old kit items
            if (kit.isMainKit()) {
                if (user.getKit() != null) {
                    KitUtil.removeKitItems(user, user.getKit());
                }
            }
            if (kit.isSideKit()) {
                if (user.getSideKit() != null) {
                    KitUtil.removeKitItems(user, user.getSideKit().getKit());
                }
            }
        }

        if (kit.isMainKit()) {
            user.setKit(kit);
            sender.sendMessage(CC.GREEN + "Successfully set " + user.getName() + "'s kit to " + StringUtils.getEnumName(kit));
        }
        if (kit.isSideKit()) {
            user.setSideKit(new KitHolder<HGKit>(kit, kit.getSupplier().get()));
            sender.sendMessage(CC.GREEN + "Successfully set " + user.getName() + "'s side kit to " + StringUtils.getEnumName(kit));
        }

        if (applyItems) {//Add new kit items
            if (kit.isMainKit()) {
                KitUtil.applyKitItems(user, user.getKit());
            }
            if (kit.isSideKit()) {
                KitUtil.applyKitItems(user, user.getSideKit().getKit());
            }
        }

    }

    @Override
    public Rank getRank() {
        return Rank.DEVELOPER;
    }
}
