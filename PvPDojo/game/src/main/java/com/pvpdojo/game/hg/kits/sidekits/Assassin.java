package com.pvpdojo.game.hg.kits.sidekits;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_7_R4.entity.disguise.CraftCreatureDisguise;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.disguise.Disguise;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.DojoRunnable;
import com.pvpdojo.util.bukkit.CC;

public class Assassin extends Kit {

    private int maxChargeSeconds = 20 * 3;
    private DojoRunnable runnable;

    @Override
    public void onToggleSneak(PlayerToggleSneakEvent e) {
        Player player = e.getPlayer();
        if (!player.isSneaking()) {
            if (kitCooldown.isOnCooldown()) {
                kitCooldown.sendCooldown(User.getUser(e.getPlayer()));
                return;
            }
            new BukkitRunnable() {

                int loop;
                int sound;

                int runnableLoop;

                boolean sneak;

                @Override
                public void run() {
                    if (!player.isSneaking() && loop > maxChargeSeconds) {
                        sneak = true;
                        cancel();
                        continueEffect();
                        return;
                    }

                    if (loop >= 20 * 20) {
                        sneak = false;
                        cancel();
                        return;
                    }

                    if (player.isSneaking()) {
                        if (loop % 20 == 0 && loop <= maxChargeSeconds) {
                            player.sendMessage(
                                    CC.GREEN + "Invisibility starts in " + ((loop / 20) - 3) * -1 + " seconds");
                        }
                        if (++loop > maxChargeSeconds) {
                            player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 20 * 20, 1));
                            disguise(player);
                        }
                    } else {
                        sneak = false;
                        cancel();
                        return;
                    }

                    if (++sound > 20 * 7) {
                        sound = 0;
                        player.getWorld().playSound(player.getLocation(), Sound.FIZZ, 1.0F, 1.0F);
                    }
                }

                @Override
                public void cancel() {
                    super.cancel();
                    if (!sneak) {
                        player.removePotionEffect(PotionEffectType.INVISIBILITY);
                        reset(player);
                    }
                }

                public void continueEffect() {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 20 * 20, 1), true);
                    runnable = PvPDojo.schedule(() -> {
                        if (++runnableLoop > 20 * 20 || !player.hasPotionEffect(PotionEffectType.INVISIBILITY)) {
                            reset(player);
                        }
                    });
                    runnable.createTimer(1, -1);
                }
            }.runTaskTimer(Game.inst(), 0, 1);
        }

    }

    @Override
    public int getCooldown() {
        return 60000;
    }

    @Override
    public void onLeave(Player player) {
        reset(player);
    }

    public void disguise(Player player) {
        if (player.getDisguise() == null) {
            Disguise disguise = new CraftCreatureDisguise(player, EntityType.ZOMBIE);
            player.setDisguise(disguise);
        }
        kitCooldown.use();

    }

    public void reset(Player player) {
        if (player.getDisguise() != null) {
            player.setDisguise(null);
            player.sendMessage(CC.RED + "Your invisibility has worn off!");
        }

        if (runnable != null) {
            runnable.cancel();
        }
    }

    public static void handleTracker(Player player, Player tracked) {
        ItemStack item = player.getItemInHand();

        new BukkitRunnable() {

            @Override
            public void run() {

                if (HGUser.getUser(player).isLoggedOut() || HGUser.getUser(tracked).isLoggedOut()) {
                    cancel();
                    reset();
                    return;
                }

                if (!HGUser.getUser(player).isIngame() || !HGUser.getUser(tracked).isIngame()) {
                    cancel();
                    reset();
                    return;
                }

                if (HG.inst().getFeastLocation() != null && player.getCompassTarget().equals(HG.inst().getFeastLocation())) {
                    cancel();
                    return;
                }

                Location targetLoc = player.getCompassTarget();
                Location trackerLoc = player.getLocation();


                if (targetLoc != null) {

                    //Reset the tracker because it was pointing to no where.
                    if (player.getCompassTarget().equals(new Location(Bukkit.getWorlds().get(0), 0, 0, 0))) {
                        cancel();
                        reset();
                        return;
                    }

                    player.setCompassTarget(targetLoc);

                    changeName(player, Material.COMPASS, CC.RED + "Tracker (" + ChatColor.GRAY + (int) trackerLoc.distance(targetLoc) + ChatColor.RED + ")", item.getItemMeta());
                }

            }

            @Override
            public void cancel() {
                changeName(player, Material.COMPASS, CC.RED + "Tracker", item.getItemMeta());
                super.cancel();
            }

            private void reset() {
                player.setCompassTarget(new Location(Bukkit.getWorlds().get(0), 0, 0, 0));
            }
        }.runTaskTimer(Game.inst(), 0, 5);
    }


    public static void changeName(Player tracker, Material type, String name, ItemMeta meta) {

        if (tracker == null) {
            return;
        }

        int slot = tracker.getInventory().first(type);
        if (meta != null) {
            meta.setDisplayName(name);
            if (slot != -1) {
                tracker.getInventory().getItem(slot).setItemMeta(meta);
            }
        }
    }

}
