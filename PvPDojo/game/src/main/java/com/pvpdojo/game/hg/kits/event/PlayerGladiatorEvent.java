/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits.event;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class PlayerGladiatorEvent extends PlayerEvent {

    public static final HandlerList handlerList = new HandlerList();
    private final Player second;

    public PlayerGladiatorEvent(Player who, Player second) {
        super(who);
        this.second = second;
    }

    public Player getSecond() {
        return second;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList() {
        return handlerList;
    }

}
