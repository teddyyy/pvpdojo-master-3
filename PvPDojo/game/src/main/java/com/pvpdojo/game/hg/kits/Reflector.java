package com.pvpdojo.game.hg.kits;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.TimeUtil;
import org.bukkit.Bukkit;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class Reflector extends Kit {

    private Set<UUID> splashDamage = new HashSet<>();
    private long lastHit;

    @Override
    public void onPlayerDamagedByPlayer(EntityDamageByEntityEvent e) {

        double damage = e.getDamage() / 1.1f;
        boolean reflect = PvPDojo.RANDOM.nextInt(6) == 1;

        if (e.isCancelled()) {
            return;
        }

        if (reflect) {
            e.setCancelled(true);
            NMSUtils.damageEntity(e.getDamager(), e.getEntity(), EntityDamageEvent.DamageCause.CONTACT, damage);
            return;
        }

        splashDamage.add(e.getDamager().getUniqueId());

        if (TimeUtil.hasPassed(lastHit, TimeUnit.SECONDS, 6)) {//Passed 3 seconds, Clear the list.
            splashDamage.clear();

        } else { // Didn't pass 3 seconds yet.
            if (splashDamage.size() > 1) {

                e.setCancelled(true);

                splashDamage.forEach(players -> NMSUtils.damageEntity(Bukkit.getPlayer(players), e.getEntity(), EntityDamageEvent.DamageCause.CONTACT, damage));
                splashDamage.clear();
            }
        }

        lastHit = System.currentTimeMillis();
    }
}
