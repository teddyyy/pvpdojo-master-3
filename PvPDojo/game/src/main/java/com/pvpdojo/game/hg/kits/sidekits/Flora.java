package com.pvpdojo.game.hg.kits.sidekits;

import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.CC;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class Flora extends Kit {

    @Override
    public void onPlayerDamagedByPlayer(EntityDamageByEntityEvent e) {
        Player damager = (Player) e.getDamager();
        if (damager.getInventory().contains(Material.YELLOW_FLOWER) || damager.getInventory().contains(Material.RED_ROSE)) {
            damager.sendMessage(CC.RED + "Your opponent seems to use your flowers to their advantage");
            e.setCancelled(true);
        }
    }

}
