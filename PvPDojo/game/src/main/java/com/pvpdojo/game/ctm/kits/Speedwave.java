/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.kits;

import com.pvpdojo.game.ctm.DamageConsumingKit;
import com.pvpdojo.game.ctm.lang.CTMKitMessageKeys;
import com.pvpdojo.game.ctm.util.CTMUtil;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.game.ctm.CTM;
import com.pvpdojo.game.ctm.lang.CTMMessageKeys;
import com.pvpdojo.game.ctm.session.CTMSession;
import com.pvpdojo.game.ctm.userdata.CTMUser;
import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.session.entity.TeamArenaEntity;
import com.pvpdojo.util.bukkit.CC;

public class Speedwave extends Kit implements InteractingKit, DamageConsumingKit {

    @Override
    public void activate(PlayerInteractEvent e) {}

    @Override
    public void click(Player player) {
        CTMUser user = CTMUser.getUser(player);
        CTMSession session = CTM.inst().getSession();

        if (consumeDamage(player)) {
            session.broadcast(CTMKitMessageKeys.SPEEDWAVE_SUMMON, "{player}", user.getTeam().getColor() + user.getName());
            user.getTeam().getPlayers().forEach(all -> all.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 15, 1)));
        }

    }

    @Override
    public boolean isSendCooldown() {
        return false;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.FEATHER);
    }


    @Override
    public double getDamageNeeded() {
        return CTMUtil.MAX_DAMAGE;
    }
}
