/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.game.kits.Kit;

public class Switcher extends Kit {

    @Override
    public void onProjectileHit(EntityDamageByEntityEvent e) {
        if (!e.isCancelled() && e.getDamager() instanceof Snowball) {
            Player player = (Player) ((Snowball) e.getDamager()).getShooter();

            Location playerLocation = player.getLocation();
            player.teleport(e.getEntity().getLocation());
            e.getEntity().teleport(playerLocation);
        }
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] { new ItemStack(Material.SNOW_BALL, 16) };
    }

}
