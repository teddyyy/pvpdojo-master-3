/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.session;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.lang.HGMessageKeys;
import com.pvpdojo.game.hg.team.HGTeam;
import com.pvpdojo.game.hg.team.TeamList;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.hg.userdata.stats.HGStats;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.game.session.StageHandler;
import com.pvpdojo.game.session.StagedSession;
import com.pvpdojo.menus.ChallengeMenu;
import com.pvpdojo.session.PastGameSession.AfterFightSnapshot;
import com.pvpdojo.session.RelogData;
import com.pvpdojo.session.RelogSession;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.arena.Arena;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.entity.RankedArenaEntity;
import com.pvpdojo.userdata.User;
import com.pvpdojo.userdata.stats.PersistentHGStats;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.CC;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;
import org.bukkit.map.MinecraftFont;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.pvpdojo.util.StreamUtils.negate;

public class HGSession extends StagedSession implements RelogSession<HGRelogData, HGUser> {

    private static final int fireworkRadius = 19;

    private static final String IMAGE_PATH = "../images/hg/";

    // TODO: move this list somewhere else
    // and maybe also disable Capitalist?
    private static final List<HGKit> NO_BONUS_KITS = Arrays.asList(HGKit.BACKUP, HGKit.NINJA, HGKit.KANGAROO, HGKit.GLADIATOR,
            HGKit.ANCHOR, HGKit.GRAPPLER, HGKit.MADMAN, HGKit.URGAL, HGKit.ENDERMAGE, HGKit.BLINK, HGKit.WEREWOLF,
            HGKit.SURPRISE, HGKit.COPYCAT);
    private Map<UUID, HGKit> bonusKits;  // prevent people from relogging to get a new bonus kit

    public HGSession(Arena arena, HGUser... participants) {
        super(arena, SessionType.HG, participants);
        recorderDelay = 29 * 20;
        bonusKits = new HashMap<>();
    }

    @Override
    public StageHandler getFirstStageHandler() {
        return new PreGame(this);
    }

    @Override
    public void endRanked(RankedArenaEntity winner, ArenaEntity... losers) {
        throw new IllegalStateException();
    }

    @Override
    public void addParticipant(ArenaEntity ae) {
        if (!(ae instanceof HGUser)) {
            throw new IllegalStateException();
        }
        super.addParticipant(ae);
    }

    @Override
    public void onLose(ArenaEntity loser) {
    }


    @Override
    public void onWin(ArenaEntity winner) {

        if (winner instanceof HGRelogData) {
            if (((HGRelogData) winner).isUseStats()) {
                ((HGStats) ((HGRelogData) winner).getStats()).addWin();
            }
            return;
        }

        HGUser user = (HGUser) winner;

        if (user.useStats()) {
            user.getStats().addWin();
            user.getFastDB().setWonHG(true);
            user.getFastDB().save(user.getUUID());
        }

        PvPDojo.schedule(() -> {
            // FireworkEffect firework = FireworkEffect.builder().with(FireworkEffect.Type.BURST).withColor(Color.RED).withColor(Color.PURPLE).withFlicker().withTrail().build();
            //BukkitUtil.launchFireworkEffect(winner.getPlayer().getLocation().add(0, 2, 0), firework);
            broadcast(HGMessageKeys.WIN, "{winner}", winner.getName());
        }).createTimer(3 * 21, 5 * 20, 5);

        PvPDojo.schedule(() -> {
            boolean usedBonusKit = KitUtil.getKitProvider(user) == user.getBonusKit();
            int coins = (PersistentHGStats.WIN_CREDIT + user.getKills() * PersistentHGStats.KILL_CREDIT * (usedBonusKit ? PersistentHGStats.KILL_BONUS_MODIFIER : 1) + PersistentHGStats.GAME_CREDIT);
            double skillpoints = user.getKills() * PersistentHGStats.SP_MODIFIER_KILL + PersistentHGStats.SP_MODIFIER_WIN + PersistentHGStats.SP_MODIFIER_GAMES_PLAYED;

            if (usedBonusKit) {
                coins *= 2;
                skillpoints += PersistentHGStats.SP_MODIFIER_BONUS_KIT;
                user.getStats().addBonusKitWin();
            }
            if (KitUtil.getKitProvider(user) == HGKit.CAPITALIST) {
                user.getStats().addMoney(PersistentHGStats.WIN_CREDIT * 2);
                coins *= 3;
            }

            winner.sendMessage(CC.GREEN + "You earned " + coins + " credits and " + new DecimalFormat("#.#").format(skillpoints) + " skillpoints");

            Player p = winner.getPlayer();

            // Give winners of big rounds a random collection chest
            if (getInitialPlayerCount() >= 30) {
                ChallengeMenu.rewardCollections(User.getUser(p), 1);
            }

            Location loc = new Location(p.getWorld(), p.getLocation().getX(), Math.max(p.getWorld().getHighestBlockYAt(p.getLocation()) + 20, 100), p.getLocation().getZ());

            for (int i = -1; i <= 2; i++) {
                for (int z = -1; z <= 2; z++) {
                    loc.clone().add(i, -1, z).getBlock().setType(Material.CAKE_BLOCK);
                    loc.clone().add(i, -2, z).getBlock().setType(Material.GLASS);
                }
            }

            p.teleport(loc);
            p.getWorld().setTime(259000);

            int kills = user.getKills();
            if (HGUser.getUser(p) != null) {
                HGUser.getUser(p).sendMessage(HGMessageKeys.WIN_INFO, "{kills}", kills + "",
                        "{playersize}", kills > 1 || kills == 0 ? "Players" : "Player");
            }

            p.setAllowFlight(true);
            p.setFlying(true);

            PvPDojo.schedule(() -> {
                for (int i = 0; i < 15; i++) {
                    double x = Math.cos(i) * fireworkRadius;
                    double y = PvPDojo.RANDOM.nextInt(3);
                    double z = Math.sin(i) * fireworkRadius;
                    loc.add(x, y, z);
                    launchFirework(loc);
                    loc.subtract(x, y, z);
                }

            }).createTimer(0, 20, 25);


            ItemStack itemmap = new ItemStack(Material.MAP);
            MapView map = Bukkit.createMap(p.getWorld());

            for (MapRenderer r : map.getRenderers()) {
                map.removeRenderer(r);
            }

            File[] files = Paths.get(IMAGE_PATH).toFile().listFiles();
            Image funnyPic = files != null ? new ImageIcon(IMAGE_PATH + files[PvPDojo.RANDOM.nextInt(files.length)].getName()).getImage() : null;

            map.addRenderer(new MapRenderer() {


                @Override
                public void render(MapView view, MapCanvas canvas, Player p) {
                    canvas.drawText(15, 1, MinecraftFont.Font, "CONGRATULATIONS");
                    canvas.drawText(30, 8, MinecraftFont.Font, " ");
                    canvas.drawText(15, 18, MinecraftFont.Font, "on winning an HG round!       ");
                    canvas.drawText(28, 28, MinecraftFont.Font, "     " + p.getName());
                    if (funnyPic != null) {
                        canvas.drawImage(35, 50, funnyPic);
                    } else {
                        canvas.drawText(35, 50, MinecraftFont.Font, "Image error :(");
                    }
                }
            });
            p.getInventory().setItem(0, itemmap);
        }).createTask(3 * 20);
    }

    @Override
    public void handleMatchDetails(TextComponent tc) {
    }

    @Override
    public void prepareEntity(ArenaEntity entity) {
        int x = PvPDojo.RANDOM.nextInt(40) - 20, z = PvPDojo.RANDOM.nextInt(40) - 20;
        Location randomLocation = BlockUtil.getHighestBlock(getArena().getWorld(), x, z).getLocation().add(0.5, 1, 0.5);

        if (randomLocation.getY() <= 58 && getState() == SessionState.PREGAME) {
            prepareEntity(entity);
        } else {
            entity.teleport(randomLocation);
            if (getCurrentHandler().getTimeSeconds() <= 15) {
                entity.setFrozen(true);
            }
        }
    }

    @Override
    public void postDeath(ArenaEntity entity) {
        switch (getState()) {
            case PREGAME:
                getParticipants().remove(entity);
                break;
            case INVINCIBILITY:
            case STARTED:
                if (!(entity instanceof HGUser) || !((HGUser) entity).isRespawning()) {
                    super.postDeath(entity);
                    triggerEnd(getCurrentlyDead().toArray(new ArenaEntity[0]));
                    entity.setSession(null);
                }
                break;
        }

        if (HG.inst().getSettings().getTeamSize() > 1) {
            HGTeam team = TeamList.getTeamForUser(entity.getUniqueId());
            if (team != null && !((HGUser) entity).isRespawning()) {
                TeamList.removeTeamUser(team, entity.getUniqueId());
            }
        }

        Bukkit.getOnlinePlayers().stream().map(HGUser::getUser).forEach(HGUser::updateScoreBoard);

        if (HG.inst().getSession().getCurrentHandler().getTimeSeconds() >= HG.inst().getSettings().getSecondsTillPit() + 5 * 60
                && HG.inst().getSettings().isPitElimination()) {
            ((Pit) getCurrentHandler()).resetElimination();
        }
    }

    @Override
    public boolean checkEndCondition() {
        return getParticipants().stream().filter(negate(getCurrentlyDead()::contains)).count() <= 1;
    }

    @Override
    public boolean isPersistent() {
        return true;
    }

    @Override
    public HGRelogData createRelogData(HGUser user) {
        return new HGRelogData(user);
    }

    @Override
    public int getRelogTimeout() {
        return 90;
    }

    @Override
    public boolean hasNPC() {
        return false;
    }

    @Override
    public void onLogout(HGUser user) {
    }

    @Override
    public void onTimeout(HGUser user) {
        broadcast(HGMessageKeys.TIMEOUT, "{user}", user.getName());
    }

    @Override
    public boolean isAllowCombatLog() {
        return false;
    }

    @Override
    public boolean hasReplay() {
        return false;
    }

    @Override
    public AfterFightSnapshot getSnapshot(Player player) {
        AfterFightSnapshot snapshot = super.getSnapshot(player);
        HGUser user = HGUser.getUser(player);
        snapshot.setKits(KitUtil.getKitList(user));
        snapshot.setRanking(getPlayersLeft());
        snapshot.setKills(user.getKills());
        return snapshot;
    }

    @Override
    public AfterFightSnapshot getSnapshot(RelogData relogData) {
        AfterFightSnapshot snapshot = super.getSnapshot(relogData);
        snapshot.setKits(KitUtil.getKitList(((HGRelogData) relogData).getKitHolders()));
        snapshot.setKills(((HGRelogData) relogData).getKills());
        snapshot.setRanking(getPlayersLeft());
        return snapshot;
    }

    public HGKit getBonusKit(UUID uuid) {
        if (bonusKits.containsKey(uuid)) {
            return bonusKits.get(uuid);
        }
        // maybe prioritize unowned kits or kits not played that often?
        List<HGKit> availableKits = Arrays.stream(HGKit.values()).filter(HGKit::isEnabled).filter(HGKit::isMainKit).collect(Collectors.toList());
        availableKits.removeAll(NO_BONUS_KITS);

        HGKit bonusKit = availableKits.isEmpty() ? null : availableKits.get(PvPDojo.RANDOM.nextInt(availableKits.size()));
        bonusKits.put(uuid, bonusKit);
        return bonusKit;
    }

    private static final ArrayList<Color> colors = new ArrayList<>(Arrays.asList(Color.WHITE, Color.PURPLE,
            Color.RED, Color.GREEN, Color.AQUA, Color.BLUE, Color.FUCHSIA, Color.GRAY, Color.LIME, Color.MAROON,
            Color.YELLOW, Color.SILVER, Color.TEAL, Color.ORANGE, Color.OLIVE, Color.NAVY, Color.BLACK));

    private static final ArrayList<FireworkEffect.Type> types = new ArrayList<>(
            Arrays.asList(FireworkEffect.Type.BALL, FireworkEffect.Type.BURST, FireworkEffect.Type.BALL_LARGE,
                    FireworkEffect.Type.STAR));

    private FireworkEffect.Type getFireworkType() {
        int size = types.size();
        return types.get(PvPDojo.RANDOM.nextInt(size));
    }

    private Color getColor() {
        int size = colors.size();
        return colors.get(PvPDojo.RANDOM.nextInt(size));
    }

    private void launchFirework(Location loc) {
        Firework fw = loc.getWorld().spawn(loc, Firework.class);
        FireworkMeta fm = fw.getFireworkMeta();
        fm.setPower(1);
        fm.addEffects(FireworkEffect.builder().with(getFireworkType()).withColor(getColor())
                .withFade(getColor()).build());
        fw.setFireworkMeta(fm);
    }
}
