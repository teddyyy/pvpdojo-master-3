/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder.LeatherArmorBuilder;

public class Phantom extends Kit implements InteractingKit {

    private boolean flying;
    private ItemStack[] prevArmor;

    @Override
    public void onDeath(PlayerDeathEvent e) {
        if (flying) {
            e.getDrops().removeIf(stack -> Bukkit.getItemFactory().isApplicable(Bukkit.getItemFactory().getItemMeta(Material.LEATHER_BOOTS), stack.getType()));
            for (ItemStack stack : prevArmor) {
                e.getDrops().add(stack);
            }
        }
    }

    @Override
    public void onLeave(Player player) {
        stopFly(player);
    }

    public void stopFly(Player player) {
        if (flying) {
            player.getInventory().setArmorContents(prevArmor);
            player.setAllowFlight(false);
            player.getWorld().playSound(player.getLocation(), Sound.WITHER_SPAWN, 0.1F, 5);
            flying = false;
            Bukkit.getPluginManager().callEvent(new PlayerToggleFlightEvent(player, false));

        }
    }

    @Override
    public void activate(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        kitCooldown.use();

        for (Entity nearby : player.getNearbyEntities(100, 200, 100)) {
            if (nearby instanceof Player) {
                ((Player) nearby).sendMessage(CC.BOLD + "A Phantom approaches...\nNote: they are not fly hacking, it's part of the kit.");
            }
        }

        prevArmor = player.getInventory().getArmorContents();

        ItemStack boots = new LeatherArmorBuilder(Material.LEATHER_BOOTS, Color.WHITE).build();
        ItemStack leggins = new LeatherArmorBuilder(Material.LEATHER_LEGGINGS, Color.WHITE).build();
        ItemStack chest = new LeatherArmorBuilder(Material.LEATHER_CHESTPLATE, Color.WHITE).build();
        ItemStack helmet = new LeatherArmorBuilder(Material.LEATHER_HELMET, Color.WHITE).build();

        ItemStack[] phantomcontent = new ItemStack[] { boots, leggins, chest, helmet };

        player.getWorld().playSound(player.getLocation(), Sound.WITHER_DEATH, 10F, 10F);

        player.getInventory().setArmorContents(phantomcontent);
        player.setAllowFlight(true);
        player.teleport(player.getLocation().add(0, 0.1, 0));
        player.setFlying(true);
        player.updateInventory();

        flying = true;

        new BukkitRunnable() {
            int i = player.isProtocolHack() ? 4 : 6;

            public void run() {

                if (!player.isOnline()) {
                    cancel();
                    return;
                }

                if (i <= 4 && i > 0) {
                    player.sendMessage(CC.RED.toString() + i + " seconds of flight remaining");
                }

                if (i == 0) {
                    stopFly(player);
                    cancel();
                }
                i--;

            }
        }.runTaskTimer(PvPDojo.get(), 0, 20);

    }

    @Override
    public void onInvClick(InventoryClickEvent e) {
        if (flying && e.getInventory() != null && e.getSlot() > 35) {
            e.setCancelled(true);
        }
    }

    @Override
    public void click(Player player) {}

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.FEATHER);
    }

    @Override
    public int getCooldown() {
        return 60000;
    }
}
