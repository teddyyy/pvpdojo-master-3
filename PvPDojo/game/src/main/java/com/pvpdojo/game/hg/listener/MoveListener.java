/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.listener;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.game.Game;
import com.pvpdojo.userdata.User;

public class MoveListener implements DojoListener {

    private Set<UUID> sponge = new HashSet<>();

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        Block block = e.getTo().getBlock().getRelative(BlockFace.DOWN);
        if (block.getType() == Material.SPONGE && e.isOnGround() && sponge.add(e.getPlayer().getUniqueId())) {

            User user = User.getUser(e.getPlayer());

            boolean cancelFall = e.getTo().getBlock().getType() == Material.AIR;

            double up = countDown(block.getLocation());
            double x = countTriangles(block.getLocation(), BlockFace.WEST) - countTriangles(block.getLocation(), BlockFace.EAST);
            double z = countTriangles(block.getLocation(), BlockFace.NORTH) - countTriangles(block.getLocation(), BlockFace.SOUTH);


            double max = Math.max(Math.max(Math.abs(x), Math.abs(z)), up);

            int moveMultiplier = max > 16 ? 4 : 2;
            int timeMultiplier = max > 16 ? 4 : 8;

            double upm = up / max * moveMultiplier;
            double xm = x / max * moveMultiplier;
            double zm = z / max * moveMultiplier;

            new BukkitRunnable() {
                int i = 0;

                @Override
                public void run() {
                    if (++i > max * timeMultiplier) {
                        cancel();
                        sponge.remove(e.getPlayer().getUniqueId());
                    } else {
                        if (cancelFall) {
                            user.cancelNextFall();
                        }
                        e.getPlayer().setVelocity(new Vector(xm, upm, zm));
                    }
                }
            }.runTaskTimer(Game.inst(), 0, 1);
        }
    }

    private int countDown(Location location) {
        Block currentBlock = location.getBlock();
        int count = 1;
        while ((currentBlock = currentBlock.getRelative(BlockFace.DOWN)).getType() == Material.SPONGE) {
            count++;
        }
        return count;
    }

    private int countTriangles(Location location, BlockFace face) {
        Block currentBlock = location.getBlock();
        int count = 0;
        boolean down = false;

        while (down ? (currentBlock = currentBlock.getRelative(BlockFace.DOWN)).getType() == Material.SPONGE : (currentBlock = currentBlock.getRelative(face)).getType() == Material.SPONGE) {
            if (down) {
                count++;
            }
            down = !down;
        }

        return count;
    }

}
