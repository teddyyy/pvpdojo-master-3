/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerStartItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.game.kits.Kit;

public class Milkman extends Kit {

    @Override
    public void onStartConsume(PlayerStartItemConsumeEvent e) {
        if (!e.isCancelled() && e.getItem().getType() == Material.MILK_BUCKET) {
            PvPDojo.schedule(() -> {
                e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 35 * 20, 0));
                e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 35 * 20, 0));
                e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 35 * 20, 0));
                e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 35 * 20, 0));
                e.getPlayer().setItemInHand(new ItemStack(Material.BUCKET));
                PvPDojo.schedule(() -> NMSUtils.get(e.getPlayer()).playerConnection.chat("I am the Milkman, my milk is delicious.", true)).createAsyncTask();
            }).nextTick();
        }
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] { new ItemStack(Material.MILK_BUCKET) };
    }

}
