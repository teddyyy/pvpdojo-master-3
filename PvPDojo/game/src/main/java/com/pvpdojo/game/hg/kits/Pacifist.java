/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import com.pvpdojo.game.kits.Kit;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class Pacifist extends Kit {

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {
        e.setDamage(Math.max(0D, e.getDamage() - 1.5));
    }

    @Override
    public void onPlayerDamagedByPlayer(EntityDamageByEntityEvent e) {
        e.setDamage(Math.max(0D, e.getDamage() - 1.5));
    }

}
