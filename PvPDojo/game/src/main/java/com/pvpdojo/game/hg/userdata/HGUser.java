/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.userdata;

import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.kits.persistent.PersistentKits;
import com.pvpdojo.game.hg.team.HGTeam;
import com.pvpdojo.game.hg.userdata.stats.HGStats;
import com.pvpdojo.game.kits.KitHolder;
import com.pvpdojo.game.kits.KitProvider;
import com.pvpdojo.game.userdata.kit.KitGameUser;
import com.pvpdojo.game.userdata.kit.SingleKitGameUser;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.UUID;

public interface HGUser extends SingleKitGameUser<HGKit> {

    static HGUser getUser(Player player) {
        return HG.inst().getUserList().getUser(player);
    }

    static HGUser getUser(UUID uuid) {
        return HG.inst().getUserList().getUser(uuid);
    }

    int getKills();

    void setKills(int kills);

    void addKill();

    void setInvincibleSeconds(int invincibleSeconds);

    int getInvincibleSeconds();

    boolean isInvincible();

    void setRespawned(boolean respawn);

    boolean isRespawned();

    boolean canRespawn();

    boolean isRespawning();
    
    boolean addKitItemAfterRespawn();

    void setRespawning(boolean respawning);

    void setAddKitItemAfterRespawn(boolean add);

    // registers the damager´s name and the timestamp
    void registerDamage(UUID damager);

    boolean receivesReducedDamage();  //also for teaming nerfs

    void updateScoreBoard();

    void disableKits(int seconds);

    Location respawn();

    @Override
    HGStats getStats();

    PersistentKits getPersistentKits();

    HGKit getBonusKit();

    HGTeam getHGTeam();

    void setHGTeam(HGTeam team);

    KitHolder<HGKit> getSideKit();

    void setSideKit(KitHolder<HGKit> kitProvider);

}
