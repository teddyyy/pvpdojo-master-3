/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.kits;

import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.bukkit.events.DojoPlayerSoupEvent;
import com.pvpdojo.game.userdata.kit.KitGameUser;

public abstract class Kit {

    protected KitCooldown kitCooldown = new KitCooldown(this);

    public void onLeave(Player player) {}

    public void onDamage(EntityDamageEvent e) {}

    public void onPlayerAttack(EntityDamageByEntityEvent e) {}

    public void onEntityAttack(EntityDamageByEntityEvent e) {}

    public void onPlayerDamagedByPlayer(EntityDamageByEntityEvent e) {}

    public void onPlayerDamagedByEntity(EntityDamageByEntityEvent e) {}

    public void onMove(PlayerMoveEvent e) {}

    public void onFish(PlayerFishEvent e) {}

    public void onToggleSneak(PlayerToggleSneakEvent e) {}

    public void onInteract(PlayerInteractEvent e) {}

    public void onInteractEntity(PlayerInteractEntityEvent e) {}

    public void onProjectileHit(EntityDamageByEntityEvent e) {}

    public void onKill(PlayerDeathEvent e) {}

    public void onKillEntity(EntityDeathEvent e) {}

    public void onProjectileLaunch(ProjectileLaunchEvent e) {}

    public void onInvClick(InventoryClickEvent e) {}

    public void onInvOpen(InventoryOpenEvent e) {}

    public void onInvClose(InventoryCloseEvent e) {}

    public void onItemSwitch(PlayerItemHeldEvent e) {}

    public void onDrop(PlayerDropItemEvent e) {}

    public void onDeath(PlayerDeathEvent e) {}

    public void onPreDeath(PlayerPreDeathEvent e) {}

    public void onPickUp(PlayerPickupItemEvent e) {}

    public void onPrepareCraft(PrepareItemCraftEvent e) {}

    public void onBowShoot(EntityShootBowEvent e) {}

    public void onLeash(PlayerLeashEntityEvent e) {}

    public void onPlace(BlockPlaceEvent e) {}

    public void onBreak(BlockBreakEvent e) {}

    public void onBlockDamage(BlockDamageEvent e) {}

    public void onConsume(PlayerItemConsumeEvent e) {}

    public void onStartConsume(PlayerStartItemConsumeEvent e) {}

    public void onExp(PlayerExpChangeEvent e) {}

    public void onSoup(DojoPlayerSoupEvent e) {}

    public void onMobTarget(EntityTargetEvent e) {}

    public void onMobEntityTarget(EntityTargetLivingEntityEvent e) {}

    public void onEnchantItem(EnchantItemEvent e) {}

    public void onTick(KitGameUser<? extends KitProvider> user) {}

    public KitCooldown getKitCooldown() {
        return kitCooldown;
    }

    public ItemStack[] getStartItems() {
        return new ItemStack[0];
    }

    public ItemStack[] getStartArmor() {
        return null;
    }

    public int getCooldown() {
        return 0;
    }

}
