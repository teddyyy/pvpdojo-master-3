package com.pvpdojo.game.hg.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;

import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.game.hg.userdata.HGUser;

public class QuitListener implements DojoListener {

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        HGUser user = HGUser.getUser(player);
        if (user.isSpectator() || user.isAdminMode()) {
            e.setQuitMessage(null);
        }
    }

}
