/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.lms.userdata;

import java.util.UUID;

import org.bukkit.entity.Player;

import com.pvpdojo.game.lms.LMS;
import com.pvpdojo.game.userdata.GameUserImpl;
import com.pvpdojo.userdata.KitData;

public class LMSUser extends GameUserImpl {

    public static LMSUser getUser(Player player) {
        return LMS.inst().getUserList().getUser(player);
    }

    private KitData kitCache;

    public LMSUser(UUID uuid) {
        super(uuid);
    }

    public KitData getKitCache() {
        return kitCache;
    }

    public void setKitCache(KitData kitCache) {
        this.kitCache = kitCache;
    }

    @Override
    public void run() {}
}
