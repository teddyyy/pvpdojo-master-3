/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.lms.lang;

import co.aikar.locales.MessageKey;
import co.aikar.locales.MessageKeyProvider;

public enum LMSMessageKeys implements MessageKeyProvider {

    KNOCKED_OUT, KNOCKED_OUT_UNKNOWN, PLAYERS_LEFT;

    private final MessageKey key = MessageKey.of("pvpdojo-lms." + name().toLowerCase());

    @Override
    public MessageKey getMessageKey() {
        return key;
    }
}
