/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.listener;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.game.hg.naturalevents.MeteorShower;
import com.pvpdojo.game.hg.util.EquipmentUtil;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.util.StreamUtils;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.MushroomCow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Comparator;

public class EntityListener implements DojoListener {

    @EventHandler
    public void onMobTarget(EntityTargetLivingEntityEvent e) {
        if (e.getTarget() instanceof Player && ((Player) e.getTarget()).isOnline()) {
            StreamUtils.appendToStream(TrackerUtil.getNearbyIngame((Player) e.getTarget(), 20, 20, 20), (Player) e.getTarget())
                    .max(Comparator.comparingDouble(EquipmentUtil::getEquipmentPoints))
                    .ifPresent(e::setTarget);
        }
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player && e.getCause() == DamageCause.PROJECTILE
                || e.getCause() == DamageCause.ENTITY_EXPLOSION || e.getCause() == DamageCause.BLOCK_EXPLOSION) {
            if (e.getDamager().hasMetadata(MeteorShower.METEOR_SHOWER)) {
                double damage = e.getDamage();
                if (damage >= 14) {
                    e.setDamage(14.0);
                }
            }
        }
    }

    @EventHandler
    public void onCreatureSpawn(CreatureSpawnEvent e) {
        if (e.getEntity() instanceof MushroomCow && e.getSpawnReason() != SpawnReason.CUSTOM) {
            PvPDojo.schedule(() -> e.getEntity().remove()).createTask(20);
        }
    }

    @EventHandler
    public void onChunkLoad(ChunkLoadEvent e) {
        for (Entity entities : e.getChunk().getWorld().getEntities()) {
            if (entities instanceof MushroomCow) {
                entities.remove();
            }
        }
    }

    @EventHandler
    public void onChunkUnload(ChunkUnloadEvent e) {
        for (Entity entities : e.getChunk().getWorld().getEntities()) {
            if (entities instanceof MushroomCow) {
                entities.remove();
            }
        }
    }

    @EventHandler
    public void onRightClick(PlayerInteractEntityEvent e) {
        if (e.getRightClicked() instanceof MushroomCow && e.getPlayer().getItemInHand().getType() == Material.BOWL) {
            e.setCancelled(true);
            e.getPlayer().updateInventory();
        }
    }


}
