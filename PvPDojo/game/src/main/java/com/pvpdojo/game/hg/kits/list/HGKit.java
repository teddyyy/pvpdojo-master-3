/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits.list;

import co.aikar.locales.MessageKey;
import co.aikar.locales.MessageKeyProvider;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.kits.*;
import com.pvpdojo.game.hg.kits.sidekits.*;
import com.pvpdojo.game.hg.settings.HGSettings;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.kits.KitProvider;
import com.pvpdojo.game.kits.defaults.Archer;
import com.pvpdojo.game.kits.defaults.Berserker;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.LeatherArmorBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.PotionBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.SkullBuilder;
import lombok.Getter;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionType;

import java.util.function.Supplier;

public enum HGKit implements KitProvider {

    ACHILLES(Achilles::new, Material.WOOD_SWORD, HGKitLevel.SHIT),
    ANCHOR(Anchor::new, Material.ANVIL, HGKitLevel.GOOD),
    ARCHER(Archer::new, Material.BOW, HGKitLevel.SHIT),
    BACKUP(Backup::new, Material.CHEST, HGKitLevel.BAD),
    BARBARIAN(Barbarian::new, Material.DIAMOND_SWORD),
    BERSERKER(Berserker::new, Material.BLAZE_POWDER, HGKitLevel.BAD),
    BEASTMASTER(Beastmaster::new, Material.MONSTER_EGG),
    BLINK(Blink::new, Material.NETHER_STAR),
    BOXER(Boxer::new, Material.STONE_SWORD, HGKitLevel.BAD),
    CAMEL(Camel::new, Material.SAND, HGKitLevel.BAD),
    CANNIBAL(Cannibal::new, Material.RAW_FISH, HGKitLevel.BAD),
    COOKIEMONSTER(CookieMonster::new, Material.COOKIE, HGKitLevel.SHIT),
    COPYCAT(Copycat::new, Material.EMERALD, HGKitLevel.SHIT),
    DEMOMAN(Demoman::new, Material.GRAVEL, HGKitLevel.SHIT),
    DIGGER(Digger::new, Material.DRAGON_EGG, HGKitLevel.SHIT),
    DWARF(Dwarf::new, Material.DIAMOND_AXE, HGKitLevel.SHIT),
    ENDERMAGE(Endermage::new, Material.ENDER_PORTAL_FRAME, HGKitLevel.SHIT),
    FIREMAN(Fireman::new, Material.WATER_BUCKET, HGKitLevel.BAD),
    FISHERMAN(Fisherman::new, Material.FISHING_ROD, HGKitLevel.BAD),
    FLASH(Flash::new, Material.REDSTONE_TORCH_ON, HGKitLevel.SHIT),
    FORGER(Forger::new, Material.COAL, HGKitLevel.SHIT),
    FROZEN(Frozen::new, Material.PACKED_ICE, HGKitLevel.SHIT),
    GAMBLER(Gambler::new, Material.WOOD_BUTTON, HGKitLevel.SHIT),
    GLADIATOR(Gladiator::new, Material.IRON_FENCE, HGKitLevel.GOOD),
    GLIDER(Glider::new, new ItemBuilder(Material.FEATHER).glow(true).build(), HGKitLevel.SHIT),
    GRAPPLER(Grappler::new, Material.LEASH),
    GRAVEDIGGER(Gravedigger::new, Material.STONE_SPADE, HGKitLevel.SHIT),
    HORCRUX(Horcrux::new, new SkullBuilder("Voldemort").build(), HGKitLevel.BAD),
    HULK(Hulk::new, Material.PISTON_BASE, HGKitLevel.SHIT),
    JACKHAMMER(Jackhammer::new, Material.STONE_AXE, HGKitLevel.SHIT),
    JELLYFISH(Jellyfish::new, Material.WATER, HGKitLevel.SHIT),
    JOKER(Joker::new, Material.RECORD_3, HGKitLevel.GOOD),
    JUMPER(Jumper::new, Material.ENDER_PEARL, HGKitLevel.SHIT),
    KANGAROO(Kangaroo::new, Material.FIREWORK, HGKitLevel.GOOD),
    KAYA(Kaya::new, Material.GRASS, HGKitLevel.SHIT),
    LAUNCHER(Launcher::new, Material.SPONGE, HGKitLevel.SHIT),
    LUMBERJACK(Lumberjack::new, new ItemBuilder(Material.WOOD_AXE).enchant(Enchantment.DURABILITY, 1).build(), HGKitLevel.SHIT),
    MADMAN(Madman::new, Material.DEAD_BUSH),
    MAGMA(Magma::new, Material.FIRE),
    //MAGNETO(Magneto::new, Material.IRON_INGOT),
    MILKMAN(Milkman::new, Material.MILK_BUCKET, HGKitLevel.SHIT),
    MINER(Miner::new, Material.STONE_PICKAXE, HGKitLevel.SHIT),
    MIRROR(Mirror::new, Material.THIN_GLASS, HGKitLevel.SHIT),
    MONK(Monk::new, Material.BLAZE_ROD, HGKitLevel.SHIT),
    TARZAN(Tarzan::new, Material.VINE, HGKitLevel.SHIT),
    NEO(Neo::new, Material.ARROW, HGKitLevel.SHIT),
    NIGHTLOCK(Nightlock::new, Material.MUSHROOM_SOUP, HGKitLevel.SHIT),
    NINJA(Ninja::new, Material.INK_SACK, HGKitLevel.GOOD),
    PACIFIST(Pacifist::new, Material.YELLOW_FLOWER, HGKitLevel.UNAVAILABLE),
    PARASITE(Parasite::new, Material.FERMENTED_SPIDER_EYE, HGKitLevel.SHIT),
    PHANTOM(Phantom::new, Material.FEATHER),
    PORTAL(Portal::new, Material.PORTAL),
    POSEIDON(Poseidon::new, Material.WATER_LILY, HGKitLevel.BAD),
    REAPER(Reaper::new, Material.WOOD_HOE, HGKitLevel.SHIT),
    REDSTONER(Redstoner::new, Material.REDSTONE),
    RIDER(Rider::new, Material.SADDLE, HGKitLevel.SHIT),
    ROGUE(Rogue::new, new ItemBuilder(Material.STICK).glow(true).build(), HGKitLevel.SHIT),
    SCOUT(Scout::new, new PotionBuilder(PotionType.SPEED).splash().level(2).build(), HGKitLevel.SHIT),
    SNAIL(Snail::new, Material.SOUL_SAND, HGKitLevel.SHIT),
    SNORLAX(Snorlax::new, Material.BOWL, HGKitLevel.SHIT),
    SOULSTEALER(SoulStealer::new, Material.BONE),
    SPECIALIST(Specialist::new, Material.EXP_BOTTLE),
    SPIDERMAN(Spiderman::new, Material.WEB, HGKitLevel.SHIT),
    STOMPER(Stomper::new, Material.DIAMOND_BOOTS, HGKitLevel.BAD),
    SUPERMAN(Superman::new , Material.EYE_OF_ENDER, HGKitLevel.SHIT),
    SURPRISE(Surprise::new, new ItemStack(Material.RAW_FISH, 1, (short) 3), HGKitLevel.UNAVAILABLE),
    SWITCHER(Switcher::new, Material.SNOW_BALL, HGKitLevel.SHIT),
    TANK(Tank::new, Material.TNT, HGKitLevel.SHIT),
    THERMO(Thermo::new, Material.DAYLIGHT_DETECTOR, HGKitLevel.SHIT),
    THOR(Thor::new, Material.WOOD_AXE),
    TURTLE(Turtle::new, Material.BEDROCK, HGKitLevel.SHIT),
    URGAL(Urgal::new, new PotionBuilder(PotionType.STRENGTH).build(), HGKitLevel.GOOD),
    VAMPIRE(Vampire::new, Material.RED_ROSE),
    VIKING(Viking::new, Material.IRON_AXE, HGKitLevel.BAD),
    VIPER(Viper::new, Material.SPIDER_EYE, HGKitLevel.SHIT),
    WEREWOLF(Werewolf::new, Material.ROTTEN_FLESH),
    ASSASSIN(Assassin::new, Material.COMPASS, HGKitLevel.SHIT, true),
    BLACKSMITH(Blacksmith::new, Material.IRON_BARDING, HGKitLevel.BAD, true),
    BURROWER(Burrower::new, Material.GOLD_SPADE, true),
    CULTIVATOR(Cultivator::new, Material.SAPLING, HGKitLevel.SHIT, true),
    CAPITALIST(Capitalist::new, Material.GOLD_INGOT, HGKitLevel.UNAVAILABLE, true),
    CHAMELEON(Chameleon::new, Material.WHEAT, HGKitLevel.SHIT, true),
    CUPID(Cupid::new, new ItemBuilder(Material.ARROW).glow(true).build(), HGKitLevel.SHIT, true),
    DOMINO(Domino::new, new ItemBuilder(Material.STEP).durability((short)7).build(), true),
    GRANDPA(Grandpa::new, Material.STICK, HGKitLevel.SHIT, true),
    FLORA(Flora::new, Material.RED_ROSE, true),
    HOBBIT(Hobbit::new, Material.MOSSY_COBBLESTONE, true),
    HOLEFIGHTER(Holefighter::new, Material.WOOD_SPADE, true),
    IMPOSTER(Imposter::new, Material.FLINT, true),
    LOKI(Loki::new, new LeatherArmorBuilder(Material.LEATHER_HELMET, Color.GREEN).build(), HGKitLevel.SHIT, true),
    MONSTER(Monster::new, new ItemStack(Material.MONSTER_EGG, 1, (byte) 54), true),
    NOOB(Noob::new, Material.STONE_SWORD, true),
    //OASIS(Oasis::new, Material.STATIONARY_WATER, true), chris hates this
    REFLECTOR(Reflector::new, new ItemBuilder(Material.STAINED_GLASS_PANE).durability(DyeColor.WHITE.getWoolData()).build(), true),
    SALVAGER(Salvager::new, Material.ANVIL, true),
    SONIC(Sonic::new, Material.LEATHER_BOOTS, true),
    TITAN(Titan::new, Material.BEDROCK, true),
    WOOLYMORPH(Woolymorph::new, Material.WOOL, true),
    WORM(Worm::new, Material.DIRT, HGKitLevel.SHIT, true);

    private Supplier<Kit> supplier;
    private HGKitLevel level;
    private boolean sideKit;
    private ItemStack icon;
    private MessageKeyProvider descriptionKey = () -> MessageKey.of("pvpdojo-hg-kits.description_" + name().toLowerCase());

    HGKit(Supplier<Kit> supplier, Material material) {
        this(supplier, new ItemStack(material));
    }

    HGKit(Supplier<Kit> supplier, Material material, boolean sideKit) {
        this(supplier, new ItemStack(material), HGKitLevel.STANDARD, true);
    }

    HGKit(Supplier<Kit> supplier, Material material, HGKitLevel level) {
        this(supplier, new ItemStack(material), level, false);
    }

    HGKit(Supplier<Kit> supplier, Material material, HGKitLevel level, boolean sideKit) {
        this(supplier, new ItemStack(material), level, sideKit);
    }

    HGKit(Supplier<Kit> supplier, ItemStack item) {
        this(supplier, item, HGKitLevel.STANDARD, false);
    }

    HGKit(Supplier<Kit> supplier, ItemStack item, boolean sideKit) {
        this(supplier, item, HGKitLevel.STANDARD, sideKit);
    }

    HGKit(Supplier<Kit> supplier, ItemStack item, HGKitLevel level) {
        this(supplier, item, level, false);
    }

    HGKit(Supplier<Kit> supplier, ItemStack item, HGKitLevel level, boolean sideKit) {
        this.supplier = supplier;
        this.level = level;
        this.sideKit = sideKit;
        this.icon = new ItemBuilder(item).name(CC.GREEN + StringUtils.getEnumName(this)).build();
    }

    @Override
    public ItemStack getIcon() {
        return icon;
    }

    public HGKitLevel getLevel() {
        return level;
    }

    public boolean isSideKit() {
        return sideKit;
    }

    public boolean isMainKit() { return !sideKit; }

    public boolean isEnabled() {
        HGSettings settings = HG.inst().getSettings();
        return !settings.getDisabledKits().contains(name().toLowerCase()) && (settings.getKitWhiteList().isEmpty() || settings.getKitWhiteList().contains(name().toLowerCase()));
    }

    public MessageKeyProvider getDescription() {
        return descriptionKey;
    }

    @Override
    public Supplier<Kit> getSupplier() {
        return supplier;
    }

    public enum HGKitLevel {
        SHIT(200), BAD(300), STANDARD(400), GOOD(450), UNAVAILABLE(Integer.MAX_VALUE);

        @Getter
        private final int price;

        HGKitLevel(int price) {
            this.price = price;
        }
    }

}