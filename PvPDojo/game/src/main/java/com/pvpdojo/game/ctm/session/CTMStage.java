/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.session;

import java.util.Collections;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.MushroomCow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.PlayerPreDeathEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.DojoPlayerJoinEvent;
import com.pvpdojo.bukkit.events.RegionEnterEvent;
import com.pvpdojo.bukkit.events.RegionEvent.CancelResult;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.ctm.CTM;
import com.pvpdojo.game.ctm.lang.CTMMessageKeys;
import com.pvpdojo.game.ctm.userdata.CTMUser;
import com.pvpdojo.game.ctm.util.CTMUtil;
import com.pvpdojo.game.session.StageHandler;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.session.arena.CTMArena;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.entity.TeamArenaEntity;
import com.pvpdojo.session.event.ArenaEntityDeathEvent;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Countdown;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class CTMStage extends StageHandler<CTMSession> {

    public CTMStage(CTMSession session) {
        super(session);
    }

    @Override
    public StageHandler init() {
        Game.inst().enableKits();

        session.setState(SessionState.STARTED);
        session.prepareEntities();

        for (CaptureRegion cregion : getSession().getCaptureRegions().values()) {
            cregion.respawn();
        }

        return super.init();
    }

    @Override
    public boolean isCountdown() {
        return false;
    }

    @Override
    public StageHandler getNextHandler() {
        return null;
    }

    public CTMSession getSession() {
        return (CTMSession) session;
    }

    @Override
    public void run() {
        super.run();

        for (Entry<UUID, Double> entry : getSession().getDamageMap().entrySet()) {
            getSession().getDamageMap().put(entry.getKey(), Math.min(CTMUtil.MAX_DAMAGE, entry.getValue() + 0.05));
        }

        if (getTimeTillRefill() == 0) {
            CTMUtil.refillMinifeasts(getSession().getArena().getMiniFeasts());
            session.broadcast(CTMMessageKeys.REFILL_MINIFEAST);
        } else if (Countdown.isUserFriendlyNumber(getTimeTillRefill())) {
            session.broadcast(CTMMessageKeys.REFILL_MINIFEAST_COUNTDOWN, "{time}", Countdown.getUserFriendlyNumber(getTimeTillRefill()));
        }

        boolean blueQuit = getSession().getTeamBlue().getMembers().stream().noneMatch(ArenaEntity::isOnline);
        boolean redQuit = getSession().getTeamRed().getMembers().stream().noneMatch(ArenaEntity::isOnline);

        if (blueQuit || redQuit) {
            session.broadcast(CTMMessageKeys.END_ALL_LEFT);

            if (blueQuit && redQuit) {
                session.end(getSession().getTeamBlue(), getSession().getTeamRed());
            } else if (blueQuit) {
                session.end(getSession().getTeamBlue());
            } else {
                session.end(getSession().getTeamRed());
            }

        }

    }

    public int getTimeTillRefill() {
        return 270 - (getTimeSeconds() % 271);
    }

    @EventHandler
    public void onDeath(ArenaEntityDeathEvent e) {

        if (e.getParentEvent() instanceof PlayerDeathEvent) {
            CTMUser user = CTMUser.getUser(e.getArenaEntity().getPlayer());
            CTMUtil.resetDamage(user.getUUID());

            PlayerDeathEvent event = (PlayerDeathEvent) e.getParentEvent();
            LocaleMessage deathMessage = StringUtils.handleDeathMessage(event, event.getEntity(), event.getEntity().getKiller(), null, null, false);
            e.addMessage(deathMessage);

            PvPDojo.schedule(() -> event.getEntity().spigot().respawn()).nextTick();
        }

    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        CTMSession session = CTM.inst().getSession();
        User user = User.getUser(e.getPlayer());

        session.prepareEntity(user);
        e.setRespawnLocation(session.getSpawnLocation(user));

        PvPDojo.schedule(() -> session.handleRespawn(e.getPlayer())).nextTick();
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (e.getCause() == DamageCause.VOID) {
            e.setDamage(100D);
        } else if (e.getEntity() instanceof Player && ((Player) e.getEntity()).isFrozen()) { // Frozen in CTM = respawning
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onDamageTrack(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player) {
            Player damager = e.getDamager() instanceof Player ? (Player) e.getDamager()
                    : e.getDamager() instanceof Projectile && ((Projectile) e.getDamager()).getShooter() instanceof Player ? (Player) ((Projectile) e.getDamager()).getShooter()
                    : null;
            if (damager != null && ((Player) e.getEntity()).getKiller() != damager) {
                CTMUtil.addDamage(e, damager);
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onDamageTrack(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player && ((Player) e.getEntity()).getKiller() != null) {
            CTMUtil.addDamage(e, ((Player) e.getEntity()).getKiller());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPreDeath(PlayerPreDeathEvent e) {
        CTM.inst().getSession().removeCowFromPlayer(e.getPlayer());
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        ApplicableRegionSet applicable = WGBukkit.getRegionManager(e.getPlayer().getWorld()).getApplicableRegions(e.getBlock().getLocation());
        CTMArena arena = CTM.inst().getSession().getArena();

        if (!Collections.disjoint(applicable.getRegions(), arena.getNoBuildZones()) || Collections.disjoint(applicable.getRegions(), arena.getBuildZones())) {
            e.setCancelled(true);
            e.getPlayer().sendMessage(CC.RED + "You can't build here");
        }

    }

    @EventHandler
    public void onRegionEnter(RegionEnterEvent e) {
        TeamArenaEntity team = User.getUser(e.getPlayer()).getTeam();
        if (team != null) {

            // Capture Zone Blue is on Red side and Capture Zone Red is on Blue side
            Set<ProtectedRegion> otherCaptureZone = team.getColor() == CC.RED ? getSession().getArena().getCaptureZonesBlue() : getSession().getArena().getCaptureZonesRed();

            ProtectedRegion spawnRegion = team.getColor() == CC.RED ? getSession().getArena().getSpawnRegionRed() : getSession().getArena().getSpawnRegionBlue();

            if (otherCaptureZone.contains(e.getRegion())) {
                e.setCancelResult(CancelResult.TELEPORT);
                e.setCancelled(true);
                e.getPlayer().sendMessage(CC.RED + "You can't enter this area");
            } else if (spawnRegion.equals(e.getRegion())) {
                getSession().tryCapture(e.getPlayer());
            }

        }

    }

    @EventHandler
    public void onInteractEntity(PlayerInteractEntityEvent e) {
        User user = User.getUser(e.getPlayer());

        if (user.getTeam() != null && e.getRightClicked() instanceof MushroomCow) {
            CTM.inst().getSession().handleCowClick(e);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoinHighest(DojoPlayerJoinEvent event) {
        CTMUser user = CTMUser.getUser(event.getPlayer());
        if (user.getTeam() != null) {
            event.setJoinMessage(user.getTeam().getColor() + "[" + user.getTeam().getName() + "] " + event.getJoinMessage());
        } else if (event.getServerSwitch() == null || !event.getServerSwitch().isSpecMode()) {
            CTMSession session = CTM.inst().getSession();
            session.addParticipant(user);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onQuit(PlayerQuitEvent event) {
        CTMUser user = CTMUser.getUser(event.getPlayer());
        if (user.getTeam() != null) {
            event.setQuitMessage(user.getTeam().getColor() + "[" + user.getTeam().getName() + "] " + event.getQuitMessage());
            if (!event.getPlayer().isDead()) { // Could be dead by combat log
                Bukkit.getPluginManager().callEvent(new EntityDamageEvent(event.getPlayer(), DamageCause.CUSTOM, Integer.MAX_VALUE));
                Bukkit.getPluginManager().callEvent(new PlayerPreDeathEvent(event.getPlayer()));
                event.getPlayer().setHealth(0D);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onLogin(AsyncPlayerPreLoginEvent e) {
        if (e.getLoginResult() != Result.KICK_OTHER) {
            e.allow();
        }
    }

}
