/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.userdata.kit.KitGameUser;
import com.pvpdojo.session.SessionState;

public class Vampire extends Kit {

    @Override
    public void onKill(PlayerDeathEvent e) {
        e.getEntity().getKiller().setMaxHealth(e.getEntity().getKiller().getMaxHealth() + 4);
    }

    @Override
    public void onTick(KitGameUser user) {
        if (HG.inst().getSession().getState() == SessionState.STARTED) {
            if (user.getPlayer() != null) {
                if (user.getPlayer().getWorld().getTime() >= 13000) {
                    user.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 6000, 0));
                }
            }
        }
    }

}
