/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.kits.defaults;

import java.util.function.Supplier;

import co.aikar.locales.MessageKeyProvider;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.kits.KitProvider;
import org.bukkit.inventory.ItemStack;

public enum NullProvider implements KitProvider {
    ;

    @Override
    public Supplier<Kit> getSupplier() {
        return null;
    }

    @Override
    public MessageKeyProvider getDescription() {
        return null;
    }

    @Override
    public ItemStack getIcon() {
        return null;
    }
}
