/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.session;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.util.FeastUtil;
import com.pvpdojo.game.session.StageHandler;
import com.pvpdojo.game.session.StagedSession;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.util.IntSet;

public class EHG extends PvPStage {

    private IntSet miniFeasts = new IntSet();

    public EHG(StagedSession session) {
        super(session);
        int amount = PvPDojo.RANDOM.nextInt(3) + 2;
        int min = HG.inst().getSettings().getSecondsTillPvP() + 180;
        int bound = HG.inst().getSettings().getSecondsTillFeast() - min + 1;
        if (bound > 1) {
            for (int i = 0; i < amount; i++) {
                miniFeasts.add(PvPDojo.RANDOM.nextInt(bound) + min);
            }
        }
    }

    @Override
    public StageHandler init() {
        // The time played is the invincibility time
        session.setState(SessionState.STARTED);
        setTimeSeconds(HG.inst().getSettings().getSecondsTillPvP());
        return super.init();
    }

    @Override
    public void run() {
        super.run();

        if (miniFeasts.contains(getTimeSeconds())) {
            FeastUtil.spawnMiniFeast();
        }

        if (getTimeSeconds() >= HG.inst().getSettings().getSecondsTillFeast()) {
            advance();
        }
    }

    @Override
    public StageHandler getNextHandler() {
        return new DefaultFeast(session);
    }

}
