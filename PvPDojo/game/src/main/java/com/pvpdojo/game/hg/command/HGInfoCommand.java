/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.pvpdojo.game.Game;
import com.pvpdojo.game.command.InfoCommand;
import com.pvpdojo.game.hg.userdata.HGUser;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("info")
public class HGInfoCommand extends InfoCommand {

    @Default
    @CatchUnknown
    @Override
    public void onInfo(CommandSender sender) {
        super.onInfo(sender);
        if (sender instanceof Player) {
            HGUser user = HGUser.getUser(((Player) sender).getUniqueId());
            sendMessage(sender, "Kills: " + user.getKills());
            sendMessage(sender, "Kit: " + (user.getKitHolder() != null ? user.getKitHolder().getKitProvider().getName() : "None"));
            sendMessage(sender, "Side Kit: " + (user.getSideKit() != null ? user.getSideKit().getKitProvider().getName() : "None"));
            sendMessage(sender, "IP: " + Game.inst().getSubdomain() + ".mc-hg.eu");
        }
    }
}
