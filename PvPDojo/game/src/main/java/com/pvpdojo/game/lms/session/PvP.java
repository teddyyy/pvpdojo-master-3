/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.lms.session;

import com.pvpdojo.game.session.StageHandler;
import com.pvpdojo.game.session.StagedSession;
import com.pvpdojo.session.SessionState;

public class PvP extends StageHandler {

    public PvP(StagedSession session) {
        super(session);
    }

    @Override
    public StageHandler init() {
        session.setState(SessionState.STARTED);
        return super.init();
    }

    @Override
    public boolean isCountdown() {
        return false;
    }

    @Override
    public StageHandler getNextHandler() {
        return null;
    }
}
