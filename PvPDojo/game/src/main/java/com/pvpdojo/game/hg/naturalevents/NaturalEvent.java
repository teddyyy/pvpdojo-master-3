/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.naturalevents;

import com.pvpdojo.lang.LocaleMessage;

public interface NaturalEvent {

    void start();

    void tick();

    void end();

    int getDuration();

    LocaleMessage getMessage();

}
