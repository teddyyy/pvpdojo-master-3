package com.pvpdojo.game.hg.team;

import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.session.HGRelogData;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.party.Party;
import com.pvpdojo.session.entity.ArenaEntity;
import lombok.Getter;

import java.util.UUID;

public class HGTeam extends Party {

    @Getter
    private int name;  // makes the party id kinda useless atm, but allows more flexibility for String names in the future

    public HGTeam(UUID uuid, int name) {
        super(uuid);
        this.name = name;
    }

    public int getKills() {
        return getUsers().stream().mapToInt(uuid -> {
            ArenaEntity ae = HG.inst().getSession().getParticipant(uuid);
            if (ae instanceof HGUser) {
                return ((HGUser)ae).getKills();
            }
            return ((HGRelogData)ae).getKills();
        }).sum();
    }

    public int size() {
        return getUsers().size();
    }

    @Override
    public boolean isValid() {
        return TeamList.contains(getPartyId()) && getLeader() != null;
    }
}
