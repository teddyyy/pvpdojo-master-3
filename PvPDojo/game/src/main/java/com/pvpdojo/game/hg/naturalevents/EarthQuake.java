/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.naturalevents;

import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.util.bukkit.CC;

public class EarthQuake implements NaturalEvent {

    private int counter;

    @Override
    public void start() {

    }

    @Override
    public void tick() {
        if (++counter % 4 == 0) {
            TrackerUtil.getIngame().filter(player -> player.getLocation().getY() <= 100).forEach(player -> {
                Vector vector = new Vector(PvPDojo.RANDOM.nextDouble() * 2 - 1, 1.8, PvPDojo.RANDOM.nextDouble() * 2 - 1);
                player.setVelocity(vector);
            });
        }
    }

    @Override
    public void end() {

    }

    @Override
    public int getDuration() {
        return 20;
    }

    @Override
    public LocaleMessage getMessage() {
        return LocaleMessage.of("The HG Gods are not satisfied with this game, you feel the earth rumble below you. " + CC.GOLD + "[Earthquake Event]");
    }
}
