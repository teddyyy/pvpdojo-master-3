/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.lang;

import co.aikar.locales.MessageKey;
import co.aikar.locales.MessageKeyProvider;

public enum HGMessageKeys implements MessageKeyProvider {
    FORCE_START, BONUSFEAST, FEAST, WIN, TIMEOUT, PIT, COUNTDOWN_START, PLAYERS_LEFT,
    SIDEBAR_PLAYERS, SIDEBAR_FEAST_BEGUN, SIDEBAR_AT, SIDEBAR_GAME_END,
    RESPAWN_KITITEMS, GAME_START, WIN_INFO, PIT_ELIMINATION, PIT_ELIMINATION_ACTIVE,
    ALREADY_IN_TEAM_OTHER, TEAM_LEADER_REQUIRED, INVITE_TEAM, INVITED_TEAM, NOT_IN_YOUR_TEAM,
    ALREADY_IN_TEAM, TOO_MANY_TEAMS, MAX_TEAM_SIZE_REACHED, TEAM_LEAVE;

    private final MessageKey key = MessageKey.of("pvpdojo-hg." + name().toLowerCase());

    @Override
    public MessageKey getMessageKey() {
        return key;
    }
}
