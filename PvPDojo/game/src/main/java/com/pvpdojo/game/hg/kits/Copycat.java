/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.event.entity.PlayerDeathEvent;

import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.kits.KitHolder;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;

public class Copycat extends Kit {

    @Override
    public void onKill(PlayerDeathEvent e) {
        HGUser victim = HGUser.getUser(e.getEntity());
        HGUser killer = HGUser.getUser(e.getEntity().getKiller());

        if (killer.getKitHolder().getKitProvider() != HGKit.COPYCAT) {
            killer.getKits().forEach(kit -> kit.onLeave(killer.getPlayer()));
            KitUtil.removeKitItems(killer);
        }

        if (victim.getKitHolder() == null) {
            killer.setKit(HGKit.COPYCAT);
        } else if (victim.getKitHolders().stream().map(KitHolder::getKitProvider).anyMatch(hgKit -> hgKit == HGKit.COPYCAT)) {
            killer.setKit(HGKit.COPYCAT);
            killer.addKit(HGKit.SURPRISE);
        } else {
            killer.setKit((HGKit) victim.getKitHolder().getKitProvider());
            killer.addKit(HGKit.COPYCAT);
            KitUtil.applyKitItems(killer);
            killer.sendMessage(CC.GREEN + "You are now " + StringUtils.getEnumName((HGKit) victim.getKitHolder().getKitProvider()));
        }
    }
}
