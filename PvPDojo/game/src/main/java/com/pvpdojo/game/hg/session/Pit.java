/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.session;

import com.boydti.fawe.FaweAPI;
import com.boydti.fawe.util.EditSessionBuilder;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.RegionLeaveEvent;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.lang.HGMessageKeys;
import com.pvpdojo.game.hg.naturalevents.NaturalEventManager;
import com.pvpdojo.game.hg.session.event.PitStartEvent;
import com.pvpdojo.game.hg.team.HGTeam;
import com.pvpdojo.game.hg.team.TeamList;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.session.StageHandler;
import com.pvpdojo.game.session.StagedSession;
import com.pvpdojo.game.userdata.GameUser;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.session.RelogData;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Countdown;
import com.pvpdojo.util.bukkit.CC;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.blocks.BaseBlock;
import com.sk89q.worldedit.patterns.SingleBlockPattern;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerPreDeathEvent;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.Comparator;

import static com.pvpdojo.util.StreamUtils.negate;

public class Pit extends PvPStage {

    private static final String PIT_REGION = "pit";
    private static final boolean debug = false;

    private Countdown pitCountdown;
    private Countdown eliminationCountdown;
    private boolean teleported;

    private int timer;

    public Pit(StagedSession session) {
        super(session);
    }

    @Override
    public void run() {
        super.run();

        if (getTimeSeconds() >= HG.inst().getSettings().getGameEnd()) {
            ArenaEntity winner = session.getParticipants().stream()
                                        .filter(entity -> entity instanceof HGUser)
                                        .map(entity -> (HGUser) entity)
                                        .filter(negate(User::isLoggedOut))
                                        .filter(GameUser::isIngame)
                                        .max(Comparator.comparingInt(HGUser::getKills))
                                        .orElse(null);
            if (winner != null) {
                session.end(session.getParticipants().stream().filter(negate(winner::equals)).toArray(ArenaEntity[]::new));
            } else {
                session.end(session.getParticipants().toArray(new ArenaEntity[0]));
            }
        }
    }

    @Override
    public StageHandler init() {
        HG.inst().getSettings().setNaturalEvents(false);
        NaturalEventManager.inst().cancelEvent();
        setTimeSeconds(HG.inst().getSettings().getSecondsTillPit());
        if (!HG.inst().getSettings().isSkipPit()) {
            if (debug) {
                pitCountdown = new Countdown(20).userCounter(i -> session.broadcast(HGMessageKeys.PIT, "{time}", Countdown.getUserFriendlyNumber(i)))
                        .finish(this::spawnPit).start();
            } else {
                pitCountdown = new Countdown(5 * 60).userCounter(i -> session.broadcast(HGMessageKeys.PIT, "{time}", Countdown.getUserFriendlyNumber(i)))
                        .finish(this::spawnPit).start();
            }
        }
        return super.init();
    }

    public void spawnPit() {
        EditSession editSession = new EditSessionBuilder(FaweAPI.getWorld(session.getArena().getWorld().getName())).fastmode(true).build();
        editSession.makeCylinder(new Vector(0, 8, 0), new SingleBlockPattern(new BaseBlock(0)), 30, 247, true);
        editSession.makeCylinder(new Vector(0, 7, 0), new SingleBlockPattern(new BaseBlock(1)), 30, 1, true);

        editSession.flushQueue();

        Bukkit.getPluginManager().callEvent(new PitStartEvent());

        session.prepareEntities();
        teleported = true;

        RegionManager manager = WorldGuardPlugin.inst().getRegionManager(session.getArena().getWorld());
        ProtectedRegion region = new ProtectedCuboidRegion(PIT_REGION, new BlockVector(-60, 0, -60), new BlockVector(60, 255, 60));
        region.setFlag(DefaultFlag.PASSTHROUGH, StateFlag.State.ALLOW);
        manager.addRegion(region);

        timer = session.getPlayersLeft() > 4 ? 10 : 5;

        if (HG.inst().getSettings().isPitElimination()) {
            resetElimination();
            session.broadcast(HGMessageKeys.PIT_ELIMINATION_ACTIVE, "{time}", Countdown.getUserFriendlyNumber(timer * 60));
        }
    }

    @EventHandler
    public void onPitLeave(RegionLeaveEvent e) {
        if (e.getRegion().getId().equals(PIT_REGION)) {
            if (GameUser.getUser(e.getPlayer()).isIngame()) {
                e.setCancelled(true);
                e.getPlayer().sendMessage(CC.RED + "You may not leave the pit area");
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (!hasStarted()) {
            return;
        }

        if (isOutsidePit(e.getBlock().getLocation())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if (!hasStarted()) {
            return;
        }

        if (isOutsidePit(e.getBlock().getLocation())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onLiquidBuild(BlockFormEvent e) {
        if (!hasStarted()) {
            return;
        }

        if (e.getNewState().getType() == Material.OBSIDIAN || e.getNewState().getType() == Material.COBBLESTONE || e.getNewState().getType() == Material.STONE) {
            if (isOutsidePit(e.getBlock().getLocation())) {
                e.setCancelled(true);
            }
        }
    }

    @Override
    public StageHandler getNextHandler() {
        return null;
    }

    public void resetElimination() {
        if (eliminationCountdown != null) {
            eliminationCountdown.cancel();
        }

        int playerCount = session.getPlayersLeft();
        if (playerCount < 3) {
            eliminationCountdown = null;
            return;
        }

        if (debug) {
            eliminationCountdown = new Countdown(timer * 10)
                    .userCounter(i -> session.broadcast(HGMessageKeys.PIT_ELIMINATION, "{time}", Countdown.getUserFriendlyNumber(i)))
                    .finish(this::eliminate).start();
        } else {
            eliminationCountdown = new Countdown(timer * 60)
                    .userCounter(i -> session.broadcast(HGMessageKeys.PIT_ELIMINATION, "{time}", Countdown.getUserFriendlyNumber(i)))
                    .finish(this::eliminate).start();
        }
    }

    private void eliminate() {
        ArenaEntity toEliminate = null;
        Comparator<ArenaEntity> comp = new Comparator<ArenaEntity>() {
            @Override
            public int compare(ArenaEntity a1, ArenaEntity a2) {
                return Integer.compare(getKills(a1), getKills(a2));
            }

            private int getKills(ArenaEntity a) {
                return a instanceof HGUser ? ((HGUser)a).getKills() : ((HGRelogData)a).getKills();
            }
        };

        if (HG.inst().getSettings().getTeamSize() <= 1) {
            toEliminate = session.getParticipants().stream()
                    .filter(negate(HG.inst().getSession().getCurrentlyDead()::contains))  //necessary?
                    .min(comp)
                    .orElse(null);
        } else {
            HGTeam leastKills = TeamList.getTeams().stream()
                    .filter(team -> team.isValid() && team.getUsers().size() > 0)
                    .min(Comparator.comparingInt(HGTeam::getKills)).orElse(null);
            if (leastKills != null) {
                toEliminate = leastKills.getUsers().stream()
                        .map(uuid -> session.getParticipant(uuid))
                        .filter(negate(HG.inst().getSession().getCurrentlyDead()::contains))  //necessary?
                        .min(comp)
                        .orElse(null);
            }
        }

        if (toEliminate == null) {
            return;
        }

        if (toEliminate.isOnline()) {
            if (!BukkitUtil.callEntityDamageEvent(toEliminate.getPlayer(), EntityDamageEvent.DamageCause.CUSTOM, 6).isCancelled()) {
                toEliminate.getPlayer().setMetadata("pit-elimination", new FixedMetadataValue(PvPDojo.get(), true));
                Bukkit.getPluginManager().callEvent(new PlayerPreDeathEvent(toEliminate.getPlayer()));
                toEliminate.getPlayer().setHealth(0D);
            }
        } else {
            RelogData relogData = (RelogData) toEliminate;
            HG.inst().getSession().postDeath(relogData);
            if (relogData.isUseStats()) {
                relogData.getStats().addDeath();
            }
            HG.inst().getSession().getParticipants().remove(relogData);
            session.broadcast(LocaleMessage.of(CC.AQUA + CC.stripColor(relogData.getName() + " was eliminated.")));
            session.broadcast(LocaleMessage.of(HGMessageKeys.PLAYERS_LEFT,
                    "{amount}", String.valueOf(session.getParticipants().stream().filter(negate(session.getCurrentlyDead()::contains)).count())));
            resetElimination();
        }
    }

    private boolean isOutsidePit(Location loc) {
        return Math.pow(Math.abs(loc.getBlockX()), 2) + Math.pow(Math.abs(loc.getBlockZ()), 2) > 1024 ||
                loc.getY() > 28;
    }

    public boolean hasStarted() {
        if (debug) {
            return getTimeSeconds() >= HG.inst().getSettings().getSecondsTillPit() + 20;
        }
        return getTimeSeconds() >= HG.inst().getSettings().getSecondsTillPit() + 5 * 60;
    }

    public Countdown getPitCountdown() {
        return pitCountdown;
    }

    public boolean isTeleported() {
        return teleported;
    }
}
