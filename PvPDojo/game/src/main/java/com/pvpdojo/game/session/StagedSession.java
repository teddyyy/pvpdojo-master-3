/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.session;

import static com.pvpdojo.util.StreamUtils.negate;
import static com.pvpdojo.util.StringUtils.getRandomHexString;

import java.util.Arrays;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.event.HandlerList;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.command.general.NickCommand;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.userdata.GameUser;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.replay.Recorder;
import com.pvpdojo.session.GameSession;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.arena.Arena;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.userdata.NickData;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.StringUtils;

public abstract class StagedSession extends GameSession {

    private StageHandler currentHandler;

    public StagedSession(Arena arena, SessionType type, ArenaEntity... participants) {
        super(arena, type, participants);
    }

    @Override
    public void addParticipant(ArenaEntity ae) {
        super.addParticipant(ae);
        if (ae instanceof GameUser && Game.inst().getGame().getSettings().isRandomizePlayers()) {
            NickCommand.nick((User) ae, new NickData(UUID.randomUUID(), getRandomHexString(6), CraftPlayer.STEVE_VALUE, CraftPlayer.STEVE_SIGNATURE));
        }
    }

    @Override
    public void start() {
        super.start();
        StageHandler firstHandler = getFirstStageHandler().init();
        if (firstHandler.isCountdown() && Game.inst().isPrivate()) {
            firstHandler.setTimeSeconds(60);
        }
        setCurrentHandler(firstHandler);
    }

    public abstract StageHandler getFirstStageHandler();

    @Override
    public void end(ArenaEntity... losers) {
        super.end(losers);
        currentHandler.timerTask.cancel();
        HandlerList.unregisterAll(currentHandler);

        PvPDojo.schedule(() -> {
            if (Game.inst().isPrivate() || StringUtils.UUID.matcher(PvPDojo.get().getFolder()).matches()) {
                BukkitUtil.shutdown();
            } else {
                BukkitUtil.restart();
            }
        }).createTask(30 * 20);
    }

    public StageHandler getCurrentHandler() {
        return currentHandler;
    }

    public void setCurrentHandler(StageHandler currentHandler) {
        this.currentHandler = currentHandler;
    }

    @Override
    public void broadcast(LocaleMessage msg) {
        Bukkit.getOnlinePlayers().stream().map(User::getUser).forEach(user -> user.sendMessage(msg));
        if (replay != null) {
            long time = System.currentTimeMillis();
            Recorder.inst().getTasks().add(() -> Arrays.stream(msg.getDefaultLocale()).forEach(message -> replay.chat(message, time)));
        }
    }

    public int getInitialPlayerCount() {
        if (getCurrentHandler() instanceof QueueableStage) {
            return ((QueueableStage) getCurrentHandler()).getPlayerCountWithQueue();
        }
        return getParticipants().size();
    }

    public int getPlayersLeft() {
        if (getCurrentHandler() instanceof QueueableStage) {
            return ((QueueableStage) getCurrentHandler()).getPlayerCountWithQueue();
        }
        return (int) getParticipants().stream().filter(negate(getCurrentlyDead()::contains)).count();
    }

}
