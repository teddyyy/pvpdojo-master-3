package com.pvpdojo.game.hg.kits;

import com.pvpdojo.game.Game;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.util.EnumOrderAccess;
import com.pvpdojo.util.TimeUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Magneto extends Kit {

    private static final Map<Material, Double> POWER = new HashMap<>();

    static {
        //INCREASEMENT
        add(Material.COMPASS, .15);
        add(Material.IRON_AXE, .2);
        add(Material.IRON_PICKAXE, .2);
        add(Material.IRON_SPADE, .2);
        add(Material.IRON_HOE, .2);
        add(Material.IRON_INGOT, .15);
        add(Material.IRON_BLOCK, .3);
        add(Material.IRON_FENCE, .2);
        add(Material.IRON_ORE, .15);
        add(Material.IRON_HELMET, .1);
        add(Material.IRON_CHESTPLATE, .3);
        add(Material.IRON_LEGGINGS, .23);
        add(Material.IRON_BOOTS, .1);
        add(Material.BUCKET, .2);
        add(Material.WATER_BUCKET, .3);
        add(Material.LAVA_BUCKET, .3);
        add(Material.MILK_BUCKET, .2);
        add(Material.ANVIL, .5);
        add(Material.GOLD_SWORD, .1);
        add(Material.GOLD_HELMET, .1);
        add(Material.GOLD_BOOTS, .1);
        add(Material.GOLD_CHESTPLATE, .2);
        add(Material.GOLD_LEGGINGS, .2);
        add(Material.GOLD_AXE, .2);
        add(Material.GOLD_PICKAXE, .2);
        add(Material.GOLD_SPADE, .1);
        add(Material.GOLD_HOE, .1);
        add(Material.GOLD_INGOT, .1);
        add(Material.GOLD_BLOCK, .2);
        add(Material.GOLD_ORE, .1);
        add(Material.DIAMOND_SWORD, .5);
        add(Material.DIAMOND_HELMET, .3);
        add(Material.DIAMOND_BOOTS, .3);
        add(Material.DIAMOND_CHESTPLATE, .5);
        add(Material.DIAMOND_LEGGINGS, .5);
        add(Material.DIAMOND_AXE, .3);
        add(Material.DIAMOND_PICKAXE, .3);
        add(Material.DIAMOND_SPADE, .3);
        add(Material.DIAMOND_HOE, .2);
        add(Material.DIAMOND, .3);
        add(Material.DIAMOND_BLOCK, .5);
        add(Material.DIAMOND_ORE, .3);

        //DECREASEMENT
        add(Material.LEATHER, 0.01);
        add(Material.LEATHER_HELMET, 0.07);
        add(Material.LEATHER_CHESTPLATE, 0.09);
        add(Material.LEATHER_LEGGINGS, 0.04);
        add(Material.LEATHER_BOOTS, 0.06);

    }

    private static void add(Material material, double power) {
        POWER.put(material, power * 0.001);
    }


    private int ticks;
    private MagnetMode type = MagnetMode.Pull;

    private long lastChange;//Prevent spamming

    @Override
    public void onInteract(PlayerInteractEvent e) {

        if (e.getItem() == null) {
            return;
        }

        if (e.getPlayer().getItemInHand().isSimilar(getStartItems()[0])) {
            if (TimeUtil.hasPassed(lastChange, TimeUnit.SECONDS, 2)) {
                type = type.next();
                e.getPlayer().sendMessage(CC.GREEN + "Switched to " + getStageName());
                e.getPlayer().updateInventory();
                lastChange = System.currentTimeMillis();
            } else {
                e.getPlayer().sendMessage(CC.RED + "Relaxo.");
            }
        }
    }

    private static final double MAX_LENGTH = 12;
    private static final double RADIUS = 8.0;

    @Override
    public void onItemSwitch(PlayerItemHeldEvent e) {

        ItemStack item = e.getPlayer().getInventory().getContents()[e.getNewSlot()];

        if (item == null) {
            return;
        }

        if (!item.isSimilar(getStartItems()[0])) {
            return;
        }

        if (getKitCooldown().isOnCooldown()) {
            getKitCooldown().sendCooldown(HGUser.getUser(e.getPlayer()));
            return;
        }

        if (TrackerUtil.getNearbyIngame(e.getPlayer(), RADIUS, RADIUS, RADIUS).count() == 0) {
            return;
        }

        e.getPlayer().sendMessage(CC.GREEN + "Activating " + getStageName());

        new BukkitRunnable() {

            @Override
            public void run() {

                if (!e.getPlayer().getItemInHand().isSimilar(getStartItems()[0])) {
                    cancel();
                    return;
                }

                if (++ticks >= (MAX_LENGTH * 20)) {
                    cancel();
                    ticks = 0;
                    if (!getKitCooldown().isOnCooldown()) {
                        getKitCooldown().use();
                    }
                    return;
                }

                TrackerUtil.getNearbyIngame(e.getPlayer(), RADIUS, RADIUS, RADIUS)
                        .forEach(target -> {
                            double amount = getPower(target);

                            target.setVelocity(e.getPlayer().getEyeLocation().getDirection()
                                    .add(target.getLocation().toVector().subtract(e.getPlayer().getLocation().toVector()).normalize().multiply(type == MagnetMode.Push ? amount : -amount)));
                            target.setFallDistance(0);//reset fall damage
                        });

            }
        }.runTaskTimer(Game.inst(), 5, 5);

    }

    @Override
    public int getCooldown() {
        return 60_000;
    }

    private double getPower(Player from) {
        double power = 0.0D;
        double maxPullAndPush = 1.0;

        //Test Increasements, add Decreasesments later.
        for (ItemStack inventory : from.getInventory().getContents()) {
            if (inventory != null) {
                Material type = inventory.getType();

                if (POWER.containsKey(type)) {
                    if ((power + POWER.get(type)) < maxPullAndPush) {
                        power += POWER.get(type);
                    } else {
                        power = maxPullAndPush;
                    }
                }
            }
        }

        for (ItemStack armour : from.getInventory().getArmorContents()) {
            if (armour != null) {
                Material type = armour.getType();

                if (POWER.containsKey(type)) {
                    if (power < maxPullAndPush) {
                        power += POWER.get(type);
                    } else {
                        power = maxPullAndPush;
                    }
                }
            }
        }
        return power;

    }

    private String getStageName() {
        CC color = type == MagnetMode.Pull ? CC.RED : CC.BLUE;
        return color + type.name();

    }


    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[]{new ItemBuilder(Material.IRON_INGOT).name(CC.GREEN + "Magneto").build()};
    }

    public enum MagnetMode implements EnumOrderAccess<MagnetMode> {
        Pull, Push
    }
}
