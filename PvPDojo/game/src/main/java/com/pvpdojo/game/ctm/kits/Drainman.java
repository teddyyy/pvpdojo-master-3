package com.pvpdojo.game.ctm.kits;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import com.pvpdojo.game.Game;
import com.pvpdojo.game.ctm.CTM;
import com.pvpdojo.game.ctm.DamageConsumingKit;
import com.pvpdojo.game.ctm.lang.CTMKitMessageKeys;
import com.pvpdojo.game.ctm.util.CTMUtil;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class Drainman extends Kit implements DamageConsumingKit {

    private static final String DRAIN_META = "drain_arrow";

    @Override
    public void onBowShoot(EntityShootBowEvent e) {
        Player player = (Player) e.getEntity();

        if (player.isSneaking()) {
            if (consumeDamage(player)) {
                e.getProjectile().setMetadata(DRAIN_META, new FixedMetadataValue(Game.inst(), null));
            } else {
                e.setCancelled(true);
            }
        }

    }

    @Override
    public void onProjectileHit(EntityDamageByEntityEvent e) {

        if (!e.isCancelled() && e.getEntity() instanceof Player && e.getDamager().hasMetadata(DRAIN_META)) {
            CTMUtil.resetDamage(e.getEntity().getUniqueId());

            String drained = User.getUser((Player) e.getEntity()).getTeam().getColor() + ((Player) e.getEntity()).getNick();
            Player player = (Player) ((Projectile) e.getDamager()).getShooter();
            String shooter = User.getUser(player).getTeam().getColor() + player.getNick();

            CTM.inst().getSession().broadcast(CTMKitMessageKeys.DRAINMAN_HIT, "{drained}", drained, "{shooter}", shooter);
        }

    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] { new ItemBuilder(Material.BOW).name(CC.GOLD + "Drainbow").build(), new ItemStack(Material.ARROW, 5) };
    }

    @Override
    public double getDamageNeeded() {
        return 15D;
    }
}
