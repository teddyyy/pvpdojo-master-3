/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.lms;

import org.bukkit.Bukkit;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.UserList;
import com.pvpdojo.abilities.Abilities;
import com.pvpdojo.extension.DojoServer;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.extension.GameExtension;
import com.pvpdojo.game.lms.session.LMSSession;
import com.pvpdojo.game.lms.settings.LMSSettings;
import com.pvpdojo.game.lms.userdata.LMSUser;
import com.pvpdojo.map.DojoMap;
import com.pvpdojo.map.MapLoader;
import com.pvpdojo.map.MapType;
import com.pvpdojo.session.arena.DojoGamesArena;

public class LMS extends GameExtension<LMSSession, LMSSettings, LMSUser> {

    private static LMS instance;

    public static LMS inst() {
        return instance;
    }

    public static LMS register(DojoServer server, LMSSettings settings) {
        instance = new LMS(server, settings);
        return instance;
    }

    private LMS(DojoServer server, LMSSettings settings) {
        super(server, settings);
    }

    @Override
    public LMSSession createSession() {
        return new LMSSession((DojoGamesArena) PvPDojo.get().getStandardMap());
    }

    @Override
    public void preLoad() {
        PvPDojo.LANG.addMessageBundles("pvpdojo-lms");
        getServer().getSettings().setReducedFallDamage(getSettings().isAbilities());
        getServer().getSettings().setNaturalRegeneration(getSettings().isAbilities());
        getServer().getSettings().setAbilities(getSettings().isAbilities());
        getServer().getSettings().setSoup(true);
    }

    @Override
    public void start() {}

    @Override
    public void postLoad() {
        super.postLoad();
        if (getSettings().isAbilities()) {
            Abilities.get().start();
        }
    }

    @Override
    public UserList<LMSUser> createUserList() {
        return new UserList<>(LMSUser::new);
    }

    @Override
    public DojoMap setupMap() {
        String forcedMap = Game.inst().getGameProperties().getForcedMap();
        DojoGamesArena map = forcedMap != null ? MapLoader.getMap(MapType.DOJOGAMES, forcedMap) : MapLoader.getRandomMap(MapType.DOJOGAMES);

        map.setWorld(Bukkit.getWorlds().get(0));
        map.shift(-map.getLocations()[0].getBlockX(), 0, -map.getLocations()[0].getBlockZ());
        map.setup(0);
        map.placeBlocks(false);
        map.setOccupied();
        return map;
    }
}
