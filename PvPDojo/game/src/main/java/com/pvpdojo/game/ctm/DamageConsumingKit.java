package com.pvpdojo.game.ctm;

import com.pvpdojo.game.ctm.lang.CTMKitMessageKeys;
import com.pvpdojo.game.ctm.lang.CTMMessageKeys;
import com.pvpdojo.game.ctm.util.CTMUtil;
import com.pvpdojo.userdata.User;
import org.bukkit.entity.Player;

public interface DamageConsumingKit {

    default boolean hasDamage(Player player) {
        return CTMUtil.getDamage(player.getUniqueId()) >= getDamageNeeded() && !CTMUtil.isCapturing(player.getUniqueId());
    }

    default boolean consumeDamage(Player player) {
        boolean hasDamage = hasDamage(player);
        if (hasDamage) {
            CTMUtil.removeDamage(player.getUniqueId(), getDamageNeeded());
        } else {
            User.getUser(player).sendMessage(CTMKitMessageKeys.DAMAGE_NEEDED, "{damage}", String.valueOf(getDamageNeeded()));
        }
        return hasDamage;
    }

    double getDamageNeeded();

}
