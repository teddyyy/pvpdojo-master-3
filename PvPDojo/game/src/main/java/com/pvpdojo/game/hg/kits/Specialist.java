/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.kits.Kit;

public class Specialist extends Kit implements InteractingKit {

    @Override
    public void activate(PlayerInteractEvent player) {}

    @Override
    public void click(Player player) {
        player.openEnchanting(player.getLocation(), true);
    }

    @Override
    public void onKill(PlayerDeathEvent e) {
        e.getEntity().getKiller().setExp(e.getEntity().getKiller().getExp() + 0.5F);
        if (e.getEntity().getKiller().getExp() > 1) {
            e.getEntity().getKiller().setExp(e.getEntity().getKiller().getExp() - 1);
            e.getEntity().getKiller().setLevel(e.getEntity().getKiller().getLevel() + 1);
        }
    }

    @Override
    public void onEnchantItem(EnchantItemEvent e) {
        e.getEnchantsToAdd().put(Enchantment.DAMAGE_ALL, 1);
    }

    @Override
    public void onExp(PlayerExpChangeEvent e) {
        e.setAmount((int) (e.getAmount() / 1.5));
    }

    @Override
    public ItemStack getClickItem() { return new ItemStack(Material.ENCHANTED_BOOK); }

    @Override
    public boolean isSendCooldown() { return false; }
}
