/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionType;

import com.boydti.fawe.FaweAPI;
import com.boydti.fawe.util.EditSessionBuilder;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.util.RandomInventory;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.PotionBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.WoolBuilder;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.Vector2D;
import com.sk89q.worldedit.blocks.BaseBlock;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.patterns.SingleBlockPattern;
import com.sk89q.worldedit.regions.CylinderRegion;

public class FeastUtil {

    public static CylinderRegion spawnFeastPlatform(Location location) {
        PvPDojo.schedule(() -> {
            EditSession editSession = new EditSessionBuilder(FaweAPI.getWorld(location.getWorld().getName())).fastmode(true).build();
            editSession.makeCylinder(BukkitUtil.toVector(location), new SingleBlockPattern(new BaseBlock(2)), 16, 1, true);
            editSession.makeCylinder(BukkitUtil.toVector(location).add(0, 1, 0), new SingleBlockPattern(new BaseBlock(0)), 16, 30, true);

            editSession.flushQueue();
        }).createAsyncTask();
        return new CylinderRegion(FaweAPI.getWorld(location.getWorld().getName()), BukkitUtil.toVector(location), new Vector2D(16, 16), location.getBlockY(), location.getBlockY() + 10);
    }

    public static Location findFeastLocation(World world, int bounds) {
        Location feastLocation;

        int loops = 0;
        do {
            loops++;
            int x = PvPDojo.RANDOM.nextInt(bounds * 2) - bounds, z = PvPDojo.RANDOM.nextInt(bounds * 2) - bounds;
            feastLocation = new Location(world, x, world.getHighestBlockYAt(x, z), z);
        } while (feastLocation.getBlock().getType() == Material.WATER
                || feastLocation.getBlock().getType() == Material.STATIONARY_WATER
                || feastLocation.getBlock().getType() == Material.LAVA
                || feastLocation.getBlock().getType() == Material.STATIONARY_LAVA
                || feastLocation.getBlock().getType() == Material.LEAVES
                || feastLocation.getBlock().getType() == Material.AIR
                || feastLocation.getBlock().getType() == Material.LOG
                || feastLocation.getBlock().getType() == Material.LOG_2
                || feastLocation.getY() > 75
                && loops < 100);

        if (loops >= 100) {
            Bukkit.broadcastMessage("Feast spawned incorrectly weird terrain?");
        }

        return feastLocation.add(0, 1, 0);
    }

    public static final RandomInventory<ItemStack> FEAST = RandomInventory.randomItemStacks()
                                                                          .addItem(new PotionBuilder(PotionType.STRENGTH).level(1).splash().build(), 10)
                                                                          .addItem(new ItemStack(Material.DIAMOND_SWORD), 30)
                                                                          .addItem(new ItemStack(Material.DIAMOND_HELMET), 30)
                                                                          .addItem(new ItemStack(Material.DIAMOND_CHESTPLATE), 30)
                                                                          .addItem(new ItemStack(Material.DIAMOND_LEGGINGS), 30)
                                                                          .addItem(new ItemStack(Material.DIAMOND_BOOTS), 30)
                                                                          .addItem(new ItemStack(Material.TNT), 40, 2, 5)
                                                                          .addItem(new ItemStack(Material.ENDER_PEARL), 40, 1, 3)
                                                                          .addItem(new ItemStack(Material.LAVA_BUCKET), 40)
                                                                          .addItem(new ItemBuilder(Material.FLINT_AND_STEEL).durability((short) (Material.FLINT_AND_STEEL.getMaxDurability() - 10)).build(), 40)
                                                                          .addItem(new ItemStack(Material.WATER_BUCKET), 40)
                                                                          .addItem(new ItemStack(Material.WEB), 40, 1, 3)
                                                                          .addItem(new ItemStack(Material.BOW), 50)
                                                                          .addItem(new ItemStack(Material.ARROW), 65, 5, 12)
                                                                          .addItem(new PotionBuilder(PotionType.INSTANT_DAMAGE).splash().build(), 65)
                                                                          .addItem(new PotionBuilder(PotionType.POISON).splash().build(), 65)
                                                                          .addItem(new PotionBuilder(PotionType.REGEN).splash().extend().build(), 65)
                                                                          .addItem(new ItemStack(Material.MUSHROOM_SOUP), 70, 4, 8)
                                                                          .addItem(new ItemStack(Material.COOKED_BEEF), 25, 5, 9)
                                                                          .addItem(new ItemStack(Material.GRILLED_PORK), 15, 5, 9)
                                                                          .addItem(new ItemStack(Material.COOKED_CHICKEN), 15, 5, 9)
                                                                          .addItem(new ItemStack(Material.CARROT_ITEM), 15, 5, 9);

    public static void spawnFeastChests(Location loc) {
        loc.add(0, 1, 0);
        loc.getBlock().setType(Material.ENCHANTMENT_TABLE);

        // pingi failed to do math for a feast lmao doing it manually
        List<Location> locs = new ArrayList<>(Arrays.asList(loc.clone().add(1, 0, 1), loc.clone().add(1, 0, -1), loc.clone().add(-1, 0, 1),
                loc.clone().add(-1, 0, -1), loc.clone().add(2, 0, 0), loc.clone().add(2, 0, 2), loc.clone().add(2, 0, -2),
                loc.clone().add(0, 0, 2), loc.clone().add(0, 0, -2), loc.clone().add(-2, 0, 2), loc.clone().add(-2, 0, 0),
                loc.clone().add(-2, 0, -2)));

        locs.stream().map(Location::getBlock).forEach(block -> setChest(block, FEAST));
    }

    public static final RandomInventory<ItemStack> MINI_FEAST = RandomInventory.randomItemStacks()
                                                                               .addItem(new ItemStack(Material.GOLD_INGOT), 70, 3, 6)
                                                                               .addItem(new WoolBuilder(DyeColor.BLACK).name(CC.GREEN + "Present").build(), 80)
                                                                               .addItem(new PotionBuilder(PotionType.POISON).splash().build(), 80)
                                                                               .addItem(new PotionBuilder(PotionType.INSTANT_DAMAGE).splash().build(), 80)
                                                                               .addItem(new PotionBuilder(PotionType.SLOWNESS).splash().build(), 80)
                                                                               .addItem(new ItemStack(Material.BROWN_MUSHROOM), 130, 6, 15)
                                                                               .addItem(new ItemStack(Material.RED_MUSHROOM), 130, 6, 15)
                                                                               .addItem(new ItemBuilder(Material.INK_SACK).durability((short) 3).build(), 130, 3, 15);

    public static void spawnMiniFeast() {
        int bound = (HG.inst().getSettings().getBorder() - 50) * 2 + 1;
        int x = PvPDojo.RANDOM.nextInt(bound) - (HG.inst().getSettings().getBorder() - 50);
        int z = PvPDojo.RANDOM.nextInt(bound) - (HG.inst().getSettings().getBorder() - 50);

        Block block = BlockUtil.getHighestBlock(Bukkit.getWorlds().get(0), x, z);

        for (int i = -2; i <= 2; i++) {
            for (int j = -2; j <= 2; j++) {
                Block cur = BlockUtil.getHighestBlock(block.getWorld(), x + i, z + j);
                if (block.getY() < cur.getY()) {
                    block = cur;
                }
            }
        }

        block = block.getLocation().add(0, 2, 0).getBlock();

        for (int i = -2; i <= 2; i++) {
            for (int j = -2; j <= 2; j++) {
                block.getLocation().add(i, 0, j).getBlock().setType(Material.GLASS);
            }
        }

        block.getLocation().add(2, 0, 2).getBlock().setType(Material.AIR);
        block.getLocation().add(-2, 0, 2).getBlock().setType(Material.AIR);
        block.getLocation().add(2, 0, -2).getBlock().setType(Material.AIR);
        block.getLocation().add(-2, 0, -2).getBlock().setType(Material.AIR);

        block = block.getRelative(BlockFace.UP);

        block.setType(Material.ENCHANTMENT_TABLE);
        setChest(block.getRelative(BlockFace.SOUTH_EAST), MINI_FEAST);
        setChest(block.getRelative(BlockFace.SOUTH_WEST), MINI_FEAST);
        setChest(block.getRelative(BlockFace.NORTH_EAST), MINI_FEAST);
        setChest(block.getRelative(BlockFace.NORTH_WEST), MINI_FEAST);

        int roundX = roundTo100(block.getX());
        int roundZ = roundTo100(block.getZ());

        String fullMessage = CC.RED + "A mini feast has appeared between ( x: " + (roundX - 100) + " and x: " + roundX + " ) and ( z: " + (roundZ - 100) + " and z: " + roundZ + " )";
        String posNegMessage = CC.RED + "A mini feast has appeared ( x: " + (roundX < 0 ? "-" : "+") + " and z: " + (roundZ < 0 ? "-" : "+") + " )";

        for (Player player : Bukkit.getOnlinePlayers()) {
            double points = EquipmentUtil.getEquipmentPoints(player);
            if (points <= EquipmentUtil.FULL_MINI_FEAST) {
                player.sendMessage(fullMessage);
            } else if (points <= EquipmentUtil.POS_NEG_MINI_FEAST) {
                player.sendMessage(posNegMessage);
            } else {
                player.sendMessage(CC.RED + "Minifeasts are only for the poor");
            }
        }
    }

    public static void setChest(Block block, RandomInventory<ItemStack> lootTable) {
        setChest(block, lootTable, 3);
    }

    public static void setChest(Block block, RandomInventory<ItemStack> lootTable, int minItems) {
        block.setType(Material.CHEST);
        Chest chest = (Chest) block.getState();
        for (int i = 0; i <= PvPDojo.RANDOM.nextInt(7) + minItems; i++) {
            chest.getInventory().setItem(PvPDojo.RANDOM.nextInt(chest.getInventory().getSize()), lootTable.generateRandomItem());
        }
    }

    private static int roundTo100(int x) {
        if (x > 0) {
            x += 100;
        }
        return x / 100 * 100;
    }
}
