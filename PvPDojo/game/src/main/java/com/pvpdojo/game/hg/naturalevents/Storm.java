/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.naturalevents;

import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.kits.Gladiator;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.util.bukkit.CC;

import java.util.Arrays;
import java.util.List;

public class Storm implements NaturalEvent {

    private static final List<HGKit> BLOCKING_KITS = Arrays.asList(HGKit.ENDERMAGE, HGKit.KAYA, HGKit.LAUNCHER, HGKit.JACKHAMMER,
            HGKit.CHAMELEON, HGKit.LUMBERJACK, HGKit.WORM);

    private int initialBorder;
    private int counter;

    @Override
    public void start() {
        initialBorder = HG.inst().getSettings().getBorder();
    }

    @Override
    public void tick() {
        if (++counter % 25 == 0) {
            if (counter > 420) {
                HG.inst().getSettings().setBorder(HG.inst().getSettings().getBorder() + 100);
                if (counter % 25 == 0) {
                    HG.inst().getSession().broadcast(LocaleMessage.of(CC.GREEN + "Storm moves away: Border at " + HG.inst().getSettings().getBorder() + " blocks"));
                }
            } else if (counter <= 300) {
                HG.inst().getSettings().setBorder(HG.inst().getSettings().getBorder() - 25);
                if (counter % 25 == 0) {
                    HG.inst().getSession().broadcast(LocaleMessage.of(CC.RED + "" + CC.BOLD + "ATTENTION " + CC.GOLD + "Border is shrinking to "
                            + CC.RED + HG.inst().getSettings().getBorder() + " blocks"));
                }
            }
        }
    }

    @Override
    public void end() {
        HG.inst().getSettings().setBorder(initialBorder);
    }

    @Override
    public int getDuration() {
        return 495;
    }

    @Override
    public LocaleMessage getMessage() {
        return LocaleMessage.of("The HG Gods are not satisfied with this game, the border is shrinking. " + CC.GOLD + "[Storm]");
    }

    public boolean canSpawn() {
        if (TrackerUtil.getIngame().filter(player -> BLOCKING_KITS.contains(HGUser.getUser(player).getKitHolder().getKitProvider())).count()
                >= (TrackerUtil.getIngame().count() / 3)) {
            // At least one third of the players has kits that block the storm event.
            return false;
        }
        if(!Gladiator.MATCH_MAP.isEmpty()) {
            // There are ongoing Gladiator matches.
            return false;
        }
        return true;
    }
}
