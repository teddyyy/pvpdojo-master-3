/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class Viper extends Kit {

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {
        if(e.isCancelled() || !PlayerUtil.isRealHit((LivingEntity) e.getEntity())) {
            return;
        }

        if (PvPDojo.RANDOM.nextInt(6) == 2) {
            ((LivingEntity) e.getEntity()).addPotionEffect(new PotionEffect(PotionEffectType.POISON, 5 * 20, 1));
        } else if (PvPDojo.RANDOM.nextInt(3) == 2) {
            ((LivingEntity) e.getEntity()).addPotionEffect(new PotionEffect(PotionEffectType.POISON, 5 * 20, 0));
        }
    }
}
