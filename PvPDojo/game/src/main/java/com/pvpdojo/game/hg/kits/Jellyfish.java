package com.pvpdojo.game.hg.kits;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.ItemUtil;

public class Jellyfish extends ComplexKit {

    private int uses;
    private static final Set<Block> JELLYFISH = new HashSet<>();

    @Override
    public void onInteract(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getPlayer().getItemInHand().getType() == Material.AIR
                    || ItemUtil.SWORDS.contains(e.getPlayer().getItemInHand().getType())) {

                if (kitCooldown.isOnCooldown()) {
                    getKitCooldown().sendCooldown(User.getUser(e.getPlayer()));
                    return;
                }
                Location clickLoc = e.getClickedBlock().getLocation();
                BlockFace face = e.getBlockFace();
                Block block = clickLoc.getBlock().getRelative(face);

                if (block.getType() == Material.STATIONARY_WATER || block.getType() == Material.WATER) {
                    return;
                }

                if(block.getType() != Material.AIR) {
                    return;
                }

                block.setType(Material.WATER);
                JELLYFISH.add(block);

                PvPDojo.schedule(() -> {
                    block.setType(Material.AIR);
                    JELLYFISH.remove(block);
                }).createTask(20 * 5);

                if (++uses >= 4) {
                    kitCooldown.use();
                    uses = 0;
                }
            }

        }
    }

    @EventHandler
    public void onBlockFlow(BlockFromToEvent e) {
        if (JELLYFISH.contains(e.getBlock()))
            e.setCancelled(true);
    }

    @EventHandler
    public void onMoveJellyfish(PlayerMoveEvent e) {
        if (JELLYFISH.isEmpty()) {
            return;
        }

        HGUser user = HGUser.getUser(e.getPlayer());

        if (user.isIngame() && KitUtil.getKitProvider(user) != HGKit.JELLYFISH) {
            Block[] blocks = new Block[] { e.getPlayer().getLocation().getBlock(),
                    e.getPlayer().getLocation().getBlock().getRelative(BlockFace.UP) };
            for (Block block : blocks) {
                if (JELLYFISH.contains(block)) {
                    e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20 * 5, 0));
                    e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 20 * 5, 0));
                    e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.POISON, 20 * 5, 0));
                    e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 20 * 5, 0));
                }
            }

        }
    }

    @Override
    public int getCooldown() {
        return 25_000;
    }
}
