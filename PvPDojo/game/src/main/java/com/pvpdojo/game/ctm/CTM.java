/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerShearEntityEvent;
import org.bukkit.event.world.ChunkUnloadEvent;

import com.pvpdojo.UserList;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.extension.DojoServer;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.ctm.session.CTMSession;
import com.pvpdojo.game.ctm.session.CaptureRegion;
import com.pvpdojo.game.ctm.settings.CTMSettings;
import com.pvpdojo.game.ctm.userdata.CTMUser;
import com.pvpdojo.game.ctm.util.CTMBat;
import com.pvpdojo.game.extension.GameExtension;
import com.pvpdojo.map.DojoMap;
import com.pvpdojo.map.MapLoader;
import com.pvpdojo.map.MapType;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.arena.CTMArena;
import com.pvpdojo.session.entity.ArenaTeam;
import com.pvpdojo.session.entity.PreparedArenaTeam;
import com.pvpdojo.util.bukkit.CC;

public class CTM extends GameExtension<CTMSession, CTMSettings, CTMUser> implements DojoListener {

    private static CTM instance;

    private PreparedArenaTeam teamRed;
    private PreparedArenaTeam teamBlue;

    public static CTM register(DojoServer server, CTMSettings settings, PreparedArenaTeam teamRed, PreparedArenaTeam teamBlue) {
        instance = new CTM(server, settings, teamRed, teamBlue);
        return instance;
    }

    public static CTM inst() {
        return instance;
    }

    public CTM(DojoServer server, CTMSettings settings, PreparedArenaTeam teamRed, PreparedArenaTeam teamBlue) {
        super(server, settings);
        this.teamRed = teamRed;
        this.teamBlue = teamBlue;
    }

    @Override
    public CTMSession createSession() {
        return new CTMSession((CTMArena) PvPDojo.get().getStandardMap(), SessionType.C_T_M, teamRed != null ? teamRed : new ArenaTeam(CC.RED), teamBlue != null ? teamBlue : new ArenaTeam(CC.BLUE));
    }

    @Override
    public void preLoad() {
        PvPDojo.LANG.addMessageBundles("pvpdojo-ctm", "pvpdojo-ctm-kits");

        getServer().getSettings().setReducedFallDamage(false);
        getServer().getSettings().setNaturalRegeneration(true);
        getServer().getSettings().setSoup(true);
        getServer().getSettings().setAbilities(false);
        getServer().getSettings().setAllowNaturalBlockChange(true);
        getServer().getSettings().getBlockedInteraction().clear();
        getServer().getSettings().setAllowQuitMessage(true);
        getServer().getSettings().setAllowJoinMessage(true);
        getServer().getSettings().setStats(false);
    }

    @Override
    public void start() {
        register();
        getServer().getSettings().setItemDespawnRate(Bukkit.getWorlds().get(0), 180);
        NMSUtils.registerEntity("Bat", 65, CTMBat.class);
    }

    @Override
    public UserList<CTMUser> createUserList() {
        return new UserList<>(CTMUser::new);
    }

    @Override
    public DojoMap setupMap() {
        String forcedMap = Game.inst().getGameProperties().getForcedMap();
        CTMArena map = forcedMap != null ? MapLoader.getMap(MapType.CTM, forcedMap) : MapLoader.getRandomMap(MapType.CTM);

        getSettings().setMaxPlayers(map.getMaxPlayers());
        map.setWorld(Bukkit.getWorlds().get(0));
        map.shift(-map.getLocations()[0].getBlockX(), 0, -map.getLocations()[0].getBlockZ());
        map.setup(0);
        map.placeBlocks(false);
        map.setOccupied();
        return map;
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (e.getEntity().hasMetadata(CaptureRegion.CAPTURE_ENTITY)) {
            e.setCancelled(true);
        }
    }

    // We do not want our cow sheared...
    @EventHandler
    public void onShear(PlayerShearEntityEvent e) {
        if (e.getEntity().hasMetadata(CaptureRegion.CAPTURE_ENTITY)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onChunkUnload(ChunkUnloadEvent e) {
        e.setCancelled(true);
    }

}
