/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder.LeatherArmorBuilder;

public class Snorlax extends Kit {

    private int eatenSoups = 0;

    private boolean hasRelaxoEffect = false;

    @Override
    public void onDeath(PlayerDeathEvent e) {
        e.getDrops().removeIf(item -> item.getType() == Material.LEATHER_BOOTS);
    }

    @Override
    public void onConsume(PlayerItemConsumeEvent e) {

        if (e.getItem().getType() == Material.MUSHROOM_SOUP && !hasRelaxoEffect) {
            eatenSoups++;
        }

        if (hasRelaxoEffect && e.getItem().getType() == Material.MUSHROOM_SOUP) {
            if ((hasRelaxoEffect = e.getPlayer().hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE))) {
                for (PotionEffect potion : e.getPlayer().getActivePotionEffects()) {
                    if (potion.getType().equals(PotionEffectType.DAMAGE_RESISTANCE) && potion.getDuration() <= 4 * 20) {
                        e.getPlayer().sendMessage(CC.GREEN + "+ resistance");
                        e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 10 * 21, 1), true);
                    }
                }
            }
        }

        if (eatenSoups >= 32) {
            e.getPlayer().sendMessage(CC.GREEN + "Your stomach seems to have transformed all the mushroom soup into resistance...");
            e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 15 * 20, 1));
            hasRelaxoEffect = true;
            eatenSoups = 0;
        }
    }


    @Override
    public ItemStack[] getStartArmor() {
        return new ItemStack[] { new LeatherArmorBuilder(Material.LEATHER_BOOTS, Color.AQUA).unbreakable().enchant(Enchantment.PROTECTION_FALL, 2).build() };
    }
}
