/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.kits;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.game.hg.kits.Gladiator;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.ItemUtil;

public class Switcher extends Kit {

    @Override
    public void onProjectileHit(EntityDamageByEntityEvent e) {
        if (!e.isCancelled() && e.getDamager() instanceof Snowball) {
            Player player = (Player) ((Snowball) e.getDamager()).getShooter();

            if (Gladiator.isInGladiator(player)) {
                return;
            }

            Location playerLocation = player.getLocation();
            player.teleport(e.getEntity().getLocation());
            e.getEntity().teleport(playerLocation);
            ItemUtil.addItemWhenFree(player, getStartItems());
        }
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] { new ItemStack(Material.SNOW_BALL) };
    }

}
