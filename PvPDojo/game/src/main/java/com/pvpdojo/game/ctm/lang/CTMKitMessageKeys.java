package com.pvpdojo.game.ctm.lang;

import co.aikar.locales.MessageKey;
import co.aikar.locales.MessageKeyProvider;

public enum CTMKitMessageKeys implements MessageKeyProvider {
    DRAINMAN_HIT,
    NUKER_CANNOT_USE_FALL,
    BLINDWAVE_SUMMON,SHOCKWAVE_SUMMON, SPEEDWAVE_SUMMON,
    DAMAGE_NEEDED, ACHILLES_ACTIVATE, ACHILLES_EXPIRE, ACHILLES_EFFECT;

    private MessageKey key = MessageKey.of("pvpdojo-ctm-kits." + name());

    @Override
    public MessageKey getMessageKey() {
        return key;
    }
}
