/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.naturalevents;

import org.bukkit.Location;
import org.bukkit.entity.Fireball;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.util.bukkit.CC;

public class MeteorShower implements NaturalEvent {

	private int counter;
	public static final String METEOR_SHOWER = "such_meteor_shower";

	@Override
	public void start() {

	}

	@Override
	public void tick() {
		if (++counter % 9 == 0) {
			TrackerUtil.getIngame().forEach(player -> {
				Location loc = player.getLocation();
				for (int x = -8; x <= 8; x += 4) {
					for (int z = -8; z <= 8; z += 4) {
						Location fireLoc = new Location(player.getWorld(), loc.getBlockX() + x,
								player.getWorld().getHighestBlockYAt(loc) + 64, loc.getBlockZ() + z);
						Fireball fireball = player.getWorld().spawn(fireLoc, Fireball.class);
						fireball.setDirection(new Vector(0, -0.15, 0));
						fireball.setYield(2F);
						fireball.setIsIncendiary(true);
						fireball.setMetadata(METEOR_SHOWER, new FixedMetadataValue(PvPDojo.get(), null));
					}
				}
			});
		}
	}

	@Override
	public void end() {

	}

	@Override
	public int getDuration() {
		return 27;
	}

	@Override
	public LocaleMessage getMessage() {
		return LocaleMessage.of("The HG Gods are not satisfied with this game, watch the skies for falling rocks. "
				+ CC.GOLD + "[Meteor Shower]");
	}
	
}
