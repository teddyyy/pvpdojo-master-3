package com.pvpdojo.game.hg.kits;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.kits.KitProvider;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.game.userdata.kit.KitGameUser;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.entity.EntityPortalEnterEvent;
import org.bukkit.event.entity.PlayerPreDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

public class Portal extends ComplexKit {

    private final ItemStack ENTERANCE_ITEM = new ItemBuilder(Material.STAINED_GLASS_PANE).name(CC.GOLD + "Enterance Gate").durability(DyeColor.ORANGE.getWoolData()).build();
    private final ItemStack EXIT_ITEM = new ItemBuilder(Material.STAINED_GLASS_PANE).name(CC.BLUE + "Exit Gate").durability(DyeColor.BLUE.getWoolData()).build();

    private final List<Block> ENTERANCE = new ArrayList<>();
    private final List<Block> EXIT = new ArrayList<>();

    private boolean DELAY;

    @Override
    public void onInteract(PlayerInteractEvent e) {

        Player player = e.getPlayer();

        if (player.getItemInHand().isSimilar(ENTERANCE_ITEM) || player.getItemInHand().isSimilar(EXIT_ITEM)) {

            e.setCancelled(true);
            player.updateInventory();

            if (e.getClickedBlock() == null) {
                return;
            }

            Location loc = e.getClickedBlock().getLocation();
            Block block = loc.getBlock().getRelative(e.getBlockFace());
            Block relative = loc.getBlock().getRelative(e.getBlockFace()).getRelative(0, 1, 0);

            if (block.getType() != Material.AIR || relative.getType() != Material.AIR) {
                return;
            }

            //Make them they can not override an existing portal.
            if ((ENTERANCE.contains(block) || ENTERANCE.contains(relative)) || (EXIT.contains(block) || EXIT.contains(relative))) {
                return;
            }

            if (player.getItemInHand().isSimilar(ENTERANCE_ITEM)) {

                //Has already made a portal before. destroy it and make a new one.
                if (!ENTERANCE.isEmpty()) {
                    destroyGates(true);
                    PvPDojo.schedule(() -> createGate(block, true)).nextTick();
                } else {
                    createGate(block, true);
                }

            } else if (player.getItemInHand().isSimilar(EXIT_ITEM)) {
                //Messages will be removed/replaced, Adding those for some debugs and tests

                if (ENTERANCE.isEmpty()) {
                    player.sendMessage(CC.RED + "You have to create the enterance portal!");
                    return;
                }

                if (!EXIT.isEmpty()) {
                    player.sendMessage(CC.RED + "You already have an exist portal, Destroy it to make a new one!");
                    return;
                }

                Block firstPortal = ENTERANCE.get(0);

                if (firstPortal.getLocation().distance(block.getLocation()) <= 5) {
                    player.sendMessage(CC.RED + "You can not place the second portal as close to the first one by 5 blocks!");
                    return;
                }

                if (firstPortal.getLocation().distance(block.getLocation()) > 20) {
                    player.sendMessage(CC.RED + "You can not create the exit portal further away than enterance portal by 20 blocks!");
                    return;
                }

                createGate(block, false);

            }
        }
    }


    //Cleanup after they die.
    @Override
    public void onPreDeath(PlayerPreDeathEvent e) {
        destroyGates(true);
        destroyGates(false);
    }

    @Override
    public void onLeave(Player player) {
        destroyGates(true);
        destroyGates(false);
    }

    @EventHandler
    public void onPhysics(BlockPhysicsEvent e) {
        if (EXIT.contains(e.getBlock()) || ENTERANCE.contains(e.getBlock())) {
            e.setCancelled(true);
        }
    }

    @Override
    public void onTick(KitGameUser<? extends KitProvider> user) {

        if (!EXIT.isEmpty()) {
            Block block = EXIT.get(0);

            if (block.getType() != Material.PORTAL) {
                EXIT.remove(block);
            }
        }

        if (!ENTERANCE.isEmpty()) {
            Block block = ENTERANCE.get(0);

            if (block.getType() != Material.PORTAL) {
                ENTERANCE.remove(block);
            }
        }

    }

    @EventHandler
    public void onPortalEnter(EntityPortalEnterEvent e) {

        streamThingy(e.getLocation().getBlock(), player -> {

            if (e.getEntity() instanceof Player) {
                Player p = (Player) e.getEntity();
                HGUser user = HGUser.getUser(p);

                if (user.isAdminMode() || user.isVanish() || !user.isIngame()) {
                    return;
                }
            }

            Portal kit = (Portal) HGUser.getUser(player).getKit();
            Location to;

            if (kit.DELAY) {
                return;
            }

            kit.DELAY = true;
            PvPDojo.schedule(() -> kit.DELAY = false).createTask(30);

            if (kit.ENTERANCE.contains(e.getLocation().getBlock())) {
                to = kit.EXIT.isEmpty() ? null : kit.EXIT.get(0).getLocation();
            } else {
                to = kit.ENTERANCE.isEmpty() ? null : kit.ENTERANCE.get(0).getLocation();
            }

            if (to == null) {
                if (e.getEntity() instanceof Player) {
                    ((Player) e.getEntity()).sendMessage(CC.RED + "Couldn't find a suitable location.");
                }
                return;
            }

            to.add(0.5, 0.5, 0.5);
            to.setPitch(e.getEntity().getLocation().getPitch());
            to.setYaw(e.getEntity().getLocation().getYaw());

            e.getEntity().teleport(to);

            if (e.getEntity() instanceof Player) {
                Player p = (Player) e.getEntity();
                HGUser user = HGUser.getUser(p);

                user.setInvincibleSeconds(5);
            }

        });
    }

    @EventHandler
    public void onPlayerInteract(BlockDamageEvent e) {

        Player player = e.getPlayer();

        if (e.isCancelled()) {
            return;
        }

        if (e.getBlock() == null) {
            return;
        }

        HGUser user = HGUser.getUser(player);

        if (user.isAdminMode() || user.isVanish() || !user.isIngame()) {
            return;
        }

        streamThingy(e.getBlock(), target -> {
            Portal kit = (Portal) HGUser.getUser(target).getKit();
            kit.destroyGates(kit.ENTERANCE.contains(e.getBlock()));

            if (target != player) {
                target.sendMessage(CC.RED + "One of your Portals has been destroyed!");
            }
        });
    }

    private void createGate(Block block, boolean isFirstPortal) {
        block.setType(Material.PORTAL);

        PvPDojo.schedule(() -> {
            if (isFirstPortal) {
                ENTERANCE.add(block.getLocation().getBlock());
            } else {
                EXIT.add(block.getLocation().getBlock());
            }
        }).createTask(5);
    }

    public void destroyGates(boolean isFirstPortal) {
        List<Block> iterator = isFirstPortal ? ENTERANCE : EXIT;

        Iterator<Block> blockIterator = iterator.iterator();

        while (blockIterator.hasNext()) {
            Block next = blockIterator.next();

            if (iterator.contains(next)) {
                if (next.getType() == Material.PORTAL) {
                    next.setType(Material.AIR);
                }
                blockIterator.remove();
            }
        }

        if (iterator == EXIT) {
            EXIT.clear();
        } else {
            ENTERANCE.clear();
        }
    }


    public void streamThingy(Block block, Consumer<Player> action) {
        TrackerUtil.getIngame()
                .filter(players -> KitUtil.hasKit(HGUser.getUser(players), HGKit.PORTAL))
                .filter(players -> ((Portal) HGUser.getUser(players).getKit()).EXIT.contains(block) || ((Portal) HGUser.getUser(players).getKit()).ENTERANCE.contains(block))
                .filter(players -> block.getType() == Material.PORTAL)
                .forEach(action);
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[]{ENTERANCE_ITEM, EXIT_ITEM};
    }
}
