/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.userdata;

import com.pvpdojo.game.hg.kits.menu.KitSelectorMenu;
import com.pvpdojo.game.hg.kits.menu.KitShopMenu;
import com.pvpdojo.game.hg.kits.menu.SideKitSelectorMenu;
import com.pvpdojo.gui.HotbarGUI;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.menus.SettingsMenu.CustomHGSettingsMenu;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.CompBuilder;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.BookBuilder;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder.FormatRetention;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;

import java.util.function.BiConsumer;

public class PreGameHotbarGUI extends HotbarGUI {

    private static final String[] BOOK = new String[] {
            test(new CompBuilder("Overview\n").bold(true).color(ChatColor.BLACK)
                                              .append("---------\n").strikethrough(true)
                                              .append("Welcome to the Hardcore Games. A battle to the death, the victor being the last player standing. " +
                                                      "Select your Kit to suit your playstyle. Dont forget to gather materials to craft soups to heal yourself instantly!", FormatRetention.NONE)
                                              .color(ChatColor.BLACK)),
            test(new CompBuilder("Kits\n").bold(true).color(ChatColor.BLACK)
                                        .append("---------\n").strikethrough(true)
                                        .append("Kits are an essential part of HG. They determine your playstyle and give you distinct advantages to aid you " +
                                                "in your battles. You have access to 8 free Kits which rotate every friday.", FormatRetention.NONE)
                                        .color(ChatColor.BLACK)),
            "You can buy Kits in the Kit Shop in the pregame with credits earned by kills (10 credits) and wins (20 credits), participating in a round gives you 2 credits automatically." +
            "Choosing a Bonus Kit will grant you double credits and 3 extra SP after winning.",
            "Tip: Always keep in mind which kit your opponent is using and keep an eye on the chat for info about special kits.",
            test(new CompBuilder("Getting Started\n").bold(true).color(ChatColor.BLACK)
                                                   .append("---------\n").strikethrough(true)
                                                   .append("The first thing you want to do is gather materials to craft your basic equipment. " +
                                                           "A stone sword, bowls and your preferred material for crafting soups should always be part of your inventory.", FormatRetention.NONE)
                                                   .color(ChatColor.BLACK)),
            "You can collect mushrooms in most biomes but you will find the most in Swamplands, you need both types of shrooms to craft a Mushroom Stew.",
            "You can collect the most cacti in deserts but mesa biomes also offer some, you need 2 cacti to craft Cactus Juice.",
            "You can collect cocoa beans from the trees in jungles, you only need 1 cocoa bean to craft some Chocolate Milk.",
            test(new CompBuilder("Tracking\n").bold(true).color(ChatColor.BLACK)
                                            .append("---------\n").strikethrough(true)
                                            .append("Clicking on your Tracker will make it point to an opponent that is < 20 blocks away from you. Reusing it will update the " +
                                                    "location/player it is pointing to.\n" +
                                                    "You can use /feast to make it point to feast once it is announced.", FormatRetention.NONE)
                                            .color(ChatColor.BLACK)),
            test(new CompBuilder("Feasts\n").bold(true).color(ChatColor.BLACK)
                                          .append("---------\n").strikethrough(true)
                                          .append("There are Minifeasts which spawn randomly after the timer hit 5 minutes, you will be informed about their location in chat." +
                                                  " They offer helpful materials such as recraft or iron/gold ingots or Kit Presents.", FormatRetention.NONE)
                                          .color(ChatColor.BLACK)),
            "The Feast will be announced at 15 minutes and start 5 minutes later. It always spawns around the spawn area but exact coordinates vary. You will find diamond equipment," +
                    " potions and other strong helpful items inside to aid you in your last battles!",
            "The Bonus Feast is identical to the regular Feast in terms of size and materials found inside. The difference is that it spawns 40 minutes into the game and you are not" +
                    " informed about its exact location but instead see the biome it spawned in.",

            test(new CompBuilder("The Pit\n").bold(true).color(ChatColor.BLACK)
                                           .append("---------\n").strikethrough(true)
                                           .append("The Pit regularly occurs 45 minutes into the game. Every player is teleported into a pit to help bring the game " +
                                                   "to an end. If the game hasn't ended after 60 minutes the player with the most kills wins.", FormatRetention.NONE)
                                           .color(ChatColor.BLACK)),
            test(new CompBuilder("The Leaderboard\n").bold(true).color(ChatColor.BLACK)
                                           .append("---------\n").strikethrough(true)
                                           .append("The HG Leaderboard consists of the players with the most SP (SkillPoints) SP: 1 Kill = 0.5, 1 GamePlayed = -0.5, " +
                                                   "1 Win = 7 SP\n" +
                                                   "You can see the top 10 in Hub if you warp to Leaderboard, the website shows the top 50.", FormatRetention.NONE)
                                           .color(ChatColor.BLACK))

    };
    private static final PreGameHotbarGUI inst = new PreGameHotbarGUI();

    public static PreGameHotbarGUI get() {
        return inst;
    }

    private PreGameHotbarGUI() {
        setItem(0, new ItemBuilder(Material.CHEST).name(CC.GREEN + "Kit Selector").build(), onRightClick(player -> new KitSelectorMenu(player).open()));
        setItem(1, new ItemBuilder(Material.STORAGE_MINECART).name(CC.GREEN + "Side Kit Selector").build(), onRightClick(player -> new SideKitSelectorMenu(player).open()));
        setItem(3, new ItemBuilder(Material.DIAMOND).name(CC.GREEN + "Queue").build(), onRightClick(player -> player.performCommand("queue")));
        setItem(4, new ItemBuilder(Material.EMERALD).name(CC.GREEN + "Kit Shop").build(), onRightClick(player -> {
            HGUser user = HGUser.getUser(player);
            if (user.getPersistentKits().isLoaded()) {
                new KitShopMenu(player).open();
            } else {
                user.sendMessage(MessageKeys.DATA_NOT_LOADED);
            }
        }));
        setItem(5, new ItemBuilder(Material.ENDER_CHEST).name(CC.GOLD + "Bonus Kit").build(), onRightClick(player -> {
            HGUser user = HGUser.getUser(player);
            if (user.getBonusKit() != null) {
                player.performCommand("kit " + user.getBonusKit().getName());
            }
        }));
        setItem(7, new BookBuilder().author("Chris7o").title(CC.RED + "PvPDojo HG").pages(BOOK).build(), (BiConsumer<Player, Action>) null);
        setItem(8, new ItemBuilder(Material.DIODE).name(CC.RED + "Settings").build(), onRightClick(player -> new CustomHGSettingsMenu(player, User.getUser(player).getSettings()).open()));
    }

    public static String test(CompBuilder builder) {
        return BaseComponent.toLegacyText(builder.create());
    }
}
