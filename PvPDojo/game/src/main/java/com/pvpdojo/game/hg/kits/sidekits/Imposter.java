package com.pvpdojo.game.hg.kits.sidekits;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.kits.EntityInteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.userdata.GameUser;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import net.minecraft.util.com.mojang.authlib.GameProfile;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_7_R4.entity.disguise.CraftPlayerDisguise;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public class Imposter extends Kit implements EntityInteractingKit {

    private boolean imposter = false;

    @Override
    public void activate(PlayerInteractEntityEvent event) {

        if (event.getRightClicked() instanceof Player) {
            GameUser user = GameUser.getUser((Player) event.getRightClicked());
            if (!user.isSpectator()) {

                imposter = true;

                Player player = event.getPlayer();
                Player fetched = (Player) event.getRightClicked();

                GameProfile fetchedProfile = new GameProfile(UUID.randomUUID(), fetched.getNick());
                player.setDisguise(new CraftPlayerDisguise(player, fetchedProfile));

                ItemStack[] oldArmorContents = player.getInventory().getArmorContents();
                player.getInventory().setArmorContents(fetched.getInventory().getArmorContents());
                player.updateInventory();

                player.sendMessage(CC.GREEN + "You are now disguised as " + fetched.getDisplayName());

                PvPDojo.schedule(() -> {
                    player.setDisguise(null);
                    player.getInventory().setArmorContents(oldArmorContents);
                    player.sendMessage(CC.RED + "You are no longer disguised");
                    imposter = false;
                }).createTask(40 * 20);

                kitCooldown.use();
            }
        }
    }

    @Override
    public void onInvClick(InventoryClickEvent e) {
        if (imposter && e.getInventory() != null && e.getSlot() > 35) {
            e.setCancelled(true);
        }
    }

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.FLINT);
    }

    @Override
    public int getCooldown() {
        return 70000;
    }
}
