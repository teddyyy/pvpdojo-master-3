/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.naturalevents;

import java.util.Set;

import org.bukkit.block.Biome;

public abstract class BiomeExcludingEvent implements NaturalEvent {

    abstract Set<Biome> getExcludedBiomes();

}
