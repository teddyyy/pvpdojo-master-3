/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.pvpdojo.game.Game;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.lang.HGKitMessageKeys;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.EntityInteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.util.bukkit.ItemUtil;

public class Joker extends Kit implements EntityInteractingKit {

    @Override
    public void activate(PlayerInteractEntityEvent e) {
        if (HG.inst().getSession().getState() != SessionState.PREGAME && e.getRightClicked() instanceof Player) {

            kitCooldown.use();
            HGUser.getUser(e.getPlayer()).sendMessage(HGKitMessageKeys.JOKER_SHUFFLE, "{target}", ((Player) e.getRightClicked()).getNick());
            HGUser.getUser((Player) e.getRightClicked()).sendMessage(HGKitMessageKeys.JOKER_SHUFFLED);
            ((Player) e.getRightClicked()).addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20, 0));

            Inventory inv = ((Player) e.getRightClicked()).getInventory();

            new BukkitRunnable() {
                int s = 0;
                ItemStack previous;

                public void run() {
                    if (s < 10) {
                        if (s != 0) {
                            inv.setItem((s - 1), previous);
                        }
                        if (s < 9) {
                            previous = inv.getItem(s);
                            inv.setItem(s, new ItemStack(Material.SKULL_ITEM, 1, (short) 0));
                        } else {
                            inv.setItem(8, previous);
                        }

                        s++;

                        for (int i = 0; i < 18; i++) {
                            int x = (int) (Math.random() * 9);
                            ItemStack itemx = inv.getItem(x);
                            ItemStack itemi = inv.getItem(i);
                            if (itemx != null && itemi != null) {
                                if (!itemx.getType().equals(Material.SKULL_ITEM) && !itemi.getType().equals(Material.SKULL_ITEM)) {
                                    if (!ItemUtil.SWORDS.contains(itemx.getType()) && !ItemUtil.SWORDS.contains(itemi.getType())) {
                                        inv.setItem(x, itemi);
                                        inv.setItem(i, itemx);
                                    }
                                }
                            }
                        }
                    } else {
                        this.cancel();
                    }
                }

            }.runTaskTimer(Game.inst(), 0, 2);
        }
    }


    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.RECORD_3);
    }

    @Override
    public int getCooldown() {
        return 75_000;
    }
}

