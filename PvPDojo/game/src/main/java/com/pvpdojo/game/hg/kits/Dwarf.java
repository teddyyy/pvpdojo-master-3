package com.pvpdojo.game.hg.kits;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.session.SessionState;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class Dwarf extends Kit {

    private double chargePowerMulitplier = 0.1D;
    private int charge = 0;
    private int chargeIncreasement = 20;

    @Override
    public void onToggleSneak(PlayerToggleSneakEvent e) {
        Player player = e.getPlayer();
        if (e.isSneaking() && HG.inst().getSession().getState() == SessionState.STARTED) {

            if (kitCooldown.isOnCooldown()) {
                getKitCooldown().sendCooldown(HGUser.getUser(player));
                return;
            }

            if (charge != 0) {
                charge = 0;
            }

            new BukkitRunnable() {
                int tick = 0;

                @Override
                public void run() {
                    if (player.isDead() || !player.isSneaking() && tick != -2) {
                        if (charge < 100 && (charge > chargeIncreasement)) {
                            yeet(player, charge * chargePowerMulitplier);
                            if (!kitCooldown.isOnCooldown()) {
                                kitCooldown.use();
                            }
                        }
                        charge = 0;
                        this.cancel();
                        return;
                    }

                    if (charge >= 100) {
                        charge = 100;
                    }

                    if (charge == 100) {
                        if (tick != -1 && tick != -2) {
                            tick = -1;
                        }
                    }

                    if (tick == -1) {
                        player.sendMessage(ChatColor.GREEN + "Fully Charged");
                        tick = -2;
                    }

                    if (tick == -2) {
                        if (!player.isSneaking()) {
                            kitCooldown.use();
                            yeet(player, charge * chargePowerMulitplier);
                            player.sendMessage(ChatColor.GREEN + "Released at FULL POWER!");
                            charge = 0;
                            this.cancel();
                            return;
                        }
                    }

                    if (tick % 20 == 0 && tick > -1) {
                        if (charge < 100) {
                            charge += chargeIncreasement;
                        }
                        player.sendMessage(ChatColor.GREEN + "Charged for " + (charge) + "%");

                    }

                    if (tick >= 0) {
                        tick++;
                    }
                }
            }.runTaskTimer(PvPDojo.get(), 0, 1);

        }
    }

    private void yeet(Player player, final double power) {
        Vector vec = player.getEyeLocation().getDirection().normalize().multiply(power);

        if (vec.getY() > 1.4) {
            vec.setY(1.4);
        }

        TrackerUtil.getNearbyIngame(player, 8, 8, 8).forEach(nearby -> {
            NMSUtils.damageEntity(nearby, player, EntityDamageEvent.DamageCause.MAGIC, 8.0);
            nearby.setVelocity(vec);
        });

        player.getWorld().playSound(player.getLocation(), Sound.IRONGOLEM_HIT, 1, 1);

    }

    @Override
    public int getCooldown() {
        return 35_000;
    }
}
