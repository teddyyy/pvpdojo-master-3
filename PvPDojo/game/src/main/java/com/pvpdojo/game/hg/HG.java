/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg;

import static com.pvpdojo.util.StringUtils.getEnumNameList;

import co.aikar.commands.ConditionFailedException;
import com.pvpdojo.game.hg.command.*;
import com.pvpdojo.session.SessionState;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.Location;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.UserList;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.command.lib.DojoCommandManager;
import com.pvpdojo.extension.DojoServer;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.command.QueueCommand;
import com.pvpdojo.game.extension.GameExtension;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.kits.rotation.KitRotation;
import com.pvpdojo.game.hg.kits.rotation.KitRotationData;
import com.pvpdojo.game.hg.listener.BlockListener;
import com.pvpdojo.game.hg.listener.CraftListener;
import com.pvpdojo.game.hg.listener.EntityListener;
import com.pvpdojo.game.hg.listener.InteractListener;
import com.pvpdojo.game.hg.listener.JoinListener;
import com.pvpdojo.game.hg.listener.MoveListener;
import com.pvpdojo.game.hg.listener.QuitListener;
import com.pvpdojo.game.hg.listener.RegionListener;
import com.pvpdojo.game.hg.session.HGSession;
import com.pvpdojo.game.hg.settings.HGSettings;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.hg.userdata.HGUserImpl;
import com.pvpdojo.game.hg.util.MapUtil;
import com.pvpdojo.game.kits.KitProvider;
import com.pvpdojo.map.DojoMap;
import com.pvpdojo.map.MapLoader;
import com.pvpdojo.map.MapType;
import com.pvpdojo.session.arena.HGArena;

import co.aikar.commands.InvalidCommandArgument;

public class HG extends GameExtension<HGSession, HGSettings, HGUser> {

    private static HG instance;
    private Location feastLocation;
    private KitRotationData currentRotation;

    public static HG register(DojoServer server, HGSettings settings) {
        instance = new HG(server, settings);
        return instance;
    }

    public static HG inst() {
        return instance;
    }

    private HG(DojoServer server, HGSettings settings) {
        super(server, settings);
    }

    @Override
    public void preLoad() {
        PvPDojo.LANG.addMessageBundles("pvpdojo-hg", "pvpdojo-hg-kits", "pvpdojo-hg-kits-info");
        PvPDojo.get().getServerSettings().setTerrainControl(true);
    }

    @Override
    public void start() {
        getServer().getSettings().getBlockedInteraction().clear();
        getServer().getSettings().setAbilities(false);
        getServer().getSettings().setSoup(true);
        getServer().getSettings().setReducedFallDamage(false);
        getServer().getSettings().setNaturalRegeneration(true);
        getServer().getSettings().setAllowNaturalBlockChange(true);
        getServer().getSettings().getLaunchMaterials().clear();
        getServer().getSettings().enableAlternativeRecraft();

        NMSUtils.get(Bukkit.getWorlds().get(0)).spigotConfig.itemDespawnRate = 6000;
        NMSUtils.get(Bukkit.getWorlds().get(0)).spigotConfig.arrowDespawnRate = 1200;
        NMSUtils.get(Bukkit.getWorlds().get(0)).spigotConfig.viewDistance = 6;

        Bukkit.getWorlds().get(0).setDifficulty(Difficulty.NORMAL);
        Bukkit.getWorlds().get(0).setAmbientSpawnLimit(15);
        Bukkit.getWorlds().get(0).setMonsterSpawnLimit(50);
        Bukkit.getWorlds().get(0).setAnimalSpawnLimit(15);
        Bukkit.getWorlds().get(0).setWaterAnimalSpawnLimit(5);
        Bukkit.getWorlds().get(0).setTicksPerAnimalSpawns(400);
        Bukkit.getWorlds().get(0).setTicksPerMonsterSpawns(1);

        new RegionListener().register(Game.inst());
        new BlockListener().register(Game.inst());
        new InteractListener().register(Game.inst());
        new MoveListener().register(Game.inst());
        new CraftListener().register(Game.inst());
        new JoinListener().register(Game.inst());
        new EntityListener().register(Game.inst());
        new QuitListener().register(Game.inst());

    }

    @Override
    public void postLoad() {
        super.postLoad();

        DojoCommandManager.get().registerCommand(new HGStatsCommand(), true);
        DojoCommandManager.get().registerCommand(new HGBalanceCommand(), true);
        DojoCommandManager.get().registerCommand(new HGInfoCommand(), true);
        DojoCommandManager.get().registerCommand(new FeastCommand());
        DojoCommandManager.get().registerCommand(new KitCommand());
        DojoCommandManager.get().registerCommand(new SideKitCommand());
        DojoCommandManager.get().registerCommand(new SpawnCommand());
        DojoCommandManager.get().registerCommand(new RandomCommand());
        DojoCommandManager.get().registerCommand(new ForceStartCommand());
        DojoCommandManager.get().registerCommand(new HGTestCommand());
        DojoCommandManager.get().registerCommand(new KitInfoCommand());
        DojoCommandManager.get().registerCommand(new SetKitCommand());

        DojoCommandManager.get().getCommandCompletions().registerCompletion("kits", context -> getEnumNameList(HGKit.values()));
        DojoCommandManager.get().getCommandContexts().registerContext(KitProvider.class, ctx -> {
            try {
                return HGKit.valueOf(ctx.popFirstArg());
            } catch (IllegalArgumentException ex) {
                throw new InvalidCommandArgument();
            }
        });
        DojoCommandManager.get().registerCommand(new QueueCommand());
        DojoCommandManager.get().registerCommand(new SpawnNaturalCommand());
        DojoCommandManager.get().registerCommand(new HGTeamCommand());

        DojoCommandManager.get().getCommandConditions().addCondition("teamsactivated", c -> {
            if (HG.inst().getSettings().getTeamSize() <= 1) {
                throw new ConditionFailedException("Teams aren´t activated in this round");
            }
        });
        DojoCommandManager.get().getCommandConditions().addCondition("notstarted", c -> {
            if (HG.inst().getSession().getState() != SessionState.PREGAME &&
                    HG.inst().getSession().getState() != SessionState.INVINCIBILITY) {
                throw new ConditionFailedException("The round has already started");
            }
        });
        DojoCommandManager.get().getCommandConditions().addCondition("hasteam", c -> {
            if (c.getIssuer().isPlayer()) {
                if (HGUser.getUser(c.getIssuer().getPlayer()).getHGTeam() == null) {
                    throw new ConditionFailedException("You need to be in a team");
                }
            }
        });
        DojoCommandManager.get().getCommandConditions().addCondition("noteam", c -> {
            if (c.getIssuer().isPlayer()) {
                if (HGUser.getUser(c.getIssuer().getPlayer()).getHGTeam() != null) {
                    throw new ConditionFailedException("You are already in a team");
                }
            }
        });
        DojoCommandManager.get().getCommandConditions().addCondition("teamleader", c -> {
            if (c.getIssuer().isPlayer()) {
                if (!HGUser.getUser(c.getIssuer().getPlayer()).getHGTeam().getLeader().equals(c.getIssuer().getUniqueId())) {
                    throw new ConditionFailedException("You need to be the team leader");
                }
            }
        });

        MapUtil.pregenerateSpawnChunks();
        MapUtil.spawnBorder();

        KitRotation.scheduleRotation();
        currentRotation = KitRotation.getCurrentRotation();

    }


    @Override
    public DojoMap setupMap() {
        HGArena map = MapLoader.getRandomMap(MapType.HG);
        map.setWorld(Bukkit.getWorlds().get(0));
        map.shift(-map.getLocations()[0].getBlockX(), 0, -map.getLocations()[0].getBlockZ());
        map.setup(0);
        map.setOccupied();
        return map;
    }

    @Override
    public HGSession createSession() {
        return new HGSession((HGArena) PvPDojo.get().getStandardMap());
    }

    @Override
    public UserList<HGUser> createUserList() {
        return new UserList<>(HGUserImpl::new);
    }

    public Location getFeastLocation() {
        return feastLocation;
    }

    public void setFeastLocation(Location feastLocation) {
        this.feastLocation = feastLocation;
    }

    public KitRotationData getCurrentRotation() {
        return currentRotation;
    }

}
