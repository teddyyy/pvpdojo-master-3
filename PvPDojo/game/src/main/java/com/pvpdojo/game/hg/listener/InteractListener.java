/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.listener;

import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemUtil;

public class InteractListener implements DojoListener {

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_AIR) {
            if (e.getItem() != null && e.getItem().getType() == Material.WOOL && e.getItem().hasItemMeta() && e.getItem().getItemMeta().hasDisplayName()
                    && e.getItem().getItemMeta().getDisplayName().contains("Present")) {
                HGUser user = HGUser.getUser(e.getPlayer());
                user.sendMessage(CC.GREEN + "Your kit items and a tracker has been given to you");
                KitUtil.applyKitItems(user);
                ItemUtil.addItemWhenFree(user.getPlayer(), new ItemBuilder(Material.COMPASS).name(CC.RED + "Tracker").build());
                if (e.getItem().getAmount() > 1) {
                    e.getItem().setAmount(e.getItem().getAmount() - 1);
                } else {
                    e.getPlayer().setItemInHand(null);
                }
                e.setCancelled(true);
            }
        }

        // Fix Nether Biome beds
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getClickedBlock().getType() == Material.BED_BLOCK && e.getClickedBlock().getBiome() == Biome.HELL) {
            e.setCancelled(true);
        }
    }

}
