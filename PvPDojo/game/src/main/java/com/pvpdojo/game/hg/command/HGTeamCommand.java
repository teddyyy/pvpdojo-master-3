package com.pvpdojo.game.hg.command;

import static com.pvpdojo.util.StringUtils.createComponent;

import java.util.UUID;

import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.lang.HGMessageKeys;
import com.pvpdojo.game.hg.team.HGTeam;
import com.pvpdojo.game.hg.team.TeamList;
import com.pvpdojo.game.hg.userdata.HGUser;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.ActionHandlers;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.Request;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Conditions;
import co.aikar.commands.annotation.Flags;
import co.aikar.commands.annotation.HelpCommand;
import co.aikar.commands.annotation.Optional;
import co.aikar.commands.annotation.Subcommand;

@CommandAlias("team")
public class HGTeamCommand extends DojoCommand {

    @HelpCommand
    public void doHelp(User user, CommandHelp help) {
        help.showHelp();
    }

    @Subcommand("kick")
    @Conditions("teamsactivated|notstarted|hasteam|teamleader")
    public void onKick(User u, String partyMember) {  // TODO: add command completion
        HGUser user = HGUser.getUser(u.getPlayer());
        PvPDojo.schedule(() -> {
            HGTeam target = user.getHGTeam();
            OfflinePlayer player = PvPDojo.getOfflinePlayer(partyMember);
            if (target.getUsers().contains(player.getUniqueId())) {
                if (target.getLeader().equals(player.getUniqueId())) {
                    PvPDojo.schedule(() -> user.getPlayer().performCommand("team leave")).sync();
                } else {
                    PvPDojo.newChain().async(() -> {
                        target.channelMessage(CC.BLUE + player.getName() + CC.GRAY + " has been kicked out of the team by " + CC.BLUE + user.getName());
                    }).delay(5).asyncFirst(() -> { // Delay by 5 ticks to give the message a chance to pass to kicked user
                        TeamList.removeTeamUser(target, player.getUniqueId());
                        return player;
                    }).abortIfNot(OfflinePlayer::isOnline).syncLast(online -> { // Update user if online
                        HGUser targetUser = HGUser.getUser(online.getUniqueId());
                        targetUser.setHGTeam(null);
                    }).execute();
                }
            } else {
                user.sendMessage(HGMessageKeys.NOT_IN_YOUR_TEAM);
            }
        }).createAsyncTask();
    }

    @Conditions("teamsactivated|hasteam")
    @Subcommand("info|list|members")
    public void onInfo(User u) {
        HGUser user = HGUser.getUser(u.getPlayer());
        PvPDojo.schedule(() -> {
            HGTeam target = user.getHGTeam();

            if (target != null) {
                user.sendMessage(CC.GRAY + "Leader:");
                user.sendMessage(CC.GRAY + "- " + CC.BLUE + PvPDojo.getOfflinePlayer(target.getLeader()).getName());
                user.sendMessage(CC.GRAY + "Members:");
                target.getUsers().stream().map(PvPDojo::getOfflinePlayer).forEach(off -> user.sendMessage(CC.GRAY + "- " + CC.BLUE + off.getName()));
            }
        }).createAsyncTask();
    }

    @Subcommand("leave|quit")
    @Conditions("teamsactivated|notstarted|hasteam")
    public void onLeave(User u) {
        HGUser user = HGUser.getUser(u.getPlayer());
        HGTeam team = user.getHGTeam();
        team.sendMessage(CC.BLUE + user.getName() + CC.GRAY + " left the team");
        user.setHGTeam(null);
        user.sendMessage(HGMessageKeys.TEAM_LEAVE);

        PvPDojo.schedule(() -> TeamList.removeTeamUser(team, user.getUUID())).createAsyncTask();
    }

    @Subcommand("create")
    @Conditions("teamsactivated|notstarted|noteam")
    public void onCreate(User u, @Optional @Flags("other") Player instantInvite) {
        if (TeamList.getTeamCount() + 1 > HG.inst().getSettings().getTeamAmount()) {
            u.sendMessage(HGMessageKeys.TOO_MANY_TEAMS);
            return;
        }
        HGUser user = HGUser.getUser(u.getPlayer());
        TeamList.createTeam(user.getUniqueId());
        PvPDojo.newChain().sync(() -> {
            if (instantInvite != null) {
                user.getPlayer().performCommand("team invite " + instantInvite.getNick());
            }
        }).execute();
    }

    @Subcommand("invite")
    @Conditions("teamsactivated|notstarted")
    @CommandCompletion("@players")
    public void onInvite(User u, @Flags("other") Player target) {
        HGUser user = HGUser.getUser(u.getPlayer());
        if (user.getHGTeam() == null) {
            user.getPlayer().performCommand("team create " + target.getNick());
            return;
        }
        if (!user.getHGTeam().getLeader().equals(user.getUniqueId())) {
            user.sendMessage(HGMessageKeys.TEAM_LEADER_REQUIRED);
            return;
        }
        if (user.getHGTeam().getUsers().size() + 1 > HG.inst().getSettings().getTeamSize()) {
            user.sendMessage(HGMessageKeys.MAX_TEAM_SIZE_REACHED);
            return;
        }
        HGUser targetUser = HGUser.getUser(target);
        if (targetUser.getHGTeam() != null && targetUser.getHGTeam().isValid()) {
            user.sendMessage(HGMessageKeys.ALREADY_IN_TEAM_OTHER);
            return;
        }
        String failReason = targetUser.makeRequest(user.getUUID(), new TeamRequest(user.getHGTeam()));
        if (failReason == null) {
            user.sendMessage(HGMessageKeys.INVITE_TEAM, "{target}", targetUser.getName());
            targetUser.sendMessage(createComponent(PvPDojo.LANG.formatMessage(targetUser, HGMessageKeys.INVITED_TEAM, "{inviter}", user.getName()), CC.GREEN + "accept",
                    "accept " + user.getUUID().toString()));
        } else {
            user.sendMessage(MessageKeys.WARNING, "{text}", failReason);
        }
    }

    private class TeamRequest extends Request {

        UUID teamId;

        public TeamRequest(HGTeam team) {
            this.teamId = team.getPartyId();
        }

        @Override
        public void accept(User a) {
            HGUser acceptor = HGUser.getUser(a.getPlayer());
            PvPDojo.newChain().asyncFirst(() -> TeamList.getTeamById(teamId)).abortIfNull(ActionHandlers.MESSAGE, acceptor.getPlayer(), PvPDojo.WARNING + "Team not found")
                    .sync(team -> {
                        if (acceptor.getHGTeam() != null) {
                            acceptor.sendMessage(HGMessageKeys.ALREADY_IN_TEAM);
                            return null;
                        }
                        if (team.getUsers().size() + 1 > HG.inst().getSettings().getTeamSize()) {
                            acceptor.sendMessage(HGMessageKeys.MAX_TEAM_SIZE_REACHED);
                            return null;
                        }
                        return team;
                    }).abortIfNull().async(team -> {
                TeamList.addTeamUser(team, acceptor.getUniqueId());
                team.sendMessage(CC.BLUE + acceptor.getName() + CC.GRAY + " joined the team");
                return team;
            }).syncLast(acceptor::setHGTeam).execute();
        }

        @Override
        public void deny(User deniedBy) {}

        @Override
        public int getExpirationSeconds() {
            return 60;
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof TeamRequest;
        }

    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
