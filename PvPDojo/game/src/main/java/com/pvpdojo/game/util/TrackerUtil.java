/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.util;

import static com.pvpdojo.util.StreamUtils.negate;
import static com.pvpdojo.util.StreamUtils.or;

import java.util.Collection;
import java.util.stream.Stream;

import com.pvpdojo.game.session.StagedSession;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.entity.TeamArenaEntity;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.pvpdojo.game.Game;
import com.pvpdojo.game.userdata.GameUser;
import com.pvpdojo.userdata.User;

public class TrackerUtil {

    public static Stream<Player> getNearbyIngame(Player player, double rangeX, double rangeY, double rangeZ) {
        return getNearbyIngame(player.getNearbyEntities(rangeX, rangeY, rangeZ));

    }

    public static Stream<Player> getNearbyIngame(Collection<Entity> nearby) {
        return nearby.stream()
                     .filter(entity -> entity instanceof Player)
                     .map(entity -> GameUser.getUser((Player) entity))
                     .filter(GameUser::isIngame)
                     .filter(negate(Game.inst().getGame().getSession().getCurrentlyDead()::contains))
                     .filter(negate(or(User::isVanish, User::isAdminMode)))
                     .map(User::getPlayer);
    }

    public static Stream<Player> getIngame() {
        return Bukkit.getOnlinePlayers().stream()
                     .map(GameUser::getUser)
                     .filter(GameUser::isIngame)
                     .filter(negate(Game.inst().getGame().getSession().getCurrentlyDead()::contains))
                     .filter(negate(or(User::isVanish, User::isAdminMode)))
                     .map(User::getPlayer);
    }

    public static Stream<ArenaEntity> getOpponents(ArenaEntity entity) {
        StagedSession session = Game.inst().getGame().getSession();

        if (session.isTeamFight()) {
            entity = entity instanceof TeamArenaEntity ? (TeamArenaEntity) entity : entity.getTeam();
        }

        return entity != null ? session.getParticipants().stream().filter(negate(entity::equals)) : Stream.of();
    }

    public static Stream<Player> getPlayerOpponents(ArenaEntity entity) {
        return getOpponents(entity).flatMap(opponent -> opponent.getPlayers().stream());
    }

}
