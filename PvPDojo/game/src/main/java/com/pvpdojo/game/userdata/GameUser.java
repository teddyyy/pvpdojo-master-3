/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.userdata;

import org.bukkit.entity.Player;

import com.pvpdojo.game.Game;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Log;

public interface GameUser extends User, Runnable {

    static GameUser getUser(Player player) {
        return Game.inst().getUser(player);
    }

    default boolean isIngame() {
        return getSession() != null && !isSpectator() && !isAdminMode();
    }

    default void runSafely() {
        try {
            run();
        } catch (Throwable throwable) {
            Log.exception(throwable);
        }
    }

}
