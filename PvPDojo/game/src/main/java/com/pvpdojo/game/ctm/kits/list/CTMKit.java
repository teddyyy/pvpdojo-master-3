/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.kits.list;

import java.util.function.Supplier;

import co.aikar.locales.MessageKey;
import co.aikar.locales.MessageKeyProvider;
import com.pvpdojo.game.ctm.kits.*;
import com.pvpdojo.userdata.User;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionType;

import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.kits.KitProvider;
import com.pvpdojo.game.kits.defaults.Archer;
import com.pvpdojo.game.kits.defaults.Berserker;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.PotionBuilder;
import com.pvpdojo.util.StringUtils;

public enum CTMKit implements KitProvider {

    ACHILLES(Achilles::new, Material.WOOD_SWORD, "While in Achilles mode: Every hit is cancelled, but other damage sources result in an instant death"),
    ARCHER(Archer::new, Material.BOW, "Start with a bow and 7 arrows. Shooting a player gives you an arrow back."),
    BERSERKER(Berserker::new, new PotionBuilder(PotionType.STRENGTH).level(1).build(), "Killing creatures activates your bloodlust"),
    BLINDWAVE(Blindwave::new, Material.GOLDEN_CARROT, ""),
    BOOSTER(Booster::new, Material.FIREWORK, "You do not receive the damage you dealt, but your team mates do instead"),
    DRAINMAN(Drainman::new, Material.ARROW, "Reset your opponents' damage by hitting them with your special arrows"),
    NUKER(Nuker::new, Material.TNT, "Save yourself from dangerous abysses by launching into the air and nuke your opponents."),
    SHOCKWAVE(Shockwave::new, Material.CAULDRON_ITEM, "Slow down your opponents by using your damage"),
    SPEEDWAVE(Speedwave::new, Material.FEATHER, "Speed up your team by using your damage"),
    SPY(Spy::new, new PotionBuilder(PotionType.INVISIBILITY).build(), "Hide yourself from other players as long as they do not fight you."),
    SWITCHER(Switcher::new, Material.SNOW_BALL, "Switch places with other players and mobs by throwing a snowball at them");

    private Supplier<Kit> supplier;
    private ItemStack icon;
    private MessageKeyProvider description = () -> MessageKey.of("pvpdojo-ctm-kits.description_" + name().toLowerCase());

    CTMKit(Supplier<Kit> supplier, Material material, String description) {
        this(supplier, new ItemStack(material), description);
    }

    CTMKit(Supplier<Kit> supplier, ItemStack item, String description) {
        this.supplier = supplier;
        this.icon = new ItemBuilder(item).name(CC.GREEN + StringUtils.getEnumName(this)).lore("", CC.GRAY + description).build();
    }

    @Override
    public ItemStack getIcon() {
        return icon;
    }

    @Override
    public MessageKeyProvider getDescription() {
        return description;
    }

    @Override
    public Supplier<Kit> getSupplier() {
        return supplier;
    }

}
