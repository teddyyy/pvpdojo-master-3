/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.naturalevents;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.CC;
import org.bukkit.Bukkit;
import org.bukkit.block.Biome;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.EnumSet;
import java.util.Set;

public class AcidRain extends BiomeExcludingEvent {

    @Override
    public void start() {
        PvPDojo.get().getServerSettings().setRain(true);
        Bukkit.getWorlds().get(0).setStorm(true);
    }

    @Override
    public void tick() {
        TrackerUtil.getIngame().forEach(player -> {
            Biome biome = player.getLocation().getBlock().getBiome();
            if (!getExcludedBiomes().contains(biome)) {
                if (player.getEyeLocation().getY() >= BlockUtil.getHighestBlock(player.getEyeLocation()).getY()) {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 2 * 20, 0), true);

                    for (ItemStack armor : player.getInventory().getArmorContents()) {

                        if (armor == null) {
                            continue;
                        }

                        if (PvPDojo.RANDOM.nextBoolean()) {
                            armor.setDurability((short) (armor.getDurability() + 2));
                        }

                    }

                }
            }
        });
    }

    @Override
    public void end() {
        PvPDojo.get().getServerSettings().setRain(false);
        Bukkit.getWorlds().get(0).setStorm(false);
    }

    @Override
    public int getDuration() {
        return 70;
    }

    @Override
    public LocaleMessage getMessage() {
        return LocaleMessage.of("The HG Gods are not satisfied with this game, better get yourself an umbrella! " + CC.GOLD + "[Acid Rain]");
    }

    @Override
    Set<Biome> getExcludedBiomes() {
        return EnumSet.of(Biome.DESERT, Biome.DESERT_HILLS, Biome.DESERT_MOUNTAINS);
    }
}
