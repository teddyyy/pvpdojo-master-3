/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits.sidekits;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_7_R4.entity.disguise.CraftCreatureDisguise;
import org.bukkit.entity.Animals;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.disguise.Disguise;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.EntityTargetEvent.TargetReason;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;

public class Chameleon extends Kit {

    private EntityType entityType;

    @Override
    public void onEntityAttack(EntityDamageByEntityEvent e) {
        Player attacker = (Player) e.getDamager();

        if (!e.isCancelled()) {
            if (attacker.getItemInHand() != null && attacker.getItemInHand().getType() == getStartItems()[0].getType()) {
                if (e.getEntity() instanceof Animals || e.getEntity() instanceof Monster) {
                    entityType = e.getEntityType();
                    attacker.setDisguise(null);
                    disguise(attacker);
                } else {
                    attacker.sendMessage(CC.RED + "You can't disguise as " + e.getEntity().getType());
                }
            }
        }
    }

    @Override
    public void onItemSwitch(PlayerItemHeldEvent e) {
        if (!e.isCancelled()) {
            ItemStack newItem = e.getPlayer().getInventory().getContents()[e.getNewSlot()];
            if (newItem != null && newItem.getType() == getStartItems()[0].getType()) {
                disguise(e.getPlayer());
            } else {
                reset(e.getPlayer());
            }
        }
    }

    @Override
    public void onInteractEntity(PlayerInteractEntityEvent e) {
        if (e.getPlayer().getItemInHand() != null && e.getPlayer().getItemInHand().getType() == getStartItems()[0].getType()) {
            e.setCancelled(true);
        }
    }

    @Override
    public void onPlayerDamagedByPlayer(EntityDamageByEntityEvent e) {
        if (!e.isCancelled()) {
            reset((Player) e.getEntity());
        }
    }

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {
        if (!e.isCancelled()) {
            reset((Player) e.getDamager());
        }
    }

    @Override
    public void onMobTarget(EntityTargetEvent e) {
        if (e.getEntity().getDisguise() != null && e.getReason() != TargetReason.FORGOT_TARGET) {
            e.setCancelled(true);
        }
    }

    public void disguise(Player player) {
        if (player.getDisguise() == null && entityType != null) {
            Disguise disguise = new CraftCreatureDisguise(player, entityType);
            player.setDisguise(disguise);
            player.sendMessage(CC.GREEN + "Other players now see you as a " + StringUtils.getEnumName(entityType));
        }
    }

    public void reset(Player player) {
        if (player.getDisguise() != null) {
            player.setDisguise(null);
            player.sendMessage(CC.RED + "You look normal to other players again.");
        }
    }

    @Override
    public void onLeave(Player player) {
        reset(player);
    }

    public ItemStack[] getStartItems() {
        return new ItemStack[]{new ItemStack(Material.NAME_TAG)};
    }
}
