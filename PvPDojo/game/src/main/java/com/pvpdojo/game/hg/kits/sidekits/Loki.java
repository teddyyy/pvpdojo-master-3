/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits.sidekits;

import java.util.Arrays;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.event.player.PlayerTrackEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.comphenix.protocol.PacketType.Play.Server;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListeningWhitelist;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.events.PacketListener;
import com.comphenix.protocol.injector.GamePhase;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.util.RandomInventory;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemUtil;
import com.pvpdojo.util.packetwrapper.WrapperPlayServerEntityEquipment;

import net.minecraft.server.v1_7_R4.Packet;
import net.minecraft.server.v1_7_R4.PacketPlayOutEntityEquipment;
import net.minecraft.server.v1_7_R4.WorldServer;

public class Loki extends ComplexKit implements PacketListener {

    private static final RandomInventory<ItemStack>
            HELMET_INV = RandomInventory.randomItemStacks()
                                        .addItem(new ItemStack(Material.GOLD_HELMET), 25)
                                        .addItem(new ItemStack(Material.IRON_HELMET), 75),
            CHESTPLATE_INV = RandomInventory.randomItemStacks()
                                            .addItem(new ItemStack(Material.GOLD_CHESTPLATE), 10)
                                            .addItem(new ItemStack(Material.IRON_CHESTPLATE), 90),
            LEGGINS_INV = RandomInventory.randomItemStacks()
                                         .addItem(new ItemStack(Material.GOLD_LEGGINGS), 10)
                                         .addItem(new ItemStack(Material.IRON_LEGGINGS), 90),
            BOOTS_INV = RandomInventory.randomItemStacks()
                                       .addItem(new ItemStack(Material.GOLD_BOOTS), 25)
                                       .addItem(new ItemStack(Material.IRON_BOOTS), 75);

    private static final Map<UUID, ItemStack[]> LOKI = new ConcurrentHashMap<>();

    @Override
    public void onInteract(PlayerInteractEvent e) {
        if ((e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_AIR)
                && e.getPlayer().isSneaking() && e.getItem() != null && ItemUtil.SWORDS.contains(e.getItem().getType())) {
            toggleLoki(e.getPlayer());
        }
    }

    @Override
    public void onToggleSneak(PlayerToggleSneakEvent e) {
        if (e.isSneaking() && e.getPlayer().isBlocking()) {
            toggleLoki(e.getPlayer());
        }
    }

    @Override
    public void onLeave(Player player) {
        if (LOKI.containsKey(player.getUniqueId())) {
            toggleLoki(player);
        }
    }

    public void toggleLoki(Player player) {
        if (LOKI.containsKey(player.getUniqueId())) {
            LOKI.remove(player.getUniqueId());
            player.sendMessage(CC.RED + "Deactivated Loki effect, other players now see your regular equipment");
            sendPackets(player, null);
        } else {
            LOKI.put(player.getUniqueId(), generateLokiContent(player));
            player.sendMessage(CC.GREEN + "Activated Loki effect, other players now see an illusion of your equipment");
            sendPackets(player, null);
        }
    }

    public void sendPackets(Player player, Player receiver) {
        for (int i = 0; i < 5; ++i) {
            net.minecraft.server.v1_7_R4.ItemStack itemstack = NMSUtils.get(player).getEquipment(i);
            int id = NMSUtils.get(player).getId();
            Packet packet = new PacketPlayOutEntityEquipment(id, i, itemstack);
            if (receiver != null) {
                NMSUtils.get(receiver).playerConnection.sendPacket(packet);
            } else {
                ((WorldServer) NMSUtils.get(player).world).getTracker().a(NMSUtils.get(player), packet);
            }
        }
    }

    public static ItemStack[] generateLokiContent(Player player) {
        ItemStack sword = new ItemStack(HG.inst().getSession().getCurrentHandler().getTimeSeconds() > HG.inst().getSettings().getSecondsTillFeast() ? Material.STONE_SWORD : Material.WOOD_SWORD);

        if (HG.inst().getSession().getCurrentHandler().getTimeSeconds() >= 300) {
            if (Arrays.stream(player.getInventory().getArmorContents()).allMatch(item -> item.getType() == Material.AIR)) {
                return new ItemStack[] {
                        sword,
                        PvPDojo.RANDOM.nextBoolean() ? null : BOOTS_INV.generateRandomItem(),
                        PvPDojo.RANDOM.nextInt(5) == 0 ? null : LEGGINS_INV.generateRandomItem(),
                        CHESTPLATE_INV.generateRandomItem(),
                        PvPDojo.RANDOM.nextBoolean() ? null : HELMET_INV.generateRandomItem()
                };
            }
            return new ItemStack[] {
                    sword,
                    PvPDojo.RANDOM.nextBoolean() ? null : BOOTS_INV.generateRandomItem(),
                    null,
                    null,
                    PvPDojo.RANDOM.nextBoolean() ? null : HELMET_INV.generateRandomItem()
            };
        }
        return new ItemStack[] { sword, null, null, null, null };
    }

    @EventHandler
    public void onTrack(PlayerTrackEntityEvent e) {
        if (LOKI.containsKey(e.getTracked().getUniqueId())) {
            PvPDojo.schedule(() -> sendPackets((Player) e.getTracked(), e.getPlayer())).nextTick();
        }
    }

    @Override
    public void onPacketSending(PacketEvent packetEvent) {
        WrapperPlayServerEntityEquipment wrapper = new WrapperPlayServerEntityEquipment(packetEvent.getPacket());
        Entity entity = wrapper.getEntity(packetEvent);
        ItemStack[] fakeContent = LOKI.get(entity.getUniqueId());

        if (fakeContent != null) {
            Player player = (Player) entity;

            if (wrapper.getSlot() == 0) {
                if (ItemUtil.SWORDS.contains(player.getItemInHand().getType())) {
                    wrapper.setItem(fakeContent[0]);
                }
            } else {
                wrapper.setItem(fakeContent[wrapper.getSlot()]);
            }
        }

    }

    @Override
    public void onPacketReceiving(PacketEvent packetEvent) {}

    @Override
    public ListeningWhitelist getSendingWhitelist() {
        return ListeningWhitelist.newBuilder().gamePhase(GamePhase.PLAYING).types(Server.ENTITY_EQUIPMENT).build();
    }

    @Override
    public ListeningWhitelist getReceivingWhitelist() {
        return ListeningWhitelist.EMPTY_WHITELIST;
    }

    @Override
    public void register() {
        ProtocolLibrary.getProtocolManager().addPacketListener(this);
        register(Game.inst());
    }

    @Override
    public void unregister() {
        ProtocolLibrary.getProtocolManager().removePacketListener(this);
        super.unregister();
    }

    @Override
    public Plugin getPlugin() {
        return Game.inst();
    }

}
