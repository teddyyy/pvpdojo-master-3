/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.session;

import java.util.List;

import com.pvpdojo.game.kits.KitHolder;
import com.pvpdojo.game.kits.KitProvider;
import com.pvpdojo.game.userdata.kit.KitGameUser;
import com.pvpdojo.session.RelogData;
import com.pvpdojo.userdata.User;

@SuppressWarnings("unchecked")
public class KitGameRelogData extends RelogData {

    private List<KitHolder<KitProvider>> kitHolders;

    public KitGameRelogData(KitGameUser user) {
        super(user);
        kitHolders = user.getKitHolders();
    }

    @Override
    public void apply(User user) {
        super.apply(user);
        ((KitGameUser<KitProvider>) user).getKitHolders().addAll(kitHolders);
    }

    public List<KitHolder<KitProvider>> getKitHolders() {
        return kitHolders;
    }
}
