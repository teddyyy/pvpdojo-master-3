/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.util.Vector;

import com.pvpdojo.game.hg.kits.event.PlayerGladiatorEvent;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.userdata.GameUser;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.TimeUtil;

public class Ninja extends ComplexKit {

    private Player lastDamaged;
    private long lastHit;

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {
        if (!e.isCancelled()) {
            lastDamaged = (Player) e.getEntity();
            lastHit = System.currentTimeMillis();
        }
    }

    @Override
    public void onToggleSneak(PlayerToggleSneakEvent e) {
        if (e.isSneaking() && !TimeUtil.hasPassed(lastHit, TimeUnit.SECONDS, 15) && lastDamaged != null && lastDamaged.isOnline() && GameUser.getUser(lastDamaged).isIngame()) {
            if (!kitCooldown.isOnCooldown()) {
                kitCooldown.use();

                Vector vec = lastDamaged.getLocation().getDirection().multiply(-1);
                Location loc = lastDamaged.getLocation().add(vec);

                if (!loc.getBlock().getType().equals(Material.AIR)) {
                    loc = lastDamaged.getLocation();
                }

                loc.setPitch(e.getPlayer().getLocation().getPitch());
                loc.setYaw(lastDamaged.getLocation().getYaw());

                e.getPlayer().teleport(loc);
            } else {
                kitCooldown.sendCooldown(User.getUser(e.getPlayer()));
            }
        }
    }

    @Override
    public void onDeath(PlayerDeathEvent e) {
        lastDamaged = null;
    }

    @Override
    public void onKill(PlayerDeathEvent e) {
        lastDamaged = null;
    }

    @EventHandler
    public void onGladiator(PlayerGladiatorEvent e) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            HGUser user = HGUser.getUser(player);
            if (user.getKit() instanceof Ninja) {
                if (((Ninja) user.getKit()).lastDamaged == e.getPlayer() || ((Ninja) user.getKit()).lastDamaged == e.getSecond()) {
                    ((Ninja) user.getKit()).lastDamaged = null;
                }
            }
        }
    }

    @Override
    public int getCooldown() {
        return 13_370;
    }
}
