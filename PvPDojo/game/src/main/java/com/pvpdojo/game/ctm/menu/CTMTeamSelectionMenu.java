/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.menu;

import java.util.function.Consumer;
import java.util.function.Supplier;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.pvpdojo.game.ctm.CTM;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.entity.TeamArenaEntity;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.LeatherArmorBuilder;

import lombok.Getter;

public class CTMTeamSelectionMenu extends SingleInventoryMenu {

    private final Consumer<TeamSelection> consumer;

    public CTMTeamSelectionMenu(Player player, Consumer<TeamSelection> consumer) {
        super(player, "Select your Team", 9);
        this.consumer = consumer;
        setUpdateInterval(20);
        setCloseOnClick(true);
    }

    @Override
    public void redraw() {
        super.redraw();

        InventoryPage inv = getCurrentPage();

        int maxPlayers = CTM.inst().getSession().getArena().getMaxPlayers();
        long teamRed = CTM.inst().getSession().getTeamRed().getMembers().stream().filter(ArenaEntity::isOnline).count();
        long teamBlue = CTM.inst().getSession().getTeamBlue().getMembers().stream().filter(ArenaEntity::isOnline).count();

        inv.setItem(0, new ItemBuilder(Material.CHAINMAIL_HELMET)
                        .name(CC.DARK_PURPLE + "Auto Assign")
                        .lore("", "" + CC.GREEN + (teamRed + teamBlue) + CC.GOLD + " / " + CC.RED + maxPlayers).build(),
                type -> process(TeamSelection.AUTO));

        inv.setItem(1, new LeatherArmorBuilder(Material.LEATHER_HELMET, Color.RED)
                        .name(CC.RED + "Team Red")
                        .lore("", "" + CC.GREEN + teamRed + CC.GOLD + " / " + CC.RED + maxPlayers / 2).build(),
                type -> process(TeamSelection.RED));

        inv.setItem(2, new LeatherArmorBuilder(Material.LEATHER_HELMET, Color.BLUE)
                        .name(CC.BLUE + "Team Blue")
                        .lore("", "" + CC.GREEN + teamBlue + CC.GOLD + " / " + CC.RED + maxPlayers / 2).build(),
                type -> process(TeamSelection.BLUE));
    }

    private void process(TeamSelection selection) {
        int maxPlayers = CTM.inst().getSession().getArena().getMaxPlayers();
        long teamRed = CTM.inst().getSession().getTeamRed().getMembers().stream().filter(ArenaEntity::isOnline).count();
        long teamBlue = CTM.inst().getSession().getTeamBlue().getMembers().stream().filter(ArenaEntity::isOnline).count();

        if (selection != TeamSelection.AUTO && !User.getUser(getPlayer()).getRank().inheritsRank(Rank.GREENBELT)) {
            getPlayer().sendMessage(CC.RED + "You need " + CC.DARK_GREEN + "Greenbelt " + CC.RED + "or higher to choose your team. You may only use auto assign.");
            return;
        }

        if ((selection == TeamSelection.RED && teamRed >= maxPlayers / 2) || (selection == TeamSelection.BLUE && teamBlue >= maxPlayers / 2) || (teamBlue + teamRed) >= maxPlayers) {
            getPlayer().sendMessage(CC.RED + "Team is already filled");
            return;
        }

        if ((selection == TeamSelection.RED && teamRed > teamBlue) || (selection == TeamSelection.BLUE && teamBlue > teamRed)) {
            if (CTM.inst().getSession().getState() == SessionState.STARTED) {
                getPlayer().sendMessage(CC.RED + "You cannot outnumber the other team after the game has started");
                return;
            } else {
                CTM.inst().getSession().broadcast(LocaleMessage.of(CC.DARK_RED + "If " + selection + CC.DARK_RED + " remains stacked, teams will be autobalanced on start"));
            }
        }

        consumer.accept(selection);
    }

    public enum TeamSelection {
        AUTO(null, CTM.inst().getSession()::getAutoAssign), RED(CC.RED, CTM.inst().getSession()::getTeamRed), BLUE(CC.BLUE, CTM.inst().getSession()::getTeamBlue);

        @Getter
        private final CC color;
        @Getter
        private final Supplier<TeamArenaEntity> team;

        TeamSelection(CC color, Supplier<TeamArenaEntity> team) {
            this.color = color;
            this.team = team;
        }

        @Override
        public String toString() {
            return color + StringUtils.getEnumName(this);
        }
    }

}
