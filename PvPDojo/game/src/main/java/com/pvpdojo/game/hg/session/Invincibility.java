/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.session;

import java.sql.SQLException;
import java.util.Comparator;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.team.HGTeam;
import com.pvpdojo.game.hg.team.TeamList;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;

import com.pvpdojo.bukkit.events.DataLoadedEvent;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.lang.GameMessageKeys;
import com.pvpdojo.game.session.StageHandler;
import com.pvpdojo.game.session.StagedSession;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.settings.UserSettings.ScoreboardMode;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.userdata.sidebar.Sidebar;
import com.pvpdojo.util.Countdown;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.CC;

public class Invincibility extends StageHandler {

    public Invincibility(StagedSession session) {
        super(session);
    }

    @Override
    public StageHandler init() {
        session.setState(SessionState.INVINCIBILITY);
        setTimeSeconds(HG.inst().getSettings().getSecondsTillPvP());
        return super.init();
    }

    @Override
    public boolean isCountdown() {
        return true;
    }

    @Override
    public void run() {
        super.run();

        if (Countdown.isUserFriendlyNumber(getTimeSeconds())) {
            session.broadcast(GameMessageKeys.COUNTDOWN_INVINCIBILITY, "{time}", Countdown.getUserFriendlyNumber(getTimeSeconds()));
        }

        if (getTimeSeconds() == 60) {
            Bukkit.setSaveBandwidth(false);
        }

        if(timeSeconds <= 5 && timeSeconds != 0) {
        	session.executeForPlayers(players -> players.playSound(players.getLocation(), Sound.NOTE_BASS, 1, 1));
        }
        
        if(timeSeconds == 0) {
        	session.executeForPlayers(players -> players.playSound(players.getLocation(), Sound.NOTE_PLING, 1, 1));
        }
        
        session.executeForPlayers(player -> {
            User user = User.getUser(player);
            if (user.isDataLoaded() && user.getSettings().getHGSettings().getScoreboardMode() == ScoreboardMode.SMALL) {
                Sidebar sidebar = user.getSidebar();
                sidebar.setTitle(CC.RED + "Invincible for " + StringUtils.getReadableSeconds(getTimeSeconds()));
                sidebar.set(session.getParticipants().size(), "Player Count");
            }
        });

    }

    @Override
    public void advance() {
        super.advance();
        Bukkit.setSaveBandwidth(false);

        boolean teamsActivated = HG.inst().getSettings().getTeamSize() > 1;
        session.executeForPlayers(player -> {
            User user = User.getUser(player);
            if (!user.getSettings().isScoreboard()) {
                user.getSidebar().removeAll();
            }
        });

        if (teamsActivated) {
            int maxTeams = HG.inst().getSettings().getTeamSize();
            PvPDojo.schedule(() -> session.executeForPlayers(player -> {
                HGUser user = HGUser.getUser(player);
                if (user.getHGTeam() == null) {
                    if (TeamList.getTeamCount() < maxTeams) {
                        TeamList.createTeam(user.getUniqueId());
                    } else {
                        HGTeam team = TeamList.getTeams().stream().min(Comparator.comparingInt(HGTeam::size)).orElse(null);
                        if (team != null) {
                            TeamList.addTeamUser(team, user.getUUID());
                            team.sendMessage(CC.BLUE + user.getName() + CC.GRAY + " joined the team");
                        }
                    }
                }
            })).createAsyncTask();
        }
    }

    @Override
    public StageHandler getNextHandler() {
        return new EHG(session);
    }

    @EventHandler
    public void onMobTarget(EntityTargetEvent e) {
        e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onLogin(AsyncPlayerPreLoginEvent e) {

        if (e.getLoginResult() == Result.KICK_OTHER) {
            return;
        }

        PersistentData db = new PersistentData(e.getUniqueId());
        try {
            db.pullUserData();
        } catch (SQLException ex) {
            Log.exception(ex);
            e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, CC.RED + "Internal error");
            return;
        }

        if (db.getRank().inheritsRank(Rank.GREENBELT)) {
            e.allow();
        }
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (e.getCause() == DamageCause.CUSTOM) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onJoin(DataLoadedEvent e) {
        HGUser user = HGUser.getUser(e.getPlayer());
        if (user.isSpectator() && user.canRespawn()) {
            user.setSpectator(false);
            Game.inst().getGame().getSession().addParticipant(user);

            Location spawnLocation = user.respawn();
            if (getTimeSeconds() >= 90) {
                spawnLocation = BlockUtil.getHighestBlock(spawnLocation.getWorld(), 0, 0).getLocation().add(0.5, 10, 0.5);
            }
            user.teleport(spawnLocation);

            user.setRespawned(false);
        }
    }

}
