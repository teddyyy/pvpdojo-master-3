/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.session;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;

import com.pvpdojo.bukkit.events.DojoPlayerJoinEvent;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.ctm.CTM;
import com.pvpdojo.game.ctm.userdata.CTMUser;
import com.pvpdojo.game.lang.GameMessageKeys;
import com.pvpdojo.game.session.StageHandler;
import com.pvpdojo.game.userdata.GameUser;
import com.pvpdojo.game.util.TeamUtil;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.util.Countdown;

public class PreGame extends StageHandler<CTMSession> {

    public PreGame(CTMSession session) {
        super(session);
    }

    @Override
    public StageHandler init() {
        session.setState(SessionState.PREGAME);
        setTimeSeconds(60);
        return super.init();
    }

    @Override
    public void run() {
        int minPlayers = CTM.inst().getSettings().getMinPlayers();
        long playerCount = Bukkit.getOnlinePlayers().stream().map(GameUser::getUser).filter(user -> user.getTeam() != null).count();

        if (Game.inst().isPrivate() && getTimeSeconds() == 5 && playerCount < minPlayers) {
            BukkitUtil.shutdown();
        }

        super.run();

        if (shouldCountDown()) {
            if (Countdown.isUserFriendlyNumber(getTimeSeconds())) {
                session.broadcast(GameMessageKeys.COUNTDOWN_START, "{time}", Countdown.getUserFriendlyNumber(getTimeSeconds()));
            }
        } else {
            setTimeSeconds(60);
        }
    }

    @Override
    public boolean shouldCountDown() {
        int minPlayers = CTM.inst().getSettings().getMinPlayers();
        long playerCount = Bukkit.getOnlinePlayers().stream().map(GameUser::getUser).filter(user -> user.getTeam() != null).count();

        return Game.inst().isPrivate() || playerCount >= minPlayers;
    }

    @Override
    public boolean isCountdown() {
        return true;
    }

    @Override
    public StageHandler getNextHandler() {
        return new CTMStage(session);
    }

    @Override
    public void advance() {
        if (session.getTeamBlue().getMembers().size() > session.getTeamRed().getMembers().size() + 1) {
            TeamUtil.balanceTeams(session.getTeamBlue(), session.getTeamRed());
        } else if (session.getTeamRed().getMembers().size() > session.getTeamBlue().getMembers().size() + 1) {
            TeamUtil.balanceTeams(session.getTeamRed(), session.getTeamBlue());
        }
        super.advance();
    }

    @EventHandler
    public void onJoin(DojoPlayerJoinEvent e) {
        CTMUser user = CTMUser.getUser(e.getPlayer());

        user.updateScoreBoard();
        user.setSpectator(true);
        user.teleport(CTM.inst().getSession().getArena().getSpectatorSpawn());

        CTMSession session = CTM.inst().getSession();
        session.addParticipant(user);

    }

}
