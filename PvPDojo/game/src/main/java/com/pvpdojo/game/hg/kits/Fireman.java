/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import java.util.concurrent.TimeUnit;

import org.bukkit.Material;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.ExpireArrayList;

public class Fireman extends Kit {

    private ExpireArrayList<Long> lavaStamps = new ExpireArrayList<>();

    @Override
    public void onDamage(EntityDamageEvent e) {
        if (e.getCause() == DamageCause.FIRE || e.getCause() == DamageCause.FIRE_TICK || e.getCause() == DamageCause.LIGHTNING) {
            e.setCancelled(true);
        }

        if (!e.isCancelled() && e.getCause() == DamageCause.LAVA) {
            lavaStamps.add(System.currentTimeMillis(), 25, TimeUnit.SECONDS);
            if (lavaStamps.size() <= 15 * 20) {
                e.setCancelled(true);
            }
        }

    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] { new ItemStack(Material.WATER_BUCKET) };
    }
}
