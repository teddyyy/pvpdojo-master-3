/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.util.TrackerUtil;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.BlockIterator;

import com.pvpdojo.game.hg.lang.HGKitMessageKeys;
import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class Flash extends Kit implements InteractingKit {

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] { new ItemBuilder(Material.REDSTONE_TORCH_OFF).build() };
    }

    @Override
    public void click(Player player) {
        BlockIterator iterator = new BlockIterator(player, 100);

        Block lastBlock = null;
        while (iterator.hasNext()) {
            Block block = iterator.next();
            if (block.getType().isSolid()) {
                lastBlock = block;
                break;
            }
        }

        if (lastBlock == null) {
            User.getUser(player).sendMessage(HGKitMessageKeys.FLASH_NO_BLOCK);
            return;
        }

        Block highestBlock = BlockUtil.getHighestBlock(lastBlock.getWorld(), lastBlock.getLocation().getBlockX(), lastBlock.getLocation().getBlockZ());
        Location location = highestBlock.getLocation().add(0, 2, 0);
        location.setYaw(player.getLocation().getYaw());
        location.setPitch(player.getLocation().getPitch());
        player.teleport(location);
        player.getWorld().playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 1, 1);
        player.getWorld().spigot().strikeLightningEffect(player.getLocation(), false);
        player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 3 * 20, 2));

        TrackerUtil.getNearbyIngame(player, 3, 3, 3)
                .filter(players -> !(HGUser.getUser(players.getUniqueId()).getKit() == null &&
                        HGUser.getUser(players.getUniqueId()).getKit() instanceof Fireman || HGUser.getUser(players.getUniqueId()).getKit() instanceof Thor))
                .forEach(players -> NMSUtils.damageEntity(players, player, EntityDamageEvent.DamageCause.LIGHTNING, 7.0));

        kitCooldown.use();
    }

    @Override
    public void activate(PlayerInteractEvent player) {}

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.REDSTONE_TORCH_ON);
    }

    @Override
    public int getCooldown() {
        return 45000;
    }
}
