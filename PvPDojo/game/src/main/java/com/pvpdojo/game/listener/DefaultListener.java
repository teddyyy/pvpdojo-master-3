/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.listener;

import java.sql.SQLException;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.server.ServerListPingEvent;

import com.pvpdojo.DBCommon;
import com.pvpdojo.bukkit.events.AdminModeEvent;
import com.pvpdojo.bukkit.events.DataLoadedEvent;
import com.pvpdojo.bukkit.events.DojoPlayerJoinEvent;
import com.pvpdojo.bukkit.events.DojoServerRestartEvent;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.session.StagedSession;
import com.pvpdojo.game.userdata.GameUser;
import com.pvpdojo.session.RelogSession;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.bukkit.CC;

public class DefaultListener implements DojoListener {

    @EventHandler
    public void onLogin(AsyncPlayerPreLoginEvent e) {
        if (Game.inst().getGame() == null || Game.inst().getGame().getSession() == null) {
            e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, "Server not prepared");
        } else if (Game.inst().getGame().getSession().getState() != SessionState.PREGAME) {
            PersistentData db = new PersistentData(e.getUniqueId());

            try {
                db.pullUserData();
            } catch (SQLException ex) {
                Log.exception(ex);
                e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, CC.RED + "Internal error");
                return;
            }

            if (!db.getRank().inheritsRank(Game.inst().getGame().getSettings().getRankToSpectate()) && !RelogSession.RELOG_MAP.containsKey(e.getUniqueId())) {
                e.disallow(Result.KICK_FULL, CC.RED + "The game already started");
            }
        }
    }

    @EventHandler
    public void onDataLoaded(DataLoadedEvent e) {
        if (Game.inst().getGame().getSession().getState() != SessionState.PREGAME && !GameUser.getUser(e.getPlayer()).isIngame()) {
            if (!e.getData().getRank().inheritsRank(Game.inst().getGame().getSettings().getRankToSpectate())) {
                User.getUser(e.getPlayer()).kick(CC.RED + "The game already started");
            }
        }
    }

    @EventHandler
    public void onAdminMode(AdminModeEvent e) {
        User user = User.getUser(e.getPlayer());
        if (e.isEnable()) {
            if (!Game.inst().getGame().getSession().getCurrentlyDead().contains(user) && !Game.inst().getGame().getSession().isTeamFight()) {
                Game.inst().getGame().getSession().getParticipants().remove(user);
                user.setSession(null);
            }
        } else {
            if (!Game.inst().getGame().getSession().getCurrentlyDead().contains(user) && !Game.inst().getGame().getSession().isTeamFight()) {
                Game.inst().getGame().getSession().getParticipants().add(user);
                user.setSession(Game.inst().getGame().getSession());
            } else if (user.getTeam() == null) {
                user.setSpectator(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(DojoPlayerJoinEvent e) {
        if (Game.inst().getGame().getSession().getState() == SessionState.PREGAME
                && (Game.inst().getParticipants().isEmpty() || Game.inst().getParticipants().contains(e.getPlayer().getUniqueId()))) {
            Game.inst().getGame().getSession().addParticipant(User.getUser(e.getPlayer()));
        } else {
            User user = User.getUser(e.getPlayer());
            if (user.getSession() == null) {
                Game.inst().getGame().getSession().getCurrentlyDead().add(user);
                user.setSpectator(true);
                e.getPlayer().teleport(Game.inst().getGame().getSession().getArena().getLocations()[0]);
                e.setJoinMessage(null);
            }
        }
    }

    @EventHandler
    public void onReset(DojoServerRestartEvent e) {
        e.addRestartCondition(() -> Game.inst().getGame().getSession().getState() == SessionState.PREGAME);
    }

    // If they respawn after our game finished just let them spectate
    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        if (Game.inst().getGame().getSession().getState() == SessionState.FINISHED) {
            e.setRespawnLocation(e.getPlayer().getLocation());
            User.getUser(e.getPlayer()).setSpectator(true);
        }
    }

    @EventHandler
    public void onListPing(ServerListPingEvent e) {
        StagedSession session = Game.inst().getGame().getSession();
        if (!Game.inst().isPrivate() && session.getState() == SessionState.FINISHED) {
            e.setMotd(DBCommon.GSON.toJson(Game.inst().createSessionInfo()));
        }
    }

    // BLEEDING HACK
    @EventHandler
    public void onSpawnMushroom(CreatureSpawnEvent e) {
        if (e.getEntityType() == EntityType.MUSHROOM_COW && e.getSpawnReason() != SpawnReason.CUSTOM) {
        	e.setCancelled(true);
        }
    }
    @EventHandler
    public void onAsyncChat(AsyncPlayerChatEvent e) {
    	Player player = e.getPlayer();
    	User user = User.getUser(player);
    	if(user.getRank().inheritsRank(Rank.FIRST_DAN) && !user.getRank().inheritsRank(Rank.BUILDER)) {
    		if(user.isSpectator()) {
    			e.setCancelled(true);
    		}
    	}
    }
}
