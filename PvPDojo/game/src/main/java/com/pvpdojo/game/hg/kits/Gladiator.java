/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.PlayerPreDeathEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.kits.event.PlayerGladiatorEvent;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.lang.HGKitMessageKeys;
import com.pvpdojo.game.hg.session.Pit;
import com.pvpdojo.game.hg.session.event.PitStartEvent;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.hg.util.EquipmentUtil;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.kits.EntityInteractingKit;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.game.session.StageHandler;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.util.DojoRunnable;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.PlayerUtil;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.regions.Region;

import lombok.Data;

public class Gladiator extends ComplexKit implements EntityInteractingKit {

    public static final Map<UUID, GladiatorMatch> MATCH_MAP = new HashMap<>();

    public static boolean isInGladiator(Player player) {
        return MATCH_MAP.containsKey(player.getUniqueId());
    }

    @Override
    public void activate(PlayerInteractEntityEvent e) {
        if (e.getRightClicked() instanceof Player && HG.inst().getSession().getState() == SessionState.STARTED) {

            if (MATCH_MAP.containsKey(e.getRightClicked().getUniqueId()) || MATCH_MAP.containsKey(e.getPlayer().getUniqueId()) || !HGUser.getUser((Player) e.getRightClicked()).isIngame()) {
                return;
            }

            int buildLimit = HG.inst().getSettings().getBorder() - 25;
            if (Math.abs(e.getPlayer().getLocation().getX()) >= buildLimit || Math.abs(e.getPlayer().getLocation().getZ()) >= buildLimit) {
                e.getPlayer().sendMessage(CC.RED + "You cannot use gladiator near the border");
                return;
            }

            Player player = e.getPlayer();
            Player target = (Player) e.getRightClicked();

            if (KitUtil.hasKit(HGUser.getUser(target), HGKit.NEO)) {
                HGUser.getUser(player).sendMessage(HGKitMessageKeys.NEO_CANNOT_GLADIATOR);
                return;
            }

            if(EquipmentUtil.getEquipmentPoints(player) - 4 > EquipmentUtil.getEquipmentPoints(target)) {
                StageHandler currentStage = HG.inst().getSession().getCurrentHandler();
                if(!(currentStage instanceof Pit)) {
                    HGUser.getUser(player).sendMessage(HGKitMessageKeys.GLADIATOR_TOO_MANY_EP);
                    return;
                }
            }

            Location initial = BlockUtil.getHighestBlock(player.getWorld(), player.getLocation().getBlockX(), player.getLocation().getBlockZ()).getLocation();
            CuboidRegion region = findRegion(initial.add(0, 40, 0));

            PlayerUtil.removeVehicleAndPassenger(player);
            PlayerUtil.removeVehicleAndPassenger(target);

            Bukkit.getPluginManager().callEvent(new PlayerGladiatorEvent(player, target));

            GladiatorMatch match = new GladiatorMatch(region);

            for (Vector vector : match.getFaces()) {
                player.getWorld().getBlockAt(vector.getBlockX(), vector.getBlockY(), vector.getBlockZ()).setType(Material.GLASS);
            }

            for (int x = -2; x < 2; x++) {
                for (int z = -2; z < 2; z++) {
                    Vector vector = region.getCenter().add(x, 5, z);
                    player.getWorld().getBlockAt(vector.getBlockX(), vector.getBlockY(), vector.getBlockZ()).setType(Material.AIR);
                }
            }

            match.start(player, target);
        }
    }

    private CuboidRegion findRegion(Location initial) {
        CuboidRegion region = new CuboidRegion(new Vector(0, 0, 0), new Vector(22, 9, 22));

        region.expand(BukkitUtil.toVector(initial));
        region.contract(BukkitUtil.toVector(initial));

        if (region.getMaximumY() > 128) {
            return findRegion(initial.add(23, -40, 0));
        }

        return BlockUtil.isEmpty(initial.getWorld(), region) ? region : findRegion(initial.add(0, 15, 0));
    }


    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        for (GladiatorMatch match : MATCH_MAP.values()) {
            if (match.isWall(e.getBlock().getLocation())) {
                e.setCancelled(true);
                break;
            }
        }
    }

    @EventHandler
    public void onExplosion(EntityExplodeEvent e) {
        Iterator<Block> iterator = e.blockList().iterator();
        while (iterator.hasNext()) {
            Block block = iterator.next();
            for (GladiatorMatch match : MATCH_MAP.values()) {
                if (match.isWall(block.getLocation())) {
                    iterator.remove();
                    break;
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onMove(PlayerMoveEvent e) {
        if (!e.getFrom().getBlock().equals(e.getTo().getBlock()) && MATCH_MAP.containsKey(e.getPlayer().getUniqueId())) {
            GladiatorMatch match = MATCH_MAP.get(e.getPlayer().getUniqueId());
            if (!match.getRegion().contains(BukkitUtil.toVector(e.getTo()))) {
                match.cancel();
            }
        }
    }

    // There is also reference in HGUserImpl#isInCombat(int), so we dont need to listen for quit
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPreDeath(PlayerPreDeathEvent e) {
        if (MATCH_MAP.containsKey(e.getPlayer().getUniqueId())) {
            MATCH_MAP.get(e.getPlayer().getUniqueId()).cancel(e.getPlayer().getKiller());
        }
    }

    @EventHandler
    public void onPitStart(PitStartEvent e) {
        Iterator<GladiatorMatch> iterator = MATCH_MAP.values().stream().distinct().iterator();
        while (iterator.hasNext()) {
            iterator.next().cancel();
            iterator.remove();
        }
    }

    @Data
    private class GladiatorMatch implements Runnable {

        private final Map<UUID, Location> locationMap = new HashMap<>();
        private final CuboidRegion region;
        private final Region faces;
        private final DojoRunnable task = PvPDojo.schedule(this);

        private GladiatorMatch(CuboidRegion region) {
            this.region = region;
            this.faces = region.getFaces();
        }

        public boolean isWall(Location location) {
            return faces.contains(location.getBlockX(), location.getBlockY(), location.getBlockZ());
        }

        public void start(Player player, Player target) {
            MATCH_MAP.put(player.getUniqueId(), this);
            MATCH_MAP.put(target.getUniqueId(), this);

            locationMap.put(player.getUniqueId(), player.getLocation());
            locationMap.put(target.getUniqueId(), target.getLocation());

            Location location = BukkitUtil.toLocation(player.getWorld(), region.getCenter());

            PotionEffect pot = new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 60, 4);

            location.setYaw(315);
            player.teleport(location.clone().add(-7.5, -3, -7.5));
            location.setYaw(135);
            target.teleport(location.clone().add(8.5, -3, 8.5));

            player.addPotionEffect(pot);
            target.addPotionEffect(pot);

            task.createTimer(20, -1);
        }

        public void cancel() {
            task.cancel();
            for (UUID uuid : locationMap.keySet()) {
                Player player = Bukkit.getPlayer(uuid);
                if (player != null) {
                    player.teleport(locationMap.get(uuid));
                }
                MATCH_MAP.remove(uuid);
            }
            for (Vector vector : region) {
                Bukkit.getWorlds().get(0).getBlockAt(vector.getBlockX(), vector.getBlockY(), vector.getBlockZ()).setType(Material.AIR);
            }
        }

        public void cancel(Player winner) {
            cancel();
            if (winner != null) {
                winner.sendMessage(CC.GREEN + "You won the shadow fight");
                winner.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 100, 4));
            }
        }

        private int seconds;

        @Override
        public void run() {
            TrackerUtil.getIngame().filter(player -> region.contains(BukkitUtil.toVector(player.getLocation()))).forEach(player -> {
                if (!locationMap.keySet().contains(player.getUniqueId())) {
                    player.damage(6);
                    player.sendMessage(CC.RED + "You took damage from interferring a Gladiator match");
                }
            });
            if (++seconds >= 180) {
                locationMap.keySet().stream().map(Bukkit::getPlayer)
                           .forEach(player -> player.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 25, seconds > 180 ? 2 : 1), true));
            }
        }
    }

    @Override
    public boolean isSendCooldown() {
        return false;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.IRON_FENCE);
    }

}
