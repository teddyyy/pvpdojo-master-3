package com.pvpdojo.game.hg.kits.sidekits;

import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.RandomInventory;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.function.Consumer;

public class Monster extends Kit implements InteractingKit {

    private final RandomInventory<Consumer<Zombie>> RANDOMEQUIP = new RandomInventory<Consumer<Zombie>>(RandomInventory.RandomItem::getItem)
            .addItem(zombie -> {
                this.setHelmet(zombie, Material.AIR);
            }, 250)
            .addItem(zombie -> {
                this.setChestplate(zombie, Material.AIR);
            }, 250)
            .addItem(zombie -> {
                this.setLeggings(zombie, Material.AIR);
            }, 250)
            .addItem(zombie -> {
                this.setBoots(zombie, Material.AIR);
            }, 250)
            .addItem(zombie -> {
                this.setHelmet(zombie, Material.LEATHER_HELMET);
            }, 100)
            .addItem(zombie -> {
                this.setChestplate(zombie, Material.LEATHER_CHESTPLATE);
            }, 100)
            .addItem(zombie -> {
                this.setLeggings(zombie, Material.LEATHER_LEGGINGS);
            }, 100)
            .addItem(zombie -> {
                this.setBoots(zombie, Material.LEATHER_BOOTS);
            }, 100)
            .addItem(zombie -> {
                this.setHelmet(zombie, Material.GOLD_HELMET);
            }, 80)
            .addItem(zombie -> {
                this.setChestplate(zombie, Material.GOLD_CHESTPLATE);
            }, 80)
            .addItem(zombie -> {
                this.setLeggings(zombie, Material.GOLD_LEGGINGS);
            }, 80)
            .addItem(zombie -> {
                this.setBoots(zombie, Material.GOLD_BOOTS);
            }, 80)
            .addItem(zombie -> {
                this.setHelmet(zombie, Material.CHAINMAIL_HELMET);
            }, 45)
            .addItem(zombie -> {
                this.setChestplate(zombie, Material.CHAINMAIL_CHESTPLATE);
            }, 45)
            .addItem(zombie -> {
                this.setLeggings(zombie, Material.CHAINMAIL_LEGGINGS);
            }, 45)
            .addItem(zombie -> {
                this.setBoots(zombie, Material.CHAINMAIL_BOOTS);
            }, 45)
            .addItem(zombie -> {
                this.setHelmet(zombie, Material.IRON_HELMET);
            }, 23)
            .addItem(zombie -> {
                this.setChestplate(zombie, Material.IRON_CHESTPLATE);
            }, 23)
            .addItem(zombie -> {
                this.setLeggings(zombie, Material.IRON_LEGGINGS);
            }, 23)
            .addItem(zombie -> {
                this.setBoots(zombie, Material.IRON_BOOTS);
            }, 23)
            .addItem(zombie -> {
                this.setHelmet(zombie, Material.DIAMOND_HELMET);
            }, 2)
            .addItem(zombie -> {
                this.setChestplate(zombie, Material.DIAMOND_CHESTPLATE);
            }, 2)
            .addItem(zombie -> {
                this.setLeggings(zombie, Material.DIAMOND_LEGGINGS);
            }, 2)
            .addItem(zombie -> {
                this.setBoots(zombie, Material.DIAMOND_BOOTS);
            }, 2);


    public void setHelmet(Zombie zombie, Material helmet) {
        zombie.getEquipment().setHelmet(new ItemStack(helmet));
    }

    public void setChestplate(Zombie zombie, Material chestplate) {
        zombie.getEquipment().setChestplate(new ItemStack(chestplate));
    }

    public void setLeggings(Zombie zombie, Material leggins) {
        zombie.getEquipment().setLeggings(new ItemStack(leggins));
    }

    public void setBoots(Zombie zombie, Material boots) {
        zombie.getEquipment().setBoots(new ItemStack(boots));
    }


    @Override
    public void activate(PlayerInteractEvent event) {
        Zombie zombie = (Zombie) event.getPlayer().getWorld().spawnEntity(event.getPlayer().getLocation(), EntityType.ZOMBIE);
        zombie.setBaby(false);
        RANDOMEQUIP.generateRandomItem().accept(zombie);
        kitCooldown.use();
    }

    @Override
    public void onKillEntity(EntityDeathEvent e) {
        if (e.getEntity() instanceof org.bukkit.entity.Monster) {
            org.bukkit.entity.Monster monster = (org.bukkit.entity.Monster) e.getEntity();
            e.getDrops().clear();

            for (ItemStack itemStack : monster.getEquipment().getArmorContents()) {
                itemStack.setDurability((short) 0);
                e.getDrops().add(itemStack);
            }
            monster.getEquipment().getItemInHand().setDurability((short) 0);
            e.getDrops().add(monster.getEquipment().getItemInHand());
        }
    }

    @Override
    public void onPlayerDamagedByEntity(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof org.bukkit.entity.Monster) {
            e.setCancelled(true);
            return;
        }
        if (e.getDamager() instanceof Projectile) {
            if (((Projectile) e.getDamager()).getShooter() instanceof org.bukkit.entity.Monster) {
                e.setCancelled(true);
            }
        }
    }

    @Override
    public void click(Player player) {
    }

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.SKULL_ITEM, 1, (byte) 2);
    }

    @Override
    public int getCooldown() {
        return 60000;
    }

}
