/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class Cannibal extends Kit {

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {
        ((Player) e.getDamager()).setSaturation(((Player) e.getDamager()).getSaturation() + 1);
        ((Player) e.getDamager()).setFoodLevel(((Player) e.getDamager()).getFoodLevel() + 1);

        if (PlayerUtil.isRealHit((LivingEntity) e.getEntity()) && PvPDojo.RANDOM.nextInt(3) == 2) {
            ((Player) (e.getEntity())).addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 20 * 30, 1));
        }
    }
}
