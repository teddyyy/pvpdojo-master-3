/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.kits.util;

import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.inventory.ItemStack;

import com.pvpdojo.game.kits.ClickKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.kits.KitHolder;
import com.pvpdojo.game.kits.KitProvider;
import com.pvpdojo.game.userdata.kit.KitGameUser;
import com.pvpdojo.game.userdata.kit.SingleKitGameUser;
import com.pvpdojo.util.bukkit.ItemUtil;

public class KitUtil {

    public static void applyKitItems(KitGameUser<?> user, Kit kit) {
        if (kit instanceof ClickKit) {
            ItemUtil.addItemWhenFree(user.getPlayer(), user.getPlayer().getInventory().addItem(((ClickKit) kit).getClickItem()).values().toArray(new ItemStack[0]));
        }
        ItemUtil.addItemWhenFree(user.getPlayer(), user.getPlayer().getInventory().addItem(kit.getStartItems()).values().toArray(new ItemStack[0]));

        // Start armor
        if (kit.getStartArmor() != null) {
            if (kit.getStartArmor().length > 0 && kit.getStartArmor()[0] != null) {
                user.getPlayer().getInventory().setBoots(kit.getStartArmor()[0]);
            }
            if (kit.getStartArmor().length > 1 && kit.getStartArmor()[1] != null) {
                user.getPlayer().getInventory().setLeggings(kit.getStartArmor()[1]);
            }
            if (kit.getStartArmor().length > 2 && kit.getStartArmor()[2] != null) {
                user.getPlayer().getInventory().setChestplate(kit.getStartArmor()[2]);
            }
            if (kit.getStartArmor().length > 3 && kit.getStartArmor()[3] != null) {
                user.getPlayer().getInventory().setHelmet(kit.getStartArmor()[3]);
            }
        }
    }

    public static void applyKitItems(KitGameUser<?> user) {
        for (Kit kit : user.getKits()) {
            applyKitItems(user, kit);
        }
    }

    public static void removeKitItems(KitGameUser<?> user) {
        for (Kit kit : user.getKits()) {
            removeKitItems(user, kit);
        }
    }

    public static void removeKitItems(KitGameUser<?> user, Kit kit) {
        if (kit instanceof ClickKit) {
            int slot = user.getPlayer().getInventory().first(((ClickKit) kit).getClickItem().getType());
            if (slot != -1) {
                user.getPlayer().getInventory().setItem(slot, null);
            }
        }

        if (kit.getStartItems() != null) {
            user.getPlayer().getInventory().removeItem(kit.getStartItems());
        }

        if (kit.getStartArmor() != null) {
            ItemStack[] updateArmor = user.getPlayer().getInventory().getArmorContents();
            for (int i = 0; i < kit.getStartArmor().length; i++) {
                if (kit.getStartArmor()[i] != null) {
                    updateArmor[i] = null;
                }
            }
            user.getPlayer().getInventory().setArmorContents(updateArmor);
        }
    }

    public static boolean hasKitItem(KitGameUser<?> user) {
        for (Kit kit : user.getKits()) {
            if (kit instanceof ClickKit) {
                return user.getPlayer().getInventory().contains(((ClickKit) kit).getClickItem());
            }

            for (ItemStack startItem : kit.getStartItems()) {
                if (startItem != null) {
                    return user.getPlayer().getInventory().contains(startItem);
                }
            }

        }
        return false;
    }

    public static String getKitList(KitGameUser<?> user) {
        return getKitList(user.getKitHolders());
    }

    public static String getKitList(List<? extends KitHolder<?>> kitList) {
        return !kitList.isEmpty() ? kitList.stream()
                .map(KitHolder::getKitProvider)
                .map(KitProvider::getName)
                .collect(Collectors.joining(", ")) : "None";
    }

    public static KitProvider getKitProvider(SingleKitGameUser<?> user) {
        return user.getKitHolder() != null ? user.getKitHolder().getKitProvider() : null;
    }

    public static boolean hasKit(KitGameUser<?> user, KitProvider kitProvider) {
        return user.getKitHolders().stream().map(KitHolder::getKitProvider).anyMatch(kitProvider::equals);
    }

}
