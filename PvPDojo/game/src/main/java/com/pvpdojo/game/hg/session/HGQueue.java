/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.session;

import java.util.UUID;

import com.pvpdojo.game.kits.KitProvider;
import com.pvpdojo.session.Reloggable;
import com.pvpdojo.userdata.User;

public class HGQueue implements Reloggable {

    private final UUID uniqueId;
    private final KitProvider kit;
    private final KitProvider sideKit;

    public HGQueue(UUID uniqueId, KitProvider kit, KitProvider sideKit) {
        this.uniqueId = uniqueId;
        this.kit = kit;
        this.sideKit = sideKit;
    }

    @Override
    public void apply(User user) {
        if (kit != null) {
            user.getPlayer().performCommand("kit " + kit.name());
        }
        if (sideKit != null) {
            user.getPlayer().performCommand("sidekit " + sideKit.name());
        }
    }

    @Override
    public UUID getUniqueId() {
        return uniqueId;
    }
}
