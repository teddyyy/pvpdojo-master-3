/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.command;

import static com.pvpdojo.util.StringUtils.getReadableSeconds;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.game.Game;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("info")
public class InfoCommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onInfo(CommandSender sender) {
        if (Game.inst().getHost() != null && Bukkit.getPlayer(Game.inst().getHost()) != null) {
            sendMessage(sender, "Hosted by: " + Bukkit.getPlayer(Game.inst().getHost()).getName());
        }
        sendMessage(sender, "State: " + Game.inst().getGame().getSession().getState());
        sendMessage(sender, "Game time: " + getReadableSeconds(Game.inst().getGame().getSession().getCurrentHandler().getTimeSeconds()));

    }

    public void sendMessage(CommandSender sender, String message) {
        sender.sendMessage(getPrefix() + message);
    }

    public String getPrefix() {
        return CC.RED.toString();
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
