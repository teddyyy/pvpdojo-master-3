/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EntityType;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Frosty extends Kit {

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] {new ItemBuilder(new ItemStack(Material.SNOW_BALL, 10)).build()};
    }

    private boolean hasFrostySpeed = false;

    @Override
    public void onMove(PlayerMoveEvent e) {
        if (e.getPlayer().getLocation().getBlock().getRelative(BlockFace.DOWN).getType() == Material.SNOW) {
            e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 5*20, 1));
        }
        if (e.getFrom().getBlock().equals(e.getTo().getBlock())) {
            Biome biome = e.getTo().getBlock().getBiome();
            if (biome == Biome.COLD_TAIGA|| biome == Biome.ICE_PLAINS || biome == Biome.ICE_PLAINS_SPIKES ) {
                if (!e.getPlayer().hasPotionEffect(PotionEffectType.SPEED) || hasFrostySpeed) {
                    hasFrostySpeed = true;
                    e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 5 * 20, 1), true);
                }
            } else {
                hasFrostySpeed = false;
            }
        }
    }

    @Override
    public void onProjectileHit(EntityDamageByEntityEvent e) {
        if (e.getEntity().getType() == EntityType.SNOWBALL && e.getEntity().isOnGround()) {
            Material material = Material.SNOW;
            BlockFace blockFace = BlockFace.SELF;
            if (e.getEntity().getLocation().getBlock().getType() == Material.WATER) {
                material = Material.ICE;
                blockFace = BlockFace.SELF;
            }
            else {
                material = Material.SNOW;
                blockFace = BlockFace.UP;
            }
            e.getEntity().getLocation().getBlock().setType(material);

            e.getEntity().getLocation().getBlock().getRelative(blockFace).getRelative(BlockFace.NORTH).setType(material);
            e.getEntity().getLocation().getBlock().getRelative(blockFace).getRelative(BlockFace.EAST).setType(material);
            e.getEntity().getLocation().getBlock().getRelative(blockFace).getRelative(BlockFace.SOUTH).setType(material);
            e.getEntity().getLocation().getBlock().getRelative(blockFace).getRelative(BlockFace.WEST).setType(material);

            e.getEntity().getLocation().getBlock().getRelative(blockFace).getRelative(BlockFace.NORTH).getRelative(BlockFace.WEST).setType(material);
            e.getEntity().getLocation().getBlock().getRelative(blockFace).getRelative(BlockFace.NORTH).getRelative(BlockFace.EAST).setType(material);
            e.getEntity().getLocation().getBlock().getRelative(blockFace).getRelative(BlockFace.SOUTH).getRelative(BlockFace.WEST).setType(material);
            e.getEntity().getLocation().getBlock().getRelative(blockFace).getRelative(BlockFace.SOUTH).getRelative(BlockFace.EAST).setType(material);
        }
    }
}

