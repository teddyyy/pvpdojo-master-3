/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.userdata.kit;

import java.util.UUID;

import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.kits.KitHolder;
import com.pvpdojo.game.kits.KitProvider;

public abstract class SingleKitGameUserImpl<P extends KitProvider> extends KitGameUserImpl<P> implements SingleKitGameUser<P> {

    public SingleKitGameUserImpl(UUID uuid) {
        super(uuid);
    }

    @Override
    public KitHolder getKitHolder() {
        return !getKitHolders().isEmpty() ? getKitHolders().get(0) : null;
    }

    @Override
    public Kit getKit() {
        return getKitHolder() != null ? getKitHolder().getKit() : null;
    }

    @Override
    public void setKit(P kitProvider) {
        getKits().clear();
        if (kitProvider != null) {
            addKit(kitProvider);
        }
    }

    @Override
    public boolean hasKit() {
        return !getKits().isEmpty();
    }

}
