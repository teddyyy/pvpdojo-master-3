/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.userdata.kit;

import java.util.List;

import org.bukkit.entity.Player;

import com.pvpdojo.game.Game;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.kits.KitHolder;
import com.pvpdojo.game.kits.KitProvider;
import com.pvpdojo.game.userdata.GameUser;
import com.pvpdojo.userdata.User;

public interface KitGameUser<P extends KitProvider> extends User, GameUser {

    static KitGameUser<?> getUser(Player player) {
        return (KitGameUserImpl<?>) Game.inst().getUser(player);
    }

    List<Kit> getKits();

    List<Kit> getKitsDirect();

    List<KitHolder<P>> getKitHolders();

    void addKit(P kitProvider);
}
