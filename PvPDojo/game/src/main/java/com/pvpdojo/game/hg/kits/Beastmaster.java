/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.util.bukkit.CC;

public class Beastmaster extends Kit implements InteractingKit {

    @Override
    public void click(Player player) {
        if (HG.inst().getSession().getState() == SessionState.STARTED) {
            for (int i = 1; i <= 3; i++) {
                Wolf wolf = (Wolf) player.getWorld().spawnEntity(player.getLocation(), EntityType.WOLF);
                wolf.setOwner(player);
                wolf.setTamed(true);
                wolf.setSitting(false);
            }
            kitCooldown.use();
        } else {
            player.sendMessage(CC.RED + "Cannot use in invincibility");
        }
    }

    @Override
    public void activate(PlayerInteractEvent e) {}

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() { return new ItemStack(Material.BONE); }

    @Override
    public int getCooldown() { return 70000; }
}
