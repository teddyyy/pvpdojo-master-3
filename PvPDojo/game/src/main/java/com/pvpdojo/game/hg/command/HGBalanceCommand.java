/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.command;

import com.pvpdojo.command.general.BalanceCommand;
import com.pvpdojo.userdata.User;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Description;

@CommandAlias("balance|bal|money|coins|credits")
public class HGBalanceCommand extends BalanceCommand {

    @Description("{@@pvpdojo.cmd-description.balance}")
    @Default
    @CatchUnknown
    public void onCommand(User user) {
        onHGBalance(user);
    }

}
