/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import java.util.List;

import com.pvpdojo.game.hg.kits.list.HGKit;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.kits.KitHolder;

public class Stomper extends Kit {

    public static final double RANGE = 3D;

    @Override
    public void onDamage(EntityDamageEvent e) {

        if (e.getCause() == DamageCause.FALL && !e.isCancelled()) {
            Player player = (Player) e.getEntity();
            player.getWorld().playSound(player.getLocation(), Sound.ANVIL_LAND, 1F, 1F);

            List<Entity> ents = player.getNearbyEntities(RANGE, RANGE, RANGE);

            for (Entity et : ents) {

                if (!(et instanceof Player)) {
                    continue;
                }

                Player t = (Player) et;

                if (!player.canSee(t)) {
                    continue;
                }

                HGUser tuser = HGUser.getUser(t);

                // Prevent StackOverFlow
                boolean stomper = tuser.getKitHolders().stream().map(KitHolder::getKitProvider).anyMatch(HGKit.STOMPER::equals);

                if (t.isSneaking()) {
                    NMSUtils.damageEntity(t, player, stomper ? DamageCause.ENTITY_ATTACK : DamageCause.FALL, 4);
                } else {
                    NMSUtils.damageEntity(t, player, stomper ? DamageCause.ENTITY_ATTACK : DamageCause.FALL, e.getFinalDamage());
                }
            }

            e.setDamage(Math.min(e.getFinalDamage(), 4D));
        }

    }

}
