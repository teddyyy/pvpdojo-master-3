/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.LightningStrike;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.kits.BlockInteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class Thor extends Kit implements BlockInteractingKit {

    private static final String THOR_META = "thor";

    @Override
    public void activate(PlayerInteractEvent event) {}

    @Override
    public void onRightClick(PlayerInteractEvent e) {
        if (HG.inst().getSession().getState() == SessionState.STARTED) {
            kitCooldown.use();

            Location c = e.getClickedBlock().getLocation();
            Block target = BlockUtil.getHighestBlockMatching(c.getWorld(), c.getBlockX(), c.getBlockZ(), block -> block.getType().isSolid() && block.getType() != Material.GLASS);

            if (target.getY() >= 79) {
                if (target.getType() == Material.NETHERRACK) {
                    BukkitUtil.createExplosionByEntity(e.getPlayer(), target.getLocation(), 5.5F, true, true);
                } else {
                    target = target.getRelative(BlockFace.UP);
                    target.setType(Material.NETHERRACK);
                }
            }
            target.getWorld().strikeLightning(target.getLocation().add(0, 1, 0)).setMetadata(THOR_META, new FixedMetadataValue(PvPDojo.get(), e.getPlayer().getName()));

        }
    }

    @Override
    public void onDamage(EntityDamageEvent e) {
        if (e instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent ee = (EntityDamageByEntityEvent) e;
            if (ee.getDamager() instanceof LightningStrike && ee.getDamager().hasMetadata(THOR_META)
                    && ee.getDamager().getMetadata(THOR_META).get(0).asString().equals(((Player) e.getEntity()).getName())) {
                e.setDamage(0);
            }
        }
    }

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemBuilder(Material.WOOD_AXE).unbreakable().build();
    }

    @Override
    public int getCooldown() {
        return 9000;
    }
}
