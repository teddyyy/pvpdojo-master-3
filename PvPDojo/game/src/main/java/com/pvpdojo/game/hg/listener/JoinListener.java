/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.listener;

import org.bukkit.event.EventHandler;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.DojoPlayerJoinEvent;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.game.hg.userdata.HGUser;

public class JoinListener implements DojoListener {

    @EventHandler
    public void onJoin(DojoPlayerJoinEvent e) {
        PvPDojo.schedule(() -> HGUser.getUser(e.getPlayer()).getPersistentKits().pull()).createAsyncTask();
    }

}
