/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits.menu;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.menu.SortType;
import com.pvpdojo.menu.SortedInventoryMenu;
import com.pvpdojo.menu.SortedItem;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class KitSelectorMenu extends SortedInventoryMenu {

    public KitSelectorMenu(Player player) {
        super(player, "Choose your kit", 54, SortType.ALPHABETICAL);
    }

    @Override
    public void redraw() {
        super.redraw();
        HGUser user = HGUser.getUser(getPlayer());
        for (HGKit kit : user.getPersistentKits().getAvailableKits()) {
            if (kit.isSideKit()) {
                continue;
            }

            List<String> lore = new ArrayList<>();
            if (HG.inst().getCurrentRotation().getKits().contains(kit) || user.getPersistentKits().getOwnedKits().contains(kit)
                || kit == user.getBonusKit()) {
                lore.add("");
                if (kit == user.getBonusKit()) {
                    lore.add(CC.GOLD + "[Bonus Kit]");
                }
                if (HG.inst().getCurrentRotation().getKits().contains(kit)) {
                    lore.add(CC.DARK_PURPLE + "[Kit Rotation]");
                }
                if (user.getPersistentKits().getOwnedKits().contains(kit)) {
                    lore.add(CC.GREEN + "[Owned]");
                }
            }
            SortedItem item = new SortedItem(new ItemBuilder(kit.getIcon(user)).appendLore(lore).build(), type -> {
                getPlayer().performCommand("kit " + kit);
                closeSafely();
            });
            item.setSortName(kit.name());
            addSortItem(item);
        }

    }

}
