/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.naturalevents;

import java.util.EnumSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.CC;

public class SnowStorm extends BiomeSpecificEvent {

    @Override
    Set<Biome> getBiomes() {
        return EnumSet.of(Biome.COLD_TAIGA, Biome.COLD_TAIGA_HILLS, Biome.COLD_TAIGA_MOUNTAINS, Biome.ICE_PLAINS_SPIKES, Biome.ICE_PLAINS, Biome.ICE_MOUNTAINS);
    }

    @Override
    public void start() {
        PvPDojo.get().getServerSettings().setRain(true);
        Bukkit.getWorlds().get(0).setStorm(true);
        TrackerUtil.getIngame().forEach(player -> {
            HGUser user = HGUser.getUser(player);
            if (KitUtil.hasKit(user, HGKit.KANGAROO) || KitUtil.hasKit(user, HGKit.GRAPPLER) || KitUtil.hasKit(user, HGKit.PHANTOM)) {
                user.disableKits(getDuration());
            }
        });
    }

    @Override
    public void tick() {
        TrackerUtil.getIngame().forEach(player -> {
            if (getBiomes().contains(player.getLocation().getBlock().getBiome())) {
                if (User.getUser(player).getNoMoveSeconds() == 2) {
                    BlockUtil.getNearbyBlocks(player.getLocation(), 3).stream().filter(block -> block.getType() == Material.AIR).forEach(block -> block.setType(Material.ICE));
                    player.sendMessage(CC.RED + "You froze by standing still too long");
                }
                player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 5 * 20, 0), true);
                player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 5 * 20, 3), true);
            }
        });

    }

    @Override
    public void end() {
        PvPDojo.get().getServerSettings().setRain(false);
        Bukkit.getWorlds().get(0).setStorm(false);
    }

    @Override
    public int getDuration() {
        return 90;
    }

    @Override
    public LocaleMessage getMessage() {
        return LocaleMessage.of("The HG Gods are not satisfied with this game, better put on warm clothes! " + CC.GOLD + "[Snow Storm]");
    }
}
