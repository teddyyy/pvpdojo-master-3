/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.userdata;

import com.pvpdojo.game.Game;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.kits.sidekits.Assassin;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.gui.HotbarGUI;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Comparator;

public class TrackerHotbarGUI extends HotbarGUI {

    private static final TrackerHotbarGUI INST = new TrackerHotbarGUI();

    public static TrackerHotbarGUI get() {
        return INST;
    }

    private TrackerHotbarGUI() {
        setCancel(false);
        setItem(8, new ItemBuilder(Material.COMPASS).name(CC.RED + "Tracker").build(), (player, action) -> {

            Player tracked = trackPlayer(player);

            if (tracked != null) {
                player.setCompassTarget(tracked.getLocation());

                boolean isAssassin = KitUtil.hasKit(HGUser.getUser(player), HGKit.ASSASSIN) || HGUser.getUser(player).getSideKit().getKitProvider() == HGKit.ASSASSIN;
                int distance = (int) player.getLocation().distance(tracked.getLocation());

                player.sendMessage(CC.YELLOW + "Your compass is now pointing at " + tracked.getNick()
                        + (isAssassin ? " (" + KitUtil.getKitList(HGUser.getUser(tracked)) + ") [" + distance + " blocks]" + " [" + HGUser.getUser(tracked).getKills() + "]" : ""));

                if (isAssassin) {
                    Assassin.handleTracker(player, tracked);
                }

            } else {
                player.setCompassTarget(new Location(Bukkit.getWorlds().get(0), 0, 0, 0));
                player.sendMessage(CC.YELLOW + "No target found. Pointing at spawn");
            }
        });
    }


    private Player trackPlayer(Player player) {
        double range = Math.sqrt(2 * Math.pow(HG.inst().getSettings().getBorder(), 2));

        return TrackerUtil.getNearbyIngame(player, range, 255, range)
                .filter(other -> !(KitUtil.hasKit(HGUser.getUser(other), HGKit.ASSASSIN) && Game.inst().getGame().getSession().getCurrentHandler().getTimeSeconds() <= 300))
                .filter(other -> other.getLocation().distance(new Location(player.getWorld(), player.getLocation().getX(), other.getLocation().getY(), player.getLocation().getZ()))
                        > (KitUtil.hasKit(HGUser.getUser(player), HGKit.ASSASSIN) ? 10 : 20))
                .min(Comparator.comparingDouble(other -> other.getLocation().distance(player.getLocation())))
                .orElse(null);
    }

}
