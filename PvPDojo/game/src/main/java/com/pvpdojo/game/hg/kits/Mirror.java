/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.game.kits.EntityInteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.CC;

public class Mirror extends Kit implements EntityInteractingKit {

    @Override
    public void activate(PlayerInteractEntityEvent e) {
        if (e.getRightClicked() instanceof Player) {
            Player target = (Player) e.getRightClicked();

            ItemStack[] contents = target.getInventory().getContents();
            // 200 IQ start
            for (int i = 0; i < target.getInventory().getContents().length; i += 9) {
                for (int j = 0; j < 4; j++) {
                    ItemStack temp = contents[i + j];
                    contents[i + j] = contents[9 + i - j - 1];
                    contents[9 + i - j - 1] = temp;
                }
            }
            // 200 IQ end
            target.getInventory().setContents(contents);
            target.sendMessage(CC.RED + "You inventory has been mirrored");
            e.getPlayer().sendMessage(CC.GREEN + "You mirrored " + target.getNick() + "'s inventory!");
            kitCooldown.use();
        }
    }

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.THIN_GLASS);
    }

    @Override
    public int getCooldown() {
        return 50_000;
    }
}
