/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.command;

import com.pvpdojo.command.general.StatsCommand;
import com.pvpdojo.userdata.User;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Optional;
import co.aikar.commands.annotation.Single;

@CommandAlias("stats")
public class HGStatsCommand extends StatsCommand {

    @Default
    @CatchUnknown
    @CommandCompletion("@allplayers")
    public void onStats(User user, @Optional @Single String target) {
        onHGStats(user, target);
    }

}
