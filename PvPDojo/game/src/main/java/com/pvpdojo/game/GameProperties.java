/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;
import java.util.UUID;

import com.pvpdojo.DBCommon;
import com.pvpdojo.Gamemode;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.clan.Clan;
import com.pvpdojo.clan.ClanList;
import com.pvpdojo.game.ctm.CTM;
import com.pvpdojo.game.ctm.settings.CTMSettings;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.settings.HGSettings;
import com.pvpdojo.game.lms.LMS;
import com.pvpdojo.game.lms.settings.LMSSettings;
import com.pvpdojo.session.entity.ClanArenaTeam;
import com.pvpdojo.session.entity.PreparedArenaTeam;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;

import lombok.Getter;

@Getter
public class GameProperties {

    private UUID host;
    private String forcedMap;
    private boolean privateGame;

    public void readProperties(Game parent) {
        Properties properties = new Properties();
        try (InputStream input = new FileInputStream(GameProperty.FILE_NAME)) {
            properties.load(input);

            // We have a predetermined server id
            if (properties.containsKey(GameProperty.SERVER_ID)) {
                PvPDojo.SERVER_ID = Integer.valueOf(properties.getProperty(GameProperty.SERVER_ID));
            }

            if (properties.containsKey(GameProperty.GAME_ID)) {
                Game.inst().setGameId(Integer.valueOf(properties.getProperty(GameProperty.GAME_ID)));
            }

            // This game is hosted
            if (properties.containsKey(GameProperty.HOST)) {
                UUID hostUUID = UUID.fromString(properties.getProperty(GameProperty.HOST));

                if (PvPDojo.getOfflinePlayer(hostUUID).hasPlayedBefore()) {
                    this.host = hostUUID;

                    // Send host to this server
                    Game.inst().addPlayer(host);
                    // Disable stats for player hosted games, preventing abuse
                    PvPDojo.get().getServerSettings().setStats(false);
                }
            }

            if (properties.containsKey(GameProperty.PRIVATE)) {
                this.privateGame = Boolean.valueOf(properties.getProperty(GameProperty.PRIVATE));
            }

            String settings = properties.getProperty(GameProperty.SETTINGS);

            // Get our game
            switch (Gamemode.valueOf(properties.getProperty(GameProperty.GAME))) {
                case HG:
                    parent.setGame(HG.register(parent, settings != null ? DBCommon.GSON.fromJson(settings, HGSettings.class) : new HGSettings()));
                    break;
                case CTM:
                    PreparedArenaTeam teamRed = readTeam(properties, GameProperty.TEAM_RED, ClanList.getClan(properties.getProperty(GameProperty.CLAN_RED)), CC.RED);
                    PreparedArenaTeam teamBlue = readTeam(properties, GameProperty.TEAM_BLUE, ClanList.getClan(properties.getProperty(GameProperty.CLAN_BLUE)), CC.BLUE);

                    parent.setGame(CTM.register(parent, settings != null ? DBCommon.GSON.fromJson(settings, CTMSettings.class) : new CTMSettings(), teamRed, teamBlue));
                    break;
                case LMS:
                    parent.setGame(LMS.register(parent, settings != null ? DBCommon.GSON.fromJson(settings, LMSSettings.class) : new LMSSettings()));
                    break;
                default:
                    Log.severe("Invalid Gamemode");
            }

            // General predetermined participants
            if (properties.containsKey(GameProperty.PARTICIPANTS)) {
                StringUtils.getListFromString(properties.getProperty(GameProperty.PARTICIPANTS), StringUtils.PIPE, UUID::fromString).forEach(Game.inst()::addParticipant);
            }

            // We have a forced map
            if (properties.containsKey(GameProperty.FORCED_MAP)) {
                this.forcedMap = properties.getProperty(GameProperty.FORCED_MAP);
            }

        } catch (IOException e) {
            Log.exception("Reading game properties", e);
        }
    }

    private PreparedArenaTeam readTeam(Properties properties, String key, Clan clan, CC color) {
        PreparedArenaTeam team = null;
        if (properties.containsKey(key)) {
            UUID[] uuids = StringUtils.getListFromString(properties.getProperty(key), StringUtils.PIPE, UUID::fromString).toArray(new UUID[0]);
            team = clan != null ? new ClanArenaTeam(null, clan, color, uuids) : new PreparedArenaTeam(null, color, uuids);
            Arrays.stream(uuids).forEach(Game.inst()::addParticipant);
        }
        return team;
    }

}
