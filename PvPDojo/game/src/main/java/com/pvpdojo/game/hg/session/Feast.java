/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.session;


import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.world.StructureGrowEvent;

import com.pvpdojo.game.session.StageHandler;
import com.pvpdojo.game.session.StagedSession;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.regions.CylinderRegion;

public abstract class Feast extends PvPStage {

    protected CylinderRegion feastRegion;

    public Feast(StagedSession session) {
        super(session);
    }

    @Override
    public StageHandler init() {
        spawnFeast();
        return super.init();
    }

    public abstract void spawnFeast();

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {

        if (feastRegion != null && feastRegion.contains(BukkitUtil.toVector(e.getBlock()))) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {

        if (feastRegion != null && feastRegion.contains(BukkitUtil.toVector(e.getBlock()))) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onLiquidBuild(BlockFormEvent e) {
        if (feastRegion != null && feastRegion.contains(BukkitUtil.toVector(e.getBlock()))) {
            if (e.getNewState().getType() == Material.OBSIDIAN || e.getNewState().getType() == Material.COBBLESTONE || e.getNewState().getType() == Material.STONE) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onExplosion(EntityExplodeEvent e) {
        if (feastRegion != null) {
            e.blockList().removeIf(block -> feastRegion.contains(BukkitUtil.toVector(block)));
        }
    }

    @EventHandler
    public void onStructureGrowEvent(StructureGrowEvent e) {
        if (feastRegion != null) {
            for (BlockState state : e.getBlocks()) {
                if (feastRegion.contains(BukkitUtil.toVector(state.getLocation()))) {
                    e.setCancelled(true);
                    break;
                }
            }
        }
    }

}
