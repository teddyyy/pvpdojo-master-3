/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.game.kits.ComplexKit;

public class Demoman extends ComplexKit {

    private static final Map<Location, Player> PLATES = new HashMap<>();

    @Override
    public void onPlace(BlockPlaceEvent e) {
        if (!e.isCancelled() && e.getBlock().getType() == Material.STONE_PLATE && e.getBlock().getRelative(BlockFace.DOWN).getType() == Material.GRAVEL) {
            PLATES.put(e.getBlock().getLocation(), e.getPlayer());
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void onInteract(PlayerInteractEvent e) {
        if (e.getAction() == Action.PHYSICAL && PLATES.containsKey(e.getClickedBlock().getLocation())
                && !e.getPlayer().getUniqueId().equals(PLATES.get(e.getClickedBlock().getLocation()).getUniqueId())) {
            Player player = PLATES.get(e.getClickedBlock().getLocation());
            BukkitUtil.createExplosionByEntity(player.isOnline() ? player : null, e.getClickedBlock().getLocation(), 5, true, false);
        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        if (!e.isCancelled()) {
            PLATES.remove(e.getBlock().getLocation());
            PLATES.remove(e.getBlock().getRelative(BlockFace.UP).getLocation());
        }
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] { new ItemStack(Material.GRAVEL, 6), new ItemStack(Material.STONE_PLATE, 6) };
    }
}
