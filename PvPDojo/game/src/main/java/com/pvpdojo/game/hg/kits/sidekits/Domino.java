package com.pvpdojo.game.hg.kits.sidekits;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.util.bukkit.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class Domino extends Kit {

    private Map<UUID, Integer> domino = new HashMap<>();

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {

        if (e.isCancelled() || !PlayerUtil.isRealHit((LivingEntity) e.getEntity())) {
            return;
        }

        if (e.getEntity() instanceof Player) {
            double damage = e.getDamage() / 1.5f;

            //Stream through all nearby players
            List<Player> stream = TrackerUtil.getNearbyIngame((Player) e.getEntity(), 3, 3, 3)
                    .filter(player -> player != e.getDamager()).collect(Collectors.toList());

            //Collected players is 1 or more
            if (stream.size() > 0) {
                //Fill the map
                stream.forEach(player -> domino.put(player.getUniqueId(), domino.size() + 1));

                //Do Domino effect.
                for (Map.Entry<UUID, Integer> entry : domino.entrySet()) {

                    UUID uuid = entry.getKey();
                    int delay = entry.getValue();

                    PvPDojo.schedule(() -> {
                        NMSUtils.damageEntity(Bukkit.getPlayer(uuid), e.getDamager(), EntityDamageEvent.DamageCause.ENTITY_ATTACK, damage);
                        applyKnockback((LivingEntity) e.getDamager(), Bukkit.getPlayer(uuid), 2);

                        domino.clear();
                    }).createTask(delay);
                }
            }
        }
    }


    @Override
    public void onDamage(EntityDamageEvent e) {

        if (e.getCause() == EntityDamageEvent.DamageCause.FALL && !e.isCancelled()) {
            Player player = (Player) e.getEntity();

            TrackerUtil.getNearbyIngame(player, 6, 6, 6)
                    .forEach(target -> {
                        NMSUtils.damageEntity(target, player, EntityDamageEvent.DamageCause.ENTITY_ATTACK, target.isSneaking() ? 4.0 : e.getFinalDamage());
                        applyKnockback(player, target, 6);
                    });
        }
    }

    private void applyKnockback(LivingEntity source, LivingEntity entity, int power) {
        for (int i = 0; i < power; i++) {//More knockback
            PvPDojo.schedule(() -> NMSUtils.applyKnockback(entity, source)).createTask(i);
        }
    }

}
