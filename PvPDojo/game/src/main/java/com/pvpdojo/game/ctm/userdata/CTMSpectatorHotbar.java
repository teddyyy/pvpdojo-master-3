/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.userdata;

import org.bukkit.Material;

import com.pvpdojo.game.ctm.util.CTMUtil;
import com.pvpdojo.gui.HotbarGUI;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class CTMSpectatorHotbar extends HotbarGUI {

    private static final CTMSpectatorHotbar INST = new CTMSpectatorHotbar();

    public static CTMSpectatorHotbar inst() {
        return INST;
    }

    private CTMSpectatorHotbar() {
        setItem(0, new ItemBuilder(Material.COMPASS).name(CC.RED + "Team Selector").build(), onRightClick(CTMUtil::handleTeamSelection));
    }

}
