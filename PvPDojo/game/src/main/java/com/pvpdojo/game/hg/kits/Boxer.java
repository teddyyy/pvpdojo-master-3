/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class Boxer extends Kit {

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {
        if (((Player) e.getDamager()).getItemInHand().getType() == Material.AIR && PlayerUtil.isRealHit((LivingEntity) e.getEntity())) {
            e.setDamage(6D / 3D * 2D);
        }
    }

    @Override
    public void onPlayerDamagedByPlayer(EntityDamageByEntityEvent e) {
        if (PlayerUtil.isCrit((LivingEntity) e.getDamager())) {
            e.setDamage(e.getDamage() - 0.5);
        }
    }
}
