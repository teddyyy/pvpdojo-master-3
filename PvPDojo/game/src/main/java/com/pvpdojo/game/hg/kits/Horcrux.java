/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Skull;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.PlayerPreDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimaps;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.lang.HGKitMessageKeys;
import com.pvpdojo.game.hg.session.event.PitStartEvent;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.CC;

import lombok.Data;

public class Horcrux extends ComplexKit {

    private static final EnumSet<BlockFace> FACES = EnumSet.of(BlockFace.SOUTH, BlockFace.NORTH, BlockFace.EAST, BlockFace.WEST, BlockFace.SOUTH_EAST,
            BlockFace.SOUTH_WEST, BlockFace.NORTH_EAST, BlockFace.NORTH_WEST, BlockFace.UP);
    private static final ListMultimap<UUID, HorcruxThingy> HORCRUX_TOTEMS = Multimaps.newListMultimap(new HashMap<>(), ArrayList::new);

    private static HorcruxThingy getLastHorcrux(UUID uuid) {
        List<HorcruxThingy> horcruxThingies = HORCRUX_TOTEMS.get(uuid);
        return horcruxThingies.get(horcruxThingies.size() - 1);
    }

    private int kills;
    private int deathsInARow;

    @Override
    public void onPreDeath(PlayerPreDeathEvent e) {
        Player killer = e.getPlayer().getKiller();

        if (HORCRUX_TOTEMS.containsKey(e.getPlayer().getUniqueId())) {
            e.getPlayer().getWorld().playSound(e.getPlayer().getLocation(), Sound.SILVERFISH_KILL, 100, 1);

            if (++deathsInARow >= 5) {
                deathsInARow = 0;
                getLastHorcrux(e.getPlayer().getUniqueId()).remove();

                if (killer != null) {
                    User.getUser(killer).sendMessage(LocaleMessage.of(HGKitMessageKeys.HORCRUX_DESTROY_DEATH).transformer(str -> CC.GREEN + str));
                }

                User.getUser(e.getPlayer()).sendMessage(LocaleMessage.of(HGKitMessageKeys.HORCRUX_DESTROY_DEATH).transformer(str -> CC.RED + str));
            } else if (killer != null) {
                killer.sendMessage(CC.RED + "Your opponent has created a Horcrux, go find it to put an end to his immortality:");

                for (HorcruxThingy horcruxThingy : HORCRUX_TOTEMS.get(e.getPlayer().getUniqueId())) {
                    killer.sendMessage(CC.RED + BlockUtil.asString(horcruxThingy.getBlock()));
                }
            }

            e.setCancelled(true);

            e.getPlayer().setHealth(e.getPlayer().getMaxHealth());
            e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 6 * 20, 1));
            e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 6 * 20, 1));
            e.getPlayer().getInventory().addItem(new ItemStack(Material.MUSHROOM_SOUP), new ItemStack(Material.MUSHROOM_SOUP), new ItemStack(Material.MUSHROOM_SOUP),
                    new ItemStack(Material.MUSHROOM_SOUP));
        }
    }

    @Override
    public void onKill(PlayerDeathEvent e) {
        kills++;
        deathsInARow = 0;

        if (kills == 1 || kills == 5 || kills == 10) {
            e.getEntity().getKiller().setMaxHealth(e.getEntity().getKiller().getMaxHealth() - 2);
            e.getEntity().getWorld().playSound(e.getEntity().getLocation(), Sound.SILVERFISH_KILL, 100, 1);

            int border = HG.inst().getSettings().getBorder() - 26;
            Block block = BlockUtil.getFirstBlockUp(e.getEntity().getLocation(), Material.AIR).getBlock();

            if (Math.abs(block.getX()) > border) {
                block = block.getWorld().getBlockAt(block.getX() < 0 ? -border : border, block.getY(), block.getZ());
            }
            if (Math.abs(block.getZ()) > border) {
                block = block.getWorld().getBlockAt(block.getX(), block.getY(), block.getZ() < 0 ? -border : border);
            }

            while (block.getType() == Material.BEDROCK) {
                block = block.getRelative(BlockFace.UP);
            }

            HORCRUX_TOTEMS.put(e.getEntity().getKiller().getUniqueId(), new HorcruxThingy(block, e.getEntity().getKiller().getUniqueId()));
        }
    }

    @EventHandler
    public void onPitStart(PitStartEvent e) {
        HG.inst().getSession().broadcast(HGKitMessageKeys.HORCRUX_DESTROY_ALL);
        new ArrayList<>(HORCRUX_TOTEMS.values()).forEach(HorcruxThingy::remove);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        for (HorcruxThingy thingy : HORCRUX_TOTEMS.values()) {
            if (e.getBlock().equals(thingy.getBlock())) {
                e.getPlayer().sendMessage(CC.GREEN + "You destroyed a Horcrux, your opponents is slowly becoming mortal again.");
                e.getBlock().setType(Material.AIR);
                e.setCancelled(true);
            }
        }
    }

    @Data
    private class HorcruxThingy implements Runnable {

        private Block block;
        private UUID owner;
        private int taskId;

        private HorcruxThingy(Block block, UUID owner) {
            this.block = block;
            this.owner = owner;

            this.block.setType(Material.SKULL);
            Skull skull = ((Skull) block.getState());
            skull.setRawData((byte) 1);
            skull.setSkullType(SkullType.WITHER);
            skull.update();

            this.taskId = PvPDojo.schedule(this).createTimer(10, -1);
        }

        @Override
        public void run() {
            if (block.getType() != Material.SKULL) {
                remove();
            } else {
                for (BlockFace face : FACES) {
                    Block b = block.getRelative(face);
                    if (b.getType() != Material.BEDROCK) {
                        b.setType(Material.AIR);
                    }
                }
            }
        }

        public void remove() {
            block.setType(Material.AIR);
            HORCRUX_TOTEMS.remove(owner, this);
            Bukkit.getScheduler().cancelTask(taskId);
            if (Bukkit.getPlayer(owner) != null) {
                Bukkit.getPlayer(owner).sendMessage(CC.RED + "A Horcrux has been destroyed, your immortality is slowly coming to an end.");
            }
        }

    }

}
