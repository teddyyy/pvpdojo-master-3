/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import com.pvpdojo.game.hg.userdata.HGUser;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class Reaper extends Kit {

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {
        Player attacker = (Player) e.getDamager();
        if (!e.isCancelled() && attacker.getItemInHand() != null && attacker.getItemInHand().getType() == getStartItems()[0].getType()) {
            ((LivingEntity) e.getEntity()).addPotionEffect(
                    new PotionEffect(PotionEffectType.WITHER, 3 * 20, 0 + HGUser.getUser(attacker).getKills()));
        }
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] { new ItemBuilder(Material.WOOD_HOE).name(CC.WHITE + "Death Scythe").enchant(Enchantment.DAMAGE_ALL, 2).build()};
    }

}
