/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.kits.listener;

import com.pvpdojo.bukkit.events.DojoPlayerSoupEvent;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.BlockInteractingKit;
import com.pvpdojo.game.kits.ClickKit;
import com.pvpdojo.game.kits.EntityInteractingKit;
import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.userdata.kit.KitGameUser;
import com.pvpdojo.util.bukkit.CC;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.*;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerStartItemConsumeEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class KitListener implements DojoListener {

    @EventHandler(priority = EventPriority.LOW)
    public void onDeath(PlayerDeathEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getEntity());

        for (Kit kit : user.getKitsDirect()) {

            List<ItemStack> startItems = new ArrayList<>(Arrays.asList(kit.getStartItems()));

            // Filter death drops for kit items / start items
            Iterator<ItemStack> itr = e.getDrops().iterator();
            while (itr.hasNext()) {
                ItemStack stack = itr.next();

                Iterator<ItemStack> startItemItr = startItems.iterator();

                while (startItemItr.hasNext()) {
                    ItemStack startStack = startItemItr.next();

                    if (stack.getType() == startStack.getType()) {
                        if (stack.getAmount() >= startStack.getAmount()) {
                            if (stack.getAmount() == startStack.getAmount()) {
                                itr.remove();
                            } else {
                                stack.setAmount(stack.getAmount() - startStack.getAmount());
                            }
                            startItemItr.remove();
                        } else {
                            startStack.setAmount(startStack.getAmount() - stack.getAmount());
                            itr.remove();
                        }
                    }
                }

                if (kit instanceof ClickKit) {
                    if (stack.getType() == ((ClickKit) kit).getClickItem().getType()) {
                        itr.remove();
                    }
                }
            }

            kit.onDeath(e);
        }

        if (e.getEntity().getKiller() != null) {
            KitGameUser<?> killer = KitGameUser.getUser(e.getEntity().getKiller());

            for (Kit kit : killer.getKits()) {
                kit.onKill(e);
            }

        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPreDeath(PlayerPreDeathEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getPlayer());

        for (Kit kit : user.getKits()) {
            kit.onPreDeath(e);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDeath(EntityDeathEvent e) {
        if (e.getEntity().getKiller() != null) {
            KitGameUser<?> killer = KitGameUser.getUser(e.getEntity().getKiller());

            if (killer instanceof HGUser) {
                killer = (HGUser) killer;
            }

            for (Kit kit : killer.getKits()) {
                kit.onKillEntity(e);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onQuit(PlayerQuitEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getPlayer());

        for (Kit kit : user.getKits()) {
            kit.onLeave(e.getPlayer());
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onItemSwitch(PlayerItemHeldEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getPlayer());

        for (Kit kit : user.getKits()) {
            kit.onItemSwitch(e);
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDrop(PlayerDropItemEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getPlayer());

        for (Kit kit : user.getKitsDirect()) {
            kit.onDrop(e);

            for (ItemStack startItem : kit.getStartItems()) {
                if (e.getItemDrop().getItemStack().isSimilar(startItem)) {
                    if (!e.getPlayer().getInventory().containsAtLeast(startItem, startItem.getAmount() + 1)) {
                        e.setCancelled(true);
                        user.sendMessage(CC.RED + "You cannot drop your kit items");
                    }
                }
            }

            if (kit instanceof ClickKit) {
                if (e.getItemDrop().getItemStack().isSimilar(((ClickKit) kit).getClickItem()) && !e.getPlayer().getInventory().containsAtLeast(((ClickKit) kit).getClickItem(), 2)) {
                    e.setCancelled(true);
                    user.sendMessage(CC.RED + "You cannot drop your kit items");
                }
            }
        }

    }

    @EventHandler(priority = EventPriority.LOW)
    public void onInteract(PlayerInteractEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getPlayer());

        for (Kit kit : user.getKitsDirect()) {

            boolean enabled = user.getKits().contains(kit);

            if (enabled) {
                kit.onInteract(e);
            }

            if (kit instanceof ClickKit && e.getItem() != null && e.getItem().getType() == ((ClickKit) kit).getClickItem().getType()) {

                if (enabled && !((ClickKit) kit).checkUsage(kit, user)) {
                    continue;
                }

                // BlockInteracting
                if (kit instanceof BlockInteractingKit) {
                    if (enabled && e.getClickedBlock() != null && (e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_BLOCK)) {
                        ((BlockInteractingKit) kit).activate(e);
                        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                            ((BlockInteractingKit) kit).onRightClick(e);
                        } else {
                            ((BlockInteractingKit) kit).onLeftClick(e);
                        }
                    }
                }

                if (e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_AIR) {
                    if (enabled) {
                        // Interacting
                        if (kit instanceof InteractingKit) {
                            ((InteractingKit) kit).activate(e);
                        }
                    }

                    if (!e.getItem().getType().isBlock()) {
                        e.setCancelled(true);
                    }
                }

                // Interacting
                if (e.getAction() != Action.PHYSICAL && kit instanceof InteractingKit && enabled) {
                    ((InteractingKit) kit).click(e.getPlayer());
                }

            }

        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockPlace(BlockPlaceEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getPlayer());

        for (Kit kit : user.getKitsDirect()) {
            if (user.getKits().contains(kit)) {
                kit.onPlace(e);
            }
            if (kit instanceof ClickKit && e.getItemInHand().getType() == ((ClickKit) kit).getClickItem().getType()) {
                e.setCancelled(true);
            }
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getPlayer());

        for (Kit kit : user.getKits()) {
            kit.onBreak(e);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockDamage(BlockDamageEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getPlayer());

        for (Kit kit : user.getKits()) {
            kit.onBlockDamage(e);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInteractEntity(PlayerInteractEntityEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getPlayer());

        for (Kit kit : user.getKits()) {
            kit.onInteractEntity(e);
            if (kit instanceof EntityInteractingKit && e.getPlayer().getItemInHand().getType() == ((ClickKit) kit).getClickItem().getType()) {
                if (((ClickKit) kit).checkUsage(kit, user)) {
                    ((EntityInteractingKit) kit).activate(e);
                }
            }
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onMove(PlayerMoveEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getPlayer());

        for (Kit kit : user.getKits()) {
            kit.onMove(e);
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onFish(PlayerFishEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getPlayer());

        for (Kit kit : user.getKits()) {
            kit.onFish(e);
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onToggleSneak(PlayerToggleSneakEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getPlayer());

        for (Kit kit : user.getKits()) {
            kit.onToggleSneak(e);
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerLeash(PlayerLeashEntityEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getPlayer());

        for (Kit kit : user.getKits()) {
            kit.onLeash(e);
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDamage(EntityDamageEvent e) {

        if (e.getEntity() instanceof Player) {
            KitGameUser<?> user = KitGameUser.getUser((Player) e.getEntity());

            for (Kit kit : user.getKits()) {
                kit.onDamage(e);
            }

        }

    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {

        if (e.getDamager() instanceof Player) {

            if (e.getEntity() instanceof Player) {
                KitGameUser<?> user = KitGameUser.getUser((Player) e.getEntity());

                for (Kit kit : user.getKits()) {
                    kit.onPlayerDamagedByPlayer(e);
                }
            }

            KitGameUser<?> attacker = KitGameUser.getUser((Player) e.getDamager());

            for (Kit kit : attacker.getKits()) {
                if (e.getEntity() instanceof Player) {
                    kit.onPlayerAttack(e);
                }
                kit.onEntityAttack(e);
            }

        }

        if (e.getEntity() instanceof Player) {
            KitGameUser<?> user = KitGameUser.getUser((Player) e.getEntity());

            for (Kit kit : user.getKits()) {
                kit.onPlayerDamagedByEntity(e);
            }
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInvClick(InventoryClickEvent e) {
        KitGameUser<?> user = KitGameUser.getUser((Player) e.getWhoClicked());

        ItemStack stack = e.getCursor() != null && e.getCursor().getType() != Material.AIR ? e.getCursor() : e.getCurrentItem();

        if (e.getHotbarButton() != -1 && stack.getType() == Material.AIR) {
            stack = e.getWhoClicked().getInventory().getItem(e.getHotbarButton());
        }

        for (Kit kit : user.getKitsDirect()) {

            if (stack != null && (e.getInventory().getType() != InventoryType.CRAFTING && e.getInventory().getType() != InventoryType.WORKBENCH || e.getSlotType() == SlotType.CRAFTING)) {
                if (kit instanceof ClickKit && stack.isSimilar(((ClickKit) kit).getClickItem())) {
                    e.setCancelled(true);
                }

                for (ItemStack startItem : kit.getStartItems()) {
                    if (stack.isSimilar(startItem)) {
                        int neededAmount = startItem.getAmount() + 1;

                        if (e.getHotbarButton() != -1 || e.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY) {
                            neededAmount += stack.getAmount();
                        }

                        if (!user.getPlayer().getInventory().containsAtLeast(startItem, neededAmount)) {
                            e.setCancelled(true);
                        }
                    }
                }
            }

            kit.onInvClick(e);
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInvClose(InventoryCloseEvent e) {
        KitGameUser<?> user = KitGameUser.getUser((Player) e.getPlayer());

        for (Kit kit : user.getKits()) {
            kit.onInvClose(e);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInvOpen(InventoryOpenEvent e) {
        KitGameUser<?> user = KitGameUser.getUser((Player) e.getPlayer());

        for (Kit kit : user.getKits()) {
            kit.onInvOpen(e);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPreCraft(PrepareItemCraftEvent e) {
        KitGameUser<?> user = KitGameUser.getUser((Player) e.getView().getPlayer());

        for (Kit kit : user.getKits()) {
            kit.onPrepareCraft(e);
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPickup(PlayerPickupItemEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getPlayer());

        for (Kit kit : user.getKits()) {
            kit.onPickUp(e);
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onProjectileLaunch(ProjectileLaunchEvent e) {
        if (e.getEntity().getShooter() instanceof Player) {
            KitGameUser<?> user = KitGameUser.getUser((Player) e.getEntity().getShooter());

            for (Kit kit : user.getKits()) {
                kit.onProjectileLaunch(e);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onProjectileHit(EntityDamageByEntityEvent e) {

        if (e.getDamager() instanceof Projectile && ((Projectile) e.getDamager()).getShooter() instanceof Player) {
            KitGameUser<?> user = KitGameUser.getUser((Player) ((Projectile) e.getDamager()).getShooter());

            for (Kit kit : user.getKits()) {
                kit.onProjectileHit(e);
            }

        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onShootBow(EntityShootBowEvent e) {

        if (e.getEntity() instanceof Player) {
            KitGameUser<?> user = KitGameUser.getUser((Player) e.getEntity());

            for (Kit kit : user.getKits()) {
                kit.onBowShoot(e);
            }

        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onConsume(PlayerItemConsumeEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getPlayer());

        for (Kit kit : user.getKits()) {
            kit.onConsume(e);
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onStartConsume(PlayerStartItemConsumeEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getPlayer());

        for (Kit kit : user.getKits()) {
            kit.onStartConsume(e);
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onExp(PlayerExpChangeEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getPlayer());

        for (Kit kit : user.getKits()) {
            kit.onExp(e);
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onSoup(DojoPlayerSoupEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getPlayer());

        for (Kit kit : user.getKits()) {
            kit.onSoup(e);
        }

    }

    @EventHandler
    public void onMobTarget(EntityTargetEvent e) {

        if (e.getTarget() instanceof Player && ((Player) e.getTarget()).isOnline()) {
            KitGameUser<?> user = KitGameUser.getUser((Player) e.getTarget());

            for (Kit kit : user.getKits()) {
                kit.onMobTarget(e);
            }
        }
    }

    @EventHandler
    public void onMobEntityTarget(EntityTargetLivingEntityEvent e) {

        if (e.getTarget() instanceof Player && ((Player) e.getTarget()).isOnline()) {
            KitGameUser<?> user = KitGameUser.getUser((Player) e.getTarget());

            for (Kit kit : user.getKits()) {
                kit.onMobEntityTarget(e);
            }
        }
    }

    @EventHandler
    public void onEnchantItemEvent(EnchantItemEvent e) {
        KitGameUser<?> user = KitGameUser.getUser(e.getEnchanter());

        for (Kit kit : user.getKits()) {
            kit.onEnchantItem(e);
        }

    }

}
