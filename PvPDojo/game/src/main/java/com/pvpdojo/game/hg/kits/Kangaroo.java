/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import java.util.Comparator;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import com.pvpdojo.game.hg.lang.HGKitMessageKeys;
import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.userdata.GameUser;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.userdata.User;

public class Kangaroo extends Kit implements InteractingKit {

    private boolean inair;
    private Player lastDamager;

    @Override
    public void onDamage(EntityDamageEvent e) {
        if (e.getCause() == DamageCause.FALL) {
            e.setDamage(e.getDamage() / 1.5);
        }
    }

    @Override
    public void onPlayerDamagedByPlayer(EntityDamageByEntityEvent e) {
        if (!e.isCancelled()) {
            lastDamager = (Player) e.getDamager();
        }
    }

    @Override
    public void onMove(PlayerMoveEvent e) {
        if (e.isOnGround()) {
            inair = false;
        }
    }

    @Override
    public void activate(PlayerInteractEvent player) {}

    @Override
    public void click(Player player) {
        if (!inair) {
            User user = User.getUser(player);

            if (!player.isSneaking()) {
                if (!user.isInCombat()) {
                    player.setVelocity(new Vector(0, 1.15, 0));
                } else {
                    user.sendMessage(HGKitMessageKeys.KANGAROO_COMBAT_UP);
                }
            } else {
                Vector handle = player.getEyeLocation().getDirection();
                handle.multiply(1.25F);
                handle.setY(0.5F);
                if (user.isInCombat() || (lastDamager != null && lastDamager.isOnline() && GameUser.getUser(lastDamager).isIngame())) {
                    int range = 30;
                    Player nearest = TrackerUtil.getNearbyIngame(player, range, range, range)
                                                .min(Comparator.comparingDouble(other -> other.getLocation().distance(player.getLocation())))
                                                .orElse(null);

                    if ((user.isInCombat() ? nearest != null : nearest == lastDamager) && nearest != null &&
                            player.getLocation().distanceSquared(nearest.getLocation()) < player.getLocation().add(handle.clone().multiply(5)).distanceSquared(nearest.getLocation())) {
                        user.sendMessage(HGKitMessageKeys.KANGAROO_COMBAT_DIRECTION);
                        player.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 15 * 20, 2));
                        return;
                    }
                }

                player.setVelocity(handle);
            }

            inair = true;
        }
    }

    @Override
    public boolean isSendCooldown() {
        return false;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.FIREWORK);
    }
}
