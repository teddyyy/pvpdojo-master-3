/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.lms.session;

import org.bukkit.entity.Player;

import com.pvpdojo.ServerType;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilities.gamemode.SilenceEffect;
import com.pvpdojo.abilityinfo.AbilityCollection;
import com.pvpdojo.bukkit.util.GuiItem;
import com.pvpdojo.challenges.ChallengeManager;
import com.pvpdojo.challenges.DojoChallenge;
import com.pvpdojo.game.lms.LMS;
import com.pvpdojo.game.lms.lang.LMSMessageKeys;
import com.pvpdojo.game.lms.userdata.LMSUser;
import com.pvpdojo.game.session.StageHandler;
import com.pvpdojo.game.session.StagedSession;
import com.pvpdojo.session.PastGameSession;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.arena.DojoGamesArena;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.entity.RankedArenaEntity;
import com.pvpdojo.userdata.KitData;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.StringUtils;

import net.md_5.bungee.api.chat.TextComponent;

public class LMSSession extends StagedSession {

    private int initialPlayers;

    public LMSSession(DojoGamesArena arena, ArenaEntity... participants) {
        super(arena, SessionType.DOJOGAMES, participants);
    }

    @Override
    public StageHandler getFirstStageHandler() {
        return new PreGame(this);
    }

    @Override
    public void endRanked(RankedArenaEntity winner, ArenaEntity... losers) {}

    @Override
    public void onLose(ArenaEntity loser) {}

    @Override
    public void onWin(ArenaEntity winner) {
        winner.sendMessage(CC.GREEN + "Congratulations, you won the tournament!");

        User user = User.getUser(winner.getPlayer());
        AbilityCollection collection = AbilityCollection.getRandomCollection();
        if (initialPlayers >= 15) {
            user.sendMessage(CC.GRAY + "You were awarded a " + GuiItem.getDisplayName(collection.getItem()));
            user.sendMessage(CC.GRAY + "You recieved " + CC.GREEN + "4000 dojo credits");
            user.getPersistentData().incrementMoney(4000);
            user.getPersistentData().addCollection(collection.getId());
        } else {
            user.sendMessage(CC.RED + "Rewards and challenges were reduced because the starting player count was below 15");
            user.sendMessage(CC.GRAY + "You recieved " + CC.GREEN + "2000 dojo credits");
            user.getPersistentData().incrementMoney(2000);
        }
        //ChallengeManager.get().callChallenge(user, DojoChallenge.T1_DAILY_DOJOGAMES, 1);
        if (this.initialPlayers >= 10) {
            ChallengeManager.get().callChallenge(user, DojoChallenge.T2_DAILY_DOJOGAMESTOP5, 1);
            ChallengeManager.get().callChallenge(user, DojoChallenge.T2_DAILY_DOJOGAMESTOP3, 1);
        } else {
            user.getPlayer().sendMessage(CC.RED + "You did not recieve the dojogames top 5 or top 3 challenge because there was less than 10 players");
        }
    }

    @Override
    public void postDeath(ArenaEntity entity) {
        getParticipants().remove(entity);
        if (getState() == SessionState.STARTED) {
            User user = (User) entity;
            Player killer = entity.getPlayer().getKiller();

            pendingSave.getLosers().add(new PastGameSession.PersistentArenaEntity(entity));
            pendingSave.getGameMessages().add(entity.getName()
                    + " was killed"
                    + (killer != null ? " by " + CC.BLUE + killer.getNick() : "")
                    + " after "
                    + StringUtils.getReadableSeconds(pendingSave.getDurationSeconds()));

            //ChallengeManager.get().callChallenge(user, DojoChallenge.T1_DAILY_DOJOGAMES, 1);
            if (this.initialPlayers >= 10) {
                if (this.getParticipants().size() <= 5) {
                    ChallengeManager.get().callChallenge(user, DojoChallenge.T2_DAILY_DOJOGAMESTOP5, 1);
                }
                if (this.getParticipants().size() <= 3) {
                    ChallengeManager.get().callChallenge(user, DojoChallenge.T2_DAILY_DOJOGAMESTOP3, 1);
                }
            } else {
                entity.sendMessage(CC.RED + "You did not recieve the dojogames top 5 or top 3 challenge because there was less than 10 players");
            }
            if (killer != null) {
                broadcast(LMSMessageKeys.KNOCKED_OUT, "{victim}", entity.getName(), "{killer}", killer.getNick());
            } else {
                broadcast(LMSMessageKeys.KNOCKED_OUT_UNKNOWN, "{victim}", entity.getName());
            }
            broadcast(LMSMessageKeys.PLAYERS_LEFT, "{amount}", String.valueOf(getParticipants().size()));
            super.postDeath(entity);
            ServerType.HUB.connect(entity.getPlayer());
            triggerEnd();
        }
        entity.setSession(null);
    }

    @Override
    public void handleMatchDetails(TextComponent tc) {

    }

    @Override
    public void setState(SessionState state) {
        super.setState(state);
        if (state == SessionState.STARTED) {
            initialPlayers = getParticipants().size();
        }
    }

    @Override
    public void prepareEntity(ArenaEntity entity) {
        Player player = entity.getPlayer();

        if (LMS.inst().getSettings().isAbilities()) {
            AbilityPlayer champion = AbilityPlayer.get(player);

            KitData kitData = LMSUser.getUser(player).getKitCache();
            if (kitData != null) {
                champion.selectChampion(kitData);
            } else {
                champion.selectChampion(-1);
            }
            champion.applyEffect(new SilenceEffect(20));
        }
    }

    @Override
    public boolean checkEndCondition() {
        return getParticipants().size() <= 1;
    }

    @Override
    public boolean isPersistent() {
        return true;
    }
}
