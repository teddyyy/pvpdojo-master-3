/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import java.util.Comparator;

import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FishHook;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.lang.HGKitMessageKeys;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class Neo extends ComplexKit {

    private static final String NEO_IGNORE = "neo_ignore";

    private int dodgeChance = 0;

    @Override
    public void onPlayerDamagedByPlayer(EntityDamageByEntityEvent e) {
        if (!e.isCancelled() && PlayerUtil.isRealHit((LivingEntity) e.getEntity()) && PvPDojo.RANDOM.nextInt(200) <= dodgeChance++) {
            e.setCancelled(true);
            dodgeChance = 0;
            ((LivingEntity) e.getEntity()).setNoDamageTicks(20);
            e.getEntity().getWorld().playSound(e.getEntity().getLocation(), Sound.ENDERDRAGON_WINGS, 5, 1);
            HGUser.getUser((Player) e.getDamager()).sendMessage(HGKitMessageKeys.NEO_DODGE);
        }
    }

    @Override
    public void onDamage(EntityDamageEvent e) {
        if (e.getCause() == DamageCause.PROJECTILE) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onProjectileLaunch(ProjectileLaunchEvent e) {
        Projectile projec = e.getEntity();

        if (projec.getShooter() instanceof Entity) {
            projec.setMetadata(NEO_IGNORE, new FixedMetadataValue(Game.inst(), ((Entity) projec.getShooter()).getUniqueId()));
            checkProjectile(projec);
            new BukkitRunnable() {

                public void run() {

                    if (projec.isDead() || !projec.isValid() || projec.isOnGround()) {
                        cancel();
                        return;
                    }

                    checkProjectile(projec);

                }
            }.runTaskTimer(Game.inst(), 0, 1);
        }

    }

    private void checkProjectile(Projectile projec) {
        TrackerUtil.getNearbyIngame(projec.getNearbyEntities(1.9, 2, 1.9))
                   .min(Comparator.comparingDouble(player -> player.getLocation().distance(projec.getLocation())))
                   .ifPresent(player -> {
                       if (!projec.getMetadata(NEO_IGNORE).get(0).value().equals(player.getUniqueId()) && KitUtil.hasKit(HGUser.getUser(player), HGKit.NEO)) {
                           if (!(projec instanceof FishHook)) {
                               projec.setVelocity(projec.getVelocity().multiply(-1));
                               projec.setShooter(player);
                           } else {
                               projec.remove();
                           }
                           projec.setMetadata(NEO_IGNORE, new FixedMetadataValue(Game.inst(), player.getUniqueId()));
                       }
                   });
    }

}
