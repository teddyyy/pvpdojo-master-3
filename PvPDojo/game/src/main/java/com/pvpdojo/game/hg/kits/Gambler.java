/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.util.RandomInventory;
import com.pvpdojo.util.RandomInventory.RandomItem;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.function.Consumer;

import static com.pvpdojo.util.StreamUtils.negate;
import static com.pvpdojo.util.bukkit.ItemUtil.addItemWhenFree;

public class Gambler extends Kit {

    public static final RandomInventory<Consumer<Player>> GAMBLE = new RandomInventory<Consumer<Player>>(RandomItem::getItem)
            .addItem(player -> {
                player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 20, 1));
                player.sendMessage(CC.GREEN + "You won speed!");
            }, 60)
            .addItem(player -> {
                player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20 * 20, 0));
                player.sendMessage(CC.GREEN + "You won strength!");
            }, 60)
            .addItem(player -> {
                player.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 20 * 20, 0));
                player.sendMessage(CC.GREEN + "You won fire resistance!");
            }, 60)
            .addItem(player -> {
                player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * 20, 1));
                player.sendMessage(CC.GREEN + "You won regeneration!");
            }, 60)
            .addItem(player -> {
                player.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION, 20 * 20, 1));
                player.sendMessage(CC.GREEN + "You won saturation!");
            }, 60)
            .addItem(player -> {
                player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20 * 20, 0));
                player.sendMessage(CC.GREEN + "You won resistance!");
            }, 60)
            .addItem(player -> {
                player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 20 * 20, 3));
                player.sendMessage(CC.GREEN + "You won jump boost!");
            }, 60)
            .addItem(player -> {
                player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20 * 20, 0));
                player.sendMessage(CC.RED + "You won slowness...");
            }, 60)
            .addItem(player -> {
                player.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 20 * 20, 0));
                player.sendMessage(CC.RED + "You won wither...");
            }, 60)
            .addItem(player -> {
                player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 20 * 20, 9));
                player.sendMessage(CC.RED + "You won mining fatigue...");
            }, 60)
            .addItem(player -> {
                player.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 20 * 20, 0));
                player.sendMessage(CC.RED + "You won weakness...");
            }, 60)
            .addItem(player -> {
                player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 10 * 20, 1));
                player.sendMessage(CC.RED + "You won poison...");
            }, 60)
            .addItem(player -> {
                player.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 20 * 20, 1));
                player.sendMessage(CC.RED + "You won hunger...");
            }, 60)
            .addItem(player -> {
                player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 20 * 10, 0));
                player.sendMessage(CC.RED + "You won nausea...");
            }, 60)
            .addItem(player -> {
                player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 20 * 20, 1));
                player.sendMessage(CC.GREEN + "You won invisibility!");
            }, 60)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.WOOD, 32));
                player.sendMessage(CC.GREEN + "You won wood!");
            }, 25)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.SEEDS, 16));
                player.sendMessage(CC.RED + "You won seeds...");
            }, 25)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.GOLD_INGOT));
                player.sendMessage(CC.GREEN + "You won a gold ingot!");
            }, 25)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.IRON_INGOT));
                player.sendMessage(CC.GREEN + "You won an iron ingot!");
            }, 15)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.DIAMOND));
                player.sendMessage(CC.GREEN + "You won a diamond!");
            }, 10)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.ENDER_PEARL));
                player.sendMessage(CC.GREEN + "You won an enderpearl!");
            }, 25)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.TNT),
                        new ItemBuilder(Material.FLINT_AND_STEEL).durability((short) (Material.FLINT_AND_STEEL.getMaxDurability() - 10)).build());
                player.sendMessage(CC.GREEN + "You won TNT and a flint and steel!");
            }, 25)
            .addItem(player -> {
                if (player.getLocation().getBlock().getType() != Material.BEDROCK) {
                    player.getLocation().getBlock().setType(Material.WEB);
                    player.sendMessage(CC.RED + "A cobweb has been placed under you!");
                }
            }, 30)
            .addItem(player -> {
                player.teleport(TrackerUtil.getIngame().filter(negate(player::equals)).findAny().orElse(player));
                player.sendMessage(CC.GREEN + "You have been teleported to a random player!");
            }, 10)
            .addItem(player -> {
                HGUser.getUser(player).getStats().addMoney(10);
                player.sendMessage(CC.GREEN + "You earned 10 credits.");
            }, 60)
            .addItem(player -> {
                HGUser.getUser(player).getStats().addMoney(1000);
                player.sendMessage(CC.GREEN + "You earned 1000 credits!");
            }, 1)
            .addItem(player -> player.performCommand("kill"), 1)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.DIAMOND_SWORD), new ItemStack(Material.DIAMOND_HELMET),
                        new ItemStack(Material.DIAMOND_CHESTPLATE), new ItemStack(Material.DIAMOND_LEGGINGS), new ItemStack(Material.DIAMOND_BOOTS));
                player.sendMessage(CC.GOLD + "JACKPOT! You won a full diamond set!");
            }, 1)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.IRON_SWORD));
                player.sendMessage(CC.GREEN + "You won an iron sword!");
            }, 7)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.GOLD_PICKAXE));
                player.sendMessage(CC.GREEN + "You won a gold pickaxe!");
            }, 20)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.GOLD_AXE));
                player.sendMessage(CC.GREEN + "You won a gold axe!");
            }, 20)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.IRON_SWORD), new ItemStack(Material.IRON_HELMET),
                        new ItemStack(Material.IRON_CHESTPLATE), new ItemStack(Material.IRON_LEGGINGS), new ItemStack(Material.IRON_BOOTS));
                player.sendMessage(CC.GREEN + "You won a full iron set!");
            }, 4)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.STONE_SWORD), new ItemStack(Material.CHAINMAIL_HELMET),
                        new ItemStack(Material.CHAINMAIL_CHESTPLATE), new ItemStack(Material.CHAINMAIL_LEGGINGS), new ItemStack(Material.CHAINMAIL_BOOTS));
                player.sendMessage(CC.GREEN + "You won a full chain set!");
            }, 8)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.GOLD_SWORD), new ItemStack(Material.GOLD_HELMET),
                        new ItemStack(Material.GOLD_CHESTPLATE), new ItemStack(Material.GOLD_LEGGINGS), new ItemStack(Material.GOLD_BOOTS));
                player.sendMessage(CC.GREEN + "You won a full gold set!");
            }, 10)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.WOOD_SWORD), new ItemStack(Material.LEATHER_HELMET),
                        new ItemStack(Material.LEATHER_CHESTPLATE), new ItemStack(Material.LEATHER_LEGGINGS), new ItemStack(Material.LEATHER_BOOTS));
                player.sendMessage(CC.GREEN + "You won a full leather set!");
            }, 18)
            .addItem(player -> {
                player.getInventory().setArmorContents(null);
                player.sendMessage(CC.RED + "Your armor has been removed!");
            }, 3)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.RED_MUSHROOM, 4), new ItemStack(Material.BOWL, 4),
                        new ItemStack(Material.BROWN_MUSHROOM, 4));
                player.sendMessage(CC.GREEN + "You won a bit of recraft!");
            }, 40)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.RED_MUSHROOM, 8), new ItemStack(Material.BOWL, 8),
                        new ItemStack(Material.BROWN_MUSHROOM, 8));
                player.sendMessage(CC.GREEN + "You won a bit of recraft!");
            }, 30)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.RED_MUSHROOM, 16), new ItemStack(Material.BOWL, 16),
                        new ItemStack(Material.BROWN_MUSHROOM, 16));
                player.sendMessage(CC.GREEN + "You won a bit of recraft!");
            }, 20)
            .addItem(player -> {
                player.getInventory().setHelmet(new ItemStack(Material.PUMPKIN));
                player.sendMessage(CC.RED + "Spooky...");
            }, 15)
            .addItem(player -> {
                player.getWorld().spawn(player.getLocation(), Creeper.class);
                player.sendMessage(CC.RED + "tssssss...");
            }, 2)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.EXP_BOTTLE));
                player.sendMessage(CC.GREEN + "You won an exp bottle!");
            }, 10)
            .addItem(player -> {
                addItemWhenFree(player, new ItemStack(Material.BUCKET));
                player.sendMessage(CC.GREEN + "You won a bucket!");
            }, 20);

    @Override
    public void onInteract(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getClickedBlock().getType() == Material.WOOD_BUTTON) {
            if (getKitCooldown().isOnCooldown()) {
                getKitCooldown().sendCooldown(HGUser.getUser(e.getPlayer()));
            } else {
                getKitCooldown().use();
                GAMBLE.generateRandomItem().accept(e.getPlayer());
            }
        }
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[]{new ItemStack(Material.WOOD_BUTTON)};
    }

    @Override
    public int getCooldown() {
        return 30_000;
    }
}
