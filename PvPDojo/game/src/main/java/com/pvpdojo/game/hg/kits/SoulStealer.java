/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import java.util.Arrays;

import com.pvpdojo.util.bukkit.CC;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_7_R4.entity.disguise.CraftCreatureDisguise;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.disguise.Disguise;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.PlayerPreDeathEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.lang.HGKitMessageKeys;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.ItemUtil;

public class SoulStealer extends Kit {

    private boolean respawning, hasHit, hasKill;
    public ItemStack[] items, armor;

    @Override
    public void onPreDeath(PlayerPreDeathEvent e) {
        if (!respawning) {

            // Reset variables
            e.setCancelled(true);
            respawning = true;
            hasHit = false;
            hasKill = false;

            // Save their items
            items = ItemUtil.cloneContents(e.getPlayer().getInventory().getContents());
            armor = ItemUtil.cloneContents(e.getPlayer().getInventory().getArmorContents());

            // Clear inv
            e.getPlayer().getInventory().clear();
            e.getPlayer().getInventory().setArmorContents(null);

            if (e.getPlayer().getOpenInventory().getType() == InventoryType.CRAFTING) {
                e.getPlayer().getOpenInventory().getTopInventory().clear();
            }

            // Stealer Equipment
            e.getPlayer().setHealth(e.getPlayer().getMaxHealth());
            if (HG.inst().getSession().getCurrentHandler().getTimeSeconds() > 8 * 60) {
                e.getPlayer().getInventory().addItem(new ItemStack(Material.DIAMOND_SWORD));
            } else {
                e.getPlayer().getInventory().addItem(new ItemStack(Material.IRON_SWORD));
            }
            e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 20 * 20, 0));
            e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 20, 1), true);
            disguise(e.getPlayer());

            if (e.getPlayer().getKiller() != null) {
                Vector vec = e.getPlayer().getKiller().getLocation().getDirection().multiply(-1);
                Location loc = e.getPlayer().getKiller().getLocation().add(vec);

                if (!loc.getBlock().getType().equals(Material.AIR)) {
                    loc = e.getPlayer().getKiller().getLocation();
                }

                loc.setPitch(e.getPlayer().getLocation().getPitch());
                loc.setYaw(e.getPlayer().getKiller().getLocation().getYaw());

                e.getPlayer().teleport(loc);
            }

            PvPDojo.newChain()
                   .delay(10 * 20)
                   .abortIfNot(obj -> respawning)
                   .sync(() -> {
                       if (!hasHit) {
                           kill(e.getPlayer());
                       }
                   })
                   .abortIfNot(obj -> hasHit)
                   .delay(10 * 20)
                   .abortIfNot(obj -> respawning)
                   .sync(() -> {
                       if (!hasKill) {
                           kill(e.getPlayer());
                       } else {
                           respawning = false;
                       }
                   })
                   .execute();

            e.getPlayer().getWorld().playSound(e.getPlayer().getLocation(), Sound.GHAST_SCREAM2, 1, 0.01F);
            HG.inst().getSession().broadcast(HGKitMessageKeys.SOULSTEALER_MODE);
        }
    }

    @Override
    public void onDeath(PlayerDeathEvent e) {
        if (respawning) {
            e.getDrops().clear();
            e.getDrops().addAll(Arrays.asList(items));
            e.getDrops().addAll(Arrays.asList(armor));
            respawning = false;
        }
    }

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {
        if (!e.isCancelled()) {
            hasHit = respawning;
        }
    }

    @Override
    public void onKill(PlayerDeathEvent e) {
        hasKill = respawning;

        if (respawning) {
            e.getEntity().getKiller().removePotionEffect(PotionEffectType.INVISIBILITY);
            e.getEntity().getKiller().removePotionEffect(PotionEffectType.SPEED);
            e.getEntity().getKiller().getInventory().setArmorContents(armor);
            e.getEntity().getKiller().getInventory().setContents(items);
            reset(e.getEntity().getKiller());
        }
    }

    @Override
    public void onLeave(Player player) {
        if (respawning) {
            kill(player);
        }
    }


    @Override
    public void onDrop(PlayerDropItemEvent e) {
        if (respawning) {
            e.setCancelled(true);
        }
    }

    public void kill(Player player) {
        reset(player);
        BukkitUtil.callEntityDamageEvent(player, DamageCause.ENTITY_ATTACK, 100D);
        Bukkit.getPluginManager().callEvent(new PlayerPreDeathEvent(player));
        player.setHealth(0D);
    }

    public void disguise(Player player) {
        if (player.getDisguise() == null) {
            Disguise disguise = new CraftCreatureDisguise(player, EntityType.ZOMBIE);
            player.setDisguise(disguise);
        }

    }

    public void reset(Player player) {
        if (player.getDisguise() != null) {
            player.setDisguise(null);
        }
    }

}
