/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.kits;

import java.util.function.Supplier;

import org.bukkit.inventory.ItemStack;

import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder.ItemEditor;

import co.aikar.locales.MessageKeyProvider;

public interface KitProvider {

    Supplier<Kit> getSupplier();

    MessageKeyProvider getDescription();

    ItemStack getIcon();

    default ItemStack getIcon(User user) {
       return new ItemEditor(getIcon()).lore("")
               .appendLore(LocaleMessage.of(getDescription()).transformer(str -> CC.GRAY + str).get(user))
               .build();
    }

    String name();

    default String getName() {
        return StringUtils.getReadableEnumName(name(), true);
    }

}
