/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.cloud;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.DojoPlayerJoinEvent;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.cloud.CloudUtil;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.userdata.GameUser;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.StringUtils;

import redis.clients.jedis.JedisPubSub;

public class GenericCloudResponder extends JedisPubSub implements DojoListener {

    private static final GenericCloudResponder INST = new GenericCloudResponder();

    public static GenericCloudResponder inst() {
        return INST;
    }

    private Thread cloudThread;
    private Cache<UUID, Boolean> currentlyConnecting = CacheBuilder.newBuilder().expireAfterWrite(2, TimeUnit.SECONDS).build();

    private GenericCloudResponder() {}

    @Override
    public void onMessage(String channel, String message) {
        try {

            String[] split = StringUtils.PIPE.split(message);

            if (Integer.valueOf(split[0]) == BungeeSync.REQUEST && split[1].equals(PvPDojo.get().getNetworkPointer())) {
                UUID uuid = UUID.fromString(split[2]);

                currentlyConnecting.cleanUp();
                boolean allow = getCurrentlyPlaying() + currentlyConnecting.size() < Game.inst().getGame().getSettings().getMaxPlayers();

                if (allow) {
                    currentlyConnecting.put(uuid, Boolean.TRUE);
                }

                Redis.get().publish(CloudUtil.getChannel(Game.inst().getGame().getSession().getType()), BungeeSync.RESPONSE + "|" + uuid + "|" + allow);
            }

        } catch (Throwable throwable) {
            Log.exception(throwable);
        }
    }

    private long getCurrentlyPlaying() {
        return Bukkit.getOnlinePlayers().stream()
                     .map(GameUser::getUser)
                     .filter(GameUser::isIngame)
                     .count();
    }

    @EventHandler
    public void onJoin(DojoPlayerJoinEvent e) {
        currentlyConnecting.invalidate(e.getPlayer().getUniqueId());
    }

    public void start() {
        register();
        cloudThread = new Thread(() -> Redis.get().subscribe(this, CloudUtil.getChannel(Game.inst().getGame().getSession().getType())));
        cloudThread.start();
    }

    public void shutdown() {
        unregister();
        unsubscribe();
        try {
            cloudThread.join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            Log.exception(e);
        }
    }

}
