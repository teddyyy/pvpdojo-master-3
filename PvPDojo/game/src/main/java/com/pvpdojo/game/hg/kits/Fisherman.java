/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import java.time.Duration;

import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.kits.sidekits.Titan;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.Throttle;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class Fisherman extends Kit {

    private final Throttle throttle = new Throttle(3, Duration.ofSeconds(30));

    @Override
    public void onFish(PlayerFishEvent e) {
        if (e.getPlayer().getVehicle() != null) {
            e.getPlayer().sendMessage(CC.RED + "You cannot fish while riding something");
            return;
        }
        if (e.getCaught() instanceof Player) {
            if (!throttle.throttle()) {
                e.getCaught().teleport(e.getPlayer());
                Titan kit = null;
                if (HGUser.getUser(e.getPlayer()).getSideKit().getKit() instanceof Titan) {
                    kit = (Titan) HGUser.getUser(e.getPlayer()).getSideKit().getKit();
                }
                if (HGUser.getUser(e.getPlayer()).isCancelNextFall() || (kit != null && kit.titan)) {
                    HGUser.getUser((Player) e.getCaught()).cancelNextFall();
                }
                if (kit != null && (!kit.getKitCooldown().isOnCooldown() || kit.getKitCooldown().isOnCooldown() && kit.getKitCooldown().getSecondsLeft() <= 5)) {
                    kit.getKitCooldown().use();
                    e.getPlayer().sendMessage(CC.RED + "Your Titan ability has been temporarily disabled");
                }
            } else {
                e.getPlayer().sendMessage(CC.RED + "You may only use this 3 times within 30 seconds");
            }
        }
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[]{new ItemBuilder(Material.FISHING_ROD).unbreakable().build()};
    }
}
