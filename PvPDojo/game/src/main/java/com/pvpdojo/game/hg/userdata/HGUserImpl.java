/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.userdata;

import co.aikar.timings.Timing;
import co.aikar.timings.Timings;
import com.google.common.collect.Lists;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.kits.Gladiator;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.kits.persistent.PersistentKits;
import com.pvpdojo.game.hg.lang.HGMessageKeys;
import com.pvpdojo.game.hg.session.BonusFeast;
import com.pvpdojo.game.hg.session.DefaultFeast;
import com.pvpdojo.game.hg.session.EHG;
import com.pvpdojo.game.hg.session.HGSession;
import com.pvpdojo.game.hg.session.Pit;
import com.pvpdojo.game.hg.team.HGTeam;
import com.pvpdojo.game.hg.userdata.stats.HGStats;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.kits.KitHolder;
import com.pvpdojo.game.lang.GameMessageKeys;
import com.pvpdojo.game.session.StageHandler;
import com.pvpdojo.game.userdata.kit.KitGameUserImpl;
import com.pvpdojo.game.userdata.kit.SingleKitGameUserImpl;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.settings.UserSettings.ScoreboardMode;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.userdata.sidebar.SidebarModule;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.TimeUtil;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.CC;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class HGUserImpl extends SingleKitGameUserImpl<HGKit> implements HGUser {

    private static final Timing scoreboardTiming = Timings.of(Game.inst(), "HG Sidebar");

    public HGUserImpl(UUID uuid) {
        super(uuid);
        this.stats = new HGStats(this);
        this.persistentKits = new PersistentKits(getPersistentData());
        this.bonusKit = HG.inst().getSession().getBonusKit(uuid);
    }

    @Getter
    @Setter
    private int kills, invincibleSeconds, disabledKitsSeconds;
    @Getter
    private HGStats stats;
    @Getter
    private PersistentKits persistentKits;
    @Getter
    private HGKit bonusKit;
    @Getter
    @Setter
    private KitHolder<HGKit> sideKit;
    @Getter
    @Setter
    private HGTeam HGTeam;
    @Getter
    @Setter
    private boolean respawned, respawning;
    @Getter
    @Setter
    private boolean addKitItem;

    // Teaming nerf: reduced damage when hit by multiple players
    private long lastTimeDamaged;
    private UUID lastDamager;
    private long reducedDamageTill;

    @Override
    public void run() {
        super.run();

        if (getSession() != null && getSession().getCurrentHandler() instanceof EHG) {
            checkRecraft();
        }

        if (disabledKitsSeconds > 0) {
            disabledKitsSeconds--;
        }

        if (invincibleSeconds > 0) {
            invincibleSeconds--;
        }

        int border = HG.inst().getSettings().getBorder();
        if (Math.abs(getPlayer().getLocation().getX()) > border + 1 || Math.abs(getPlayer().getLocation().getZ()) > border + 1) {
            if (getSession() != null && getSession().getState() == SessionState.STARTED) {
                if (!BukkitUtil.callEntityDamageEvent(getPlayer(), DamageCause.CUSTOM, 6).isCancelled()) {
                    getPlayer().damage(6);
                }
                sendMessage("" + CC.RED + CC.BOLD + "ATTENTION " + CC.GOLD + "you're outside the border");
            } else if (getSession() != null && getSession().getState() == SessionState.PREGAME) {
                getSession().prepareEntity(this);
            }
        }
        StageHandler<?> currentHandler = HG.inst().getSession().getCurrentHandler();

        if (currentHandler instanceof Pit && ((Pit) currentHandler).isTeleported()) {
            if (getPlayer().getLocation().getY() > 30) {
                if (!BukkitUtil.callEntityDamageEvent(getPlayer(), DamageCause.CUSTOM, 4).isCancelled()) {
                    getPlayer().damage(4);
                }
            }
        } else if (getPlayer().getLocation().getY() > 140) {
            if (!BukkitUtil.callEntityDamageEvent(getPlayer(), DamageCause.CUSTOM, 4).isCancelled()) {
                getPlayer().damage(4);
            }
        }

        scoreboardTiming.startTiming();
        updateScoreBoard();
        scoreboardTiming.stopTiming();
    }

    private final SidebarModule sidebarModule = new SidebarModule();
    private HGSession session = HG.inst().getSession();

    @Override
    public void updateScoreBoard() {
        HGSession session = HG.inst().getSession();

        if (isDataLoaded()) {
            if (getSettings().getHGSettings().getScoreboardMode() == ScoreboardMode.DEFAULT) {

                getSidebar().setTitle(CC.DARK_PURPLE.toString() + CC.BOLD + "PvPDojo");

                CC color = session.getState() == SessionState.PREGAME ? CC.RED : session.getState() == SessionState.INVINCIBILITY ? CC.YELLOW : CC.GREEN;
                GameMessageKeys messageKey = session.getState() == SessionState.PREGAME ? GameMessageKeys.SIDEBAR_STARTING : session.getState() == SessionState.INVINCIBILITY ?
                        GameMessageKeys.SIDEBAR_INVINCIBLE : GameMessageKeys.SIDEBAR_GAME_TIME;

                sidebarModule.append(LocaleMessage.of(messageKey).transformer(str -> color + str).get(this))
                        .append("  " + StringUtils.getReadableSeconds(session.getCurrentHandler().getTimeSeconds()));

                if (!isSpectator()) {
                    sidebarModule.append("")
                            .append(CC.GRAY + "Kit: " + CC.WHITE + (getKitHolder() != null ? getKitHolder().getKitProvider().getName() : "None"));
                    sidebarModule.append(CC.GRAY + "Side Kit: " + CC.WHITE + (getSideKit() != null ? getSideKit().getKitProvider().getName() : "None"));
                }


                if (session.getState() != SessionState.PREGAME) {
                    if (!isSpectator() || getKills() > 0) {
                        if (isSpectator()) {
                            sidebarModule.append("");
                        }
                        sidebarModule.append(CC.GRAY + "Kills: " + CC.WHITE + getKills());
                    }
                }

                sidebarModule.append("")
                        .append(LocaleMessage.of(HGMessageKeys.SIDEBAR_PLAYERS).get(this))
                        .append("  " + session.getPlayersLeft() + "/" + session.getInitialPlayerCount())
                        .append("");

                if (session.getCurrentHandler() instanceof DefaultFeast) {
                    int currentCount = ((DefaultFeast) session.getCurrentHandler()).getFeastCountdown().getCurrentCount();
                    Location loc = HG.inst().getFeastLocation();

                    sidebarModule.append(CC.GOLD + "Feast")
                            .append("  " + (currentCount <= 0 ? LocaleMessage.of(HGMessageKeys.SIDEBAR_FEAST_BEGUN).get(this)[0] : StringUtils.getReadableSeconds(currentCount)))
                            .append("  " + LocaleMessage.of(HGMessageKeys.SIDEBAR_AT).get(this)[0] + " " + BlockUtil.asString(loc.getBlock()))
                            .append("");

                } else if (session.getCurrentHandler() instanceof BonusFeast &&
                        ((BonusFeast) session.getCurrentHandler()).isSpawned()) {
                    sidebarModule.append(CC.GOLD + "Bonus Feast")
                            .append("  " + ((BonusFeast) session.getCurrentHandler()).getBiome())
                            .append("");
                } else if (session.getCurrentHandler() instanceof Pit) {
                    if (((Pit) session.getCurrentHandler()).getPitCountdown() == null || ((Pit) session.getCurrentHandler()).getPitCountdown().getCurrentCount() <= 0) {
                        sidebarModule.append(LocaleMessage.of(HGMessageKeys.SIDEBAR_GAME_END).get(this))
                                .append("  in " + StringUtils.getReadableSeconds(HG.inst().getSettings().getGameEnd() - session.getCurrentHandler().getTimeSeconds()));
                    } else {
                        sidebarModule.append(CC.GOLD + "Pit")
                                .append("  in " + StringUtils.getReadableSeconds(((Pit) session.getCurrentHandler()).getPitCountdown().getCurrentCount()));
                    }

                    sidebarModule.append("");
                }

                sidebarModule.append(CC.ITALIC + Game.inst().getSubdomain() + ".mc-hg.eu");

                getSidebar().set(sidebarModule);

            } else if (getSettings().getHGSettings().getScoreboardMode() == ScoreboardMode.SMALL) {
                if (getSidebar().getSize() > (session.getState() == SessionState.STARTED ? 0 : 1)) {
                    getSidebar().removeAll();
                }
            } else if (getSettings().getHGSettings().getScoreboardMode() == ScoreboardMode.NONE) {
                getSidebar().removeAll();
            }
        }
    }

    @Override
    public void setKit(HGKit kitProvider) {
        super.getKits().clear();
        if (kitProvider != null) {
            addKit(kitProvider);
        }
    }

    @Override
    public List<Kit> getKits() {
        if (disabledKitsSeconds > 0) {
            return Collections.emptyList();
        }
        if (sideKit != null) {
            List<Kit> kits = Lists.newArrayList(super.getKits());
            kits.add(sideKit.getKit());
            return kits;
        }
        return super.getKits();
    }

    @Override
    public List<Kit> getKitsDirect() {
        if (sideKit != null) {
            List<Kit> kits = Lists.newArrayList(super.getKits());
            kits.add(sideKit.getKit());
            return kits;
        }
        return super.getKits();
    }

    @Override
    public void addKill() {
        kills++;
    }

    @Override
    public boolean isInvincible() {
        return invincibleSeconds > 0;
    }

    @Override
    public boolean canBuild() {
        return super.canBuild() || isIngame();
    }

    @Override
    public boolean canRespawn() {
        int respawnTime = getRank().inheritsRank(Rank.BLACKBELT) ? 300 : getRank().inheritsRank(Rank.GREENBELT) ? 180 : 0;
        return !isLoggedOut() && HG.inst().getSettings().isRespawn() && HG.inst().getSession().getCurrentHandler().getTimeSeconds() < respawnTime && !isRespawned();
    }

    @Override
    public boolean isInCombat(int ignoreAmount) {
        return super.isInCombat(ignoreAmount) || Gladiator.MATCH_MAP.containsKey(getUUID());
    }

    @Override
    public void disableKits(int seconds) {
        disabledKitsSeconds += seconds;
    }

    @Override
    public Location respawn() {
        int border = HG.inst().getSettings().getBorder() - 25;
        int x = PvPDojo.RANDOM.nextInt(border * 2 + 1) - border, z = PvPDojo.RANDOM.nextInt(border * 2 + 1) - border;
        getSession().getCurrentlyDead().remove(this);
        setRespawning(false);
        setRespawned(true);
        setAddKitItem(true);
        setHotbarGui(TrackerHotbarGUI.get());
        cancelNextFall();
        return BlockUtil.getHighestBlock(getPlayer().getWorld(), x, z).getLocation().add(0.5, 10, 0.5);
    }

    private boolean recraftMessageSent;

    private void checkRecraft() {
        int recraft = countMaterial(Material.INK_SACK) + Math.min(countMaterial(Material.RED_MUSHROOM), countMaterial(Material.BROWN_MUSHROOM));
        if (recraft >= 97) {
            getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 2 * 20, 5), true);
            if (!recraftMessageSent) {
                recraftMessageSent = true;
                getPlayer().sendMessage(CC.RED + "Carrying so much recraft in your pockets is weakening you, get rid of it to regain your strength!");
            }
        } else {
            recraftMessageSent = false;
        }
    }

    private int countMaterial(Material material) {
        int sum = 0;
        for (ItemStack stack : getPlayer().getInventory()) {
            if (stack != null && stack.getType() == material) {
                sum += stack.getAmount();
            }
        }
        return sum;
    }


    @Override
    public boolean addKitItemAfterRespawn() {
        return addKitItem;
    }

    @Override
    public void setAddKitItemAfterRespawn(boolean add) {
        this.addKitItem = add;
    }

    @Override
    public void registerDamage(UUID damager) {
        if (lastDamager != null && !damager.equals(lastDamager)) {
            if (!TimeUtil.hasPassed(lastTimeDamaged, TimeUnit.SECONDS, 3)) {
                reducedDamageTill = System.currentTimeMillis() + 3 * 1000;
            }
        }
        lastTimeDamaged = System.currentTimeMillis();
        lastDamager = damager;
    }

    @Override
    public boolean receivesReducedDamage() {
        return System.currentTimeMillis() <= reducedDamageTill;
    }

    @Override
    public StringBuilder generatePrefix(StringBuilder prefix, User td) {
        if (HG.inst().getSettings().getTeamSize() > 1) {
            prefix.append(getHGTeam() == null ? "" : "[" + CC.YELLOW + getHGTeam().getName() + CC.RESET + "] ");
        }
        return super.generatePrefix(prefix, td);
    }
}
