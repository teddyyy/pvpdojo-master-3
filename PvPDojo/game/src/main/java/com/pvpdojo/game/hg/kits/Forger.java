/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;

import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.kits.KitHolder;
import com.pvpdojo.game.userdata.kit.KitGameUser;

import java.util.Arrays;

public class Forger extends ComplexKit {

    private static final ShapelessRecipe[] RECIPES;

    static {
        RECIPES = new ShapelessRecipe[] {
                createRecipe(Material.IRON_ORE, Material.IRON_INGOT),
                createRecipe(Material.GOLD_ORE, Material.GOLD_INGOT),
                createRecipe(Material.SAND, Material.GLASS),
                createRecipe(Material.PORK, Material.GRILLED_PORK),
                createRecipe(Material.RAW_BEEF, Material.COOKED_BEEF),
                createRecipe(Material.RAW_CHICKEN, Material.COOKED_CHICKEN),
                createRecipe(Material.COBBLESTONE, Material.STONE)
        };
    }

    private static ShapelessRecipe createRecipe(Material m, Material result) {
        ShapelessRecipe recipe = new ShapelessRecipe(new ItemStack(result));
        recipe.addIngredient(Material.COAL);
        recipe.addIngredient(m);
        return recipe;
    }

    @EventHandler
    public void onCraft(PrepareItemCraftEvent e) {
        KitGameUser<?> user = KitGameUser.getUser((Player) e.getView().getPlayer());

        if (Arrays.stream(RECIPES).anyMatch(e.getRecipe()::equals) && user.getKitHolders().stream().map(KitHolder::getKitProvider).noneMatch(HGKit.FORGER::equals)) {
            e.getInventory().setResult(null);
        }
    }

    @Override
    public void register() {
        super.register();
        Arrays.stream(RECIPES).forEach(Bukkit.getServer()::addRecipe);
    }
}
    