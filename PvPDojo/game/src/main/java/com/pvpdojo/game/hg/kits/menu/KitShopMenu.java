/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits.menu;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.kits.list.HGKit.HGKitLevel;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.gui.ConfirmInputGUI;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.menu.SortedInventoryMenu;
import com.pvpdojo.menu.SortedItem;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.sql.SQLException;

public class KitShopMenu extends SortedInventoryMenu {

    public KitShopMenu(Player player) {
        super(player, "Kit Shop", 54);
    }

    @Override
    public void redraw() {

        HGUser user = HGUser.getUser(getPlayer());
        int money = user.getStats().getMoney();

        for (HGKit kit : HGKit.values()) {
            if (kit.getLevel() == HGKitLevel.UNAVAILABLE || user.getPersistentKits().getOwnedKits().contains(kit) || !kit.isEnabled() || kit.isSideKit()) {
                continue;
            }

            CC color = money >= kit.getLevel().getPrice() ? CC.GREEN : CC.RED;
            int needed = kit.getLevel().getPrice() - money;
            ItemStack stack = new ItemBuilder(kit.getIcon(user))
                    .lore(true, "", color + "Costs " + kit.getLevel().getPrice() + "$" + (needed > 0 ? " (" + needed + "$ needed)" : ""))
                    .build();

            SortedItem item = new SortedItem(stack, needed <= 0 ? type -> new ConfirmInputGUI(getPlayer(), "Buy " + kit.getName(), () -> PvPDojo.schedule(() -> {
                try {
                    user.getStats().addMoney(-kit.getLevel().getPrice());
                    user.getPersistentKits().addKit(kit);
                    user.sendMessage(CC.GREEN + "Successfully bought " + kit.getName() + " for " + kit.getLevel().getPrice() + "$");
                } catch (SQLException e) {
                    Log.exception("Buying kit " + kit + " for " + user.getUUID(), e);
                    user.sendMessage(MessageKeys.ERROR);
                }
            }).createAsyncTask()).open() : null);
            item.setSortName(kit.name());
            addSortItem(item);
        }

    }

}
