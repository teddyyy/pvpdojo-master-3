/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.session;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.lang.HGMessageKeys;
import com.pvpdojo.game.hg.naturalevents.NaturalEventManager;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.game.session.StageHandler;
import com.pvpdojo.game.session.StagedSession;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.event.ArenaEntityDeathEvent;
import com.pvpdojo.userdata.stats.PersistentHGStats;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemUtil;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import java.text.DecimalFormat;

import static com.pvpdojo.util.StreamUtils.negate;

public abstract class PvPStage extends StageHandler {

    public PvPStage(StagedSession session) {
        super(session);
    }

    @Override
    public boolean isCountdown() {
        return false;
    }

    @Override
    public void run() {
        super.run();
        if (session.getState() != SessionState.FINISHED) {
            session.triggerEnd(session.getCurrentlyDead().toArray(new ArenaEntity[0]));
        }
        if (HG.inst().getSettings().isNaturalEvents()) {
            NaturalEventManager.inst().tick();
        }
    }

    @Override
    public StageHandler init() {
        PvPDojo.get().getServerSettings().setHunger(true);
        return super.init();
    }

    @EventHandler
    public void onDeath(ArenaEntityDeathEvent e) {
        PlayerDeathEvent deathEvent = (PlayerDeathEvent) e.getParentEvent();
        HGUser user = HGUser.getUser(deathEvent.getEntity());
        HGUser killerUser = HGUser.getUser(user.getPlayer().getKiller());

        if (killerUser != null) {
            killerUser.addKill();
            NaturalEventManager.inst().updateLastKill();
        }

        if (user.canRespawn()) {
            user.getSession().getCurrentlyDead().remove(user);
            user.setRespawned(true);
            user.setRespawning(true);
            PvPDojo.schedule(() -> user.getPlayer().spigot().respawn()).nextTick();
            if(KitUtil.hasKit(user, HGKit.ANCHOR)) {
                PvPDojo.schedule(() -> user.getPlayer().getInventory().setArmorContents(user.getKit().getStartArmor())).createTask(3);
            }
            return;
        }

        String abilities = KitUtil.getKitList(user);
        String abilitiesKiller = "None";

        if (killerUser != null) {
            abilitiesKiller = KitUtil.getKitList(killerUser);
        }

        if (deathEvent.getEntity().hasMetadata("pit-elimination")) {
            deathEvent.setDeathMessage("was eliminated");
            deathEvent.getEntity().removeMetadata("pit-elimination", PvPDojo.get());
        } else if (deathEvent.getEntity().getLastDamageCause() != null && deathEvent.getEntity().getLastDamageCause().getCause() == DamageCause.CUSTOM
                && deathEvent.getEntity().getLastDamageCause().getDamage() == 6) {
            deathEvent.setDeathMessage("thought he could dig a tunnel under the wall");
        }

        LocaleMessage deathMessage = StringUtils.handleDeathMessage(deathEvent, deathEvent.getEntity(), deathEvent.getEntity().getKiller(), abilities, abilitiesKiller, true);
        deathMessage.transformer(string -> CC.AQUA + CC.stripColor(string) + ".");
        e.addMessage(deathMessage);

        e.addMessage(LocaleMessage.of(HGMessageKeys.PLAYERS_LEFT,
                "{amount}", String.valueOf(session.getParticipants().stream().filter(negate(session.getCurrentlyDead()::contains)).count())));

        if(killerUser != null && killerUser.addKitItemAfterRespawn()) {
        	if(!KitUtil.hasKitItem(killerUser)) {
        		KitUtil.applyKitItems(killerUser);
        		killerUser.sendMessage(HGMessageKeys.RESPAWN_KITITEMS);
        		killerUser.setAddKitItemAfterRespawn(false);
        	}
        }
        
        if (!user.getRank().inheritsRank(Game.inst().getGame().getSettings().getRankToSpectate()) || !user.getSettings().getHGSettings().isSpectate()) {
            if (!user.isLoggedOut()) {
                user.getFastDB().setLostHG(true);
                user.getFastDB().save(user.getUUID());
            }
            PvPDojo.schedule(() -> {
                int credits = user.getKills() * PersistentHGStats.KILL_CREDIT + PersistentHGStats.GAME_CREDIT;
                if (KitUtil.hasKit(user, HGKit.CAPITALIST)) {
                    credits *= 3;
                }
                double skillpoints = user.getKills() * PersistentHGStats.SP_MODIFIER_KILL + PersistentHGStats.GAME_CREDIT;

                user.kick(CC.GREEN + "You earned " + credits + " credits and " + new DecimalFormat("#.#").format(skillpoints) + " skillpoints");
            }).nextTick();
        }

        user.setVanish(true);
        user.getPlayer().getWorld().playSound(user.getPlayer().getLocation(), Sound.AMBIENCE_THUNDER, 10000F, 1);
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        HGUser user = HGUser.getUser(e.getPlayer());

        if (user.isRespawning()) {
            e.setRespawnLocation(user.respawn());
            return;
        }

        e.setRespawnLocation(e.getPlayer().getLocation());
        user.setSpectator(true);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player && HGUser.getUser((Player) e.getEntity()).isInvincible()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onAttack(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player) {
            ItemStack item = ((Player) e.getDamager()).getItemInHand();
            if (ItemUtil.SWORDS.contains(item.getType()) && PvPDojo.RANDOM.nextBoolean()) {
                item.setDurability((short) (item.getDurability() - 1));
            }

            if (e.getEntity() instanceof Player) {
                if (HGUser.getUser((Player) e.getDamager()).isInvincible()) {
                    e.setCancelled(true);
                } else {
                    HGUser damaged = HGUser.getUser((Player) e.getEntity());
                    damaged.registerDamage(e.getDamager().getUniqueId());
                    if (damaged.receivesReducedDamage()) {
                        e.setDamage(e.getDamage() / 2);
                    }
                }
            }
        }
    }

}
