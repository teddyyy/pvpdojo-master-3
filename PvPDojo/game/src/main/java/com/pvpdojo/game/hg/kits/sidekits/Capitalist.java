/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits.sidekits;

import org.bukkit.event.entity.PlayerDeathEvent;

import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.userdata.stats.PersistentHGStats;

public class Capitalist extends Kit {

    @Override
    public void onKill(PlayerDeathEvent e) {
        HGUser.getUser(e.getEntity().getKiller()).getStats().addMoney(PersistentHGStats.KILL_CREDIT * 3);
    }

}
