/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.listener;

import org.bukkit.event.EventHandler;

import com.pvpdojo.bukkit.events.RegionLeaveEvent;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.userdata.User;

public class RegionListener implements DojoListener {

    @EventHandler
    public void onRegionLeave(RegionLeaveEvent e) {
        if (User.getUser(e.getPlayer()).isSpectator()) {
            e.setCancelled(true);
        }
    }

}
