/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.util.Vector;

import com.pvpdojo.game.kits.Kit;

public class Turtle extends Kit {

    @Override
    public void onDamage(EntityDamageEvent e) {
        if (((Player) e.getEntity()).isSneaking() && Madman.getMultiplier(e.getEntity().getUniqueId()) == 1) {
            e.setDamage(1);
        }
    }

    @Override
    public void onPlayerAttack(EntityDamageByEntityEvent e) {
        Player damager = (Player) e.getDamager();
        if (damager.isSneaking() && Madman.getMultiplier(e.getEntity().getUniqueId()) == 1) {
            e.setDamage(1);
        }
    }

    @Override
    public void onPlayerDamagedByPlayer(EntityDamageByEntityEvent e) {
        Player damager = (Player) e.getDamager();
        Player entity = (Player) e.getEntity();

        Vector entityDirection = entity.getEyeLocation().getDirection();
        Vector damagerDirection = damager.getEyeLocation().getDirection();

        if (damagerDirection.dot(entityDirection) > 0 && Madman.getMultiplier(e.getEntity().getUniqueId()) == 1) {
            e.setDamage(e.getDamage() * 0.5);
        }
    }
}
