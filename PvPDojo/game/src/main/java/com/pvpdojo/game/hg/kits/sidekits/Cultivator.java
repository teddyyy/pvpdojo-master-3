/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits.sidekits;

import org.bukkit.CropState;
import org.bukkit.Material;
import org.bukkit.TreeType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerSettings.CustomRecipeModule;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.kits.KitHolder;
import com.pvpdojo.game.userdata.kit.KitGameUser;

public class Cultivator extends ComplexKit {

    @Override
    public void onPlace(BlockPlaceEvent e) {
        if (!e.isCancelled()) {
            switch (e.getBlockPlaced().getType()) {
                case CROPS:
                case CARROT:
                case POTATO:
                    e.getBlock().setData(CropState.RIPE.getData());
                    break;
                case SAPLING:
                    e.getBlock().setType(Material.AIR);
                    boolean canSpawn = e.getBlock().getWorld().generateTree(e.getBlock().getLocation(),
                            generateTreeData(e.getBlock().getData()));
                    if (!canSpawn) {
                        e.setCancelled(true);
                        e.getPlayer().updateInventory();
                    }
                    break;
            }
        }
    }

    @Override
    public void onBreak(BlockBreakEvent e) {
        if (!e.isCancelled()) {
            if (e.getBlock().getType() == Material.LEAVES || e.getBlock().getType() == Material.LEAVES_2) {
                if (PvPDojo.RANDOM.nextInt(100) < 5 * 5) {

                    int data = (byte) getDropData(e.getBlock().getData());
                    if (e.getBlock().getType() == Material.LEAVES_2) {

                        if (data == 0) {//// Leaves_2 has 2 type of leaves (data = 0 is Acacia, data = 1 is Dark Oak)
                            data = 4;
                        } else {
                            data = 5;
                        }
                    }

                    e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(),
                            new ItemStack(Material.SAPLING, 1, (short) data));
                }
            }
        }
    }

    @EventHandler
    public void onPrepareCraft(PrepareItemCraftEvent e) {
        KitGameUser<?> user = KitGameUser.getUser((Player) e.getView().getPlayer());

        if (e.getInventory().contains(Material.SEEDS) && e.getInventory().contains(Material.BOWL)
                && user.getKitHolders().stream().map(KitHolder::getKitProvider).noneMatch(HGKit.CULTIVATOR::equals)) {
            e.getInventory().setResult(null);
        }
    }

    private TreeType generateTreeData(byte data) {
        switch (data) {
            case 1:
                return TreeType.REDWOOD;
            case 2:
                return TreeType.BIRCH;
            case 3:
                return PvPDojo.RANDOM.nextBoolean() ? TreeType.SMALL_JUNGLE : TreeType.JUNGLE;
            case 4:
                return TreeType.ACACIA;
            case 5:
                return TreeType.DARK_OAK;
            default:
                return TreeType.TREE;
        }
    }

    public int getDropData(int data) {
        return data & 0x3;
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[]{new ItemStack(Material.INK_SACK, 16, (short) 15)};
    }

    @Override
    public void register() {
        super.register();
        ShapelessRecipe cultirecipe = new ShapelessRecipe(new ItemStack(Material.MUSHROOM_SOUP));
        cultirecipe.addIngredient(3, Material.SEEDS);
        cultirecipe.addIngredient(1, Material.BOWL);
        new CustomRecipeModule(cultirecipe).register();
    }
}