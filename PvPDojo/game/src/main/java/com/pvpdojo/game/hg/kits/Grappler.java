/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.ClickKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import net.minecraft.server.v1_7_R4.EntityFishingHook;
import net.minecraft.server.v1_7_R4.EntitySnowball;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_7_R4.CraftWorld;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftSnowball;
import org.bukkit.entity.*;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.PlayerLeashEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

public class Grappler extends Kit implements ClickKit<PlayerInteractEvent> {

    private GrapplerRod hook;
    private boolean waiting;

    @Override
    public void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        User user = User.getUser(p);

        if (e.getItem() == null || e.getItem().getType() != getClickItem().getType()) {
            return;
        }

        if (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK) {

            if (waiting) {
                return;
            }

            if (user.isInCombat() || kitCooldown.isOnCooldown()) {
                p.sendMessage(CC.RED + "You can't throw another hook yet");
                return;
            }

            removeHook();
            GrapplerRod newHook = new GrapplerRod(p.getWorld(), p);
            this.hook = newHook;
            if (user.isInCombat()) {
                waiting = true;
                PvPDojo.schedule(() -> {
                    waiting = false;
                    if (this.hook == newHook) {
                        this.hook.spawn(p.getEyeLocation());
                    }
                }).createTask(3 * 10);
            } else {
                hook.spawn(p.getEyeLocation());
            }

        } else if (this.hook != null && this.hook.isHooked()) {
            grapple(e.getPlayer());
        }
    }

    @Override
    public void onDeath(PlayerDeathEvent e) {
        removeHook();
    }

    @Override
    public void onItemSwitch(PlayerItemHeldEvent e) {
        removeHook();
    }

    @Override
    public void onLeave(Player p) {
        removeHook();
    }

    private void removeHook() {
        if (this.hook != null) {
            this.hook.remove();
            this.hook = null;
        }
    }

    @Override
    public void onLeash(PlayerLeashEntityEvent e) {
        e.setCancelled(true);
    }

    private void grapple(Player p) {

        if (HGUser.getUser(p).isInCombat()) {
            return;
        }

        double t = this.hook.getBukkitEntity().getLocation().distance(p.getLocation());
        double v_x = (0.8D + 0.07D * t) * (this.hook.getBukkitEntity().getLocation().getX() - p.getLocation().getX()) / t;
        double v_y = (0.9D + 0.01D * t) * (this.hook.getBukkitEntity().getLocation().getY() - p.getLocation().getY()) / t;
        double v_z = (0.8D + 0.07D * t) * (this.hook.getBukkitEntity().getLocation().getZ() - p.getLocation().getZ()) / t;

        Vector v = new Vector();
        v.setX(v_x);
        v.setY(v_y);
        v.setZ(v_z);
        p.setVelocity(v);

        if (hook.locY > p.getLocation().getY() && p.getFallDistance() > 3) {
            p.setFallDistance(0);
            NMSUtils.damageEntity(p, null, DamageCause.FALL, 1);
            p.sendMessage(CC.GREEN + "You yanked the rope to break your fall, but that did hurt a little");
        }

        p.getWorld().playSound(p.getLocation(), Sound.STEP_GRAVEL, 0.05F, 1.0F);

    }

    @Override
    public void onDamage(EntityDamageEvent e) {

        Damageable d = (Damageable) e.getEntity();

        if (e.getCause() == DamageCause.FALL) {
            e.setDamage(e.getDamage() * 0.75);
        }

        if (d.getHealth() - e.getDamage() <= 0 && !e.isCancelled() && this.hook != null) {
            removeHook();
        }
    }

    @Override
    public void onPlayerDamagedByPlayer(EntityDamageByEntityEvent e) {

        if (!kitCooldown.isOnCooldown()) {
            kitCooldown.use();
        }
    }

    @Override
    public void activate(PlayerInteractEvent player) {
    }

    @Override
    public boolean isSendCooldown() {
        return false;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.LEASH);
    }

    class GrapplerRod extends EntityFishingHook {

        private Snowball sb;
        private EntitySnowball controller;
        private Player ref;
        private Entity hooked;
        private boolean isHooked;

        public GrapplerRod(org.bukkit.World world, Player player) {
            super(((CraftWorld) world).getHandle(), NMSUtils.get(player));
            this.ref = player;
        }

        protected void c() {
        }

        @Override
        public void h() {
            if (!isHooked) {
                for (Entity entity : sb.getNearbyEntities(3, 4, 3)) {
                    if (!(entity instanceof Firework) && !(entity instanceof Item) && !(entity instanceof FishHook) && !(entity instanceof Snowball)
                            && !entity.equals(ref)
                            && ((entity.getLocation().distance(this.controller.getBukkitEntity().getLocation()) < 2.0D) || (((entity instanceof Player))
                            && (((Player) entity).getEyeLocation().distance(this.controller.getBukkitEntity().getLocation()) < 2.0D)))) {

                        if (entity instanceof Player && !HGUser.getUser((Player) entity).isIngame()) {
                            continue;
                        }

                        sb.remove();
                        this.hooked = entity;
                        this.isHooked = true;
                        ref.sendMessage(CC.GREEN + "Gotcha!");
                    }
                }
            }
            if (hooked != null) {
                if (hooked.isDead()) {
                    remove();
                    return;
                }
                this.locX = this.hooked.getLocation().getX();
                this.locY = this.hooked.getLocation().getY();
                this.locZ = this.hooked.getLocation().getZ();
                this.motX = 0.0D;
                this.motY = 0.04D;
                this.motZ = 0.0D;
                this.isHooked = true;
            } else {
                if (this.controller.dead && !isHooked) {
                    ref.sendMessage(CC.GREEN + "The hook is attached now!");
                    getBukkitEntity().teleport(sb);
                    this.isHooked = true;
                    return;
                }

                Vector difference = sb.getLocation().toVector().subtract(getBukkitEntity().getLocation().toVector()).normalize();
                getBukkitEntity().setVelocity(difference);
                this.locX = this.sb.getLocation().getX();
                this.locY = this.sb.getLocation().getY();
                this.locZ = this.sb.getLocation().getZ();
            }
        }

        public void die() {
        }

        public void remove() {
            if (sb != null) {
                sb.removeMetadata("grappler", PvPDojo.get());
                sb.remove();
            }
            super.die();
            this.sb = null;
            this.controller = null;
            owner = null;
            this.hooked = null;
        }

        public void spawn(Location location) {
            this.sb = ref.launchProjectile(Snowball.class);
            sb.setMetadata("grappler", new FixedMetadataValue(PvPDojo.get(), null));
            sb.setVelocity(ref.getEyeLocation().getDirection().multiply(1.5));
            this.controller = ((CraftSnowball) this.sb).getHandle();

            for (Player p : Bukkit.getOnlinePlayers()) {
                NMSUtils.get(p).removeQueue.add(this.controller.getId());
            }
            ((CraftWorld) location.getWorld()).getHandle().addEntity(this);
        }

        public boolean isHooked() {
            return this.isHooked;
        }

        public void setHookedEntity(Entity damaged) {
            this.hooked = damaged;
        }

    }

    @Override
    public int getCooldown() {
        return 5_000;
    }
}