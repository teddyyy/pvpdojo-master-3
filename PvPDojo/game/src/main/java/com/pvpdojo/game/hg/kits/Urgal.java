/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.util.bukkit.ItemBuilder.PotionBuilder;

public class Urgal extends Kit {

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] { new PotionBuilder(PotionType.STRENGTH).addEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 35 * 20, 0)).build() };
    }
}
