/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.util;

import java.sql.SQLException;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.achievement.Achievement;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.Log;

public class ShitRewardUtil {

    public static void reward(HGUser user) {
        PvPDojo.schedule(() -> {
            if (user.getFastDB().hasLikedNameMC()) {
                PvPDojo.schedule(() -> Achievement.CATTLE_FARMER.trigger(user.getPlayer())).sync();
                try {
                    if (!user.getPersistentKits().getOwnedKits().contains(HGKit.MILKMAN)) {
                        user.getPersistentKits().addKit(HGKit.MILKMAN);
                        user.sendMessage(CC.GREEN + "You received Milkman for free due to your NameMC like");
                    }
                } catch (SQLException ex) {
                    Log.exception(ex);
                }
            }

            if (user.getStats().getKills() >= 100) {
                PvPDojo.schedule(() -> Achievement.CARBON_COPY.trigger(user.getPlayer())).sync();
                try {
                    if (!user.getPersistentKits().getOwnedKits().contains(HGKit.COPYCAT)) {
                        user.getPersistentKits().addKit(HGKit.COPYCAT);
                        user.sendMessage(CC.GREEN + "You received Copycat for reaching 100 kills");
                    }
                } catch (SQLException ex) {
                    Log.exception(ex);
                }
            }

            if (user.getStats().getWins() >= 1) {
                PvPDojo.schedule(() -> Achievement.TRAPPING_STARTER_PACK.trigger(user.getPlayer())).sync();
                try {
                    if (!user.getPersistentKits().getOwnedKits().contains(HGKit.ENDERMAGE)) {
                        user.getPersistentKits().addKit(HGKit.ENDERMAGE);
                        user.sendMessage(CC.GREEN + "You received Endermage for winning your first round");
                    }
                } catch (SQLException ex) {
                    Log.exception(ex);
                }
            }

            if (user.getPersistentKits().getOwnedKits().size() >= 10) {
                PvPDojo.schedule(() -> Achievement.TEN_FOR_ONE.trigger(user.getPlayer())).sync();
                try {
                    if (!user.getPersistentKits().getOwnedKits().contains(HGKit.WORM)) {
                        user.getPersistentKits().addKit(HGKit.WORM);
                        user.sendMessage(CC.GREEN + "You received Worm for buying 10 kits");
                    }
                } catch (SQLException ex) {
                    Log.exception(ex);
                }
            }

        }).createAsyncTask();

    }

}
