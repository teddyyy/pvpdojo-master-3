/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.lms.session;

import com.pvpdojo.game.lang.GameMessageKeys;
import com.pvpdojo.game.session.StageHandler;
import com.pvpdojo.game.session.StagedSession;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.util.Countdown;

public class Invincibility extends StageHandler {

    public Invincibility(StagedSession session) {
        super(session);
    }

    @Override
    public StageHandler init() {
        session.setState(SessionState.INVINCIBILITY);
        setTimeSeconds(15);
        return super.init();
    }

    @Override
    public void run() {
        super.run();
        if (Countdown.isUserFriendlyNumber(getTimeSeconds())) {
            session.broadcast(GameMessageKeys.COUNTDOWN_INVINCIBILITY, "{time}", Countdown.getUserFriendlyNumber(getTimeSeconds()));
        }
    }

    @Override
    public boolean isCountdown() {
        return true;
    }

    @Override
    public StageHandler getNextHandler() {
        return new PvP(session);
    }
}
