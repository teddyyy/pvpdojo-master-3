/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.command;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import co.aikar.locales.MessageKey;

@CommandAlias("kitinfo")
public class KitInfoCommand extends DojoCommand {

    @CommandCompletion("@kits")
    @Default
    @CatchUnknown
    public void onKitInfo(User user, HGKit kit) {
        user.sendMessage(LocaleMessage.of(MessageKey.of("pvpdojo-hg-kits-info.description_" + kit.name().toLowerCase())).transformer(str -> CC.GREEN + CC.stripColor(str)));
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
