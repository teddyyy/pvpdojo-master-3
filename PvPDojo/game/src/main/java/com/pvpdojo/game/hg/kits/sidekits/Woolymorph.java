package com.pvpdojo.game.hg.kits.sidekits;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.kits.ComplexKit;
import com.pvpdojo.game.kits.EntityInteractingKit;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.util.bukkit.CC;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_7_R4.entity.disguise.CraftCreatureDisguise;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.disguise.Disguise;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

public class Woolymorph extends ComplexKit implements EntityInteractingKit {

    public String SHEEP_KEY = "shrep";

    @Override
    public void activate(PlayerInteractEntityEvent e) {

        if (e.getRightClicked() instanceof Player) {
            Player player = (Player) e.getRightClicked();

            Disguise disguise = new CraftCreatureDisguise(player, EntityType.SHEEP);

            player.setDisguise(disguise);
            player.setMetadata(SHEEP_KEY, new FixedMetadataValue(Game.inst(), null));
            player.sendMessage(CC.RED + "Your enemy has turned you into a sheep");

            PvPDojo.schedule(() -> deactivate(player)).createTask(20 * 15);

            kitCooldown.use();
        }
    }

    private void deactivate(Player player) {
        player.setDisguise(null);
        player.sendMessage(CC.GREEN + "You transformed back into an ordinary player");
        player.removeMetadata(SHEEP_KEY, Game.inst());
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {

        if (e.getPlayer().hasMetadata(SHEEP_KEY)) {
            deactivate(e.getPlayer());//Mostly they will die in combat. so lets reset them
        }
    }

    @EventHandler
    public void onPlayerDamagedByPlayer(EntityDamageByEntityEvent e) {

        if (e.getDamager() instanceof Player && e.getEntity() instanceof Player) {

            if (e.isCancelled()) {
                return;
            }

            Player entity = (Player) e.getEntity();
            Player player = (Player) e.getDamager();

            if (player.hasMetadata(SHEEP_KEY)) {
                e.setCancelled(true);
                player.sendMessage(CC.RED + "Sheeps should be friendly");
                return;
            }

            if (entity.hasMetadata(SHEEP_KEY)) {
                e.setCancelled(true);
                player.sendMessage(CC.RED + "Sheeps are too cute to be harmed");
            }

        }
    }

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.SHEARS);
    }

    @Override
    public int getCooldown() {
        return 50000;
    }
}
