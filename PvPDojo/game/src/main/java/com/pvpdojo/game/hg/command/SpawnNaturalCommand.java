/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.command;

import java.util.function.Supplier;

import org.bukkit.command.CommandSender;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.game.hg.naturalevents.NaturalEvent;
import com.pvpdojo.game.hg.naturalevents.NaturalEventManager;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Single;
import co.aikar.commands.annotation.Subcommand;

@CommandAlias("spawnnatural")
public class SpawnNaturalCommand extends DojoCommand {

    @Default
    @CatchUnknown
    @CommandCompletion("acidrain|heatwave|snowstorm|thunderstorm|storm|beastattack|meteorshower|earthquake|supplydrop")
    public void onSpawnNatural(CommandSender sender, @Single String naturalEvent) {
        for (Supplier<NaturalEvent> eventSupplier : NaturalEventManager.inst().getNaturalEvents()) {
            NaturalEvent event = eventSupplier.get();
            if (event.getClass().getSimpleName().equalsIgnoreCase(naturalEvent)) {
                if (NaturalEventManager.inst().spawnEvent(event)) {
                    sender.sendMessage("Event Spawned");
                }
            }
        }
    }

    @Subcommand("stop")
    public void onStop(CommandSender sender) {
        if (NaturalEventManager.inst().cancelEvent()) {
            sender.sendMessage("Stopped");
        }
    }

    @Override
    public Rank getRank() {
        return Rank.DEVELOPER;
    }

}
