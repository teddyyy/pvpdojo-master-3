package com.pvpdojo.game.ctm.kits;

import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.kits.Kit;

public class Spy extends Kit implements InteractingKit {

    private boolean spyMode;

    @Override
    public void activate(PlayerInteractEvent event) {
        spyMode = true;
        event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1));
    }


    @Override
    public void onDamage(EntityDamageEvent e) {
        if (spyMode && !e.isCancelled() && (e.getCause() == EntityDamageEvent.DamageCause.ENTITY_ATTACK || e.getCause() == EntityDamageEvent.DamageCause.PROJECTILE)) {
            ((LivingEntity)e.getEntity()).removePotionEffect(PotionEffectType.INVISIBILITY);
            spyMode = false;
            getKitCooldown().use();
        }
    }

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.GLASS_BOTTLE);
    }

    @Override
    public int getCooldown() {
        return 45_000;
    }

}
