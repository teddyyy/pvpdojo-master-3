/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.kits;

import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.kits.KitProvider;
import com.pvpdojo.game.userdata.kit.KitGameUser;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder.PotionBuilder;
import com.pvpdojo.util.bukkit.ItemUtil;

public class Scout extends Kit {

    private int seconds;

    @Override
    public void onTick(KitGameUser<? extends KitProvider> user) {
        if (HG.inst().getSession().getState() != SessionState.PREGAME && ++seconds >= 300) {
            seconds = 0;
            user.sendMessage(CC.GREEN + "You have been granted a speed potion");
            ItemUtil.addItemWhenFree(user.getPlayer(), new PotionBuilder(PotionType.SPEED).splash().level(2).build());
        }
    }

    @Override
    public void onDamage(EntityDamageEvent e) {
        if (e.getCause() == DamageCause.FALL && ((Player) e.getEntity()).hasPotionEffect(PotionEffectType.SPEED)) {
            e.setDamage(Math.min(e.getDamage(), 0));
        }
    }

    @Override
    public ItemStack[] getStartItems() {
        return new ItemStack[] { new PotionBuilder(PotionType.SPEED).splash().level(2).amount(2).build() };
    }
}
