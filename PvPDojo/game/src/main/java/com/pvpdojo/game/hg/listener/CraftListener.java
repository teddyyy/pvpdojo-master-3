/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.listener;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.PrepareItemCraftEvent;

import com.pvpdojo.bukkit.listeners.DojoListener;

public class CraftListener implements DojoListener {

    @EventHandler
    public void onCraft(PrepareItemCraftEvent e) {
        if (e.getRecipe().getResult().getType() == Material.FISHING_ROD) {
            e.getInventory().setResult(null);
        }
    }

}
