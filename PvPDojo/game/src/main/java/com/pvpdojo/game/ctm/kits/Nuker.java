/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.kits;

import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.game.ctm.kits.list.CTMKit;
import com.pvpdojo.game.ctm.userdata.CTMUser;
import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.kits.Kit;

public class Nuker extends Kit implements InteractingKit {

    private boolean inair, pushed;

    @Override
    public void onLeave(Player p) {
        inair = false;
        pushed = false;
    }

    @Override
    public void onMove(PlayerMoveEvent e) {
        if (e.isOnGround()) {
            if (pushed) {
                getKitCooldown().use();
            }
            pushed = false;
        }
        if (e.getPlayer().isOnGround()) {
            if (inair) {
                getKitCooldown().use();
            }
            inair = false;
        }
    }

    @Override
    public void onDamage(EntityDamageEvent e) {

        if (e.getCause() == DamageCause.FALL && inair) {
            e.setDamage(0D);
            for (Entity en : e.getEntity().getNearbyEntities(5, 5, 5)) {
                if (!(en instanceof Damageable)) {
                    continue;
                }
                en.getWorld().playEffect(en.getLocation(), Effect.EXPLOSION_LARGE, 0);
                en.setVelocity(new Vector(0, 1.9, 0));

                boolean nuker = en instanceof Player && CTMUser.getUser((Player) en).getKitHolder().getKitProvider() == CTMKit.NUKER;
                EntityDamageEvent event = BukkitUtil.callEntityDamageEvent(en, nuker ? DamageCause.ENTITY_ATTACK : DamageCause.FALL, 7);

                if (!event.isCancelled()) {
                    ((Damageable) en).damage(7);
                }
            }
        }
    }

    @Override
    public void click(Player player) {}

    @Override
    public void activate(PlayerInteractEvent e) {

        Player player = e.getPlayer();

        if (inair && !pushed) {
            Vector push = new Vector(player.getLocation().getDirection().getX(), player.getLocation().getDirection().getY() * 3, player.getLocation().getDirection().getZ());
            if (push.getY() > 0) {
                push.setY(-push.getY());
            }
            player.setVelocity(push);
            pushed = true;
            getKitCooldown().use();
            return;
        }

        if (player.getFallDistance() > 12) {
           //  User.getUser(player).sendMessage(CTMKitMessageKeys.NUKER_CANNOT_USE_FALL);
            return;
        }

        player.teleport(player.getLocation().add(0, 0.2, 0));
        player.setVelocity(new Vector(0, 2.6, 0));
        inair = true;

    }

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.TNT);
    }

    @Override
    public int getCooldown() {
        return 60_000;
    }
}
