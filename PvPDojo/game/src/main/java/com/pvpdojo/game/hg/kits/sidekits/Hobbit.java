package com.pvpdojo.game.hg.kits.sidekits;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.TempBlock;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.session.Feast;
import com.pvpdojo.game.hg.util.FeastUtil;
import com.pvpdojo.game.kits.InteractingKit;
import com.pvpdojo.game.kits.Kit;
import com.pvpdojo.game.util.TrackerUtil;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.CC;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Hobbit extends Kit implements InteractingKit {

    private static final List<Player> hobbitFighters = new ArrayList<Player>();

    @Override
    public void activate(PlayerInteractEvent e) {

        Player player = e.getPlayer();
        int playerCount = (int) TrackerUtil.getNearbyIngame(e.getPlayer(), 10, 10, 10).count();

        if (playerCount >= 2) {

            if (player.getLocation().getY() <= 80 && player.getLocation().getY() >= 50) {

                if (!hobbitFighters.contains(player)) {

                    Player closestPlayer = TrackerUtil.getNearbyIngame(player, 10, 10, 10)
                            .min(Comparator.comparingDouble(other -> other.getLocation().distance(player.getLocation())))
                            .get();

                    if (!hobbitFighters.contains(closestPlayer)) {

                        Location hobbitLoc = player.getLocation();

                        if (HG.inst().getFeastLocation() == null || (HG.inst().getFeastLocation() != null && hobbitLoc.distance(HG.inst().getFeastLocation()) > 20)) {
                            TrackerUtil.getNearbyIngame(player, 5, 5, 5)
                                    .filter(p -> p != closestPlayer)
                                    .forEach(p -> p.teleport(new Location(e.getPlayer().getWorld(), hobbitLoc.getX(), hobbitLoc.getY() + 6, hobbitLoc.getZ())));

                            for (Location sphere : BlockUtil.circle(hobbitLoc, 5, 3, true,
                                    true, 0)) {
                                if (sphere.getBlock().getType().equals(Material.AIR)) {
                                    sphere.getBlock().setType(Material.MOSSY_COBBLESTONE);
                                }
                            }

                            hobbitFighters.add(player);
                            hobbitFighters.add(closestPlayer);

                            PvPDojo.schedule(() -> {
                                hobbitFighters.remove(player);
                                hobbitFighters.remove(closestPlayer);
                                for (Location sphere : BlockUtil.circle(hobbitLoc, 5, 3, true,
                                        true, 0)) {
                                    if (sphere.getBlock().getType().equals(Material.MOSSY_COBBLESTONE)) {
                                        sphere.getBlock().setType(Material.AIR);
                                    }
                                }
                            }).createTask(30 * 20);

                            kitCooldown.use();
                        } else {
                            player.sendMessage(CC.RED + "You may not use this kit near the feast");
                        }
                    } else {
                        player.sendMessage(CC.RED + "You may not use this kit if the closest player already is in a hobbit fight");
                    }
                } else {
                    player.sendMessage(CC.RED + "You cannot use this kit while already being in a hobbit fight");
                }
            } else {
                player.sendMessage(CC.RED + "You can only use this kit while being between y: 50 and y: 80");
            }
        } else {
            player.sendMessage(CC.RED + "You can only use this kit when two or more players are nearby");
        }
    }

    @Override
    public void click(Player player) {
    }

    @Override
    public ItemStack getClickItem() {
        return new ItemStack(Material.MOSSY_COBBLESTONE);
    }

    @Override
    public boolean isSendCooldown() {
        return true;
    }

    @Override
    public int getCooldown() {
        return 60000;
    }
}
