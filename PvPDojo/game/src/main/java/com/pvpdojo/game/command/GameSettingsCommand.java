/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.command;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.game.Game;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Conditions;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Subcommand;

@CommandAlias("gamesettings")
public class GameSettingsCommand extends DojoCommand {

    @Default
    @CatchUnknown
    @Conditions("state_pregame")
    public void onSettings(@Conditions("game_settings") User user) {
        Game.inst().getGame().getSettings().getMenu(user.getPlayer()).open();
    }

    @Subcommand("debug")
    @CommandPermission("Owner")
    public void onSettingsDebug(User user) {
        Game.inst().getGame().getSettings().getMenu(user.getPlayer()).open();
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
