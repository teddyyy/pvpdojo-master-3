/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.command;

import org.bukkit.entity.Player;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.session.DefaultFeast;
import com.pvpdojo.game.hg.session.Pit;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Subcommand;

@CommandAlias("feast")
public class FeastCommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onFeast(Player player) {
        if (HG.inst().getFeastLocation() != null) {
            player.setCompassTarget(HG.inst().getFeastLocation());
            player.sendMessage(CC.YELLOW + "Your compass is now pointing at feast");
        } else {
            player.sendMessage(CC.RED + "Feast location is not known yet");
        }
    }

    @CommandPermission("Staff")
    @Subcommand("force")
    public void onFeastForce() {
        if (HG.inst().getSession().getCurrentHandler() instanceof DefaultFeast) {
            ((DefaultFeast) HG.inst().getSession().getCurrentHandler()).getFeastCountdown().setCurrentCount(1).start();
        } else if (HG.inst().getSession().getCurrentHandler() instanceof Pit) {
            ((Pit) HG.inst().getSession().getCurrentHandler()).getPitCountdown().setCurrentCount(1).start();
        }
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
