/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.kits;

import com.pvpdojo.game.lang.GameMessageKeys;
import com.pvpdojo.userdata.User;

public class KitCooldown {

    private Kit kit;
    private long lastUse;
    private int usesTillCooldown = 1;
    private int uses;

    public KitCooldown(Kit kit) {
        this.kit = kit;
    }

    public void use() {
        if (++uses >= usesTillCooldown) {
            resetUses();
            this.lastUse = System.currentTimeMillis();
        }
    }

    public void resetUses() {
        uses = 0;
    }

    public boolean isOnCooldown() {
        return System.currentTimeMillis() < lastUse + kit.getCooldown();
    }

    public float getSecondsLeft() {
        return ((lastUse + kit.getCooldown() - System.currentTimeMillis()) / 1000F);
    }

    public void setUsesTillCooldown(int usesTillCooldown) {
        this.usesTillCooldown = usesTillCooldown;
    }

    public void sendCooldown(User user) {
        user.sendMessage(GameMessageKeys.COOLDOWN, "{seconds}", String.format("%.1f", getSecondsLeft()));
    }

    public float getCooldownRatio() {
        long timePast = System.currentTimeMillis() - lastUse;
        return (kit.getCooldown() - timePast) / (float) kit.getCooldown();
    }

}
