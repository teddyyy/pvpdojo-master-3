/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.session;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.DataLoadedEvent;
import com.pvpdojo.bukkit.events.DojoPlayerJoinEvent;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.game.Game;
import com.pvpdojo.game.hg.HG;
import com.pvpdojo.game.hg.kits.sidekits.Cupid;
import com.pvpdojo.game.hg.kits.Madman;
import com.pvpdojo.game.hg.kits.list.HGKit;
import com.pvpdojo.game.hg.lang.HGMessageKeys;
import com.pvpdojo.game.hg.userdata.HGUser;
import com.pvpdojo.game.hg.userdata.PreGameHotbarGUI;
import com.pvpdojo.game.hg.userdata.TrackerHotbarGUI;
import com.pvpdojo.game.hg.util.ShitRewardUtil;
import com.pvpdojo.game.kits.util.KitUtil;
import com.pvpdojo.game.session.QueueableStage;
import com.pvpdojo.game.session.StageHandler;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.settings.UserSettings.ScoreboardMode;
import com.pvpdojo.userdata.User;
import com.pvpdojo.userdata.sidebar.Sidebar;
import com.pvpdojo.userdata.stats.PersistentHGStats;
import com.pvpdojo.util.Countdown;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class PreGame extends QueueableStage<HGQueue, HGUser, HGSession> {

    public PreGame(HGSession session) {
        super(session);
    }

    @Override
    public HGQueue createQueued(HGUser user) {
        return new HGQueue(user.getUUID(), KitUtil.getKitProvider(user), user.getSideKit().getKitProvider());
    }

    @Override
    public boolean canQueue(HGUser user) {
        return timeSeconds > 60;
    }

    @Override
    public StageHandler getNextHandler() {
        return new Invincibility(session);
    }

    @Override
    public StageHandler init() {
        session.setState(SessionState.PREGAME);
        setTimeSeconds(420);
        return super.init();
    }

    private int lastPlayerCount;

    @Override
    public void run() {
        super.run();

        int playerCount = getPlayerCountWithQueue();

        session.executeForPlayers(player -> {
            User user = User.getUser(player);

            if (user.isDataLoaded() && user.getSettings().getHGSettings().getScoreboardMode() == ScoreboardMode.SMALL) {
                Sidebar sidebar = user.getSidebar();
                sidebar.setTitle(CC.RED + "Starting In " + StringUtils.getReadableSeconds(getTimeSeconds()));
                if (sidebar.get(lastPlayerCount) != null) {
                    sidebar.changeLine(lastPlayerCount, playerCount);
                } else {
                    sidebar.removeAll();
                    sidebar.set(playerCount, "Player Count");
                }
            }
        });

        this.lastPlayerCount = playerCount;

        if (!shouldCountDown()) {
            return;
        }

        if (timeSeconds < 30) {
            pullQueued();
        }

        if (timeSeconds <= 5 && timeSeconds != 0) {
            session.executeForPlayers(players -> players.playSound(players.getLocation(), Sound.NOTE_BASS, 1, 1));
        }

        if (timeSeconds == 0) {
            session.executeForPlayers(players -> players.playSound(players.getLocation(), Sound.NOTE_PLING, 1, 1));
        }

        if (Countdown.isUserFriendlyNumber(getTimeSeconds())) {
            LocaleMessage localeMessage = LocaleMessage.of(HGMessageKeys.COUNTDOWN_START, "{time}",
                    Countdown.getUserFriendlyNumber(getTimeSeconds()));
            session.broadcast(localeMessage);
            broadcastQueued(localeMessage.getDefaultLocale()[0]);
        }

        if (getTimeSeconds() == 15) {
            prepareForStart();
        }

    }

    @Override
    public boolean shouldCountDown() {
        return HG.inst().getSettings().getMinPlayers() < 4 || getPlayerCountWithQueue() > 5 || timeSeconds > 300;
    }

    @Override
    public void setTimeSeconds(int timeSeconds) {
        super.setTimeSeconds(timeSeconds);
        if (timeSeconds <= 15) {
            prepareForStart();
        }
    }

    private void prepareForStart() {
        Bukkit.setSaveBandwidth(true);
        session.prepareEntities(10, 1, (timeSeconds - 5) * 20);
    }

    @Override
    public void advance() {
        Bukkit.getWorlds().get(0).setTime(0);
        session.getParticipants().forEach(entity -> entity.setFrozen(false));

        if (session.getParticipants().size() >= HG.inst().getSettings().getMinPlayers()) {

            Game.inst().enableKits();
            PvPDojo.get().getServerSettings().setAllowQuitMessage(true);
            PvPDojo.get().getServerSettings().setAllowJoinMessage(true);

            session.executeForPlayers(player -> {
                HGUser user = HGUser.getUser(player);
                user.setHotbarGui(TrackerHotbarGUI.get());
                user.getStats().startGame();
                if (user.getKit() != null && user.getKitHolder().getKitProvider() == HGKit.CAPITALIST) {
                    user.getStats().addMoney(PersistentHGStats.GAME_CREDIT * 2);
                }
                player.setSaturation(20);
                KitUtil.applyKitItems(user);
                user.sendMessage(HGMessageKeys.GAME_START);

                if (user.getKit() instanceof Madman || user.getKit() instanceof Cupid) {
                    user.sendMessage("");
                    user.sendMessage(MessageKeys.WARNING, "{text}", CC.DARK_RED + "Teaming with this Kit can result in a punishment.");
                }
            });
            super.advance();
        } else if (Game.inst().isPrivate()) {
            BukkitUtil.shutdown();
        } else {
            session.broadcast(LocaleMessage.of(""));
            setTimeSeconds(3 * 60);
            resetQueue();
        }
    }

    @Override
    public boolean isCountdown() {
        return true;
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPickup(PlayerPickupItemEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (e.getAction() != Action.RIGHT_CLICK_BLOCK || (!e.getPlayer().getItemInHand().getType().isBlock()
                || e.getPlayer().getItemInHand().getType() == Material.AIR)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onInteractEntity(PlayerInteractEntityEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onMobTarget(EntityTargetEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onDataLoaded(DataLoadedEvent e) {
        ShitRewardUtil.reward(HGUser.getUser(e.getData().getUUID()));
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onJoin(DojoPlayerJoinEvent e) {
        HGUser user = HGUser.getUser(e.getPlayer());

        if (session.getParticipants().size() >= HG.inst().getSettings().getMaxPlayers() && timeSeconds > 30) {
            timeSeconds = 30;
        }
        session.prepareEntity(user);

        user.setHotbarGui(PreGameHotbarGUI.get());
        e.setJoinMessage(null);

    }

    @EventHandler
    public void onInvClick(InventoryClickEvent e) {
        e.setCancelled(true);
    }

}
