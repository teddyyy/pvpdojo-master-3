/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.lms.session;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;

import com.pvpdojo.bukkit.events.DojoPlayerJoinEvent;
import com.pvpdojo.game.lang.GameMessageKeys;
import com.pvpdojo.game.lms.LMS;
import com.pvpdojo.game.lms.userdata.PreGameHotbarGUI;
import com.pvpdojo.game.session.StageHandler;
import com.pvpdojo.game.session.StagedSession;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Countdown;

public class PreGame extends StageHandler {

    public PreGame(StagedSession session) {
        super(session);
    }

    @Override
    public StageHandler init() {
        session.setState(SessionState.PREGAME);
        setTimeSeconds(30);
        return super.init();
    }

    @Override
    public void advance() {
        super.advance();
        session.initialTeleport();
        session.prepareEntities();
    }

    @Override
    public void run() {
        super.run();

        if (Bukkit.getOnlinePlayers().size() < 2) {
            setTimeSeconds(30);
            return;
        }

        if (Countdown.isUserFriendlyNumber(getTimeSeconds())) {
            session.broadcast(GameMessageKeys.COUNTDOWN_START, "{time}", Countdown.getUserFriendlyNumber(getTimeSeconds()));
        }
    }

    @Override
    public boolean isCountdown() {
        return true;
    }

    @Override
    public StageHandler getNextHandler() {
        return new Invincibility(session);
    }

    @EventHandler
    public void onJoin(DojoPlayerJoinEvent e) {
        e.getPlayer().teleport(LMS.inst().getSession().getArena().getLocations()[0]);
        User.getUser(e.getPlayer()).setHotbarGui(PreGameHotbarGUI.get());
    }

}
