/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.kits;

public class KitHolder<P extends KitProvider> {

    private final P kitProvider;
    private final Kit kit;

    public KitHolder(P kitProvider, Kit kit) {
        this.kitProvider = kitProvider;
        this.kit = kit;
    }

    public P getKitProvider() {
        return kitProvider;
    }

    public Kit getKit() {
        return kit;
    }

}
