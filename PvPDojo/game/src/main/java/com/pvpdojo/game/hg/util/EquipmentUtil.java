/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.util;

import java.util.EnumMap;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import net.minecraft.server.v1_7_R4.EnumArmorMaterial;

public class EquipmentUtil {

    public static final double FULL_MINI_FEAST = 2, POS_NEG_MINI_FEAST = 8;

    private static final EnumMap<Material, Double> EQUIPMENT_MAP = new EnumMap<>(Material.class);

    static {
        EQUIPMENT_MAP.put(Material.FIREWORK, 8D);
        EQUIPMENT_MAP.put(Material.LEASH, 3D);
        EQUIPMENT_MAP.put(Material.NETHER_STAR, 3D);
        EQUIPMENT_MAP.put(Material.STONE_SWORD, 0.5D);
        EQUIPMENT_MAP.put(Material.IRON_SWORD, 3D);
        EQUIPMENT_MAP.put(Material.IRON_AXE, 4D);
        EQUIPMENT_MAP.put(Material.DIAMOND_AXE, 6D);
        EQUIPMENT_MAP.put(Material.DIAMOND_SWORD, 5D);
        EQUIPMENT_MAP.put(Material.MUSHROOM_SOUP, 1D / 32D);
        EQUIPMENT_MAP.put(Material.BROWN_MUSHROOM, 1D / 64D);
        EQUIPMENT_MAP.put(Material.RED_MUSHROOM, 1D / 64D);
        EQUIPMENT_MAP.put(Material.IRON_INGOT, 0.25D);
        EQUIPMENT_MAP.put(Material.IRON_FENCE, 1D);
        EQUIPMENT_MAP.put(Material.INK_SACK, 1D / 32D);
        EQUIPMENT_MAP.put(Material.CACTUS, 1D / 64D);
    }

    public static double getEquipmentPoints(Player player) {
        double points = 0;
        PlayerInventory inventory = player.getInventory();

        for (ItemStack stack : inventory) {
            if (stack != null) {
                if (EQUIPMENT_MAP.containsKey(stack.getType())) {
                    points += stack.getAmount() * EQUIPMENT_MAP.get(stack.getType());
                }
                if (stack.containsEnchantment(Enchantment.DAMAGE_ALL)) {
                    points += stack.getEnchantmentLevel(Enchantment.DAMAGE_ALL);
                }
                points += getArmorPoints(stack);
            }
        }

        for (ItemStack armor : inventory.getArmorContents()) {
            if (armor != null) {
                points += getArmorPoints(armor);
            }
        }

        return points;
    }

    public static double getArmorPoints(ItemStack stack) {
        EnumArmorMaterial armorMaterial = getArmorMaterial(stack);
        if (armorMaterial != null) {
            return armorMaterial.b(getArmorSlot(stack.getType())) / 2D;
        }
        return 0;
    }

    private static EnumArmorMaterial getArmorMaterial(ItemStack stack) {
        switch (stack.getType()) {
            case DIAMOND_BOOTS:
            case DIAMOND_LEGGINGS:
            case DIAMOND_CHESTPLATE:
            case DIAMOND_HELMET:
                return EnumArmorMaterial.DIAMOND;
            case LEATHER_BOOTS:
            case LEATHER_LEGGINGS:
            case LEATHER_CHESTPLATE:
            case LEATHER_HELMET:
                return EnumArmorMaterial.CLOTH;
            case GOLD_BOOTS:
            case GOLD_LEGGINGS:
            case GOLD_CHESTPLATE:
            case GOLD_HELMET:
                return EnumArmorMaterial.GOLD;
            case IRON_BOOTS:
            case IRON_LEGGINGS:
            case IRON_CHESTPLATE:
            case IRON_HELMET:
                return EnumArmorMaterial.IRON;
            case CHAINMAIL_BOOTS:
            case CHAINMAIL_LEGGINGS:
            case CHAINMAIL_CHESTPLATE:
            case CHAINMAIL_HELMET:
                return EnumArmorMaterial.CHAIN;
        }
        return null;
    }

    private static int getArmorSlot(Material material) {
        String type = material.name().split("_")[1];
        switch (type) {
            case "HELMET":
                return 0;
            case "CHESTPLATE":
                return 1;
            case "LEGGINGS":
                return 2;
            case "BOOTS":
                return 3;
        }
        return -1;
    }

}
