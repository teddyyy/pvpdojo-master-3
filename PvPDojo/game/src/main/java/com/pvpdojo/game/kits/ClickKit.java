/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.kits;

import org.bukkit.event.player.PlayerEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.userdata.User;

public interface ClickKit<T extends PlayerEvent> {

    default boolean checkUsage(Kit kit, User user) {
        if (kit.getKitCooldown().isOnCooldown()) {
            if (isSendCooldown()) {
                kit.getKitCooldown().sendCooldown(user);
            }
            return false;
        }
        return true;
    }

    void activate(T event);

    boolean isSendCooldown();

    ItemStack getClickItem();

}
