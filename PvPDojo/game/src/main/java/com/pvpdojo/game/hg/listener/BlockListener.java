/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.listener;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.comphenix.protocol.PacketType.Play.Client;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerOptions;
import com.comphenix.protocol.events.ListeningWhitelist;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.events.PacketListener;
import com.comphenix.protocol.injector.GamePhase;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.game.Game;
import com.pvpdojo.util.bukkit.ItemUtil;

import net.minecraft.server.v1_7_R4.PacketPlayInBlockDig;

public class BlockListener implements DojoListener, PacketListener {

    @Override
    public void register(Plugin plugin) {
        DojoListener.super.register(plugin);
        ProtocolLibrary.getProtocolManager().addPacketListener(this);
    }

    @EventHandler
    public void onBuild(BlockPlaceEvent e) {
        if (e.getBlockPlaced().getLocation().getBlockY() >= 128) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onSecretBreak(BlockBreakEvent e) {
        if (e.getBlock().getType() == Material.GOLD_BLOCK && ItemUtil.PICKAXES.contains(e.getPlayer().getItemInHand().getType())) {
            e.getBlock().getDrops().forEach(itemStack -> e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(), itemStack));
            e.getBlock().setType(Material.AIR);
        }
    }

    private static final Set<UUID> digging = new HashSet<>();

    @EventHandler
    public void onObiBreak(BlockDamageEvent e) {
        if (e.getBlock().getType() == Material.OBSIDIAN) {
            digging.add(e.getPlayer().getUniqueId());
            e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 60 * 20, 99), true);
        }
    }

    @Override
    public void onPacketSending(PacketEvent packetEvent) {}

    @Override
    public void onPacketReceiving(PacketEvent packetEvent) {
        PacketPlayInBlockDig packet = (PacketPlayInBlockDig) packetEvent.getPacket().getHandle();
        if (packet.g() == 1 || packet.g() == 2) {
            PvPDojo.schedule(() -> {
                if (digging.remove(packetEvent.getPlayer().getUniqueId())) {
                    packetEvent.getPlayer().removePotionEffect(PotionEffectType.FAST_DIGGING);
                }
            }).sync();
        }
    }

    @Override
    public ListeningWhitelist getSendingWhitelist() {
        return ListeningWhitelist.EMPTY_WHITELIST;
    }

    @Override
    public ListeningWhitelist getReceivingWhitelist() {
        return ListeningWhitelist.newBuilder().normal().gamePhase(GamePhase.PLAYING).options(new ListenerOptions[] {}).types(Client.BLOCK_DIG).build();
    }

    @Override
    public Plugin getPlugin() {
        return Game.inst();
    }
}
