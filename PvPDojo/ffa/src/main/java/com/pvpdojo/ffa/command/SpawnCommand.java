/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.ffa.command;

import java.util.concurrent.TimeUnit;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.ffa.listeners.JoinListener;
import com.pvpdojo.ffa.userdata.FFAUser;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.TimeUtil;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("spawn")
public class SpawnCommand extends DojoCommand {

    @CatchUnknown
    @Default
    public void onSpawn(User user) {
        if (!user.isInCombat() && TimeUtil.hasPassed(((FFAUser) user).getLastSpawnCommand(), TimeUnit.SECONDS, 45)) {
            ((FFAUser) user).setLastSpawnCommand(System.currentTimeMillis());
            JoinListener.spawnPlayer(user.getPlayer());
        } else {
            user.sendMessage(MessageKeys.CANNOT_USE_IN_COMBAT);
        }
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
