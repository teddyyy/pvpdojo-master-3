/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.ffa.listeners;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerPickupItemEvent;

import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.userdata.PlayStyle;

public class DropListener implements DojoListener {

    @EventHandler
    public void onSurvivorPickup(PlayerPickupItemEvent e) {
        AbilityPlayer ap = AbilityPlayer.get(e.getPlayer());

        if (ap.getPlayStyle() == PlayStyle.SURVIVALIST) {
            if (e.getItem().getItemStack().getType() == Material.GOLD_NUGGET) {
                e.setCancelled(true);
            }
        }
    }

}
