/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.ffa.userdata;

import java.util.UUID;

import org.bukkit.entity.Player;

import com.pvpdojo.ffa.FFA;
import com.pvpdojo.userdata.impl.UserImpl;
import com.pvpdojo.userdata.KitData;

import lombok.Getter;
import lombok.Setter;

public class FFAUser extends UserImpl {

    public static FFAUser getUser(Player player) {
        return FFA.inst().getUser(player);
    }

    private KitData kitCache;
    @Getter
    @Setter
    private long lastSpawnCommand;

    public FFAUser(UUID uuid) {
        super(uuid);
    }

    public KitData getKitCache() {
        return kitCache;
    }

    public void setKitCache(KitData kitCache) {
        this.kitCache = kitCache;
    }

}
