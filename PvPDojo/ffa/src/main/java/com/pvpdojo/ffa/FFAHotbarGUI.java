/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.ffa;

import org.bukkit.Material;

import com.pvpdojo.ServerType;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.ffa.userdata.FFAUser;
import com.pvpdojo.gui.HotbarGUI;
import com.pvpdojo.menus.KitSelectorMenu;
import com.pvpdojo.userdata.KitAbilityData;
import com.pvpdojo.userdata.KitData;
import com.pvpdojo.userdata.PlayStyle;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.StringUtils;

public class FFAHotbarGUI extends HotbarGUI {

    private static final FFAHotbarGUI INST = new FFAHotbarGUI();

    public static FFAHotbarGUI get() {
        return INST;
    }

    private FFAHotbarGUI() {
        setItem(0,  new ItemBuilder(Material.ANVIL).name(CC.GREEN + "Select Kit").build(), onRightClick(player -> {
            new KitSelectorMenu(player, true, data -> {
                FFAUser.getUser(player).setKitCache(data);
                player.sendMessage(CC.GRAY + StringUtils.getLS());
                player.sendMessage(CC.GRAY + "You selected: " + CC.GREEN + data.getName());
                player.sendMessage(CC.GRAY + "Playstyle: " + CC.DARK_PURPLE + data.getPlayStyle().getName());
                player.sendMessage(CC.GRAY + "Abilities: " + listAbilities(data));
                player.sendMessage(CC.GRAY + StringUtils.getLS());
                User.getUser(player).getMenu().closeSafely();
            }, true, FFA.inst().getServerType() == ServerType.CLASSIC ? PlayStyle.SOUP : null).open();
        }));
        setItem(8, new ItemBuilder(Material.REDSTONE).name(CC.RED + "Return to Hub").build(), onRightClick(ServerType.HUB::connect));
    }

    private static String listAbilities(KitData data) {
        StringBuilder abilities = new StringBuilder();
        for (KitAbilityData ability : data.getAbilities()) {
            AbilityLoader loader = AbilityLoader.getAbilityData(ability.getId());
            abilities.append(loader.getRarity().getColor()).append(loader.getName()).append(", ");
        }
        if (abilities.length() != 0)
            return abilities.substring(0, abilities.length() - 2);
        return abilities.toString();
    }

}
