/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.ffa.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.bukkit.events.RegionEnterEvent;
import com.pvpdojo.bukkit.events.RegionLeaveEvent;
import com.pvpdojo.ffa.userdata.FFAUser;
import com.pvpdojo.userdata.KitData;
import com.pvpdojo.userdata.PlayStyle;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;

public class RegionChangeListener implements Listener {

    public static void register() {
        Bukkit.getPluginManager().registerEvents(new RegionChangeListener(), PvPDojo.get());
    }

    @EventHandler
    public void onRegionEnter(RegionEnterEvent e) {
        if (User.getUser(e.getPlayer()).isAdminMode()) {
            return;
        }
        if (e.getRegion().getId().startsWith("safe")) {
            AbilityPlayer cp = AbilityPlayer.get(e.getPlayer());
            if (cp.isTargetable()) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onRegionLeave(RegionLeaveEvent e) {
        Player player = e.getPlayer();
        FFAUser user = FFAUser.getUser(player);

        if (user.isAdminMode()) {
            return;
        }

        if (e.getRegion().getId().startsWith("bleibhier")) {
            e.setCancelled(true);
        }

        if (e.getRegion().getId().startsWith("safe")) {
            AbilityPlayer champion = AbilityPlayer.get(player);
            KitData data = user.getPersistentData().getKits().get(0);

            user.setHotbarGui(null);

            player.sendMessage(PvPDojo.WARNING + CC.RED + "Teaming and Trucing is a bannable offence!");

            if (champion.getKit() != null) {
                user.setKitCache(champion.getKit());
                return;
            }
            if (user.getKitCache() != null) {
                champion.selectChampion(user.getKitCache());
                return;
            }

            if (user.getPersistentData().getKits().isEmpty()) {
                return;
            }

            if (PvPDojo.SERVER_TYPE == ServerType.CLASSIC) {
                data.setPlayStyle(PlayStyle.SOUP);
            }

            champion.selectChampion(data.getId());
        }
    }

}
