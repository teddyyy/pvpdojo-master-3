/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.ffa.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.pvpdojo.PvPDojo;

public class RespawnListener implements Listener {
    public static void register() {
       Bukkit.getPluginManager().registerEvents(new RespawnListener(), PvPDojo.get());
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        e.setRespawnLocation(JoinListener.spawnPlayer(e.getPlayer()));
    }

}
