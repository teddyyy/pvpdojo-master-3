/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.ffa;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import com.pvpdojo.UserList;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.abilities.Abilities;
import com.pvpdojo.command.lib.DojoCommandManager;
import com.pvpdojo.extension.DojoServer;
import com.pvpdojo.ffa.command.SpawnCommand;
import com.pvpdojo.ffa.listeners.DropListener;
import com.pvpdojo.ffa.listeners.JoinListener;
import com.pvpdojo.ffa.listeners.RegionChangeListener;
import com.pvpdojo.ffa.listeners.RespawnListener;
import com.pvpdojo.ffa.userdata.FFAUser;
import com.pvpdojo.map.DojoMap;
import com.pvpdojo.map.MapLoader;
import com.pvpdojo.map.MapType;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;

public class FFA extends DojoServer<FFAUser> {

    private static FFA instance;

    public static FFA inst() {
        return instance;
    }

    @Override
    public void onLoad() {
        super.onLoad();
        instance = this;
        deleteFolder("world");
    }

    @Override
    public void onEnable() {
        super.onEnable();
    }

    @Override
    public ServerType getServerType() {
        if (getConfig().getBoolean("ffa")) {
            return ServerType.FFA;
        } else {
            return ServerType.CLASSIC;
        }
    }

    public void start() {
        PvPDojo.get().getServerSettings().setItemDespawnRate(Bukkit.getWorlds().get(0), 140);
        loadListeners();
        clearRegions(Bukkit.getWorlds().get(0));
    }

    @Override
    public void postLoad() {
        Abilities.get().start();
        loadCommands();
    }

    @Override
    public DojoMap setupMap() {
        DojoMap map;
        if (PvPDojo.SERVER_TYPE == ServerType.CLASSIC) {
            map = MapLoader.getMap(MapType.DEFAULT, "classic");
        } else {
            map = MapLoader.getMap(MapType.DEFAULT, "ffa");
        }
        map.setWorld(Bukkit.getWorlds().get(0));
        map.shift(-map.getLocations()[0].getBlockX(), 0, -map.getLocations()[0].getBlockZ()); // Move spawn to 0,0
        map.setup(0);
        map.placeBlocks(false);
        map.getRegion().setFlag(DefaultFlag.PVP, StateFlag.State.ALLOW);
        return map;
    }

    public Location getSpawn() {
        return PvPDojo.get().getStandardMap().getLocations()[0];
    }

    @Override
    public UserList<FFAUser> createUserList() {
        return new UserList<>(FFAUser::new);
    }

    public void loadListeners() {
        new JoinListener().register();
        new DropListener().register();
        RespawnListener.register();
        RegionChangeListener.register();
    }

    public void loadCommands() {
        DojoCommandManager.get().registerCommand(new SpawnCommand());
    }
}