/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.ffa.listeners;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.bukkit.events.DojoPlayerJoinEvent;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.ffa.FFA;
import com.pvpdojo.ffa.FFAHotbarGUI;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class JoinListener implements DojoListener {

    @EventHandler
    public void onJoin(DojoPlayerJoinEvent e) {
        spawnPlayer(e.getPlayer());
    }

    public static Location spawnPlayer(final Player player) {
        PlayerUtil.resetPlayer(player);
        Location loc = FFA.inst().getSpawn();
        player.closeInventory();
        AbilityPlayer.get(player).die();
        player.teleport(loc);
        User.getUser(player).setHotbarGui(FFAHotbarGUI.get());
        return loc;
    }
}
