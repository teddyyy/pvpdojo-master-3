/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.deathmatch.session;

import static com.pvpdojo.util.StreamUtils.negate;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.challenges.ChallengeManager;
import com.pvpdojo.challenges.DojoChallenge;
import com.pvpdojo.deathmatch.Deathmatch;
import com.pvpdojo.deathmatch.userdata.DeathmatchUser;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.menus.KitSelectorMenu;
import com.pvpdojo.session.GameSession;
import com.pvpdojo.session.PastGameSession.AfterFightSnapshot;
import com.pvpdojo.session.RelogSession;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.arena.Arena;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.entity.TeamArenaEntity;
import com.pvpdojo.session.network.component.PokerComponent;
import com.pvpdojo.userdata.KitData;
import com.pvpdojo.userdata.PlayStyle;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Countdown;
import com.pvpdojo.util.DojoRunnable;
import com.pvpdojo.util.bukkit.CC;
import com.sk89q.worldguard.bukkit.BukkitUtil;

import net.md_5.bungee.api.chat.TextComponent;

public abstract class DeathmatchSession extends GameSession implements RelogSession<DeathmatchRelogData, User> {

    // default is enabled
    private boolean abilities = true;
    private boolean reducedCountdown;
    private Countdown countdown;
    private DojoRunnable awaitingPlayersRunnable;
    private PlayStyle playStyle;

    public DeathmatchSession(Arena arena, SessionType type, ArenaEntity... participants) {
        super(arena, type, participants);
        setState(SessionState.PREGAME);
        awaitingPlayersRunnable = new Countdown(15).finish(this::end).start();
    }

    @Override
    public void addParticipant(ArenaEntity ae) {
        super.addParticipant(ae);
        if (ae instanceof TeamArenaEntity) {
            return;
        }
        if (!Deathmatch.PREPARED_SESSIONS.values().contains(this) && ae.getPlayer() != null) {
            start();
        } else {
            ArenaEntity ref = ae;
            if (isTeamFight()) {
                ref = ae.getTeam();
            }
            ae.teleport(getArena().getLocations()[getParticipants().indexOf(ref) % getArena().getLocations().length]);
        }
    }

    public abstract DojoChallenge getBaseChallenge();

    public abstract DojoChallenge getWinChallenge();

    @Override
    public void start() {
        awaitingPlayersRunnable.cancel();
        setState(SessionState.INVINCIBILITY);

        if (getParticipants().size() == 2) {
            broadcast(LocaleMessage.of(PvPDojo.PREFIX + CC.BLUE + getParticipants().get(0).getName() + CC.GRAY + " vs. " + CC.BLUE + getParticipants().get(1).getName()));
        }
        broadcast(LocaleMessage.of(PvPDojo.PREFIX + "Arena: " + CC.GOLD + getArena().getName()));
        super.start();

        PvPDojo.schedule(() -> {
            if (abilities) {
                executeForPlayers(p -> new KitSelectorMenu(p, false, AbilityPlayer.get(p)::selectChampion, true, playStyle).open());
            } else {
                if (playStyle != null) {
                    KitData emptyPlayStyle = new KitData("", 1);
                    emptyPlayStyle.setPlayStyle(playStyle);
                    executeForPlayers(player -> AbilityPlayer.get(player).selectChampion(emptyPlayStyle));
                } else {
                    executeForPlayers(player -> AbilityPlayer.get(player).selectChampion(KitData.EMTPY_DATA));
                }
            }
            countdown.modCounter("Deathmatch starting in", getPlayers());
        }).createTask(5); // Delay that by 5 ticks to ensure clients are ready to accept such packets

        countdown = new Countdown(abilities && !reducedCountdown ? 7 : 3)
                .counter(count -> broadcast(MessageKeys.DEATHMATCH_COUNTDOWN, "{timestring}", count + "" + CC.GRAY + (count == 1 ? " second" : " seconds")))
                .finish(() -> {
                    if (!getArena().isBlocksGenerated()) {
                        broadcast(LocaleMessage.of(CC.RED + "Arena is not fully generated restarting countdown"));
                        countdown.setCurrentCount(10).start();
                        return;
                    }
                    setState(SessionState.STARTED);
                    getParticipants().forEach(entity -> entity.setFrozen(false));
                })
                .start();
    }

    @Override
    public void end(ArenaEntity... losers) {
        // Clean up possibly timed out session
        Deathmatch.PREPARED_SESSIONS.values().remove(this);

        if (countdown != null) {
            countdown.cancel();
            if (countdown.getCurrentCount() != 0) {
                countdown.getFinish().run();
            }
        }

        getPlayers().forEach(player -> {
            DeathmatchUser user = DeathmatchUser.getUser(player);

            if (user != null && user.getSession() == this) {
                if (user.isDisconnectAfterSession()) {
                    PvPDojo.schedule(() -> {
                        if (!user.isLoggedOut()) {
                            ServerType.HUB.connect(player);
                        }
                    }).createTask(50);
                }
                PvPDojo.schedule(() -> {
                    if (!user.isLoggedOut() && user.getSession() == null && getArena().getRegion().contains(BukkitUtil.toVector(player.getLocation()))) {
                        user.setSpectator(true);
                    }
                }).createTask(5 * 20);
            }
        });

        super.end(losers);
        getPlayers().stream().map(User::getUser).forEach(usr -> ChallengeManager.get().callChallenge(usr, getBaseChallenge(), 1));
    }

    @Override
    public void postDeath(ArenaEntity entity) {
        super.postDeath(entity);
        triggerEnd(getCurrentlyDead().stream().filter(getParticipants()::contains).toArray(ArenaEntity[]::new));
    }

    @Override
    public boolean checkEndCondition() {
        return getParticipants().stream().filter(negate(getCurrentlyDead()::contains)).count() <= 1;
    }

    @Override
    public void prepareEntity(ArenaEntity entity) {
        entity.setFrozen(true);
    }

    @Override
    public void onWin(ArenaEntity winner) {
        broadcast(MessageKeys.DEATHMATCH_WON, "{winner}", winner.getLongName(), "{color}", (winner.getTeam() != null ? winner.getTeam().getColor() : CC.BLUE).toString());
        winner.getPlayers().stream().map(User::getUser).forEach(usr -> ChallengeManager.get().callChallenge(usr, getWinChallenge(), 1));
    }

    @Override
    public void onLose(ArenaEntity loser) {}

    @Override
    public void handleMatchDetails(TextComponent tc) {
        getParticipants().forEach(entity -> entity.sendMessage(tc));
    }

    public boolean isAbilities() {
        return abilities;
    }

    public void setAbilities(boolean abilities) {
        this.abilities = abilities;
    }

    public boolean isReducedCountdown() {
        return reducedCountdown;
    }

    public void setReducedCountdown(boolean reducedCountdown) {
        this.reducedCountdown = reducedCountdown;
    }

    public PlayStyle getPlayStyle() {
        return playStyle;
    }

    public void setPlayStyle(PlayStyle playStyle) {
        this.playStyle = playStyle;
    }

    @Override
    public boolean isPersistent() {
        return true;
    }

    @Override
    public DeathmatchRelogData createRelogData(User user) {
        return new DeathmatchRelogData(user);
    }

    @Override
    public int getRelogTimeout() {
        return 60;
    }

    @Override
    public boolean hasNPC() {
        return true;
    }

    @Override
    public boolean isAllowCombatLog() {
        return true;
    }

    @Override
    public void onLogout(User user) {
        broadcast(LocaleMessage.of(CC.RED + user.getName() + " [CombatLogger]: " + "They have " + getRelogTimeout() + " seconds to reconnect"));
    }

    @Override
    public void onTimeout(User user) {}

    @Override
    public void handleSpecialComponentGameSpecific() {
        if (getSpecialComponent() instanceof PokerComponent) {
            PokerComponent component = (PokerComponent) getSpecialComponent();

            if (getState() != SessionState.FINISHED) {
                setAbilities(false);
                executeForPlayers(player -> DeathmatchUser.getUser(player).setDisconnectAfterSession(false));
                PvPDojo.schedule(() -> executeForPlayers(player -> AbilityPlayer.get(player).selectChampion(component.getKit(player.getUniqueId())))).createTask(10);
            } else {
                Player loser = Bukkit.getPlayer(component.getLoserData().getUUID());
                if (loser != null) {
                    ServerType.HUB.connect(loser);
                }
            }
        }
    }

    @Override
    public AfterFightSnapshot getSnapshot(Player player) {
        AbilityPlayer abilityPlayer = AbilityPlayer.get(player);
        AfterFightSnapshot snapshot = super.getSnapshot(player);
        snapshot.setKits(CC.stripColor(abilityPlayer.listAbilities()));
        return snapshot;
    }

}
