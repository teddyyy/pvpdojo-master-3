/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.deathmatch;

import java.util.function.Predicate;

import com.pvpdojo.map.MapType;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.arena.ArenaManager;
import com.pvpdojo.session.arena.DeathmatchArena;
import com.pvpdojo.session.arena.FeastFightArena;

public class DeathmatchArenaManager {

    private static DeathmatchArenaManager instance = new DeathmatchArenaManager();

    public static DeathmatchArenaManager get() {
        return instance;
    }

    private ArenaManager<DeathmatchArena> deathmatchArenaManager = new ArenaManager<>(MapType.DEATHMATCH);
    private ArenaManager<FeastFightArena> feastFightArenaManager = new ArenaManager<>(MapType.FEAST_FIGHT);

    private ArenaManager<DeathmatchArena> clanFightArenaManager = new ArenaManager<>(MapType.DEATHMATCH, DeathmatchArena.CLAN_MAP_FLAG);

    public synchronized DeathmatchArena getRandomFreeArena(Predicate<? extends DeathmatchArena> filter, SessionType type) {
        ArenaManager<? extends DeathmatchArena> mapPool;
        if (type == SessionType.FEAST_FIGHT) {
            mapPool = feastFightArenaManager;
        } else if (type == SessionType.TEAM_DUEL_RANKED) {
            mapPool = clanFightArenaManager;
        } else {
            mapPool = deathmatchArenaManager;
        }

        return mapPool.getRandomFreeArena((Predicate) filter);
    }

}
