/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.deathmatch.listeners;

import org.bukkit.event.EventHandler;

import com.pvpdojo.bukkit.events.RegionEvent.MovementWay;
import com.pvpdojo.bukkit.events.RegionLeaveEvent;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.userdata.User;

public class RegionChangeListener implements DojoListener {

    @EventHandler
    public void onRegionChange(RegionLeaveEvent e) {
        User user = User.getUser(e.getPlayer());

        if (user.getSession() != null) {
            int localId = user.getSession().getArena().getLocalId();
            String[] regionSplit = e.getRegion().getId().split("_");
            if (regionSplit.length > 1 && regionSplit[0].equals("bleibhier") && regionSplit[regionSplit.length - 1].equals(String.valueOf(localId))) {
                e.setCancelled(true);
            }
        }

        if (user.isSpectator() && e.getMovementWay() == MovementWay.MOVE) {
            e.setCancelled(true);
        }
    }

}
