/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.deathmatch.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerDropItemEvent;

import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.userdata.User;

public class DropListener implements DojoListener {

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        User user = User.getUser(e.getPlayer());

        if (user.getSession() == null && !user.isAdminMode()) {
            e.getItemDrop().remove();
        }
    }

}
