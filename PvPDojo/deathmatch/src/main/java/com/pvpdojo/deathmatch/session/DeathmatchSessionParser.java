/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.deathmatch.session;

import java.util.Set;
import java.util.UUID;

import com.pvpdojo.deathmatch.DeathmatchArenaManager;
import com.pvpdojo.party.Party;
import com.pvpdojo.party.PartyPacketHandler;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.arena.Arena;
import com.pvpdojo.session.entity.ClanArenaTeam;
import com.pvpdojo.session.entity.PreparedArenaTeam;
import com.pvpdojo.session.network.SessionParser;
import com.pvpdojo.session.network.component.ClanComponent;

public class DeathmatchSessionParser extends SessionParser<DeathmatchSession, Arena> {

    public DeathmatchSessionParser(String[] source) {
        super(source);
    }

    @Override
    public DeathmatchSession parse() {
        DeathmatchSession session = super.parse();
        session.setPlayStyle(playStyle);
        return session;
    }

    @Override
    protected Arena computeArena(Set<String> disabledBySettings) {
        return DeathmatchArenaManager.get().getRandomFreeArena(
                filterArena -> forcedMap != null ? !filterArena.getName().equals(forcedMap) : disabledBySettings.contains(filterArena.getName()),
                reader.getSessionType());
    }

    @Override
    protected Reader getReader(SessionType type) {
        switch (type) {
            case TEAM_DUEL:
            case TEAM_DUEL_RANKED:
                return new DeathmatchTeamReader(type);
            default:
                return new DeathmatchDefaultReader(type);
        }
    }

    protected class DeathmatchDefaultReader extends DefaultReader {

        protected DeathmatchDefaultReader(SessionType sessionType) {
            super(sessionType);
        }

        @Override
        protected DeathmatchSession finish() {
            switch (sessionType) {
                case BOT_DUEL:
                    return new BotSession(arena);
                case DUEL_CASUAL_NO_ABILITY:
                case DUEL_CASUAL:
                case POKER:
                    DeathmatchSession session = new CasualSession(arena, sessionType);
                    if (sessionType == SessionType.DUEL_CASUAL_NO_ABILITY) {
                        session.setAbilities(false);
                    }
                    return session;
                case DUEL_RANKED:
                    return new RankedSession(arena, sessionType);
                case FEAST_FIGHT:
                    return new FeastFightSession(arena);
                default:
                    throw new IllegalArgumentException("Wrong session type");

            }
        }
    }

    protected class DeathmatchTeamReader extends TeamReader {

        protected DeathmatchTeamReader(SessionType sessionType) {
            super(sessionType);
        }

        @Override
        protected DeathmatchSession finish() {
            // Check if party was modified
            players.removeIf(player -> parties.stream().flatMap(party -> party.getUsers().stream()).noneMatch(player::equals));

            for (Party party : parties) {
                if (party.isTemporary()) {
                    PartyPacketHandler.sendDisbandPacket(party);
                }
            }

            if (sessionType == SessionType.TEAM_DUEL_RANKED) {
                return new RankedTeamSession(arena, sessionType,
                        parties.stream()
                               .map(party -> new ClanArenaTeam(party.getLeader(), ((ClanComponent) specialComponent).getPartyClanMap().get(party.getPartyId()), colors.remove(0),
                                       party.getUsers().stream()
                                            .filter(players::contains)
                                            .toArray(UUID[]::new)))
                               .toArray(ClanArenaTeam[]::new));
            } else {
                return new TeamSession(arena, sessionType,
                        parties.stream()
                               .map(party -> new PreparedArenaTeam(party.getLeader(), colors.remove(0),
                                       party.getUsers().stream()
                                            .filter(players::contains)
                                            .toArray(UUID[]::new)))
                               .toArray(PreparedArenaTeam[]::new));
            }
        }
    }

}
