/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.deathmatch.userdata;

import java.util.EnumSet;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.pvpdojo.deathmatch.Deathmatch;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.userdata.impl.UserImpl;

public class DeathmatchUser extends UserImpl {

    public static DeathmatchUser getUser(Player player) {
        return Deathmatch.inst().getUser(player);
    }

    public static DeathmatchUser getUser(UUID uuid) {
        return Deathmatch.inst().getUser(uuid);
    }

    private boolean disconnectAfterSession = true;

    public DeathmatchUser(UUID uuid) {
        super(uuid);
    }

    public boolean isDisconnectAfterSession() {
        return disconnectAfterSession;
    }

    public void setDisconnectAfterSession(boolean disconnectAfterSession) {
        this.disconnectAfterSession = disconnectAfterSession;
    }

    @Override
    public EnumSet<Material> getBypassBlockedInteraction() {
        if (getSession() != null && getSession().getType() == SessionType.FEAST_FIGHT) {
            return EnumSet.of(Material.CHEST);
        }
        return super.getBypassBlockedInteraction();
    }
}
