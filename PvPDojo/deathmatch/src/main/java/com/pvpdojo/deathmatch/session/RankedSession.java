/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.deathmatch.session;


import com.pvpdojo.PvPDojo;
import com.pvpdojo.achievement.Achievement;
import com.pvpdojo.achievement.AchievementManager;
import com.pvpdojo.challenges.DojoChallenge;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.arena.Arena;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.entity.RankedArenaEntity;
import com.pvpdojo.userdata.EloRank;
import com.pvpdojo.util.BroadcastUtil;
import com.pvpdojo.util.bukkit.CC;

public class RankedSession extends CasualSession {

    public RankedSession(Arena arena, SessionType type, ArenaEntity... participants) {
        super(arena, type, participants);
        setRanked(true);
        setAbilities(true);
    }

    @Override
    public DojoChallenge getWinChallenge() {
        return DojoChallenge.T2_DAILY_RANKED1V1WIN;
    }

    @Override
    public void endRanked(RankedArenaEntity winner, ArenaEntity... losers) {
        if (losers.length == 1) {
            ArenaEntity loser = losers[0];
            if (loser instanceof RankedArenaEntity) {
                EloRank prevWinner = EloRank.getRank(winner.getElo());
                EloRank prevLoser = EloRank.getRank(((RankedArenaEntity) loser).getElo());

                int prevWinnerElo = winner.getElo();
                int prevLoserElo = ((RankedArenaEntity) loser).getElo();

                if (prevWinner.ordinal() < EloRank.getRank(((RankedArenaEntity) loser).getElo()).ordinal()) {
                    AchievementManager.inst().trigger(Achievement.SMURF, winner.getPlayer());
                }
                AchievementManager.inst().trigger(Achievement.ELO_HELL, loser.getPlayer());

                int eloChange = calcEloChange(winner.getElo(), ((RankedArenaEntity) loser).getElo());
                pendingSave.setEloChange(eloChange);
                winner.addElo(eloChange);
                EloRank afterWinner = EloRank.getRank(winner.getElo());

                if (afterWinner == EloRank.GLOBAL && afterWinner != prevWinner) {
                    BroadcastUtil.global(ChatChannel.GLOBAL, PvPDojo.PREFIX + CC.BLUE + winner.getName() + CC.RED + " just mastered their skill by ranking up to "
                            + CC.LIGHT_PURPLE + "MC Global");
                }
                ((RankedArenaEntity) loser).addElo(-eloChange);
                broadcast(LocaleMessage.of(CC.GREEN + winner.getName() + " (" + winner.getElo() + ") (+" + eloChange + ") " + CC.GRAY + "defeated " + CC.RED + loser.getName() + " ("
                        + ((RankedArenaEntity) loser).getElo() + ") (-" + eloChange + ")"));

                EloRank afterLoser = EloRank.getRank(((RankedArenaEntity) loser).getElo());

                int afterWinnerElo = winner.getElo();
                int afterLoserElo = ((RankedArenaEntity) loser).getElo();

                //Rankup
                if (afterWinner != prevWinner && afterWinnerElo > prevWinnerElo) {
                    winner.sendMessage(MessageKeys.ELO_RANKUP,
                            "{oldrank}", prevWinner.getColor() + prevWinner.getName(), "{newrank}",
                            afterWinner.getColor() + afterWinner.getName());

                }
                //Derank
                if (afterLoser != prevLoser && afterLoserElo < prevLoserElo) {
                    loser.sendMessage(MessageKeys.ELO_DERANK,
                            "{oldrank}", prevLoser.getColor() + prevLoser.getName(), "{newrank}",
                            afterLoser.getColor() + afterLoser.getName());
                }
                return;
            }
        }
        broadcast(LocaleMessage.of(CC.RED + "Not changing any elo end state is incorrect"));
    }

    @Override
    public void setState(SessionState state) {
        super.setState(state);
        if (state == SessionState.STARTED) {
            broadcast(MessageKeys.RANKED_WARNING);
        }
    }

    public static int calcEloChange(int winnerElo, int loserElo) {

        final int oldElo = winnerElo;
        final double kFactor = 38;
        final double winEstimated = 1D / (1D + Math.pow(10, ((loserElo - winnerElo) / 400D)));
        final double eloChange = kFactor * (1 - winEstimated);

        winnerElo += (int) eloChange;

        return (winnerElo - oldElo) > 0 ? (winnerElo - oldElo) : 1;
    }

}
