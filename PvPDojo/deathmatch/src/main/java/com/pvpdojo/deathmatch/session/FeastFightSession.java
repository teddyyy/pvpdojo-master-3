/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.deathmatch.session;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionType;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.listeners.events.DojoSelectKitEvent;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.session.GameSession;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.arena.Arena;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.Countdown;
import com.pvpdojo.util.bukkit.ItemBuilder.PotionBuilder;
import com.pvpdojo.util.RandomInventory;

public class FeastFightSession extends CasualSession implements DojoListener {


    private static boolean armor;

    public FeastFightSession(Arena arena, ArenaEntity... participants) {
        super(arena, SessionType.FEAST_FIGHT, participants);
        if (!armor) {
            armor = true;
            register();
        }
    }

    @Override
    public void start() {
        super.start();
        getArena().addBlocksGeneratedTask(() -> getArena().forEachChest(chest -> chest.getInventory().clear()));
    }

    @Override
    public void prepareEntity(ArenaEntity entity) {
        super.prepareEntity(entity);
        entity.getPlayers().forEach(player -> player.getInventory().setArmorContents(IRON_ARMOR));
    }

    @Override
    public void setState(SessionState state) {
        super.setState(state);
        if (state == SessionState.STARTED) {
            new Countdown(30).userCounter(count -> broadcast(LocaleMessage.of(CC.RED + "Feast starting in " + count + " seconds"))).finish(this::refillChests).start();
        }
    }

    public void refillChests() {
        getArena().forEachChest(chest -> {
            chest.getBlockInventory().clear();
            for (int i = 0; i < PvPDojo.RANDOM.nextInt(7) + 4; i++) {
                chest.getInventory().setItem(PvPDojo.RANDOM.nextInt(chest.getInventory().getSize()), FEAST.generateRandomItem());
            }
        });
    }



    public static final ItemStack[] IRON_ARMOR = new ItemStack[] { new ItemStack(Material.IRON_BOOTS), new ItemStack(Material.IRON_LEGGINGS),
            new ItemStack(Material.IRON_CHESTPLATE), new ItemStack(Material.IRON_HELMET) };

    public static final RandomInventory<ItemStack> FEAST = RandomInventory.randomItemStacks()
            .addItem(new PotionBuilder(PotionType.STRENGTH).level(2).splash().build(), 5)
            .addItem(new ItemStack(Material.DIAMOND_SWORD), 30)
            .addItem(new ItemStack(Material.IRON_SWORD), 100)
            .addItem(new ItemStack(Material.DIAMOND_HELMET), 30)
            .addItem(new ItemStack(Material.DIAMOND_CHESTPLATE), 30)
            .addItem(new ItemStack(Material.DIAMOND_LEGGINGS), 30)
            .addItem(new ItemStack(Material.DIAMOND_BOOTS), 30)
            .addItem(new ItemStack(Material.ENDER_PEARL), 40, 1, 3)
            .addItem(new ItemStack(Material.BOW), 50)
            .addItem(new ItemStack(Material.ARROW), 65, 5, 12)
            .addItem(new PotionBuilder(PotionType.INSTANT_DAMAGE).splash().build(), 65)
            .addItem(new PotionBuilder(PotionType.POISON).splash().build(), 65)
            .addItem(new PotionBuilder(PotionType.REGEN).splash().extend().build(), 65)
            .addItem(new ItemStack(Material.MUSHROOM_SOUP), 70, 4, 8);

    @EventHandler
    public void onKitSelect(DojoSelectKitEvent event) {
        GameSession session = User.getUser(event.getEntity().getUUID()).getSession();
        if (session != null && session.getType() == SessionType.FEAST_FIGHT) {
            event.getEntity().getPlayer().getInventory().setArmorContents(IRON_ARMOR);
        }
    }

}
