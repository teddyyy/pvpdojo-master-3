/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.deathmatch.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import com.pvpdojo.bukkit.events.DataLoadedEvent;
import com.pvpdojo.bukkit.events.DojoPlayerJoinEvent;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.deathmatch.Deathmatch;
import com.pvpdojo.deathmatch.session.DeathmatchSession;
import com.pvpdojo.deathmatch.userdata.DeathmatchUser;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class JoinListener implements DojoListener {

    @EventHandler
    public void onDataLoaded(DataLoadedEvent e) {
        User user = User.getUser(e.getPlayer());
        if (!e.getData().getRank().inheritsRank(Rank.HELPER) && user.getSession() == null) {
            user.kick("Cannot access server. No valid session found.");
        } else if (user.getSession() == null && !user.isVanish()) {
            user.setSpectator(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(DojoPlayerJoinEvent e) {
        DeathmatchUser user = DeathmatchUser.getUser(e.getPlayer());

        DeathmatchSession session = Deathmatch.PREPARED_SESSIONS.remove(e.getPlayer().getUniqueId());
        user.setAutoRespawn(false);
        if (session != null) {

            if (user.getSession() != null) {
                user.setDisconnectAfterSession(false);
                user.getSession().disconnect(user);
                user.setDisconnectAfterSession(true);
                PlayerUtil.resetPlayer(user.getPlayer());
            }

            session.addParticipant(user);
        }
    }

}
