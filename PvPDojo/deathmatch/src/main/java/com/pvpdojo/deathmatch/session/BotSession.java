/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.deathmatch.session;

import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.challenges.DojoChallenge;
import com.pvpdojo.npc.DojoBot;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.arena.Arena;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.entity.RankedArenaEntity;
import com.pvpdojo.settings.BotSettings;
import com.pvpdojo.settings.SettingsType;
import com.pvpdojo.userdata.User;
import com.pvpdojo.userdata.impl.UserImpl;
import com.pvpdojo.util.bukkit.CC;

public class BotSession extends DeathmatchSession {

    private DojoBot bot;

    public BotSession(Arena arena, ArenaEntity... participants) {
        super(arena, SessionType.BOT_DUEL, participants);
        setReducedCountdown(true);
        addBot();
    }

    public void addBot() {
        bot = new DojoBot(NMSUtils.get(getArena().getWorld()));
        bot.setCustomName("DojoBot");
        addParticipant(bot);
        bot.useSoups();
    }

    @Override
    public DojoChallenge getBaseChallenge() {
        return null;
    }

    @Override
    public DojoChallenge getWinChallenge() {
        return DojoChallenge.T1_DAILY_DOJOBOT;
    }

    @Override
    public void addParticipant(ArenaEntity ae) {
        super.addParticipant(ae);
        if (ae instanceof UserImpl) {
            ((User) ae).setUseStats(false);
            PvPDojo.newChain().asyncFirst(() -> DBCommon.loadSettings(SettingsType.BOT, ae.getPlayer().getUniqueId()))
                   .syncLast(settings -> {
                       bot.applySettings(settings != null ? settings : new BotSettings());
                       bot.transform(NMSUtils.get(ae.getPlayer()).getProfile(), bot.getBukkitEntity().getLocation(), settings.isOldBot());
                   }).execute();
        }
    }

    @Override
    public void end(ArenaEntity... losers) {
        super.end(losers);
        getPlayers().stream().map(User::getUser).forEach(user -> {
            user.setUseStats(true);
            double soups = bot.getSoups(), hearts = (int) (bot.getBukkitEntity().getHealth() / 2);

            if (!bot.getBukkitEntity().isDead()) {
                user.sendMessage(PvPDojo.PREFIX + "Soups Left: " + CC.BLUE + soups, PvPDojo.PREFIX + "Hearts Left: " + CC.BLUE + hearts);
            }

        });
        if (!bot.getBukkitEntity().isDead()) {
            bot.getBukkitEntity().remove();
        }
    }

    @Override
    public boolean isPersistent() {
        return false;
    }

    @Override
    public void endRanked(RankedArenaEntity winner, ArenaEntity... losers) {}

}
