/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.deathmatch.session;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.arena.Arena;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.entity.PreparedArenaTeam;
import com.pvpdojo.session.entity.TeamArenaEntity;

public class TeamSession extends CasualSession {

    private Map<UUID, PreparedArenaTeam> userToTeam = new HashMap<>();

    public TeamSession(Arena arena, SessionType type, PreparedArenaTeam... participants) {
        super(arena, type, participants);
        for (PreparedArenaTeam team : participants) {
            team.getPotentialPlayers().forEach(uuid -> userToTeam.put(uuid, team));
        }
    }

    @Override
    public void addParticipant(ArenaEntity ae) {
        if (!(ae instanceof TeamArenaEntity)) {
            userToTeam.get(ae.getUniqueId()).addMember(ae);
        }
        super.addParticipant(ae);
    }

}
