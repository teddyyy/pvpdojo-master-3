/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.deathmatch.session;

import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.arena.Arena;
import com.pvpdojo.session.entity.ClanArenaTeam;

public class RankedTeamSession extends TeamSession {

    public RankedTeamSession(Arena arena, SessionType type, ClanArenaTeam... participants) {
        super(arena, type, participants);
    }
}
