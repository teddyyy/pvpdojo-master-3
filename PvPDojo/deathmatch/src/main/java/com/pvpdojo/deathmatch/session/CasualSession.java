/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.deathmatch.session;

import com.pvpdojo.achievement.Achievement;
import com.pvpdojo.achievement.AchievementManager;
import com.pvpdojo.challenges.DojoChallenge;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.arena.Arena;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.entity.RankedArenaEntity;

public class CasualSession extends DeathmatchSession {

    public CasualSession(Arena arena, SessionType type, ArenaEntity... participants) {
        super(arena, type, participants);
    }
    
    @Override
    public DojoChallenge getBaseChallenge() {
        return DojoChallenge.T1_DAILY_DEATHMATCH;
    }
    
    @Override
    public DojoChallenge getWinChallenge() {
        return null;
    }

    @Override
    public void onWin(ArenaEntity winner) {
        super.onWin(winner);
        Achievement achievement = isTeamFight() ? Achievement.TEAM_PLAYER : Achievement.DUEL_PLAYER;
        winner.getPlayers().forEach(player -> AchievementManager.inst().trigger(achievement, player));
    }

    @Override
    public void endRanked(RankedArenaEntity winner, ArenaEntity... losers) {}

}
