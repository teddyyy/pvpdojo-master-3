/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.deathmatch.session;

import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilities.base.entities.AbilityPlayerSnapshot;
import com.pvpdojo.session.RelogData;
import com.pvpdojo.userdata.User;

public class DeathmatchRelogData extends RelogData {

    private AbilityPlayerSnapshot abilityPlayerSnapshot;

    public DeathmatchRelogData(User user) {
        super(user);
        this.abilityPlayerSnapshot = new AbilityPlayerSnapshot(AbilityPlayer.get(user.getPlayer()));
    }

    @Override
    public void apply(User user) {
        super.apply(user);
        abilityPlayerSnapshot.apply(AbilityPlayer.get(user.getPlayer()));
    }
}
