/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.deathmatch.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.pvpdojo.ServerType;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.deathmatch.userdata.DeathmatchUser;

public class RespawnListener implements DojoListener {

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        e.setRespawnLocation(e.getPlayer().getLocation());
        DeathmatchUser user = DeathmatchUser.getUser(e.getPlayer());
        user.setSpectator(true);
        // player respawned and got nothing more to do other than wait for timeout, let's help them a bit
        if (user.getSession() == null) {
            if (user.isDisconnectAfterSession()) {
                ServerType.HUB.connect(e.getPlayer());
            }
        }
    }

}
