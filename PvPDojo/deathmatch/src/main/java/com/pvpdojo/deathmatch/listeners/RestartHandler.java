/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.deathmatch.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.server.ServerListPingEvent;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.DojoServerRestartEvent;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.session.GameSession;

public class RestartHandler implements DojoListener {

    private boolean restarting;

    @EventHandler
    public void onRestart(DojoServerRestartEvent e) {
        PvPDojo.schedule(() -> restarting = true).createTask((e.getSecondsTillRestart() - 60) * 20);
        e.addRestartCondition(() -> GameSession.getSessions().isEmpty());
    }

    @EventHandler
    public void onPing(ServerListPingEvent e) {
        if (restarting) {
            e.setMotd("closing");
        }
    }

}
