/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.deathmatch;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;

import com.pvpdojo.UserList;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.abilities.Abilities;
import com.pvpdojo.deathmatch.listeners.DropListener;
import com.pvpdojo.deathmatch.listeners.JoinListener;
import com.pvpdojo.deathmatch.listeners.RegionChangeListener;
import com.pvpdojo.deathmatch.listeners.RespawnListener;
import com.pvpdojo.deathmatch.listeners.RestartHandler;
import com.pvpdojo.deathmatch.session.DeathmatchSession;
import com.pvpdojo.deathmatch.userdata.DeathmatchUser;
import com.pvpdojo.extension.DojoServer;
import com.pvpdojo.map.DojoMap;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;

public class Deathmatch extends DojoServer<DeathmatchUser> {

    public static final Map<UUID, DeathmatchSession> PREPARED_SESSIONS = new ConcurrentHashMap<>();
    public static final DeathmatchSync SYNC = new DeathmatchSync();

    private static Deathmatch instance;

    public static Deathmatch inst() {
        return instance;
    }

    @Override
    public void onLoad() {
        instance = this;
        super.onLoad();
        deleteFolder("world");
    }

    @Override
    public ServerType getServerType() {
        return ServerType.DEATHMATCH;
    }

    @Override
    public void onDisable() {
        SYNC.unsubscribe();
    }

    @Override
    public void start() {
        clearRegions(Bukkit.getWorlds().get(0));
        loadListeners();
        PvPDojo.schedule(DeathmatchSync::postPlayerCount).createTimerAsync(20, -1);
    }

    @Override
    public void postLoad() {
        //noinspection resource
        new Thread(() -> Redis.getResource().subscribe(SYNC, Redis.get().getCh(BungeeSync.SESSION)), "DeathmatchSync-Read-Thread").start();
        Abilities.get().start();
    }

    @Override
    public DojoMap setupMap() {
        return null;
    }

    @Override
    public UserList<DeathmatchUser> createUserList() {
        return new UserList<>(DeathmatchUser::new);
    }

    private void loadListeners() {
        new DropListener().register();
        new JoinListener().register();
        new RespawnListener().register();
        new RestartHandler().register();
        new RegionChangeListener().register();
    }

}
