/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.deathmatch;

import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.deathmatch.session.DeathmatchSession;
import com.pvpdojo.deathmatch.session.DeathmatchSessionParser;
import com.pvpdojo.deathmatch.userdata.DeathmatchUser;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.session.GameSession;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.bukkit.PlayerUtil;
import org.bukkit.Bukkit;
import redis.clients.jedis.JedisPubSub;

import java.util.EnumMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.pvpdojo.util.StringUtils.PIPE;

public class DeathmatchSync extends JedisPubSub {

    @Override
    public void onMessage(String channel, String message) {
        try {

            if (Redis.get().isTest()) {
                channel = channel.replaceFirst("test_", "");
            }

            if (Redis.get().isUsProxy()) {
                channel = channel.replaceFirst(BungeeSync.US_PROXY_PREFIX, "");
            }

            if (channel.equals(BungeeSync.SESSION)) {
                String[] split = PIPE.split(message);
                if (Integer.parseInt(split[0]) != BungeeSync.REQUEST) {
                    return;
                }
                if (Long.parseLong(split[1]) != PvPDojo.SERVER_ID) {
                    return;
                }
                int requestId = Integer.parseInt(split[2]);

                DeathmatchSessionParser builder = new DeathmatchSessionParser(split);
                DeathmatchSession session = builder.parse();

                builder.getPlayers().forEach(uuid -> {
                    DeathmatchUser user = DeathmatchUser.getUser(uuid);
                    if (user != null && user.getPlayer().isOnline()) {
                        PvPDojo.schedule(() -> {
                            if (user.getSession() != null) {
                                user.setDisconnectAfterSession(false);
                                user.getSession().disconnect(user);
                                user.setDisconnectAfterSession(true);
                                PlayerUtil.resetPlayer(user.getPlayer());
                            }
                            session.addParticipant(user);
                        }).sync();
                        return;
                    }
                    Deathmatch.PREPARED_SESSIONS.put(uuid, session);
                });
                Redis.get().publish(BungeeSync.SESSION, BungeeSync.RESPONSE + "|" + requestId);
            }
        } catch (Throwable t) {
            Log.exception("Handle Redis packet ch: " + channel + " m: " + message, t);
        }
    }

    private static final Map<SessionType, Long> TEMPLATE = new EnumMap<>(SessionType.class);
    private static Map<SessionType, Long> lastPlayerCount = new EnumMap<>(SessionType.class);

    static {
        for (SessionType type : SessionType.values()) {
            TEMPLATE.put(type, 0L);
        }
    }

    public static void postPlayerCount() {
        Map<SessionType, Long> currentPlayerCountPerSession = new EnumMap<>(TEMPLATE);
        currentPlayerCountPerSession.putAll(Bukkit.getOnlinePlayers().stream()
                                                  .map(User::getUser)
                                                  .map(User::getSession)
                                                  .filter(Objects::nonNull)
                                                  .collect(Collectors.groupingBy(GameSession::getType, Collectors.counting())));

        if (lastPlayerCount.equals(currentPlayerCountPerSession)) {
            return;
        }

        lastPlayerCount = currentPlayerCountPerSession;

        Redis.get().publish(BungeeSync.SESSION, BungeeSync.PLAYERCOUNT + "|"
                + PvPDojo.get().getNetworkPointer() + "|"
                + DBCommon.GSON.toJson(currentPlayerCountPerSession));
    }

}
