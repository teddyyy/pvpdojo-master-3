/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.cloud;

import com.pvpdojo.session.SessionType;

public class CloudUtil {

    public static String getChannel(SessionType sessionType) {
        return "cloud_" + sessionType.name().toLowerCase();
    }

}
