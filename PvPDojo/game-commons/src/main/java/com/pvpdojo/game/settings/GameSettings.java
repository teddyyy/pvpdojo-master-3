/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.settings;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import com.pvpdojo.userdata.Rank;

import lombok.Data;

@Data
public abstract class GameSettings implements Cloneable {

    private int maxPlayers = 100;
    private int minPlayers = 2;
    private Rank rankToSpectate = Rank.GREENBELT;
    private boolean randomizePlayers;

    public abstract GameSettingsMenu getMenu(Player player);

    public List<SettingsCheck<?>> runChecks() {
        return new ArrayList<>();
    }

    protected void check(SettingsCheck check, List<SettingsCheck<?>> failedChecks) {
        if (!check.check(this)) {
            failedChecks.add(check);
        }
    }

    @Override
    public final Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }


}
