/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.settings;

import lombok.Getter;

public class NumberCheck implements SettingsCheck {

    private String name;
    @Getter
    private int given;
    private int min;
    private int max;


    public NumberCheck(String name, int given, int min, int max) {
        this.name = name;
        this.given = given;
        this.min = min;
        this.max = max;
    }

    @Override
    public String getMessage() {
        return name + " must be between " + min + " and " + max;
    }

    @Override
    public boolean check(GameSettings settings) {
        return given >= min && given <= max;
    }
}
