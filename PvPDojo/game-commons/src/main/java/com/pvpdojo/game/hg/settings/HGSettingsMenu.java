/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.settings;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.pvpdojo.game.settings.GameSettingsMenu;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class HGSettingsMenu extends GameSettingsMenu<HGSettings> {

    public HGSettingsMenu(Player player, HGSettings settings) {
        super(player, "HG Settings", settings);
    }

    @Override
    public void redraw() {
        super.redraw();

        InventoryPage inv = getCurrentPage();

        inv.setHasBar(true);

        addSetting(inv, new ItemBuilder(Material.STONE_SWORD)
                        .name(colorize("Invincibility", settings.getSecondsTillPvP()))
                        .build(),
                editNumber("Set new invincibility time", settings::setSecondsTillPvP));

        addSetting(inv, new ItemBuilder(Material.CHEST)
                        .name(CC.DARK_AQUA + "Disabled Kits")
                        .lore(settings.getDisabledKits().stream().collect(Collectors.joining(", ", CC.GRAY.toString(), "")),
                                "", CC.GOLD + "Left " + CC.GRAY + "click to add disabled kit", CC.GOLD + "Right " + CC.GRAY + "click to reset")
                        .build(),
                clickType -> {
                    if (clickType == ClickType.LEFT) {
                        getString("Enter the kit to disable", kit -> Arrays.stream(StringUtils.COMMA.split(kit.toLowerCase())).forEach(settings.getDisabledKits()::add)).accept(clickType);
                    } else if (clickType == ClickType.RIGHT) {
                        settings.getDisabledKits().clear();
                    }
                }
        );

        addSetting(inv, new ItemBuilder(Material.PAPER)
                        .name(CC.DARK_AQUA + "Kit Whitelist")
                        .lore(settings.getKitWhiteList().stream().collect(Collectors.joining(", ", CC.GRAY.toString(), "")),
                                "", CC.GOLD + "Left " + CC.GRAY + "click to add whitelisted kits", CC.GOLD + "Right " + CC.GRAY + "click to reset")
                        .build(),
                clickType -> {
                    if (clickType == ClickType.LEFT) {
                        getString("Enter the kit to whitelist", kit -> Arrays.stream(StringUtils.COMMA.split(kit.toLowerCase())).forEach(settings.getKitWhiteList()::add)).accept(clickType);
                    } else if (clickType == ClickType.RIGHT) {
                        settings.getKitWhiteList().clear();
                    }
                }
        );

        addSetting(inv, new ItemBuilder(Material.ENCHANTMENT_TABLE)
                        .name(colorize("Feast", settings.getSecondsTillFeast()))
                        .build(),
                editNumber("Set new feast time", settings::setSecondsTillFeast));

        addSetting(inv, new ItemBuilder(Material.ENDER_CHEST)
                        .name(colorize("Bonus Feast", settings.getSecondsTillBonusFeast()))
                        .build(),
                editNumber("Set new bonus feast time", settings::setSecondsTillBonusFeast));

        addSetting(inv, new ItemBuilder(Material.DIAMOND_SWORD)
                        .name(colorize("Pit", settings.getSecondsTillPit()))
                        .build(),
                editNumber("Set new pit time", settings::setSecondsTillPit));

        addSetting(inv, new ItemBuilder(Material.BEDROCK)
                        .name(colorize("Border", settings.getBorder()))
                        .build(),
                editNumber("Set new border", settings::setBorder));

        addSetting(inv, new ItemBuilder(Material.IRON_DOOR)
                        .name(colorize("Game End", settings.getGameEnd()))
                        .build(),
                editNumber("Set new game end", settings::setGameEnd));

        addSetting(inv, new ItemBuilder(Material.ARROW)
                        .name(colorize("Natural Events", settings.isNaturalEvents()))
                        .build(),
                clickType -> settings.setNaturalEvents(!settings.isNaturalEvents()));
        addSetting(inv, new ItemBuilder(Material.BONE)
                        .name(colorize("Pit Elimination", settings.isPitElimination()))
                        .build(),
                clickType -> settings.setPitElimination(!settings.isPitElimination()));
        addSetting(inv, new ItemBuilder(Material.SKULL_ITEM)
                        .name(colorize("Team Size", settings.getTeamSize()))
                        .build(),
                editNumber("Set the new team size", settings::setTeamSize));
        addSetting(inv, new ItemBuilder(Material.DIAMOND)
                        .name(colorize("Team Amount", settings.getTeamAmount()))
                        .build(),
                editNumber("Set the new team amount", settings::setTeamAmount));

    }

}
