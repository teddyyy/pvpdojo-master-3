/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.lms.settings;

import java.util.List;

import org.bukkit.entity.Player;

import com.pvpdojo.game.settings.GameSettings;
import com.pvpdojo.game.settings.GameSettingsMenu;
import com.pvpdojo.game.settings.NumberCheck;
import com.pvpdojo.game.settings.SettingsCheck;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class LMSSettings extends GameSettings {

    private boolean abilities = true;

    public LMSSettings() {
        setMinPlayers(5);
    }

    @Override
    public GameSettingsMenu getMenu(Player player) {
        return new LMSSettingsMenu(player, this);
    }

    @Override
    public List<SettingsCheck<?>> runChecks() {
        List<SettingsCheck<?>> checks = super.runChecks();

        check(new NumberCheck("Min Players", getMinPlayers(), 5, 10), checks);
        check(new NumberCheck("Max Players", getMaxPlayers(), 5, 200), checks);

        return checks;
    }
}
