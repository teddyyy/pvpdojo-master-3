/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game;

public class GameProperty {

    public static final String FILE_NAME = "game.properties";

    public static final String SERVER_ID = "serverid";
    public static final String GAME_ID = "gameid";
    public static final String HOST = "host";
    public static final String PRIVATE = "private";
    public static final String GAME = "game";
    public static final String SETTINGS = "settings";
    public static final String PARTICIPANTS = "participants";
    public static final String FORCED_MAP = "forcedmap";

    public static final String TEAM_RED = "team.red";
    public static final String TEAM_BLUE = "team.blue";
    public static final String CLAN_RED = "team.red.clan";
    public static final String CLAN_BLUE = "team.blue.clan";

}
