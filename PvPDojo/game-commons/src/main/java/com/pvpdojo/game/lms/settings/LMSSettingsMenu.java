/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.lms.settings;

import org.bukkit.entity.Player;

import com.pvpdojo.game.settings.GameSettingsMenu;

public class LMSSettingsMenu extends GameSettingsMenu<LMSSettings> {

    public LMSSettingsMenu(Player player, LMSSettings settings) {
        super(player, "LMS Settings", settings);
    }
}
