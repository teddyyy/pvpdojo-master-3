/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.settings;

import org.bukkit.entity.Player;

import com.pvpdojo.game.settings.GameSettingsMenu;

public class CTMSettingsMenu extends GameSettingsMenu<CTMSettings> {

    public CTMSettingsMenu(Player player, CTMSettings settings) {
        super(player, "CTM Settings", settings);
    }

}
