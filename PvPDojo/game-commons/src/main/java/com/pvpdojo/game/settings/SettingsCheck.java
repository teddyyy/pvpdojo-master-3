/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.settings;

public interface SettingsCheck<T extends GameSettings> {
    String getMessage();
    boolean check(T settings);
}
