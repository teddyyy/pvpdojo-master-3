/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.settings;

import java.util.function.Consumer;
import java.util.function.IntConsumer;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.gui.NumberInputGUI;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.PlayerUtil;

public abstract class GameSettingsMenu<T extends GameSettings> extends SingleInventoryMenu {

    protected T settings;
    private int currentPos;

    public GameSettingsMenu(Player player, String title, T settings) {
        super(player, title, 27);
        this.settings = settings;
    }

    @Override
    public void redraw() {
        super.redraw();
        currentPos = 9;
        InventoryPage inv = getCurrentPage();

        addSetting(inv, new ItemBuilder(Material.SKULL_ITEM)
                        .durability((short) 3)
                        .name(colorize("Min. Players", settings.getMinPlayers()))
                        .build(),
                editNumber("Set min. amount of players", settings::setMinPlayers));

        addSetting(inv, new ItemBuilder(Material.SKULL_ITEM)
                        .name(colorize("Max. Players", settings.getMaxPlayers()))
                        .build(),
                editNumber("Set max. amount of players", settings::setMaxPlayers));

        if (PlayerUtil.hasRank(getPlayer(), Rank.DEVELOPER, false)) {
            addSetting(inv, new ItemBuilder(Material.NAME_TAG)
                            .name(colorize("Random Name Tags", settings.isRandomizePlayers()))
                            .build(),
                    type -> settings.setRandomizePlayers(!settings.isRandomizePlayers()));
        }

        inv.fillBlank();
    }

    public Consumer<ClickType> editNumber(String description, IntConsumer intConsumer) {
        return clickType -> new NumberInputGUI(description, i -> {
            intConsumer.accept(i);
            redrawAndOpen();
        }, this::open).open(getPlayer());
    }

    public void addSetting(InventoryPage inv, ItemStack item, Consumer<ClickType> clickAction) {
        inv.setItem(currentPos++, item, clickAction);
    }

    protected String colorize(String key, Object value) {
        return CC.GRAY + key + PvPDojo.POINTER + CC.DARK_AQUA + value.toString();
    }
}
