/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.hg.settings;

import com.pvpdojo.game.settings.GameSettings;
import com.pvpdojo.game.settings.GameSettingsMenu;
import com.pvpdojo.game.settings.NumberCheck;
import com.pvpdojo.game.settings.SettingsCheck;
import com.pvpdojo.userdata.Rank;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
public class HGSettings extends GameSettings {

    public HGSettings() {
        setMinPlayers(10);
        setRankToSpectate(Rank.FIRST_DAN);
    }

    private int border = 500;
    private int secondsTillPvP = 120;
    private int secondsTillFeast = 900;
    private int secondsTillBonusFeast = 2100;
    private int secondsTillPit = 2400;//2700 = 45 min, 2400 = 40 min
    private int gameEnd = 3600;
    private boolean respawn = true, freekits, skipPit, naturalEvents = true, pitElimination = true;
    private int teamSize = 1;
    private int teamAmount = 0;

    private Set<String> disabledKits = new HashSet<>();
    private Set<String> kitWhiteList = new HashSet<>();

    @Override
    public List<SettingsCheck<?>> runChecks() {
        List<SettingsCheck<?>> checks = super.runChecks();

        check(new NumberCheck("Min Players", getMinPlayers(), 3, 20), checks);
        check(new NumberCheck("Max Players", getMaxPlayers(), 12, 300), checks);
        check(new BorderCheck(getBorder()), checks);
        check(new NumberCheck("Invincibility", getSecondsTillPvP(), 5, 600), checks);
        check(new NumberCheck("Feast", getSecondsTillFeast(), 300, 1500), checks);
        check(new NumberCheck("Bonus Feast", getSecondsTillBonusFeast(), 500, 3000), checks);
        check(new NumberCheck("Pit", getSecondsTillPit(), 2500, 5000), checks);
        check(new NumberCheck("End", getGameEnd(), 3000, 15000), checks);

        return checks;
    }

    public class BorderCheck extends NumberCheck {

        public BorderCheck(int given) {
            super("Border", given, 200, 1000);
        }

        @Override
        public String getMessage() {
            return super.getMessage() + " and must be a factor of 50";
        }

        @Override
        public boolean check(GameSettings settings) {
            return super.check(settings) && getGiven() % 50 == 0;
        }
    }

    @Override
    public GameSettingsMenu getMenu(Player player) {
        return new HGSettingsMenu(player, this);
    }
}
