/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.game.ctm.settings;

import org.bukkit.entity.Player;

import com.pvpdojo.game.settings.GameSettings;
import com.pvpdojo.game.settings.GameSettingsMenu;
import com.pvpdojo.userdata.Rank;

public class CTMSettings extends GameSettings {

    public CTMSettings() {
        setRankToSpectate(Rank.DEFAULT);
    }

    @Override
    public GameSettingsMenu getMenu(Player player) {
        return new CTMSettingsMenu(player, this);
    }

}
