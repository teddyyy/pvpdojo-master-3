/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.fallback;

import java.util.LinkedList;
import java.util.Queue;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;
import org.bukkit.event.player.PlayerJoinEvent;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.UserList;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.extension.DojoServer;
import com.pvpdojo.map.DojoMap;
import com.pvpdojo.map.MapLoader;
import com.pvpdojo.map.MapType;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.userdata.impl.UserImpl;
import com.pvpdojo.util.bukkit.CC;

public class Fallback extends DojoServer<UserImpl> implements DojoListener {

    @Override
    public ServerType getServerType() {
        return ServerType.FALLBACK;
    }

    private Queue<Player> queue = new LinkedList<>();

    @Override
    public void start() {
        register();

        PvPDojo.schedule(() -> {
            if (queue.isEmpty() && Bukkit.getOnlinePlayers().size() > 0 && BungeeSync.findServerTypeWithLeastPlayers(ServerType.HUB) != null) {
                PvPDojo.schedule(() -> queue.addAll(Bukkit.getOnlinePlayers())).sync();
            }
        }).createTimerAsync(2 * 20, -1);

        PvPDojo.schedule(() -> {
            Player player = queue.poll();
            while (player != null && !player.isOnline()) {
                player = queue.poll();
            }
            if (player != null) {
                ServerType.HUB.connect(player);
            }
        }).createTimer(15, -1);
    }

    @Override
    public void postLoad() {
        Bukkit.setSaveBandwidth(true);
    }

    @Override
    public DojoMap setupMap() {
        DojoMap map = MapLoader.getMap(MapType.DEFAULT, "fallback");
        PvPDojo.get().setStandardMap(map);
        if (map == null) {
            return null;
        }
        map.setWorld(Bukkit.getWorlds().get(0));
        map.shift(-map.getLocations()[0].getBlockX(), 0, -map.getLocations()[0].getBlockZ()); // Move spawn to 0,0
        map.setup(0);
        map.placeBlocks(false);
        return map;
    }

    @Override
    public UserList<UserImpl> createUserList() {
        return new UserList<>(UserImpl::new);
    }

    @EventHandler
    public void onConnect(AsyncPlayerPreLoginEvent e) {
        if (Bukkit.getPlayer(e.getUniqueId()) != null) {
            e.disallow(Result.KICK_OTHER, "Failed to send you to fallback");
            PvPDojo.schedule(() -> Bukkit.getPlayer(e.getUniqueId()).kickPlayer("You logged in from another location")).sync();
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        e.getPlayer().sendMessage(CC.RED + "Hub server is currently not available");
        e.getPlayer().sendMessage(CC.RED + "Hang on! We will automatically reconnect you once it's back");
        e.getPlayer().sendMessage(CC.RED + "Sorry for the inconvenience");
        e.getPlayer().teleport(PvPDojo.get().getStandardMap().getLocations()[0]);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        e.setCancelled(true);
    }

}
