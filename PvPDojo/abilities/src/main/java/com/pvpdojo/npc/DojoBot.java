/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.npc;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_7_R4.CraftWorld;
import org.bukkit.craftbukkit.v1_7_R4.entity.disguise.CraftPlayerDisguise;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.npcs.ai.DojoBotPathfinder;
import com.pvpdojo.session.GameSession;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.entity.TeamArenaEntity;
import com.pvpdojo.settings.BotSettings;
import com.pvpdojo.settings.BotSettings.Difficulty;

import lombok.Getter;
import net.minecraft.server.v1_7_R4.Block;
import net.minecraft.server.v1_7_R4.Entity;
import net.minecraft.server.v1_7_R4.EntityCreature;
import net.minecraft.server.v1_7_R4.EntityHuman;
import net.minecraft.server.v1_7_R4.EntityLiving;
import net.minecraft.server.v1_7_R4.EntityZombie;
import net.minecraft.server.v1_7_R4.GenericAttributes;
import net.minecraft.server.v1_7_R4.PacketPlayOutAnimation;
import net.minecraft.server.v1_7_R4.PathfinderGoalFloat;
import net.minecraft.server.v1_7_R4.PathfinderGoalLookAtPlayer;
import net.minecraft.server.v1_7_R4.PathfinderGoalMeleeAttack;
import net.minecraft.server.v1_7_R4.PathfinderGoalNearestAttackableTarget;
import net.minecraft.server.v1_7_R4.World;
import net.minecraft.util.com.mojang.authlib.GameProfile;

public class DojoBot extends EntityZombie implements ArenaEntity {

    public Difficulty difficulty;
    private TeamArenaEntity team;
    private boolean freeze;
    private Set<UUID> ignore = new HashSet<>();

    @Getter
    private double generalSpeed, strafe, strafeSpeed;
    @Getter
    public boolean oldBot = false, extremekb = false, disallowKb = false;

    public DojoBot(World nmsWorld) {
        super(nmsWorld);

        this.goalSelector.clear();
        this.targetSelector.clear();
        getAttributeInstance(GenericAttributes.e).setValue(0);

    }

    public void transform(GameProfile profile, Location l, boolean oldBot) {

        Runnable transform = () -> {
            World nmsWorld = ((CraftWorld) l.getWorld()).getHandle();
            callPathfinders(oldBot);
            setPositionRotation(l.getX(), l.getY(), l.getZ(), l.getYaw(), 2.5f);
            attachedToPlayer = true; // Load Chunks Hack
            nmsWorld.addEntity(this, SpawnReason.CUSTOM);
            attachedToPlayer = false; // Load Chunks Hack end
            getEntity().getEquipment().setItemInHand(new ItemStack(Material.STONE_SWORD));
            GameProfile botProfile = new GameProfile(UUID.randomUUID(), profile.getName());
            botProfile.getProperties().putAll(profile.getProperties());
            getBukkitEntity().setDisguise(new CraftPlayerDisguise(getBukkitEntity(), botProfile));
            getBukkitEntity().setRemoveWhenFarAway(false);
            AbilityServer.get().createEntity(getBukkitEntity());
        };

        if (getSession() != null) {
            getSession().getArena().addBlocksGeneratedTask(transform);
        } else {
            transform.run();
        }

    }

    private void callPathfinders(boolean oldBot) {
        this.oldBot = oldBot;
        if (!oldBot) {
            goalSelector.a(2, new PathfinderGoalMeleeAttack(this, DojoBot.class, 1.3D, false));
            goalSelector.a(1, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 8.0F));
            goalSelector.a(0, new PathfinderGoalMeleeAttack(this, EntityHuman.class, 1.3D, false));
            targetSelector.a(0, new CustomPathPlayerFinder(this, EntityHuman.class, 0, true));
            targetSelector.a(1, new CustomPathPlayerFinder(this, DojoBot.class, 0, true));
            setSprinting(true);
        } else {
            goalSelector.a(1, new DojoBotPathfinder(this, 20, difficulty));
            goalSelector.a(2, new PathfinderGoalFloat(this));
            applyDifficultySettings();
            generalSpeed = 1.7f;
        }
        System.out.println("Called the method 'callPathfinders' ( Is Old Bot? = " + oldBot + " )");
    }

    private void applyDifficultySettings() {
        if (difficulty == null)
            difficulty = Difficulty.EASY;

        switch (difficulty) {
            case EASY:
                strafe = 3.0D;
                strafeSpeed = 1.0D;
                extremekb = false;
                break;
            case MEDIUM:
                strafe = 7.5D;
                strafeSpeed = 2.0D;
                extremekb = false;
                break;
            case HARD:
                strafe = 8.5D;
                strafeSpeed = 2.0D;
                extremekb = false;
                break;

        }

        /*
         * Max Health = GenericAttributes.a
         * Follow Range = GenericAttributes.b
         * Knockback Resistance = GenericAttributes.c
         * Movement Speed = GenericAttributes.d
         * Attack Damage = GenericAttributes.e
         *
         */

        if (disallowKb && !difficulty.equals(Difficulty.EASY)) {// No knockback
            getAttributeInstance(GenericAttributes.c).setValue(1);
        }
    }

    @Override
    protected void bn() {
        if (!freeze) {
            super.bn();
        }
    }

    public void setName(String name) {
        getEntity().setCustomName(name);
    }

    public String getName() {
        return getEntity().getCustomName();
    }

    public LivingEntity getEntity() {

        return getBukkitEntity();
    }

    public Set<UUID> getIgnore() {
        return ignore;
    }

    @Override
    protected void a(Entity entity, float f) {
    }

    private int soups;

    // Don't respect undead potion behaviour
    @Override
    public boolean aR() {
        return false;
    }

    public void setSoups(int soups) {
        this.soups = soups;
    }

    public int getSoups() {
        return soups;
    }

    public void useSoups() {

        int soup = 7;

        new BukkitRunnable() {

            int damage = 1;
            int jump = 1;
            boolean animation;

            @Override
            public void run() {

                if (soups > 0) {
                    if (((Damageable) bukkitEntity).getHealth() <= 15.0D && !bukkitEntity.isDead()
                            || ((Damageable) bukkitEntity).getHealth() <= 0) {

                        ((Damageable) bukkitEntity).setHealth(Math.min(((Damageable) bukkitEntity).getHealth() + soup, ((Damageable) bukkitEntity).getMaxHealth()));

                        soups--;

                        LivingEntity l = (LivingEntity) bukkitEntity;

                        l.getEquipment().setItemInHand(new ItemStack(Material.MUSHROOM_SOUP));

                        PvPDojo.schedule(() -> {
                            getEntity().getEquipment().setItemInHand(new ItemStack(Material.BOWL));
                            Item i = l.getWorld().dropItem(l.getEyeLocation(), new ItemStack(Material.BOWL));
                            i.setVelocity(l.getEyeLocation().getDirection().multiply(0.08));
                            i.setCustomDespawnRate(5 * 20);
                        }).createTask(4);

                        PvPDojo.schedule(
                                () -> getEntity().getEquipment().setItemInHand(new ItemStack(Material.STONE_SWORD)))
                               .createTask(8);

                    }
                }

                if (bukkitEntity.isDead()) {
                    if (session != null) {
                        session.postDeath(DojoBot.this);
                    }
                    this.cancel();
                }

                if (!oldBot) {// We have our own Damage, Jumping, Arm swinging Packet in the Pathfinder
                    damage--;
                    jump--;

                    if (animation) {
                        for (Player player : bukkitEntity.getWorld().getPlayers()) {
                            if (player.getLocation().distanceSquared(getBukkitEntity().getLocation()) < 150) {
                                NMSUtils.get(player).playerConnection
                                        .sendPacket(new PacketPlayOutAnimation(DojoBot.this, 0));
                            }
                        }
                        animation = false;
                    } else {
                        animation = true;
                    }

                    if (damage <= 0) {
                        for (org.bukkit.entity.Entity ent : getBukkitEntity().getNearbyEntities(5, 5, 5)) {
                            Location comp = ent.getLocation();
                            comp.setY(getBukkitEntity().getLocation().getY());
                            if (comp.distanceSquared(getBukkitEntity().getLocation()) < 9.1
                                    && !ignore.contains(ent.getUniqueId()) && getBukkitEntity().hasLineOfSight(ent)) {
                                if (ent instanceof LivingEntity) {
                                    hit((LivingEntity) ent);
                                    if (jump <= 0 && getBukkitEntity().isOnGround()
                                            && comp.distanceSquared(getBukkitEntity().getLocation()) < 6) {

                                        jump();
                                        jump = 30;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            private void hit(LivingEntity ent) {
                if (!(ent instanceof Zombie)) {
                    if (AbilityServer.get().getEntity(getBukkitEntity())
                                     .canTarget(AbilityServer.get().getEntity(ent))) {
                        ent.damage(difficulty.ordinal() > Difficulty.EASY.ordinal()
                                ? difficulty.ordinal() > Difficulty.MEDIUM.ordinal() ? 10D : 8D
                                : 6D, getBukkitEntity());
                        damage = 13;
                    }
                }
            }

        }.runTaskTimer(PvPDojo.get(), 0, 0);
    }

    public void jump() {
        bj();
    }

    @Override
    protected String t() {
        return "game.player.step";
    }

    @Override
    protected String aT() {
        return "game.player.hurt";
    }

    @Override
    protected String aU() {
        return "game.player.death";
    }

    @Override
    protected void a(int i, int j, int k, Block block) {
        makeSound("game.player.step", 0.15F, 1.0F);
    }

    private GameSession session;

    @Override
    public GameSession getSession() {
        return session;
    }

    @Override
    public void setSession(GameSession session) {
        this.session = session;
    }

    @Override
    public void teleport(Location location) {
        getBukkitEntity().teleport(location);
    }

    @Override
    public List<Player> getPlayers() {
        return Collections.emptyList();
    }

    @Override
    public Player getPlayer() {
        return null;
    }

    public void applySettings(BotSettings settings) {
        setSoups(settings.getSoups());
        difficulty = settings.getDifficulty();
    }

    @Override
    public void setFrozen(boolean freeze) {
        this.freeze = freeze;
    }

    @Override
    public UUID getUniqueId() {
        return getUniqueID();
    }

    @Override
    public TeamArenaEntity getTeam() {
        return team;
    }

    @Override
    public void setTeam(TeamArenaEntity team) {
        this.team = team;
    }

    private class CustomPathPlayerFinder extends PathfinderGoalNearestAttackableTarget {

        public CustomPathPlayerFinder(EntityCreature entitycreature, Class oclass, int i, boolean flag) {
            super(entitycreature, oclass, i, flag);

        }

        @Override
        protected boolean a(EntityLiving entityliving, boolean flag) {
            if (ignore.contains(entityliving.getUniqueID())) {
                return false;
            }
            return super.a(entityliving, flag);
        }

    }

    public static boolean isBot(org.bukkit.entity.Entity entity) {
        return NMSUtils.get(entity) instanceof DojoBot;
    }

}