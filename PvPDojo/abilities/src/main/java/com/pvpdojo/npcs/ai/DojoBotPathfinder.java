package com.pvpdojo.npcs.ai;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.npc.DojoBot;
import com.pvpdojo.settings.BotSettings.Difficulty;
import com.pvpdojo.userdata.User;
import net.minecraft.server.v1_7_R4.*;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class DojoBotPathfinder extends PathfinderGoal {
    private EntityInsentient e;
    private Location loc;
    private double dist, strafe, strafeSpeed;
    private boolean extremeKb;
    private AbilityEntity target;
    private Difficulty difficulty;

    public DojoBotPathfinder(EntityInsentient e, int d, Difficulty diff) {
        this.extremeKb = false;
        this.strafe = 8.0D;
        this.strafeSpeed = 2.0D;
        this.dist = d;
        this.e = e;
        this.difficulty = diff;
    }

    public boolean a() {
        loc = e.getBukkitEntity().getLocation();
        Map<Double, AbilityEntity> p = new HashMap<>();
        double least = 100D;

        for (AbilityEntity ent : AbilityServer.get().getNearbyEntities(e.getBukkitEntity().getLocation(), dist, 5)) {

            if (ent.getEntity().equals(e.getBukkitEntity())) {
                continue;
            }

            if (!ent.canTarget(AbilityEntity.get(e.getBukkitEntity()))) {
                continue;
            }

            if (ent.getEntity() instanceof Player) {

                Player player = (Player) ent.getEntity();
                User user = User.getUser(player);

                if (player.getGameMode().equals(GameMode.CREATIVE) || user.isSpectator() || user.isVanish()
                        || user.isAdminMode() || user.isFrozenByStaff()) {
                    continue;
                }

                if (e.getName() != null && e.getName().contains(player.getName()))
                    continue;
            }

            if (ent.getEntity().getCustomName() != null && e.getCustomName() != null
                    && e.getCustomName().contains(ent.getEntity().getCustomName())) {
                continue;
            }

            if (e instanceof DojoBot) {
                DojoBot m = (DojoBot) e;
                if (m.getIgnore().contains(ent.getEntity().getUniqueId())) {
                    continue;
                }
            }

            if (loc.getWorld().equals(ent.getEntity().getWorld())) {
                p.put(ent.getEntity().getLocation().distance(loc), ent);
            }

        }

        for (Double d : p.keySet()) {
            if (d < least) {
                least = d;
            }
        }

        AbilityEntity e = p.get(least);
        if (e == null) {
            target = null;
            this.e.setGoalTarget(null);
            return false;
        }
        target = e;
        return true;
    }

    public void e() {
        if (loc != null && target != null && loc.getWorld().equals(target.getEntity().getWorld())) {

            if (e instanceof DojoBot) {
                DojoBot bot = (DojoBot) e;
                if (!bot.disallowKb) {
                    double value = bot.getBukkitEntity().isOnGround() && Math.random() * 100.0D <= 36 ? 1.0 : 0.0;
                    bot.getAttributeInstance(GenericAttributes.c).setValue(value);
                }
            }

            if (loc.distance(target.getEntity().getLocation()) > 2.4D + Math.random()) {

                PathEntity p = e.getNavigation().a(NMSUtils.get(target.getEntity()));
                if (e instanceof DojoBot) {
                    DojoBot m = (DojoBot) e;
                    e.getNavigation().a(p, m.getGeneralSpeed());

                }
            }

            if (loc.distance(target.getEntity().getLocation()) < 4.5D + Math.random() * 2.0D) {
                loc.getWorld().getEntities().stream().filter(target -> target instanceof Player).forEach(players -> {
                    PacketPlayOutAnimation armAnimation = new PacketPlayOutAnimation(e, 0);
                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(armAnimation);

                });
            }

            if (loc.distance(target.getEntity().getLocation()) < 3.0D) {

                e.getControllerLook().a(NMSUtils.get(target.getEntity()), 10.0F, 0.0F);

                e.yaw = e.getHeadRotation();
                if (Math.random() * 10.0D > 9.0D) {
                    e.getControllerJump().a();
                }

                if (e instanceof DojoBot) {
                    DojoBot m = (DojoBot) e;
                    extremeKb = m.isExtremekb();
                    strafe = m.getStrafe();
                    strafeSpeed = m.getStrafeSpeed();
                }

                target.getEntity().damage(difficulty.ordinal() > Difficulty.EASY.ordinal()
                        ? difficulty.ordinal() > Difficulty.MEDIUM.ordinal() ? 10D : 8D
                        : 6D, e.getBukkitEntity());

                int y = target.getEntity() instanceof Player ? 1 : 7;
                int x = (int) (Math.random() * 10.0D);

                if (x > y) {
                    e.getControllerMove().a(strafe, 2.5D, strafe, strafeSpeed);
                    if (extremeKb && PvPDojo.RANDOM.nextInt(100) <= 50) {
                        NMSUtils.applyKnockback(target.getEntity(), e.getBukkitEntity());
                    }
                }
            }
        }

    }
}
