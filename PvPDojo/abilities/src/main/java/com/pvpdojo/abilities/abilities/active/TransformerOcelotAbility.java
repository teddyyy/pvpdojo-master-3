/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.craftbukkit.v1_7_R4.entity.disguise.CraftAgeableDisguise;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.disguise.AgeableDisguise;
import org.bukkit.event.Event;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;

public class TransformerOcelotAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "TransformerOcelotAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        if (ability.getAbilityEntity().getEntity().getDisguise() == null) {
            LivingEntity entity = ability.getAbilityEntity().getEntity();

            entity.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0));

            AgeableDisguise disguise = new CraftAgeableDisguise(ability.getAbilityEntity().getEntity(), EntityType.OCELOT);
            disguise.setCustomName(entity instanceof Player ? ((Player) entity).getNick() : entity.getCustomName());
            disguise.setCustomNameVisible(true);

            ability.getAbilityEntity().getEntity().setDisguise(disguise);
            new AbilityRunnable(ability) {
                @Override
                public void run() {
                    cancel();
                }

                @Override
                public void cancel() {
                    super.cancel();
                    entity.setDisguise(null);
                    entity.removePotionEffect(PotionEffectType.SPEED);
                }
            }.createTask(20 * 8);
            return true;
        }

        return false;
    }
}
