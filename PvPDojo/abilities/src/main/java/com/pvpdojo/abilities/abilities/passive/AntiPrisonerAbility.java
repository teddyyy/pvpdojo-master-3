/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.event.Event;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;

public class AntiPrisonerAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "AntiPrisonerAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        return false;
    }

}
