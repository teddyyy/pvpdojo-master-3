package com.pvpdojo.abilities.abilities.passive;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class VolcanoAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "VolcanoAbility";
    }

    private static final String META = "LAVA_META";

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        return false;
    }

    @EventHandler
    public void onEntityDamageEvent(EntityDamageEvent e) {
        if (e.getEntity() instanceof LivingEntity) {
            LivingEntity living = (LivingEntity) e.getEntity();
            if (canAbilityExecute(living)) {
                if (e.getCause() == DamageCause.LAVA) {
                    e.setDamage(2.0);
                }
                if (e.getCause() == DamageCause.FIRE_TICK || e.getCause() == DamageCause.FIRE) {
                    e.setCancelled(true);
                    living.setFireTicks(0);
                }
            }
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        Player player = e.getPlayer();
        if (canAbilityExecute(player)) {
            if (player.getLocation().getBlock().getType() == Material.LAVA
                    || player.getLocation().getBlock().getType() == Material.STATIONARY_LAVA) {
                if (player.hasMetadata(META)) {
                    player.setMetadata(META, new FixedMetadataValue(PvPDojo.get(), true));
                }
            } else {
                if (player.hasMetadata(META)) {
                    player.removeMetadata(META, PvPDojo.get());
                }
            }
        }
    }

    @EventHandler
    public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof LivingEntity && e.getDamager() instanceof Player
                && e.getDamager().hasMetadata(META)) {
            Player player = (Player) e.getDamager();
            if (canAbilityExecute(player)) {
                Ability ability = getAbility(player);
                final float damage = ability.getAttribute("damage");
                if (damage > 0) {
                    double increase_damage = e.getDamage() + 1.0D;
                    e.setDamage(increase_damage);
                }
            }
        }
    }

}
