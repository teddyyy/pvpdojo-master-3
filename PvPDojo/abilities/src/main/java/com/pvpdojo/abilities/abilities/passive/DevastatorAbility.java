/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.bukkit.events.EntityFallEvent;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.util.VectorUtil;
import com.pvpdojo.util.bukkit.BlockUtil;
import org.bukkit.Effect;
import org.bukkit.block.Block;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

public class DevastatorAbility extends AbilityExecutor {

    public static final String FALL_DISTANCE = "FALL";
    public static final String DEVASTATOR_BLOCK = "devastator";

    @Override
    public String getExecutorName() {
        return "DevastatorAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        double distance = ability.getMetaData().getData(FALL_DISTANCE);
        LivingEntity entity = ability.getAbilityEntity().getEntity();

        for (Block block : BlockUtil.getNearbyBlocks(entity.getLocation().add(0, -1, 0), 6)) {
            if (PvPDojo.RANDOM.nextInt(100) <= 50) {
                FallingBlock fb = entity.getWorld().spawnFallingBlock(block.getLocation().add(0, 2, 0), block.getTypeId(), block.getData());
                fb.setVelocity(VectorUtil.getRandomVelocity().multiply(distance / 8 > 3 ? 3 : distance / 8));
                fb.setDropItem(false);
                fb.setMetadata(DEVASTATOR_BLOCK, new FixedMetadataValue(PvPDojo.get(), entity));
            }
        }
        return true;
    }

    @EventHandler
    public void onFall(EntityFallEvent e) {
        if (e.getEntity() instanceof LivingEntity && canAbilityExecute((LivingEntity) e.getEntity())) {
            if (e.getDistance() >= 5) {
                Ability ability = getAbility((LivingEntity) e.getEntity());
                ability.getMetaData().addData(FALL_DISTANCE, e.getDistance());
                ability.activateAbility(e);
            }
        }
    }

    @EventHandler
    public void onDevastatorBlock(EntityChangeBlockEvent e) {
        if (e.getEntity().hasMetadata(DEVASTATOR_BLOCK)) {
            e.setCancelled(true);
            e.getEntity().remove();
            e.getEntity().getWorld().playEffect(e.getEntity().getLocation(), Effect.STEP_SOUND, ((FallingBlock) e.getEntity()).getMaterial());

            LivingEntity shooter = (LivingEntity) BukkitUtil.consumeMeta(e.getEntity(), DEVASTATOR_BLOCK).get(0).value();
            AbilityEntity source = AbilityServer.get().getEntity(shooter);
            AbilityServer.get().getNearbyEntities(source, 2.5).forEach(target -> source.abilityDamage(target, 100F, getAbility(shooter), DamageCause.FALL));
        }
    }
}
