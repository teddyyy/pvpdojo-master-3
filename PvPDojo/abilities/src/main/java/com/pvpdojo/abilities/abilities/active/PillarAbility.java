/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.bukkit.util.TempBlock;

public class PillarAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "PillarAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        final AbilityEntity activator = ability.getAbilityEntity();
        final float height = ability.getAttribute("height");
        final float speed = ability.getAttribute("speed");
        final Player player = (Player) activator.getEntity();
        final Location start = player.getLocation();
        new AbilityRunnable(ability) {
            int x = 0;

            public void run() {
                if (!player.getWorld().equals(start.getWorld())) {
                    cancel();
                    return;
                }
                if (player.getLocation().distance(start) > 25) {
                    cancel();
                    return;
                }
                if (x < height) {
                    Location loc = start.clone().add(0, x + 1, 0);
                    if (loc.clone().add(0, 2, 0).getBlock().getType().equals(Material.AIR) && loc.clone().add(0, 1, 0).getBlock().getType().equals(Material.AIR)) {
                        TempBlock.createTempBlock(loc, Material.QUARTZ_BLOCK, (byte) 0, 8);
                        loc.setYaw(player.getLocation().getYaw());
                        loc.setPitch(player.getLocation().getPitch());
                        player.teleport(loc.clone().add(0, 1, 0));
                        ++x;
                    }
                } else
                    cancel();
            }

        }.createTimer(8 - (int) speed, (int) height);
        return true;
    }
}