/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.base.abilities;

import static com.pvpdojo.util.StreamUtils.negate;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.bukkit.entity.Entity;
import org.bukkit.metadata.FixedMetadataValue;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.abilities.lib.TemporaryEntity;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.ExpireArrayList;
import com.pvpdojo.util.ExpireHashMap;

public class AbilityMetaData {

    private ExpireArrayList<AbilityEntity> targets = new ExpireArrayList<>();
    private ExpireHashMap<String, Object> data = new ExpireHashMap<>();
    private ArrayList<TemporaryEntity> entityTracker = new ArrayList<>();
    private ArrayList<AbilityRunnable> runnables = new ArrayList<>();
    private Ability ability;

    public AbilityEntity getTarget() {
        AbilityEntity target = targets.first();
        if (target == null)
            return null;
        if (!target.isTargetable())
            return null;
        return target;
    }

    public AbilityMetaData(Ability ability) {
        this.ability = ability;
    }

    public void clear() {
        if (!(ability.getAbilityEntity() instanceof AbilityPlayer) || !User.getUser(((AbilityPlayer) ability.getAbilityEntity()).getPlayer()).isLoggedOut()) {
            data.clear();
        }
        targets.clear();
        for (int i = runnables.size() - 1; i >= 0; i--) {
            AbilityRunnable run = runnables.get(i);
            run.cancel();
        }
        runnables.clear();
        entityTracker.forEach(TemporaryEntity::remove);
        entityTracker.clear();
    }

    public void tick() {
        targets.removeIf(negate(AbilityEntity::isTargetable));
    }

    public ArrayList<TemporaryEntity> getEntityTracker() {
        return entityTracker;
    }

    public ArrayList<AbilityRunnable> getAbilityRunnables() {
        return this.runnables;
    }

    public ExpireHashMap<String, Object> getData() {
        return data;
    }

    public void addEntity(Entity entity, int timeout, boolean allowInteractionWithWorld) {
        if (!allowInteractionWithWorld) {
            entity.setMetadata(AbilityEntity.ABILITY_ENTITY_NO_INTERACTION, new FixedMetadataValue(PvPDojo.get(), null));
        }
        entity.setMetadata(AbilityEntity.OWNED_ENTITY, new FixedMetadataValue(PvPDojo.get(), ability.getAbilityEntity().getEntity().getUniqueId()));
        this.entityTracker.add(new TemporaryEntity(entity, timeout));
    }

    public void addTarget(AbilityEntity target, double delay) {
        if (target == null)
            return;
        this.targets.add(target, (long) (delay * 1000), TimeUnit.MILLISECONDS);
    }

    public boolean hasTarget() {
        return this.getTarget() != null && this.getTarget().isTargetable();
    }

    public boolean hasEntity(Entity entity) {
        for (TemporaryEntity check : this.entityTracker) {
            if (check.getId() == entity.getEntityId()) {
                return true;
            }
        }
        return false;
    }

    public void removeTarget(AbilityEntity target) {
        this.targets.remove(target);
    }

    public void addData(String key, Object data) {
        this.addData(key, data, Integer.MAX_VALUE);
    }

    public void addData(String key, Object data, double expire) {
        this.data.put(key, data, (long) (expire * 1000), TimeUnit.MILLISECONDS);
    }

    public void removeData(String key) {
        this.data.remove(key);
    }

    public boolean hasData(String key) {
        return data.containsKey(key);
    }

    @SuppressWarnings("unchecked")
    public <T> T getData(String key) {
        return hasData(key) ? (T) data.get(key) : null;
    }
}
