package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Location;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.CC;

public class ZeusAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "ZeusAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        if (!(event instanceof PlayerInteractEvent)) {
            ability.getAbilityEntity().sendMessage(MessageKeys.GENERIC, "{text}", "Cannot use with sneak activation");
            return false;
        }

        PlayerInteractEvent interactEvent = (PlayerInteractEvent) event;

        if (interactEvent.getClickedBlock() != null) {
            final float radius_attribute = ability.getAttribute("radius");
            final float delay_attribute = ability.getAttribute("delay");
            final float damage_attribute = ability.getAttribute("ability_damage");

            for (float x = -radius_attribute; x <= radius_attribute; x++) {
                for (float y = -radius_attribute; y <= radius_attribute; y++) {
                    for (float z = -radius_attribute; z <= radius_attribute; z++) {
                        Location loc = interactEvent.getClickedBlock().getLocation().clone().add(x, y, z);
                        new AbilityRunnable(ability) {

                            @Override
                            public void run() {
                                loc.getWorld().strikeLightningEffect(loc);
                                for (AbilityEntity near : AbilityServer.get().getNearbyEntities(ability.getAbilityEntity(), loc, 2, 2)) {
                                    ability.getAbilityEntity().abilityDamage(near, damage_attribute, ability, DamageCause.MAGIC);
                                    near.getEntity().setFireTicks(20 * 5);

                                }
                            }
                        }.createTask((int) (20 * delay_attribute));
                    }
                }
            }
            ability.getAbilityEntity().sendMessage(MessageKeys.GENERIC, "{text}", PvPDojo.PREFIX + CC.GREEN + "Lightning will strike at "
                    + BlockUtil.asString(interactEvent.getClickedBlock()) + " in " + delay_attribute + " seconds");
            return true;
        }

        return false;
    }

}
