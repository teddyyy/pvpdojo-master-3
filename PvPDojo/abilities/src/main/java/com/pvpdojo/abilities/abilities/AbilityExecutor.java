/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import com.pvpdojo.abilities.Abilities;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityEffect.AbilityEffectType;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.userdata.User;

public abstract class AbilityExecutor implements DojoListener {

    private static final Map<String, AbilityExecutor> executors = new HashMap<>();

    public static AbilityExecutor getExecutor(String executor) {
        return executors.get(executor);
    }

    public AbilityExecutor() {
        register(Abilities.get());
        executors.put(getExecutorName(), this);
    }

    public Ability getAbility(LivingEntity entity) {
        if (entity == null) {
            return null;
        }
        AbilityEntity champion = AbilityServer.get().getEntity(entity);
        return champion.getAbilityFromExecutor(getExecutorName());
    }

    public boolean canAbilityExecute(LivingEntity entity) {
        if (!AbilityServer.get().containsEntity(entity))
            return false;
        AbilityEntity abilityEntity = AbilityServer.get().getEntity(entity);

        if (abilityEntity.getEntity() instanceof Player) {
            User user = User.getUser((Player) abilityEntity.getEntity());
            if (user.isAdminMode()) {
                return false;
            }
            if (!user.canPvP()) {
                return false;
            }
        }

        if (abilityEntity.hasEffect(AbilityEffectType.SILENCE)) {
            return false;
        }

        if (abilityEntity.isTargetable()) {
            Ability ability = abilityEntity.getAbilityFromExecutor(getExecutorName());
            return ability != null;
        }
        return false;
    }

    public abstract String getExecutorName();

    public abstract boolean activateAbility(Ability ability, Event event);

    public void onReset(Ability ability) {}

}
