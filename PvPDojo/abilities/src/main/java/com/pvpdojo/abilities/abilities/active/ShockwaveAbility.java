/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.event.Event;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.effects.ShockWaveEffect;
import com.pvpdojo.abilities.base.abilities.Ability;

public class ShockwaveAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "ShockwaveAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        new ShockWaveEffect(ability).start();
        return false;
    }

}
