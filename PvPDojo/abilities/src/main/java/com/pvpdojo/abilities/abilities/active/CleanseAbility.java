/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Sound;
import org.bukkit.event.Event;
import org.bukkit.potion.PotionEffect;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityEffect;
import com.pvpdojo.lang.MessageKeys;

public class CleanseAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "CleanseAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        AbilityEntity activator = ability.getAbilityEntity();
        if (activator.getEffects().isEmpty() && activator.getEntity().getActivePotionEffects().isEmpty() && activator.getEntity().getFireTicks() > 0) {
            activator.sendMessage(MessageKeys.CLEANSE_NO_EFFECTS);
            return false;
        }
        activator.getEntity().setFireTicks(0);
        activator.getEntity().getWorld().playSound(activator.getEntity().getEyeLocation(), Sound.EAT, 1, -3f);
        activator.getEntity().getWorld().playSound(activator.getEntity().getEyeLocation(), Sound.SUCCESSFUL_HIT, 1, -3f);
        for (int i = activator.getEffects().size() - 1; i >= 0; i--) {
            AbilityEffect effect = activator.getEffects().get(i);
            effect.endEffect(activator);
            activator.removeEffect(effect);
            activator.sendMessage(MessageKeys.CLEANSE_CLEAN, "{effect}", effect.getEffectType().getName());
        }
        for (PotionEffect effects : activator.getEntity().getActivePotionEffects()) {
            activator.getEntity().removePotionEffect(effects.getType());
        }
        return true;
    }

}
