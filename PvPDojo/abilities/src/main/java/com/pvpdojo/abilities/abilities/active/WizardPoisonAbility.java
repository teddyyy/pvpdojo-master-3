/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.entity.Snowball;
import org.bukkit.event.Event;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.ProjectileEntity;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.PoisonEffect;

public class WizardPoisonAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "WizardPoisonAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {

        AbilityEntity activator = ability.getAbilityEntity();
        final int duration = (int) ability.getAttribute("duration");
        Snowball snowball = activator.getEntity().launchProjectile(Snowball.class);
        snowball.setVelocity(snowball.getVelocity().multiply(1.2));
        ProjectileEntity projectileEntity = new ProjectileEntity(activator);
        projectileEntity.startProjectileCollision(snowball);
        projectileEntity.setProjectileDetection(target -> target.applyEffect(new PoisonEffect(duration * 20, 0)));

        return true;
    }

}
