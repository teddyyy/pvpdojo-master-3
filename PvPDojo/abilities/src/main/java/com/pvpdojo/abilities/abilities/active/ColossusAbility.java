/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;

public class ColossusAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "ColossusAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        AbilityEntity activator = ability.getAbilityEntity();
        Location center = activator.getEntity().getLocation();
        Set<AbilityEntity> flying = new HashSet<>();

        float damage = ability.getAttribute("ability_damage");
        float radius = ability.getAttribute("radius");

        double amount = 1.5 * radius;
        double increment = (2 * Math.PI) / amount;
        for (int i = 0; i < amount; i++) {
            double angle = i * increment;
            double x = center.getX() + (radius * Math.cos(angle));
            double z = center.getZ() + (radius * Math.sin(angle));

            new BukkitRunnable() {

                int i = 0;

                public void run() {
                    Location loc = new Location(center.getWorld(), x, center.getY() + i, z);
                    i++;

                    if (i <= 6) {
                        if (loc.getBlock().getType() == Material.AIR) {
                            loc.getBlock().getWorld().playEffect(loc, Effect.STEP_SOUND, Material.SANDSTONE);
                            loc.getBlock().setType(Material.SANDSTONE);

                            PvPDojo.schedule(() -> {
                                loc.getBlock().getWorld().playEffect(loc, Effect.STEP_SOUND, Material.SANDSTONE);
                                loc.getBlock().setType(Material.AIR);
                            }).createTask(10 * 20);

                        }
                    }

                    if (i > 4) {
                        for (AbilityEntity nearby : AbilityServer.get().getNearbyEntities(center, radius, 2)) {

                            if (nearby != activator) {

                                if (!flying.contains(nearby)) {

                                    nearby.recieveAbilityDamage(damage, activator, ability, DamageCause.MAGIC);
                                    nearby.getEntity().setVelocity(new Vector(0.0D, 1.75D, 0.0D));
                                    flying.add(nearby);
                                }
                            }
                        }
                    }

                    if (i >= 16) {
                        this.cancel();
                    }

                }
            }.runTaskTimer(PvPDojo.get(), 5, 5);

        }
        return true;
    }

}
