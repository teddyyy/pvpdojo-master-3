/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.guns;

import java.util.ArrayList;
import java.util.Collections;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.util.bukkit.CC;

import eu.the5zig.mod.server.api.ModUser;

public class GunPlayer {

    private Gun currentlyFiring;
    private boolean aimed = false;
    private AbilityEntity activator;
    private ArrayList<Gun> guns = new ArrayList<>();

    public GunPlayer(AbilityEntity activator) {
        this.activator = activator;
    }

    private boolean modCountDownRunning;

    public void updateForModUser(Gun gun, boolean writeText) {
        if (getController() instanceof Player) {
            ModUser modUser = ModUser.getUser((Player) getController());
            if (modUser != null) {
                if (writeText) {
                    modUser.getStatsManager().getStat("Ammo").setScore(String.valueOf(gun.ammoLeft));
                    if (gun.hasClip && gun.ammoLeft > 0) {
                        modUser.getStatsManager().getStat("Clip").setScore(String.valueOf(gun.maxClipSize - gun.roundsFired));
                    } else {
                        modUser.getStatsManager().resetStat("Clip");
                    }
                }
                if (gun.reloading) {
                    if (isHoldingGun(gun)) {
                        if (!modCountDownRunning) {
                            modUser.getStatsManager().startCountdown("Reloading", gun.gunReloadTimer * 50);
                            modCountDownRunning = true;
                        }
                    } else {
                        modUser.getStatsManager().resetCountdown();
                        modCountDownRunning = false;
                    }
                }
            }
        }
    }

    public void clearForModUser() {
        if (getController() instanceof Player) {
            ModUser modUser = ModUser.getUser((Player) getController());
            if (modUser != null) {
                modUser.getStatsManager().resetStat("Ammo");
                modUser.getStatsManager().resetStat("Clip");
                modUser.getStatsManager().resetCountdown();
                modCountDownRunning = false;
            }
        }
    }

    public boolean isAimedIn() {
        if (this.getController() == null)
            return false;
        if (!this.aimed) {
            return false;
        }

        return this.getController().hasPotionEffect(PotionEffectType.SLOW);
    }

    public LivingEntity getController() {
        if (activator == null)
            return null;
        return activator.getEntity();
    }

    public AbilityEntity getActivator() {
        return activator;
    }

    public void setActivator(AbilityEntity activator) {
        this.activator = activator;
    }

    public void playSound(Location location, Sound sound, float f, float f2) {
        if (getController() instanceof Player) {
            ((Player) getController()).playSound(location, sound, f, f2);
        }
    }

    public void sendMessage(MessageKeys message) {
        this.activator.sendMessage(message);
    }

    public boolean isSneaking() {
        if (getController() instanceof Player) {
            return ((Player) getController()).isSneaking();
        }
        return false;
    }

    public boolean isHoldingGun(Gun gun) {
        if (getController() instanceof Player) {
            return gun.getAbility().getClickItem().equals(((Player) getController()).getItemInHand());
        }
        return true;

    }

    public boolean clickGunAbility(String ability_name) {
        return onClick("right", ability_name);
    }

    public boolean onClick(String clickType, String ability) {
        Gun holding = this.getGun(ability);
        if (holding.getMaxAmmo() == 0) {
            holding.setMaxAmmo(this.getAmmoLeft(holding));
        }
        if (((holding.canClickRight) || (holding.canAimRight())) && (clickType.equals("right"))) {
            if (!holding.canAimRight()) {

                holding.heldDownTicks += 1;
                holding.lastFired = 0;
                if (this.currentlyFiring == null) {
                    return fireGun(holding);
                }
            } else {
                checkAim();
            }
        } else if (((holding.canClickLeft) || (holding.canAimLeft())) && (clickType.equals("left"))) {
            if (!holding.canAimLeft()) {
                holding.heldDownTicks = 0;
                if (this.currentlyFiring == null) {
                    return fireGun(holding);
                }
            } else {
                checkAim();
            }
        }
        return false;
    }

    public void checkAim() {
        if (isAimedIn()) {
            this.getController().removePotionEffect(PotionEffectType.SLOW);
            aimed = false;
        } else {
            this.getController().addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 12000, 4));
            aimed = true;
        }
    }

    private boolean fireGun(Gun gun) {
        if (gun.timer <= 0) {
            this.currentlyFiring = gun;
            gun.firing = true;
            return true;
        }
        return false;
    }

    public void destroy() {
        for (int i = this.guns.size() - 1; i >= 0; i--) {
            Gun g = this.guns.get(i);
            g.clear();
        }
        this.guns.clear();
        activator = null;
    }

    public void tick() {
        if (this.getController() == null)
            return;
        for (int i = this.guns.size() - 1; i >= 0; i--) {
            Gun g = this.guns.get(i);
            g.tick();
            if (this.getController().isDead()) {
                g.finishReloading();
            }
            if ((isAimedIn()) && (!g.canAimLeft()) && (!g.canAimRight())) {
                this.getController().removePotionEffect(PotionEffectType.SLOW);
            }

            if ((this.currentlyFiring != null) && (g.timer <= 0) && (this.currentlyFiring.equals(g)))
                this.currentlyFiring = null;
        }
        // remove aim in slow effect
        // check if kit contains it
        // update gun lore
        // set guns max ammo
    }

    public void registerGun(Gun gun) {
        for (int i = this.guns.size() - 1; i >= 0; i--) {
            Gun g = guns.get(i);
            if (g.getName().equals(gun.getName()))
                guns.remove(g);
        }
        gun.owner = this;
        updateForModUser(gun, true);
        this.guns.add(gun);
    }

    public ArrayList<Gun> getGuns() {
        return guns;
    }

    public Gun getGun(String ability) {
        for (Gun gun : guns) {
            if (gun.getName().equals(ability)) {
                return gun;
            }
        }
        throw new IllegalArgumentException("invalid gun ability : " + ability);
    }

    public int getAmmoLeft(Gun current) {
        int ammoLeft;
        int maxInClip = current.maxClipSize;

        int currentAmmo = (int) Math.floor(current.ammoLeft) / current.getAmmoAmtNeeded();
        ammoLeft = currentAmmo - maxInClip + current.roundsFired;
        return ammoLeft;
    }

    private static final String DIVIDER = "\u007C";
    private static final String BLACK_SQUARE = "\u2B1B";
    private static final String CLEAR_SQUARE = "\u2B1C";

    public String getGunLore(Gun current) {
        String add = "";
        StringBuilder refresh = new StringBuilder();

        if (current.hasClip) {
            int leftInClip;
            int ammoLeft;
            int maxInClip = current.maxClipSize;

            int currentAmmo = (int) Math.floor(current.ammoLeft) / current.getAmmoAmtNeeded();

            ammoLeft = currentAmmo - maxInClip + current.roundsFired;

            if (ammoLeft < 0) {
                ammoLeft = 0;
            }

            leftInClip = currentAmmo - ammoLeft;

            add = CC.YELLOW + "    " + leftInClip + " " + DIVIDER + " " + ammoLeft;

            if (current.reloading) {
                int reloadSize = 4;

                double reloadFrac = (double) (current.getReloadTime() - current.gunReloadTimer) / current.getReloadTime();

                int amt = (int) Math.round(reloadFrac * reloadSize);

                for (int i = 0; i < amt; i++) {
                    refresh.append(BLACK_SQUARE);
                }

                for (int j = 0; j < reloadSize - amt; j++) {
                    refresh.append(CLEAR_SQUARE);
                }

                add = CC.RED + "    " + new StringBuffer(refresh.toString()).reverse() + " RELOADING " + refresh;
            }
        }

        String name = current.getName();

        return name + add;
    }

    public ItemStack setLore(ItemStack item, String lore) {
        ItemMeta im = item.getItemMeta();
        im.setLore(Collections.singletonList(lore));
        item.setItemMeta(im);

        return item;
    }

    public boolean checkAmmo(Gun gun, int amount) {
        return gun.ammoLeft >= amount;
    }

    public void addAmmo(Gun gun, int amount) {
        gun.ammoLeft += amount;
    }

    public void removeAmmo(Gun gun, int amount) {
        if (amount == 0)
            return;
        gun.ammoLeft -= amount;
        updateForModUser(gun, true);
    }
}