/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.base.entities;

import org.bukkit.entity.Monster;

public class AbilityMonster extends AbilityEntity {

    public AbilityMonster(Monster monster) {
        super(monster);
    }
    
    @Override
    public Monster getEntity() {
        return (Monster) super.getEntity();
    }

}
