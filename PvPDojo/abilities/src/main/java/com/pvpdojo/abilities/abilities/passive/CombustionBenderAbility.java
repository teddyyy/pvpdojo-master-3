package com.pvpdojo.abilities.abilities.passive;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.effects.ParticleEffectRotation;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.abilities.lib.ChargeSystem;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.gamemode.BurnEffect;
import com.pvpdojo.util.bukkit.ParticleEffects;
import net.minecraft.server.v1_7_R4.World;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_7_R4.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.util.Vector;

import java.util.EnumSet;

public class CombustionBenderAbility extends AbilityExecutor {
    @Override
    public String getExecutorName() {
        return "CombustionBenderAbility";
    }

    private static final EnumSet<ParticleEffects> EFFECTS = EnumSet.of(ParticleEffects.FIREWORK_SPARK, ParticleEffects.ENCHANTMENT_TABLE, ParticleEffects.CLOUD);

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        if (event instanceof PlayerToggleSneakEvent) {
            Player player = ((PlayerToggleSneakEvent) event).getPlayer();
            float fast = ability.getAttribute("fast");

            ChargeSystem charge = new ChargeSystem(ability, 100, (int) fast) {
                @Override
                public void trigger() {
                    callCombustionBender(ability);
                }

                @Override
                public void run() {

                }
            };
            charge.call();
        }

        return false;
    }

    private void callCombustionBender(Ability ability) {
        Location loc = ability.getAbilityEntity().getEntity().getEyeLocation();
        loc.getWorld().playSound(loc, Sound.FIREWORK_LARGE_BLAST2, 4f, 4f);

        //This thing contains a shit ton of math.
        new AbilityRunnable(ability) {

            int loop = 0;
            int shockWaves = 0;
            boolean hit = false;

            @Override
            public void run() {
                if (++loop >= 32) {
                    this.cancel();
                    return;
                }

                //Magic starts.

                double x, y, z;
                double r = 0.7;

                x = Math.cos(loop * loop) * 0.2;
                y = Math.sin(loop * loop) * 0.2;
                z = loop * 2;

                Vector v = new Vector(x, y, z);
                v = ParticleEffectRotation.rotateFunction(v, loc);

                loc.add(v.getX(), v.getY(), v.getZ());

                if (loop % 10 == 0) {
                    ParticleEffects.LARGE_EXPLODE.sendToLocation(loc, 0, 0, 0, 0, 1);
                    loc.getWorld().playSound(loc, Sound.EXPLODE, 1.0f, 1.0f);
                }

                EFFECTS.forEach(particleEffects -> particleEffects.sendToLocation(loc, 0, 0, 0, 0, 1));

                if (hit) {
                    if (++shockWaves % 10 == 0) {

                        ParticleEffects.LARGE_EXPLODE.sendToLocation(loc.add(PvPDojo.RANDOM.nextInt(3), 0, PvPDojo.RANDOM.nextInt(3)), 0, 0, 0, 0, 1);

                        World world = ((CraftWorld) loc.getWorld()).getHandle();
                        world.makeSound(loc.getX(), loc.getY(), loc.getZ(), "random.explode", 4.0F,
                                (1.0F + (world.random.nextFloat() - world.random.nextFloat()) * 0.2F) * 0.7F);//Real sounds.
                    }

                    AbilityServer.get().getNearbyEntities(ability.getAbilityEntity(), loc, 5, 5)
                            .forEach(target -> {
                                ability.getAbilityEntity().abilityDamage(target, ability.getAttribute("ability_damage"), ability, EntityDamageEvent.DamageCause.BLOCK_EXPLOSION);
                                target.getEntity().setVelocity(target.getEntity().getLocation().getDirection().normalize().multiply(-1.63).setY(0.4));
                                target.applyEffect(new BurnEffect((int) (20 * ability.getAttribute("fire"))));

                                if (!hit) {
                                    hit = true;
                                }
                            });

                    // this.cancel();
                }
                if (!loc.getBlock().getType().isSolid()) {
                    if (!hit) {
                        hit = true;
                        return;
                    }
                }

                loc.subtract(v.getX(), v.getY(), v.getZ());

                x = -x;
                y = -y;

                Vector v1 = new Vector(x, y, z);
                v1 = ParticleEffectRotation.rotateFunction(v1, loc);

                loc.add(v1.getX(), v1.getY(), v1.getZ());
                EFFECTS.forEach(particleEffects -> particleEffects.sendToLocation(loc, 0, 0, 0, 0, 1));
                loc.subtract(v1.getX(), v1.getY(), v1.getZ());

                for (double theta = 0; theta <= 2 * Math.PI; theta = theta + Math.PI / 12.0) {

                    x = r * Math.cos(theta);
                    y = r * Math.sin(theta);
                    z = loop * loop;

                    Vector vector = new Vector(x, y, z);
                    vector = ParticleEffectRotation.rotateFunction(vector, loc);

                    loc.add(vector.getX(), vector.getY(), vector.getZ());
                    EFFECTS.forEach(particleEffects -> particleEffects.sendToLocation(loc, 0, 0, 0, 0, 1));
                    loc.subtract(vector.getX(), vector.getY(), vector.getZ());
                }
                // Magic Ends.
            }
        }.createTimer(0, -1);
    }

    @EventHandler
    public void onCombustionBender(PlayerToggleSneakEvent e) {
        if (canAbilityExecute(e.getPlayer())) {
            getAbility(e.getPlayer()).activateAbility(e);
        }
    }
}
