/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.effects.PrisonerEffect;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.abilities.lib.ProjectileEntity;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.PoisonEffect;
import com.pvpdojo.abilities.gamemode.WitherEffect;
import com.pvpdojo.achievement.Achievement;
import com.pvpdojo.achievement.AchievementManager;
import com.pvpdojo.bukkit.util.TempBlock;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.CC;

public class CataclymBeamAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "CataclymBeamAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        final AbilityEntity activator = ability.getAbilityEntity();
        final int uses = ability.getCooldown().getUsesLeft();
        final float ice_duration = ability.getAttribute("ice_duration");
        final float poison_damage = ability.getAttribute("poison_power");
        final float poison_duration = ability.getAttribute("poison_duration");
        final int prisoner_duration = (int) ability.getAttribute("prisoner_duration");
        final float distance = ability.getAttribute("distance");
        final float speed = ability.getAttribute("projectile_speed");
        final float damage = ability.getAttribute("ability_damage");

        ProjectileEntity projectile = new ProjectileEntity(activator, false, 1.5f);

        projectile.setProjectileLoop(loc -> {
            Material type = Material.REDSTONE_BLOCK;
            if (uses == 2)
                type = Material.EMERALD_BLOCK;
            if (uses == 1)
                type = Material.LAPIS_BLOCK;
            loc.getWorld().playEffect(loc, Effect.STEP_SOUND, type);
            return false;
        });

        projectile.setProjectileDetection(target -> {
            activator.abilityDamage(target, damage / 3, ability, DamageCause.MAGIC);
            LivingEntity living = target.getEntity();
            if (uses == 3) {
                boolean antiPrisoner = target.hasAbility(AbilityLoader.getAbilityData(79).getName());

                if (antiPrisoner) {
                    activator.sendMessage(MessageKeys.GENERIC, "{text}", CC.RED + "Bad luck you hit an Anti Prisoner");
                }

                //Location loc = BlockUtil.findLocationBetweenPoints(target.getEntity().getLocation().clone().add(0, 7, 0), target.getEntity().getLocation());
                //trying ambush code
                target.getEntity().eject();
                Location loc = target.getEntity().getLocation().add(0, 7, 0);
                target.getEntity().teleport(BlockUtil.findLocationBetweenPoints(loc, target.getEntity().getLocation()));
                new PrisonerEffect(loc, activator, target,antiPrisoner ? prisoner_duration / 2 : prisoner_duration);

                // Prisoner survive achievement
                if (target.getEntity() instanceof Player) {
                    new AbilityRunnable(ability) {
                        int loop;

                        @Override
                        public void run() {
                            if (target.getEntity().isDead()) {
                                cancel();
                                return;
                            }
                            if (++loop > prisoner_duration) {
                                AchievementManager.inst().trigger(Achievement.ESCAPEE, (Player) target.getEntity());
                            }
                        }
                    }.createTimer(20, -1);
                }
            }
            if (uses == 2) {
                target.applyEffect(new PoisonEffect((int) (poison_duration * 20), (int) poison_damage));
                target.getEntity().getWorld().spigot().strikeLightningEffect(target.getEntity().getEyeLocation(), false);
            }
            if (uses == 1) {
                Location center = living.getLocation().clone().add(0, 0, 0);
                createIceBox(center, (int) ice_duration);
                Location tp = center.getWorld().getBlockAt(center.getBlockX(), center.getBlockY(), center.getBlockZ()).getLocation().add(.45, .45, .45);
                living.teleport(tp);
                target.applyEffect(new WitherEffect((int) ice_duration * 20, 3));
            }
        });
        projectile.startBlockCollision(activator.getEntity().getEyeLocation(), 1, (int) speed, (int) distance);

        return true;
    }

    private void createIceBox(Location center, int duration) {
        for (int x = -1; x < 2; x++) {
            for (int y = -1; y < 3; y++) {
                for (int z = -1; z < 2; z++) {
                    Location loc = center.clone().add(x, y, z);
                    TempBlock.createTempBlock(loc, Material.ICE, (byte) 0, duration);
                }
            }
        }
    }

}
