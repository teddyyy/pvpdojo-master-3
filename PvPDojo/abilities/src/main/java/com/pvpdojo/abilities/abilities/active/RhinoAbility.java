/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;

public class RhinoAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "RhinoAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        LivingEntity entity = ability.getAbilityEntity().getEntity();

        entity.setVelocity(entity.getLocation().getDirection().multiply(4).setY(0.5));

        for (AbilityEntity target : AbilityServer.get().getNearbyEntities(ability.getAbilityEntity(), 6)) {
            target.getEntity().setVelocity(entity.getLocation().getDirection().multiply(3).setY(1.3));
        }

        entity.getWorld().playSound(entity.getLocation(), Sound.CHEST_OPEN, 1, 1);
        return true;
    }

}
