/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.command;

import org.bukkit.entity.Player;

import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Flags;

@CommandAlias("seekit|ckit|showkit")
public class SeeAbilitiesCommand extends DojoCommand {

    @Default
    public void onSeeKit(Player player) {
        player.sendMessage(AbilityServer.get().getPlayer(player).listAbilities());
    }

    @CommandPermission("Staff")
    @CommandCompletion("@players")
    @CatchUnknown
    public void onSeeKit(Player player, @Flags("other") Player target) {
        player.sendMessage(AbilityServer.get().getPlayer(target).listAbilities());
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
