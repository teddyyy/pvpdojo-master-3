/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.listeners.events;

import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;

public class AbilityActivateEvent extends AbilityEvent {

    private final Ability ability;
    private final boolean cooldown;
    private boolean sendCooldown = true;

    public AbilityActivateEvent(AbilityEntity entity, Ability ability, boolean cooldown) {
        super(entity);
        this.ability = ability;
        this.cooldown = cooldown;
    }

    public Ability getAbility() {
        return ability;
    }

    public boolean isCooldown() {
        return cooldown;
    }

    public void setSendCooldown(boolean sendCooldown) {
        this.sendCooldown = sendCooldown;
    }

    public boolean isSendCooldown() {
        return sendCooldown;
    }
}
