package com.pvpdojo.abilities.abilities.active;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ParticleEffects;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftLivingEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.Random;

public class SuffocatorAbility extends AbilityExecutor {
    @Override
    public String getExecutorName() {
        return "SuffocatorAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        if (event instanceof PlayerInteractEntityEvent) {
            Player player = ((PlayerInteractEntityEvent) event).getPlayer();
            LivingEntity clicked = (LivingEntity) ((PlayerInteractEntityEvent) event).getRightClicked();

            if (!AbilityServer.get().getEntity(player).canTarget(AbilityEntity.get(clicked))) {
                return false;
            }

            float duration = ability.getAttribute("duration");
            float ability_damage = ability.getAttribute("ability_damage");

            new AbilityRunnable(ability) {

                float counter = 20 * duration;

                boolean sent;

                @Override
                public void run() {
                    if (--counter <= 0 || clicked.isDead()) {
                        cancel();
                        return;
                    }

                    Location loc = clicked.getEyeLocation().add(0, 0.25D, 0);
                    for (int i = 0; i < 25; i++) {
                        Vector vec = getRandomVelocity().multiply(0.5);
                        loc.add(vec);
                        ParticleEffects.SPELL.sendToLocation(loc, 0, 0, 0, 0, 1);
                        loc.subtract(vec);
                    }
                    boolean sprint = ((CraftLivingEntity) clicked).getHandle().isSprinting();

                    if (sprint) {
                        if (!sent) {
                            sent = true;

                            if (clicked instanceof Player) {
                                ((Player) clicked).sendMessage(PvPDojo.PREFIX + CC.RED + "You are feeling exhausted, better stop sprinting to reduce damage.");
                            }

                        }
                    }

                    clicked.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 40, 0));
                    ability.getAbilityEntity().abilityDamage(AbilityEntity.get(clicked), sprint ? ability_damage + 25 : ability_damage, ability, EntityDamageEvent.DamageCause.SUFFOCATION);

                }
            }.createTimer(1, -1);
            return true;
        }
        return false;
    }

    @EventHandler
    public void onInteraction(PlayerInteractEntityEvent e) {
        Player player = e.getPlayer();
        if (canAbilityExecute(player)) {
            Ability ability = getAbility(player);
            if (player.getItemInHand().isSimilar(ability.getClickItem())) {
                ability.activateAbility(e);
            }
        }
    }

    private Vector getRandomVelocity() {
        final Random random = new Random(System.nanoTime());

        double x = random.nextDouble() * 2 - 1;
        double y = random.nextDouble() * 2 - 1;
        double z = random.nextDouble() * 2 - 1;

        return new Vector(x, y, z).normalize();
    }
}
