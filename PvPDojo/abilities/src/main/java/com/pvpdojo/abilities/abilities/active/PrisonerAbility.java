/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.effects.PrisonerEffect;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.abilities.lib.ProjectileEntity;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.BurnEffect;
import com.pvpdojo.achievement.Achievement;
import com.pvpdojo.achievement.AchievementManager;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.CC;

public class PrisonerAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "PrisonerAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        final AbilityEntity activator = ability.getAbilityEntity();
        final float damage = ability.getAttribute("ability_damage");
        final int duration = (int) ability.getAttribute("duration");
        final float afterBurn = ability.getAttribute("after_burn");
        ProjectileEntity projectile = new ProjectileEntity(activator, false, 1.5F);

        projectile.setProjectileLoop(loc -> {
            loc.getWorld().playEffect(loc, Effect.STEP_SOUND, Material.REDSTONE_BLOCK);
            return false;
        });

        projectile.setProjectileDetection(target -> {
            boolean antiPrisoner = target.hasAbility(AbilityLoader.getAbilityData(79).getName());

            if (antiPrisoner) { // Anti Prisoner
                activator.sendMessage(MessageKeys.GENERIC, "{text}", CC.RED + "Bad luck you hit an Anti Prisoner");
            }
            target.getEntity().eject();

            Location loc = target.getEntity().getLocation().add(0, 7, 0);
            target.getEntity().teleport(BlockUtil.findLocationBetweenPoints(loc, target.getEntity().getLocation()));
            new PrisonerEffect(loc, activator, target, antiPrisoner ? duration / 2 : duration);

            if (afterBurn > 0) {
                target.applyEffect(new BurnEffect(20 * 20));
            }

            // Prisoner survive achievement
            if (target.getEntity() instanceof Player) {
                new AbilityRunnable(ability) {
                    int loop;

                    @Override
                    public void run() {
                        if (target.getEntity().isDead()) {
                            cancel();
                            return;
                        }
                        if (++loop > duration) {
                            AchievementManager.inst().trigger(Achievement.ESCAPEE, (Player) target.getEntity());
                        }
                    }
                }.createTimer(20, -1);
            }
        });

        projectile.startBlockCollision(activator.getEntity().getEyeLocation(), 1, 2, 40);

        return true;
    }

}
