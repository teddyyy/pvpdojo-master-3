/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.lib;

import org.bukkit.Location;

public interface ProjectileLoop {

    boolean onLoop(Location loc);
}
