/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.listeners.events.AbilityTargetEvent;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.util.BroadcastUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.ExpireHashMap;

public class SelectorAbility extends AbilityExecutor {

    private ExpireHashMap<UUID, UUID> selected = new ExpireHashMap<>();

    @Override
    public String getExecutorName() {
        return "SelectorAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        return true;
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        UUID dead = e.getEntity().getUniqueId();
        selected.entrySet().removeIf(entry -> entry.getKey().equals(dead) || entry.getValue().equals(dead));
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof LivingEntity && e.getEntity() instanceof LivingEntity) {
            AbilityEntity victim = AbilityServer.get().getEntity((LivingEntity) e.getEntity());
            AbilityEntity damager = AbilityServer.get().getEntity((LivingEntity) e.getDamager());
            if (victim instanceof AbilityPlayer && damager instanceof AbilityPlayer) {
                if (canAbilityExecute(damager.getEntity())) {
                    Ability ability = getAbility(damager.getEntity());
                    float duration = ability.getAttribute("duration");

                    UUID damagerUUID = ((AbilityPlayer) damager).getUUID();
                    UUID victimUUID = ((AbilityPlayer) victim).getUUID();

                    if (!selected.containsKey(damagerUUID) && !selected.containsKey(victimUUID)) {
                        startDuel(((AbilityPlayer) damager).getPlayer(), ((AbilityPlayer) victim).getPlayer(), (int) duration);
                    } else if (selected.containsKey(damagerUUID) && selected.get(damagerUUID).equals(victimUUID)) {
                        refreshDuel(((AbilityPlayer) damager).getPlayer(), ((AbilityPlayer) victim).getPlayer(), (int) duration);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onEntityDamageByEntity(AbilityTargetEvent e) {
        if (selected.size() == 0)
            return;
        if (e.getEntity() instanceof AbilityPlayer && e.getAttacker() instanceof AbilityPlayer) {
            AbilityPlayer victim = (AbilityPlayer) e.getEntity();
            AbilityPlayer damager = (AbilityPlayer) e.getAttacker();

            if (selected.containsKey(damager.getUUID())) {
                UUID check = selected.get(damager.getUUID());
                if (!check.equals(victim.getUUID())) {
                    e.setCancelled(true);
                    damager.sendMessage(MessageKeys.SELECTOR_DUEL_WARN);
                    return;
                }
            }
            if (selected.containsKey(victim.getUUID())) {
                UUID check = selected.get(victim.getUUID());
                if (!check.equals(damager.getUUID())) {
                    e.setCancelled(true);
                    damager.sendMessage(MessageKeys.SELECTOR_DUEL_WARN_OTHER);
                }
            }
        }
    }

    private void refreshDuel(Player player, Player player2, int time) {
        selected.put(player.getUniqueId(), player2.getUniqueId(), time, TimeUnit.SECONDS);
        selected.put(player2.getUniqueId(), player.getUniqueId(), time, TimeUnit.SECONDS);
    }

    private void startDuel(final Player player, Player player2, int time) {
        player.sendMessage(PvPDojo.PREFIX + CC.RED + "You are now in a duel with " + player2.getNick());
        player2.sendMessage(CC.RED + "A selector has hit you, you are now in a duel with " + player.getNick() + "!");
        BroadcastUtil.broadcastNearby(player.getLocation(), PvPDojo.PREFIX + CC.BLUE + player.getNick() + CC.GRAY + " and " + CC.BLUE + player2.getNick() + CC.GRAY
                + " are in now in a duel for " + CC.GREEN + time + CC.GRAY + " seconds", 15);
        refreshDuel(player, player2, time);
    }
}
