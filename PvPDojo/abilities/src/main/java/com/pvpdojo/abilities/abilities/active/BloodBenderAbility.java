package com.pvpdojo.abilities.abilities.active;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityCrosshair;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.abilities.ActiveAbility;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class BloodBenderAbility extends AbilityExecutor {
    @Override
    public String getExecutorName() {
        return "BloodBenderAbility";
    }

    private static final String DELAY_KEY = "DELAY";

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        if (!(ability.getAbilityEntity() instanceof AbilityPlayer)) {
            return false;
        }

        AbilityEntity player = ability.getAbilityEntity();

        boolean bool = new AbilityCrosshair(player) {

            boolean sound = false;
            final String TELEPORT = "teleportation";

            float ROTATE_POWER;

            {
                int rand = PvPDojo.RANDOM.nextInt(2);
                if (rand == 0) {
                    ROTATE_POWER = 180;
                } else if (rand == 1) {
                    ROTATE_POWER = 270;
                } else {
                    ROTATE_POWER = 90;
                }
            }

            @Override
            public void targetFound(AbilityEntity target, double accuracy, int distance) {
                Location rotatedLocation = null;
                if (!target.getEntity().hasMetadata(TELEPORT)) {
                    float rotation = (target.getEntity().getLocation().getYaw() - ROTATE_POWER) % 360;
                    rotatedLocation = new Location(target.getEntity().getWorld(), target.getEntity().getLocation().getX(), target.getEntity().getLocation().getY(), target.getEntity().getLocation().getZ(), rotation, target.getEntity().getLocation().getPitch());

                    target.getEntity().teleport(rotatedLocation);
                    target.getEntity().setMetadata(TELEPORT, new FixedMetadataValue(PvPDojo.get(), null));
                    PvPDojo.schedule(() -> target.getEntity().removeMetadata(TELEPORT, PvPDojo.get())).createTask(4);

                }

                if (target.getEntity() instanceof Player) {
                    ((Player) target.getEntity()).playSound(target.getEntity().getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 1);
                }

                if (!sound) {//No ear rapes
                    sound = true;
                    assert rotatedLocation != null;
                    rotatedLocation.getWorld().playSound(rotatedLocation, Sound.ZOMBIE_WOODBREAK, 1, 1);
                }

                PvPDojo.schedule(() -> target.getAbilities()
                        .stream()
                        .filter(ab -> ab instanceof ActiveAbility)
                        .filter(ab -> !ab.getCooldown().isCoolingDown())
                        .filter(ab -> ab.getAbilityEntity().getMana() > ab.getAttribute("mana_use"))
                        .forEach(ab -> ab.activateAbility(true, null))).nextTick();

                player.abilityDamage(target, 175, ability, EntityDamageEvent.DamageCause.MAGIC);
            }
        }.calculate(3, 17, false);

        if (bool) {
            ability.getMetaData().addData(DELAY_KEY, ability.getMetaData().getData(DELAY_KEY) == null ? 1 : (int) ability.getMetaData().getData(DELAY_KEY) + 1);
        }

        if ((int) ability.getMetaData().getData(DELAY_KEY) >= 3) {
            ability.getMetaData().removeData(DELAY_KEY);
            return true;
        }

        return false;

    }


}
