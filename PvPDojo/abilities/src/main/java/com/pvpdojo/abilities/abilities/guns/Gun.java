/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.guns;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.lang.MessageKeys;

public class Gun {

    private boolean canHeadshot;
    private boolean hasSmokeTrail;
    private boolean localGunSound;
    private boolean canAimLeft;
    private boolean canAimRight;
    private boolean canAimSneak;
    private boolean canGoPastMaxDistance;
    private byte gunByte;
    private byte ammoByte;
    private int ammoType;
    private int ammoAmtNeeded;
    private float gunDamage;
    private int explosionDamage = -1;
    private int roundsPerBurst;
    private int reloadTime;
    private int maxDistance;
    private int bulletsPerClick;
    private int bulletsShot;
    private int releaseTime = -1;
    private double bulletSpeed;
    private double accuracy;
    private double accuracy_aimed = -1.0D;
    private double accuracy_crouched = -1.0D;
    private float headshot_multiplier = 1.0f;
    private double explodeRadius;
    private double fireRadius;
    private double flashRadius;
    private double knockback;
    private double recoil;
    private double gunVolume = 1.0D;
    private String gunName;
    private String fileName;
    public String projType = "";
    public ArrayList<String> gunSound = new ArrayList<>();
    public boolean canClickRight;
    public boolean canClickLeft;
    public boolean hasClip = true;
    public boolean ignoreItemData = false;
    public boolean reloadGunOnDrop = true;
    public int maxClipSize = 30;
    public int bulletDelayTime = 10;
    public int roundsFired;
    public int gunReloadTimer;
    public int timer;
    public int lastFired;
    public int ticks;
    public int heldDownTicks;
    public int maxAmmo = 0;
    public int ammoLeft = -11;
    public int ammoReset = 0;
    public double gun_recoil = 0;
    public boolean firing = false;
    public boolean reloading;
    public boolean changed = false;
    public GunPlayer owner;
    public String node;
    public String reloadType = "NORMAL";
    private GunEffect releaseEffect;
    private ArrayList<Bullet> bullets = new ArrayList<>();
    private Ability ability;

    public Gun(String name, Ability ability) {
        this.gunName = name;
        this.fileName = name;
        this.ability = ability;
    }

    public Ability getAbility() {
        return ability;
    }

    public float getHeadShotMultiplier() {
        return this.headshot_multiplier;
    }

    public void setHeadShotMultiplier(float amount) {
        this.headshot_multiplier = amount;
    }

    public void shoot() {
        if ((this.owner != null) && (!this.reloading)) {
            if (((this.owner.checkAmmo(this, this.getAmmoAmtNeeded())) && (this.getAmmoAmtNeeded() > 0)) || (this.getAmmoAmtNeeded() == 0)) {
                this.owner.removeAmmo(this, this.getAmmoAmtNeeded());
                if ((this.roundsFired >= this.maxClipSize) && (this.hasClip)) {
                    reloadGun();
                    return;
                }
                doRecoil(this.owner.getController());
                this.changed = true;
                this.roundsFired += 1;
                for (String aGunSound : this.gunSound) {
                    String snd = aGunSound.toUpperCase().replace(" ", "_");
                    Sound sound = Sound.valueOf(snd);

                    if (this.localGunSound)
                        this.owner.playSound(this.owner.getController().getLocation(), sound, (float) this.gunVolume, 2.0F);
                    else {
                        this.owner.getController().getWorld().playSound(this.owner.getController().getLocation(), sound, (float) this.gunVolume, 2.0F);
                    }
                }

                double gun_accuracy = this.accuracy;
                if ((this.owner.isSneaking()) && (this.getAccuracy_crouched() > -1.0D))
                    gun_accuracy = this.getAccuracy_crouched();
                if ((this.owner.isAimedIn()) && (this.getAccuracy_aimed() > -1.0D))
                    gun_accuracy = this.getAccuracy_aimed();

                for (int i = 0; i < this.bulletsPerClick; i++) {
                    int acc = (int) (gun_accuracy * 1000.0D);

                    if (acc <= 0) {
                        acc = 1;
                    }
                    Location ploc = this.owner.getController().getLocation();
                    Random rand = PvPDojo.RANDOM;
                    double dir = -ploc.getYaw() - 90.0F;
                    double pitch = -ploc.getPitch();
                    double xwep = (rand.nextInt(acc) - rand.nextInt(acc) + 0.5D) / 1000.0D;
                    double ywep = (rand.nextInt(acc) - rand.nextInt(acc) + 0.5D) / 1000.0D;
                    double zwep = (rand.nextInt(acc) - rand.nextInt(acc) + 0.5D) / 1000.0D;
                    double xd = Math.cos(Math.toRadians(dir)) * Math.cos(Math.toRadians(pitch)) + xwep;
                    double yd = Math.sin(Math.toRadians(pitch)) + ywep
                            + ((heldDownTicks > 1 ? ((Math.min(this.heldDownTicks - 1, 3)) * this.gun_recoil) : 0) / (this.owner.isSneaking() ? 1.2 : 1));
                    double zd = -Math.sin(Math.toRadians(dir)) * Math.cos(Math.toRadians(pitch)) + zwep;
                    Vector vec = new Vector(xd, yd, zd);
                    vec.multiply(this.bulletSpeed);
                    Bullet bullet = new Bullet(this.owner, vec, this);
                    this.bullets.add(bullet);
                }

                if ((this.roundsFired >= this.maxClipSize) && (this.hasClip))
                    reloadGun();
            } else {
                this.owner.playSound(this.owner.getController().getLocation(), Sound.ITEM_BREAK, 20.0F, 20.0F);
                this.owner.sendMessage(MessageKeys.OUT_OF_AMMO);
                finishShooting();
            }
        }
    }

    public void removeBullet(Bullet bullet) {
        this.bullets.remove(bullet);
    }

    boolean wasHolding = false;

    public void tick() {

        this.ticks += 1;
        this.lastFired += 1;
        this.timer -= 1;
        this.gunReloadTimer -= 1;

        if (owner.isHoldingGun(this) != wasHolding) {
            owner.updateForModUser(this, !wasHolding);
            wasHolding = owner.isHoldingGun(this);
        }

        for (int i = bullets.size() - 1; i >= 0; i--) {
            Bullet bullet = bullets.get(i);
            bullet.tick();
        }

        if (this.gunReloadTimer < 0) {
            if (this.reloading) {
                this.reloading = false;
                finishReloading();
            }
        }

        if (canAimSneak) {
            if (owner.isAimedIn() && !owner.isHoldingGun(this)) {
                owner.checkAim();
            } else if ((owner.isSneaking() && !owner.isAimedIn() && owner.isHoldingGun(this)) || (!owner.isSneaking() && owner.isAimedIn())) {
                owner.checkAim();
            }
        }

        gunSounds();

        if (this.lastFired > 6) {
            this.heldDownTicks = 0;
        }
        if (((this.heldDownTicks >= 2) && (this.timer <= 0)) || ((this.firing) && (!this.reloading))) {
            if (this.roundsPerBurst > 1) {
                int bulletDelay = 2;
                if (this.ticks % bulletDelay == 0) {
                    this.bulletsShot += 1;
                    if (this.bulletsShot <= this.roundsPerBurst)
                        shoot();
                    else
                        finishShooting();
                }
            } else {
                shoot();
                finishShooting();
            }
        }

        if (this.reloading)
            this.firing = false;
    }

    public Gun copy() {
        Gun g = new Gun(this.gunName, this.ability);
        g.gunName = this.gunName;
        g.gunByte = this.gunByte;
        g.ammoByte = this.ammoByte;
        g.ammoAmtNeeded = this.ammoAmtNeeded;
        g.ammoType = this.ammoType;
        g.roundsPerBurst = this.roundsPerBurst;
        g.bulletsPerClick = this.bulletsPerClick;
        g.bulletSpeed = this.bulletSpeed;
        g.accuracy = this.accuracy;
        g.accuracy_aimed = this.accuracy_aimed;
        g.accuracy_crouched = this.accuracy_crouched;
        g.maxDistance = this.maxDistance;
        g.gunVolume = this.gunVolume;
        g.headshot_multiplier = this.headshot_multiplier;
        g.gunDamage = this.gunDamage;
        g.explodeRadius = this.explodeRadius;
        g.fireRadius = this.fireRadius;
        g.flashRadius = this.flashRadius;
        g.canHeadshot = this.canHeadshot;
        g.reloadTime = this.reloadTime;
        g.canAimLeft = this.canAimLeft;
        g.canAimRight = this.canAimRight;
        g.canClickLeft = this.canClickLeft;
        g.canClickRight = this.canClickRight;
        g.hasSmokeTrail = this.hasSmokeTrail;
        g.ignoreItemData = this.ignoreItemData;
        g.projType = this.projType;
        g.node = this.node;
        g.gunSound = this.gunSound;
        g.bulletDelayTime = this.bulletDelayTime;
        g.hasClip = this.hasClip;
        g.maxClipSize = this.maxClipSize;
        g.reloadGunOnDrop = this.reloadGunOnDrop;
        g.localGunSound = this.localGunSound;
        g.fileName = this.fileName;
        g.explosionDamage = this.explosionDamage;
        g.recoil = this.recoil;
        g.knockback = this.knockback;
        g.reloadType = this.reloadType;
        g.releaseTime = this.releaseTime;
        g.canGoPastMaxDistance = this.canGoPastMaxDistance;

        if (this.releaseEffect != null) {
            g.releaseEffect = this.releaseEffect.clone();
        }

        return g;
    }

    public void reloadGun() {
        this.reloading = true;
        this.gunReloadTimer = this.reloadTime;
        owner.updateForModUser(this, true);
    }

    private void gunSounds() {
        if (this.reloading) {
            int amtReload = this.reloadTime - this.gunReloadTimer;
            if (this.reloadType.equalsIgnoreCase("bolt")) {
                if (amtReload == 6)
                    this.owner.playSound(this.owner.getController().getLocation(), Sound.DOOR_OPEN, 2.0F, 1.5F);
                if (amtReload == this.reloadTime - 4)
                    this.owner.playSound(this.owner.getController().getLocation(), Sound.DOOR_CLOSE, 1.0F, 1.5F);
            } else if ((this.reloadType.equalsIgnoreCase("pump")) || (this.reloadType.equals("INDIVIDUAL_BULLET"))) {
                int rep = (this.reloadTime - 10) / this.maxClipSize;
                if ((amtReload >= 5) && (amtReload <= this.reloadTime - 5) && (amtReload % rep == 0)) {
                    this.owner.playSound(this.owner.getController().getLocation(), Sound.NOTE_STICKS, 1.0F, 1.0F);
                    this.owner.playSound(this.owner.getController().getLocation(), Sound.NOTE_SNARE_DRUM, 1.0F, 2.0F);
                }

                if (amtReload == this.reloadTime - 3)
                    this.owner.playSound(this.owner.getController().getLocation(), Sound.PISTON_EXTEND, 1.0F, 2.0F);
                if (amtReload == this.reloadTime - 1)
                    this.owner.playSound(this.owner.getController().getLocation(), Sound.PISTON_RETRACT, 1.0F, 2.0F);
            } else {
                if (amtReload == 6) {
                    this.owner.playSound(this.owner.getController().getLocation(), Sound.FIRE_IGNITE, 2.0F, 2.0F);
                    this.owner.playSound(this.owner.getController().getLocation(), Sound.DOOR_OPEN, 1.0F, 2.0F);
                }
                if (amtReload == this.reloadTime / 2)
                    this.owner.playSound(this.owner.getController().getLocation(), Sound.PISTON_RETRACT, 0.33F, 2.0F);
                if (amtReload == this.reloadTime - 4) {
                    this.owner.playSound(this.owner.getController().getLocation(), Sound.FIRE_IGNITE, 2.0F, 2.0F);
                    this.owner.playSound(this.owner.getController().getLocation(), Sound.DOOR_CLOSE, 1.0F, 2.0F);
                }
            }
        } else {
            if (this.reloadType.equalsIgnoreCase("pump")) {
                if (this.timer == 8)
                    this.owner.playSound(this.owner.getController().getLocation(), Sound.PISTON_EXTEND, 1.0F, 2.0F);
                if (this.timer == 6) {
                    this.owner.playSound(this.owner.getController().getLocation(), Sound.PISTON_RETRACT, 1.0F, 2.0F);
                }
            }
            if (this.reloadType.equalsIgnoreCase("bolt")) {
                if (this.timer == this.bulletDelayTime - 4)
                    this.owner.playSound(this.owner.getController().getLocation(), Sound.DOOR_OPEN, 2.0F, 1.25F);
                if (this.timer == 6)
                    this.owner.playSound(this.owner.getController().getLocation(), Sound.DOOR_CLOSE, 1.0F, 1.25F);
            }
        }
    }

    private void doRecoil(LivingEntity player) {
        if (this.recoil != 0.0D) {
            Location ploc = player.getLocation();
            double dir = -ploc.getYaw() - 90.0F;
            double pitch = -ploc.getPitch() - 180.0F;
            double xd = Math.cos(Math.toRadians(dir)) * Math.cos(Math.toRadians(pitch));
            double yd = Math.sin(Math.toRadians(pitch));
            double zd = -Math.sin(Math.toRadians(dir)) * Math.cos(Math.toRadians(pitch));
            Vector vec = new Vector(xd, yd, zd);
            vec.multiply(this.recoil / 2.0D).setY(0);
            player.setVelocity(player.getVelocity().add(vec));
        }
    }

    public void doKnockback(LivingEntity entity, Vector speed) {
        if (this.knockback > 0.0D) {
            speed.normalize().setY(0.6D).multiply(this.knockback / 4.0D);
            entity.setVelocity(speed.setY(0));
        }
    }

    public void setMaxAmmo(int amount) {
        this.maxAmmo = amount;
    }

    public int getMaxAmmo() {
        return maxAmmo;
    }

    public void finishReloading() {
        this.bulletsShot = 0;
        this.roundsFired = 0;
        this.changed = false;
        this.gunReloadTimer = 0;
        owner.updateForModUser(this, true);
    }

    private void finishShooting() {
        this.bulletsShot = 0;
        this.timer = this.bulletDelayTime;
        this.firing = false;
    }

    public String getName() {
        return this.gunName;
    }

    @SuppressWarnings("deprecation")
    public Material getAmmoMaterial() {
        int id = getAmmoType();
        Material mat = Material.getMaterial(id);
        if (mat != null) {
            return mat;
        }
        return null;
    }

    public int getAmmoType() {
        return this.ammoType;
    }

    public int getAmmoAmtNeeded() {
        return this.ammoAmtNeeded;
    }

    public double getExplodeRadius() {
        return this.explodeRadius;
    }

    public double getFireRadius() {
        return this.fireRadius;
    }

    public void setName(String val) {
        val = val.replace("&", "�");
        this.gunName = val;
    }

    public int getValueFromString(String str) {
        if (str.contains(":")) {
            String news = str.substring(0, str.indexOf(":"));
            return Integer.parseInt(news);
        }
        return Integer.parseInt(str);
    }

    public byte getByteDataFromString(String str) {
        if (str.contains(":")) {
            String news = str.substring(str.indexOf(":") + 1);
            return Byte.parseByte(news);
        }
        return -1;
    }

    public void setAmmoType(String val) {
        this.ammoType = getValueFromString(val);
        this.ammoByte = getByteDataFromString(val);
        if (this.ammoByte == -1)
            this.ammoByte = 0;
    }

    public void setAmmoAmountNeeded(int parseInt) {
        this.ammoAmtNeeded = parseInt;
    }

    public void setRoundsPerBurst(int parseInt) {
        this.roundsPerBurst = parseInt;
    }

    public void setBulletsPerClick(int parseInt) {
        this.bulletsPerClick = parseInt;
    }

    public void setBulletSpeed(double parseDouble) {
        this.bulletSpeed = parseDouble;
    }

    public void setAccuracy(double parseDouble) {
        this.accuracy = parseDouble;
    }

    public void setAccuracyAimed(double parseDouble) {
        this.accuracy_aimed = parseDouble;
    }

    public void setAccuracyCrouched(double parseDouble) {
        this.accuracy_crouched = parseDouble;
    }

    public void setExplodeRadius(double parseDouble) {
        this.explodeRadius = parseDouble;
    }

    public void setFireRadius(double parseDouble) {
        this.fireRadius = parseDouble;
    }

    public void setCanHeadshot(boolean parseBoolean) {
        this.canHeadshot = parseBoolean;
    }

    public void setCanClickLeft(boolean parseBoolean) {
        this.canClickLeft = parseBoolean;
    }

    public void setCanClickRight(boolean parseBoolean) {
        this.canClickRight = parseBoolean;
    }

    public void clear() {
        owner.clearForModUser();
        this.owner = null;
        for (int i = this.bullets.size() - 1; i >= 0; i--) {
            Bullet bullet = bullets.get(i);
            bullet.destroy();
        }
        bullets.clear();
    }

    public void setReloadTime(int parseInt) {
        this.reloadTime = parseInt;
    }

    public int getReloadTime() {
        return this.reloadTime;
    }

    public float getGunDamage() {
        return this.gunDamage;
    }

    public void setGunDamage(float f) {
        this.gunDamage = f;
    }

    public double getMaxDistance() {
        return this.maxDistance;
    }

    public void setMaxDistance(int i) {
        this.maxDistance = i;
    }

    public boolean canAimLeft() {
        return this.canAimLeft;
    }

    public boolean canAimRight() {
        return this.canAimRight;
    }

    public void setCanAimLeft(boolean parseBoolean) {
        this.canAimLeft = parseBoolean;
    }

    public void setCanAimRight(boolean parseBoolean) {
        this.canAimRight = parseBoolean;
    }

    public boolean canAimSneak() {
        return this.canAimSneak;
    }

    public void setCanAimSneak(boolean canAimSneak) {
        this.canAimSneak = canAimSneak;
    }

    public void setFlashRadius(double parseDouble) {
        this.flashRadius = parseDouble;
    }

    public double getFlashRadius() {
        return this.flashRadius;
    }

    public boolean canHeadShot() {
        return this.canHeadshot;
    }

    public boolean hasSmokeTrail() {
        return this.hasSmokeTrail;
    }

    public void setSmokeTrail(boolean b) {
        this.hasSmokeTrail = b;
    }

    public boolean isLocalGunSound() {
        return this.localGunSound;
    }

    public void setLocalGunSound(boolean b) {
        this.localGunSound = b;
    }

    public void setExplosionDamage(int i) {
        this.explosionDamage = i;
    }

    public int getExplosionDamage() {
        return this.explosionDamage;
    }

    public String getFilename() {
        return this.fileName;
    }

    public void setFilename(String string) {
        this.fileName = string;
    }

    public void setGunTypeByte(byte b) {
        this.gunByte = b;
    }

    public byte getGunTypeByte() {
        return this.gunByte;
    }

    public void setAmmoTypeByte(byte b) {
        this.ammoByte = b;
    }

    public byte getAmmoTypeByte() {
        return this.ammoByte;
    }

    public void setRecoil(double d) {
        this.recoil = d;
    }

    public double getRecoil() {
        return this.recoil;
    }

    public void setKnockback(double d) {
        this.knockback = d;
    }

    public double getKnockback() {
        return this.knockback;
    }

    public void addGunSounds(String val) {
        String[] sounds = val.split(",");
        Collections.addAll(this.gunSound, sounds);
    }

    public int getReleaseTime() {
        return this.releaseTime;
    }

    public void setReleaseTime(int v) {
        this.releaseTime = v;
    }

    public void setCanGoPastMaxDistance(boolean parseBoolean) {
        this.canGoPastMaxDistance = parseBoolean;
    }

    public boolean canGoPastMaxDistance() {
        return this.canGoPastMaxDistance;
    }

    public void setGunVolume(double parseDouble) {
        this.gunVolume = parseDouble;
    }

    public double getGunVolume() {
        return this.gunVolume;
    }

    public double getAccuracy() {
        return this.accuracy;
    }

    public double getAccuracy_aimed() {
        return this.accuracy_aimed;
    }

    public double getAccuracy_crouched() {
        return this.accuracy_crouched;
    }

    public GunEffect getReleaseEffect() {
        return this.releaseEffect;
    }

    public void setReleaseEffect(GunEffect releaseEffect) {
        this.releaseEffect = releaseEffect;
    }
}