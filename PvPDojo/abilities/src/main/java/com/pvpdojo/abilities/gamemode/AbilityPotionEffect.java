/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.gamemode;

import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.abilities.base.entities.AbilityEntity;

public class AbilityPotionEffect extends AbilityEffect {

    private int strength;
    private PotionEffectType potionType;
    
    public AbilityPotionEffect(AbilityEffectType type, PotionEffectType potionType, int duration, int strength) {
        super(type, duration);
        this.strength = strength;
        this.potionType = potionType;
    }

    @Override
    public void applyEffect(AbilityEntity entity) {
        entity.getEntity().addPotionEffect(new PotionEffect(potionType, getTickDuration(), strength));
    }

    @Override
    public void endEffect(AbilityEntity entity) {
        entity.getEntity().removePotionEffect(potionType);
    }
    

}
