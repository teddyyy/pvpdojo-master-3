/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.abilities.AbilityMetaData;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.bukkit.events.EntityFallEvent;

public class NukerAbility extends AbilityExecutor {

	@Override
	public String getExecutorName() {
		return "NukerAbility";
	}

	private static final String DOWN_BOOST = "nuker";
	private static final String UP_BOOST = "nuker_up";
	private static final String NUKED_ENTITIES = "get_nuked_boi";// very common from a me

	@Override
	public boolean activateAbility(Ability ability, Event event) {

		AbilityEntity activator = ability.getAbilityEntity();
		AbilityMetaData meta = ability.getMetaData();

		if (meta.hasData(UP_BOOST)) {
			boolean canNuke = true;
			if (activator.getEntity().isOnGround()) {
				canNuke = false;
				PvPDojo.schedule(() -> {
					if (activator.getEntity().isOnGround()) {
						doNuker(activator.getEntity());
					}
				}).createTask(1);
			}
			if (canNuke)
				activator.getEntity().setVelocity(
						activator.getEntity().getLocation().getDirection().normalize().multiply(3).setY(-2.5));
			meta.addData(DOWN_BOOST, null);

		} else if (!meta.hasData(UP_BOOST)) {

			Location loc = activator.getEntity().getLocation();
			Block block = loc.getBlock();
			boolean canNuke = true;

			for (int y = 0; y <= 3; y++) {// u can not nuke while there is 3 blocks above you
				block = block.getRelative(BlockFace.UP);
				if (block.getLocation().getBlock().getType() != Material.AIR) {
					canNuke = false;
					return false;
				}
			}
			if (canNuke) {
				if (meta.hasData(DOWN_BOOST))
					meta.removeData(DOWN_BOOST);
				
				activator.getEntity().setVelocity(new Vector(0.0D, 2.6D, 0.0D));
				meta.addData(DOWN_BOOST, null);
				meta.addData(UP_BOOST, null);
			}
		}

		return false;
	}

	@EventHandler
	public void onFall(EntityFallEvent e) {
		if (!(e.getEntity() instanceof LivingEntity) || !canAbilityExecute((LivingEntity) e.getEntity()))
			return;
		if (getAbility((LivingEntity) e.getEntity()).getMetaData().hasData(DOWN_BOOST))
			e.setCancelled(true);
		doNuker((LivingEntity) e.getEntity());

	}

	@EventHandler
	public void onNukedEntitiesFall(EntityFallEvent e) {
		if (e.getEntity() instanceof LivingEntity) {
			if (e.getEntity().hasMetadata(NUKED_ENTITIES)) {
				e.setCancelled(true);
				Player player = Bukkit
						.getPlayer(UUID.fromString(e.getEntity().getMetadata(NUKED_ENTITIES).get(0).asString()));
				e.getEntity().removeMetadata(NUKED_ENTITIES, PvPDojo.get());
				if (player == null || !player.isOnline()) {
					return;
				}
				Ability ability = getAbility(player);
				AbilityEntity abilityEntity = ability.getAbilityEntity();
				final float damage = ability.getAttribute("ability_damage");
				abilityEntity.abilityDamage(AbilityEntity.get(e.getEntity()), damage / 2, ability, DamageCause.FALL);
			}
		}
	}

	private void doNuker(LivingEntity entity) {
		Ability ability = getAbility(entity);
		AbilityMetaData meta = ability.getMetaData();

		if (meta.hasData(DOWN_BOOST)) {
			AbilityEntity activator = ability.getAbilityEntity();

			ability.activateAbility(false, null);

			entity.getWorld().createExplosion(entity.getLocation(), 0.0F);
			entity.getWorld().playEffect(entity.getLocation(), Effect.EXPLOSION_HUGE, 0);

			final float radius = ability.getAttribute("radius");
			final float damage = ability.getAttribute("ability_damage");

			for (AbilityEntity nearby : AbilityServer.get().getNearbyEntities(activator, radius)) {
				nearby.getEntity().setVelocity(new Vector(0.0D, 1.6D, 0.0D));
				activator.abilityDamage(nearby, damage, ability, DamageCause.ENTITY_EXPLOSION);
				nearby.getEntity().setMetadata(NUKED_ENTITIES,
						new FixedMetadataValue(PvPDojo.get(), entity.getUniqueId().toString()));
			}
			meta.removeData(DOWN_BOOST);
		}
		meta.removeData(UP_BOOST);
	}

	// we clean up
	@EventHandler
	public void onEntityDeath(EntityDeathEvent e) {
		if (e.getEntity().hasMetadata(NUKED_ENTITIES))
			e.getEntity().removeMetadata(NUKED_ENTITIES, PvPDojo.get());
	}

}
