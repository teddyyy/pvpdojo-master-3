/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import java.util.ArrayList;

import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Sheep;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.abilities.AbilityMetaData;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.util.bukkit.ParticleEffects;

public class HerdsmanAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "HerdsmanAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        final AbilityEntity activator = ability.getAbilityEntity();

        final float radius = ability.getAttribute("radius");
        final float damage = ability.getAttribute("damage");

        Location loc = activator.getEntity().getLocation();

        ArrayList<Sheep> list = new ArrayList<>();
        AbilityMetaData meta = ability.getMetaData();

        for (int i = 0; i < 360; i++) {

            i += 90;

            Sheep herds = (Sheep) activator.getEntity().getWorld().spawnEntity(loc, EntityType.SHEEP);

            list.add(herds);

            meta.addEntity(herds, 5, false);

            herds.setSheared(false);
            herds.setColor(DyeColor.RED);

            Location l = new Location(activator.getEntity().getWorld(), herds.getLocation().getX(), herds.getLocation().getY(), herds.getLocation().getZ(), i, 2F);
            l.setYaw(i);
            l.setPitch(1.5F);

            herds.teleport(l);

            herds.setVelocity(herds.getLocation().getDirection().normalize().multiply(0.5));

            new AbilityRunnable(ability) {

                int i = 0;

                @Override
                public void run() {

                    if (!herds.isDead()) {
                        i = i + 1;
                        herds.setVelocity(herds.getLocation().getDirection().normalize().multiply(0.5));

                        for (AbilityEntity nearby : AbilityServer.get().getNearbyEntities(activator, herds.getLocation(), radius, radius)) {

                            activator.abilityDamage(nearby, damage, ability, DamageCause.MAGIC);

                            ParticleEffects.LARGE_EXPLODE.sendToLocation(nearby.getEntity().getLocation(), 0, 0, 0, 0, 1);
                            nearby.getEntity().getWorld().playSound(nearby.getEntity().getLocation(), Sound.EXPLODE, 1, 1);

                            for (Sheep sheepList : list) {

                                sheepList.remove();

                            }

                            this.cancel();

                        }

                    }

                    if (activator.isDead() || i >= 75) {
                        herds.remove();
                        this.cancel();
                    }
                }
            }.createTimer(0, 20000);
        }

        return true;

    }

}
