package com.pvpdojo.abilities.abilities.passive;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.abilities.AbilityMetaData;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilities.gamemode.ManaSilenceEffect;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.PlayerUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class ChiBlockerAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "ChiBlockerAbility";
    }

    private static final String KEY = "Chi-Blocker";
    private static final float STEAL_MANA = 100.0F;

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        return true;
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {

        if (e.isCancelled()) {
            return;
        }

        if (e.getDamager() instanceof Player && e.getEntity() instanceof Player) {

            Player player = (Player) e.getDamager();
            Player entity = (Player) e.getEntity();

            if (!AbilityPlayer.get(player).canTarget(AbilityPlayer.get(entity))) {
                return;
            }

            if (!PlayerUtil.isRealHit(player)) {
                return;
            }

            if (canAbilityExecute(player)) {
                Ability ability = getAbility(player);
                AbilityMetaData data = ability.getMetaData();

                AbilityPlayer abilityPlayer = AbilityPlayer.get(player);
                AbilityPlayer abilityEntity = AbilityPlayer.get(entity);

                float duration = ability.getAttribute("duration");
                float steal = ability.getAttribute("steal");

                if (!PlayerUtil.isCrit(player)) {
                    return;
                }

                if (ability.getCooldown().isCoolingDown()) {
                    return;
                }

                if (!data.hasData(KEY)) {
                    data.addData(KEY, 1);
                } else {
                    data.addData(KEY, (int) data.getData(KEY) + 1);
                }
                int hits = data.getData(KEY);

                if (hits >= 2) {
                    data.removeData(KEY);
                    abilityEntity.applyEffect(new ManaSilenceEffect((int) (20 * duration)));
                    ability.activateAbility(e);

                    if (steal > 0) {
                        abilityEntity.setMana(abilityEntity.getMana() - STEAL_MANA);
                        abilityPlayer.setMana(abilityEntity.getMana() + STEAL_MANA);
                    }

                    player.sendMessage(PvPDojo.PREFIX + CC.RED + "You blocked " + entity.getNick() + "'s chi and disabled his mana regeneration");
                    entity.sendMessage(PvPDojo.PREFIX + CC.RED + "Your mana regeneration was disabled by a Chi-Blocker");
                }

            } else if (canAbilityExecute(entity)) {
                if (getAbility(entity).getMetaData().hasData(KEY)) {
                    getAbility(entity).getMetaData().removeData(KEY);
                }
            }

        }

    }

}
