/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.lib;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

import com.pvpdojo.DeveloperSettings;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.bukkit.events.DojoEvent;
import com.pvpdojo.bukkit.events.DojoProjectileHitEntityEvent;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.util.DojoRunnable;
import com.pvpdojo.util.IntSet;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.CC;

public class ProjectileEntity {

	private ProjectileDetection detection;
	private ProjectileLoop loop;
	private ProjectileDetectionCheck check;
	private ProjectileHit onHit;
	public AbilityEntity activator;
	private boolean collateral;
	private boolean disableKnockback = false;
	private float radius;
	private float damage;
	private IntSet hit = new IntSet();
	private static final int REFLECTOR_ABILITY_ID = 175;// do not change this unless if it was changed in Reflector.txt

	static {
		DojoProjectileHitEntityEvent event = new DojoProjectileHitEntityEvent() {

			@Override
			public void execute() {
				Entity projectile = this.projectile;
				LivingEntity v = this.entityHit;
				if (projectile.hasMetadata("projectile_entity")) {
					ProjectileEntity projectileEntity = (ProjectileEntity) projectile.getMetadata("projectile_entity")
							.get(0).value();
					projectile.removeMetadata("projectile_entity", PvPDojo.get());
					event.setDamage(projectileEntity.getDamage() / DeveloperSettings.HEALTH_CONVERTER);

					AbilityEntity victim = AbilityServer.get().getEntity(v);
					if (projectileEntity.check.canCollide(projectileEntity.activator, victim)) {
						projectileEntity.detection.onCollision(victim);
						if (projectileEntity.disableKnockback) {
							this.setCancelled(true);
						}
					} else {
						this.setCancelled(true);
					}
				}
			}
		};
		DojoEvent.register(event);
	}

	public ProjectileEntity(AbilityEntity activator, boolean collateral, float radius) {
		this(activator);
		this.collateral = collateral;
		this.radius = radius;
	}

	public ProjectileEntity(AbilityEntity activator) {
		this.activator = activator;
		this.loop = loc -> false;
		this.onHit = loc -> activator.getEntity().getLocation();//
		this.check = (activator1, target) -> {
			if (!activator1.canTarget(target)) {
				return false;
			}
			return !activator1.equals(target);
		};
	}

	public void disableKnockback() {
		this.disableKnockback = true;
	}

	/*private DojoRunnable blockCollisionRunnable;

	public void startBlockCollision(final Location loc, int delayPerLoop, final int blocksPerLoop, int distance) {
		ArrayList<Block> b = BlockUtil.getBlockIterator(loc, 0, distance, false);
		final Iterator<Block> itr = b.iterator();

		blockCollisionRunnable = PvPDojo.schedule(() -> {
			for (int i = 0; i < blocksPerLoop; i++) {
				if (itr.hasNext()) {
					Block block = itr.next();
					if (block == null) {
						blockCollisionRunnable.cancel();
						return;
					}
					if (block.getType().isSolid()) {
						blockCollisionRunnable.cancel();
						return;
					}
					if (loop.onLoop(block.getLocation())) {
						blockCollisionRunnable.cancel();
						return;
					}
					if (checkCollision(block.getLocation())) {
						blockCollisionRunnable.cancel();
						return;
					}

				} else {
					blockCollisionRunnable.cancel();
				}
			}
		});
		blockCollisionRunnable.createTimer(delayPerLoop, distance);
	}
*/
	public void startBlockCollision(final Location loc, int delay_per_loop, final int blocks_per_loop, int distance) {

		new BukkitRunnable() {
			ArrayList<Block> b = BlockUtil.getBlockIterator(loc, 0, distance, false);
			Iterator<Block> itr = b.iterator();
			boolean reflected = false;
			int dist = distance;
			int x = 0;

			public void run() {
				if (++x > distance)
					this.cancel();
				for (int i = 0; i < blocks_per_loop; i++) {
					if (itr.hasNext()) {
						Block block = itr.next();
						if (block == null) {
							this.cancel();
							return;
						}
						if (block.getType().isSolid()) {
							onHit.onHit(block.getLocation());
							this.cancel();
							return;
						}
						if (loop.onLoop(block.getLocation())) {
							this.cancel();
							return;
						}

						if (!reflected)
							dist--;

						if (checkCollision(block.getLocation(), true) != null) {
							for (AbilityEntity ents : checkCollision(block.getLocation(), true)) {
								if (isReflector(ents) && canUseReflector(ents)) {
									if (!reflected) {
										b = BlockUtil.getBlockIterator(ents.getEntity().getEyeLocation(), 0, dist,
												false);
										itr = b.iterator();
										activator.sendMessage(MessageKeys.REFLECTOR_REFLECTION);
										activator = ents;
										reflected = true;
										useReflector(ents);
									}
								}
							}
						} else if (checkCollision(block.getLocation(), false) != null) {
							this.cancel();
							return;
						}
					} else
						this.cancel();
				}
			}

		}.runTaskTimer(PvPDojo.get(), 0, delay_per_loop);
	}

	private DojoRunnable itemCollisionRunnable;

	public void startItemCollision(Item entity, int delayPerLoop, int loops, final boolean checkGround) {
		entity.setPickupDelay(10000);
		itemCollisionRunnable = new DojoRunnable(() -> {
			if (loop.onLoop(entity.getLocation())) {
				itemCollisionRunnable.cancel();
				return;
			}
			if (checkCollision(entity.getLocation(), false) == null) {
				onHit.onHit(entity.getLocation());
				itemCollisionRunnable.cancel();
				return;
			}
			if (checkGround) {
				if (entity.isOnGround()) {
					onHit.onHit(entity.getLocation());
					itemCollisionRunnable.cancel();
				}
			}
		}) {
			@Override
			public void cancel() {
				entity.removeMetadata("projectile_entity", PvPDojo.get());
				entity.remove();
				super.cancel();
			}
		};
		itemCollisionRunnable.createTimer(delayPerLoop, loops);
	}

	public void startProjectileCollision(Entity entity) {
		entity.setMetadata("projectile_entity", new FixedMetadataValue(PvPDojo.get(), this));
	}

	/*private boolean checkCollision(Location location) {
		List<AbilityEntity> nearby = AbilityServer.get().getNearbyEntities(activator, location, radius + 0.3, 0.5);
		for (AbilityEntity target : nearby) {
			if (hit.contains(target.getEntity().getEntityId())) {
				continue;
			}
			if (check.canCollide(activator, target)) {
				if (detection != null) {
					detection.onCollision(target);
				}
				hit.add(target.getEntity().getEntityId());
				if (!collateral) {
					return true;
				}
			}
		}
		return false;
	}
*/
	private List<AbilityEntity> checkCollision(Location location, boolean canReflect) {
		List<AbilityEntity> nearby = AbilityServer.get().getNearbyEntities(activator, location, radius + 0.3, 0.5);
		for (AbilityEntity target : nearby) {
			if (hit.contains(target.getEntity().getEntityId()))
				continue;

			if (check.canCollide(activator, target)) {
				if (canReflect) {
					if (isReflector(target)) {
						if(canUseReflector(target)) {
							break;
						} else {
							target.sendMessage(MessageKeys.NOT_ENOUGH_MANA, "{mana}", "" + (reflectorMana(target) - target.getMana()));
						}
					}
				}

				if (detection != null) {
					detection.onCollision(target);
				}
				hit.add(target.getEntity().getEntityId());

				if (!collateral)
					return null;

			}
		}
		return nearby;
	}

	public void setProjectileLoop(ProjectileLoop loop) {
		this.loop = loop;
	}

	public void setProjectileHit(ProjectileHit hit) {
		this.onHit = hit;
	}

	public void setProjectileDetection(ProjectileDetection detection) {
		this.detection = detection;
	}

	public void setProjectileDetectionCheck(ProjectileDetectionCheck check) {
		this.check = check;
	}

	public float getDamage() {
		return damage;
	}

	public void setDamage(float damage) {
		this.damage = damage;
	}

	private boolean isReflector(AbilityEntity entity) {
		return entity.hasAbility(AbilityLoader.getAbilityData(REFLECTOR_ABILITY_ID).getName());
	}
	private float reflectorMana(AbilityEntity entity) {
		return entity.getAbilityFromExecutor(AbilityLoader.getAbilityData(REFLECTOR_ABILITY_ID).
				getAbilityExecutor()).getAttribute("mana_use");
	}
	private boolean canUseReflector(AbilityEntity entity) {//for Mana
		return entity.getMana() > reflectorMana(entity);
	}
	private void useReflector(AbilityEntity entity) {
		entity.getAbilityFromExecutor(AbilityLoader.getAbilityData(REFLECTOR_ABILITY_ID).getAbilityExecutor()).activateAbility(null);
	}
}
