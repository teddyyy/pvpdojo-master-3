package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Item;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.util.bukkit.ParticleEffects;

public class WormHoleAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "WormHoleAbility";
    }

    @Override // duration damage radius
    public boolean activateAbility(Ability ability, Event event) {
        AbilityEntity activator = ability.getAbilityEntity();
        final float duration = ability.getAttribute("duration");
        final float radius = ability.getAttribute("radius");
        final float damage = ability.getAttribute("ability_damage");
        Item item = activator.getEntity().getWorld().dropItem(activator.getEntity().getEyeLocation(),
                new ItemStack(Material.EYE_OF_ENDER));
        item.setPickupDelay(20000);
        item.setVelocity(activator.getEntity().getEyeLocation().getDirection().normalize().multiply(0.8f));

        new AbilityRunnable(ability) {

            double timer = 20 * duration; // 20*7/2
            boolean soundEffect = false;

            public void run() {
                if (item.isOnGround() || item.isDead()) {
                    timer--;
                    item.remove();

                    if (!soundEffect) {
                        item.getWorld().playSound(item.getLocation(), Sound.WITHER_HURT, 1, 1);
                        item.getWorld().strikeLightningEffect(item.getLocation());
                        soundEffect = true;
                    }
                    for (AbilityEntity entity : AbilityServer.get().getNearbyEntities(activator, item.getLocation(), radius, radius)) {
                        Location lc = entity.getEntity().getLocation();
                        Location to = item.getLocation();

                        double speed = 0.08;

                        double time = to.distance(lc);
                        double vx = (speed + 0.03 * time) * (to.getX() - lc.getX()) / time;
                        double vy = (speed + 0.03 * time) * (to.getY() - lc.getY()) / time;
                        double vz = (speed + 0.03 * time) * (to.getZ() - lc.getZ()) / time;

                        Vector v = item.getVelocity();
                        v.setX(vx);
                        v.setY(vy);
                        v.setZ(vz);
                        entity.getEntity().setVelocity(v);

                        activator.abilityDamage(entity, damage, ability, DamageCause.VOID);

                    }
                }

                if (timer <= 0) {
                    this.cancel();
                }
            }
        }.createTimer(0, 1, -1);
        new AbilityRunnable(ability) {

            double timer = 20 * duration / 10;

            public void run() {
                if (item.isOnGround() || item.isDead()) {
                    timer--;
                    blackHoleEffect(item.getLocation(), radius);
                }
                if (timer <= 0) {
                    this.cancel();
                }
            }
        }.createTimer(10, -1);

        return true;
    }

    public void blackHoleEffect(Location location, double radius) {
        // TAKEN FROM EFFECTLIB
        // https://github.com/Slikey/EffectLib/blob/master/src/main/java/de/slikey/effectlib/effect/HelixEffect.java
        double rotation = Math.PI / 4;
        for (int i = 1; i <= 8; i++) {
            for (int j = 1; j <= 50; j++) {
                float ratio = (float) j / 50;
                double angle = 10 * ratio * 2 * Math.PI / 8 + (2 * Math.PI * i / 8) + rotation;
                double x = Math.cos(angle) * ratio * radius;
                double z = Math.sin(angle) * ratio * radius;
                location.add(x, 0.5, z);
                ParticleEffects.MAGIC_CRITICAL_HIT.sendToLocation(location, 0, 0, 0, 0, 1);
                location.subtract(x, 0.5, z);
            }
        }
    }
}
