package com.pvpdojo.abilities.abilities.active;

import java.util.HashMap;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftFireball;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.bukkit.util.TempBlock;
import com.pvpdojo.util.bukkit.BlockUtil;

import net.minecraft.server.v1_7_R4.EntityFireball;

public class MeteorAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "MeteorAbility";
    }

    public HashMap<Fireball, AbilityEntity> hellsFireball = new HashMap<Fireball, AbilityEntity>();

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        AbilityEntity activator = ability.getAbilityEntity();

        float waves = ability.getAttribute("waves");
        Location loc = activator.getEntity().getLocation();

        int cy = loc.getBlockY() + Math.min(50, BlockUtil.getFirstBlockUp(loc, null).getBlock().getRelative(BlockFace.DOWN).getY());

        new AbilityRunnable(ability) {

            @Override
            public void run() {
                int r = 10;
                int cx = loc.getBlockX();
                int cz = loc.getBlockZ();
                int rSquared = r * r;
                for (int x = cx - r; x <= cx + r; x++) {
                    for (int z = cz - r; z <= cz + r; z++) {
                        if ((cx - x) * (cx - x) + (cz - z) * (cz - z) <= rSquared) {
                            final Location l = new Location(loc.getWorld(), x, cy, z);
                            new AbilityRunnable(ability) {

                                double countDown = 40;

                                public void run() {
                                    if (activator.getEntity().isDead() || countDown <= 0) {
                                        this.cancel();
                                    }
                                    countDown--;
                                    if (waves == 2 ? countDown == 24 || countDown == 4
                                            : countDown == 36 || countDown == 20 || countDown == 4) {
                                        if (new Random().nextInt(100) <= 10) {
                                            Fireball fireball = (Fireball) l.getWorld().spawnEntity(l,
                                                    EntityType.FIREBALL);
                                            hellsFireball.put(fireball, activator);

                                            new AbilityRunnable(ability) {

                                                int timer = 0;

                                                public void run() {

                                                    try {
                                                        EntityFireball entityFireball = ((CraftFireball) fireball)
                                                                .getHandle();
                                                        if (entityFireball.isAlive()) {
                                                            if (hellsFireball.get(fireball) == activator) {
                                                                entityFireball.motX = 1.0E-4D;
                                                                entityFireball.motZ = 1.0E-4D;
                                                                entityFireball.motY = -1.5D;// -1.0D
                                                            } else {
                                                                fireball.remove();
                                                                hellsFireball.remove(fireball);
                                                                this.cancel();
                                                            }
                                                        } else {
                                                            fireball.remove();
                                                            hellsFireball.remove(fireball);
                                                            this.cancel();
                                                        }
                                                    } catch (Exception e) {
                                                        fireball.remove();
                                                        hellsFireball.remove(fireball);
                                                        this.cancel();
                                                    }
                                                    timer++;
                                                    if (timer >= 150 || activator.getEntity().isDead()) {
                                                        fireball.remove();
                                                        this.cancel();
                                                    }
                                                }

                                            }.createTimer(0, -1);
                                        }
                                    }
                                }
                            }.createTimer(0, -1);
                        }
                    }
                }
            }

        }.createTask(0);

        return true;
    }

    @EventHandler
    public void onProjectileHit(ProjectileHitEvent e) {
        if (e.getEntity() instanceof Fireball) {
            Fireball fireball = (Fireball) e.getEntity();
            if (hellsFireball.containsKey(fireball)) {
                AbilityEntity player = hellsFireball.get(fireball);
                Ability ability = getAbility(player.getEntity());
                if (canAbilityExecute(player.getEntity())) {
                    float damage = ability.getAttribute("ability_damage");
                    for (AbilityEntity l : AbilityServer.get().getNearbyEntities(player, fireball.getLocation(), 3, 3)) {

                        player.abilityDamage(l, damage, ability, DamageCause.BLOCK_EXPLOSION);
                        l.getEntity().setFireTicks(20 * 5);
                        l.getEntity().setVelocity(l.getEntity().getLocation().getDirection().normalize().multiply(-1.5).setY(0.5));
                        l.getEntity().setFallDistance(-50F);

                    }

                    TempBlock.createTempBlock(fireball.getLocation(), Material.FIRE, (byte) 0, 7);
                }
            }
        }
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player && e.getDamager() instanceof Fireball) {
            Player player = (Player) e.getEntity();
            Fireball damager = (Fireball) e.getDamager();
            AbilityEntity activator = AbilityServer.get().getEntity(player);
            if (canAbilityExecute(player)) {
                if (hellsFireball.get(damager) != null && hellsFireball.get(damager) == activator) {
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onExplosion(EntityExplodeEvent e) {
        if (e.getEntity() instanceof Fireball) {
            if (hellsFireball.containsKey(e.getEntity())) {
                e.blockList().clear();
            }
        }
    }

}
