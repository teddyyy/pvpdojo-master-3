/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.ProjectileEntity;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.WitherEffect;

public class BlackBeamAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "BlackBeamAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        AbilityEntity activator = ability.getAbilityEntity();

        final float damage = ability.getAttribute("damage");
        final float duration = ability.getAttribute("wither_duration");

        ProjectileEntity projectile = new ProjectileEntity(activator, false, 1.5f);
        activator.getEntity().getWorld().playSound(activator.getEntity().getLocation(), Sound.EXPLODE, 2.0F, -1.0F);

        projectile.setProjectileLoop(loc -> {
            Material type = Material.COAL_BLOCK;
            loc.getWorld().playEffect(loc, Effect.STEP_SOUND, type);
            return false;
        });

        projectile.setProjectileDetection(target -> {

            activator.abilityDamage(target, damage, ability, DamageCause.MAGIC);
            target.applyEffect(new WitherEffect((int) duration * 20, 1));

        });
        projectile.startBlockCollision(activator.getEntity().getEyeLocation(), 1, 2, 40);

        return true;
    }
}