/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerInteractEvent;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.bukkit.util.TempBlock;
import com.pvpdojo.lang.MessageKeys;

public class PyroAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "PyroAbility";
    }

    @Override // radius -> 1x1blocks -> 3x3blocks
    public boolean activateAbility(Ability ability, Event event) {
        if (!(event instanceof PlayerInteractEvent)) {
            ability.getAbilityEntity().sendMessage(MessageKeys.GENERIC, "{text}", "Cannot use with sneak activation");
            return false;
        }

        PlayerInteractEvent interactEvent = (PlayerInteractEvent) event;

        if (interactEvent.getClickedBlock() != null) {
            Location loc = interactEvent.getClickedBlock().getLocation();
            float radius = ability.getAttribute("radius");

            if (radius == 1) {
                TempBlock.createTempBlock(loc.add(0, 1, 0), Material.FIRE, (byte) 0, 5);
            } else {
                for (float x = -1; x <= 1; x++) {
                    for (float y = -1; y <= 1; y++) {
                        for (float z = -1; z <= 1; z++) {
                            Location l = loc.clone().add(x + 0.5, y, z + 0.5);
                            TempBlock.createTempBlock(l, Material.FIRE, (byte) 0, 5);
                        }
                    }
                }
            }
        }
        return true;
    }

}
