/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.listeners.events;

import org.bukkit.event.Cancellable;

import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;

public class AbilityHitEvent extends AbilityEvent implements Cancellable {
    public AbilityEntity activator;
    public Ability ability;
    private boolean cancelled = false;

    public AbilityHitEvent(AbilityEntity activator, AbilityEntity victim, Ability ability) {
        super(victim);
        this.activator = activator;
        this.ability = ability;
    }

    public AbilityEntity getActivator() {
        return activator;
    }

    public Ability getAbility() {
        return ability;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }
}
