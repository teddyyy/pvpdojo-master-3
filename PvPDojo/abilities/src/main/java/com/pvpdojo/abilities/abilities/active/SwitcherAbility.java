/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Snowball;
import org.bukkit.event.Event;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.ProjectileEntity;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.lang.MessageKeys;

public class SwitcherAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "SwitcherAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        final AbilityEntity activator = ability.getAbilityEntity();
        Snowball snowball = activator.getEntity().launchProjectile(Snowball.class);
        snowball.setVelocity(snowball.getVelocity().multiply(ability.getAttribute("projectile_speed")));
        ProjectileEntity projectileEntity = new ProjectileEntity(activator);
        projectileEntity.startProjectileCollision(snowball);
        projectileEntity.setProjectileDetection(target -> {
            LivingEntity entityHit = target.getEntity();
            Location targetLoc = entityHit.getLocation();
            entityHit.teleport(activator.getEntity().getLocation());
            activator.getEntity().teleport(targetLoc);
            target.sendMessage(MessageKeys.SWITCHER_SWITCH);
        });
        return true;
    }

}
