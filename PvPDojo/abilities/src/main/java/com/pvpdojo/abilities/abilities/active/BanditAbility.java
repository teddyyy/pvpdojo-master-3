/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;

public class BanditAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "BanditAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        AbilityEntity activator = ability.getAbilityEntity();

        final float radius = ability.getAttribute("radius");
        final float damage = ability.getAttribute("ability_damage");
        final float duration = ability.getAttribute("duration");
        new AbilityRunnable(ability) {

            double i = 0;
            double insideRadius = radius - 2;

            public void run() {

                i++;

                Location loc = activator.getEntity().getLocation();

                double x = Math.sin(i * 0.2) * radius;
                double y = 0;
                double z = Math.cos(i * 0.2) * radius;

                loc.add(x, y, z);
                loc.getWorld().playEffect(loc, Effect.STEP_SOUND, Material.QUARTZ_BLOCK);
                loc.subtract(x, y, z);


                x = Math.sin(i * -0.2) * insideRadius;
                y = 0;
                z = Math.cos(i * -0.2) * insideRadius;
                loc.add(x, y, z);
                loc.getWorld().playEffect(loc, Effect.STEP_SOUND, Material.COAL_BLOCK);

                for (AbilityEntity near : AbilityServer.get().getNearbyEntities(activator, radius, 2)) {
                    if (i % 10 == 0) {
                        activator.abilityDamage(near, damage, ability, DamageCause.MAGIC);
                    }
                }


            }
        }.createTimer(1, (int) (duration * 20));

        return true;
    }

}
