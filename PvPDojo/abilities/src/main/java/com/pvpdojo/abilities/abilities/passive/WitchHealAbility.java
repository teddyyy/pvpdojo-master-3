package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.listeners.events.DojoSelectKitEvent;

public class WitchHealAbility extends AbilityExecutor {

	@Override // copy pasta witch damage
	public String getExecutorName() {
		return "WitchHealAbility";
	}

	@Override
	public boolean activateAbility(final Ability ability, Event event) {
		return true;
	}

	@EventHandler
	public void onSelect(DojoSelectKitEvent e) {
		AbilityEntity activator = e.getEntity();
		LivingEntity living = activator.getEntity();
		if (activator.getAbilityFromExecutor(getExecutorName()) == null)
			return;
		Ability ability = getAbility(living);

		final float interval = ability.getAttribute("interval");
		new AbilityRunnable(ability) {
			public void run() {
				if (!canAbilityExecute(living))
					return;
				if (AbilityEntity.get(living).getMana() < 150) {
					return;
				}

				Location ploc = living.getEyeLocation();
				spawnPotion(new Location(living.getWorld(), ploc.getX() + 2, ploc.getY() + 2, ploc.getZ()), ability);
				spawnPotion(new Location(living.getWorld(), ploc.getX() + 2, ploc.getY() + 2, ploc.getZ() + 2),
						ability);
				spawnPotion(new Location(living.getWorld(), ploc.getX(), ploc.getY() + 2, ploc.getZ() + 2), ability);
				spawnPotion(new Location(living.getWorld(), ploc.getX() - 2, ploc.getY() + 2, ploc.getZ()), ability);
				spawnPotion(new Location(living.getWorld(), ploc.getX() - 2, ploc.getY() + 2, ploc.getZ() - 2),
						ability);
				spawnPotion(new Location(living.getWorld(), ploc.getX(), ploc.getY() + 2, ploc.getZ() - 2), ability);
			}
		}.createTimer(20, (int) interval * 20, -1);
	}

	@EventHandler
	public void onPotionSplash(PotionSplashEvent e) {
		ThrownPotion potion = e.getPotion();
		if (potion.getShooter() == null)
			return;
		LivingEntity living = (LivingEntity) potion.getShooter();
		if (living instanceof Player) {
			if (!((Player) living).isOnline()) {
				return;
			}
		}
		AbilityEntity activator = AbilityServer.get().getEntity(living);
		if (canAbilityExecute(living)) {
			Ability ability = getAbility(living);
			if (ability.getMetaData().hasEntity(potion)) {
				float heal = ability.getAttribute("heal");
				float regen = ability.getAttribute("regen");

				if (potion.getLocation().distanceSquared(activator.getEntity().getLocation()) <= 2.5 * 2.5) {
					activator.heal(heal);
					if (regen > 0)
						activator.getEntity()
								.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * 5, 1, false));

					if (AbilityEntity.get(living).getMana() > 150) {
						activator.setMana(activator.getMana() - 150);
					}

				}
				e.setCancelled(true);
			}
			e.getAffectedEntities().forEach(entity -> e.setIntensity(entity, 0D));
		}

	}

	private void spawnPotion(Location location, Ability ability) {
		ThrownPotion potion = (ThrownPotion) location.getWorld().spawnEntity(location, EntityType.SPLASH_POTION);
		potion.setShooter(ability.getAbilityEntity().getEntity());
		ability.getMetaData().addEntity(potion, 30, true);
		Potion pot = new Potion(PotionType.INSTANT_HEAL, 1);
		pot.setSplash(true);
		potion.setItem(pot.toItemStack(1));
	}
}
