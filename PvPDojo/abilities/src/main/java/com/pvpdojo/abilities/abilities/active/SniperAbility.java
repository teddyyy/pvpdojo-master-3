/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.guns.Gun;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.listeners.events.DojoSelectKitEvent;

public class SniperAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "SniperAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        AbilityEntity activator = ability.getAbilityEntity();
        return activator.getGunController().clickGunAbility(this.getExecutorName());
    }

    @SuppressWarnings("deprecation")
    @EventHandler
    public void onSelect(DojoSelectKitEvent e) {
        AbilityEntity activator = e.getEntity();
        LivingEntity living = activator.getEntity();
        if (activator.getAbilityFromExecutor(getExecutorName()) == null)
            return;
        Ability ability = getAbility(living);
        Gun gun = new Gun("SniperAbility", ability);
        gun.setName("SniperAbility");
        gun.setAmmoType("" + Material.SNOW_BALL.getId());
        gun.setAmmoAmountNeeded(1);
        gun.bulletDelayTime = 10;
        gun.reloadType = "INDIVIDUAL_BULLET";
        gun.setRoundsPerBurst(1);
        gun.setBulletsPerClick(1);
        gun.setGunDamage(ability.getAttribute("ability_damage"));
        gun.setHeadShotMultiplier(ability.getAttribute("headshot_damage"));
        gun.setMaxDistance(65);
        gun.setBulletSpeed(4);
        gun.setAccuracy(.05);
        gun.setAccuracyAimed(.00);
        gun.setAccuracyCrouched(.00);
        gun.setRecoil(2);
        gun.setKnockback(1);
        gun.setCanAimLeft(true);
        gun.setCanAimRight(false);
        gun.setCanAimSneak(true);
        gun.setCanHeadshot(true);
        gun.setCanClickLeft(false);
        gun.setCanClickRight(true);
        gun.setExplodeRadius(0);
        gun.gunSound.add("irongolem_hit");
        gun.gunSound.add("zombie_wood");
        gun.gunSound.add("explode");
        gun.setSmokeTrail(false);
        gun.setGunVolume(1);
        gun.hasClip = true;
        gun.setLocalGunSound(false);
        gun.maxClipSize = (int) ability.getAttribute("clip_size");
        gun.ammoLeft = (int) ability.getAttribute("total_clips") * gun.maxClipSize;
        gun.ammoReset = gun.ammoLeft;
        gun.setReloadTime((int) (ability.getAttribute("reload_time") * 20));

        activator.selectGunAbility(gun, ability);
    }

}