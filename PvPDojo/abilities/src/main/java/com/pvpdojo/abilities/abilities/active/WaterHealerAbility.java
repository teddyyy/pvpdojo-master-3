package com.pvpdojo.abilities.abilities.active;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class WaterHealerAbility extends AbilityExecutor {
    @Override
    public String getExecutorName() {
        return "WaterHealerAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        if (event instanceof PlayerInteractEvent) {
            applyEffect(((PlayerInteractEvent) event).getPlayer(), ability);

        } else if (event instanceof PlayerInteractEntityEvent) {
            if (((PlayerInteractEntityEvent) event).getRightClicked() instanceof LivingEntity) {
                applyEffect((LivingEntity) ((PlayerInteractEntityEvent) event).getRightClicked(), ability);
            }
        }

        return true;
    }

    private void applyEffect(LivingEntity entity, Ability ability) {
        entity.setHealth(Math.min(entity.getHealth() + ability.getAttribute("heal"), entity.getMaxHealth()));
        entity.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * 5, 1));
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {

        Player player = e.getPlayer();

        if (canAbilityExecute(player)) {
            if (player.getItemInHand().isSimilar(getAbility(player).getClickItem())) {
                getAbility(player).activateAbility(e);
            }
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEntityEvent e) {

        Player player = e.getPlayer();

        if (canAbilityExecute(player)) {
            if (player.getItemInHand().isSimilar(getAbility(player).getClickItem())) {
                getAbility(player).activateAbility(e);
            }
        }
    }
}
