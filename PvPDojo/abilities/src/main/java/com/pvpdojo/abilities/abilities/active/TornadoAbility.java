package com.pvpdojo.abilities.abilities.active;

import org.bukkit.block.Block;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.effects.TornadoEffect;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.util.bukkit.BlockUtil;

public class TornadoAbility extends AbilityExecutor {

	@Override
	public String getExecutorName() {
		return "TornadoAbility";
	}


	@Override
	public boolean activateAbility(Ability ability, Event event) {
		AbilityEntity activator = ability.getAbilityEntity();
		float duration = ability.getAttribute("duration");

		Block block = BlockUtil.getTargetBlock(10, ability.getAbilityEntity().getEntity());

		if (block != null) {
			LivingEntity entity = activator.getEntity();
			TornadoEffect.spawnTornado(ability, entity.getLocation(), entity.getEyeLocation().getDirection().multiply(2).setY(0), 2, 150, (long) duration);
			return true;
		}

		return false;
	}

}
