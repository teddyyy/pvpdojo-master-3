/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.listeners;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.listeners.DojoListener;

public class BotListener implements DojoListener {

    @EventHandler
    public void onBotDamage(EntityDamageByEntityEvent e) {

        if (!(e.getEntity() instanceof Zombie) || !(e.getDamager() instanceof LivingEntity))
            return;

        if ((((LivingEntity) e.getEntity()).getNoDamageTicks() > ((LivingEntity) e.getDamager()).getMaximumNoDamageTicks() / 2D)) {
            e.setCancelled(true);
            return;
        }

        PvPDojo.schedule(() -> e.getEntity().setVelocity(e.getEntity().getVelocity().multiply(0.8))).nextTick();

    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onEntityDeathEvent(EntityDeathEvent e) {
        if (e.getEntity() instanceof Zombie) {
            e.getDrops().clear();
        }
        e.setDroppedExp(0);
    }

    @EventHandler
    public void onCombust(EntityCombustEvent e) {
        if (e.getEntity() instanceof Zombie) {
            e.setCancelled(true);
        }
    }

}
