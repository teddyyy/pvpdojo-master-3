package com.pvpdojo.abilities.abilities.active;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.effects.ParticleEffectRotation;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.util.bukkit.ParticleEffects;

public class RayGunAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "RayGunAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        AbilityEntity activator = ability.getAbilityEntity();
        // - cooldown
        // - damage
        // - radius of the damage
        final float damage = ability.getAttribute("damage");
        final float radius = ability.getAttribute("radius");

        new AbilityRunnable(ability) {

            double t = 0;
            Location loc = activator.getEntity().getLocation();

            public void run() {
                t += Math.PI / 10;
                double x, y, z;
                for (double theta = 0; theta <= 2 * Math.PI; theta = theta + Math.PI / 12.0) {
                    double r = 0.2;
                    x = t * r * Math.cos(theta);
                    y = t * r * Math.sin(theta) + 1.5;
                    z = t * t;
                    Vector ve = new Vector(x, y, z);
                    ve = ParticleEffectRotation.rotateFunction(ve, loc);
                    loc.add(ve.getX(), ve.getY(), ve.getZ());
                    ParticleEffects.DRIP_WATER.sendToLocation(loc, 0, 0, 0, 0, 1);
                    loc.subtract(ve.getX(), ve.getY(), ve.getZ());
                    if (t > Math.PI * 4) {
                        this.cancel();
                    }
                }
            }
        }.createTimer(1, -1);
        final List<Block> blocks = activator.getEntity().getLineOfSight(null, 20);
        new AbilityRunnable(ability) {

            int counter = 0;

            public void run() {
                Block b = blocks.get(counter);

                for (AbilityEntity nearby : AbilityServer.get().getNearbyEntities(activator, b.getLocation(), radius, radius)) {
                    activator.abilityDamage(nearby, damage, ability, DamageCause.MAGIC);
                    nearby.getEntity().setVelocity(nearby.getEntity().getLocation().getDirection().normalize()
                                                         .multiply(-1.2D).add(new Vector(0.0D, 0.5D, 0.0D)));
                }
                counter++;
                if (counter >= blocks.size()) {
                    this.cancel();
                }
            }
        }.createTimer(1, -1);
        return true;
    }

}
