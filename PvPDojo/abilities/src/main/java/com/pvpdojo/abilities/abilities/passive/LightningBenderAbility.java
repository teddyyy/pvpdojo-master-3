package com.pvpdojo.abilities.abilities.passive;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.abilities.lib.ChargeSystem;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.util.bukkit.BlockUtil;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

import java.util.List;

public class LightningBenderAbility extends AbilityExecutor {
    @Override
    public String getExecutorName() {
        return "LightningBenderAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        if (event instanceof PlayerToggleSneakEvent) {
            Player player = ((PlayerToggleSneakEvent) event).getPlayer();

            ChargeSystem charge = new ChargeSystem(ability, 100, player.getLevel() < 100 ? 2 : 1) {

                @Override
                public void trigger() {
                    callLightningBender(ability);
                }

                @Override
                public void run() {
                }
            };
            charge.call();
        }

        return false;
    }

    private void callLightningBender(Ability ability) {
        new AbilityRunnable(ability) {

            int x = 3;
            int redirectX = 3;
            int maxSize = 34;
            List<Block> blocks;
            Location loc;
            AbilityEntity reflector;
            boolean reflected = false;

            @Override
            public void run() {
                if (x >= maxSize) {
                    this.cancel();
                    return;
                }

                loc = reflector == null ? ability.getAbilityEntity().getEntity().getEyeLocation().add(0, 20, 0)
                        : reflector.getEntity().getLocation().add(0, 20, 0);
                reflector = reflector == null ? ability.getAbilityEntity() : reflector;

                blocks = BlockUtil.getLineOfSight(loc, 100);

                Location loc = blocks.get((reflector == ability.getAbilityEntity() ? x : redirectX) - 1).getLocation();
                Location strikeLoc = BlockUtil.getFirstBlockDown(loc, null);

                strikeLoc.getWorld().strikeLightningEffect(strikeLoc);

                AbilityServer.get().getNearbyEntities(reflector, strikeLoc, 3.7, 2.8).forEach(target -> {

                    if (!reflected) {
                        if (canAbilityExecute(target.getEntity())) {
                            reflected = true;
                            redirectX = 4;
                            reflector = target;
                            return;
                        }
                    }

                    reflector.abilityDamage(target, ability.getAttribute("ability_damage"), ability, EntityDamageEvent.DamageCause.MAGIC);
                    target.getEntity().setFireTicks(20 * 10);
                });


                if (redirectX > 3) {
                    redirectX += 2;

                }
                x += 2;
            }
        }.createTimer(1, -1);
    }

    @EventHandler
    public void onLightningDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {
            Player player = (Player) e.getEntity();
            if (canAbilityExecute(player)) {
                if (e.getCause() == EntityDamageEvent.DamageCause.LIGHTNING) {
                    e.setCancelled(true);
                    player.setFireTicks(0);
                }
            }
        }
    }

    @EventHandler
    public void onLightningBender(PlayerToggleSneakEvent e) {
        if (canAbilityExecute(e.getPlayer())) {
            getAbility(e.getPlayer()).activateAbility(e);
        }
    }
}
