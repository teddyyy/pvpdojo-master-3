/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities;

import co.aikar.commands.InvalidCommandArgument;
import co.aikar.timings.lib.MCTiming;
import co.aikar.timings.lib.TimingManager;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilities.command.AbilityDataCommand;
import com.pvpdojo.abilities.command.GiveAbilityCommand;
import com.pvpdojo.abilities.command.SeeAbilitiesCommand;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.listeners.*;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.command.lib.DojoCommandManager;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.npc.DojoBot;
import com.pvpdojo.userdata.PlayStyle;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.Reflection;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.ItemBuilder.PotionBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionType;

import java.util.stream.Collectors;

public class Abilities extends JavaPlugin {

    private static Abilities instance;
    private static TimingManager timingManager;


    public static Abilities get() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        timingManager = TimingManager.of(this);
    }

    @Override
    public void onDisable() {
        AbilityServer.get().shutdown();
    }

    private void loadListeners() {
        new AbilityJoinListener().register(this);
        new AbilityLeaveListener().register(this);
        new AbilityClickListener().register(this);
        new AbilityDamageListener().register(this);
        new AbilityDeathListener().register(this);
        new AbilityToggleSneakListener().register(this);
        new AbilityDropListener().register(this);
        new AbilityConsumeItemListener().register(this);
        new AbilityProjectileListener().register(this);
        new AbilityEntityListener().register(this);
        new BotListener().register(this);
        new BlockListener().register(this);
    }

    private void loadCommands() {
        DojoCommandManager.get().getCommandCompletions()
                .registerStaticCompletion("abilities", AbilityLoader.getAbilities().stream()
                        .map(AbilityLoader::getName)
                        .map(str -> str.replaceAll(" ", "|"))
                        .sorted().collect(Collectors.toList()));

        DojoCommandManager.get().getCommandContexts()
                .registerContext(AbilityLoader.class, ctx -> {
                    AbilityLoader loader = AbilityLoader.getAbilityData(String.join(" ", StringUtils.PIPE.split(ctx.popFirstArg())));
                    if (loader == null) {
                        throw new InvalidCommandArgument();
                    }
                    return loader;
                });

        DojoCommandManager.get().registerCommand(new SeeAbilitiesCommand(), true);
        DojoCommandManager.get().registerCommand(new GiveAbilityCommand());
        DojoCommandManager.get().registerCommand(new AbilityDataCommand());
    }

    private void loadAbilityExecutors() {
        for (AbilityLoader loader : AbilityLoader.getAbilities()) {
            if (AbilityExecutor.getExecutor(loader.getAbilityExecutor()) == null) {
                try {
                    if (loader.isActive()) {
                        Reflection.createClassInstance("com.pvpdojo.abilities.abilities.active." + loader.getAbilityExecutor());
                    } else {
                        Reflection.createClassInstance("com.pvpdojo.abilities.abilities.passive." + loader.getAbilityExecutor());
                    }
                } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                    Log.severe(loader.getName() + " not found as ability class");
                    loader.setEnabled(false);
                }
            }
        }
    }

    public void start() {

        PvPDojo.get().getServerSettings().setSoup(true);
        PvPDojo.get().getServerSettings().addSoupCondition(player -> {
            AbilityPlayer champion = AbilityPlayer.get(player);
            return champion != null && champion.getPlayStyle() == PlayStyle.SOUP;
        });

        loadListeners();
        loadCommands();
        loadAbilityExecutors();
        loadUpdater();
        NMSUtils.registerEntity("Zombie", 54, DojoBot.class);

        ShapelessRecipe potterRecraft = new ShapelessRecipe(new PotionBuilder(PotionType.INSTANT_HEAL).splash().level(2).build());
        potterRecraft.addIngredient(Material.GLASS_BOTTLE);
        potterRecraft.addIngredient(Material.SPECKLED_MELON);
        Bukkit.getServer().addRecipe(potterRecraft);

        ShapelessRecipe ingotCraft = new ShapelessRecipe(new ItemStack(Material.GOLD_INGOT)).addIngredient(4, Material.GOLD_NUGGET);
        ShapelessRecipe gappleCraft = new ShapelessRecipe(new ItemStack(Material.GOLDEN_APPLE)).addIngredient(2, Material.GOLD_INGOT);
        ShapelessRecipe gappleCraft2 = new ShapelessRecipe(new ItemStack(Material.GOLDEN_APPLE, 2)).addIngredient(4, Material.GOLD_INGOT);

        Bukkit.getServer().addRecipe(ingotCraft);
        Bukkit.getServer().addRecipe(gappleCraft);
        Bukkit.getServer().addRecipe(gappleCraft2);

    }

    private void loadUpdater() {
        PvPDojo.schedule(() -> AbilityServer.get().update()).createTimer(1, -1);
    }

    public static MCTiming timing(String name) {
        return timingManager.of(name);
    }

}
