/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.abilities.AbilityMetaData;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.bukkit.events.EntityFallEvent;
import com.pvpdojo.bukkit.util.TempBlock;
import com.pvpdojo.util.bukkit.BlockUtil;

public class JudgementDayAbility extends AbilityExecutor {

    private String META_KEY = "judgementday";

    @Override
    public String getExecutorName() {
        return "JudgementDayAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        AbilityEntity activator = ability.getAbilityEntity();
        final AbilityMetaData meta = ability.getMetaData();
        final LivingEntity living = activator.getEntity();
        if (!meta.hasData(META_KEY)) {
            living.setVelocity(new Vector(0, 2.5, 0));

            PvPDojo.schedule(() -> meta.addData(META_KEY, System.currentTimeMillis() + 500, 10)).createTask(5);

            new AbilityRunnable(ability) {
                public void run() {
                    if (living.isOnGround()) {
                        cancel();
                    }
                }

                @Override
                public void cancel() {
                    super.cancel();
                    meta.removeData(META_KEY);
                    ability.activateAbility(false, null);
                }

            }.createTimer(20, 1, 100);
        } else {
            long timestamp = meta.getData(META_KEY);
            if (timestamp <= System.currentTimeMillis()) {
                living.setVelocity(living.getEyeLocation().getDirection().multiply(3).setY(-2.3));
            }
        }
        return false;
    }

    @EventHandler
    public void onFall(EntityFallEvent e) {
        if (!(e.getEntity() instanceof LivingEntity) || !canAbilityExecute((LivingEntity) e.getEntity()))
            return;
        final Ability ability = getAbility((LivingEntity) e.getEntity());
        if (!ability.getMetaData().hasData(META_KEY))
            return;
        e.setCancelled(true);
        ability.getMetaData().removeData(META_KEY);
        
        if(ability.getCooldown().isCoolingDown()) {
        	return;
        }
        
        final AbilityEntity activator = ability.getAbilityEntity();
        final float radius = ability.getAttribute("radius");
        float duration = ability.getAttribute("duration");
        final Location center = activator.getEntity().getLocation();
        for (Location loc : BlockUtil.circle(activator.getEntity().getLocation(), (int) radius, 3, true, false, 0)) {
            TempBlock.createTempBlock(loc, Material.OBSIDIAN, (byte) 0, (long) duration);
        }

        for (AbilityEntity nearby : AbilityServer.get().getNearbyEntities(activator, radius, 2)) {
            float damage = ability.getAttribute("ability_damage");
            activator.abilityDamage(nearby, damage, ability, DamageCause.FALL);
        }
        new AbilityRunnable(ability) {
            public void run() {
                for (AbilityEntity nearby : AbilityServer.get().getNearbyEntities(activator, center, radius, 2)) {
                    float damage = ability.getAttribute("dps_damage");
                    activator.abilityDamage(nearby, damage, ability, DamageCause.STARVATION);
                }
            }
        }.createTimer(20, (int) duration);
    }
}
