package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.Event;
import org.bukkit.scheduler.BukkitRunnable;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.ProjectileEntity;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.bukkit.util.TempBlock;
import com.pvpdojo.util.bukkit.BlockUtil;

public class TimeOutAbility extends AbilityExecutor {

	@Override
	public String getExecutorName() {
		return "TimeOutAbility";
	}

	@Override
	public boolean activateAbility(Ability ability, Event event) {
		AbilityEntity activator = ability.getAbilityEntity();
		final float duration = ability.getAttribute("duration");
		ProjectileEntity projectile = new ProjectileEntity(activator, false, 1.5f);

		projectile.setProjectileLoop(loc -> {
			loc.getWorld().playEffect(loc, Effect.STEP_SOUND, Material.GLASS);
			return false;
		});
		projectile.setProjectileDetection(target -> {

			Location prev_loc = target.getEntity().getLocation();
			
			Location loc = target.getEntity().getLocation().add(0, 7, 0);
			target.getEntity().teleport(BlockUtil.findLocationBetweenPoints(loc, target.getEntity().getLocation()));
			TimeoutEffect(loc, activator, target, (int) duration);
			new BukkitRunnable() {
				
				int c = (int) duration * 20;
				public void run() {
					if(target.getEntity().isDead()) this.cancel();
					if(--c <= 1) {
						target.getEntity().teleport(prev_loc);
						this.cancel();
					}
				}
			}.runTaskTimer(PvPDojo.get(), 0, 1);

		});
		projectile.startBlockCollision(activator.getEntity().getEyeLocation(), 1, 2, 40);

		return true;
	}

	// took off from prisoner but little changes
	public void TimeoutEffect(Location c, AbilityEntity attacker, AbilityEntity victim, int duration) {
		if (!attacker.canTarget(victim))
			return;
		Location center = c.clone().add(0, -2, 0);
		Location tp = center.getWorld().getBlockAt(center.getBlockX(), center.getBlockY(), center.getBlockZ())
				.getLocation().add(.45, .45, .45);
		for (int x = -1; x < 2; x++) {
			for (int y = -1; y < 2; y++) {
				for (int z = -1; z < 2; z++) {
					Location loc = center.clone().add(x, y, z);
					if (z == 0 && x == 0 && y > -1) {
						TempBlock.createTempBlock(loc, Material.AIR, (byte) 0, duration);
					} else
						TempBlock.createTempBlock(loc, Material.GLASS, (byte) 0, duration);
				}
			}
		}
		TempBlock.createTempBlock(center.clone().add(0, 2, 0), Material.GLASS, (byte) 0, duration);
		victim.getEntity().teleport(tp);
	}
}