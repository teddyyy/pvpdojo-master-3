/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilityinfo.AbilityType;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemUtil;

public class JokerAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "JokerAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        if (ability.getAbilityType() != AbilityType.CLICK) {
            ability.getAbilityEntity().sendMessage(MessageKeys.GENERIC, "{text}", "You can only use this ability with click activation");
            return false;
        }

        if (event instanceof PlayerInteractEntityEvent) {

            Player player = ((PlayerInteractEntityEvent) event).getPlayer();
            Player clicked = (Player) ((PlayerInteractEntityEvent) event).getRightClicked();

            player.sendMessage(PvPDojo.PREFIX + "You shuffled " + CC.BLUE + clicked.getNick() + "'s Inventory");
            clicked.sendMessage(PvPDojo.PREFIX + CC.RED + "Your inventory has been shuffled by a joker");
            final Inventory inv = clicked.getInventory();
            new AbilityRunnable(ability) {
                int s = 0;
                ItemStack previous;

                public void run() {
                    if (s < 10) {
                        if (s != 0) {
                            inv.setItem((s - 1), previous);

                        }
                        if (s < 9) {
                            previous = inv.getItem(s);
                            inv.setItem(s, new ItemStack(Material.SKULL_ITEM, 1, (short) 0));
                        } else
                            inv.setItem(8, previous);

                        s++;

                        for (int i = 0; i < 18; i++) {
                            int x = (int) (Math.random() * 9);
                            ItemStack itemx = inv.getItem(x);
                            ItemStack itemi = inv.getItem(i);
                            if (itemx != null && itemi != null) {
                                if (!itemx.getType().equals(Material.SKULL_ITEM) && !itemi.getType().equals(Material.SKULL_ITEM)) {
                                    if (!ItemUtil.SWORDS.contains(itemx.getType()) && !ItemUtil.SWORDS.contains(itemi.getType())) {
                                        inv.setItem(x, itemi);
                                        inv.setItem(i, itemx);
                                    }
                                }
                            }
                        }
                    } else
                        this.cancel();
                }
            }.createTimer(3, -1);

            return true;
        }

        return false;
    }

    @EventHandler
    public void onJoker(PlayerInteractEntityEvent e) {
        if (e.getRightClicked() instanceof Player && canAbilityExecute(e.getPlayer())) {
            Ability ability = getAbility(e.getPlayer());
            if (e.getPlayer().getItemInHand().getType() == ability.getClickItem().getType()) {
                ability.activateAbility(e);
            }
        }
    }

}
