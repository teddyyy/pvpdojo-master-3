/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;

public class SentryGunAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "SentryGunAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        final AbilityEntity activator = ability.getAbilityEntity();
        final LivingEntity living = activator.getEntity();
        final float amount = ability.getAttribute("amount");
        final float speed = ability.getAttribute("speed");
        final float fireRate = ability.getAttribute("fire_rate");
        new AbilityRunnable(ability) {
            @Override
            public void run() {
                Arrow arrow = living.getWorld().spawnArrow(living.getEyeLocation().add(0, 1.0, 0), living.getEyeLocation().getDirection().clone().multiply(speed), 1, 0);
                arrow.setShooter(living);
                ability.getMetaData().addEntity(arrow, 10, true);
            }
        }.createTimer((int) (6 - fireRate), (int) amount);
        return true;
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (!(e.getDamager() instanceof Arrow))
            return;
        Arrow arrow = (Arrow) e.getDamager();
        LivingEntity shooter = (LivingEntity) arrow.getShooter();
        if (!canAbilityExecute(shooter))
            return;
        Ability ability = getAbility(shooter);
        if (!ability.getMetaData().hasEntity(arrow))
            return;
        AbilityEntity target = AbilityServer.get().getEntity((LivingEntity) e.getEntity());
        AbilityEntity activator = AbilityServer.get().getEntity(shooter);
        float damage = ability.getAttribute("arrow_damage");
        activator.abilityDamage(target, damage, ability, DamageCause.PROJECTILE);
    }

}
