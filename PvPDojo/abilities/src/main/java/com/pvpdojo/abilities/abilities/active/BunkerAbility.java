package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.Event;
import org.bukkit.util.Vector;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.bukkit.util.TempBlock;
import com.pvpdojo.util.bukkit.BlockUtil;

public class BunkerAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "BunkerAbility";
    }

    @Override // base radius = 4, base duration = 5 sec, base cd 45 base points 4
    public boolean activateAbility(Ability ability, Event event) {
        AbilityEntity activator = ability.getAbilityEntity();
        final float radius = ability.getAttribute("radius");
        final float duration = ability.getAttribute("duration");

        // small feature
        for (AbilityEntity entity : AbilityServer.get().getNearbyEntities(activator, radius + 2.5, radius + 2.5)) {

            entity.getEntity()
                  .setVelocity(entity.getEntity().getVelocity()
                                     .add(entity.getEntity().getLocation().toVector()
                                                .subtract(activator.getEntity().getLocation().toVector()).normalize().multiply(1.2D)
                                                .add(new Vector(0, 0.2, 0))));
            // ends here

        }
        for (Location sphere : BlockUtil.circle(activator.getEntity().getLocation(), (int) radius, (int) radius, true,
                true, 0)) {
            TempBlock.createTempBlock(sphere, Material.IRON_BLOCK, (byte) 0, (long) (duration));

        }
        return true;
    }

}
