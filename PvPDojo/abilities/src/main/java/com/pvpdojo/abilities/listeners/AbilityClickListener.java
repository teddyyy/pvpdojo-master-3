/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.listeners;

import java.util.Optional;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.abilities.ActiveAbility;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilityinfo.AbilityType;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.util.bukkit.ItemUtil;

public class AbilityClickListener implements DojoListener {

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        ItemStack item = e.getItem();
        if (item == null)
            return;
        if (item.getType().equals(Material.FISHING_ROD))
            return;
        
        if (ItemUtil.SWORDS.contains(item.getType())) {
            if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                AbilityPlayer champion = AbilityPlayer.get(player);
                champion.getAbilities().stream().filter(ability -> ability.getAbilityType() == AbilityType.BLOCK && ability instanceof ActiveAbility && ability.isEnabled())
                        .forEach(ability -> ability.activateAbility(e));

            }
        }
        if (e.getAction() != Action.PHYSICAL && item.hasItemMeta() && item.getItemMeta().hasDisplayName()) {
            AbilityPlayer abilityPlayer = AbilityPlayer.get(player);
            Optional<Ability> found = abilityPlayer.getAbilities().stream().filter(ability -> ability instanceof ActiveAbility && ability.getAbilityType() == AbilityType.CLICK
                    && ability.isEnabled() && ability.getClickItem().getType() == item.getType()).findAny();
            if (found.isPresent()) {
                found.get().activateAbility(e);
                player.updateInventory();
                if (!item.getType().isBlock()) {
                    e.setCancelled(true);
                }
            }
        }
    }

}
