/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import java.util.Comparator;
import java.util.Optional;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.util.Vector;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.util.bukkit.CC;

public class ZenAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "ZenAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        AbilityPlayer source = (AbilityPlayer) ability.getAbilityEntity();
        Player player = source.getPlayer();

        double range = ability.getAttribute("range");
        int invincibility = (int) ability.getAttribute("invincibility");
        Location location = player.getLocation();
        Optional<AbilityPlayer> closestChampion = AbilityServer.get().getNearbyEntities(source, range).stream()
                                                               .filter(entity -> entity instanceof AbilityPlayer)
                                                               .map(entity -> (AbilityPlayer) entity)
                                                               .min(Comparator.comparingDouble(o -> o.getEntity().getLocation().distanceSquared(location)));
        if (closestChampion.isPresent()) {
            if (invincibility > 0) {
                source.setInvulnerable(invincibility * 20);
            }
            player.sendMessage(CC.GREEN + "Teleporting to " + closestChampion.get().getPlayer().getNick() + " in 2 seconds");
            closestChampion.get().sendMessage(MessageKeys.ZEN_TELEPORTING);
            new AbilityRunnable(ability) {
                public void run() {
                    Player closest = closestChampion.get().getPlayer();
                    Vector vec = closest.getLocation().getDirection().multiply(-1);
                    Location loc = closest.getLocation().add(vec);
                    if (!loc.getBlock().getType().equals(Material.AIR))
                        loc = closest.getLocation();

                    loc.setPitch(source.getEntity().getLocation().getPitch());
                    loc.setYaw(closest.getLocation().getYaw());
                    source.getEntity().teleport(loc);
                    closest.getWorld().playSound(loc, Sound.ENDERMAN_TELEPORT, 1f, 3);
                }
            }.createTask(40);
        } else {
            player.sendMessage(CC.RED + "No players in range!");
            return false;
        }
        return true;
    }

}
