/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.listeners;

import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.userdata.PlayStyle;
import com.pvpdojo.util.bukkit.CC;

public class AbilityProjectileListener implements DojoListener {

    private static final String OWNED_POTION = "owned_potion";

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        AbilityPlayer abilityPlayer = AbilityPlayer.get(e.getPlayer());

        if ((e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) && e.getItem() != null && e.getItem().getType() == Material.ENDER_PEARL) {
            if (abilityPlayer.getEnderPearlThrow() > System.currentTimeMillis()) {
                e.setCancelled(true);
                abilityPlayer.getPlayer().updateInventory();
                abilityPlayer.getPlayer().sendMessage(CC.RED + "Cooling down: " + (abilityPlayer.getEnderPearlThrow() - System.currentTimeMillis()) / 1000);
            } else {
                abilityPlayer.setEnderPearlThrow(System.currentTimeMillis() + 20000);
            }
        }
    }

    @EventHandler
    public void onProjectileLaunch(ProjectileLaunchEvent e) {
        if (e.getEntity().getShooter() instanceof Player) {
            AbilityPlayer abilityPlayer = AbilityPlayer.get((Player) e.getEntity().getShooter());
            if (abilityPlayer.getPlayStyle() == PlayStyle.POTTER) {
                if (e.getEntity() instanceof ThrownPotion) {
                    if (((ThrownPotion) e.getEntity()).getEffects().stream().anyMatch(potionEffect -> potionEffect.getType().equals(PotionEffectType.HEAL))) {
                        e.getEntity().setMetadata(OWNED_POTION, new FixedMetadataValue(PvPDojo.get(), abilityPlayer.getPlayer().getName()));
                    }
                }
            }
        }
    }

    @EventHandler
    public void onHealPotion(PotionSplashEvent e) {
        if (e.getEntity().hasMetadata(OWNED_POTION)) {
            String owner = BukkitUtil.consumeMeta(e.getEntity(), OWNED_POTION).get(0).asString();
            for (LivingEntity entity : e.getAffectedEntities()) {
                if (entity instanceof Player && !owner.equals(((Player) entity).getName())) {
                    e.setIntensity(entity, 0);
                }
            }
        }
    }

}
