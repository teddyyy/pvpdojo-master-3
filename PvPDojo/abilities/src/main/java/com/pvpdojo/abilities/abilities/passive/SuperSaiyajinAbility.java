/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.LazyMetadataValue;
import org.bukkit.metadata.LazyMetadataValue.CacheStrategy;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.util.bukkit.ItemUtil;

public class SuperSaiyajinAbility extends AbilityExecutor {

    public static final String SUPER_BLOCK = "supersain";

    @Override
    public String getExecutorName() {
        return "SuperSaiyajinAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        if (!(ability.getAbilityEntity().getEntity() instanceof Player)) {
            return false;
        }

        Player player = (Player) ability.getAbilityEntity().getEntity();

        new AbilityRunnable(ability) {
            int blocking = 0;

            @Override
            public void run() {

                if (blocking < 100) {
                    blocking += 2;
                }

                if (player.isBlocking()) {
                    player.setLevel(blocking);
                    Item item = player.getWorld().dropItem(player.getEyeLocation().clone().add(0, 0.2, 0), new ItemStack(Material.FIRE));
                    item.setCustomDespawnRate(2 * 20);
                    item.setPickupDelay(Integer.MAX_VALUE);
                    item.setVelocity(new Vector().setY(0.3));
                } else if (player.getLevel() == 100) {
                    cancel();
                    player.setLevel(0);

                    if (player.isOnGround()) {
                        player.setVelocity(player.getLocation().getDirection().multiply(-3).setY(.8));
                    }

                    Location loc = player.getEyeLocation().getBlock().getLocation();
                    ability.activateAbility(false, null);

                    new AbilityRunnable(ability) {
                        int i = 0;

                        @Override
                        public void run() {
                            i++;
                            if (i < 20) {
                                player.getWorld().strikeLightningEffect(player.getLocation());
                                Vector vec = player.getEyeLocation().getDirection().clone().multiply(3.0);
                                FallingBlock block = loc.getWorld().spawnFallingBlock(player.getEyeLocation(), Material.ICE, (byte) 0);
                                block.setVelocity(vec);
                                block.setDropItem(false);
                                block.setMetadata(SUPER_BLOCK, new LazyMetadataValue(PvPDojo.get(), CacheStrategy.NEVER_CACHE, () -> Bukkit.getPlayer(player.getUniqueId())));
                            } else {
                                cancel();
                            }
                        }
                    }.createTimer(1, -1);
                } else {
                    player.setLevel(0);
                    cancel();
                }

            }
        }.createTimer(1, -1);

        return false;
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player player = e.getPlayer();

        if (canAbilityExecute(player) && e.getItem() != null && ItemUtil.SWORDS.contains(e.getItem().getType())) {
            if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                getAbility(player).activateAbility(e);

            }
        }
    }

    @EventHandler
    public void onDevastatorBlock(EntityChangeBlockEvent e) {
        if (e.getEntity().hasMetadata(SUPER_BLOCK)) {
            e.setCancelled(true);
            e.getEntity().remove();
            e.getEntity().getWorld().playEffect(e.getEntity().getLocation(), Effect.STEP_SOUND, ((FallingBlock) e.getEntity()).getMaterial());

            LivingEntity shooter = (LivingEntity) e.getEntity().getMetadata(SUPER_BLOCK).get(0).value();
            if (shooter != null) {
                AbilityEntity source = AbilityEntity.get(shooter);
                for (AbilityEntity entity : AbilityServer.get().getNearbyEntities(source, e.getEntity().getLocation(), 4, 4)) {
                    source.abilityDamage(entity, 200F, getAbility(shooter), DamageCause.MAGIC);
                    entity.getEntity().setVelocity(entity.getEntity().getLocation().getDirection().multiply(-1.8));
                }
            }
        }
    }

}
