package com.pvpdojo.abilities.abilities.active;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.abilities.lib.ProjectileEntity;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.gamemode.BlindEffect;
import com.pvpdojo.abilities.gamemode.PoisonEffect;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.*;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import org.bukkit.util.Vector;

import java.util.List;

public class OctopusAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "OctopusAbility";
    }

    private static final String POTION_OCTOPUS = "potion_octopus";

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        AbilityEntity activator = ability.getAbilityEntity();

        ProjectileEntity projectile = new ProjectileEntity(activator, false, 1f);

        Item item = activator.getEntity().getWorld().dropItem(activator.getEntity().getEyeLocation(),
                new ItemStack(Material.WATER));
        item.setVelocity(activator.getEntity().getEyeLocation().getDirection().multiply(1.5));

        projectile.setProjectileHit(loc -> PvPDojo.schedule(() -> start(loc, ability)).createTask(10));

        projectile.startItemCollision(item, 1, 1000, true);

        return true;
    }

    private void start(Location loc, Ability ability) {

        new AbilityRunnable(ability) {
            float t = 20 * ability.getAttribute("duration") / 2;
            float a = 0;
            float rows = 6.53f;// 5.32f

            @Override
            public void run() {

                if (--t <= 0)
                    this.cancel();

                a += rows;
                if (a >= 360)
                    a = rows;
                if (a <= 30)// was 50
                    a = 30;
                for (int i = 0; i < 360; i++) {
                    i += a;
                    loc.setYaw(i);
                    ThrownPotion potion = (ThrownPotion) loc.getWorld().spawnEntity(loc, EntityType.SPLASH_POTION);
                    potion.setShooter(ability.getAbilityEntity().getEntity());
                    pushNearby(loc, potion);

                    Potion pot = new Potion(PotionType.INSTANT_DAMAGE, 1);
                    pot.setSplash(true);
                    potion.setItem(pot.toItemStack(1));
                    potion.setMetadata(POTION_OCTOPUS, new FixedMetadataValue(PvPDojo.get(),
                            ability.getAbilityEntity().getEntity().getUniqueId().toString()));

                    potion.setVelocity(loc.getDirection().normalize().multiply(0.45).setY(0.4));//multiply 0.45
                }
                // playFirework(loc.getWorld(), loc.getBlock().getLocation().add(0.0D, 1.0D,
                // 0.0D), FireworkEffect
                // .builder().with(FireworkEffect.Type.BURST).withColor(Color.LIME).withFade(Color.GREEN).build());
                // ^^^^^^ made chris blind

            }
        }.createTimer(2, -1);
    }

    private void pushNearby(Location ploc, Entity relation) {

        List<AbilityEntity> nearby = AbilityServer.get().getNearbyEntities(ploc, 3.0, 3);
        for (AbilityEntity e : nearby) {
            if (e.getEntity().equals(relation))
                continue;
            Location hloc = e.getEntity().getLocation();
            double distance = ploc.distance(hloc);
            Vector vector = hloc.toVector().subtract(ploc.toVector());
            if (distance < 7.0D) {
                if (e.getEntity().getType().equals(EntityType.DROPPED_ITEM)) {
                    e.getEntity().setVelocity(vector.multiply(0.4D).setY(0.01D));
                    continue;
                }
                e.getEntity().setVelocity(vector.multiply(getRelativeSpeed(distance)).setY(0.01D));
            }
        }

    }

    private double getRelativeSpeed(double distance) {
        return 0.3D / distance;
    }

    public void playFirework(World world, Location loc, FireworkEffect fe) {
        final Firework fw = (Firework) world.spawn(loc, Firework.class);
        FireworkMeta data = fw.getFireworkMeta();

        data.clearEffects();
        data.setPower(1);
        data.addEffect(fe);
        fw.setFireworkMeta(data);
        PvPDojo.schedule(() -> fw.detonate()).createTask(4);
    }

    @EventHandler
    public void onPotionSplash(PotionSplashEvent e) {
        if (e.getEntity().hasMetadata(POTION_OCTOPUS) && e.getEntity().getShooter() instanceof LivingEntity) {
            for (PotionEffect p : e.getPotion().getEffects()) {
                if (p.getType().equals(PotionEffectType.HARM)) {
                    e.setCancelled(true);

                    AbilityEntity activator = AbilityEntity.get((LivingEntity) e.getEntity().getShooter());
                    Ability ability = getAbility(activator.getEntity());

                    //Could be casted when the activator dies and the effect is still running.
					if(activator == null || ability == null) {
						return;
					}

                    for (AbilityEntity entities : AbilityServer.get().getNearbyEntities(e.getEntity().getLocation(),
                            3.5, 3.5)) {
                        if (entities == null || entities.getEntity().equals(e.getEntity().getShooter())) {
                            continue;
                        }

                        activator.abilityDamage(entities, ability.getAttribute("damage"), ability, DamageCause.MAGIC);
                        entities.applyEffect(new BlindEffect(20 * 4, 1));
                        entities.applyEffect(new PoisonEffect(20 * 4, 1));

                    }
                }
            }

        }
    }

}
