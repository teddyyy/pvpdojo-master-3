/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import static java.lang.Math.random;

import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.abilities.AbilityMetaData;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.listeners.events.AbilityHitEvent;

public class ThunderLordsAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "ThunderLordsAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        AbilityMetaData meta = ability.getMetaData();
        AbilityEntity target = meta.getTarget();
        AbilityEntity activator = ability.getAbilityEntity();
        if (target == null)
            return false;

        PvPDojo.schedule(() -> {
            target.getEntity().getWorld().spigot().strikeLightningEffect(target.getEntity().getEyeLocation(), true);
            float damage = ability.getAttribute("ability_damage");
            activator.abilityDamage(target, damage, ability, DamageCause.LIGHTNING);
            if (target instanceof AbilityPlayer) {
                // Stolen from EntityLightning
                ((Player) target.getEntity()).playSound(target.getEntity().getEyeLocation(), Sound.AMBIENCE_THUNDER, 1000, (float) (0.8F + random() * 0.2F));
                ((Player) target.getEntity()).playSound(target.getEntity().getEyeLocation(), Sound.EXPLODE, 2F, (float) (0.5F + random() * 0.2F));
            }
            if (activator instanceof AbilityPlayer) {
                ((Player) activator.getEntity()).playSound(target.getEntity().getEyeLocation(), Sound.AMBIENCE_THUNDER, 1000, (float) (0.8F + random() * 0.2F));
                ((Player) activator.getEntity()).playSound(target.getEntity().getEyeLocation(), Sound.EXPLODE, 2F, (float) (0.5F + random() * 0.2F));
            }
        }).nextTick(); // Delay by one tick to dont call inside entitydamagebyentity logic

        return true;
    }

    @EventHandler
    public void onAbilityHit(AbilityHitEvent e) {
        Ability ability = e.getAbility();

        if (!canAbilityExecute(e.getActivator().getEntity()))
            return;

        if (ability.getAbilityExecutor().equals(getExecutorName())) {
            return;
        }

        AbilityMetaData meta = ability.getMetaData();
        meta.addTarget(e.getEntity(), 60);
        if (meta.getData().size() > 2) {
            if (ability.activateAbility(true, e)) {
                meta.getData().clear();
            }
        } else {
            meta.addData(ability.getAbility(), "");
        }
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (!(e.getDamager() instanceof LivingEntity) || e.getCause() == DamageCause.LIGHTNING)
            return;
        LivingEntity damager = (LivingEntity) e.getDamager();
        if (!canAbilityExecute(damager) || !(e.getEntity() instanceof LivingEntity))
            return;
        Ability ability = getAbility(damager);
        AbilityMetaData meta = ability.getMetaData();
        AbilityEntity target = AbilityServer.get().getEntity((LivingEntity) e.getEntity());
        if (target == null) {
            return;
        }
        ability.getMetaData().addTarget(target, 20);
        if (meta.getData().size() > 2) {
            if (ability.activateAbility(true, e)) {
                meta.getData().clear();
            }
        } else {
            meta.addData(ability.getAbility() + meta.getData().size(), "");
        }
    }

}
