/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.gamemode;

import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilityinfo.KitAttributeArray.AttributeArrayType;
import com.pvpdojo.abilityinfo.KitAttributeArray.KitAttribute;

public class SlowEffect extends AbilityEffect {

    float intensity;
    KitAttribute attribute;

    public SlowEffect(int duration, float intensity) {
        super(AbilityEffectType.SLOW, duration);
        this.intensity = intensity;
    }

    @Override
    public void applyEffect(AbilityEntity entity) {
        attribute = new KitAttribute("move_speed", this.intensity, AttributeArrayType.BONUS_ATTRIBUTES);
        entity.addAttribute(attribute);
        // if (entity.getEntity() instanceof Player)
        // DojoListeners.cancelJump((Player) entity.getEntity(), this.getTickDuration());
    }

    @Override
    public void endEffect(AbilityEntity entity) {
        entity.removeAttribute(attribute);
        // if (entity.getEntity() instanceof Player)
        // DojoListeners.resumeJump((Player) entity.getEntity());
    }
}