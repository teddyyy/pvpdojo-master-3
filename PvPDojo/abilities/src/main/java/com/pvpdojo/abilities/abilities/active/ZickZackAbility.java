package com.pvpdojo.abilities.abilities.active;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Firework;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.effects.ParticleEffectRotation;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.bukkit.events.EntityFallEvent;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.ParticleEffects;

import lombok.Getter;
import lombok.Setter;

public class ZickZackAbility extends AbilityExecutor {

	@Override
	public String getExecutorName() {
		return "ZickZackAbility";
	}

	private static final String FALL_DAMAGE = "fall_damage";
	// tried to use cancelNextFall method, but it does not trigger well if u get
	// damage before falling.

	@Override
	public boolean activateAbility(Ability ability, Event event) {
		LivingEntity player = ability.getAbilityEntity().getEntity();
		String icon = player instanceof Player ? ((Player) player).getNick().substring(0, 1).toUpperCase() : "A";
		Font font = new Font("Tahoma", 0, 24);
		boolean invert = true;
		int stepX = 1;
		int stepY = 1;
		float size = 0.3F;
		int clr = 0;
		BufferedImage image = null;
		Location loc = player.getLocation().add(player.getLocation().getDirection().multiply(8.0D).getX(), 2.0D,
				player.getLocation().getDirection().multiply(8.0D).getZ());

		try {
			if (image == null) {
				image = stringToBufferedImage(font, icon);
			}

			for (int y = 0; y < image.getHeight(); y += stepY) {
				for (int x = 0; x < image.getWidth(); x += stepX) {
					clr = image.getRGB(x, y);
					if ((invert || Color.black.getRGB() == clr) && (!invert || Color.black.getRGB() != clr)) {
						Vector v = (new Vector(image.getWidth() / 2.0F - x, image.getHeight() / 2.0F - y, 0.0F))
								.multiply(size);
						ParticleEffectRotation.rotateAboutY(v, (-loc.getYaw() * Math.PI / 180));
						loc.add(v);
						ParticleEffects.FIREWORK_SPARK.sendToLocation(loc, 0, 0, 0, 0, 1);
						loc.subtract(v);
					}

				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		ZickZackBeam beam = new ZickZackBeam(ability, player) {

			@Override
			public void onHit(AbilityEntity entity) {
				ability.getAbilityEntity().abilityDamage(entity, ability.getAttribute("damage"), ability,
						DamageCause.ENTITY_ATTACK);
				entity.getEntity().setFireTicks(20 * 5);
			}
		};
		beam.start();
		return true;
	}

	public static BufferedImage stringToBufferedImage(Font font, String s) {
		BufferedImage img = new BufferedImage(1, 1, 6);
		Graphics g = img.getGraphics();
		g.setFont(font);

		FontRenderContext frc = g.getFontMetrics().getFontRenderContext();
		Rectangle2D rect = font.getStringBounds(s, frc);
		g.dispose();

		img = new BufferedImage((int) Math.ceil(rect.getWidth()), (int) Math.ceil(rect.getHeight()), 6);
		g = img.getGraphics();
		g.setColor(Color.black);
		g.setFont(font);

		FontMetrics fm = g.getFontMetrics();
		int x = 0;
		int y = fm.getAscent();

		g.drawString(s, x, y);
		g.dispose();

		return img;
	}

	@Getter
	@Setter
	public abstract class ZickZackBeam {

		LivingEntity player;
		Ability ability;
		private static final int DELAY = 20;
		private static final double KNOCKBACK = 1.2D;
		private static final double KNOCKBACK_Y = 1.2D;

		public ZickZackBeam(Ability ability, LivingEntity player) {
			this.player = player;
			this.ability = ability;

		}

		public abstract void onHit(AbilityEntity entity);

		public void start() {
			player.getWorld().playSound(player.getLocation(), Sound.EXPLODE, 1, 0);
			new BukkitRunnable() {

				Location loc = player.getEyeLocation();
				Vector dir = loc.getDirection().normalize();
				int t = 0;

				public void run() {
					if (++t >= 30)
						this.cancel();
					if (player.isDead())
						this.cancel();

					double x = dir.getX() * t;
					double y = dir.getY() * t;
					double z = dir.getZ() * t;
					loc.add(x, y, z);
					Firework firework = loc.getWorld().spawn(loc, Firework.class);
					firework.remove();

					List<AbilityEntity> nearby = AbilityServer.get().getNearbyEntities(loc, 3.5D, 0.0d);

					for (AbilityEntity target : nearby) {
						if (target.equals(ability.getAbilityEntity()))
							continue;
						onHit(target);
						new BukkitRunnable() {

							int x = DELAY;

							public void run() {
								if (target.isDead()) {
									this.cancel();
									return;
								}
								if (--x <= 0)
									this.cancel();
								if (x <= 0 && !target.isDead()) {
									final Vector v = player.getLocation().getDirection().multiply(KNOCKBACK)
											.setY(KNOCKBACK_Y);
									target.getEntity().setVelocity(v);
									target.getEntity().setMetadata(FALL_DAMAGE, new FixedMetadataValue(PvPDojo.get(), null));
								}
							}

						}.runTaskTimer(PvPDojo.get(), 0, 1);
					}

					loc.subtract(x, y, z);
				}

			}.runTaskTimer(PvPDojo.get(), 0, 1);
			new BukkitRunnable() {

				int x = DELAY;

				public void run() {
					if (player.isDead()) {
						this.cancel();
						return;
					}
					if (--x <= 0)
						this.cancel();
					if (x <= 0 && !player.isDead()) {
						final Vector v = player.getLocation().getDirection().multiply(KNOCKBACK).setY(KNOCKBACK_Y);
						player.setVelocity(v);
						player.setMetadata(FALL_DAMAGE, new FixedMetadataValue(PvPDojo.get(), null));
					}
				}

			}.runTaskTimer(PvPDojo.get(), 0, 1);
		}
	}

	@EventHandler
	public void onEntityDeath(EntityDeathEvent e) {
		if (e.getEntity().hasMetadata(FALL_DAMAGE))
			e.getEntity().removeMetadata(FALL_DAMAGE, PvPDojo.get());
	}

	@EventHandler
	public void onFall(EntityFallEvent e) {
		if (e.getEntity().hasMetadata(FALL_DAMAGE)) {
			e.setCancelled(true);
			e.getEntity().removeMetadata(FALL_DAMAGE, PvPDojo.get());
		}
	}
}
