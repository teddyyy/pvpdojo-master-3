package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemUtil;

public class NoobAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "NoobAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        return false;
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        Player player = e.getPlayer();
        Item item = e.getItemDrop();
        if (canAbilityExecute(player)) {
            Ability ability = getAbility(player);
            final float dropSoup = ability.getAttribute("drop_soup");

            // You are allowed to drop your sword if you have more than 1
            if (ItemUtil.SWORDS.contains(e.getItemDrop().getItemStack().getType())) {
                int swords = 0;
                for (ItemStack inventory : player.getInventory().getContents()) {
                    if (inventory != null && inventory.getType() != null) {
                        if (ItemUtil.SWORDS.contains(inventory.getType())) {
                            swords++;
                        }
                    }
                }

                if (swords == 0) {
                    e.setCancelled(true);
                    player.sendMessage(PvPDojo.PREFIX + CC.RED + "You can't drop your sword!");
                }
            }

            if (dropSoup > 0) {
                if (item.getItemStack().getType() == Material.MUSHROOM_SOUP) {
                    e.setCancelled(true);
                    player.sendMessage(PvPDojo.PREFIX + CC.RED + "You can't drop your soup!");
                }
            }
        }
    }

}
