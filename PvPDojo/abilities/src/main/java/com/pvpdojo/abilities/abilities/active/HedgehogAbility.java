/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.entity.Arrow;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;

public class HedgehogAbility extends AbilityExecutor {

    private static final String HEDGE_META = "hedgehog";

    @Override
    public String getExecutorName() {
        return "HedgehogAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        Location loc = ability.getAbilityEntity().getEntity().getEyeLocation();
        Map<Arrow, Vector> arrows = new HashMap<>();

        for (int yaw = -180; yaw <= 180; yaw += 15) {
            for (int pitch = -75; pitch <= 15; pitch += 15) {

                Arrow a = ability.getAbilityEntity().getEntity().launchProjectile(Arrow.class);

                a.setKnockbackStrength(6);
                a.setMetadata(HEDGE_META, new FixedMetadataValue(PvPDojo.get(), true));

                loc.setYaw(yaw);
                loc.setPitch(pitch);

                Vector handle = loc.getDirection().multiply(0.5);
                arrows.put(a, handle);
                a.setVelocity(handle);

            }
        }

        new AbilityRunnable(ability) {
            int running = 0;

            public void run() {

                if (running >= 8 * 20) {

                    for (Arrow a : arrows.keySet()) {
                        a.remove();
                        a.removeMetadata(HEDGE_META, PvPDojo.get());
                    }
                    cancel();
                    return;
                }

                for (Arrow a : arrows.keySet()) {

                    if (a == null || arrows.get(a) == null) continue;
                    if (a.isOnGround()) {
                        a.remove();
                        continue;
                    }
                    if (!a.isValid() || a.isDead()) continue;

                    a.setVelocity(arrows.get(a));

                }

                running++;

            }
        }.createTimer(1, -1);
        return true;
    }

    @EventHandler
    public void onHedgeHit(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Arrow && e.getDamager().hasMetadata(HEDGE_META)) {
            e.setDamage(7.5);
            if (((Arrow) e.getDamager()).getShooter() == e.getEntity()) {
                e.setCancelled(true);
            }
        }
    }


}
