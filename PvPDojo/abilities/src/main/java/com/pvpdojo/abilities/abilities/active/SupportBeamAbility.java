/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.ProjectileDetection;
import com.pvpdojo.abilities.abilities.lib.ProjectileEntity;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.util.bukkit.ParticleEffects;

public class SupportBeamAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "SupportBeamAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        final AbilityEntity activator = ability.getAbilityEntity();
        final boolean collateral = true;
        final float heal = ability.getAttribute("heal");
        final float damage = ability.getAttribute("ability_damage");

        ProjectileEntity projectile = new ProjectileEntity(activator, collateral, 1f);
        projectile.setProjectileLoop(loc -> {
            loc.getWorld().playEffect(loc, Effect.STEP_SOUND, Material.GOLD_BLOCK);
            return false;
        });
        projectile.setProjectileDetection(new ProjectileDetection() {
            int collisions = 0;
            AbilityEntity healed;

            @Override
            public void onCollision(AbilityEntity target) {
                if (collisions == 0) {
                    target.heal(heal);
                    ParticleEffects.HEART.sendToLocation(target.getEntity().getEyeLocation().add(0, .5, 0), 0, 0, 0, 1, 1);
                    healed = target;
                    collisions++;
                } else if (collisions == 1) {
                    activator.abilityDamage(target, damage, ability, DamageCause.MAGIC);
                    activator.heal(heal);
                    ParticleEffects.HEART.sendToLocation(activator.getEntity().getEyeLocation().add(0, .5, 0), 0, 0, 0, 1, 1);
                    if (healed != null) {
                        if (healed.isTargetable()) {
                            healed.heal(heal);
                            ParticleEffects.HEART.sendToLocation(healed.getEntity().getEyeLocation().add(0, .5, 0), 0, 0, 0, 1, 1);
                        }
                    }
                }

            }
        });
        projectile.startBlockCollision(activator.getEntity().getEyeLocation(), 1, 1, 35);
        return true;
    }

}
