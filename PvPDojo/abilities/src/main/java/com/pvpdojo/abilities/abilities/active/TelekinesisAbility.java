/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityCrosshair;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.BlockUtil;

public class TelekinesisAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "TelekinesisAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        LivingEntity activator = ability.getAbilityEntity().getEntity();

        return new AbilityCrosshair(ability.getAbilityEntity()) {

            @Override
            public void targetFound(AbilityEntity target, double accuracy, int distance) {
                Location startLocation = target.getEntity().getLocation();
                double targetDistance = target.getEntity().getLocation().subtract(activator.getLocation()).length();

                new AbilityRunnable(ability) {

                    int runs = (int) (ability.getAttribute("air") * 20);

                    @Override
                    public void run() {
                        if (target.getEntity() instanceof Player) {
                            User.getUser((Player) target.getEntity()).cancelNextFall();
                        }

                        if (--runs == 0) {
                            cancel();
                            return;
                        }

                        Location targetLocation = target.getEntity().getLocation();
                        Location moveTo = activator.getEyeLocation().add(activator.getLocation().getDirection().multiply(targetDistance));
                        target.getEntity().setVelocity(moveTo.subtract(targetLocation).toVector().multiply(0.2));

                    }

                    @Override
                    public void cancel() {
                        super.cancel();
                        Location searchLocation = target.getEntity().getLocation();
                        searchLocation.setY(0);
                        double lowestBlock = BlockUtil.getFirstBlockUp(searchLocation, null).getY();
                        if (lowestBlock > target.getEntity().getLocation().getY() || lowestBlock == 0) {
                            target.getEntity().teleport(startLocation);
                        } else {
                            target.getEntity().setVelocity(startLocation.subtract(target.getEntity().getLocation()).toVector().normalize().multiply(0.5));
                        }
                    }
                }.createTimer(1, -1);

            }
        }.calculate(3, 17, false);
    }
}
