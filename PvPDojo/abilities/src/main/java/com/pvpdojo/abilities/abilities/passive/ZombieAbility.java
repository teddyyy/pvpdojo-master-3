package com.pvpdojo.abilities.abilities.passive;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class ZombieAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "ZombieAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        LivingEntity killer = ability.getAbilityEntity().getEntity();
        killer.setHealth(killer.getMaxHealth());
        killer.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, (int) (20 * (ability.getAttribute("duration"))), (int) (ability.getAttribute("power"))));
        return false;
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        if (e.getEntity().getKiller() != null) {
            Player killer = (Player) e.getEntity().getKiller();
            if (canAbilityExecute(killer)) {
                activateAbility(getAbility(killer), e);
            }
        }
    }

}
