/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.guns.Gun;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.listeners.events.DojoSelectKitEvent;

public class ShotgunAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "ShotgunAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        final AbilityEntity activator = ability.getAbilityEntity();
        activator.getGunController().clickGunAbility(this.getExecutorName());
        return true;
    }

    @SuppressWarnings("deprecation")
    @EventHandler
    public void onSelect(DojoSelectKitEvent e) {
        AbilityEntity activator = e.getEntity();
        LivingEntity living = activator.getEntity();
        if (activator.getAbilityFromExecutor(getExecutorName()) == null)
            return;
        Ability ability = getAbility(living);
        Gun gun = new Gun("ShotgunAbility", ability);
        gun.setName("ShotgunAbility");
        gun.setAmmoType("" + Material.SNOW_BALL.getId());
        gun.setAmmoAmountNeeded(1);
        gun.bulletDelayTime = 15;
        gun.reloadType = "PUMP";
        gun.setRoundsPerBurst(1);
        gun.setBulletsPerClick((int) ability.getAttribute("pellet_count"));
        gun.setGunDamage(ability.getAttribute("bullet_damage"));
        gun.setMaxDistance(11);
        gun.setBulletSpeed(3);
        gun.setAccuracy(ability.getAttribute("pellet_spread"));
        gun.setAccuracyAimed(ability.getAttribute("pellet_spread"));
        gun.setAccuracyCrouched(ability.getAttribute("pellet_spread"));
        gun.setRecoil(3);
        gun.setKnockback(5);
        gun.setCanAimLeft(true);
        gun.setCanAimRight(false);
        gun.setCanHeadshot(false);
        gun.setCanClickLeft(false);
        gun.setCanClickRight(true);
        gun.setExplodeRadius(0);
        gun.gunSound.add("ghast_fireball");
        gun.gunSound.add("explode");
        gun.setSmokeTrail(false);
        gun.setGunVolume(.7f);
        gun.hasClip = true;
        gun.setLocalGunSound(false);
        gun.maxClipSize = (int) ability.getAttribute("clip_size");
        gun.ammoLeft = (int) ability.getAttribute("total_ammo");
        gun.ammoReset = gun.ammoLeft;
        gun.setReloadTime((int) (ability.getAttribute("reload_time") * 20));

        activator.selectGunAbility(gun, ability);
    }

}
