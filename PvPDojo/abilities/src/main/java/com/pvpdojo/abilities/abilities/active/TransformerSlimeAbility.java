
package com.pvpdojo.abilities.abilities.active;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.craftbukkit.v1_7_R4.entity.disguise.CraftCreatureDisguise;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Slime;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.SlimeSplitEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.gamemode.AbilityServer;

public class TransformerSlimeAbility extends AbilityExecutor {

	private HashMap<UUID, Double> damage = new HashMap<UUID, Double>();
	private HashMap<UUID, List<Slime>> slimes = new HashMap<UUID, List<Slime>>();
	private static final String TAMEABLE = "tameable";

	/*
	 * 
	 * 
	 * REPLACED WITH WITCH HEAL ABILITY
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	
	
	@Override
	public String getExecutorName() {
		return "TransformerSlimeAbility";
	}

	@Override
	public boolean activateAbility(Ability ability, Event event) {

		if (ability.getAbilityEntity().getEntity().getDisguise() == null) {
			LivingEntity entity = ability.getAbilityEntity().getEntity();

			CraftCreatureDisguise disguise = new CraftCreatureDisguise(ability.getAbilityEntity().getEntity(), EntityType.SLIME);
			disguise.setCustomName(entity instanceof Player ? ((Player) entity).getNick() : entity.getCustomName());
			disguise.setCustomNameVisible(true);

			ability.getAbilityEntity().getEntity().setDisguise(disguise);
			new AbilityRunnable(ability) {
				@Override
				public void run() {
					cancel();
				}

				@Override
				public void cancel() {
					super.cancel();
					entity.setDisguise(null);
				}
			}.createTask(20 * 10);
			return true;
		}

		return false;
	}

	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player) {
			Player player = (Player) e.getEntity();
			if (canAbilityExecute(player)) {
				if (player.getDisguise() != null && player.getDisguise().getDisguiseType() != null
						&& player.getDisguise().getDisguiseType() == EntityType.SLIME) {
					double currentDamage = damage.containsKey(player.getUniqueId()) ? damage.get(player.getUniqueId())
							: 0;
					List<Slime> currentSlimes = slimes.containsKey(player.getUniqueId())
							? slimes.get(player.getUniqueId())
							: null;
					if ((currentDamage + e.getDamage()) >= 5.0) {
						if (!slimes.containsKey(player.getUniqueId())) {
							slimes.put(player.getUniqueId(), new ArrayList<Slime>());
						}
						if (currentSlimes != null && currentSlimes.size() < 5) {
							Slime slime = (Slime) player.getWorld().spawnEntity(player.getLocation(), EntityType.SLIME);
							AbilityServer.get().createEntity(slime);
							slime.setMaxHealth(10.0D);
							slime.setHealth(10.0D);
							slime.setMetadata(TAMEABLE,
									new FixedMetadataValue(PvPDojo.get(), player.getUniqueId().toString()));
							slimes.get(player.getUniqueId()).add(slime);
							damage.remove(player.getUniqueId());
							new BukkitRunnable() {

								@Override
								public void run() {
									if (slimes.containsKey(player.getUniqueId()))
										slimes.remove(player.getUniqueId());
								}
							}.runTaskLater(PvPDojo.get(), 20 * 15);
						}
					} else {
						damage.put(player.getUniqueId(), (e.getDamage() + currentDamage));
					}
				}
			}
		}
	}

	@EventHandler
	public void onSpilt(SlimeSplitEvent e) {
		if (e.getEntity().hasMetadata(TAMEABLE))
			e.setCancelled(true);
	}

	@EventHandler
	public void onDeathDrop(EntityDeathEvent e) {
		if (e.getEntity() instanceof Slime && e.getEntity().hasMetadata(TAMEABLE))
			e.getDrops().clear();

	}

	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player && (e.getDamager() instanceof Slime && e.getDamager().getMetadata(TAMEABLE)
				.get(0).asString().contains(e.getEntity().getUniqueId().toString())))// sometimes they attack wtf?
			e.setCancelled(true);
}

	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		Player player = e.getEntity();
		if (damage.containsKey(player.getUniqueId()))
			damage.remove(player.getUniqueId());
		if (slimes.containsKey(player.getUniqueId()))
			slimes.remove(player.getUniqueId());
	}

	@EventHandler
	public void onTarget(EntityTargetEvent e) {
		if (e.getEntity() instanceof Slime && e.getTarget() instanceof Player) {
			Player target = (Player) e.getTarget();
			if (e.getEntity().hasMetadata(TAMEABLE) && e.getEntity().getMetadata(TAMEABLE).get(0).asString()
					.contains(target.getUniqueId().toString())) {
				e.setCancelled(true);
			}
		}
	}

}
