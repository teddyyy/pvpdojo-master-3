/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import java.util.Random;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Item;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.ProjectileEntity;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.util.bukkit.ParticleEffects;

public class ZapperAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "ZapperAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {

        final AbilityEntity activator = ability.getAbilityEntity();
        ProjectileEntity projectile = new ProjectileEntity(activator, false, 1);

        final Item item = activator.getEntity().getWorld().dropItem(activator.getEntity().getEyeLocation(), new ItemStack(Material.FIREWORK_CHARGE));
        item.setVelocity(activator.getEntity().getEyeLocation().getDirection().multiply(1.5));

        projectile.setProjectileDetection(target -> {
            spawnRandomFirework(item.getLocation());
            float damage = ability.getAttribute("ability_damage");
            activator.abilityDamage(target, damage, ability, DamageCause.ENTITY_EXPLOSION);
        });

        projectile.setProjectileLoop(loc -> {
            ParticleEffects.LARGE_SMOKE.sendToLocation(loc, 0, 0, 0, 0, 1);
            return false;
        });

        projectile.startItemCollision(item, 1, 500, true);
        return true;
    }

    private void spawnRandomFirework(Location location) {
        Random random = PvPDojo.RANDOM;
        Firework firework = (Firework) location.getWorld().spawnEntity(location, EntityType.FIREWORK);
        FireworkMeta fireworkMeta = firework.getFireworkMeta();
        fireworkMeta.addEffect(FireworkEffect.builder().trail(true).withColor(Color.fromBGR(random.nextInt(255), random.nextInt(255), random.nextInt(255))).build());
        firework.setFireworkMeta(fireworkMeta);
        firework.detonate();
    }

}
