/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class StoneSwordersAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "StoneSwordersAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        AbilityServer.get().getNearbyEntities(ability.getAbilityEntity(), 10).forEach(abilityEntity -> {
            if (abilityEntity instanceof AbilityPlayer) {
                Player target = ((AbilityPlayer) abilityEntity).getPlayer();

                target.sendMessage(PvPDojo.PREFIX + CC.RED + "YOU'RE IN THE MAX ZONE NOW BITCH!");

                for (int i = 0; i < 36; i++) {
                    if (target.getInventory().getItem(i) != null && target.getInventory().getItem(i).getType() == Material.STONE_SWORD) {
                        target.getInventory().setItem(i, new ItemBuilder(Material.WOOD_SWORD).unbreakable().build());
                        target.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 20 * 15, 1));
                    }
                }

                PvPDojo.schedule(() -> {
                    for (int i = 0; i < 36; i++) {
                        if (target.getInventory().getItem(i) != null && target.getInventory().getItem(i).getType() == Material.WOOD_SWORD) {
                            target.getInventory().setItem(i, new ItemBuilder(Material.STONE_SWORD).unbreakable().build());
                        }
                    }
                }).createTask(25 * 20);
            }
        });

        return true;
    }
}
