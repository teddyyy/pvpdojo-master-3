/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import java.util.HashMap;
import java.util.Iterator;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.util.bukkit.CC;

public class DisplacerAbility extends AbilityExecutor {

    public static final String REFILLED_META = "refilled";

    @Override
    public String getExecutorName() {
        return "DisplacerAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        AbilityPlayer activator = (AbilityPlayer) ability.getAbilityEntity();
        boolean refill = ability.getAttribute("refill") > 0;

        Player player = activator.getPlayer();

        boolean replace = refill && !ability.getMetaData().hasData(REFILLED_META);

        for (int i = 0; i <= 8; i++) {
            ItemStack item = player.getInventory().getItem(i);
            if (item != null) {
                if (item.getType().equals(Material.BOWL)) {
                    if (replace) {
                        ability.getMetaData().addData(REFILLED_META, null, 180);
                        item.setAmount(1);
                        item.setType(Material.MUSHROOM_SOUP);
                    } else {
                        player.getInventory().clear(i);
                    }
                }
            }
        }

        if (refill && !replace) {
            HashMap<Integer, ? extends ItemStack> soups = player.getInventory().all(Material.MUSHROOM_SOUP);
            Iterator<Integer> itr = soups.keySet().iterator();

            for (int i = 0; i < Math.min(soups.size(), 8); i++) {
                player.getInventory().clear(itr.next());
                player.getInventory().addItem(new ItemStack(Material.MUSHROOM_SOUP));
            }

        }

        player.updateInventory();
        player.sendMessage(CC.GREEN + "Bowls have been cleared from your hotbar");

        return true;
    }

}
