/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.listeners.events.DojoSelectKitEvent;
import com.pvpdojo.bukkit.util.NMSUtils;

public class WitchDamageAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "WitchDamageAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        return true;
    }

    @EventHandler
    public void onSelect(DojoSelectKitEvent e) {
        AbilityEntity activator = e.getEntity();
        LivingEntity living = activator.getEntity();
        if (activator.getAbilityFromExecutor(getExecutorName()) == null)
            return;
        Ability ability = getAbility(living);

        final float interval = ability.getAttribute("interval");
        new AbilityRunnable(ability) {
            public void run() {
                if (!canAbilityExecute(living))
                    return;
                Location ploc = living.getEyeLocation();
                spawnPotion(new Location(living.getWorld(), ploc.getX() + 2, ploc.getY() + 2, ploc.getZ()), ability);
                spawnPotion(new Location(living.getWorld(), ploc.getX() + 2, ploc.getY() + 2, ploc.getZ() + 2), ability);
                spawnPotion(new Location(living.getWorld(), ploc.getX(), ploc.getY() + 2, ploc.getZ() + 2), ability);
                spawnPotion(new Location(living.getWorld(), ploc.getX() - 2, ploc.getY() + 2, ploc.getZ()), ability);
                spawnPotion(new Location(living.getWorld(), ploc.getX() - 2, ploc.getY() + 2, ploc.getZ() - 2), ability);
                spawnPotion(new Location(living.getWorld(), ploc.getX(), ploc.getY() + 2, ploc.getZ() - 2), ability);
            }
        }.createTimer(20, (int) interval * 20, -1);
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (e.getEntity().hasMetadata(getExecutorName())) {
            e.setCancelled(true);
            e.getEntity().removeMetadata(getExecutorName(), PvPDojo.get());
        }
    }

    @EventHandler
    public void onPotionSplash(PotionSplashEvent e) {
        ThrownPotion potion = e.getPotion();
        if (potion.getShooter() == null)
            return;
        LivingEntity living = (LivingEntity) potion.getShooter();
        if (living instanceof Player) {
            if (!((Player) living).isOnline()) {
                return;
            }
        }
        AbilityEntity activator = AbilityServer.get().getEntity(living);
        if (canAbilityExecute(living)) {
            Ability ability = getAbility(living);
            if (ability.getMetaData().hasEntity(potion)) {
                float damage = ability.getAttribute("ability_damage");
                for (LivingEntity near : e.getAffectedEntities()) {
                    if (near.equals(living))
                        continue;
                    if (ability.getMetaData().hasData("" + near.getEntityId()))
                        continue;
                    ability.getMetaData().addData("" + near.getEntityId(), "", 4);
                    AbilityEntity nearby = AbilityServer.get().getEntity(near);
                    NMSUtils.applyKnockback(near, potion);
                    activator.abilityDamage(nearby, damage, ability, DamageCause.MAGIC);
                }
                e.getAffectedEntities().forEach(entity -> e.setIntensity(entity, 0D));
            }
        }
    }

    private void spawnPotion(Location location, Ability ability) {
        ThrownPotion potion = (ThrownPotion) location.getWorld().spawnEntity(location, EntityType.SPLASH_POTION);
        potion.setShooter(ability.getAbilityEntity().getEntity());
        ability.getMetaData().addEntity(potion, 30, true);
        Potion pot = new Potion(PotionType.INSTANT_DAMAGE, 1);
        pot.setSplash(true);
        potion.setItem(pot.toItemStack(1));
    }
}
