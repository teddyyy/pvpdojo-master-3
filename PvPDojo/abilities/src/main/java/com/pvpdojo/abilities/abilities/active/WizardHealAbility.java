/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.entity.Snowball;
import org.bukkit.event.Event;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.ProjectileEntity;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.util.bukkit.ParticleEffects;

public class WizardHealAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "WizardHealAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        AbilityEntity activator = ability.getAbilityEntity();
        final float heal = ability.getAttribute("heal");
        Snowball snowball = activator.getEntity().launchProjectile(Snowball.class);
        snowball.setVelocity(snowball.getVelocity().multiply(1.2));
        ProjectileEntity projectileEntity = new ProjectileEntity(activator);
        projectileEntity.startProjectileCollision(snowball);
        projectileEntity.setProjectileDetection(target -> {
            target.heal(heal);
            ParticleEffects.HEART.sendToLocation(target.getEntity().getEyeLocation().add(0, .5, 0), 0, 0, 0, 1, 1);
        });

        return true;
    }

}
