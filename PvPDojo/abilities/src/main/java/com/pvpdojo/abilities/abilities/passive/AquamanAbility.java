/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.Material;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.userdata.User;

public class AquamanAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "AquamanAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        if (event instanceof PlayerToggleSneakEvent) {
            Material type = ((PlayerToggleSneakEvent) event).getPlayer().getLocation().getBlock().getType();
            if (type == Material.WATER || type == Material.STATIONARY_WATER) {
                ((PlayerToggleSneakEvent) event).getPlayer().setVelocity(((PlayerToggleSneakEvent) event).getPlayer().getEyeLocation().getDirection().multiply(2).setY(2));
                User.getUser(((PlayerToggleSneakEvent) event).getPlayer()).cancelNextFall();
                return true;
            }
        } else if (event instanceof PlayerMoveEvent) {
            if (((PlayerMoveEvent) event).getTo().getBlock().getType() == Material.WATER || ((PlayerMoveEvent) event).getTo().getBlock().getType() == Material.STATIONARY_WATER) {
                ((PlayerMoveEvent) event).getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 5 * 20, 0));
            }
        }

        return false;
    }

    @EventHandler
    public void onAquamanMove(PlayerMoveEvent e) {
        if (!e.getFrom().getBlock().equals(e.getTo().getBlock()) && canAbilityExecute(e.getPlayer())) {
            getAbility(e.getPlayer()).activateAbility(e);
        }
    }

    @EventHandler
    public void onAquaman(PlayerToggleSneakEvent e) {
        if (canAbilityExecute(e.getPlayer())) {
            getAbility(e.getPlayer()).activateAbility(e);
        }
    }
}
