/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.DeveloperSettings;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.bukkit.util.GuiItem;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class CaptainAmericaAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "CaptainAmericaAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        if (ability.getAbilityEntity() instanceof AbilityPlayer && !(event instanceof PlayerInteractEvent)) {
            ((AbilityPlayer) ability.getAbilityEntity()).getPlayer().sendMessage(CC.RED + "You can only use this by holding an item");
        }
        return false;
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (!(e.getEntity() instanceof LivingEntity) || !canAbilityExecute((LivingEntity) e.getEntity()))
            return;
        Ability ability = getAbility((LivingEntity) e.getEntity());
        ItemStack item = ((LivingEntity) e.getEntity()).getEquipment().getItemInHand();
        if (GuiItem.getDisplayNameStripped(item).equalsIgnoreCase("CaptainAmerica Ability")) {
            if (ability.getCooldown().isCoolingDown()) {
                ability.getAbilityEntity().sendMessage(MessageKeys.ABILITY_COOLDOWN, "{time}", ability.getCooldown().getMSLeft() / 1000 + "",
                        "{ability}", ability.getLoader().getRarity().getColor() + ability.getAbility());
                return;
            }
            if (PlayerUtil.isRealHit((LivingEntity) e.getEntity())) {
                ability.activateAbility(false, e);
            }
            e.getEntity().getWorld().playSound(e.getEntity().getLocation(), Sound.ITEM_BREAK, 1, -1);
            final float damageReflectionMultiplier = ability.getAttribute("ability_damage");
            if (damageReflectionMultiplier > 0) {
                LivingEntity damager = BukkitUtil.getSourceDamager(e.getDamager());
                if (e.getEntity() instanceof LivingEntity && damager != null) {
                    AbilityServer.get().getEntity((LivingEntity) e.getEntity()).abilityDamage(AbilityServer.get().getEntity(damager),
                            (float) (e.getFinalDamage() * damageReflectionMultiplier * DeveloperSettings.HEALTH_CONVERTER), ability, DamageCause.THORNS);
                }
            }
            e.setDamage(0);
        }
    }

}