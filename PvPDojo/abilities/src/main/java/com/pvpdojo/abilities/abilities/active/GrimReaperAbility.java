package com.pvpdojo.abilities.abilities.active;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftSkeleton;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Skeleton.SkeletonType;
import org.bukkit.entity.Zombie;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimaps;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.lang.MessageKeys;

import net.minecraft.server.v1_7_R4.EntitySkeleton;
import net.minecraft.server.v1_7_R4.GenericAttributes;

public class GrimReaperAbility extends AbilityExecutor {

    private ListMultimap<UUID, UUID> souls = Multimaps.newListMultimap(new HashMap<>(), ArrayList::new);

    @Override
    public String getExecutorName() {
        return "GrimReaperAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        AbilityEntity activator = ability.getAbilityEntity();
        if (souls.containsKey(activator.getEntity().getUniqueId())) {
            List<UUID> list = souls.get(activator.getEntity().getUniqueId());
            if (list.isEmpty()) {
                activator.sendMessage(MessageKeys.GRIMREAPER_NO_SOULS);
                return false;
            } else {
                for (int i = 0; i < list.size(); i++) {
                    EntityType type = PvPDojo.RANDOM.nextBoolean() ? EntityType.ZOMBIE : EntityType.SKELETON;

                    Monster monster = (Monster) activator.getEntity().getWorld().spawnEntity(activator.getEntity().getLocation(), type);
                    monster.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 252525, 2));
                    monster.setMaxHealth(10);
                    monster.setHealth(10);
                    monster.setCustomName(
                            (activator.getEntity() instanceof Player ? ((Player) activator.getEntity()).getNick()
                                    : "Grim Reaper") + "'s Minion");

                    if (monster instanceof Zombie)
                        ((Zombie) monster).setBaby(false);

                    if (monster instanceof Skeleton) {
                        ((Skeleton) monster).setSkeletonType(SkeletonType.NORMAL);

                        EntitySkeleton eS = ((CraftSkeleton) monster).getHandle();
                        // Taken from EntityZombie
                        eS.getAttributeInstance(GenericAttributes.b).setValue(40.0D);
                        eS.getAttributeInstance(GenericAttributes.d).setValue(0.23000000417232513D);
                        eS.getAttributeInstance(GenericAttributes.e).setValue(3.0D);
                    }

                    if (monster.getVehicle() != null)
                        monster.getVehicle().remove();

                    EntityEquipment eq = monster.getEquipment();

                    eq.setHelmet(new ItemStack(Material.CHAINMAIL_HELMET));
                    eq.setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
                    eq.setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
                    eq.setBoots(new ItemStack(Material.GOLD_BOOTS));
                    eq.setItemInHand(new ItemStack(Material.STONE_SWORD));

                    eq.setHelmetDropChance(0);
                    eq.setChestplateDropChance(0);
                    eq.setLeggingsDropChance(0);
                    eq.setBootsDropChance(0);
                    eq.setItemInHandDropChance(0);
                    ability.getMetaData().addEntity(monster, 120, true);
                    new AbilityRunnable(ability) {// add a little bit of range

                        @Override
                        public void run() {
                            if (monster.isDead())
                                this.cancel();

                            AbilityServer.get().getNearbyEntities(activator, monster.getLocation(), 3.0, 1.5)
                                         .stream().filter(target -> !(target instanceof Monster)).findAny().ifPresent(target -> {
                                if (target.getEntity().getCustomName() != null
                                        && monster.getCustomName() != null) {
                                    if (target.getEntity().getCustomName()
                                              .equalsIgnoreCase(monster.getCustomName())) {
                                        return;
                                    }
                                }
                                activator.abilityDamage(target, 75, ability, DamageCause.ENTITY_ATTACK);
                                NMSUtils.applyKnockback(target.getEntity(), monster);
                            });
                        }
                    }.createTimer(2, -1);

                }
                activator.sendMessage(MessageKeys.GRIMREAPER_RELEASE);
                souls.removeAll(activator.getEntity().getUniqueId());
            }
        } else {
            activator.sendMessage(MessageKeys.GRIMREAPER_NO_SOULS);
            return false;
        }

        return true;
    }

    @EventHandler
    public void onKillingPlayer(PlayerDeathEvent e) {
        if (e.getEntity() != null) {
            if (e.getEntity().getKiller() != null) {
                Player entityKiller = e.getEntity().getKiller();
                if (canAbilityExecute(entityKiller)) {
                    Player entity = e.getEntity();
                    AbilityEntity killer = AbilityEntity.get(entityKiller);
                    float maxSouls = getAbility(entityKiller).getAttribute("max_souls");

                    if (souls.get(entityKiller.getUniqueId()).size() > maxSouls) {
                        killer.sendMessage(MessageKeys.GRIMREAPER_MAXSOULS, "{max}", maxSouls + "");
                        return;
                    }

                    // Abusers Restriction
                    if (souls.get(entityKiller.getUniqueId()).stream().filter(entity.getUniqueId()::equals).count() >= 3) {
                        killer.sendMessage(MessageKeys.GRIMREAPER_CANT_CAPTURE);
                        return;
                    }

                    souls.put(entityKiller.getUniqueId(), entity.getUniqueId());
                    killer.sendMessage(MessageKeys.GRIMREAPER_CAPTURE, "{name}", entity.getNick());
                }

            }
        }
    }

}
