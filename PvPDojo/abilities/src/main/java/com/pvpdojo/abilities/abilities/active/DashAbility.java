/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.effects.ParticleEffectRotation;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.gamemode.BurnEffect;
import com.pvpdojo.util.bukkit.ParticleEffects;

public class DashAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "DashAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        final float damage = ability.getAttribute("ability_damage");
        final int distance = (int) ability.getAttribute("distance");
        final float height = ability.getAttribute("knockup_height");

        final AbilityEntity activator = ability.getAbilityEntity();
        final Player player = (Player) activator.getEntity();

        final Location loc0 = player.getLocation();
        final Vector vVect = loc0.getDirection().multiply(2);
        vVect.setY(0);

        new AbilityRunnable(ability) {
            ArrayList<AbilityEntity> hit = new ArrayList<>();

            public void run() {
                Location loc = player.getLocation();
                loc.setPitch(loc0.getPitch());
                loc.setYaw(loc0.getYaw());

                player.setVelocity(vVect);
                createParticles(loc);

                // check if hits player
                List<AbilityEntity> nearby = AbilityServer.get().getNearbyEntities(activator, 3);

                for (AbilityEntity target : nearby) {

                    if (!hit.contains(target)) {
                        if (activator.abilityDamage(target, damage, ability, DamageCause.MAGIC)) {
                            target.applyEffect(new BurnEffect(5 * 20));
                            target.getEntity().setFallDistance(-50);
                            hit.add(target);
                            target.getEntity().setVelocity(new Vector(0, height, 0));
                            ParticleEffects.LARGE_EXPLODE.sendToLocation(loc, 0, 0, 0, 0, 1);
                            loc.getWorld().playSound(loc, Sound.EXPLODE, .3f, 1);
                        }
                    }
                }
            }
        }.createTimer(2, distance);

        return true;
    }

    private void createParticles(Location loc) {
        double r = 0.75;

        for (double i = 0; i < 2 * Math.PI; i = i + Math.PI / 12) {
            double x = r * Math.cos(i);
            double y = r * Math.sin(i);
            double z = 1.5;
            Vector u = new Vector(x, y, z);
            Vector rotatedPoint = ParticleEffectRotation.rotateFunction(u, loc);
            loc.add(rotatedPoint);
            loc.add(0, 1, 0);
            ParticleEffects.FIRE.sendToLocation(loc, 0, 0, 0, 0, 1);
            loc.subtract(rotatedPoint);
            loc.subtract(0, 1, 0);
        }
    }

}