/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerPreDeathEvent;
import org.bukkit.util.Vector;

import com.pvpdojo.DeveloperSettings;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.util.bukkit.ParticleEffects;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class JesusAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "JesusAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        AbilityPlayer player = (AbilityPlayer) ability.getAbilityEntity();
        float soup = ability.getAttribute("soup");
        float recraft = ability.getAttribute("recraft");
        float health = ability.getAttribute("health");
        float secondComing = ability.getAttribute("second_coming");
        if (secondComing == 0)
            secondComing = 1000000;
        if (this.canAbilityExecute(player.getPlayer())) {
            if (!ability.getMetaData().hasData("jesus")) {
                ability.getMetaData().addData("jesus", "", secondComing);
                player.setInvulnerable(60);
                player.setHealth(health);
                switch (player.getPlayStyle()) {
                    case POTTER:
                        if (soup > 0)
                            PlayerUtil.refillHealPotions(player.getPlayer(), (int) ((int) soup / 1.5D));
                        break;
                    case SOUP:
                        if (soup > 0)
                            PlayerUtil.refillSoup(player.getPlayer(), (int) soup);
                        if (recraft > 0)
                            PlayerUtil.addRecraft(player.getPlayer(), (int) recraft);
                        break;
                    case SURVIVALIST:
                        player.setHealth(player.getHealth() + (soup + recraft) * 3.5 * DeveloperSettings.HEALTH_CONVERTER);
                        break;
                    default:
                        break;

                }

                createEffect(ability, player.getPlayer());
                if (secondComing != 1000000) {
                    player.sendMessage(MessageKeys.JESUS_RESURRECT, "{time}", secondComing + "");
                }

                return true;
            }
        }
        return false;
    }

    private void createEffect(Ability ability, final Player player) {
        new AbilityRunnable(ability) {
            double t = 0;
            double tmax = 6 * Math.PI; // (3.6 secs)

            public void run() {
                final Location loc = player.getLocation();
                final Location loc2 = player.getLocation();
                player.setVelocity(new Vector(0, t / 60, 0));
                t += Math.PI / 12;
                double r = 0.5 + 0.07 * (Math.pow((tmax - t), 1.5));
                for (double theta = 0; theta <= Math.PI / 3; theta += Math.PI / 8) {
                    double x = r * Math.cos(t + theta);
                    double y = 1.5;
                    double z = r * Math.sin(t + theta);
                    loc.add(x, y, z);
                    ParticleEffects.FIREWORK_SPARK.sendToLocation(loc, 0, 0, 0, 0, 1);
                    loc.subtract(x, y, z);
                    theta -= Math.PI;
                    x = r * Math.cos(t + theta);
                    y = 1.5;
                    z = r * Math.sin(t + theta);
                    loc2.add(x, y, z);
                    ParticleEffects.FIREWORK_SPARK.sendToLocation(loc2, 0, 0, 0, 0, 1);
                    loc2.subtract(x, y, z);
                    theta += Math.PI;
                }

                if (t > tmax) {
                    player.setVelocity(new Vector(0, .5, 0));
                    ParticleEffects.FIREWORK_SPARK.sendToLocation((player.getLocation()), 0, 0, 0, 2, 100);
                    ParticleEffects.CLOUD.sendToLocation(player.getEyeLocation(), 0F, 0F, 0F, 0.45F, 50);
                    player.setFallDistance(-15);
                    this.cancel();
                }
            }
        }.createTimer(1, -1);

    }

    @EventHandler
    public void onPreDeath(PlayerPreDeathEvent event) {
        if (canAbilityExecute(event.getPlayer())) {
            event.setCancelled(activateAbility(getAbility(event.getPlayer()), event));
        }
    }
}