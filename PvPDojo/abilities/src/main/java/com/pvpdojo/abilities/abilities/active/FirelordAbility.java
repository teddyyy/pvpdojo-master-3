/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Item;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.effects.ParticleEffectRotation;
import com.pvpdojo.abilities.abilities.effects.PrisonerEffect;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.abilities.lib.ProjectileEntity;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.gamemode.BurnEffect;
import com.pvpdojo.bukkit.util.TempBlock;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ParticleEffects;

public class FirelordAbility extends AbilityExecutor {

	@Override
	public String getExecutorName() {
		return "FirelordAbility";
	}

	@Override
	public boolean activateAbility(final Ability ability, Event event) {
		final AbilityEntity activator = ability.getAbilityEntity();

		final int uses = ability.getCooldown().getUsesLeft();

		final float fire_time = ability.getAttribute("fire_duration");
		final float beam_damage = ability.getAttribute("beam_damage");
		final float cage_time = ability.getAttribute("case_duration");

		if (uses == 3) {
			ProjectileEntity projectile = new ProjectileEntity(activator, false, 1f);

			Item item = activator.getEntity().getWorld().dropItem(activator.getEntity().getEyeLocation(),
					new ItemStack(Material.FIREBALL));

			item.setVelocity(activator.getEntity().getEyeLocation().getDirection().multiply(1.5));

			projectile.setProjectileDetection(target -> target.applyEffect(new BurnEffect((int) (20 * fire_time))));

			projectile.startItemCollision(item, 1, 1000, true);
		} else if (uses == 2) {

			activator.getEntity().getWorld().playSound(activator.getEntity().getLocation(), Sound.BLAZE_BREATH, 0.5f,
					0.6f);
			new AbilityRunnable(ability) {// we can use ProjectEntity#setProjectileHit method

				double t = 0;
				double tmax = 10 * Math.PI;
				double r = 0.9;
				Location startLoc = activator.getEntity().getEyeLocation();
				Location entityLoc = null;// This is needed when the beam hits the player, so the fire field creates
											// underneath the player

				@Override
				public void run() {// we are doing 3 effects, first side helix, second side helix, and an invisible
									// beam to detect entities/blocks
					t += Math.PI / 5;
					for (double i = 0; i < r * 3; i += 0.4) {

						double x = r * Math.cos(t + i) * (-1);
						double y = r * Math.sin(t + i) * (-1);
						double z = t;
						Vector v = new Vector(x, y, z);
						
						v = ParticleEffectRotation.rotateFunction(v, startLoc);
						
						startLoc.add(v.getX(), v.getY(), v.getZ());
						ParticleEffects.FIRE.sendToLocation(startLoc, 0, 0, 0, 0, 1);
						ParticleEffects.LAVA_SPARK.sendToLocation(startLoc, 0, 0, 0, 0, 1);
						startLoc.subtract(v.getX(), v.getY(), v.getZ());

						Vector v1 = new Vector(-x, -y, z);
						v1 = ParticleEffectRotation.rotateFunction(v1, startLoc);
						
						startLoc.add(v1.getX(), v1.getY(), v1.getZ());
						ParticleEffects.FIRE.sendToLocation(startLoc, 0, 0, 0, 0, 1);
						startLoc.subtract(v1.getX(), v1.getY(), v1.getZ());

						double c_x = startLoc.getDirection().getX() * t;
						double c_y = startLoc.getDirection().getY() * t;
						double c_z = startLoc.getDirection().getZ() * t;
						
						startLoc.add(c_x, c_y, c_z);
					
						if (startLoc.getBlock().getType() != Material.AIR) {
							playEffect();
							this.cancel();
							return;
						}
						
						for (AbilityEntity target : AbilityServer.get().getNearbyEntities(activator, startLoc, 3, 3)) {
							activator.abilityDamage(target, beam_damage, ability, DamageCause.ENTITY_ATTACK);
							entityLoc = target.getEntity().getLocation();
							playEffect();
							this.cancel();
							return;
						}
						
						startLoc.subtract(c_x, c_y, c_z);
						
						if (t > tmax) {
							this.cancel();
						}
						
					}
				}

				void playEffect() {
					this.cancel();
					int radius = 3;
					startLoc.getWorld().playSound(startLoc, Sound.FIREWORK_LARGE_BLAST2, 4, 4);
					ParticleEffects.LARGE_EXPLODE.sendToLocation(startLoc, 0, 0, 0, 0, 1);

					Location selectedLoc = entityLoc == null ? startLoc : entityLoc;
					
					for (int x = -radius; x < radius; x++) {
						for (int y = -radius; y < radius; y++) {
							for (int z = -radius; z < radius; z++) {
								Location loc = selectedLoc.clone().add(x, y, z);
								Material below = loc.getBlock().getRelative(BlockFace.DOWN).getType();
								if (loc.getBlock().getType().equals(Material.FIRE) || !below.isSolid())
									continue;
								TempBlock.createTempBlock(loc, Material.FIRE, (byte) 0, 5);
							}
						}
					}
					this.cancel();

				}
			}.createTimer(1, -1);

		} else if (uses == 1) {
			ProjectileEntity projectile = new ProjectileEntity(activator, false, 1f);

			Item item = activator.getEntity().getWorld().dropItem(activator.getEntity().getEyeLocation(),
					new ItemStack(Material.GLASS));
			item.setVelocity(activator.getEntity().getEyeLocation().getDirection().multiply(1.5));

			projectile.setProjectileDetection(target -> {

				boolean antiPrisoner = target.hasAbility(AbilityLoader.getAbilityData(79).getName());

				if (antiPrisoner) { // Anti Prisoner
					activator.sendMessage(MessageKeys.GENERIC, "{text}", CC.RED + "Bad luck you hit an Anti Prisoner");
				}
				target.getEntity().eject();

				Location loc = target.getEntity().getLocation().add(0, 2, 0);
				Location newLoc = BlockUtil.findLocationBetweenPoints(loc, target.getEntity().getLocation());
				target.getEntity().teleport(newLoc);
				activator.getEntity().teleport(newLoc);
				new PrisonerEffect(loc, activator, target, (int) (antiPrisoner ? cage_time / 2 : cage_time));

				activator.getEntity().teleport(target.getEntity().getLocation());// tests

			});

			projectile.startItemCollision(item, 1, 1000, true);
		}

		return true;
	}
	

}
