/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.command;

import java.util.UUID;

import org.bukkit.command.CommandSender;

import com.pvpdojo.Discord;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Single;

@CommandAlias("giveability")
public class GiveAbilityCommand extends DojoCommand {

    @CatchUnknown
    @Default
    @CommandCompletion("@abilities")
    public void onGiveAbility(CommandSender sender, @Single String target, AbilityLoader loader) {
        PvPDojo.schedule(() -> {
            DojoOfflinePlayer targetPlayer = PvPDojo.getOfflinePlayer(target);

            if (!targetPlayer.hasPlayedBefore()) {
                PvPDojo.LANG.sendMessage(sender, MessageKeys.PLAYER_NEVER_JOINED);
                return;
            }

            targetPlayer.getPersistentData().pullUserDataSneaky();
            UUID collectionUUID = targetPlayer.getPersistentData().addCollection(-1);
            targetPlayer.getPersistentData().updateAbility(collectionUUID, loader.getId());

            sender.sendMessage(CC.GREEN + "Successfully given " + targetPlayer.getName() + " " + loader.getDisplayName());
            if (!PvPDojo.get().isTest()) {
                Discord.text().sendMessage(sender.getName() + " gave " + targetPlayer.getName() + " " + loader.getName()).complete();
            }
        }).createAsyncTask();
    }

    @Override
    public Rank getRank() {
        return Rank.SERVER_ADMINISTRATOR;
    }
}
