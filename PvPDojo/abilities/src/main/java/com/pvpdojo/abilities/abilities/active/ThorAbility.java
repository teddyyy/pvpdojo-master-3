/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.FixedMetadataValue;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.lang.MessageKeys;

public class ThorAbility extends AbilityExecutor {

    public static final String THOR_META = "thor_meta";

    @Override
    public String getExecutorName() {
        return "ThorAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        if (!(event instanceof PlayerInteractEvent)) {
            ability.getAbilityEntity().sendMessage(MessageKeys.GENERIC, "{text}", "Cannot use with sneak activation");
            return false;
        }

        PlayerInteractEvent interactEvent = (PlayerInteractEvent) event;

        if (interactEvent.getClickedBlock() != null) {
            interactEvent.getPlayer().getWorld().strikeLightning(interactEvent.getClickedBlock().getLocation().add(0, 1, 0))
                         .setMetadata(THOR_META, new FixedMetadataValue(PvPDojo.get(), interactEvent.getPlayer().getName()));
            return true;
        }

        return false;
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent e) {
        if (e.getDamager().hasMetadata(THOR_META) && e.getEntity() instanceof Player) {
            if (e.getDamager().getMetadata(THOR_META).get(0).asString().equals(((Player) e.getEntity()).getName())) {
                e.setDamage(0D);
            } else {
                e.getEntity().setFireTicks(6 * 20);
            }
        }
    }

}
