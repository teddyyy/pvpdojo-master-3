package com.pvpdojo.abilities.abilities.active;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftFirework;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Firework;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.metadata.FixedMetadataValue;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.bukkit.events.EntityFallEvent;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.util.bukkit.ParticleEffects;

public class KompetenzAbility extends AbilityExecutor {

	@Override
	public String getExecutorName() {
		return "KompetenzAbility";
	}

	private static final String KEY = "parabolic_fall";

	@Override
	public boolean activateAbility(Ability ability, Event event) {
		LivingEntity activator = ability.getAbilityEntity().getEntity();
		if (activator.getVehicle() == null) {

			Location loc = activator.getLocation();
			Block block = loc.getBlock();

			// = DO NOT CHANGE THOSE VALUES
			for (int y = 0; y <= 26; y++) {
				if (y <= 15)
					block = block.getRelative(BlockFace.UP);
				else
					block = block.getRelative(BlockFace.DOWN);
				Location direcLoc = block.getLocation()
						.add(activator.getLocation().getDirection().normalize().multiply(y));
				if (direcLoc.getBlock().getType() != Material.AIR) {
					ability.getAbilityEntity().sendMessage(MessageKeys.KOMPETENZ_ARC_IMPOSSIBLE);
					return false;
				}
			}

			EnderPearl projectile = activator.launchProjectile(EnderPearl.class);
			projectile.setBounce(true);
			projectile.setShooter(activator);
			projectile.setVelocity(activator.getLocation().getDirection().setY(1.65).multiply(0.6));// = DO NOT CHANGE THOSE VALUES															// THOSE VALUES
			projectile.setPassenger(activator);
			activator.setMetadata(KEY, new FixedMetadataValue(PvPDojo.get(), projectile));
			displayEffect(activator, ability, true);

			new AbilityRunnable(ability) {

				@Override
				public void run() {
					if (projectile.isOnGround() || projectile.isDead() || activator.isDead()) {
						this.cancel();
					}
					displayEffect(activator, ability, false);

					Firework firework = projectile.getLocation().getWorld().spawn(projectile.getLocation(),
							Firework.class);
					FireworkMeta fireworkMeta = firework.getFireworkMeta();
					fireworkMeta.addEffect(FireworkEffect.builder().flicker(true).with(FireworkEffect.Type.BALL)
							.withColor(Color.AQUA).withFade(Color.RED).trail(true).build());
					fireworkMeta.setPower(0);
					firework.setFireworkMeta(fireworkMeta);
					((CraftFirework) firework).getHandle().expectedLifespan = 1;
				}
			}.createTimer(6, -1);
			new AbilityRunnable(ability) {
				// A new runnable that runs every 1 tick to check everything correctly without
				// letting the passenger bypassing and trying to make a bug.
				int x = 0;
				boolean disAllowLeaving = true;

				public void run() {
					if (projectile.isOnGround() || projectile.isDead() || activator.isDead()) {
						this.cancel();
					}

					if (++x >= 20) {// they can eject while mid-air
						disAllowLeaving = false;
					}

					if (disAllowLeaving)
						projectile.setPassenger(activator);

				}
			}.createTimer(1, -1);
			return true;
		}
		return false;
	}

	@EventHandler
	public void onTeleport(PlayerTeleportEvent e) {
		if (e.getCause() == TeleportCause.ENDER_PEARL) {
			Player player = e.getPlayer();
			if (canAbilityExecute(player)) {
				if (player.hasMetadata(KEY)) {
					displayEffect(player, getAbility(player), true);
					player.teleport(new Location(e.getTo().getWorld(), e.getTo().getX(), e.getTo().getY() + 2,
							e.getTo().getZ(), player.getLocation().getYaw(), player.getLocation().getPitch()));
					e.setCancelled(true);
				}
			}
		}
	}

	@EventHandler
	public void onFall(EntityFallEvent e) {
		if (e.getEntity() instanceof Player) {
			Player player = (Player) e.getEntity();
			if (canAbilityExecute(player)) {
				if (player.hasMetadata(KEY)) {
					e.setCancelled(true);
					player.removeMetadata(KEY, PvPDojo.get());
				}
			}
		}
	}

	private void displayEffect(LivingEntity activator, Ability ability, boolean onGround) {
		if (onGround) {
			activator.getLocation().getWorld().strikeLightningEffect(activator.getLocation());
			ParticleEffects.LARGE_EXPLODE.sendToLocation(activator.getLocation(), 0, 0, 0, 0, 1);
		}
		float radius = ability.getAttribute("radius");
		float damage = ability.getAttribute("damage");

		for (AbilityEntity entity : AbilityServer.get().getNearbyEntities(AbilityEntity.get(activator),
				activator.getLocation(), radius, radius)) {
			entity.getEntity().setFireTicks(20 * 5);
			ability.getAbilityEntity().abilityDamage(entity, damage, ability, DamageCause.ENTITY_ATTACK);
			entity.getEntity()
					.setVelocity(entity.getEntity().getVelocity()
							.add(entity.getEntity().getLocation().toVector()
									.subtract(activator.getLocation().toVector()).normalize().setY(0.6f / 2)
									.multiply(1.5D / 2)));
		}

	}

}
