/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.Sound;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.spigotmc.event.entity.EntityDismountEvent;

import com.pvpdojo.DeveloperSettings;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.bukkit.events.EntityFallEvent;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.util.bukkit.CC;

public class StomperAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "StomperAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        return true;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onFall(EntityFallEvent e) {
        if (!(e.getEntity() instanceof LivingEntity)) {
            return;
        }
        if (!canAbilityExecute((LivingEntity) e.getEntity()))
            return;
        Ability ability = getAbility((LivingEntity) e.getEntity());
        AbilityEntity activator = ability.getAbilityEntity();
        float radius = ability.getAttribute("damage_radius");
        float stomp_damage = (float) (e.getFallDamage() * DeveloperSettings.HEALTH_CONVERTER);
        for (AbilityEntity nearby : AbilityServer.get().getNearbyEntities(activator, radius)) {
            if (nearby.hasAbility(AbilityLoader.getAbilityData(80).getName())) { // Anti Stomper
                activator.sendMessage(MessageKeys.GENERIC, "{text}", CC.RED + "Bad luck you stomped an Anti Stomper");
                // reflect the damage on the Stomper
                nearby.abilityDamage(activator, stomp_damage / 2f, nearby.getAbility(AbilityLoader.getAbilityData(80).getName()), DamageCause.FALL);
                continue;
            }
            if (nearby.getEntity() instanceof Player) {
                Player player = (Player) nearby.getEntity();
                if (player.isSneaking()) {
                    activator.abilityDamage(nearby, 4 * DeveloperSettings.HEALTH_CONVERTER, ability, DamageCause.FALL);
                    continue;
                }
            }
            activator.abilityDamage(nearby, stomp_damage, ability, DamageCause.FALL);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof LivingEntity) {
            if (e.getCause() != DamageCause.FALL || !canAbilityExecute((LivingEntity) e.getEntity())) {
                return;
            }

            if (e.getFinalDamage() > 4D) {
                e.setDamage(4D);
                e.getEntity().getWorld().playSound(e.getEntity().getLocation(), Sound.ANVIL_LAND, 1, 1);
            }
        }
    }

    @EventHandler
    public void onVehicleExit(EntityDismountEvent e) {
        if (e.getEntity() instanceof EnderPearl && e.getDismounted() instanceof LivingEntity && canAbilityExecute((LivingEntity) e.getDismounted())) {
            e.setCancelled(true);
        }
    }

}