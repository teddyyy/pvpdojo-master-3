/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Location;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityCrosshair;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityEffect.AbilityEffectType;
import com.pvpdojo.abilities.gamemode.RunningAbilityEffect;
import com.pvpdojo.util.bukkit.ParticleEffects;
import com.pvpdojo.util.VectorUtil;

public class SoulStealAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "SoulStealAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        final AbilityEntity activator = ability.getAbilityEntity();
        final float total_damage = ability.getAttribute("ability_damage");
        final float heal_percent = ability.getAttribute("heal_percent");
        final float duration = ability.getAttribute("duration");
        final int ticks = (int) (duration * 4);
        final Location loc1 = activator.getEntity().getEyeLocation();
        AbilityCrosshair crosshair = new AbilityCrosshair(activator) {
            @Override
            public void targetFound(final AbilityEntity target, double accuracy, int distance) {
                final Location loc2 = target.getEntity().getEyeLocation();
                AbilityRunnable run = new AbilityRunnable(ability) {
                    double increment = 0;

                    @Override
                    public void run() {
                        if (!target.isTargetable() || !activator.isTargetable()) {
                            return;
                        }
                        increment++;
                        target.getEntity().setVelocity(new Vector(0, .20, 0));
                        Location point = VectorUtil.getPointOnLine(loc2, loc1, increment / 10);
                        ParticleEffects.HEART.sendToLocation(point, 0, 0, 0, 1, 1);
                        activator.abilityDamage(target, total_damage / ticks, ability, DamageCause.MAGIC);
                        activator.heal((total_damage / ticks) * (heal_percent / 100));
                    }
                };
                target.applyEffect(new RunningAbilityEffect(run, AbilityEffectType.SOULSTEAL, ticks * 5));
                run.createTimer(5, ticks);
            }
        };
        return crosshair.calculate(1.5, 10, true);
    }

}