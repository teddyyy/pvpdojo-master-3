package com.pvpdojo.abilities.listeners;

import com.pvpdojo.bukkit.listeners.DojoListener;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPhysicsEvent;

import java.util.EnumSet;

public class BlockListener implements DojoListener {

    private static final EnumSet<Material> BLOCKS = EnumSet.of(Material.RED_MUSHROOM, Material.BROWN_MUSHROOM,
            Material.WATER, Material.STATIONARY_WATER, Material.LAVA, Material.STATIONARY_LAVA);

    @EventHandler
    public void onBlockPhysics(BlockPhysicsEvent e) {
        if (BLOCKS.contains(e.getBlock().getType())) {
            e.setCancelled(true);
        }
    }

}
