/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.listeners;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerStartItemConsumeEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.userdata.PlayStyle;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class AbilityConsumeItemListener implements DojoListener {

    @EventHandler
    public void onConsume(PlayerItemConsumeEvent e) {
        AbilityPlayer cp = AbilityPlayer.get(e.getPlayer());

        if (cp.getPlayStyle() == PlayStyle.SURVIVALIST && e.getItem().getType() == Material.GOLDEN_APPLE) {
            if (e.getItem().hasItemMeta() && e.getItem().getItemMeta().hasDisplayName()
                    && e.getItem().getItemMeta().getDisplayName().equals(ItemBuilder.GOLDEN_HEAD.build().getItemMeta().getDisplayName())) {
                e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 20 * 120, 1), true);
                e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 3 * 20, 6), true);
            } else {
                e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 20 * 120, 0), true);
                e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 30, 6), true);
            }
        }
    }

    @EventHandler
    public void onStartConsume(PlayerStartItemConsumeEvent e) {
        AbilityPlayer cp = AbilityPlayer.get(e.getPlayer());

        if (cp.getPlayStyle() == PlayStyle.SURVIVALIST && e.getItem().getType() == Material.GOLDEN_APPLE) {
            e.setConsumeTicks(24);
        }
    }

}
