/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.gamemode;

import com.pvpdojo.abilities.base.entities.AbilityEntity;

public class BurnEffect extends AbilityEffect {

    public BurnEffect(int duration) {
        super(AbilityEffectType.BURN, duration);
    }

    @Override
    public void applyEffect(AbilityEntity entity) {
        entity.getEntity().setFireTicks(getTickDuration());
    }

    @Override
    public void endEffect(AbilityEntity entity) {
        entity.getEntity().setFireTicks(0);
    }

}
