/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerQuitEvent;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.bukkit.listeners.DojoListener;

public class AbilityLeaveListener implements DojoListener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onQuitMonitor(PlayerQuitEvent e) {
        AbilityPlayer.get(e.getPlayer()).die();
        PvPDojo.schedule(() -> {
            if (Bukkit.getPlayer(e.getPlayer().getUniqueId()) == null) AbilityServer.get().removeEntity(AbilityPlayer.get(e.getPlayer()));
        }).nextTick();
    }

}
