/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.effects.SmokeEffect;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.util.bukkit.ItemUtil;

public class SmokeGrenadeAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "SmokeGrenadeAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        LivingEntity entity = ability.getAbilityEntity().getEntity();

        int radius = (int) ability.getAttribute("radius");
        int duration = (int) ability.getAttribute("duration");

        PvPDojo.schedule(() -> entity.getWorld().playSound(entity.getLocation(), Sound.CHICKEN_EGG_POP, 2, 1)).createTask(4);

        Item item = ItemUtil.throwItem(entity, ability.getClickItem(), false, true);

        PvPDojo.schedule(() -> {

            Location location = item.getLocation();
            new SmokeEffect(radius, duration * 20, 20, 50, location).start();
            item.remove();

        }).createTask(20);

        return true;
    }


}
