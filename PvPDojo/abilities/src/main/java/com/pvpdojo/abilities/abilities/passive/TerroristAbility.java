/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;

public class TerroristAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "TerroristAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        return true;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerDeath(PlayerDeathEvent e) {
        Player player = e.getEntity();
        if (canAbilityExecute(player)) {
            Ability ability = getAbility(player);
            AbilityEntity activator = ability.getAbilityEntity();

            player.getWorld().playEffect(player.getLocation(), Effect.EXPLOSION_HUGE, 1);
            player.getWorld().playSound(player.getLocation(), Sound.EXPLODE, 1, 1);

            float damage = ability.getAttribute("ability_damage");
            float radius = ability.getAttribute("damage_radius");
            for (AbilityEntity nearby : AbilityServer.get().getNearbyEntities(activator, radius)) {
                if (!nearby.equals(activator))
                    nearby.recieveAbilityDamage(damage, activator, ability, DamageCause.ENTITY_EXPLOSION);
            }
        }
    }

}
