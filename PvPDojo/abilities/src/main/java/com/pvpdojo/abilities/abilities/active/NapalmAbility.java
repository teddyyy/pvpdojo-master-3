/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.effects.NapalmEffect;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;

public class NapalmAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "NapalmAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        AbilityEntity activator = ability.getAbilityEntity();
        Player player = (Player) activator.getEntity();
        final Location loc = player.getLocation();
        Location l = loc.add(0, 3, 0);
        Vector dir = l.getDirection().multiply(.8).add(new Vector(0, -.3, 0));
        final Fireball fire = (Fireball) loc.getWorld().spawnEntity(l, EntityType.FIREBALL);
        fire.setMetadata(getExecutorName(), new FixedMetadataValue(PvPDojo.get(), null));
        fire.setShooter(player);
        fire.setVelocity(dir.multiply(1.2));
        ability.getMetaData().addEntity(fire, 20, false);
        loc.getWorld().playSound(loc, Sound.FIREWORK_LARGE_BLAST2, 4, 4);
        return true;
    }

    @EventHandler
    public void onExplosion(ExplosionPrimeEvent e) {
        if (e.getEntity().hasMetadata(getExecutorName())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onProjectileHit(ProjectileHitEvent e) {
        if (!(e.getEntity() instanceof Fireball)) {
            return;
        }
        Projectile fireball = e.getEntity();
        LivingEntity shooter = (LivingEntity) fireball.getShooter();
        if (!canAbilityExecute(shooter)) {
            return;
        }
        Ability ability = getAbility(shooter);
        if (!ability.getMetaData().hasEntity(fireball)) {
            return;
        }
        float duration = ability.getAttribute("duration");
        float dps = ability.getAttribute("dps_damage");
        float spread = ability.getAttribute("spread_radius");
        float maxRadius = ability.getAttribute("max_radius");
        new NapalmEffect(ability, fireball.getLocation(), dps, (int) maxRadius, (int) (duration * 20), (int) spread, ability.getAttribute("follow") != 0).start();
    }

}
