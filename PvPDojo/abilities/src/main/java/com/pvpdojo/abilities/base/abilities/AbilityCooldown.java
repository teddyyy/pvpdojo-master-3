/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.base.abilities;

public class AbilityCooldown {

    long start;
    int cooldown;
    int useReset;
    int useLeft;
    long noUseTime;

    public AbilityCooldown() {
        start = 0L;
        cooldown = 0;
        useLeft = -1;
        useReset = -1;
        noUseTime = 0;
    }

    private void checkExpired() {
        if (System.currentTimeMillis() >= start + cooldown) {
            this.resetCooldown();
        }
    }

    public void resetCooldown() {
        useLeft = useReset;
        start = 0;
    }

    public int getUsesLeft() {
        return useLeft;
    }

    public boolean checkNoUse() {
        return System.currentTimeMillis() < noUseTime;
    }

    public int activate(int delay) {
        start = System.currentTimeMillis();
        useLeft--;
        noUseTime = System.currentTimeMillis() + delay;
        return useLeft;
    }

    public boolean isCoolingDown() {
        checkExpired();
        return useLeft == 0;
    }

    public long getMSLeft() {
        return (start + cooldown) - System.currentTimeMillis();
    }

    public void setUses(int uses) {
        this.useReset = uses;
    }

    public void setUsesLeft(int useLeft) {
        this.useLeft = useLeft;
    }

    public void setCooldown(int cooldown) {
        this.cooldown = cooldown;
    }

}
