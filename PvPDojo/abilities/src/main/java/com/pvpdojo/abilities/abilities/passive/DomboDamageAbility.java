/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.abilities.AbilityMetaData;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.gamemode.PoisonEffect;
import com.pvpdojo.abilityinfo.KitAttributeArray.AttributeArrayType;
import com.pvpdojo.abilityinfo.KitAttributeArray.KitAttribute;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class DomboDamageAbility extends AbilityExecutor {

    private static final String META_KEY = "dombo_damage_hits";
    private static final String META_KEY2 = "dombo_damage_object";

    @Override
    public String getExecutorName() {
        return "DomboDamageAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        return true;
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (!(e.getDamager() instanceof LivingEntity) || !(e.getEntity() instanceof LivingEntity) || !PlayerUtil.isRealHit((LivingEntity) e.getEntity()))
            return;
        LivingEntity damager = (LivingEntity) e.getDamager();
        if (canAbilityExecute(damager)) {
            Ability ability = getAbility(damager);
            AbilityMetaData meta = ability.getMetaData();
            AbilityEntity entity = AbilityServer.get().getEntity(damager);

            if (!entity.canTarget(AbilityServer.get().getEntity((LivingEntity) e.getEntity()))) {
                return;
            }

            if (!meta.hasData(META_KEY)) {
                meta.addData(META_KEY, 1);
            } else {
                meta.addData(META_KEY, (int) meta.getData(META_KEY) + 1);
            }
            int hits = meta.getData(META_KEY);
            float combo = ability.getAttribute("combo_hits");
            if (entity.getEntity().hasPotionEffect(PotionEffectType.SPEED)) {
                combo += 4;
            }
            if (hits == combo && !meta.hasData(META_KEY2)) {
                e.getEntity().getWorld().playSound(e.getEntity().getLocation(), Sound.ZOMBIE_WOODBREAK, .3f, 1.5f);
                float damage = ability.getAttribute("bonus_damage");
                KitAttribute attribute = new KitAttribute("physical_damage", damage, AttributeArrayType.BONUS_ATTRIBUTES);
                meta.addData(META_KEY2, attribute);
                entity.addAttribute(attribute);
                float poison = ability.getAttribute("combo_poison");
                if (poison > 0) {
                    AbilityEntity target = AbilityServer.get().getEntity((LivingEntity) e.getEntity());
                    target.applyEffect(new PoisonEffect(20 * 4, (int) poison));
                }

            }
        }
        
        if (canAbilityExecute((LivingEntity) e.getEntity())) {
            Ability ability = getAbility((LivingEntity) e.getEntity());
            AbilityMetaData meta = ability.getMetaData();
            AbilityEntity entity = AbilityServer.get().getEntity((LivingEntity) e.getEntity());
            if (meta.hasData(META_KEY2))
                entity.removeAttribute(meta.getData(META_KEY2));
            meta.addData(META_KEY, 0);
            meta.removeData(META_KEY2);
        }
    }
}