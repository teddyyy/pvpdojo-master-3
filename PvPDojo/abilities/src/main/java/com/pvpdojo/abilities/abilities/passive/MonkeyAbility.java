/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.util.Vector;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.userdata.User;

public class MonkeyAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "MonkeyAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        if (event instanceof PlayerToggleSneakEvent) {
            Player player = ((PlayerEvent) event).getPlayer();
            User user = User.getUser(player);

            Block block = checkforWall(player);

            if (block != null && ((PlayerToggleSneakEvent) event).isSneaking()) {
                Vector vec = player.getLocation().getDirection().multiply(2.1).setY(1.2);
                player.setVelocity(vec);
                player.getWorld().playEffect(block.getLocation(), Effect.STEP_SOUND, block.getType());
                user.cancelNextFall();
            }

            return true;
        }

        return false;
    }

    private Block checkforWall(Player item) {

        Block itemb = item.getLocation().getBlock();
        Block cur;
        BlockFace[] faces = new BlockFace[] { BlockFace.EAST, BlockFace.WEST, BlockFace.SOUTH, BlockFace.NORTH };

        for (BlockFace face : faces) {
            cur = itemb.getRelative(face);
            if (cur.getType().isSolid() || cur.getType() == Material.GLASS) {
                return cur;
            }
        }

        return null;
    }

    @EventHandler
    public void onMonkey(PlayerToggleSneakEvent e) {
        if (canAbilityExecute(e.getPlayer())) {
            getAbility(e.getPlayer()).activateAbility(e);
        }
    }

}
