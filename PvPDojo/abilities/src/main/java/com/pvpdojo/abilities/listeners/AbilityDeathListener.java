/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.listeners;

import static com.pvpdojo.util.StringUtils.handleDeathMessage;

import java.util.stream.Stream;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

import com.pvpdojo.DeveloperSettings;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.guns.Gun;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilityinfo.AbilityType;
import com.pvpdojo.achievement.Achievement;
import com.pvpdojo.achievement.AchievementManager;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.KitAbilityData;
import com.pvpdojo.userdata.KitData;
import com.pvpdojo.userdata.PlayStyle;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.PlayerUtil;
import com.pvpdojo.util.StringUtils;

public class AbilityDeathListener implements DojoListener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDeath(EntityDeathEvent e) {
        if (!(e.getEntity() instanceof Player) && AbilityServer.get().containsEntity(e.getEntity())) {
            AbilityServer.get().removeEntity(AbilityServer.get().getEntity(e.getEntity()));
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onDespawn(ChunkUnloadEvent e) {
        for (Entity ent : e.getChunk().getEntities()) {
            if (ent instanceof LivingEntity && AbilityServer.get().containsEntity((LivingEntity) ent)) {
                AbilityServer.get().removeEntity(AbilityServer.get().getEntity((LivingEntity) ent));
            }
        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        Player victim = e.getEntity();

        User user = User.getUser(victim);
        AbilityPlayer cp = AbilityPlayer.get(e.getEntity());
        KitData data = cp.getKit();

        if (data != null) {
            cp.getAbilities()
              .stream()
              .filter(d -> d.getAbilityType() == AbilityType.CLICK)
              .forEach(adata -> e.getDrops().removeIf(item -> item.hasItemMeta() && item.getItemMeta().hasDisplayName() && item.getType() == adata.getClickItem().getType()));

            switch (data.getPlayStyle()) {
                case POTTER:
                    e.getDrops().removeIf(item -> item.getType().name().contains("LEATHER_") 
                    		|| item.getType() == Material.DIAMOND_CHESTPLATE || item.getType() == Material.DIAMOND_BOOTS
                            || (item.getType() == Material.POTION && Potion.fromItemStack(item).getType() != PotionType.INSTANT_HEAL)
                            || item.getType() == Material.ENDER_PEARL);
                    break;
                case SURVIVALIST:
                    e.getDrops().removeIf(item -> item.getType() == Material.FISHING_ROD);
                    break;
                default:
                    break;
            }
        }


        if (victim.getKiller() != null && victim.getKiller().isOnline() && victim != victim.getKiller()) {
            Player killerPlayer = victim.getKiller();
            User kuser = User.getUser(killerPlayer);
            AbilityPlayer k = AbilityPlayer.get(killerPlayer);
            ItemStack[] killerContents = killerPlayer.getInventory().getContents();

            // Killer message start
            kuser.sendMessage(CC.GRAY + StringUtils.getLS());
            kuser.sendMessage(CC.GRAY + "Victim" + PvPDojo.POINTER + CC.GOLD + victim.getNick());
            int streak = kuser.getStats().getKillstreak() + 1;

            kuser.sendMessage(CC.GRAY + "Your killstreak" + PvPDojo.POINTER + CC.BLUE + streak);
            if (kuser.useStats()) {
                kuser.sendMessage(CC.GRAY + "Your reward" + PvPDojo.POINTER + CC.LIGHT_PURPLE + Math.min(5 + 5 * streak, 50) + " dojo credits");
            }
            kuser.sendMessage(CC.GRAY + StringUtils.getLS());
            // end

            victim.sendMessage(CC.GRAY + StringUtils.getLS());
            victim.sendMessage(CC.GRAY + "Killer" + PvPDojo.POINTER + CC.GOLD + killerPlayer.getNick());
            switch (k.getPlayStyle()) {
                case POTTER:
                    victim.sendMessage(CC.GRAY + "Pots" + PvPDojo.POINTER + CC.GREEN + Stream.of(killerContents).filter(item -> item != null
                            && item.getType() == Material.POTION && Potion.fromItemStack(item).getType() == PotionType.INSTANT_HEAL).count());
                    AchievementManager.inst().trigger(Achievement.PATH_OF_THE_POTTER, killerPlayer);
                    break;
                case SOUP:
                    victim.sendMessage(CC.GRAY + "Soups" + PvPDojo.POINTER + CC.GREEN + Stream.of(killerContents).filter(item -> item != null && item.getType() == Material.MUSHROOM_SOUP).count());
                    AchievementManager.inst().trigger(Achievement.PATH_OF_THE_SOUPER, killerPlayer);
                    break;
                case SURVIVALIST:
                    victim.sendMessage(CC.GRAY + "Hearts" + PvPDojo.POINTER + CC.BLUE + String.format("%.1f", k.getHealth() / DeveloperSettings.HEALTH_CONVERTER / 2));
                    victim.sendMessage(CC.GOLD + "GApples" + PvPDojo.POINTER + CC.GREEN + Stream.of(killerContents).filter(item -> item != null && item.getType() == Material.GOLDEN_APPLE).count());
                    AchievementManager.inst().trigger(Achievement.PATH_OF_THE_SURVIVOR, killerPlayer);
                default:
                    break;

            }
            victim.sendMessage(CC.GRAY + StringUtils.getLS());

            if (kuser.getSession() == null || !kuser.getSession().isTeamFight()) {
                PvPDojo.schedule(() -> {
                    if (k.getPlayStyle().equals(PlayStyle.SOUP)) {
                        PlayerUtil.addRecraft(k.getPlayer(), 32);
                    } else if (k.getPlayStyle().equals(PlayStyle.POTTER)) {
                        PlayerUtil.addEnderPearls(k.getPlayer(), 16);
                        PlayerUtil.addSpeedPotions(k.getPlayer(), 3);
                        PlayerUtil.addPotterRecraft(k.getPlayer(), 16);
                    } else {
                        k.getPlayer().setHealth(k.getPlayer().getMaxHealth());
                        k.getPlayer().getInventory().addItem(ItemBuilder.GOLDEN_HEAD.amount(1).build());
                    }
                }).nextTick();
            }

            if (k.getGunController() != null) {
                for (Gun gun : k.getGunController().getGuns()) {
                    gun.ammoLeft = gun.ammoReset;
                    k.sendMessage(MessageKeys.AMMO_REFILL, "{gun}", gun.getName());
                }
            }

            if (k.getAbilities().isEmpty()) {
                AchievementManager.inst().trigger(Achievement.CLASSICAL, k.getPlayer());
            }
        }

        if (user.getSession() != null) {
            user.getSession().broadcast(handleDeathMessage(e, e.getEntity(), e.getEntity().getKiller(), CC.stripColor(cp.listAbilities()),
                    victim.getKiller() != null ? CC.stripColor(AbilityPlayer.get(victim.getKiller()).listAbilities()) : null, false));
        }

        // Cleanup
        cp.die();
    }

    private static String listAbilities(KitData data) {
        StringBuilder abilities = new StringBuilder();
        for (KitAbilityData ability : data.getAbilities()) {
            AbilityLoader loader = AbilityLoader.getAbilityData(ability.getId());
            abilities.append(loader.getRarity().getColor()).append(loader.getName()).append(", ");
        }
        if (abilities.length() != 0)
            return abilities.substring(0, abilities.length() - 2);
        return abilities.toString();
    }
}
