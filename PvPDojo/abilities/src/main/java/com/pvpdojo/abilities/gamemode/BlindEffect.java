/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.gamemode;

import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.abilities.base.entities.AbilityEntity;

public class BlindEffect extends AbilityEffect {

    private int intensity;

    public BlindEffect(int duration, int intensity) {
        super(AbilityEffectType.BLIND, duration);
        this.intensity = intensity;
    }

    @Override
    public void applyEffect(AbilityEntity entity) {
        entity.getEntity().addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, getTickDuration(), intensity));
    }

    @Override
    public void endEffect(AbilityEntity entity) {
        entity.getEntity().removePotionEffect(PotionEffectType.BLINDNESS);
    }
}
