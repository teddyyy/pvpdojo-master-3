package com.pvpdojo.abilities.command;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilityinfo.AbilityUpgrade;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;
import org.bukkit.command.CommandSender;

@CommandAlias("abilitydata")
public class AbilityDataCommand extends DojoCommand {

    @CatchUnknown
    @Default
    @CommandCompletion("@abilities")
    public void onAbilityData(CommandSender sender, AbilityLoader loader) {
        String name = loader.getName();
        int ID = loader.getId();
        String rarity = loader.getRarity().getColor() + loader.getRarity();
        String collection = loader.getCollection().getItem().getItemMeta().getDisplayName();
        float points = loader.getAbilityBasePoints();
        String desc = loader.getAbilityDescription();
        String executor = loader.getAbilityExecutor();
        String type = loader.isActive() ? CC.GREEN + "Active" : CC.RED + "Passive";
        String enable = loader.isEnabled() ? CC.GREEN + "Yes" : CC.RED + "No";

        sender.sendMessage(StringUtils.getLS());

        sender.sendMessage(PvPDojo.PREFIX + "Name: " + loader.getRarity().getColor() + name);
        sender.sendMessage(PvPDojo.PREFIX + "ID: " + CC.AQUA + ID);
        sender.sendMessage(PvPDojo.PREFIX + "Rarity: " + rarity);
        sender.sendMessage(PvPDojo.PREFIX + "Collection: " + collection);
        sender.sendMessage(PvPDojo.PREFIX + "Points: " + CC.GOLD + points);
        sender.sendMessage(PvPDojo.PREFIX + "Desc: " + desc);
        sender.sendMessage(PvPDojo.PREFIX + "Executor: " + executor);
        sender.sendMessage(PvPDojo.PREFIX + "Type: " + type);
        sender.sendMessage(PvPDojo.PREFIX + "Enabled: " + enable);

        sender.sendMessage(PvPDojo.PREFIX + CC.DARK_GRAY + "--------UPGRADES--------");
        for (AbilityUpgrade abilityUpgrades : loader.getAbilityUpgrades()) {
            String upgradeName = abilityUpgrades.getName();
            float upgradePoint = abilityUpgrades.getPoints();
            String upgradesDescription = abilityUpgrades.getDescription();
            int upgradeID = abilityUpgrades.getUpgradeNumber();

            sender.sendMessage(PvPDojo.PREFIX + CC.GOLD + "Upgrade Name: " + CC.BLUE + upgradeName);
            sender.sendMessage(PvPDojo.PREFIX + CC.GOLD + "Upgrade ID: " + CC.BLUE + upgradeID);
            sender.sendMessage(PvPDojo.PREFIX + CC.GOLD + "Upgrade Points: " + CC.BLUE + upgradePoint);
            sender.sendMessage(PvPDojo.PREFIX + CC.GOLD + "Upgrade Desc: " + CC.BLUE + upgradesDescription);

            sender.sendMessage(" ");
        }

        sender.sendMessage(StringUtils.getLS());
    }

    @Override
    public Rank getRank() {
        return Rank.DEVELOPER;
    }
}
