package com.pvpdojo.abilities.abilities.active;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftBat;
import org.bukkit.entity.Bat;
import org.bukkit.entity.EntityType;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.gamemode.BlindEffect;

import net.minecraft.server.v1_7_R4.EntityBat;

public class BatmanAbility extends AbilityExecutor {

	@Override
	public String getExecutorName() {
		return "BatmanAbility";
	}

	@Override // cooldown, damage, uses, blindness duration
	public boolean activateAbility(Ability ability, Event event) {
		AbilityEntity activator = ability.getAbilityEntity();
		final float damage = ability.getAttribute("ability_damage");
		final float effect_duration = ability.getAttribute("effect_duration");

		Location loc = activator.getEntity().getEyeLocation();
		Vector dir = loc.getDirection();
		for (int i = 0; i < 10; i++) {
			Bat b = (Bat) activator.getEntity().getWorld().spawnEntity(loc, EntityType.BAT);
			b.setVelocity(dir);
			
			new AbilityRunnable(ability) {
				
				EntityBat bat = ((CraftBat) b).getHandle();
				int timer = 25;

				public void run() {
					timer--;

					bat.move(0, 0, 0);
					bat.yaw = 0;
					bat.pitch = 0;
					bat.lastPitch = 0;
					bat.lastYaw = 0;
					b.setVelocity(dir);

					List<AbilityEntity> nearby = AbilityServer.get().getNearbyEntities(activator, b.getLocation(), 2.0, 2.0);

					for (AbilityEntity target : nearby) {

						if(target.getEntity() == b) {
							continue;
						}

						activator.abilityDamage(target, damage, ability, DamageCause.MAGIC);
						target.applyEffect(new BlindEffect((int) (effect_duration*20), 2));
					}
					if (timer <= 0) {
						b.remove();
						this.cancel();
					}
				}
			}.createTimer(1, -1);
		}
		return true;
	}

}
