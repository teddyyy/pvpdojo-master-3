/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.userdata.User;

public class KangarooAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "KangarooAbility";
    }

	private static final String KANGAROO = "kangaroo";

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        AbilityEntity activator = ability.getAbilityEntity();
        if (activator.getEntity() instanceof Player) {
            Player player = (Player) activator.getEntity();
            User user = User.getUser(player);
            float jumpHeight = ability.getAttribute("jump_height");
            float crouchHeight = ability.getAttribute("crouch_height");
            float crouchDistance = ability.getAttribute("crouch_distance");

            if (ability.getMetaData().hasData(KANGAROO)) {
                return false;
            }
            
            if (player.isSneaking()) {
                Vector vector = player.getEyeLocation().getDirection();
                vector.multiply(crouchDistance);
                vector.setY(crouchHeight);
                player.setVelocity(vector);

                user.setCancelNextFall(true);
                player.setFallDistance(player.getFallDistance() > 0 ? -100 : -100);
            } else {
                player.setVelocity(new Vector(0, jumpHeight, 0));
                player.setFallDistance(player.getFallDistance() > 0 ? -100 : -100);

                user.setCancelNextFall(true);
            }
            ability.getMetaData().addData(KANGAROO, null);
        }
        return true;
    }
    
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
    	Player player = e.getPlayer();
    	if(canAbilityExecute(player)) {
    		Ability ability = getAbility(player);
    		if(ability.getMetaData().hasData(KANGAROO)) {
    			Block block = player.getLocation().getBlock();
    			if(block.getType() == Material.AIR && block.getRelative(BlockFace.DOWN).getType() != Material.AIR) {
    				ability.getMetaData().removeData(KANGAROO);
    			}
    		}
    	}
    }

}