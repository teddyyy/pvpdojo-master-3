/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityEffect.AbilityEffectType;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.gamemode.PoisonEffect;
import com.pvpdojo.abilities.gamemode.WitherEffect;

public class ViperAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "ViperAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        if (!ability.getMetaData().hasTarget())
            return false;
        AbilityEntity entity = ability.getMetaData().getTarget();
        float duration = ability.getAttribute("duration");
        float type = ability.getAttribute("effect_type");
        float effectPower = ability.getAttribute("effect_power");
        AbilityEffectType effect = type == 0 ? AbilityEffectType.POISON : AbilityEffectType.WITHER;
        if (!entity.hasEffect(effect)) {
            if (effect.equals(AbilityEffectType.POISON)) {
                entity.applyEffect(new PoisonEffect((int) duration * 20, (int) effectPower));
            } else if (effect.equals(AbilityEffectType.WITHER)) {
                entity.applyEffect(new WitherEffect((int) duration * 20, (int) effectPower));
            }
        }
        return true;
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (!(e.getDamager() instanceof LivingEntity) || !(e.getEntity() instanceof LivingEntity))
            return;
        LivingEntity damager = (LivingEntity) e.getDamager();
        if (canAbilityExecute(damager)) {
            Ability ability = getAbility(damager);
            AbilityEntity entity = AbilityServer.get().getEntity((LivingEntity) e.getEntity());
            ability.getMetaData().addTarget(entity, 10.0);
            ability.activateAbility(true, e);
        }
    }
}
