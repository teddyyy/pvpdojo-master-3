package com.pvpdojo.abilities.abilities.passive;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.effects.OctopusFormEffect;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.abilities.lib.ChargeSystem;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.abilities.AbilityMetaData;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerPreDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

public class WaterBenderAbility extends AbilityExecutor {
    @Override
    public String getExecutorName() {
        return "WaterBenderAbility";
    }

    private static final String OCTOPUS_FORM = "Water_Form_Key";

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        if (event instanceof PlayerToggleSneakEvent) {
            Player player = ((PlayerToggleSneakEvent) event).getPlayer();
            AbilityMetaData data = ability.getMetaData();

            float fast = ability.getAttribute("fast");
            float damage = ability.getAttribute("ability_damage");

            if (data.hasData(OCTOPUS_FORM)) {
                data.removeData(OCTOPUS_FORM);
            }

            OctopusFormEffect form = new OctopusFormEffect(ability);

            form.setBlockMaterial(Material.WATER);
            form.setApplyEffects(true);
            form.setDamage(damage);
            form.setSync(true);

            ChargeSystem charge = new ChargeSystem(ability, 100, (int) fast) {
                @Override
                public void trigger() {
                    form.form();
                    data.addData(OCTOPUS_FORM, form);

                    new AbilityRunnable(ability) {

                        int loop;

                        @Override
                        public void run() {
                            if (++loop > 20 * 10) {
                                cancel();
                            }
                        }

                        @Override
                        public void cancel() {
                            super.cancel();
                            form.destroy();
                            data.removeData(OCTOPUS_FORM);
                        }
                    }.createTimer(1, -1);

                }

                @Override
                public void run() {

                }
            };
            charge.call();
        }

        return false;
    }


    @EventHandler
    public void onPlayerPreDeath(PlayerPreDeathEvent e) {
        Player player = e.getPlayer();

        if (canAbilityExecute(player)) {
            if (e.isCancelled()) {
                return;
            }

            Ability ability = getAbility(player);

            if (ability.getMetaData().hasData(OCTOPUS_FORM)) {
                ((OctopusFormEffect) ability.getMetaData().getData(OCTOPUS_FORM)).destroy();
            }
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();

        if (canAbilityExecute(player)) {
            Ability ability = getAbility(player);

            if (ability.getMetaData().hasData(OCTOPUS_FORM)) {
                ((OctopusFormEffect) ability.getMetaData().getData(OCTOPUS_FORM)).destroy();
            }
        }
    }

    @EventHandler
    public void onWaterBender(PlayerToggleSneakEvent e) {
        if (canAbilityExecute(e.getPlayer())) {
            getAbility(e.getPlayer()).activateAbility(e);
        }
    }

}
