package com.pvpdojo.abilities.abilities.passive;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.abilities.lib.ChargeSystem;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.bukkit.util.TempBlock;
import com.pvpdojo.util.bukkit.BlockUtil;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class EarthBenderAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "EarthBenderAbility";
    }

    private static final String META = "EARTHBENDER_FALLING";
    private static final List<Block> EARTHBENDER_BLOCKS = new ArrayList<>();

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        if (event instanceof PlayerToggleSneakEvent) {
            Player player = ((PlayerToggleSneakEvent) event).getPlayer();

            ChargeSystem charge = new ChargeSystem(ability, 100, player.getLevel() < 100 ? 2 : 1) {

                @Override
                public void trigger() {
                    callEarthBender(ability);
                }

                @Override
                public void run() {
                }
            };
            charge.call();
        }
        return false;
    }

    private void callEarthBender(Ability ability) {// can be reflected.
        new AbilityRunnable(ability) {

            int x = 2, y = 0, rx = 2, ry = 0;

            AbilityEntity reflector;
            List<Block> blocks = BlockUtil.getLineOfSight(35, ability.getAbilityEntity().getEntity());

            public void run() {

                if (blocks.size() <= x) {
                    this.cancel();
                    return;
                }

                Block block = BlockUtil
                        .getFirstBlockDown(blocks.get((reflector == ability.getAbilityEntity() ? x : rx) - 1).getLocation(), null).getBlock();

                reflector = reflector == null ? ability.getAbilityEntity() : reflector;

                if (block != null) {

                    Material matType = block.getType();
                    byte data = block.getData();
                    Block blockUp = block.getRelative(BlockFace.UP);

                    if (block.getType().isSolid()) {
                        TempBlock.createTempBlock(blockUp.getLocation(), matType, data, 5);
                    }

                    for (int i = 0; i < (reflector == ability.getAbilityEntity() ? y : ry); i++) {

                        blockUp = blockUp.getRelative(BlockFace.UP);

                        if (block.getType().isSolid()) {
                            TempBlock.createTempBlock(blockUp.getLocation(), matType, data, 5);
                        }
                    }
                    EARTHBENDER_BLOCKS.add(block);

                    AbilityServer.get().getNearbyEntities(reflector, block.getLocation(), 5, 5)
                            .forEach(target -> {

                                if (reflector == null) {
                                    if (canAbilityExecute(target.getEntity())) {
                                        reflector = target;
                                        rx = 3;
                                        return;
                                    }
                                }

                                target.getEntity().setVelocity(target.getEntity().getVelocity()
                                        .add(new Vector(0.4, ((x / 8) != 0 ? (x / 8f) * 0.5f : 1.0), 0.0)));

                                reflector.abilityDamage(target, ability.getAttribute("ability_damage"), ability, EntityDamageEvent.DamageCause.PROJECTILE);

                                if (!target.getEntity().hasMetadata(META)) {
                                    target.getEntity().setMetadata(META, new FixedMetadataValue(PvPDojo.get(), null));
                                }
                            });

                    block.getLocation().getWorld().playEffect(block.getLocation(), Effect.STEP_SOUND, matType);
                }

                if (rx > 2) {
                    rx++;
                    ry++;
                }

                y++;
                x++;
            }
        }.createTimer(1, -1);
    }

    @EventHandler
    public void onBlockFromTo(BlockFromToEvent e) {
        if (EARTHBENDER_BLOCKS.contains(e.getBlock())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) {

        if (e.getEntity() instanceof LivingEntity) {
            if (e.getEntity().hasMetadata(META)) {
                if (e.getCause() == EntityDamageEvent.DamageCause.FALL) {

                    if (e.getDamage() > 10) {
                        e.setDamage(10.0);
                    }

                    e.getEntity().removeMetadata(META, PvPDojo.get());
                }
            }
        }
    }

    @EventHandler
    public void onEarthBender(PlayerToggleSneakEvent e) {
        if (canAbilityExecute(e.getPlayer())) {
            getAbility(e.getPlayer()).activateAbility(e);
        }
    }
}
