package com.pvpdojo.abilities.abilities.active;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.gamemode.BurnEffect;
import com.pvpdojo.lang.MessageKeys;

public class GodAbility extends AbilityExecutor {

    private static final String GOD_META = "god_meta";

    public HashMap<FallingBlock, AbilityEntity> storedBlocks = new HashMap<>();
    public HashMap<FallingBlock, AbilityEntity> owner = new HashMap<>();

    @Override
    public String getExecutorName() {
        return "GodAbility";
    }

    @Override // radius damage
    public boolean activateAbility(Ability ability, Event event) {
        AbilityEntity activator = ability.getAbilityEntity();

        Location loc = activator.getEntity().getTargetBlock(null, 50).getLocation();
        FallingBlockUtils blocksUtil = new FallingBlockUtils(activator, loc);

        if (!ability.getMetaData().hasData(GOD_META)) {
            blocksUtil.activate();
            if (!blocksUtil.isWorking()) {
                activator.sendMessage(MessageKeys.WARNING, "{text}", ChatColor.RED + "There are no blocks in a distance of 50 blocks");
                return false;
            }
            ability.getMetaData().addData(GOD_META, null);
            return false;
        } else {
            blocksUtil.throwEntity();
            ability.getMetaData().removeData(GOD_META);
        }
        return true;
    }

    public static final String META_KEY_GRAVITY_CONTROLL = "ENTITY_GRAVITY_CONTROL";

    public static boolean hasGravityControll(FallingBlock entity) {
        return entity.hasMetadata(META_KEY_GRAVITY_CONTROLL);
    }

    public static void addGravityControll(FallingBlock entity) {
        if (!hasGravityControll(entity)) {
            entity.setMetadata(META_KEY_GRAVITY_CONTROLL, new FixedMetadataValue(PvPDojo.get(), null));
        }
    }

    public static void removeGravityControll(FallingBlock entity) {
        if (hasGravityControll(entity)) {
            entity.removeMetadata(META_KEY_GRAVITY_CONTROLL, PvPDojo.get());
        }
    }

    public class FallingBlockUtils {

        AbilityEntity player;
        Location loc;

        public FallingBlockUtils(AbilityEntity owner, Location loc) {
            this.player = owner;
            this.loc = loc;
        }

        public AbilityEntity getOwner() {
            return player;
        }


        public void suckToLoc(FallingBlock entity) {
            Location lc = entity.getLocation().add(0, -6, 0);
            Location to = player.getEntity().getLocation();
            double speed = 0.5;
            double distances = lc.distance(to);
            double plus = 0.01;
            if (distances <= 5) {
                speed = 0.8;

            }
            double vx = (speed + plus * distances) * (to.getX() - lc.getX()) / distances;
            double vy = (speed + plus * distances) * (to.getY() - lc.getY()) / distances;
            double vz = (speed + plus * distances) * (to.getZ() - lc.getZ()) / distances;

            Vector v = entity.getVelocity();
            v.setX(vx);
            v.setY(vy);
            v.setZ(vz);
            entity.setVelocity(v);

        }

        boolean working;

        public void setWorking(boolean b) {
            this.working = b;
        }

        public boolean isWorking() {
            return working;
        }

        public void activate() {
            if (loc.getBlock().getType() != Material.AIR) {
                setWorking(true);

                int r = 3;
                ArrayList<BlockState> updatedBlocks = new ArrayList<>();

                int cx = loc.getBlockX();
                int cz = loc.getBlockZ();
                int cy = loc.getBlockY();
                int rSquared = r * r;
                for (int x = cx - r; x <= cx + r; x++) {
                    for (int y = cy; y <= cy + 3; y++) {
                        for (int z = cz - r; z <= cz + r; z++) {
                            if ((cx - x) * (cx - x) + (cz - z) * (cz - z) <= rSquared) {
                                final Location l = new Location(loc.getWorld(), x, y, z);
                                if (l.getBlock().getType().isBlock()) {
                                    l.getWorld().playEffect(l, Effect.STEP_SOUND, l.getBlock().getType());
                                    updatedBlocks.add(l.getBlock().getState());
                                    FallingBlock entity = l.getWorld().spawnFallingBlock(l,
                                            l.getBlock().getType(), l.getBlock().getData());
                                    addGravityControll(entity);
                                    entity.teleport(entity.getLocation().add(0, 3, 0));
                                    entity.setDropItem(false);
                                    storedBlocks.put(entity, player);
                                    owner.put(entity, player);

                                    new AbilityRunnable(getAbility(player.getEntity())) {
                                        @Override
                                        public void run() {
                                            if (entity.isDead() || entity.isOnGround()) {
                                                owner.remove(entity);
                                            }
                                        }

                                        @Override
                                        public void cancel() {
                                            super.cancel();
                                            owner.remove(entity);
                                        }
                                    }.createTimer(1, -1);

                                    new AbilityRunnable(getAbility(player.getEntity())) {

                                        double timer = 0;

                                        public void run() {
                                            timer++;
                                            if (player.isDead() || entity.isDead()) {
                                                entity.remove();
                                                owner.remove(entity);
                                                this.cancel();
                                            }
                                            if (timer <= 5) {
                                                for (BlockState states : updatedBlocks) {
                                                    states.update();
                                                }
                                            }
                                            if (hasGravityControll(entity)) {
                                                suckToLoc(entity);
                                            } else {
                                                this.cancel();
                                            }
                                            if (getAbility(player.getEntity()).getCooldown().isCoolingDown()) {
                                                if (hasGravityControll(entity)) {
                                                    PvPDojo.schedule(FallingBlockUtils.this::throwEntity).createTask(20);
                                                }
                                            }
                                        }
                                    }.createTimer(2, -1);
                                }
                            }
                        }
                    }
                }
            } else {
                setWorking(false);
            }
        }

        public void throwEntity() {
            for (FallingBlock fb : storedBlocks.keySet()) {
                if (storedBlocks.get(fb) == player) {
                    removeGravityControll(fb);
                    PvPDojo.schedule(() -> fb.setVelocity(player.getEntity().getEyeLocation().getDirection().multiply(2))).createTask(7);
                }
            }
            storedBlocks.clear();
        }
    }

    List<UUID> hits = new ArrayList<>();

    @EventHandler
    public void onDeath(EntityDeathEvent e) {

    }

    @EventHandler
    public void onEntityChange(EntityChangeBlockEvent e) {
        if (e.getEntity() instanceof FallingBlock) {
            FallingBlock f = (FallingBlock) e.getEntity();
            AbilityEntity player = owner.remove(f);

            if (!hasGravityControll(f) && player != null) {
                Ability ability = getAbility(player.getEntity());
                if (ability != null) {
                    f.getWorld().playEffect(f.getLocation(), Effect.STEP_SOUND, f.getBlockId());
                    final float radius = ability.getAttribute("radius");
                    final float damage = ability.getAttribute("ability_damage");
                    for (AbilityEntity near : AbilityServer.get().getNearbyEntities(ability.getAbilityEntity(), f.getLocation(), radius, radius)) {

                        if (!hits.contains(near.getEntity().getUniqueId())) {
                            hits.add(near.getEntity().getUniqueId());
                            if (f.getMaterial() == Material.LAVA || f.getMaterial() == Material.STATIONARY_LAVA) {
                                near.applyEffect(new BurnEffect(20 * 5));
                            }
                            if (f.getMaterial() == Material.WATER) {
                                near.getEntity().setFireTicks(0);
                            }
                            player.abilityDamage(near, damage, ability, DamageCause.MAGIC);
                            near.getEntity().setVelocity(near.getEntity().getVelocity()
                                                             .add(near.getEntity().getLocation().toVector()
                                                                      .subtract(player.getEntity().getLocation().toVector()).normalize()
                                                                      .multiply(2.2).add(new Vector(0, 0.7, 0))));

                            PvPDojo.schedule(() -> hits.remove(near.getEntity().getUniqueId())).createTask(15);
                        }
                    }
                }
            }
        }
    }
}
