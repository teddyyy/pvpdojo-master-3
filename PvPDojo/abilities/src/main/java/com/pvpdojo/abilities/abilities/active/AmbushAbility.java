/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityCrosshair;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.bukkit.events.EntityFallEvent;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.BlockUtil;

public class AmbushAbility extends AbilityExecutor {

    public static final String BLOCK_META = "ambush_block";

    @Override
    public String getExecutorName() {
        return "AmbushAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        final AbilityEntity activator = ability.getAbilityEntity();
        AbilityCrosshair crosshair = new AbilityCrosshair(activator) {
            @Override
            public void targetFound(AbilityEntity target, double accuracy, int distance) {
                int amount = 15;
                ability.getMetaData().addTarget(target, 5);
                Location loc = target.getEntity().getLocation().add(0, amount, 0);
                loc.setPitch(90);
                activator.getEntity().teleport(BlockUtil.findLocationBetweenPoints(loc, target.getEntity().getLocation()));
                activator.getEntity().setVelocity(loc.getDirection().multiply(.9));
                createSlamEffect(activator.getEntity().getLocation());
                if (activator instanceof AbilityPlayer) {
                    User.getUser(((AbilityPlayer) activator).getPlayer()).cancelNextFall();
                } else {
                    activator.getEntity().setFallDistance(-amount);
                }
            }
        };
        return crosshair.calculate(1.5, 15, true);
    }

    @EventHandler
    public void onEntityFall(EntityFallEvent e) {
        Entity entity = e.getEntity();
        if (entity instanceof LivingEntity) {
            if (!canAbilityExecute((LivingEntity) entity))
                return;
            Ability ability = getAbility((LivingEntity) entity);
            AbilityEntity activator = ability.getAbilityEntity();
            if (ability.getMetaData().hasTarget()) {
                AbilityEntity target = ability.getMetaData().getTarget();
                float damage = ability.getAttribute("ability_damage");
                activator.abilityDamage(target, damage, ability, DamageCause.ENTITY_ATTACK);
                ability.getMetaData().removeTarget(target);
                target.getEntity().getWorld().playSound(target.getEntity().getLocation(), Sound.ENDERDRAGON_WINGS, 10, 7);
            }
        }
    }

    @EventHandler
    public void onAmbushBlockChange(EntityChangeBlockEvent e) {
        if (e.getEntity().hasMetadata(BLOCK_META)) {
            e.getEntity().removeMetadata(BLOCK_META, PvPDojo.get());
            e.setCancelled(true);
        }
    }

    @SuppressWarnings("deprecation")
    private void createSlamEffect(Location center) {
        double radius = 3F;
        double amount = 7;
        double increment = (2 * Math.PI) / amount;
        for (int i = 0; i < amount; i++) {
            double angle = i * increment;
            double x = center.getX() + (radius * Math.cos(angle));
            double z = center.getZ() + (radius * Math.sin(angle));
            Location loc = new Location(center.getWorld(), x, center.getY() - 3, z);
            FallingBlock block = loc.getWorld().spawnFallingBlock(loc, Material.GOLD_BLOCK, (byte) 0);
            block.setDropItem(false);
            block.setVelocity(new Vector(0, -.9, 0));
            block.setMetadata(BLOCK_META, new FixedMetadataValue(PvPDojo.get(), null));
        }
    }
}
