/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.effects;

import org.bukkit.Location;
import org.bukkit.Material;

import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.BurnEffect;
import com.pvpdojo.bukkit.util.TempBlock;

public class PrisonerEffect {

	public PrisonerEffect(Location c, AbilityEntity attacker, AbilityEntity victim, int duration) {
		if (!victim.isTargetable() || !attacker.isTargetable()) {
			return;
		}
		Location center = c.clone().add(0, -2, 0);
		Location tp = center.getWorld().getBlockAt(center.getBlockX(), center.getBlockY(), center.getBlockZ())
				.getLocation().add(.45, .45, .45);
		for (int x = -1; x < 2; x++) {
			for (int y = -1; y < 2; y++) {
				for (int z = -1; z < 2; z++) {
					Location loc = center.clone().add(x, y, z);
					if (z == 0 && x == 0 && y > -1) {
						TempBlock.createTempBlock(loc, Material.LAVA, (byte) 0, duration);
					} else
						TempBlock.createTempBlock(loc, Material.GLASS, (byte) 0, duration);
				}
			}
		}
		TempBlock.createTempBlock(center.clone().add(0, 2, 0), Material.GLASS, (byte) 0, duration);
		victim.getEntity().teleport(tp);
		victim.applyEffect(new BurnEffect(duration * 20));

	}
}
