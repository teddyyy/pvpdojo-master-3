/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Effect;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityCrosshair;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;

public class AfterShockAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "AfterShockAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        AbilityEntity activator = ability.getAbilityEntity();

        final float damage = ability.getAttribute("ability_damage");
        final float knockback = ability.getAttribute("knockback");

        AbilityCrosshair crosshair = new AbilityCrosshair(activator) {

            @Override
            public void targetFound(AbilityEntity target, double accuracy, int distance) {

                activator.abilityDamage(target, damage, ability, DamageCause.ENTITY_ATTACK);
                LivingEntity victim = target.getEntity();
                victim.getWorld().playEffect(victim.getEyeLocation(), Effect.POTION_SWIRL, 3);
                victim.getWorld().playEffect(victim.getEyeLocation(), Effect.POTION_SWIRL, 2);
                if (knockback > 0) {
                    Vector v = activator.getEntity().getEyeLocation().getDirection().setY(0.3);
                    victim.setVelocity(v.multiply(knockback));
                }
            }
        };
        return crosshair.calculate(5, 20, true);
    }

}
