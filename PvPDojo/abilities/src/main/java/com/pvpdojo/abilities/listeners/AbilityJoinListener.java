/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;

import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.bukkit.listeners.DojoListener;

public class AbilityJoinListener implements DojoListener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoin(PlayerJoinEvent e) {
        AbilityServer.get().createPlayer(e.getPlayer());
    }

}
