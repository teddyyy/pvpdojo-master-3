/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.gamemode;

import org.bukkit.potion.PotionEffectType;

public class WitherEffect extends AbilityPotionEffect {

    public WitherEffect(int duration, int strength) {
        super(AbilityEffectType.WITHER, PotionEffectType.WITHER, duration, strength);
    }

}
