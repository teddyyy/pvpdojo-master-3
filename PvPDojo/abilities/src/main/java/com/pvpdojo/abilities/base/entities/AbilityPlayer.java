/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.base.entities;

import com.pvpdojo.DeveloperSettings;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.abilities.Abilities;
import com.pvpdojo.abilities.abilities.lib.TemporaryEntity;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.abilities.ActiveAbility;
import com.pvpdojo.abilities.base.abilities.PassiveAbility;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.listeners.events.DojoSelectKitEvent;
import com.pvpdojo.abilityinfo.AbilityType;
import com.pvpdojo.abilityinfo.KitAttributeArray;
import com.pvpdojo.abilityinfo.KitAttributeArray.AttributeArrayType;
import com.pvpdojo.abilityinfo.KitAttributeArray.KitAttribute;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.dataloader.KitLoader;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.replay.Recorder;
import com.pvpdojo.session.entity.TeamArenaEntity;
import com.pvpdojo.userdata.KitAbilityData;
import com.pvpdojo.userdata.KitData;
import com.pvpdojo.userdata.PlayStyle;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.LeatherArmorBuilder;
import com.pvpdojo.util.bukkit.ItemUtil;
import com.pvpdojo.util.bukkit.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public class AbilityPlayer extends AbilityEntity {

    KitData currentKit;
    PlayStyle playstyle = PlayStyle.SOUP;
    private UUID uuid;
    long enderPearlThrow;

    public static AbilityPlayer get(Player player) {
        return AbilityServer.get().getPlayer(player);
    }

    public AbilityPlayer(Player player) {
        super(player);
        uuid = player.getUniqueId();
    }

    public Player getPlayer() {
        return (Player) getEntity();
    }

    public UUID getUUID() {
        return uuid;
    }

    public KitData getKit() {
        return currentKit;
    }

    public long getEnderPearlThrow() {
        return enderPearlThrow;
    }

    public void setEnderPearlThrow(long enderPearlThrow) {
        this.enderPearlThrow = enderPearlThrow;
    }

    @Override
    public void invalidate() {
        super.invalidate();
        currentKit = null;
    }

    public PlayStyle getPlayStyle() {
        return playstyle;
    }

    @Override
    public boolean isTargetable() {
        if (super.isTargetable()) {
            User user = User.getUser(getPlayer());
            return !user.isSpectator() && !user.isAdminMode() && user.canPvP();
        }
        return false;
    }

    @Override
    public boolean canTarget(AbilityEntity entity) {
        if (!super.canTarget(entity)) {
            return false;
        }
        User user = User.getUser(getPlayer());
        if (user.isAdminMode()) {
            user.sendMessage(CC.RED + "You cannot attack people while in staff mode");
            return false;
        }

        if (user.isFrozenByStaff()) {
            return false;
        }

        if (entity instanceof AbilityPlayer) {
            TeamArenaEntity otherTeam = User.getUser(((AbilityPlayer) entity).getPlayer()).getTeam();
            TeamArenaEntity ourTeam = user.getTeam();
            if (otherTeam != null && ourTeam != null && otherTeam == ourTeam && !otherTeam.isFriendlyFire()) {
                return false;
            }
        }

        return true;
    }

    public void updateMoveSpeed() {
        float move_speed = this.getMoveSpeed();
        if (this.getPlayer().getWalkSpeed() != move_speed) {
            if (move_speed < 0)
                move_speed = 0;
            this.getPlayer().setWalkSpeed(move_speed);
        }
    }

    @Override
    public void update() {
        super.update();

        if (tick % 10 != 0)
            return;

        Abilities.timing("Move Speed").startTiming();
        updateMoveSpeed();
        Abilities.timing("Move Speed").stopTiming();

        Abilities.timing("Ability Owned Entities").startTiming();
        for (Ability ability : getAbilities()) {
            for (TemporaryEntity entity : ability.getMetaData().getEntityTracker()) {
                entity.tick();
            }
            ability.getMetaData().getEntityTracker().removeIf(TemporaryEntity::isDead);
        }
        Abilities.timing("Ability Owned Entities").stopTiming();

    }

    @Override
    public void setMana(float mana) {
        super.setMana(mana);
        float mana_percent = getMana() / getMaxMana();
        if (getPlayer().getExp() == mana_percent) {
            return;
        }
        getPlayer().setExp(mana_percent);
    }

    @Override
    public void combatTag(AbilityEntity entity, float healthBefore, float damage, DamageCause cause) {
        super.combatTag(entity, healthBefore, damage, cause);
        if (entity instanceof AbilityPlayer) {
            NMSUtils.get(getPlayer()).killer = NMSUtils.get(((AbilityPlayer) entity).getPlayer());
            NMSUtils.get(getPlayer()).lastDamageByPlayerTime = 200;
        }
    }

    public void selectChampion(int kitId) {
        User user = User.getUser(getPlayer());

        // Fallback
        KitData fallBackData = new KitData("none", 1);
        fallBackData.setPlayStyle(user.getPersistentData().getKits().get(0).getPlayStyle());
        if (kitId == -1) {
            if (user.getPersistentData().getKits().isEmpty()) {
                user.kick("error.nokits");
                return;
            }
            kitId = user.getPersistentData().getKits().get(0).getId();
        }

        selectChampion(kitId == -2 ? fallBackData : user.getPersistentData().getKit(kitId));
    }

    public void selectChampion(KitData data) {
        User user = User.getUser(getPlayer());

        isDead = false;
        invalidate();
        PlayerUtil.resetPlayer(getPlayer());
        getEffects().clear();

        KitLoader loader = KitLoader.getKitData(1);
        this.currentKit = data;
        clearBonusAttributes();
        setBaseAttributeArray(loader.getKitBaseAttributes(), loader.getKitIncrementAttributes());

        this.playstyle = data.getPlayStyle();

        int i = playstyle == PlayStyle.SURVIVALIST ? 1 : 0;
        for (KitAbilityData ability : data.getAbilities()) {
            AbilityLoader abilityData = AbilityLoader.getAbilityData(ability.getId());
            if (abilityData == null)
                continue;
            if (!DeveloperSettings.freeAbilities(user)) {
                if (ability.getId() > 0) {
                    if (!user.getPersistentData().hasAbility(abilityData.getId())) {
                        user.sendOverlay(MessageKeys.YOU_DONT_OWN, "{item}", abilityData.getName());
                        continue;
                    }
                }
            }
            if (!abilityData.isEnabled()) {
                user.getPlayer().sendMessage(CC.RED + "Ability disabled for now: " + abilityData.getName());
                continue;
            }
            String abilityName = abilityData.getName();
            String abilityExecutor = abilityData.getAbilityExecutor();
            KitAttributeArray baseAttributes = abilityData.getAbilityBaseAttributes();
            Ability kitAbility;
            if (abilityData.isActive()) {
                kitAbility = new ActiveAbility(this, abilityName, abilityExecutor, baseAttributes, ability);
                if (ability.getAbilityType().equals(AbilityType.CLICK)) {
                    i++;
                    getPlayer().getOpenInventory().getBottomInventory().setItem(i, kitAbility.getClickItem());
                }
            } else {
                kitAbility = new PassiveAbility(this, abilityName, abilityExecutor, baseAttributes, ability);
            }

            getAbilities().add(kitAbility);
            kitAbility.enableAbility();

        }
        getPlayer().getInventory().setItem(0, new ItemBuilder(Material.STONE_SWORD).unbreakable().build());
        DojoSelectKitEvent selectEvent = new DojoSelectKitEvent(this, playstyle);
        Bukkit.getPluginManager().callEvent(selectEvent);

        if (data.getInventory() != null) {
            getPlayer().getInventory().setContents(data.getInventory());
        } else {

            if (playstyle == PlayStyle.SURVIVALIST) {
                this.getPlayer().getInventory().setItem(1, new ItemBuilder(Material.FISHING_ROD).unbreakable().build());
                getPlayer().getInventory().setItem(8, new ItemStack(Material.GOLDEN_APPLE, 4));
                getPlayer().getInventory().setItem(7, ItemBuilder.GOLDEN_HEAD.amount(2).build());
            }

            selectEvent.getAddItems().forEach(getPlayer().getInventory()::addItem);

            if (this.playstyle.equals(PlayStyle.SOUP)) {
                PlayerUtil.refillRecraft(getPlayer(), 32);
                PlayerUtil.refillSoup(getPlayer(), 36);
            } else if (this.playstyle.equals(PlayStyle.POTTER)) {
                PlayerUtil.refillSpeedPotions(getPlayer(), i);
                PlayerUtil.refillHealPotions(getPlayer(), 35);
            }
        }

        if (playstyle == PlayStyle.POTTER) {
            getPlayer().getInventory().setChestplate(new LeatherArmorBuilder(Material.LEATHER_CHESTPLATE, Color.AQUA)
                    .enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1)
                    .unbreakable()
                    .build());
            getPlayer().getInventory().setLeggings(new LeatherArmorBuilder(Material.LEATHER_LEGGINGS, Color.AQUA)
                    .enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1)
                    .unbreakable()
                    .build());
            getPlayer().getInventory().setBoots(new LeatherArmorBuilder(Material.LEATHER_BOOTS, Color.AQUA)
                    .enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1)
                    .enchant(Enchantment.PROTECTION_FALL, 3)
                    .unbreakable()
                    .build());
        } else if (playstyle == PlayStyle.SURVIVALIST) {
            this.addAttribute(new KitAttribute("max_health", 1300, AttributeArrayType.BONUS_ATTRIBUTES));
        }

        if (PvPDojo.SERVER_TYPE == ServerType.FFA) {
            implementFFAEquipments(data);
        }

        user.getPlayer().setMaxHealth(getMaxHealth());
        this.setHealth(getMaxHealth());
        setMana(getMaxMana());
        getPlayer().getPlayer().setMaxHealth(getMaxHealth() / HEALTH_CONVERTER);

        long time = System.currentTimeMillis();
        Recorder.inst().applyAction(getPlayer(), replay -> {
            replay.heldItem(getPlayer(), getPlayer().getItemInHand(), time);
            replay.armor(getPlayer(), getPlayer().getInventory().getArmorContents(), time);
        });
    }


    private void implementFFAEquipments(KitData data) {

        if (data == null) {
            return;
        }

        boolean isPotter = data.getPlayStyle() == PlayStyle.POTTER;

        // Swords
        for (ItemStack item : getPlayer().getInventory().getContents()) {

            if (item == null || item.getType() == Material.AIR) {
                continue;
            }

            if (ItemUtil.SWORDS.contains(item.getType())) {
                item.setType(Material.DIAMOND_SWORD);
                item.addEnchantment(Enchantment.DAMAGE_ALL, 1);
                item.setItemMeta(new ItemBuilder.ItemEditor(item).unbreakable().build().getItemMeta());
            }
        }

        // Soup, Survivor Armour
        if (!isPotter) {
            getPlayer().getInventory().setHelmet(new ItemBuilder(Material.IRON_HELMET).enchant(Enchantment.DURABILITY, 1).build());
            getPlayer().getInventory().setChestplate(new ItemBuilder(Material.IRON_CHESTPLATE).enchant(Enchantment.DURABILITY, 1).build());
            getPlayer().getInventory().setLeggings(new ItemBuilder(Material.IRON_LEGGINGS).enchant(Enchantment.DURABILITY, 1).build());
            getPlayer().getInventory().setBoots(new ItemBuilder(Material.IRON_BOOTS).enchant(Enchantment.DURABILITY, 1).build());

        } else {
            // Potter Armour
            getPlayer().getInventory().setChestplate(new ItemBuilder(Material.DIAMOND_CHESTPLATE)
                    .enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4)
                    .enchant(Enchantment.DURABILITY, 1)
                    .build());

            getPlayer().getInventory().setLeggings(new ItemBuilder.ItemEditor(getPlayer().getInventory().getLeggings())
                    .enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3)
                    .unbreakable()
                    .build());

            getPlayer().getInventory().setBoots(new ItemBuilder(Material.DIAMOND_BOOTS)
                    .enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4)
                    .enchant(Enchantment.PROTECTION_FALL, 3)
                    .enchant(Enchantment.DURABILITY, 1)
                    .build());

        }
    }

}
