/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.bukkit.util.TempBlock;
import com.pvpdojo.userdata.User;

public class AngelAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "AngelAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        return false;
    }

    @EventHandler
    public void onAngel(ProjectileLaunchEvent e) {
        if (e.getEntity() instanceof ThrownPotion) {
            ThrownPotion pot = (ThrownPotion) e.getEntity();
            if (pot.getShooter() instanceof LivingEntity && canAbilityExecute((LivingEntity) pot.getShooter())) {
                for (PotionEffect effect : pot.getEffects()) {
                    if (effect.getType().equals(PotionEffectType.HEAL)) {
                        Location location = ((LivingEntity) pot.getShooter()).getLocation().subtract(0, 1, 0);
                        if (location.getBlock().getType() == Material.AIR) {
                            TempBlock.createTempBlock(location, Material.OBSIDIAN, (byte) 0, 2);
                            if (pot.getShooter() instanceof Player) {
                                User.getUser((Player) pot.getShooter()).cancelNextFall();
                            }
                        }
                        break;
                    }
                }
            }
        }
    }
}
