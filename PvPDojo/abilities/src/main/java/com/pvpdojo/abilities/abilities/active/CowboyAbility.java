/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.abilities.AbilityMetaData;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;

public class CowboyAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "CowboyAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        AbilityEntity activator = ability.getAbilityEntity();
        AbilityMetaData meta = ability.getMetaData();

        Location loc = activator.getEntity().getLocation();

        final float radius = ability.getAttribute("radius");
        final float damage = ability.getAttribute("damage");
        final int duration = (int) ability.getAttribute("duration");

        ArrayList<Horse> horses = new ArrayList<>();

        for (int i = 0; i < 15; i++) {
            Location horseLoc = activator.getEntity().getLocation();

            horseLoc.add(Math.random(), 0, Math.random());
            Horse h = (Horse) activator.getEntity().getWorld().spawnEntity(horseLoc, EntityType.HORSE);
            meta.addEntity(h, duration, false);

            h.setAdult();

            h.setVelocity(loc.getDirection().setY(0).normalize().multiply(2));

            horses.add(h);
        }
        new AbilityRunnable(ability) {
            int counter = 0;

            @Override
            public void cancel() {

            }

            List<AbilityEntity> damaged = new ArrayList<>();

            @Override
            public void run() {

                for (Horse h : horses) {
                    Vector newLocation = loc.getDirection();
                    h.setVelocity(newLocation.setY(0).normalize().multiply(0.6D).setX(newLocation.getX()).setZ(newLocation.getZ()));

                    if (--counter <= 0) {
                        counter = 10;
                        for (AbilityEntity near : AbilityServer.get().getNearbyEntities(h.getLocation(), radius, radius)) {

                            if (near.equals(activator) || damaged.contains(near)) {
                                continue;
                            }
                            if (activator.abilityDamage(near, damage, ability, DamageCause.MAGIC)) {
                                damaged.add(near);
                            }
                        }
                    }

                    if (activator.isDead()) {
                        h.remove();
                        this.cancel();
                    }
                }
            }
        }.createTimer(1, duration * 20);

        return true;
    }

}
