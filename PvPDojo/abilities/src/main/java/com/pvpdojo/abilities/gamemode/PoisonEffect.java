/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.gamemode;

import org.bukkit.potion.PotionEffectType;

public class PoisonEffect extends AbilityPotionEffect {

    public PoisonEffect(int duration, int strength) {
        super(AbilityEffectType.POISON, PotionEffectType.POISON, duration, strength);
    }

}
