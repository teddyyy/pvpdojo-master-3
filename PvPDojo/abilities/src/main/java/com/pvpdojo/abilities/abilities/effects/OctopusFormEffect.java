package com.pvpdojo.abilities.abilities.effects;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.abilities.lib.BendingBlock;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import lombok.Data;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

@Data
public class OctopusFormEffect {

    private Ability ability;
    private Block sourceblock;
    private Location sourcelocation;
    private BendingBlock source;
    private ArrayList<BendingBlock> blocks = new ArrayList<>(), newblocks = new ArrayList<>();
    private Material blockMaterial;

    private double range = 10, radius = 4, interval = 50, time, startangle, angle, y, animstep = 1, step = 1, inc = 3, dta = 45;
    private boolean sourceselected = false, settingup = false, forming = false, formed = false, cancelled = false, applyEffects = false;
    private float damage = 50;

    private PotionEffectType[] effects = new PotionEffectType[]{PotionEffectType.CONFUSION, PotionEffectType.SLOW, PotionEffectType.POISON};
    private int effectDuration = 20 * 7;
    private int effectAmplifier = 1;

    private boolean sync = false;

    public OctopusFormEffect(Ability ability) {
        this.ability = ability;

        time = System.currentTimeMillis();
        sourceblock = ability.getAbilityEntity().getEntity().getLocation().add(0, 0, 1).getBlock();

        if (sourceblock != null) {
            sourcelocation = sourceblock.getLocation();
            sourceselected = true;
        }

    }

    private void incrementStep() {
        if (sourceselected) {
            sourceselected = false;
            settingup = true;
        } else if (settingup) {
            settingup = false;
            forming = true;
        } else if (forming) {
            forming = false;
            formed = true;
        }
    }


    public void form() {
        incrementStep();
        source = new BendingBlock(sourceblock, blockMaterial, (byte) 0);

        new AbilityRunnable(ability) {

            @Override
            public void run() {
                if (!cancelled) {
                    progress();
                } else {
                    cancel();
                }
            }
        }.createTimer(1, -1);
    }

    public void damage() {

        if (cancelled) {
            return;
        }


        AbilityServer.get()
                .getEntities(ability.getAbilityEntity().getEntity().getWorld())
                .stream()
                .filter(target -> target != ability.getAbilityEntity())
                .filter(target -> target.getEntity().getLocation().distance(ability.getAbilityEntity().getEntity().getLocation()) >= radius && target.getEntity().getLocation().distance(ability.getAbilityEntity().getEntity().getLocation()) <= radius + 1)
                .forEach(target -> {
                    ability.getAbilityEntity().abilityDamage(target, damage, ability, EntityDamageEvent.DamageCause.MAGIC);

                    if (isApplyEffects()) {
                        for (PotionEffectType type : effects) {
                            target.getEntity().addPotionEffect(new PotionEffect(type, effectDuration, effectAmplifier));
                        }
                    }

                });

    }

    private void progress() {

        if (!sourceblock.getWorld().equals(ability.getAbilityEntity().getEntity().getWorld())) {
            destroy();
            return;
        }

        if ((sourceblock.getLocation().distance(ability.getAbilityEntity().getEntity().getLocation()) > range) && (sourceselected)) {
            sourceblock = ability.getAbilityEntity().getEntity().getLocation().add(0, 0, 1).getBlock();
            return;
        }

        if (System.currentTimeMillis() > time + interval) {
            time = System.currentTimeMillis();
            Location location = ability.getAbilityEntity().getEntity().getLocation();

            if (!sourceselected && settingup) {
                if (sourceblock.getY() < location.getBlockY()) {
                    source.revertBlock();
                    source = null;

                    Block newblock = sourceblock.getRelative(BlockFace.UP);
                    sourcelocation = newblock.getLocation();

                    if (!newblock.getType().isSolid()) {
                        source = new BendingBlock(newblock, blockMaterial, (byte) 0);
                        sourceblock = newblock;
                    } else {
                        destroy();
                    }

                } else if (sourceblock.getY() > location.getBlockY()) {
                    source.revertBlock();
                    source = null;

                    Block newblock = sourceblock.getRelative(BlockFace.DOWN);
                    sourcelocation = newblock.getLocation();

                    if (!newblock.getType().isSolid()) {
                        source = new BendingBlock(newblock, blockMaterial, (byte) 0);
                        sourceblock = newblock;
                    } else {
                        destroy();
                    }

                } else if (sourcelocation.distance(location) > radius) {
                    Vector vector = getDirection(sourcelocation, location.getBlock().getLocation()).normalize();
                    sourcelocation.add(vector);

                    Block newblock = sourcelocation.getBlock();
                    if (!newblock.equals(sourceblock)) {
                        source.revertBlock();
                        source = null;

                        if (!newblock.getType().isSolid()) {
                            source = new BendingBlock(newblock, blockMaterial, (byte) 0);
                            sourceblock = newblock;
                        }

                    }
                } else {
                    incrementStep();

                    source.revertBlock();
                    source = null;

                    Vector vector = new Vector(1, 0, 0);
                    startangle = vector.angle(getDirection(sourceblock.getLocation(), location));
                    angle = startangle;
                }
            } else if (forming) {
                if (angle - startangle >= 360.0D) {
                    y += 1.0D;
                } else {
                    angle += 20.0D;
                }

                formOctopus();
                if (y == 2.0D) {
                    incrementStep();
                }

            } else if (formed) {
                step += 1;
                if (step % inc == 0) {
                    animstep += 1;
                }
                if (animstep > 8) {
                    animstep = 1;
                }
                formOctopus();
            } else {
                destroy();
            }
        }
    }

    private void formOctopus() {
        Location location = ability.getAbilityEntity().getEntity().getLocation();
        newblocks.clear();

        List<Block> blockList = new ArrayList<Block>();

        for (double theta = startangle; theta < startangle + angle; theta += 10.0D) {
            double rtheta = Math.toRadians(theta);
            Block block = location.clone().add(new Vector(radius * Math.cos(rtheta), 0.0D, radius * Math.sin(rtheta))).getBlock();

            if (!blockList.contains(block)) {
                spawnBlock(block);
                blockList.add(block);
            }
        }

        Vector eyedir = ability.getAbilityEntity().getEntity().getEyeLocation().getDirection();
        eyedir.setY(0);

        double tentacleangle = Math.toDegrees(new Vector(1, 0, 0).angle(eyedir)) + dta / 2.0D;
        double astep = animstep;

        for (double tangle = tentacleangle; tangle < tentacleangle + 360.0D; tangle += dta) {
            astep++;
            double phi = Math.toRadians(tangle);
            tentacle(location.clone().add(new Vector(radius * Math.cos(phi), 0.0D, radius * Math.sin(phi))), (int) astep);
        }

        for (BendingBlock block : blocks) {
            if (!newblocks.contains(block)) {
                block.revertBlock();
            }
        }

        blocks.clear();
        blocks.addAll(newblocks);
    }

    private void tentacle(Location base, int animationstep) {
        if (!BendingBlock.isBendingBlock(base.getBlock())) {
            return;
        }

        if (!blocks.contains(BendingBlock.get(base.getBlock()))) {
            return;
        }

        damage();
        Vector direction = getDirection(ability.getAbilityEntity().getEntity().getLocation(), base);
        direction.setY(0);
        direction.normalize();

        if (animationstep > 8) {
            animationstep %= 8;
        }

        if (y >= 1.0D) {
            Block baseblock = base.clone().add(0.0D, 1.0D, 0.0D).getBlock();

            if (animationstep == 1) {
                spawnBlock(baseblock);
            } else if ((animationstep == 2) || (animationstep == 8)) {
                spawnBlock(baseblock);
            } else {
                spawnBlock(base.clone().add(direction.getX(), 1.0D, direction.getZ()).getBlock());
            }
        }

        if (y == 2.0D) {
            Block baseblock = base.clone().add(0.0D, 2.0D, 0.0D).getBlock();

            if (animationstep == 1) {
                spawnBlock(base.clone().add(-direction.getX(), 2.0D, -direction.getZ()).getBlock());
            } else if ((animationstep == 3) || (animationstep == 7) || (animationstep == 2) || (animationstep == 8)) {
                spawnBlock(baseblock);
            } else if ((animationstep == 4) || (animationstep == 6)) {
                spawnBlock(base.clone().add(direction.getX(), 2.0D, direction.getZ()).getBlock());
            } else {
                spawnBlock(base.clone().add(2.0D * direction.getX(), 2.0D, 2.0D * direction.getZ()).getBlock());
            }
        }
    }

    private void spawnBlock(Block block) {
        if (BendingBlock.isBendingBlock(block)) {
            BendingBlock tblock = BendingBlock.get(block);

            if (!newblocks.contains(tblock)) {
                if (!blocks.contains(tblock)) {
                    assert tblock != null;
                    tblock.setType(blockMaterial, (byte) 0);
                }
                newblocks.add(tblock);
            }

        } else if ((block.getType() == Material.FIRE) || (block.getType() == Material.AIR)
                || !block.getType().isSolid()) {
            BendingBlock tblock = new BendingBlock(block, blockMaterial, (byte) 0);
            newblocks.add(tblock);
        }
    }

    public void destroy() {

        if (!sync) {
            cancel();
        }

        int loop = 0;
        for (BendingBlock block : blocks) {

            if (sync) {
                loop++;
                PvPDojo.schedule(block::revertBlock).createTask(loop);
            } else {
                block.revertBlock();
            }

            if (sync && loop >= blocks.size()) {
                cancel();
            }
        }
    }

    private void cancel() {
        cancelled = true;
        if (source != null) {
            source.revertBlock();
        }
    }

    public Vector getDirection(final Location location, final Location destination) {
        double x1, y1, z1;
        double x0, y0, z0;

        x1 = destination.getX();
        y1 = destination.getY();
        z1 = destination.getZ();

        x0 = location.getX();
        y0 = location.getY();
        z0 = location.getZ();

        return new Vector(x1 - x0, y1 - y0, z1 - z0);
    }

}
