package com.pvpdojo.abilities.abilities.active;

import org.bukkit.DyeColor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.Event;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilities.gamemode.AbilityServer;

public class HadesAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "HadesAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        AbilityEntity entity = ability.getAbilityEntity();
        if (entity instanceof AbilityPlayer) {
            Player activator = ((AbilityPlayer) ability.getAbilityEntity()).getPlayer();
            final float amount = ability.getAttribute("amount");
            final float speed = ability.getAttribute("speed");
            for (int i = 0; i < amount; i++) {
                Wolf wolf = (Wolf) activator.getWorld().spawnEntity(activator.getLocation(), EntityType.WOLF);
                wolf.setOwner(activator);
                wolf.setCustomName(activator.getNick() + "'s Minions");

                wolf.setAdult();
                wolf.setCollarColor(DyeColor.RED);
                wolf.setCustomNameVisible(true);

                if (speed > 0) {
                    wolf.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 20, 1));
                }

                wolf.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20 * 20, 0));
                ability.getMetaData().addEntity(wolf, 40, true);

                AbilityServer.get().createEntity(wolf).setOwner(entity);
            }
        }
        return true;
    }

}
