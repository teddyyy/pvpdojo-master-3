/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileLaunchEvent;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;

public class PearlRiderAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "PearlRiderAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        return false;
    }

    @EventHandler
    public void onPearlRider(ProjectileLaunchEvent e) {
        if (e.getEntity() instanceof EnderPearl && e.getEntity().getShooter() instanceof LivingEntity && canAbilityExecute((LivingEntity) e.getEntity().getShooter())) {
            e.getEntity().setPassenger((Entity) e.getEntity().getShooter());
        }
    }
}
