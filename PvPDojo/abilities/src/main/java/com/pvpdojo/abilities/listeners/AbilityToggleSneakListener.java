/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerToggleSneakEvent;

import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilityinfo.AbilityType;
import com.pvpdojo.bukkit.listeners.DojoListener;

public class AbilityToggleSneakListener implements DojoListener {

    @EventHandler
    public void onSneak(PlayerToggleSneakEvent e) {
        if (e.isSneaking()) {
            AbilityPlayer champion = AbilityPlayer.get(e.getPlayer());
            champion.getAbilities().stream().filter(a -> a.getAbilityType() == AbilityType.SHIFT && a.isEnabled()).forEach(a -> a.activateAbility(true, e));
        }
    }

}
