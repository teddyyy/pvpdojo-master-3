/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Sound;
import org.bukkit.event.Event;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.abilities.AbilityMetaData;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.util.bukkit.ParticleEffects;

public class ShadowAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "ShadowAbility";
    }

    private String META_KEY = "shadow";

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        AbilityEntity activator = ability.getAbilityEntity();

        AbilityMetaData meta = ability.getMetaData();

        ParticleEffects.CLOUD.sendToLocation(activator.getEntity().getLocation(), 0, 0, 0, 0, 5);
        ParticleEffects.ENCHANTMENT_TABLE.sendToLocation(activator.getEntity().getLocation(), 0, 0, 0, 0, 5);
        ParticleEffects.ANGRY_VILLAGER.sendToLocation(activator.getEntity().getLocation(), 0, 1, 0, 0, 5);

        activator.getEntity().getWorld().playSound(activator.getEntity().getLocation(), Sound.WITHER_SPAWN, 3, 3);

        activator.getEntity().setVelocity(activator.getEntity().getEyeLocation().getDirection().multiply(7).setY(4));

        activator.getEntity().addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 20 * 20, 2));
        activator.getEntity().addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20 * 20, 2));
        meta.addData(META_KEY, "");

        activator.getEntity().setFallDistance(-100);

        new AbilityRunnable(ability) {

            @Override
            public void run() {

                if (activator.getEntity().isOnGround() && meta.hasData(META_KEY)) {
                    meta.removeData(META_KEY);
                    activator.getEntity().removePotionEffect(PotionEffectType.INVISIBILITY);
                    activator.getEntity().removePotionEffect(PotionEffectType.BLINDNESS);

                    activator.getEntity().setFallDistance(0);

                    this.cancel();
                }

            }
        }.createTimer(20, 1, -1);

        return true;
    }

}
