package com.pvpdojo.abilities.base.entities;

import org.bukkit.entity.LivingEntity;

public class AbilityLivingEntity extends AbilityEntity {

    public AbilityLivingEntity(LivingEntity livingEntity) {
        super(livingEntity);
    }

    @Override
    public LivingEntity getEntity() {
        return (LivingEntity) super.getEntity();
    }

}
