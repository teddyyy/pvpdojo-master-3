package com.pvpdojo.abilities.abilities.effects;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.active.AvatarAbility;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.gamemode.BurnEffect;
import com.pvpdojo.bukkit.util.TempBlock;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.ExpireArrayList;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.bukkit.ParticleEffects;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class AvatarEffect {

    /*
     * This class is filled up with the most useless math equations you will ever witness.
     */

    private static final double RADIUS = 3.5D;
    private static final double OFFSETY = 1.5D;

    public final int DELAY = 20 * 3;

    private static final ExpireArrayList<AbilityEntity> NEARBY = new ExpireArrayList<>();

    private AbilityEntity activator;
    private Ability ability;


    public AvatarEffect(Ability ability) {
        this.ability = ability;
        this.activator = ability.getAbilityEntity();
    }

    public void activationEffect() {
        ability.getMetaData().addData(AvatarAbility.USE, null);

        new AbilityRunnable(ability) {

            int loop = 0;
            double t = 0;

            @Override
            public void run() {
                if (AvatarEffect.this.cancel()) {
                    stop();
                    removeEffects();
                    this.cancel();
                    return;
                }

                Location loc = activator.getEntity().getLocation();
                // Circular Effect
                if (++loop <= DELAY) {
                    double cir_R = loop - 10;

                    for (int i = 0; i < 45; i++) {
                        double x = Math.cos(i) * cir_R;
                        double y = 0;
                        double z = Math.sin(i) * cir_R;

                        loc.add(x, y, z);
                        display(ParticleEffects.CLOUD, loc);
                        loc.subtract(x, y, z);
                    }
                    // Helix Effect
                    t += Math.PI / 22;
                    double Hel_R = 1.5;
                    double x, y, z;

                    for (int i = 0; i < Hel_R * 2; i++) {// Creates multiple helixs
                        x = Math.cos(t + i) * Hel_R;
                        y = t / t;
                        z = Math.sin(t + i) * Hel_R;

                        loc.add(x, y, z);

                        display(ParticleEffects.CLOUD, loc);

                        for (AbilityEntity near : AbilityServer.get().getNearbyEntities(activator, 5.45f)) {
                            if (!NEARBY.contains(near)) {
                                NEARBY.add(near, 2, TimeUnit.SECONDS);
                                near.getEntity()
                                        .setVelocity(near.getEntity().getVelocity()
                                                .add(near.getEntity().getLocation().toVector()
                                                        .subtract(activator.getEntity().getLocation().toVector())
                                                        .normalize().multiply(2.4).add(new Vector(0, 0.8, 0))));
                                activator.abilityDamage(near, 75, ability, DamageCause.CONTACT);
                            }
                        }

                        loc.subtract(x, y, z);

                        // Other side of the helix
                        x = -x;
                        z = -z;

                        loc.add(x, y, z);
                        display(ParticleEffects.CLOUD, loc);
                        loc.subtract(x, y, z);
                    }

                    activator.getEntity().setVelocity(new Vector(0.0D, 0.2D, 0.0D));

//					if (loop % 10 >= 3) {
//						player.getLocation().getWorld().playSound(player.getLocation(), Sound.WITHER_IDLE,
//								new Random().nextInt(5), 0);
//					}

                }
                if (loop >= 20 * 20) {
                    stop();
                    this.cancel();
                }

            }
        }.createTimer(0, -1);
    }

    public void cloudSphere() {
        new AbilityRunnable(ability) {

            Location loc = activator.getEntity().getLocation();

            int loop = 20 * 70;

            @Override
            public void run() {

                if (AvatarEffect.this.cancel() || !activator.getEntity().hasMetadata(AvatarAbility.AIR) || --loop <= 0) {
                    removeEffects();
                    stop();
                    this.cancel();
                    return;
                }

                Location loc = activator.getEntity().getLocation().add(0, OFFSETY, 0);
                for (int i = 0; i < 25; i++) {
                    Vector vector = getRandomVelocity().multiply(RADIUS);

                    loc.add(vector);
                    display(ParticleEffects.CLOUD, loc);
                    loc.subtract(vector);
                }

                for (AbilityEntity near : AbilityServer.get().getNearbyEntities(activator, loc, 1.5, 1.5)) {
                    if (!NEARBY.contains(near)) {
                        NEARBY.add(near, 1, TimeUnit.SECONDS);
                        near.getEntity()
                                .setVelocity(near.getEntity().getVelocity()
                                        .add(near.getEntity().getLocation().toVector()
                                                .subtract(activator.getEntity().getLocation().toVector())
                                                .normalize().multiply(1.0D).add(new Vector(0, 0.54f, 0))));
                        activator.abilityDamage(near, 25, ability, DamageCause.CONTACT);
                    }
                }

            }
        }.createTimer(0, -1);
    }

    private static double ringsIncr = 0.1;

    public void fireRing() {
        new AbilityRunnable(ability) {

            double loop, x = 0, y = 0, z = 0, r = RADIUS + 1;

            @Override
            public void run() {

                if (AvatarEffect.this.cancel()) {
                    removeEffects();
                    this.cancel();
                    return;
                }

                if (!activator.getEntity().hasMetadata(AvatarAbility.FIRE)) {
                    this.cancel();
                    return;
                }

                Location loc = activator.getEntity().getLocation().add(0, OFFSETY, 0);
                loop++;

                x = Math.cos(loop * -ringsIncr) * r;
                y = 0;
                z = Math.sin(loop * -ringsIncr) * r;

                loc.add(x, y, z);
                for (int i = 0; i < 20; i++) {
                    display(ParticleEffects.FIRE, loc);
                }
                loc.subtract(x, y, z);

            }
        }.createTimer(0, -1);

    }

    public void waterRing() {
        new AbilityRunnable(ability) {
            double loop, x = 0, z = 0, r = RADIUS - 0.5D;

            @Override
            public void run() {

                if (AvatarEffect.this.cancel()) {
                    removeEffects();
                    this.cancel();
                    return;
                }

                if (!activator.getEntity().hasMetadata(AvatarAbility.WATER)) {
                    this.cancel();
                    return;
                }

                Location loc = activator.getEntity().getLocation().add(0, OFFSETY, 0);
                loop++;

                x = Math.cos(loop * -ringsIncr) * r;
                z = Math.sin(loop * -ringsIncr) * r;

                loc.add(x, x, z);
                for (int i = 0; i < 20; i++) {
                    display(ParticleEffects.DRIP_WATER, loc);
                }
                loc.subtract(x, x, z);
            }
        }.createTimer(0, -1);

    }

    public void earthRing() {
        new AbilityRunnable(ability) {
            double loop, x = 0, z = 0, r = RADIUS - 0.5D;

            @Override
            public void run() {

                if (AvatarEffect.this.cancel()) {
                    removeEffects();
                    this.cancel();
                    return;
                }

                if (!activator.getEntity().hasMetadata(AvatarAbility.EARTH)) {
                    this.cancel();
                    return;
                }

                Location loc = activator.getEntity().getLocation().add(0, OFFSETY, 0);
                loop++;

                x = Math.cos(loop * -ringsIncr) * r;
                z = Math.sin(loop * -ringsIncr) * r;

                loc.add(-x, x, z);
                for (int i = 0; i < 20; i++) {
                    activator.getEntity().getWorld().playEffect(loc, Effect.TILE_BREAK, 3);
                    // Works same as STEP_SOUND but without sounds.
                }
                loc.subtract(-x, x, z);
            }
        }.createTimer(0, -1);

    }

    public void displayAll() {
        if (activator.getEntity() instanceof Player) {
            Player player = (Player) activator.getEntity();
            player.setAllowFlight(true);
            player.setFlying(true);
        }

        activator.getEntity().setMetadata(AvatarAbility.AIR, new FixedMetadataValue(PvPDojo.get(), null));
        activator.getEntity().setMetadata(AvatarAbility.EARTH, new FixedMetadataValue(PvPDojo.get(), null));
        activator.getEntity().setMetadata(AvatarAbility.WATER, new FixedMetadataValue(PvPDojo.get(), null));
        activator.getEntity().setMetadata(AvatarAbility.FIRE, new FixedMetadataValue(PvPDojo.get(), null));

        cloudSphere();
        waterRing();
        fireRing();
        earthRing();
    }

    private boolean cancel() {
        return ability.getCooldown().isCoolingDown();
    }

    public void useWaterBender() {
        if (activator.getEntity().hasMetadata(AvatarAbility.WATER)) {

            activator.getEntity().removeMetadata(AvatarAbility.WATER, PvPDojo.get());

            // Shoots a Beam

            new AbilityRunnable(ability) {

                double loop = 0;
                double length = 25;
                double radius = 0.7;

                @Override
                public void run() {

                    if (loop > length) {
                        this.cancel();
                        return;
                    }

                    Location loc = activator.getEntity().getEyeLocation();// Controllable
                    loop += Math.PI / 17;
                    double x, y, z;

                    for (double i = 0; i < radius * 2; i += 0.5) {// Creates multiple helixs
                        x = Math.cos(loop + i) * radius;
                        y = Math.sin(loop + i) * radius;
                        z = loop * loop;

                        // FIRST SIDE OF THE HELIX
                        Vector v = ParticleEffectRotation.rotateFunction(new Vector(x, y, z), loc);

                        loc.add(v.getX(), v.getY(), v.getZ());

                        display(ParticleEffects.DRIP_WATER, loc);

                        for (AbilityEntity nearby : AbilityServer.get().getNearbyEntities(activator, loc, 4.5, 2.5)) {
                            activator.abilityDamage(nearby, 100, ability, DamageCause.MAGIC);
                            nearby.getEntity().setFireTicks(0);
                        }

                        loc.subtract(v.getX(), v.getY(), v.getZ());

                        // SECOND SIDE OF THE HELIX
                        x = -x;
                        y = -y;

                        v = ParticleEffectRotation.rotateFunction(new Vector(x, y, z), loc);

                        loc.add(v.getX(), v.getY(), v.getZ());
                        display(ParticleEffects.DRIP_WATER, loc);
                        loc.subtract(v.getX(), v.getY(), v.getZ());

                    }
                }
            }.createTimer(0, -1);
        }
    }

    public void useEarthBender() {
        if (activator.getEntity().hasMetadata(AvatarAbility.EARTH)) {
            activator.getEntity().removeMetadata(AvatarAbility.EARTH, PvPDojo.get());

            // Throws blocks out of the ground

            new AbilityRunnable(ability) {

                int loop = 0;
                int length = 5;

                int y = 0;
                int maxHeight = 10;

                List<AbilityEntity> target = new ArrayList<>();

                @Override
                public void run() {

                    if (++loop > length) {
                        target.clear();
                        this.cancel();
                        return;
                    }

                    // Adding nearby entities to the list
                    AbilityServer.get().getNearbyEntities(activator, 10).stream().filter(near -> !target.contains(near)).forEach(near -> target.add(near));

                    if (target.size() != 0) {

                        for (AbilityEntity entity : target) {
                            entity.getEntity().setVelocity(new Vector(0.1, 1.5, 0));

                            PvPDojo.schedule(() -> {
                                Block block = BlockUtil.getFirstBlockDown(entity.getEntity().getLocation(), null)
                                        .getBlock();

                                if (block != null) {
                                    if (y <= maxHeight) {
                                        Material type = block.getType();
                                        byte data = block.getData();
                                        Block blockUp = block.getRelative(BlockFace.UP);

                                        if (blockUp.getLocation().getBlock().getType() == Material.AIR) {
                                            TempBlock.createTempBlock(blockUp.getLocation(), type, data, 3);
                                        }

                                        for (int i = 0; i < y; i++) {
                                            blockUp = blockUp.getRelative(BlockFace.UP);
                                            TempBlock.createTempBlock(blockUp.getLocation(), type, data, 3);
                                        }
                                    }
                                }
                                y += 3.5;
                            }).createTask(5);
                        }
                    }
                }
            }.createTimer(0, -1);
        }
    }

    public void useFireBender() {
        if (activator.getEntity().hasMetadata(AvatarAbility.FIRE)) {
            activator.getEntity().removeMetadata(AvatarAbility.FIRE, PvPDojo.get());

            //Fire breathing.

            activator.getEntity().getLocation().getWorld().playSound(activator.getEntity().getLocation(), Sound.BLAZE_BREATH, 0.5f, 0.6f);
            new AbilityRunnable(ability) {

                double x, y, z, loop, radius, length = 5;
                Location loc = activator.getEntity().getEyeLocation();
                Vector direc = loc.getDirection();

                @Override
                public void run() {
                    if (loop > length) {
                        cancel();
                        return;
                    }

                    for (double i = 0; i < radius * 4; i += loop < 0.65 ? 1.0 : 0.5) {
                        x = Math.cos(i * radius) * loop;
                        y = Math.sin(i * radius) * loop;
                        z = loop * loop;

                        Vector v = new Vector(x, y, z);
                        v = ParticleEffectRotation.rotateFunction(v, loc);

                        loc.add(v.getX(), v.getY(), v.getZ());
                        ParticleEffects.FIRE.sendToLocation(loc, 0, 0, 0, 0, 1);
                        loc.subtract(v.getX(), v.getY(), v.getZ());
                    }

                    x = direc.getX() * Math.pow(loop, 2);
                    y = direc.getY() * Math.pow(loop, 2);
                    z = direc.getZ() * Math.pow(loop, 2);

                    loc.add(x, y, z);
                    if (check()) {

                        for (int x = 0; x < 25; x++) {
                            ParticleEffects.FIRE.sendToLocation(loc, 0, 0, 0, 0.4f, x);
                        }

                        loc.getWorld().playSound(loc, Sound.GHAST_FIREBALL, 1, 1);

                        cancel();
                        return;
                    }
                    loc.subtract(x, y, z);

                    loop += 0.15;
                    radius += 0.08f;

                }

                private boolean check() {
                    attackNearbyEntities();
                    return loc.getBlock().getType() != Material.AIR;
                }

                private void attackNearbyEntities() {
                    AbilityServer.get()
                            .getNearbyEntities(activator, loc, radius + 1.2f, 0.76)
                            .forEach(target -> {
                                target.applyEffect(new BurnEffect(20 * 7));
                                activator.abilityDamage(target, 175, ability, DamageCause.FIRE);
                            });
                }

            }.createTimer(0, -1);

        }

    }

    public void useAirBender() {
        if (activator.getEntity().hasMetadata(AvatarAbility.AIR)) {
            activator.getEntity().removeMetadata(AvatarAbility.AIR, PvPDojo.get());

            // Increases size of the Cloud Sphere
            new AbilityRunnable(ability) {

                double loop;
                double length = 10;

                @Override
                public void run() {
                    Location loc = activator.getEntity().getLocation();

                    if (++loop > length) {
                        this.cancel();
                        return;
                    }

                    for (int i = 0; i < 25; i++) {
                        Vector vector = getRandomVelocity().multiply(loop);
                        loc.add(vector);
                        display(ParticleEffects.CLOUD, loc);

                        for (AbilityEntity nearby : AbilityServer.get().getNearbyEntities(activator, loc, loop, loop)) {
                            activator.abilityDamage(nearby, 75, ability, DamageCause.CONTACT);

                            nearby.getEntity()
                                    .setVelocity(nearby.getEntity().getVelocity()
                                            .add(nearby.getEntity().getLocation().toVector()
                                                    .subtract(activator.getEntity().getLocation().toVector())
                                                    .normalize().multiply(4.2).add(new Vector(0, 1.5, 0))));
                        }
                        loc.subtract(vector);
                    }
                }
            }.createTimer(0, -1);
        }

    }

    private void display(ParticleEffects effect, Location loc) {
        effect.sendToLocation(loc, 0, 0, 0, 0, 1);
    }

 /*   private boolean isInside(Location loc, double radius) {
        if (activator.getEntity().getLocation().getBlockY() >= loc.clone().getBlockY() + 15.0 ||
                activator.getEntity().getLocation().getBlockY() <= loc.clone().getBlockY() - 15.0 ||
                activator.getEntity().getLocation().getBlockX() + radius <= loc.clone().getBlockX() ||
                activator.getEntity().getLocation().getBlockZ() + radius <= loc.clone().getBlockZ()) {
            return false;
        }

        return true;
    }
*/

    private void stop() {
        forceCooldown();
        ability.getMetaData().removeData(AvatarAbility.USE);
    }

    private void forceCooldown() {
        if (!ability.getCooldown().isCoolingDown()) {
            ability.activateAbility(false, null);
            ability.callItemTimer(false);
        }
    }

    private void removeEffects() {
        PvPDojo.schedule(() -> {
            if (activator.getEntity() instanceof Player) {
                Player player = (Player) activator.getEntity();
                User.getUser(player).setCancelNextFall(true);
                player.setAllowFlight(false);
                player.setFlySpeed(0.1f);
                player.setFlying(false);
                player.setVelocity(new Vector(0, -3, 0));
            }
        }).createTask(20);

    }

    private Vector getRandomVelocity() {
        final Random random = new Random(System.nanoTime());

        double x = random.nextDouble() * 2 - 1;
        double y = random.nextDouble() * 2 - 1;
        double z = random.nextDouble() * 2 - 1;

        return new Vector(x, y, z).normalize();
    }

}
