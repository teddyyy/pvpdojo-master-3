/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.Event;
import org.bukkit.potion.PotionType;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.util.bukkit.ItemBuilder.PotionBuilder;

public class DebuffIIIAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "DebuffIIIAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        ThrownPotion pot = ability.getAbilityEntity().getEntity().launchProjectile(ThrownPotion.class);
        pot.setItem(new PotionBuilder(PotionType.POISON).level(2).splash().build());
        return true;
    }
}
