/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.lib;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

import org.bukkit.entity.Entity;

import com.pvpdojo.util.TimeUtil;

public class TemporaryEntity {

    private WeakReference<Entity> entity;
    private long creation;
    private int timeout;

    public TemporaryEntity(Entity entity, int timeoutSeconds) {
        this.entity = new WeakReference<>(entity);
        this.timeout = timeoutSeconds;
        this.creation = System.currentTimeMillis();
    }

    public void tick() {
        if (TimeUtil.hasPassed(creation, TimeUnit.SECONDS, timeout)) {
            remove();
        }
    }

    public void remove() {
        Entity removal = entity.get();
        if (removal != null) {
            removal.remove();
        }
    }

    public int getId() {
        Entity en = entity.get();
        if (en != null) {
            return en.getEntityId();
        }
        return -1;
    }

    public boolean isDead() {
        Entity en = entity.get();
        return en == null || en.isDead();
    }

}
