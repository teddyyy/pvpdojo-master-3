/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.guns;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;

public class GunEffect {
    private int maxDuration;
    private int duration;
    private Effect type;
    private double radius;
    private Location location;
    private byte specialDat = -1;

    public GunEffect(int duration, double radius, Effect type) {
        this.duration = duration;
        this.maxDuration = duration;
        this.type = type;
        this.radius = radius;
    }

    public void start(Location location) {
        this.location = location;
        this.duration = this.maxDuration;

        /* PvPDojo.getInstance().getGunLoader().addEffect(this); */
    }

    public GunEffect clone() {
        return new GunEffect(this.maxDuration, this.radius, this.type).setSpecialDat(this.specialDat);
    }

    public void tick() {
        this.duration -= 1;

        if (this.duration < 0) {
            // PvPDojo.getInstance().getGunLoader().removeEffect(this);
            return;
        }

        double yRad = this.radius;
        if (this.type.equals(Effect.MOBSPAWNER_FLAMES)) {
            yRad = 0.75D;

            for (Player player : Bukkit.getOnlinePlayers()) {
                if (player.getWorld().getUID() == this.location.getWorld().getUID()) {
                    if (this.location.distance(player.getLocation()) < this.radius) {
                        player.setFireTicks(20);
                    }
                }
            }
        }

        for (double i = -this.radius; i <= this.radius; i += 1.0D) {
            for (double ii = -this.radius; ii <= this.radius; ii += 1.0D) {
                for (double iii = 0.0D; iii <= yRad * 2.0D; iii += 1.0D) {
                    int rand = PvPDojo.RANDOM.nextInt(8);
                    if (rand == 2) {
                        Location newloc = this.location.clone().add(i, iii - 1.0D, ii);
                        Location testLoc = this.location.clone().add(0.0D, yRad - 1.0D, 0.0D);
                        if (newloc.distance(testLoc) <= this.radius) {
                            byte dat = (byte) PvPDojo.RANDOM.nextInt(8);

                            if (this.specialDat > -1) {
                                dat = this.specialDat;
                            }
                            newloc.getWorld().playEffect(newloc, this.type, dat);
                        }
                    }
                }
            }
        }
    }

    public GunEffect setSpecialDat(byte specialDat) {
        this.specialDat = specialDat;

        return this;
    }

    public int getMaxDuration() {
        return this.maxDuration;
    }

    public int getDuration() {
        return this.duration;
    }

    public Effect getType() {
        return this.type;
    }

    public double getRadius() {
        return this.radius;
    }

    public Location getLocation() {
        return this.location;
    }

    public byte getSpecialDat() {
        return this.specialDat;
    }

    public void setMaxDuration(int maxDuration) {
        this.maxDuration = maxDuration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setType(Effect type) {
        this.type = type;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}