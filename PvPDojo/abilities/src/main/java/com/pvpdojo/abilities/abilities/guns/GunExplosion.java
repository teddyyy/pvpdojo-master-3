/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.guns;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

import com.pvpdojo.PvPDojo;

public class GunExplosion {
    private Location location;

    public GunExplosion(Location location) {
        this.location = location;
    }

    public void explode() {
        final Firework fw = (Firework) location.getWorld().spawnEntity(location, EntityType.FIREWORK);
        FireworkMeta fm = fw.getFireworkMeta();
        fm.addEffect(getFirework());
        fw.setFireworkMeta(fm);

        PvPDojo.schedule(fw::detonate).nextTick();
    }

    public FireworkEffect getFirework() {
        Random rand = PvPDojo.RANDOM;

        FireworkEffect.Type type = FireworkEffect.Type.BALL_LARGE;
        if (rand.nextInt(2) == 0)
            type = FireworkEffect.Type.BURST;

        ArrayList<Color> c = new ArrayList<>();

        c.add(Color.RED);
        c.add(Color.RED);
        c.add(Color.RED);
        c.add(Color.ORANGE);
        c.add(Color.ORANGE);
        c.add(Color.ORANGE);
        c.add(Color.BLACK);
        c.add(Color.GRAY);

        return FireworkEffect.builder().flicker(true).withColor(c).withFade(c).with(type).trail(true).build();
    }
}