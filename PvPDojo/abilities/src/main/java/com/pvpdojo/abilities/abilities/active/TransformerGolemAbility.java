/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.craftbukkit.v1_7_R4.entity.disguise.CraftCreatureDisguise;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.disguise.CreatureDisguise;
import org.bukkit.event.Event;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;

public class TransformerGolemAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "TransformerGolemAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        if (ability.getAbilityEntity().getEntity().getDisguise() == null) {
            LivingEntity entity = ability.getAbilityEntity().getEntity();

            entity.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.MAX_VALUE, 1));
            entity.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 0));
            entity.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 1));

            CreatureDisguise disguise = new CraftCreatureDisguise(ability.getAbilityEntity().getEntity(), EntityType.IRON_GOLEM);
            disguise.setCustomName(entity instanceof Player ? ((Player) entity).getNick() : entity.getCustomName());
            disguise.setCustomNameVisible(true);

            ability.getAbilityEntity().getEntity().setDisguise(disguise);
            new AbilityRunnable(ability) {
                @Override
                public void run() {
                    cancel();
                }

                @Override
                public void cancel() {
                    super.cancel();
                    entity.setDisguise(null);
                    entity.removePotionEffect(PotionEffectType.HEALTH_BOOST);
                    entity.removePotionEffect(PotionEffectType.SLOW);
                    entity.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
                }
            }.createTask(20 * 12);
            return true;
        }

        return false;
    }
}
