/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.effects;

import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.lang.MessageKeys;

public class ShockWaveEffect {

    private final Ability ability;
    private final AbilityEntity activator;

    public ShockWaveEffect(Ability ability) {
        this.ability = ability;
        this.activator = ability.getAbilityEntity();
    }

    public void start() {
        int nearby = AbilityServer.get().getNearbyEntities(activator, ability.getAttribute("radius")).size();
        if (nearby < 1) {
            activator.sendMessage(MessageKeys.NO_PLAYER_NEARBY);
            return;
        }
        ability.activateAbility(false, null);
        new AbilityRunnable(ability) {
            @Override
            public void run() {
                pushBackNearby();
            }
        }.createTimer(20, (int) (ability.getAttribute("duration")));
    }

    private void pushBackNearby() {
        for (AbilityEntity nearby : AbilityServer.get().getNearbyEntities(activator, ability.getAttribute("radius"))) {
            if (activator.abilityDamage(nearby, ability.getAttribute("ability_damage"), ability, DamageCause.ENTITY_ATTACK)) {
                nearby.getEntity().setVelocity(activator.getEntity().getLocation().getDirection().multiply(1.4).setY(1.0));
            }
        }
    }
}
