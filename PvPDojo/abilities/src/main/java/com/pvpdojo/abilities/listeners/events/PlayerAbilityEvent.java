/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.listeners.events;

import com.pvpdojo.abilities.base.entities.AbilityPlayer;

public abstract class PlayerAbilityEvent extends AbilityEvent {

    public PlayerAbilityEvent(AbilityPlayer player) {
        super(player);
    }

    @Override
    public AbilityPlayer getEntity() {
        return (AbilityPlayer) super.getEntity();
    }

}
