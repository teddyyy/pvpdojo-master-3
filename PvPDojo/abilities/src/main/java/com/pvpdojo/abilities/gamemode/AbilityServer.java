/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.gamemode;

import com.pvpdojo.abilities.Abilities;
import com.pvpdojo.abilities.base.entities.*;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.bukkit.util.TempBlock;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Animals;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.stream.Collectors;

import static com.pvpdojo.util.StreamUtils.negate;

public class AbilityServer {

    private Map<UUID, AbilityEntity> entityMap = new HashMap<>();
    private static final AbilityServer instance = new AbilityServer();

    public static AbilityServer get() {
        return instance;
    }

    public void update() {
        this.updateEntities();
        Abilities.timing("Blocks").startTiming();
        this.updateBlocks();
        Abilities.timing("Blocks").stopTiming();
    }

    public void updateEntities() {
        Abilities.timing("Entities").startTiming();
        Abilities.timing("Entity Removal").startTiming();
        entityMap.values().removeIf(entity -> !entity.getEntity().isValid() && !(entity.getEntity() instanceof Player));
        Abilities.timing("Entity Removal").stopTiming();
        Abilities.timing("Entities Update").startTiming();
        for (AbilityEntity entity : entityMap.values()) {
            entity.update();
        }
        Abilities.timing("Entities Update").stopTiming();
        Abilities.timing("Entities").stopTiming();
    }

    public void updateBlocks() {
        for (int i = TempBlock.getTempBlocks().size() - 1; i >= 0; i--) {
            TempBlock temp = TempBlock.getTempBlocks().get(i);
            if (temp == null)
                continue;
            if (TempBlock.checkTempBlocks(temp)) {
                temp.reset();
            }
        }
    }

    public void createPlayer(Player player) {
        AbilityPlayer abilityPlayer = new AbilityPlayer(player);
        entityMap.put(player.getUniqueId(), abilityPlayer);
    }

    public AbilityEntity createEntity(LivingEntity entity) {
        AbilityEntity abilityEntity = null;
        if (entity instanceof Monster) {
            abilityEntity = new AbilityMonster((Monster) entity);
        } else if (entity instanceof Animals) {
            abilityEntity = new AbilityAnimal((Animals) entity);
        } else if (entity instanceof LivingEntity)
            abilityEntity = new AbilityLivingEntity(entity);

        if (abilityEntity != null) {
            entityMap.put(entity.getUniqueId(), abilityEntity);
        }
        return abilityEntity;
    }

    public void removeEntity(AbilityEntity entity) {
        entity.cleanup();
        this.entityMap.remove(entity.getEntity().getUniqueId());
    }

    public boolean containsEntity(LivingEntity living) {
        if (living == null) {
            return false;
        }
        return entityMap.containsKey(living.getUniqueId());
    }

    public void shutdown() {
        for (int i = TempBlock.getTempBlocks().size() - 1; i >= 0; i--) {
            TempBlock temp = TempBlock.getTempBlocks().get(i);
            temp.reset();
        }
    }

    public List<AbilityEntity> getNearbyEntities(Location from, double radius, double radiusY) {
        return BukkitUtil.getNearbyEntities(from, radius, radiusY, radius).stream()
                .map(AbilityEntity::get)
                .filter(Objects::nonNull)
                .filter(AbilityEntity::isTargetable)
                .collect(Collectors.toList());
    }

    public List<AbilityEntity> getEntities(World world) {//get all entities in a selected world.
        return world.getEntities()
                .stream()
                .map(AbilityEntity::get)
                .filter(Objects::nonNull)
                .filter(AbilityEntity::isTargetable)
                .collect(Collectors.toList());
    }

    public List<AbilityEntity> getNearbyEntities(AbilityEntity e, double radius, double radiusY) {
        if (e == null) {
            return Collections.emptyList();
        }
        return getNearbyEntities(e, e.getEntity().getLocation(), radius, radiusY);
    }

    public List<AbilityEntity> getNearbyEntities(AbilityEntity e, Location location, double radius, double radiusY) {
        if (e == null) {
            return Collections.emptyList();
        }
        List<AbilityEntity> nearby = getNearbyEntities(location, radius, radiusY);
        nearby.removeIf(negate(e::canTarget));
        return nearby;
    }

    public List<AbilityEntity> getNearbyEntities(AbilityEntity e, double radius) {
        return getNearbyEntities(e, radius, radius);
    }

    public AbilityPlayer getPlayer(Player player) {
        if (player != null) {
            return (AbilityPlayer) entityMap.get(player.getUniqueId());
        }
        throw new IllegalArgumentException("invalid player");
    }

    public AbilityEntity getEntity(LivingEntity living) {
        if (!containsEntity(living)) {
            return createEntity(living);
        }
        return entityMap.get(living.getUniqueId());
    }

}
