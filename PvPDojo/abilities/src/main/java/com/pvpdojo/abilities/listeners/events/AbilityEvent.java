/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.listeners.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.pvpdojo.abilities.base.entities.AbilityEntity;

public abstract class AbilityEvent extends Event {

    private static final HandlerList handlerList = new HandlerList();

    private AbilityEntity entity;

    public AbilityEvent(AbilityEntity entity) {
        this.entity = entity;
    }

    public AbilityEntity getEntity() {
        return entity;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList() {
        return handlerList;
    }

}
