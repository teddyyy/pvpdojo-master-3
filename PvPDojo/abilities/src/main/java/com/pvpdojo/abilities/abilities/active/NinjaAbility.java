/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.util.Vector;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;

public class NinjaAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "NinjaAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        final AbilityEntity activator = ability.getAbilityEntity();
        if (!ability.getMetaData().hasTarget()) {
            return false;
        }
        final LivingEntity target = ability.getMetaData().getTarget().getEntity();
        Vector vec = target.getLocation().getDirection().multiply(-1);
        Location loc = target.getLocation().add(vec);
        if (!loc.getBlock().getType().equals(Material.AIR))
            loc = target.getLocation();

        loc.setPitch(activator.getEntity().getLocation().getPitch());
        loc.setYaw(target.getLocation().getYaw());
        activator.getEntity().teleport(loc);
        target.getWorld().playSound(loc, Sound.ENDERMAN_TELEPORT, 1f, 3);
        return true;
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (!(e.getDamager() instanceof LivingEntity) || !(e.getEntity() instanceof LivingEntity))
            return;
        LivingEntity damager = (LivingEntity) e.getDamager();
        if (!canAbilityExecute(damager))
            return;
        Ability ability = getAbility(damager);
        AbilityEntity target = AbilityServer.get().getEntity((LivingEntity) e.getEntity());
        if (target == null) {
            return;
        }
        ability.getMetaData().addTarget(target, 20);
    }

}
