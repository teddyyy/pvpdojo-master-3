/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.gamemode.BurnEffect;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.ParticleEffects;

public class SmashAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "SmashAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        Location location = ability.getAbilityEntity().getEntity().getLocation();
        for (int x = -2; x <= 2; x++) {
            for (int z = -2; z <= 2; z++) {
                Location cur = location.clone().add(x, -1, z);
                Material material = cur.getBlock().getType();
                if (material != Material.AIR) {
                    location.getWorld().playEffect(cur, Effect.STEP_SOUND, material);
                }
            }
        }

        AbilityServer.get().getNearbyEntities(ability.getAbilityEntity(), 5).forEach(target -> {
            ability.getAbilityEntity().abilityDamage(target, ability.getAttribute("ability_damage"), ability, DamageCause.MAGIC);
            target.applyEffect(new BurnEffect(15 * 20));
            target.getEntity().setVelocity(new Vector(0, 2, 0));
            Location loc = target.getEntity().getLocation();
            ParticleEffects.LARGE_EXPLODE.sendToLocation(loc, 0, 0, 0, 0, 1);
            loc.getWorld().playSound(loc, Sound.EXPLODE, .3f, 1);
            if (target instanceof AbilityPlayer) {
                PvPDojo.schedule(() -> User.getUser(((AbilityPlayer) target).getPlayer()).cancelNextFall()).createTask(30);
            }
        });

        return true;
    }
}
