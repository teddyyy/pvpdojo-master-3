/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.ParticleEffects;

public class SupermanAbility extends AbilityExecutor {

    private static final String SUPERMAN_FLYING = "superman_flying";

    @Override
    public String getExecutorName() {
        return "SupermanAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        if (ability.getAbilityEntity() instanceof AbilityPlayer) {

            Player player = ((AbilityPlayer) ability.getAbilityEntity()).getPlayer();

            player.setMetadata(SUPERMAN_FLYING, new FixedMetadataValue(PvPDojo.get(), null));
            player.setVelocity(new Vector(.1, 1.5, .1));

            final int duration = (int) ability.getAttribute("duration");

            player.setAllowFlight(true);

            new AbilityRunnable(ability) {
                int x = 0;

                public void run() {
                    x++;
                    player.setAllowFlight(true);
                    player.setFlying(true);

                    if (x > duration) {
                        cancel();
                    }
                }

                @Override
                public void cancel() {
                    super.cancel();
                    player.removeMetadata(SUPERMAN_FLYING, PvPDojo.get());
                    player.setAllowFlight(false);
                    player.setFlySpeed((float) .1);
                    player.setFlying(false);
                    player.setVelocity(new Vector(0, -3, 0));
                    User.getUser(player).setCancelNextFall(true);
                }
            }.createTimer(2, 20, -1);

        }

        return true;
    }

    @EventHandler
    public void onSupermanEffect(PlayerMoveEvent e) {
        if (e.getPlayer().hasMetadata(SUPERMAN_FLYING) && e.getTo().getBlock() != e.getFrom().getBlock()) {
            ParticleEffects.FIRE.sendToLocation(e.getFrom(), .2F, .2F, .2F, 0, 5);
            ParticleEffects.FIRE.sendToLocation(e.getPlayer().getEyeLocation(), .2F, .2F, .2F, 0, 5);
        }
    }

}
