/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.ProjectileEntity;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.BurnEffect;

public class BeamAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "BeamAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        final AbilityEntity activator = ability.getAbilityEntity();
        final boolean collateral = false;
        final float fire_damage = ability.getAttribute("fire_damage");
        final float speed = ability.getAttribute("projectile_speed");
        final float distance = ability.getAttribute("distance");
        final float damage = ability.getAttribute("ability_damage");

        ProjectileEntity projectile = new ProjectileEntity(activator, collateral, 1f);
        projectile.setProjectileLoop(loc -> {
            loc.getWorld().playEffect(loc, Effect.STEP_SOUND, Material.REDSTONE_BLOCK);
            return false;
        });
        projectile.setProjectileDetection(target -> {
            activator.abilityDamage(target, damage, ability, DamageCause.MAGIC);
            if (fire_damage != 0) {
                target.applyEffect(new BurnEffect(5 * 20));
            }
        });
        projectile.startBlockCollision(activator.getEntity().getEyeLocation(), 1, (int) speed, (int) distance);
        return true;
    }

}
