package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.event.Event;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;

public class ReflectorAbility extends AbilityExecutor {

	@Override
	public String getExecutorName() {
		return "ReflectorAbility";
	}

	@Override
	public boolean activateAbility(Ability ability, Event event) {
		return true;
	}

}
