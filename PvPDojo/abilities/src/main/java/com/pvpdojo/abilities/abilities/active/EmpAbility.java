/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Location;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.gamemode.SilenceEffect;
import com.pvpdojo.util.bukkit.ParticleEffects;

public class EmpAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "EmpAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        AbilityEntity activator = ability.getAbilityEntity();
        final float radius = ability.getAttribute("radius");
        final float duration = ability.getAttribute("duration");
        final float damage = ability.getAttribute("ability_damage");
        for (AbilityEntity nearby : AbilityServer.get().getNearbyEntities(activator, radius)) {

            if (activator.abilityDamage(nearby, damage, ability, DamageCause.MAGIC)) {
                nearby.applyEffect(new SilenceEffect((int) duration * 20));
                nearby.getEntity().getWorld().strikeLightning(nearby.getEntity().getEyeLocation());
            }
        }
        createEffect(activator.getEntity().getEyeLocation(), radius);
        return true;
    }

    private void createEffect(Location loc, float radius) {
        for (int i = 0; i < 360; i = i + 20) {
            double increment = Math.PI * i / 180;
            double x = radius * Math.cos(increment);
            double z = radius * Math.sin(increment);
            Location newLoc = loc.clone().add(x, 0, z);
            ParticleEffects.LAVA_SPARK.sendToLocation(newLoc, 0, 0, 0, 0, 10);
        }
    }

}