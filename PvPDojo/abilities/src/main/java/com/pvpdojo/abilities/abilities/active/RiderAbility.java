/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Material;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.util.bukkit.CC;

import net.minecraft.server.v1_7_R4.GenericAttributes;

public class RiderAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "RiderAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        if (ability.getAbilityEntity() instanceof AbilityPlayer) {

            int duration = (int) ability.getAttribute("duration");

            Player player = ((AbilityPlayer) ability.getAbilityEntity()).getPlayer();

            Horse horse = player.getWorld().spawn(player.getLocation(), Horse.class);
            horse.setAdult();
            horse.setOwner(player);
            horse.getInventory().setArmor(new ItemStack(Material.DIAMOND_BARDING));
            horse.getInventory().setSaddle(new ItemStack(Material.SADDLE));
            horse.setJumpStrength(0.85);

            NMSUtils.get(horse).getAttributeInstance(GenericAttributes.d).setValue(0.25);

            ability.getMetaData().addEntity(horse, duration, true);
            horse.setPassenger(player);
            player.sendMessage(PvPDojo.PREFIX + CC.GREEN + "Enjoy your mount");
        }
        return true;
    }
}
