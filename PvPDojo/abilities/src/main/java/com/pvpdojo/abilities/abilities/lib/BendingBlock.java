package com.pvpdojo.abilities.abilities.lib;

import com.pvpdojo.PvPDojo;
import com.sk89q.worldguard.bukkit.RegionQuery;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Comparator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.ConcurrentHashMap;

public class BendingBlock {

    public static final PriorityQueue<BendingBlock> REVERT_QUEUE = new PriorityQueue<>(100, new Comparator<BendingBlock>() {
        @Override
        public int compare(final BendingBlock t1, final BendingBlock t2) {
            return (int) (t1.revertTime - t2.revertTime);
        }
    });

    public static Map<Block, BendingBlock> instances = new ConcurrentHashMap<Block, BendingBlock>();
    private final Block block;
    private byte newdata;
    private BlockState state;
    private long revertTime;
    private boolean inRevertQueue;
    private RevertTask revertTask = null;

    @SuppressWarnings("deprecation")
    public BendingBlock(final Block block, final Material newtype, final byte newdata) {
        this.block = block;
        this.newdata = newdata;
        if (instances.containsKey(block)) {

            RegionQuery query = WGBukkit.getPlugin().getRegionContainer().createQuery();
            if (!query.testState(block.getLocation(), (Player) null, DefaultFlag.PVP)) {
                return;
            }

            final BendingBlock temp = instances.get(block);
            if (newtype != temp.block.getType()) {
                temp.block.setType(newtype);
            }
            if (newdata != temp.block.getData()) {
                temp.block.setData(newdata);
                temp.newdata = newdata;
            }
            this.state = temp.state;
            instances.put(block, temp);
        } else {
            this.state = block.getState();
            instances.put(block, this);
            block.setType(newtype);
            block.setData(newdata);
        }
        if (this.state.getType() == Material.FIRE) {
            this.state.setType(Material.AIR);
        }
    }

    public static BendingBlock get(final Block block) {
        if (isBendingBlock(block)) {
            return instances.get(block);
        }
        return null;
    }

    public static boolean isBendingBlock(final Block block) {
        return block != null && instances.containsKey(block);
    }

    public static boolean isTouchingBendingBlock(final Block block) {
        final BlockFace[] faces = {BlockFace.NORTH, BlockFace.SOUTH, BlockFace.EAST, BlockFace.WEST, BlockFace.UP, BlockFace.DOWN};
        for (final BlockFace face : faces) {
            if (instances.containsKey(block.getRelative(face))) {
                return true;
            }
        }
        return false;
    }

    public static void removeAll() {
        for (final Block block : instances.keySet()) {
            revertBlock(block, Material.AIR);
        }
        for (final BendingBlock BendingBlock : REVERT_QUEUE) {
            BendingBlock.revertBlock();
        }
    }

    public static void removeBlock(final Block block) {
        instances.remove(block);
    }

    @SuppressWarnings("deprecation")
    public static void revertBlock(final Block block, final Material defaulttype) {
        if (instances.containsKey(block)) {
            instances.get(block).revertBlock();
        } else {
            if ((defaulttype == Material.LAVA || defaulttype == Material.STATIONARY_LAVA)) {
                block.setType(Material.LAVA);
                block.setData((byte) 0x0);
            } else if ((defaulttype == Material.WATER || defaulttype == Material.STATIONARY_WATER)) {
                block.setType(Material.WATER);
                block.setData((byte) 0x0);
            } else {
                block.setType(defaulttype);
            }
        }
    }

    public static void startReversion() {
        new BukkitRunnable() {
            @Override
            public void run() {
                final long currentTime = System.currentTimeMillis();
                while (!REVERT_QUEUE.isEmpty()) {
                    final BendingBlock BendingBlock = REVERT_QUEUE.peek();
                    if (currentTime >= BendingBlock.revertTime) {
                        REVERT_QUEUE.poll();
                        BendingBlock.revertBlock();
                    } else {
                        break;
                    }
                }
            }
        }.runTaskTimer(PvPDojo.get(), 0, 1);
    }

    public Block getBlock() {
        return this.block;
    }

    public Location getLocation() {
        return this.block.getLocation();
    }

    public BlockState getState() {
        return this.state;
    }

    public void setState(final BlockState newstate) {
        this.state = newstate;
    }

    public RevertTask getRevertTask() {
        return this.revertTask;
    }

    public void setRevertTask(final RevertTask task) {
        this.revertTask = task;
    }

    public long getRevertTime() {
        return this.revertTime;
    }

    public void setRevertTime(final long revertTime) {
        if (this.inRevertQueue) {
            REVERT_QUEUE.remove(this);
        }
        this.inRevertQueue = true;
        this.revertTime = revertTime + System.currentTimeMillis();
        REVERT_QUEUE.add(this);
    }

    public void revertBlock() {
        this.state.update(true);
        instances.remove(this.block);
        if (REVERT_QUEUE.contains(this)) {
            REVERT_QUEUE.remove(this);
        }
        if (this.revertTask != null) {
            this.revertTask.run();
        }
    }

    public void setType(final Material material) {
        this.setType(material, this.newdata);
    }

    @SuppressWarnings("deprecation")
    public void setType(final Material material, final byte data) {
        this.newdata = data;
        this.block.setType(material);
        this.block.setData(data);
    }

    public interface RevertTask {
        public void run();
    }

}
