package com.pvpdojo.abilities.abilities.active;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.util.bukkit.BlockUtil;

public class EarthQuakeAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "EarthQuakeAbility";
    }

    @Override // damage, length
    public boolean activateAbility(Ability ability, Event event) {
        AbilityEntity activator = ability.getAbilityEntity();
        final float damage = ability.getAttribute("ability_damage");
        final float length = ability.getAttribute("duration");
        new AbilityRunnable(ability) {
            double time_length = length * 20 / 3;
            double radius = 0.5;
            Location loc = activator.getEntity().getLocation();
            Set<AbilityEntity> hits = new HashSet<>();

            public void run() {
                time_length--;
                radius++;
                if (time_length <= 0)
                    this.cancel();
                for (int i = 0; i < 50; i++) {
                    double x = radius * 1.5 * Math.cos(i);
                    double y = 1;
                    double z = radius * 1.5 * Math.sin(i);
                    loc.add(x, y, z);
                    Block block = BlockUtil.getFirstBlockDown(loc, null).getBlock();

                    block.getWorld().playEffect(block.getLocation(), Effect.STEP_SOUND, block.getTypeId());
                    for (AbilityEntity entities : AbilityServer.get().getNearbyEntities(activator, loc, 3, 3)) {

                        if (!hits.contains(entities)) {
                            hits.add(entities);
                            activator.abilityDamage(entities, damage, ability, DamageCause.FALL);
                            entities.getEntity()
                                    .setVelocity(entities.getEntity().getVelocity()
                                                         .add(entities.getEntity().getLocation().toVector()
                                                                      .subtract(activator.getEntity().getLocation().toVector())
                                                                      .normalize().multiply(1.8).add(new Vector(0, 0.7, 0))));
                            PvPDojo.schedule(() -> hits.remove(entities)).createTask(10);
                        }

                    }
                    loc.subtract(x, y, z);
                }
            }
        }.createTimer(3, -1);
        return true;
    }

}
