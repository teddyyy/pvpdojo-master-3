package com.pvpdojo.abilities.abilities.passive;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerSettings.CustomRecipeModule;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.listeners.events.DojoSelectKitEvent;
import com.pvpdojo.dataloader.AbilityLoader;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;

public class SoupMasterAbility extends AbilityExecutor {

    public SoupMasterAbility() {
        ShapelessRecipe soupMasterRecipe = new ShapelessRecipe(new ItemStack(Material.MUSHROOM_SOUP)).addIngredient(Material.BOWL).addIngredient(Material.INK_SACK, 3);
        CustomRecipeModule recipeModule = new CustomRecipeModule(soupMasterRecipe);
        recipeModule.register();
    }

    @Override
    public String getExecutorName() {
        return "SoupMasterAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        return false;
    }

    @EventHandler
    public void onKitSelect(DojoSelectKitEvent e) {
        AbilityEntity activator = e.getEntity();
        boolean soupMaster = activator.hasAbility(AbilityLoader.getAbilityData(177).getName());
        // canExecuteAbility(entity) will not trigger in deathmatch session while
        // counting down.
        if (soupMaster) {
            PvPDojo.schedule(() -> createSoupMaster(activator)).nextTick();
        }
    }

    private void createSoupMaster(AbilityEntity activator) {
        if (activator.getEntity() instanceof Player) {
            Player player = (Player) activator.getEntity();
            for (ItemStack item : player.getInventory()) {
                if (item != null) {
                    if (item.getType() == Material.RED_MUSHROOM) {
                        item.setType(Material.INK_SACK);
                        item.setDurability((short) 3);
                    }

                    if (item.getType() == Material.BROWN_MUSHROOM) {
                        item.setAmount(1);
                        item.setType(Material.MUSHROOM_SOUP);
                    }
                }

            }
        }
    }

}
