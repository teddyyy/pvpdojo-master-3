/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.effects.OctopusFormEffect;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.abilities.lib.ChargeSystem;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.abilities.AbilityMetaData;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.BurnEffect;
import com.pvpdojo.bukkit.util.TempBlock;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerPreDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

import java.util.Objects;

public class FireBenderAbility extends AbilityExecutor {
    @Override
    public String getExecutorName() {
        return "FireBenderAbility";
    }

    private static final String OCTOPUS_FORM = "Fire_Form_Key";

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        if (event instanceof PlayerToggleSneakEvent) {
            Player player = ((PlayerToggleSneakEvent) event).getPlayer();
            AbilityMetaData data = ability.getMetaData();

            float fast = ability.getAttribute("fast");

            if (data.hasData(OCTOPUS_FORM)) {
                data.removeData(OCTOPUS_FORM);
            }

            OctopusFormEffect form = new OctopusFormEffect(ability);

            form.setBlockMaterial(Material.LAVA);
            form.setApplyEffects(false);
            form.setSync(false);

            ChargeSystem charge = new ChargeSystem(ability, 100, (int) fast) {
                @Override
                public void trigger() {
                    form.form();
                    data.addData(OCTOPUS_FORM, form);

                    new AbilityRunnable(ability) {

                        int loop;

                        @Override
                        public void run() {
                            if (++loop > 20 * 10) {
                                cancel();
                            }
                        }

                        @Override
                        public void cancel() {
                            super.cancel();
                            form.destroy();
                            data.removeData(OCTOPUS_FORM);
                        }
                    }.createTimer(1, -1);

                }

                @Override
                public void run() {

                }
            };
            charge.call();
        }

        return false;
    }


    @EventHandler
    public void onPlayerPreDeath(PlayerPreDeathEvent e) {
        Player player = e.getPlayer();

        if (canAbilityExecute(player)) {
            if (e.isCancelled()) {
                return;
            }

            Ability ability = getAbility(player);

            if (ability.getMetaData().hasData(OCTOPUS_FORM)) {
                ((OctopusFormEffect) ability.getMetaData().getData(OCTOPUS_FORM)).destroy();
            }
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();

        if (canAbilityExecute(player)) {
            Ability ability = getAbility(player);

            if (ability.getMetaData().hasData(OCTOPUS_FORM)) {
                ((OctopusFormEffect) ability.getMetaData().getData(OCTOPUS_FORM)).destroy();
            }
        }
    }

    private void createFlame(Ability ability) {

        Location loc = ability.getAbilityEntity().getEntity().getLocation();

        new AbilityRunnable(ability) {

            int loop = 2;

            @Override
            public void run() {

                if (++loop >= 40) {
                    cancel();
                    return;
                }

                for (int i = -30; i < 30; i += 2) {

                    double x = Math.cos(i) * loop;
                    double z = Math.sin(i) * loop;
                    double y = 0;

                    loc.add(x, y, z);
                    if (loc.getBlock().getType().equals(Material.AIR) || loc.getBlock().getType().isSolid()) {
                        TempBlock.createTempBlock(loc, Material.FIRE, (byte) 0, 1);

                        loc.getWorld()
                                .getLivingEntities()
                                .stream()
                                .map(AbilityEntity::get)
                                .filter(Objects::nonNull)
                                .filter(AbilityEntity::isTargetable)
                                .filter(target -> target.getEntity().getLocation().distance(loc) <= 3D)
                                .filter(target -> target != ability.getAbilityEntity())
                                .forEach(target -> {
                                    ability.getAbilityEntity().abilityDamage(target, 175, ability, EntityDamageEvent.DamageCause.FIRE);
                                    target.applyEffect(new BurnEffect(20 * 5));
                                });

                    }
                    loc.subtract(x, y, z);
                }

            }
        }.createTimer(1, -1);

    }

    @EventHandler
    public void onFireBender(PlayerToggleSneakEvent e) {
        if (canAbilityExecute(e.getPlayer())) {
            getAbility(e.getPlayer()).activateAbility(e);
        }
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof LivingEntity && (e.getCause() == EntityDamageEvent.DamageCause.FIRE || e.getCause() == EntityDamageEvent.DamageCause.FIRE_TICK) && canAbilityExecute((LivingEntity) e.getEntity())) {
            e.getEntity().setFireTicks(0);
            e.setCancelled(true);
        }
    }

}
