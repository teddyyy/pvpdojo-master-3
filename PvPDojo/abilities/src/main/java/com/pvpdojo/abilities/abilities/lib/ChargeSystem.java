package com.pvpdojo.abilities.abilities.lib;

import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import org.bukkit.entity.Player;

public abstract class ChargeSystem {

    private Ability ability;
    private int max;
    private int increasement;

    public boolean cancel;

    protected ChargeSystem(Ability ability, int max, int increasement) {
        this.ability = ability;
        this.max = max;
        this.increasement = increasement;

        if (increasement <= 0) {
            this.increasement = 1;
        }
    }


    public abstract void trigger();

    public abstract void run();

    public void call() {

        new AbilityRunnable(ability) {

            int level;

            {
                if (ability.getAbilityEntity() instanceof AbilityPlayer) {
                    Player player = ((AbilityPlayer) ability.getAbilityEntity()).getPlayer();
                    level = player.getLevel();
                }
            }

            @Override
            public void run() {
                if (cancel) {
                    cancel();
                    return;
                }

                if (ability.getCooldown().isCoolingDown()) {
                    cancel();
                    return;
                }

                if (ability.getAbilityEntity() instanceof AbilityPlayer) {
                    Player player = ((AbilityPlayer) ability.getAbilityEntity()).getPlayer();

                    if (player.getLevel() >= max && !player.isSneaking()) {
                        cancel();
                        trigger();
                        ability.activateAbility(false, null);//activates cooldown.
                        return;
                    }

                    if (!player.isSneaking() && player.getLevel() <= max) {
                        cancel();
                        return;
                    }


                    if (player.getLevel() >= max) {
                        player.setLevel(max);
                    }

                    if (player.getLevel() < max) {
                        player.setLevel(player.getLevel() + increasement);
                    }

                }
                ChargeSystem.this.run();
            }

            @Override
            public void cancel() {
                super.cancel();

                if (ability.getAbilityEntity() instanceof AbilityPlayer) {
                    Player player = ((AbilityPlayer) ability.getAbilityEntity()).getPlayer();
                    player.setLevel(level);
                }

            }

        }.createTimer(1, -1);
    }

}
