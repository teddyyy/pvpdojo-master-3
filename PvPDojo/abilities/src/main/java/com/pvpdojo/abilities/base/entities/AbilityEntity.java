/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.base.entities;

import static com.pvpdojo.util.StringUtils.getStringFromArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.jetbrains.annotations.Contract;

import com.pvpdojo.abilities.Abilities;
import com.pvpdojo.abilities.abilities.guns.Gun;
import com.pvpdojo.abilities.abilities.guns.GunPlayer;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.gamemode.AbilityEffect;
import com.pvpdojo.abilities.gamemode.AbilityEffect.AbilityEffectType;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.listeners.events.AbilityHitEvent;
import com.pvpdojo.abilities.listeners.events.AbilityTargetEvent;
import com.pvpdojo.abilityinfo.KitAttributeArray;
import com.pvpdojo.abilityinfo.KitAttributeArray.AttributeArrayType;
import com.pvpdojo.abilityinfo.KitAttributeArray.KitAttribute;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Log;

import lombok.Getter;
import lombok.Setter;

public abstract class AbilityEntity {

    public static final String ABILITY_ENTITY_NO_INTERACTION = "ability_entity_no_interaction";
    public static final String OWNED_ENTITY = "owned_entity";
    public static final float HEALTH_CONVERTER = 25;

    private LivingEntity entity;
    private ArrayList<AbilityEffect> effects = new ArrayList<>();
    Map<AttributeArrayType, KitAttributeArray> attributes = new HashMap<>();
    List<Ability> abilities = new ArrayList<>();
    GunPlayer gp;
    float mana;
    private float invulnerability = 0;
    protected boolean isDead = false;
    protected int tick = 0;
    @Getter
    @Setter
    private AbilityEntity owner;

    public AbilityEntity(LivingEntity entity) {
        this.entity = entity;
        this.attributes.put(AttributeArrayType.BASE_ATTRIBUTES, new KitAttributeArray());
        this.attributes.put(AttributeArrayType.BASE_ATTRIBUTES_INCREMENT, new KitAttributeArray());
        this.attributes.put(AttributeArrayType.BONUS_ATTRIBUTES, new KitAttributeArray());
        this.attributes.put(AttributeArrayType.APPLIED_BASE_ATTRIBUTES, new KitAttributeArray());
        this.attributes.put(AttributeArrayType.APPLIED_BONUS_ATTRIBUTES, new KitAttributeArray());
        this.attributes.put(AttributeArrayType.APPLIED_TOTAL_ATTRIBUTES, new KitAttributeArray());
    }

    /**
     * THIS MAY RETURN NULL
     *
     * @param entity
     * @return
     */
    @Contract("null -> null")
    public static AbilityEntity get(Entity entity) {
        if (entity instanceof LivingEntity) {
            return AbilityServer.get().getEntity((LivingEntity) entity);
        }
        return null;
    }

    public void update() {
        try {
            Abilities.timing("Entity").startTiming();
            Abilities.timing("Values").startTiming();
            tick++;
            invulnerability--;
            if (invulnerability < 0) {
                invulnerability = 0;
            }
            Abilities.timing("Values").stopTiming();

            if (!this.isTargetable()) {
                return;
            }

            Abilities.timing("Effects").startTiming();
            for (int i = this.effects.size() - 1; i >= 0; i--) {
                if (effects.isEmpty()) {
                    break;
                }
                AbilityEffect effect = effects.get(i);
                effect.update(this);
            }
            Abilities.timing("Effects").stopTiming();

            Abilities.timing("Guns").startTiming();
            if (gp != null) {
                gp.tick();
            }
            Abilities.timing("Guns").stopTiming();

            Abilities.timing("Tick Abilities").startTiming();
            for (Ability ability : getAbilities()) {
                ability.tick();
            }
            Abilities.timing("Tick Abilities").stopTiming();

        } finally {
            Abilities.timing("Entity").stopTiming();
        }
    }

    public void selectGunAbility(Gun gun, Ability ability) {
        GunPlayer gp = getGunController();
        gp.registerGun(gun);
    }

    public GunPlayer getGunController() {
        if (gp == null) {
            this.gp = new GunPlayer(this);
        }
        return this.gp;
    }

    public void cleanup() {
        if (gp != null) {
            gp.destroy();
        }
        gp = null;
        this.abilities.clear();
        this.attributes.clear();
        this.effects.clear();
    }

    public boolean isTargetable() {
        if (this.getEntity() != null) {
            return !isDead();
        }
        Log.warn("entity is null");
        return false;
    }

    public void setBaseAttributeArray(KitAttributeArray base_attributes, KitAttributeArray attribute_increment) {
        this.attributes.put(AttributeArrayType.BASE_ATTRIBUTES, base_attributes);
        this.attributes.put(AttributeArrayType.BASE_ATTRIBUTES_INCREMENT, attribute_increment);
        this.setHealth(getMaxHealth());
    }

    public boolean isInvincible() {
        return this.invulnerability > 0;
    }

    public void setInvulnerable(int ticks) {
        getEntity().setNoDamageTicks(ticks + getEntity().getMaximumNoDamageTicks() / 2);
        this.invulnerability = ticks;
    }

    public void setHealth(double health) {
        getEntity().setHealth(health / HEALTH_CONVERTER);
    }

    public double getHealth() {
        return getEntity().getHealth() * HEALTH_CONVERTER;
    }

    public LivingEntity getEntity() {
        return entity;
    }

    public int getTicks() {
        return tick;
    }

    public boolean isDead() {
        return isDead;
    }

    public void regenMana(float factor) {
        float regenAmount = mana + getManaRegen() * factor;
        if (regenAmount > getMaxMana()) {
            setMana(getMaxMana());
        } else {
            setMana(regenAmount);
        }
    }

    public float getMana() {
        return mana;
    }

    public void setMana(float mana) {
        this.mana = mana;
    }

    public float getMaxMana() {
        return getAttribute("max_mana");
    }

    public float getManaRegen() {
        return getAttribute("mana_regen");
    }

    public float getMaxHealth() {
        return getAttribute("max_health");
    }

    public float getMoveSpeed() {
        float moveSpeed = getAttribute("move_speed");
        return moveSpeed == 0F ? 0.2F : moveSpeed;
    }

    public float getAbilityResistance() {
        return getAttribute("ability_resistance");
    }

    public float getMonsterLifeSteal() {
        return getAttribute("monster_lifesteal");
    }

    public float getPlayerLifeSteal() {
        return getAttribute("player_lifesteal");
    }

    public boolean canTarget(AbilityEntity entity) {
        if (entity == null || entity == this || !isTargetable() || !entity.isTargetable() || entity.getOwner() == this) {
            return false;
        }

        AbilityTargetEvent event = new AbilityTargetEvent(entity, this);
        Bukkit.getPluginManager().callEvent(event);

        return !event.isCancelled();
    }

    public void combatTag(AbilityEntity source, float healthBefore, float damage, DamageCause cause) {
        NMSUtils.get(getEntity()).aW().a(NMSUtils.getSourceFromCause(cause, source.getEntity()), healthBefore, damage);
    }

    public List<Ability> getAbilities() {
        return abilities;
    }

    public void removeAbility(String ability) {
        getAbilities().removeIf(active -> active.getAbility().equals(ability));
    }

    public Ability getAbilityFromExecutor(String executor) {
        for (Ability ability : getAbilities()) {
            if (ability == null)
                continue;
            if (ability.getAbilityExecutor().equals(executor)) {
                return ability;
            }
        }
        return null;
    }

    public Ability getAbility(String name) {
        for (Ability ability : getAbilities()) {
            if (ability == null)
                continue;
            if (ability.getAbility().equals(name)) {
                return ability;
            }
        }
        return null;
    }

    public boolean hasAbility(String name) {
        return getAbility(name) != null;
    }

    public void removeEffect(AbilityEffect effect) {
        this.effects.remove(effect);
    }

    public void applyEffect(AbilityEffect effect) {
        effect.applyEffect(this);
        this.effects.add(effect);
    }

    public boolean hasEffect(AbilityEffectType type) {
        for (AbilityEffect effect : this.effects) {
            if (effect.getEffectType().equals(type))
                return true;
        }
        return false;
    }

    public ArrayList<AbilityEffect> getEffect(AbilityEffectType type) {
        ArrayList<AbilityEffect> list = new ArrayList<>();
        for (AbilityEffect effect : this.effects) {
            if (effect.getEffectType().equals(type))
                list.add(effect);
        }
        return list;
    }

    public ArrayList<AbilityEffect> getEffects() {
        return this.effects;
    }

    public void sendMessage(MessageKeys key, String... replacements) {
        if (getEntity() instanceof Player) {
            Player player = (Player) getEntity();
            User.getUser(player).sendOverlay(key, replacements);
        }
    }

    public void heal(double d) {
        getEntity().setHealth(Math.min(getEntity().getMaxHealth(), getEntity().getHealth() + d / HEALTH_CONVERTER));
    }

    public void invalidate() {
        for (Ability ability : getAbilities()) {
            ability.resetAbility();
        }
        this.getAbilities().clear();
    }

    public void die() {
        invalidate();
        if (gp != null) {
            gp.destroy();
        }
        gp = null;
        this.effects.clear();
        this.isDead = true;
    }

    public boolean absoluteDamage(float damage, AbilityEntity source, DamageCause damageCause) {

        @SuppressWarnings("deprecation")
        EntityDamageByEntityEvent damageEvent = new EntityDamageByEntityEvent(source.getEntity(), getEntity(), damageCause, damage / HEALTH_CONVERTER);

        getEntity().setLastDamageCause(damageEvent);
        if (source != this) {
            this.combatTag(source, (float) getEntity().getHealth(), damage / HEALTH_CONVERTER, damageCause);
        }
        getEntity().damage(damage / HEALTH_CONVERTER);

        return true;
    }

    public boolean recieveAbilityDamage(float damage, AbilityEntity source, Ability ability, DamageCause damageCause) {
        if (source == this) {
            return false;
        }

        if (this.invulnerability > 0 || this.isDead() || !source.canTarget(this)) {
            return false;
        }

        AbilityHitEvent event = new AbilityHitEvent(source, this, ability);
        Bukkit.getPluginManager().callEvent(event);

        if (event.isCancelled()) {
            return false;
        }

        float finalDamage = damage * 100 / (100 + getAbilityResistance());
        if (finalDamage < 0) {
            return false;
        }
        return absoluteDamage(finalDamage, source, damageCause);
    }

    public boolean abilityDamage(AbilityEntity victim, float damage, Ability ability, DamageCause damageCause) {
        if (victim != null) {
            return victim.recieveAbilityDamage(damage, this, ability, damageCause);
        }
        return false;
    }


    public void addAttribute(KitAttribute attribute) {
        KitAttributeArray array = this.attributes.get(attribute.getAttributeArrayType());
        if (array != null) {
            array.addAttribute(attribute);
        }
    }

    public void removeAttribute(KitAttribute attribute) {
        KitAttributeArray array = this.attributes.get(attribute.getAttributeArrayType());
        if (array != null) {
            array.removeAttribute(attribute);
        }
    }

    public void clearBonusAttributes() {
        this.attributes.get(AttributeArrayType.BONUS_ATTRIBUTES).getAttributeArray().clear();
        this.attributes.get(AttributeArrayType.APPLIED_BONUS_ATTRIBUTES).getAttributeArray().clear();
    }

    public float getAttribute(String attribute_name) {
        float base_attribute = getBaseAttribute(attribute_name);
        float bonus_attribute = getBonusAttribute(attribute_name);
        float total_value = base_attribute + bonus_attribute;
        float applied_total_attribute = this.getAttributeValue(AttributeArrayType.APPLIED_TOTAL_ATTRIBUTES, attribute_name);
        return total_value * (applied_total_attribute + 100) / 100;
    }

    public float getBonusAttribute(String attribute_name) {
        float applied_bonus_attribute = this.getAttributeValue(AttributeArrayType.APPLIED_BONUS_ATTRIBUTES, attribute_name);
        return this.getAttributeValue(AttributeArrayType.BONUS_ATTRIBUTES, attribute_name) * (applied_bonus_attribute + 100) / 100;
    }

    public float getBaseAttribute(String attribute_name) {
        KitAttributeArray base_attributes = this.attributes.get(AttributeArrayType.BASE_ATTRIBUTES);
        KitAttributeArray base_attributes_increment = this.attributes.get(AttributeArrayType.BASE_ATTRIBUTES_INCREMENT);
        if (!base_attributes.hasAttribute(attribute_name) || !base_attributes_increment.hasAttribute(attribute_name)) {
            return 0.0f;
        }
        float base_value = base_attributes.getKitAttribute(attribute_name).getAttributeValue();
        float applied_base_attribute = this.getAttributeValue(AttributeArrayType.APPLIED_BASE_ATTRIBUTES, attribute_name);
        return base_value * (applied_base_attribute + 100) / 100;
    }

    public float getAttributeValue(AttributeArrayType type, String attribute_name) {
        KitAttributeArray bonus_attributes = this.attributes.get(type);
        if (!bonus_attributes.hasAttribute(attribute_name)) {
            return 0.0f;
        }
        float attribute_value = 0.0f;
        for (KitAttribute attribute : bonus_attributes.getAttributeArray()) {
            if (attribute.getAttributeName().equals(attribute_name)) {
                if (attribute.isValid())
                    attribute_value += attribute.getAttributeValue();
            }
        }
        return attribute_value;
    }

    public String listAbilities() {
        if (getAbilities().isEmpty()) {
            return "none";
        }
        return getStringFromArray(getAbilities(), abl -> abl.getLoader().getDisplayName(), ", ");
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof AbilityEntity && getEntity().equals(((AbilityEntity) obj).getEntity());
    }

    @Override
    public int hashCode() {
        return getEntity().hashCode();
    }
}
