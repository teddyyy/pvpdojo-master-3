/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import static com.pvpdojo.util.StreamUtils.negate;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.effects.ParticleEffectRotation;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.gamemode.SnareEffect;
import com.pvpdojo.util.bukkit.ParticleEffects;

//THIS IS BROKEN (or the rotate functions are broken)
public class HelixAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "HelixAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        final AbilityEntity activator = ability.getAbilityEntity();
        final Player player = (Player) activator.getEntity();

        final float damage = ability.getAttribute("ability_damage");
        final int snareSeconds = (int) ability.getAttribute("snare_duration");
        final float beamRange = ability.getAttribute("beam_range");

        final Location loc0 = player.getLocation();
        final Location loc1 = player.getLocation();
        final Location loc2 = player.getLocation();

        // this line is for testing to see if there is a problem with the math or the effect

        new AbilityRunnable(ability) {

            double t = 0;
            Set<AbilityEntity> hit = new HashSet<>();

            public void run() {
                Location loc3;
                t = t + Math.PI / 12;
                double r = 0.75;
                double x1 = r * Math.cos(t);
                double y1 = r * Math.sin(t);
                double z1 = 2 * t;

                Vector v1 = new Vector(x1, y1, z1);
                v1 = ParticleEffectRotation.rotateFunction(v1, loc0);
                loc1.add(v1.getX(), v1.getY(), v1.getZ());

                double x2 = r * Math.cos(t + Math.PI);
                double y2 = r * Math.sin(t + Math.PI);
                double z2 = 2 * t;
                Vector v2 = new Vector(x2, y2, z2);
                v2 = ParticleEffectRotation.rotateFunction(v2, loc0);
                loc2.add(v2.getX(), v2.getY(), v2.getZ());

                Vector vect = new Vector(loc2.getX() - loc1.getX(), loc2.getY() - loc1.getY(), loc2.getZ() - loc1.getZ()).normalize();
                double x3;
                double y3;
                double z3;
                for (double i = 0; i < r * 2; i += 0.1) {
                    x3 = vect.getX() * i;
                    y3 = vect.getY() * i;
                    z3 = vect.getZ() * i;
                    loc3 = loc1;
                    loc3.add(x3, y3, z3);
                    loc3.add(0, 1.5, 0);
                    ParticleEffects.BLUE_SPARKLE.sendToLocation(loc3, 0, 0, 0, 0, 1);
                    loc3.subtract(x3, y3, z3);
                    loc3.subtract(0, 1.5, 0);
                }

                // add and subtract 1.5 only when displaying it to shift everything up by 1.5 so it comes out of the user's head
                loc1.add(0, 1.5, 0);
                loc2.add(0, 1.5, 0);
                ParticleEffects.FIRE.sendToLocation(loc1, 0, 0, 0, 0, 1);
                ParticleEffects.FIRE.sendToLocation(loc2, 0, 0, 0, 0, 1);

                AbilityServer.get().getNearbyEntities(activator, 3).stream()
                             .filter(negate(hit::contains))
                             .filter(target -> activator.abilityDamage(target, damage, ability, DamageCause.MAGIC))
                             .forEach(target -> {
                                 target.applyEffect(new SnareEffect(snareSeconds * 20));
                                 hit.add(target);
                             });

                loc1.subtract(v1.getX(), v1.getY() + 1.5, v1.getZ());
                loc2.subtract(v2.getX(), v2.getY() + 1.5, v2.getZ());

                if (t > beamRange) {
                    this.cancel();
                }

            }
        }.createTimer(1, -1);

        return true;

    }

}
