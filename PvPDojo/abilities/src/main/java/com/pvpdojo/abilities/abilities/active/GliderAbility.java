package com.pvpdojo.abilities.abilities.active;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.abilities.AbilityMetaData;
import com.pvpdojo.userdata.User;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

public class GliderAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "GliderAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        return false;
    }

    private static final String KEY = "FALL";

    @EventHandler
    public void onMove(PlayerMoveEvent e) {

        Player player = e.getPlayer();
        if (!canAbilityExecute(player)) {
            return;
        }

        Ability ability = getAbility(player);
        AbilityMetaData data = ability.getMetaData();

        if (player.getItemInHand().isSimilar(ability.getClickItem())) {

            if (player.getLocation().subtract(0, 1, 0).getBlock().getType() == Material.AIR
                    && player.getLocation().subtract(0, 2, 0).getBlock().getType() == Material.AIR) {

                double glide = ability.getAttribute("speed") > 0 ? 0.86 : 0.76;
                double y = -0.03;
                Vector vec = player.getLocation().clone().getDirection()
                        .multiply(User.getUser(player).isInCombat() ? glide / 2.5 : glide).setY(y);
                player.setVelocity(vec);

            } else {
                if (!player.isOnGround()) {
                    if (!data.hasData(KEY)) {
                        data.addData(KEY, null);
                    }
                } else if (data.hasData(KEY)) {
                    data.removeData(KEY);
                }
            }
        }
    }


    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (e.getCause() == EntityDamageEvent.DamageCause.FALL && canAbilityExecute((LivingEntity) e.getEntity())) {
            if (getAbility((LivingEntity) e.getEntity()).getMetaData().hasData(KEY)) {
                e.setCancelled(true);
                getAbility((LivingEntity) e.getEntity()).getMetaData().removeData(KEY);
            }
        }
    }

}
