package com.pvpdojo.abilities.abilities.passive;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.util.bukkit.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.entity.PlayerPreDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;

public class ExorcistAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "ExorcistAbility";
    }

    private static final String TAMEABLE = "tameable";
    private static final HashMap<UUID, Zombie> ZOMBIES = new HashMap<>();

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        if (ability.getAbilityEntity().getEntity() instanceof Player) {
            Player player = (Player) ability.getAbilityEntity().getEntity();
            String name = player.getNick();

            final Zombie zombie = (Zombie) ability.getAbilityEntity().getEntity().getWorld()
                    .spawnEntity(ability.getAbilityEntity().getEntity().getLocation(), EntityType.ZOMBIE);
            ItemStack skull = new ItemBuilder.SkullBuilder(name).type(Material.SKULL_ITEM).durability((short) 3)
                    .build();
            zombie.setBaby(false);
            zombie.setCustomName(name);
            zombie.getEquipment().setHelmet(skull);
            zombie.getEquipment().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
            zombie.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 21474647, 1));
            zombie.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 21483647, 0));
            zombie.getEquipment().setItemInHandDropChance(0);
            zombie.getEquipment().setChestplateDropChance(0);
            zombie.getEquipment().setHelmetDropChance(0);
            zombie.setMaxHealth(35);
            zombie.setHealth(35);
            zombie.setMetadata(TAMEABLE, new FixedMetadataValue(PvPDojo.get(), player.getUniqueId().toString()));
            PvPDojo.schedule(() -> {
                ZOMBIES.put(player.getUniqueId(), zombie);
            }).nextTick();

            new BukkitRunnable() {

                int timer = 20 * 30;

                @Override
                public void run() {
                    if (--timer <= 0) {
                        zombie.remove();
                        this.cancel();
                        return;
                    }

                    if (zombie.isDead()) {
                        this.cancel();
                        return;
                    }

                    for (AbilityEntity nearby : AbilityServer.get().getNearbyEntities(ability.getAbilityEntity(),
                            zombie.getLocation(), 3.0, 1.5)) {

                        if (nearby.getEntity().equals(zombie)) {
                            continue;
                        }

                        ability.getAbilityEntity().abilityDamage(nearby, 100, ability, DamageCause.MAGIC);
                        NMSUtils.applyKnockback(nearby.getEntity(), zombie);
                    }

                }
            }.runTaskTimer(PvPDojo.get(), 0, 1);
        }
        return false;
    }

    @EventHandler
    public void onTrigger(PlayerPreDeathEvent e) {
        Player player = e.getPlayer();
        if (canAbilityExecute(player) && !e.isCancelled()) {
            Ability ability = getAbility(player);

            reset(ability);
            activateAbility(ability, e);
        }
    }

    public void reset(Ability ability) {

        if (ZOMBIES == null || ZOMBIES.isEmpty()) {
            return;
        }

        if (!ZOMBIES.containsKey(ability.getAbilityEntity().getEntity().getUniqueId())) {
            return;
        }

        Iterator<Zombie> iterator = ZOMBIES.values().iterator();
        while (iterator.hasNext()) {
            Zombie zombie = iterator.next();
            if (ZOMBIES.get(ability.getAbilityEntity().getEntity().getUniqueId()).equals(zombie)) {
                zombie.remove();
                break;
            }
        }
    }

    @EventHandler
    public void onTarget(EntityTargetLivingEntityEvent e) {
        if (e.getEntity() instanceof Zombie && e.getTarget() instanceof Player) {
            Player target = (Player) e.getTarget();
            if (e.getEntity().hasMetadata(TAMEABLE)
                    && e.getEntity().getMetadata(TAMEABLE).get(0).asString().equals(target.getUniqueId().toString())
                    || !AbilityEntity.get(target).isTargetable()) {
                e.setCancelled(true);
            }
        }
    }
}
