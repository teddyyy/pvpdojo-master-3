/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.listeners;

import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.spigotmc.event.entity.EntityMountEvent;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.npc.DojoBot;

public class AbilityEntityListener implements DojoListener {

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (e.getEntity().hasMetadata(AbilityEntity.ABILITY_ENTITY_NO_INTERACTION)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onDeath(EntityDeathEvent e) {
        e.getEntity().removeMetadata(AbilityEntity.ABILITY_ENTITY_NO_INTERACTION, PvPDojo.get());
        if (e.getEntity().hasMetadata(AbilityEntity.OWNED_ENTITY)) {
            e.getEntity().removeMetadata(AbilityEntity.OWNED_ENTITY, PvPDojo.get());
            e.getDrops().clear();
        }
    }

    @EventHandler
    public void onMount(EntityMountEvent e) {
        if (e.getMount().hasMetadata(AbilityEntity.ABILITY_ENTITY_NO_INTERACTION)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onInteractEntity(PlayerInteractEntityEvent e) {
        if (e.getRightClicked().hasMetadata(AbilityEntity.ABILITY_ENTITY_NO_INTERACTION)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onTarget(EntityTargetEvent e) {
        if (e.getTarget() == null) {
            return;
        }
        if (e.getEntity().hasMetadata(AbilityEntity.OWNED_ENTITY) && e.getEntity().getMetadata(AbilityEntity.OWNED_ENTITY).get(0).value().equals(e.getTarget().getUniqueId())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onChunkUnload(ChunkUnloadEvent e) {
        for (Entity entity : e.getChunk().getEntities()) {
            if (DojoBot.isBot(entity)) {
                e.setCancelled(true);
                break;
            }
        }
    }

}
