/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.listeners;

import com.pvpdojo.DeveloperSettings;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilities.gamemode.AbilityEffect;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.listeners.events.AbilityTargetEvent;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.userdata.PlayStyle;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.PlayerUtil;
import org.bukkit.entity.FishHook;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

public class AbilityDamageListener implements DojoListener {

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (e.getCause() == DamageCause.VOID) {
            e.setDamage(Integer.MAX_VALUE);
        }
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {

        if (e.getDamager() instanceof Projectile && e.getDamager() instanceof FishHook) {
            if (((FishHook) e.getDamager()).getShooter() instanceof Player) {
                AbilityPlayer player = AbilityPlayer.get((Player) ((FishHook) e.getDamager()).getShooter());
                int manaUse = 20;
                if (player.getMana() < manaUse) {
                    player.sendMessage(MessageKeys.NOT_ENOUGH_MANA, "{mana}", "" + (manaUse - player.getMana()));
                    e.setCancelled(true);
                } else {
                    player.setMana(player.getMana() - manaUse);
                }
            }
        }

        if (e.getDamager() instanceof LivingEntity && e.getEntity() instanceof LivingEntity) {
            AbilityEntity damager = AbilityServer.get().getEntity((LivingEntity) e.getDamager());
            AbilityEntity victim = AbilityServer.get().getEntity((LivingEntity) e.getEntity());

            if (damager instanceof AbilityPlayer) {
                ItemStack inHand = ((AbilityPlayer) damager).getPlayer().getItemInHand();
                if (inHand.hasItemMeta() && inHand.getItemMeta().hasDisplayName()) {
                    e.setDamage(e.getDamage() / 2);
                }
            }

            if (!damager.canTarget(victim)) {
                e.setCancelled(true);
            } else if (PlayerUtil.isRealHit(victim.getEntity())) {
                e.setDamage(e.getDamage() + damager.getBonusAttribute("physical_damage") / DeveloperSettings.HEALTH_CONVERTER);

                if (!damager.hasEffect(AbilityEffect.AbilityEffectType.MANA_SILENCE)) {
                    damager.regenMana(0.75F);
                }

                if (!victim.hasEffect(AbilityEffect.AbilityEffectType.MANA_SILENCE)) {
                    victim.regenMana(0.25F);
                }
            } else if (damager instanceof AbilityPlayer && ((AbilityPlayer) damager).getPlayStyle() == PlayStyle.SURVIVALIST) {
                e.setDamage(e.getDamage() / 2);
            }
        }
    }

    @EventHandler
    public void onAbilityTarget(AbilityTargetEvent e) {
        if (!(e.getEntity() instanceof AbilityPlayer))
            return;
        AbilityPlayer champion = (AbilityPlayer) e.getEntity();
        User user = User.getUser(champion.getUUID());
        if (user.getSession() != null) {
            if (user.getSession().getState() != SessionState.STARTED) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPotionHeal(PotionSplashEvent e) {
        if (e.getEntity().getEffects().stream().anyMatch(effect -> effect.getType() == PotionEffectType.HEAL)) {
            e.getAffectedEntities().stream().map(AbilityServer.get()::getEntity).filter(en -> en instanceof AbilityPlayer).map(en -> (AbilityPlayer) en)
                    .filter(ap -> ap.getPlayStyle() == PlayStyle.SURVIVALIST).forEach(ap -> e.setIntensity(ap.getEntity(), 0D));
        }
    }

}
