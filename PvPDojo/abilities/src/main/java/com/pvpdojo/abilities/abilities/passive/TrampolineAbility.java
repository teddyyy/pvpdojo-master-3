/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.util.Vector;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.bukkit.events.EntityFallEvent;

public class TrampolineAbility extends AbilityExecutor {

    private static final String META_KEY = "trampoline_bounces";

    @Override
    public String getExecutorName() {
        return "TrampolineAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        return true;
    }

    @EventHandler
    public void onFall(EntityFallEvent e) {
        if (!(e.getEntity() instanceof LivingEntity)) {
            return;
        }
        if (canAbilityExecute((LivingEntity) e.getEntity())) {
            Ability ability = getAbility((LivingEntity) e.getEntity());
            int maxBounces = (int) ability.getAttribute("bounce");
            float bounceForce = ability.getAttribute("bounce_force");
            int bounces = getBounces(ability);
            if (bounces <= maxBounces) {
                setBounces(ability, bounces + 1);
                e.getEntity().setVelocity(new Vector(0d, e.getDistance() / (15 / (bounceForce / 10d)), 0d));
            } else {
                setBounces(ability, 1);
            }

        }
    }

    /*
     * private double getDecimal(double value) { return value - Math.floor(value); }
     */
    private int getBounces(Ability ability) {
        return ability.getMetaData().hasData(META_KEY) ? ability.getMetaData().getData(META_KEY) : 1;
    }

    private void setBounces(Ability ability, int bounces) {
        ability.getMetaData().addData(META_KEY, bounces);
    }
}
