/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.inventory.CraftItemEvent;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.util.bukkit.ParticleEffects;

public class CraftAuraAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "CraftAuraAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        AbilityEntity activator = ability.getAbilityEntity();

        ParticleEffects.HUGE_EXPLODE.sendToLocation(activator.getEntity().getLocation(), 0, 0, 0, 0, 1);
        activator.getEntity().getWorld().playSound(activator.getEntity().getLocation(), Sound.EXPLODE, 3, 3);

        return true;
    }

    @EventHandler
    public void onCraft(CraftItemEvent e) {
        if (!canAbilityExecute(e.getWhoClicked()))
            return;

        Ability ability = getAbility(e.getWhoClicked());

        AbilityEntity activator = ability.getAbilityEntity();

        final float radius = ability.getAttribute("radius");
        final float damage = ability.getAttribute("ability_damage");

        if (e.getInventory().getResult() != null && e.getInventory().getResult().getType() == Material.MUSHROOM_SOUP) {

            if (ability.activateAbility(true, e)) {
                for (AbilityEntity nearby : AbilityServer.get().getNearbyEntities(activator, radius)) {
                    activator.abilityDamage(nearby, damage, ability, DamageCause.ENTITY_EXPLOSION);
                }
            }
        }
    }

}
