/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;

public class AstronautAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "AstronautAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        AbilityEntity activator = ability.getAbilityEntity();
        Player player = (Player) activator.getEntity();
        int duration = (int) ability.getAttribute("duration");
        int jumpPower = (int) ability.getAttribute("jump_power");
        player.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.JUMP, duration * 20, jumpPower));
        return true;
    }

}
