/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.effects;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.PvPDojo;

public final class SmokeEffect implements Runnable {

    private final int smokeRadius;
    private final int smokeDuration;
    private final int smokeDensityLow;
    private final int smokeDensityHigh;
    private int smokeTick = 0;
    private int taskId = -1;
    private final Location smokeLocation;

    public SmokeEffect(int smokeRadius, int smokeDuration, int smokeDensityLow, int smokeDensityHigh, Location smokeLocation) {
        this.smokeRadius = smokeRadius;
        this.smokeDuration = smokeDuration;
        this.smokeDensityLow = smokeDensityLow;
        this.smokeDensityHigh = smokeDensityHigh;
        this.smokeLocation = smokeLocation;
    }

    public void start() {
        if (taskId != -1) {
            throw new IllegalStateException("Already started!");
        }


        this.taskId = PvPDojo.schedule(this).createTimer(1, -1);
    }

    public void stop() {
        if (taskId == -1) {
            throw new IllegalStateException("Not started yet!");
        }

        Bukkit.getScheduler().cancelTask(taskId);
    }

    private Location randomLocation() {
        Random random = PvPDojo.RANDOM;

        double randomDegree = random.nextDouble() * 2 * Math.PI;
        double randomUp = random.nextDouble() * 2 * Math.PI;
        double randomRadius = random.nextInt(smokeRadius);
        double distanceUp = Math.cos(randomUp) * randomRadius;

        randomRadius = Math.sin(randomUp) * randomRadius;

        double distanceLeft = Math.cos(randomDegree) * randomRadius;
        double distanceFront = Math.sin(randomDegree) * randomRadius;

        return smokeLocation.clone().add(distanceLeft, distanceUp, distanceFront);
    }

    @Override
    public void run() {
        if (smokeTick > this.smokeDuration) {
            this.stop();
            return;
        }

        smokeTick++;

        int smokeAmount = this.smokeDensityLow;

        if (this.smokeDensityLow < this.smokeDensityHigh) {
            smokeAmount += PvPDojo.RANDOM.nextInt(this.smokeDensityHigh - this.smokeDensityLow);
        }

        for (int i = 0; i < smokeAmount; i++) {
            smokeLocation.getWorld().playEffect(this.randomLocation(), Effect.SMOKE, PvPDojo.RANDOM.nextInt(8));
        }

        for (Entity e : getNearbyEntities(smokeLocation, smokeRadius)) {
            if (e instanceof Player) {
                Player p = (Player) e;
                p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20 * 2, 1), true);
            }
        }
    }

    public static Set<Entity> getNearbyEntities(Location l, int radius) {
        int chunkRadius = radius < 16 ? 1 : (radius - radius % 16) / 16;
        HashSet<Entity> radiusEntities = new HashSet<>();
        for (int chX = 0 - chunkRadius; chX <= chunkRadius; chX++) {
            for (int chZ = 0 - chunkRadius; chZ <= chunkRadius; chZ++) {
                int x = (int) l.getX();
                int y = (int) l.getY();
                int z = (int) l.getZ();
                for (Entity e : new Location(l.getWorld(), x + chX * 16, y, z
                        + chZ * 16).getChunk().getEntities()) {
                    if ((e.getLocation().distance(l) <= radius) && (e.getLocation().getBlock() != l.getBlock()))
                        radiusEntities.add(e);
                }
            }
        }
        return radiusEntities;
    }

}
