/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import java.util.UUID;

import org.bukkit.entity.Entity;
import org.bukkit.event.Event;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.npc.DojoBot;
import com.pvpdojo.settings.BotSettings;
import com.pvpdojo.userdata.User;

import net.minecraft.util.com.mojang.authlib.GameProfile;

public class LifeLineAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "LifeLineAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {

        AbilityEntity entity = ability.getAbilityEntity();
        final float duration = ability.getAttribute("duration");

        DojoBot bot = new DojoBot(NMSUtils.get(entity.getEntity().getWorld()));
        bot.getIgnore().add(entity.getEntity().getUniqueId());

        if (entity instanceof AbilityPlayer) {
            AbilityPlayer abilityPlayer = (AbilityPlayer) entity;
            User user = User.getUser(abilityPlayer.getPlayer());

            if (user.getTeam() != null) {
                user.getTeam().getPlayers().stream().map(Entity::getUniqueId).forEach(bot.getIgnore()::add);
            }
        }

        bot.applySettings(new BotSettings());
        GameProfile profile = entity instanceof AbilityPlayer ? NMSUtils.get(((AbilityPlayer) entity).getPlayer()).getNickProfile() : new GameProfile(UUID.randomUUID(), "LifeLine");
        bot.transform(profile, entity.getEntity().getLocation(), false);
        bot.useSoups();
        ability.getMetaData().addEntity(bot.getBukkitEntity(), (int) duration, true);

        return true;
    }

}
