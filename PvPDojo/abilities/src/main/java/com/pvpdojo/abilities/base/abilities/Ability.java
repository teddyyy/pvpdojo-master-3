/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.base.abilities;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilities.gamemode.AbilityEffect.AbilityEffectType;
import com.pvpdojo.abilities.listeners.events.AbilityActivateEvent;
import com.pvpdojo.abilityinfo.AbilityType;
import com.pvpdojo.abilityinfo.KitAttributeArray;
import com.pvpdojo.abilityinfo.KitAttributeArray.AttributeArrayType;
import com.pvpdojo.bukkit.util.GuiItem;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.KitAbilityData;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import eu.the5zig.mod.server.api.ModUser;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Set;

public abstract class Ability {

    private AbilityLoader loader;
    private boolean enabled = false;
    private String ability;
    private String abilityExecutor;
    private AbilityType type;
    private AbilityCooldown cooldown;
    private AbilityEntity abilityEntity;
    private AbilityMetaData metaData;
    private KitAttributeArray baseAttributes;
    private Set<Integer> enabledUpgrades;
    private ItemStack clickItem;

    public Ability(AbilityEntity champion, String ability, String ability_executor, KitAttributeArray base_attributes, KitAbilityData data) {
        this.loader = AbilityLoader.getAbilityData(ability);
        this.abilityEntity = champion;
        this.ability = ability;
        this.type = data.getAbilityType();
        this.abilityExecutor = ability_executor;
        this.cooldown = new AbilityCooldown();
        this.metaData = new AbilityMetaData(this);
        this.baseAttributes = base_attributes;
        this.enabledUpgrades = data.getUpgrades();
        this.clickItem = data.getClickItem() != null ? new ItemBuilder(data.getClickItem())
                .name(GuiItem.getDisplayName(loader.getAbilityIcon()))
                .lore(GuiItem.getLore(loader.getAbilityIcon()))
                .build() : loader.getAbilityIcon();

    }

    public AbilityLoader getLoader() {
        return loader;
    }

    public AbilityType getAbilityType() {
        return type;
    }

    public float getAttribute(String attribute) {
        return loader.getAbilityAttribute(attribute, enabledUpgrades);
    }

    public boolean hasAttribute(String attribute_name) {
        return baseAttributes.hasAttribute(attribute_name);
    }

    public void enableAbility() {
        this.enabled = true;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public AbilityEntity getAbilityEntity() {
        return abilityEntity;
    }

    public void setAbilityEntity(AbilityEntity abilityEntity) {
        this.abilityEntity = abilityEntity;
    }

    public String getAbility() {
        return ability;
    }

    public String getAbilityExecutor() {
        return abilityExecutor;
    }

    public AbilityCooldown getCooldown() {
        return cooldown;
    }

    public AbilityMetaData getMetaData() {
        return metaData;
    }

    public ItemStack getClickItem() {
        return clickItem;
    }

    public void tick() {
        getMetaData().tick();
    }

    public void resetAbility() {
        this.getMetaData().clear();
        AbilityExecutor.getExecutor(getAbilityExecutor()).onReset(this);
    }

    public boolean activateAbility(Event event) {
        return activateAbility(true, event);
    }

    public boolean activateAbility(boolean callExecutor, Event event) {
        if (!isEnabled()) {
            return false;
        }

        if (getAbilityEntity().hasEffect(AbilityEffectType.SILENCE)) {
            getAbilityEntity().sendMessage(MessageKeys.ABILITY_SILENCED);
            return false;
        }
        if (!AbilityExecutor.getExecutor(getAbilityExecutor()).canAbilityExecute(getAbilityEntity().getEntity())) {
            return false;
        }

        if (!hasAttribute("cooldown")) {
            return true;
        }
        float cooldownReduction = (100 + getAbilityEntity().getAttributeValue(AttributeArrayType.APPLIED_BONUS_ATTRIBUTES, "cooldown")) / 100;
        getCooldown().setCooldown((int) (getAttribute("cooldown") * cooldownReduction * 1000));
        getCooldown().setUses((int) getAttribute("ability_uses"));
        float manaUse = getAttribute("mana_use");
        if (getAbilityEntity().getMana() < manaUse) {
            getAbilityEntity().sendMessage(MessageKeys.NOT_ENOUGH_MANA, "{mana}", "" + (manaUse - getAbilityEntity().getMana()));
            return false;
        }

        AbilityActivateEvent activateEvent = new AbilityActivateEvent(getAbilityEntity(), this, getCooldown().isCoolingDown());
        Bukkit.getPluginManager().callEvent(activateEvent);

        if (getCooldown().isCoolingDown()) {
            if (activateEvent.isSendCooldown() && getAbilityType().equals(AbilityType.CLICK) && this instanceof ActiveAbility) {
                getAbilityEntity().sendMessage(MessageKeys.ABILITY_COOLDOWN, "{ability}", loader.getRarity().getColor() + getAbility(), "{time}", "" + (getCooldown().getMSLeft() / 1000));
            }
            return false;
        }
        if (getCooldown().checkNoUse())
            return false;
        if (callExecutor) {
            if (AbilityExecutor.getExecutor(getAbilityExecutor()).activateAbility(this, event)) {
                processMessage(manaUse);
                return true;
            }
        } else {
            processMessage(manaUse);
            return true;
        }

        return false;
    }

    public void processMessage(float manaUse) {
        int delay = 100 + (this.getAbilityType().equals(AbilityType.CLICK) ? 0 : 300);

        if (getCooldown().activate(delay) == 0 && getCooldown().isCoolingDown()) {
            callItemTimer(true);
        }
        getAbilityEntity().setMana(getAbilityEntity().getMana() - manaUse);
    }

    public void callItemTimer(boolean itemInHand) {
        if (getAbilityType() == AbilityType.CLICK && this instanceof ActiveAbility && getAbilityEntity() instanceof AbilityPlayer) {
            ItemStack baseItem = getClickItem();
            ItemStack abilityItem = itemInHand ? ((AbilityPlayer) getAbilityEntity()).getPlayer().getItemInHand() : baseItem;
            new AbilityRunnable(this) {
                @Override
                public void run() {
                    if (getCooldown().isCoolingDown()) {
                        changeName(baseItem.getType(), baseItem.getItemMeta().getDisplayName() + CC.GRAY + " (" + CC.RED + (getCooldown().getMSLeft() / 1000) + CC.GRAY + ")",
                                abilityItem.getItemMeta());
                        ModUser modUser = ModUser.getUser(((AbilityPlayer) getAbilityEntity()).getPlayer());
                        if (modUser != null) {
                            modUser.getStatsManager().getStat(getAbility()).setScore(String.valueOf(getCooldown().getMSLeft() / 1000));
                        }
                    } else {
                        changeName(baseItem.getType(), baseItem.getItemMeta().getDisplayName(), abilityItem.getItemMeta());
                        cancel();
                    }
                }

                @Override
                public void cancel() {
                    super.cancel();
                    ModUser modUser = ModUser.getUser(((AbilityPlayer) getAbilityEntity()).getPlayer());
                    if (modUser != null) {
                        modUser.getStatsManager().resetStat(getAbility());
                    }
                }
            }.createTimer(2, 20, -1);
        } else if (this instanceof ActiveAbility) {
            ModUser modUser = ModUser.getUser(((AbilityPlayer) getAbilityEntity()).getPlayer());

            if (modUser != null) {
                new AbilityRunnable(this) {
                    @Override
                    public void run() {
                        modUser.getStatsManager().getStat(getAbility()).setScore(String.valueOf(getCooldown().getMSLeft() / 1000));
                        if (!getCooldown().isCoolingDown()) {
                            cancel();
                        }
                    }

                    @Override
                    public void cancel() {
                        super.cancel();
                        modUser.getStatsManager().resetStat(getAbility());
                    }
                }.createTimer(2, 20, -1);
            }
            new AbilityRunnable(this) {
                @Override
                public void run() {
                    getAbilityEntity().sendMessage(MessageKeys.ABILITY_COOLDOWN_EXPIRED, "{ability}", loader.getRarity().getColor() + ability);
                }
            }.createTask((int) (getCooldown().getMSLeft() / 1000 * 20));
        }
    }

    public void changeName(Material type, String name, ItemMeta meta) {
        int slot = ((AbilityPlayer) getAbilityEntity()).getPlayer().getInventory().first(type);
        if (meta != null) {
            meta.setDisplayName(name);
            if (slot != -1) {
                ((AbilityPlayer) getAbilityEntity()).getPlayer().getInventory().getItem(slot).setItemMeta(meta);
            }
        }
    }
}
