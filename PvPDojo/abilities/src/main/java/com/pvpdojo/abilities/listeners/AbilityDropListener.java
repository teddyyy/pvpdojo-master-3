/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.listeners;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerPrePickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilityinfo.AbilityType;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.userdata.PlayStyle;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.ItemEditor;
import com.pvpdojo.util.bukkit.ItemBuilder.PotionBuilder;

public class AbilityDropListener implements DojoListener {

    @EventHandler
    public void onInvClick(InventoryClickEvent e) {
        AbilityPlayer cp = AbilityPlayer.get((Player) e.getView().getPlayer());

        if (e.getCurrentItem() != null && e.getCurrentItem().getType() == Material.MUSHROOM_SOUP) {
            if (cp.getPlayStyle() == PlayStyle.POTTER) {
                e.setCurrentItem(new PotionBuilder(PotionType.INSTANT_HEAL).level(2).splash()
                                                                           .amount(e.getCurrentItem().getAmount()).build());
            } else if (cp.getPlayStyle() == PlayStyle.SURVIVALIST) {
                e.setCurrentItem(new ItemStack(Material.GOLD_NUGGET, e.getCurrentItem().getAmount()));
            }
        }
    }

    @EventHandler
    public void onPrepareCraft(PrepareItemCraftEvent e) {

        AbilityPlayer cp = AbilityPlayer.get((Player) e.getView().getPlayer());
        ItemStack item = e.getInventory().getResult();

        for (ItemStack matrix : e.getInventory().getMatrix()) {
            if (matrix != null && matrix.hasItemMeta() && matrix.getItemMeta().hasDisplayName()) {
                e.getInventory().setResult(null);
                return;
            }
        }

        switch (cp.getPlayStyle()) {
            case POTTER:
                if (item.getType() == Material.MUSHROOM_SOUP) {
                    e.getInventory().setResult(new PotionBuilder(PotionType.INSTANT_HEAL).splash().level(1).build());
                }
                break;
            case SURVIVALIST:
                if (item.getType() == Material.MUSHROOM_SOUP) {
                    e.getInventory().setResult(new ItemStack(Material.GOLD_NUGGET));
                } else if (item.getType() == Material.GOLD_PLATE) {
                    e.getInventory().setResult(new ItemStack(Material.GOLDEN_APPLE));
                } else if (item.getType() == Material.GOLD_NUGGET) {
                    e.getInventory().setResult(null);
                }
                break;
        }

    }

    @EventHandler
    public void onPrePickup(PlayerPrePickupItemEvent e) {
        AbilityPlayer cp = AbilityPlayer.get(e.getPlayer());
        ItemStack item = e.getItem().getItemStack();
        if (cp.getKit() != null) {
            switch (cp.getPlayStyle()) {
                case POTTER:
                    if (item.getType() == Material.RED_MUSHROOM
                            || (item.getType() == Material.INK_SACK && item.getDurability() == 3)) {
                        item.setType(Material.SPECKLED_MELON);
                    } else if (item.getType() == Material.BOWL) {
                        item.setType(Material.GLASS_BOTTLE);
                    } else if (item.getType() == Material.MUSHROOM_SOUP) {
                        item.setType(Material.POTION);
                        item.setDurability(
                                new PotionBuilder(PotionType.INSTANT_HEAL).splash().level(2).build().getDurability());
                    } else if (item.getType() == Material.GOLDEN_APPLE) {
                        item.setAmount(item.getAmount() * 5);

                        if (item.hasItemMeta() && item.getItemMeta().hasDisplayName()
                                && item.getItemMeta().getDisplayName().equals(ItemBuilder.GOLDEN_HEAD.build().getItemMeta().getDisplayName())) {
                            item.setAmount(item.getAmount() * 3);
                            new ItemEditor(item).name(null).build();
                        }

                        item.setType(Material.POTION);
                        item.setDurability(
                                new PotionBuilder(PotionType.INSTANT_HEAL).splash().level(2).build().getDurability());
                    } else if (item.getType() == Material.GOLD_INGOT) {
                        item.setType(Material.POTION);
                        item.setDurability(
                                new PotionBuilder(PotionType.INSTANT_HEAL).splash().level(2).build().getDurability());
                        item.setAmount(item.getAmount() * 3);
                    } else if (item.getType() == Material.GOLD_NUGGET) {
                        item.setType(Material.POTION);
                        item.setDurability(
                                new PotionBuilder(PotionType.INSTANT_HEAL).splash().level(2).build().getDurability());
                    }
                    break;
                case SOUP:
                    if (item.getType() == Material.POTION
                            && Potion.fromItemStack(item).getType() == PotionType.INSTANT_HEAL) {
                        item.setType(Material.MUSHROOM_SOUP);
                        item.setData(new MaterialData(Material.MUSHROOM_SOUP));
                    } else if (item.getType() == Material.GLASS_BOTTLE) {
                        item.setType(Material.BOWL);
                    } else if (item.getType() == Material.SPECKLED_MELON) {
                        item.setType(Material.RED_MUSHROOM);
                        e.getItem().getWorld().dropItem(e.getItem().getLocation(),
                                new ItemStack(Material.BROWN_MUSHROOM, item.getAmount())).setPickupDelay(0);
                    } else if (item.getType() == Material.GOLDEN_APPLE) {
                        item.setAmount(item.getAmount() * 5);
                        if (item.hasItemMeta() && item.getItemMeta().hasDisplayName()
                                && item.getItemMeta().getDisplayName().equals(ItemBuilder.GOLDEN_HEAD.build().getItemMeta().getDisplayName())) {
                            item.setAmount(item.getAmount() * 2);
                            new ItemEditor(item).name(null).build();
                        }
                        item.setType(Material.MUSHROOM_SOUP);
                        item.setData(new MaterialData(Material.MUSHROOM_SOUP));
                    } else if (item.getType() == Material.GOLD_INGOT) {
                        item.setType(Material.MUSHROOM_SOUP);
                        item.setData(new MaterialData(Material.MUSHROOM_SOUP));
                        item.setAmount(item.getAmount() * 3);
                    } else if (item.getType() == Material.GOLD_NUGGET) {
                        item.setType(Material.MUSHROOM_SOUP);
                        item.setData(new MaterialData(Material.MUSHROOM_SOUP));
                    }
                    break;
                case SURVIVALIST:
                    if ((item.getType() == Material.POTION
                            && Potion.fromItemStack(item).getType() == PotionType.INSTANT_HEAL)
                            || item.getType() == Material.MUSHROOM_SOUP) {
                        item.setType(Material.GOLD_NUGGET);
                        item.setData(new MaterialData(Material.GOLD_NUGGET));
                        item.setDurability((short) 0);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @EventHandler
    public void onPickup(PlayerPickupItemEvent e) {
        AbilityPlayer cp = AbilityPlayer.get(e.getPlayer());
        if (cp.getKit() != null) {
            switch (cp.getPlayStyle()) {
                case POTTER:
                    if (e.getItem().getItemStack().getType() == Material.BROWN_MUSHROOM
                            || (e.getItem().getItemStack().getType() == Material.INK_SACK
                            && e.getItem().getItemStack().getDurability() == 3)) {
                        e.setCancelled(true);
                        e.getItem().remove();
                    } else if (e.getItem().getItemStack().getType() == Material.GLASS_BOTTLE) {
                        if (e.getPlayer().getInventory().contains(Material.GLASS_BOTTLE,
                                e.getPlayer().getInventory().all(Material.SPECKLED_MELON).values().
                                        stream().mapToInt(ItemStack::getAmount).sum())) {
                            e.setCancelled(true);
                        }
                    }
                    break;
                case SURVIVALIST:
                    if (e.getItem().getItemStack().getType() == Material.RED_MUSHROOM
                            || e.getItem().getItemStack().getType() == Material.BROWN_MUSHROOM
                            || e.getItem().getItemStack().getType() == Material.BOWL
                            || e.getItem().getItemStack().getType() == Material.SPECKLED_MELON
                            || e.getItem().getItemStack().getType() == Material.GLASS_BOTTLE) {
                        e.setCancelled(true);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        AbilityPlayer cp = AbilityPlayer.get(e.getPlayer());
        if (cp.getKit() != null) {
            switch (cp.getPlayStyle()) {
                case POTTER:
                    if (e.getItemDrop().getItemStack().getType() == Material.POTION) {
                        PotionType type = Potion.fromItemStack(e.getItemDrop().getItemStack()).getType();
                        if (type == PotionType.SPEED) {
                            e.setCancelled(true);
                        }
                    }
                    if (e.getItemDrop().getItemStack().getType() == Material.ENDER_PEARL) {
                        e.setCancelled(true);
                    }
                    if (e.getItemDrop().getItemStack().getType().name().contains("LEATHER_")
                            || e.getItemDrop().getItemStack().getType() == Material.DIAMOND_CHESTPLATE
                            || e.getItemDrop().getItemStack().getType() == Material.DIAMOND_BOOTS) {
                        e.getItemDrop().remove();
                    }
                    // else if (event.getItemDrop().getItemStack().getType() ==
                    // Material.GLASS_BOTTLE) {
                    // event.getItemDrop().setItemStack(new ItemStack(Material.BOWL,
                    // event.getItemDrop().getItemStack().getAmount()));
                    // } else if (event.getItemDrop().getItemStack().getType() ==
                    // Material.SPECKLED_MELON) {
                    // event.getItemDrop().setItemStack(new ItemStack(Material.RED_MUSHROOM,
                    // event.getItemDrop().getItemStack().getAmount()));
                    // event.getItemDrop().getWorld()
                    // .dropItem(event.getItemDrop().getLocation(), new
                    // ItemStack(Material.BROWN_MUSHROOM,
                    // event.getItemDrop().getItemStack().getAmount()))
                    // .setVelocity(event.getItemDrop().getVelocity());
                    // }
                    break;
                case SURVIVALIST:
                    if (e.getItemDrop().getItemStack().getType() == Material.FISHING_ROD) {
                        e.setCancelled(true);
                    }
                    break;
                default:
                    break;

            }
            if (cp.getAbilities().stream().anyMatch(ca -> ca.getAbilityType() == AbilityType.CLICK
                    && ca.getClickItem().getType() == e.getItemDrop().getItemStack().getType())) {
                e.setCancelled(true);
            }
        }
    }

}
