/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.gamemode;

import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.entities.AbilityEntity;

public class RunningAbilityEffect extends AbilityEffect {

    private AbilityRunnable runnable;

    public RunningAbilityEffect(AbilityRunnable runnable, AbilityEffectType type, int duration) {
        super(type, duration);
        this.runnable = runnable;
    }

    @Override
    public void applyEffect(AbilityEntity entity) {

    }

    @Override
    public void endEffect(AbilityEntity entity) {
        runnable.cancel();
    }

    @Override
    public void update(AbilityEntity entity) {

    }

}
