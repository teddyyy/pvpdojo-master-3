/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.lib;

import java.util.List;

import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.util.bukkit.BlockUtil;

public abstract class AbilityCrosshair {

    private AbilityEntity activator;

    public AbilityCrosshair(AbilityEntity activator) {
        this.activator = activator;
    }

    public boolean calculate(double accuracy, int distance, boolean single) {
        List<Entity> nearby = activator.getEntity().getNearbyEntities(distance, distance, distance);

        if (nearby.isEmpty()) {
            return false;
        }

        boolean worked = false;
        for (Entity entity : nearby) {
            if (entity instanceof LivingEntity) {
                for (Block b : BlockUtil.getLineOfSight(distance, activator.getEntity())) {
                    LivingEntity living = (LivingEntity) entity;
                    AbilityEntity abilityEntity = AbilityServer.get().getEntity(living);

                    if (abilityEntity == null || !activator.canTarget(abilityEntity)) {
                        continue;
                    }

                    double bx = b.getLocation().getBlockX();
                    double by = b.getLocation().getBlockY();
                    double bz = b.getLocation().getBlockZ();
                    int px = entity.getLocation().getBlockX();
                    int py = entity.getLocation().getBlockY();
                    int pz = entity.getLocation().getBlockZ();
                    if (bx >= px - accuracy && bx <= px + accuracy) {
                        if (by >= py - accuracy && by <= py + accuracy) {
                            if (bz >= pz - accuracy && bz <= pz + accuracy) {
                                worked = true;
                                targetFound(abilityEntity, accuracy, distance);
                                if (single)
                                    break;
                            }
                        }
                    }
                }
            }
        }
        return worked;
    }

    public abstract void targetFound(AbilityEntity target, double accuracy, int distance);

}
