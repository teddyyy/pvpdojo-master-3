/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerDropItemEvent;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilities.gamemode.AbilityServer;

public class DischargeAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "DischargeAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        return false;
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {

        if (e.getItemDrop().getItemStack().getType() == Material.MUSHROOM_SOUP && canAbilityExecute(e.getPlayer())) {
            Player player = e.getPlayer();
            AbilityPlayer source = AbilityPlayer.get(player);

            PvPDojo.schedule(() -> {
                if (!player.isOnline()) {
                    return;
                }
                player.getWorld().playEffect(e.getItemDrop().getLocation(), Effect.EXPLOSION_LARGE, 1);
                player.getWorld().playSound(e.getItemDrop().getLocation(), Sound.EXPLODE, 1, 4);

                Ability ability = getAbility(player);

                if (ability != null) {
                    int damage = (int) ability.getAttribute("ability_damage");

                    for (Entity nearby : e.getItemDrop().getNearbyEntities(2, 2, 2)) {
                        if (nearby instanceof LivingEntity) {
                            AbilityEntity entity = AbilityServer.get().getEntity((LivingEntity) nearby);
                            source.abilityDamage(entity, damage, ability, DamageCause.ENTITY_EXPLOSION);
                        }
                    }
                    e.getItemDrop().remove();
                }
            }).createTask(20);

        }

    }

}
