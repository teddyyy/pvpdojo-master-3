/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.lib;

import com.pvpdojo.abilities.base.entities.AbilityEntity;

public interface ProjectileDetectionCheck {

    boolean canCollide(AbilityEntity activator, AbilityEntity target);
}
