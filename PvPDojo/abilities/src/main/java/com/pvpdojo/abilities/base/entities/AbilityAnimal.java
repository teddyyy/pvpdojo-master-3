/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.base.entities;

import org.bukkit.entity.Animals;

public class AbilityAnimal extends AbilityEntity {

    public AbilityAnimal(Animals entity) {
        super(entity);
    }

    @Override
    public Animals getEntity() {
        return (Animals) super.getEntity();
    }
}
