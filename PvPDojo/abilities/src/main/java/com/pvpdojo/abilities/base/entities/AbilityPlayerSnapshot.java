/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.base.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pvpdojo.abilities.abilities.guns.Gun;
import com.pvpdojo.abilities.abilities.guns.GunPlayer;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilityinfo.KitAttributeArray;
import com.pvpdojo.abilityinfo.KitAttributeArray.AttributeArrayType;
import com.pvpdojo.userdata.KitData;
import com.pvpdojo.userdata.PlayStyle;

public class AbilityPlayerSnapshot {

    private KitData kitData;
    private PlayStyle playStyle;
    private long enderPearlThrow;
    private List<Ability> abilityList;
    private GunPlayer gunPlayer;
    private List<Gun> guns;
    private Map<AttributeArrayType, KitAttributeArray> attributes;
    private float mana;

    public AbilityPlayerSnapshot(AbilityPlayer abilityPlayer) {
        this.kitData = abilityPlayer.getKit();
        this.abilityList = new ArrayList<>(abilityPlayer.getAbilities());
        this.playStyle = abilityPlayer.getPlayStyle();
        this.enderPearlThrow = abilityPlayer.getEnderPearlThrow();
        this.gunPlayer = abilityPlayer.getGunController();
        this.guns = new ArrayList<>(abilityPlayer.getGunController().getGuns());
        this.attributes = new HashMap<>(abilityPlayer.attributes);
        this.mana = abilityPlayer.getMana();
    }

    public void apply(AbilityPlayer abilityPlayer) {
        abilityPlayer.currentKit = kitData;
        abilityPlayer.enderPearlThrow = enderPearlThrow;
        abilityPlayer.playstyle = playStyle;
        abilityList.forEach(ability -> ability.setAbilityEntity(abilityPlayer));
        abilityPlayer.abilities = abilityList;
        gunPlayer.setActivator(abilityPlayer);
        abilityPlayer.gp = gunPlayer;
        guns.forEach(abilityPlayer.gp::registerGun);
        abilityPlayer.attributes = attributes;
        abilityPlayer.mana = mana;
    }

}
