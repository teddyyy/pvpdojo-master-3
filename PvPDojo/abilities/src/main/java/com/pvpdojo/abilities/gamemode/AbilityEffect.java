/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.gamemode;

import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.util.bukkit.CC;

public abstract class AbilityEffect {

    private int tickDuration = 1;
    private AbilityEffectType type;

    public AbilityEffect(AbilityEffectType type, int duration) {
        this.type = type;
        this.tickDuration = duration;
    }

    public AbilityEffectType getEffectType() {
        return type;
    }

    public int getTickDuration() {
        return tickDuration;
    }

    public void update(AbilityEntity entity) {
        if (tickDuration == 1) {
            this.endEffect(entity);
            entity.removeEffect(this);
            return;
        }
        tickDuration--;
    }

    public abstract void applyEffect(AbilityEntity entity);

    public abstract void endEffect(AbilityEntity entity);

    public enum AbilityEffectType {

        SNARE(CC.DARK_PURPLE + "Snare"),
        SILENCE(CC.BLUE + "Silence"),
        MANA_SILENCE(CC.DARK_RED + "Mana Silence"),
        SLOW(CC.DARK_GRAY + "Slowing"),
        BURN(CC.RED + "Burn"),
        BLIND(CC.DARK_GRAY + "Blinding"),
        POISON(CC.GREEN + "Poison"),
        WITHER(CC.BLACK + "Wither"),
        SOULSTEAL(CC.LIGHT_PURPLE + "SoulSteal");

        String name;

        AbilityEffectType(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}
