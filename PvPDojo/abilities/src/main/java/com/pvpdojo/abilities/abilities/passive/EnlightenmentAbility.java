/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.listeners.events.DojoSelectKitEvent;
import com.pvpdojo.abilityinfo.KitAttributeArray.AttributeArrayType;
import com.pvpdojo.abilityinfo.KitAttributeArray.KitAttribute;

public class EnlightenmentAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "EnlightenmentAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        return true;
    }

    @EventHandler
    public void onSelect(DojoSelectKitEvent e) {
        AbilityEntity activator = e.getEntity();
        if (activator.getAbilityFromExecutor(getExecutorName()) == null)
            return;
        activator.addAttribute(new KitAttribute("cooldown", -15, AttributeArrayType.APPLIED_BONUS_ATTRIBUTES));
    }

}
