package com.pvpdojo.abilities.abilities.passive;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.abilities.lib.ChargeSystem;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.util.bukkit.ParticleEffects;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AirBenderAbility extends AbilityExecutor {
    @Override
    public String getExecutorName() {
        return "AirBenderAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        if (event instanceof PlayerToggleSneakEvent) {
            Player player = ((PlayerToggleSneakEvent) event).getPlayer();
            float fast = ability.getAttribute("fast");

            ChargeSystem charge = new ChargeSystem(ability, 100, (int) fast) {

                @Override
                public void trigger() {
                    callAirBender(ability);
                }

                @Override
                public void run() {
                }
            };
            charge.call();
        }

        return false;
    }

    private void callAirBender(Ability ability) {

        Location loc = ability.getAbilityEntity().getEntity().getLocation();
        float max = ability.getAttribute("max");
        new AbilityRunnable(ability) {

            int radius = 5;
            List<AbilityEntity> hit = new ArrayList<>();

            @Override
            public void run() {
                if (++radius > (max > 0 ? 20 : 15)) {
                    hit.clear();
                    cancel();
                    return;
                }

                for (int i = 0; i < 70; i++) {
                    Vector vec = getRandomVelocity().multiply(radius);
                    loc.add(vec);
                    ParticleEffects.CLOUD.sendToLocation(loc, 0, 0, 0, 0, 1);
                    loc.subtract(vec);
                }

                AbilityServer.get()
                        .getNearbyEntities(ability.getAbilityEntity(), loc, radius, radius)
                        .stream()
                        .filter(target -> !hit.contains(target))
                        .forEach(target -> {

                            hit.add(target);
                            ability.getAbilityEntity().abilityDamage(target, 150, ability, EntityDamageEvent.DamageCause.CUSTOM);

                            target.getEntity().setVelocity(target.getEntity().getVelocity()
                                    .add(target.getEntity().getLocation().toVector().subtract(ability.getAbilityEntity().getEntity().getLocation().toVector())
                                            .normalize().multiply(4.2).add(new Vector(0, 1.5, 0))));
                        });

            }
        }.createTimer(1, -1);

    }

    @EventHandler
    public void onAirBender(PlayerToggleSneakEvent e) {
        if (canAbilityExecute(e.getPlayer())) {
            getAbility(e.getPlayer()).activateAbility(e);
        }
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) {
        if (e.getCause() == EntityDamageEvent.DamageCause.FALL && !e.isCancelled() && e.getEntity() instanceof LivingEntity) {
            if (canAbilityExecute((LivingEntity) e.getEntity())) {
                e.setCancelled(true);
            }
        }
    }

    private Vector getRandomVelocity() {
        final Random random = new Random(System.nanoTime());

        double x = random.nextDouble() * 2 - 1;
        double y = random.nextDouble() * 2 - 1;
        double z = random.nextDouble() * 2 - 1;

        return new Vector(x, y, z).normalize();
    }
}
