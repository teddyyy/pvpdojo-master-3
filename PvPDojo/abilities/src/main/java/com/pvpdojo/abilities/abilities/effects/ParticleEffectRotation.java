/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

//http://ksuweb.kennesaw.edu/~plaval//math4490/rotgen.pdf

package com.pvpdojo.abilities.abilities.effects;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

import org.bukkit.Location;
import org.bukkit.util.Vector;

public class ParticleEffectRotation {

    public static Vector rotateFunction(Vector u0, Location loc) {
        double yawR = loc.getYaw() / 180.0 * Math.PI;

        double pitchR = -loc.getPitch() / 180.0 * Math.PI;

        if (yawR < 0) {
            yawR = 2 * Math.PI + yawR;
        }

        // axis perpendicular to looking direction
        Vector rotationAxis = new Vector(-loc.getDirection().getZ(), 0.0, loc.getDirection().getX());
        rotationAxis.normalize();

        Vector u1 = rotateAboutY(u0, -yawR);

        return rotateAboutAxis(rotationAxis, u1, pitchR);
    }

    // Axis Rotation Functions
    public static Vector rotateAboutX(Vector vect, double a) {
        double Y = cos(a) * vect.getY() - sin(a) * vect.getZ();
        double Z = sin(a) * vect.getY() + cos(a) * vect.getZ();
        return vect.setY(Y).setZ(Z);
    }

    public static Vector rotateAboutY(Vector vect, double b) {
        double X = cos(b) * vect.getX() + sin(b) * vect.getZ();
        double Z = -sin(b) * vect.getX() + cos(b) * vect.getZ();
        return vect.setX(X).setZ(Z);
    }

    public static Vector rotateAboutZ(Vector vect, double c) {
        double X = cos(c) * vect.getX() - sin(c) * vect.getY();
        double Y = sin(c) * vect.getX() + cos(c) * vect.getY();
        return vect.setX(X).setY(Y);
    }

    public static Vector rotateAboutAxis(Vector axis, Vector u1, double theta) {
        axis.normalize();

        double x = u1.getX();
        double y = u1.getY();
        double z = u1.getZ();

        double ux = axis.getX();
        double uy = axis.getY();
        double uz = axis.getZ();

        double c = Math.cos(theta);
        double s = Math.sin(theta);
        double t = 1 - Math.cos(theta);

        double gammaX = (x * (t * Math.pow(ux, 2) + c)) + (y * (t * ux * uy - s * uz)) + (z * (t * ux * uz + s * uy));
        double gammaY = (x * (t * ux * uy + s * uz)) + (y * (t * Math.pow(uy, 2) + c)) + (z * (t * uy * uz - s * ux));
        double gammaZ = (x * (t * ux * uz - s * uy)) + (y * (t * uy * uz + s * ux)) + (z * (t * Math.pow(uz, 2) + c));

        return new Vector(gammaX, gammaY, gammaZ);
    }

}
