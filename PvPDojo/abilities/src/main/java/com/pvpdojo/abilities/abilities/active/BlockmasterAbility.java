/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import java.util.ArrayList;
import java.util.Collections;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.Event;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.bukkit.util.TempBlock;
import com.pvpdojo.util.bukkit.BlockUtil;

public class BlockmasterAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "BlockmasterAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        final AbilityEntity activator = ability.getAbilityEntity();
        ArrayList<Block> list = BlockUtil.getLineOfSight(5, activator.getEntity());
        Collections.reverse(list);
        final float duration = ability.getAttribute("block_duration");
        for (Block b : list) {
            if (b.getType().equals(Material.AIR)) {
                TempBlock.createTempBlock(b.getLocation(), Material.DIAMOND_BLOCK, (byte) 0, (long) duration);
                break;
            }
        }
        return true;
    }

}