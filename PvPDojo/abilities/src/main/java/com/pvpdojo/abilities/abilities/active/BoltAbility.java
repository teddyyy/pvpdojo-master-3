/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.event.Event;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;

public class BoltAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "BoltAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        ability.getAbilityEntity().getEntity().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 5 * 20, 5));
        return true;
    }
}
