/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import static com.pvpdojo.util.StringUtils.getEnumName;

import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Minecart;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.bukkit.event.vehicle.VehicleUpdateEvent;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.listeners.events.AbilityActivateEvent;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.util.bukkit.BlockUtil;
import com.pvpdojo.util.EnumOrderAccess;

public class RacerAbility extends AbilityExecutor {

    private static final String SPEED = "speed";

    @Override
    public String getExecutorName() {
        return "RacerAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        if (ability.getAbilityEntity().getEntity().getVehicle() == null) {

            int duration = (int) ability.getAttribute("duration");

            ability.getMetaData().addData(SPEED, RacerSpeed.SLOW);

            LivingEntity entity = ability.getAbilityEntity().getEntity();
            Minecart cart = (Minecart) entity.getWorld().spawnEntity(entity.getEyeLocation(), EntityType.MINECART);
            cart.setPassenger(entity);
            cart.setMaxSpeed(3.0);
            cart.setVelocity(entity.getLocation().getDirection().multiply(1.1));

            ability.getMetaData().addEntity(cart, duration, false);
            return true;
        } else {
            return false;
        }

    }

    @EventHandler
    public void onAbilityActivate(AbilityActivateEvent e) {
        if (e.getAbility().getAbilityExecutor().equals(getExecutorName()) && e.isCooldown() && canAbilityExecute(e.getEntity().getEntity()) && e.getEntity().getEntity().getVehicle() != null) {
            RacerSpeed curSpeed = e.getAbility().getMetaData().getData(SPEED);
            e.getAbility().getMetaData().addData(SPEED, curSpeed.next());
            e.getEntity().sendMessage(MessageKeys.RACER_SPEED, "{speed}", getEnumName(curSpeed.next()));
            e.setSendCooldown(false);
        }
    }

    @EventHandler
    public void onVehicleLeave(VehicleExitEvent e) {
        if (e.getVehicle().getType() == EntityType.MINECART) {
            if (canAbilityExecute(e.getExited()) && getAbility(e.getExited()).getMetaData().hasEntity(e.getVehicle())) {
                e.getVehicle().remove();
            }
        }
    }

    @EventHandler
    public void onVehicleUpdate(VehicleUpdateEvent e) {
        if (e.getVehicle().getType() == EntityType.MINECART && e.getVehicle().getPassenger() instanceof LivingEntity) {
            Minecart cart = (Minecart) e.getVehicle();
            LivingEntity entity = (LivingEntity) cart.getPassenger();

            if (canAbilityExecute(entity)) {

                BlockFace face = BlockUtil.getClosestFace(entity.getLocation().getYaw());
                cart.getLocation().setYaw(entity.getLocation().getYaw());
                double y = 0;
                Material faced = cart.getLocation().getBlock().getRelative(face).getType();
                Material facedAbove = cart.getLocation().getBlock().getRelative(face).getLocation().clone().add(0, 1, 0).getBlock().getType();
                if (faced.isSolid() && (!facedAbove.isSolid())) {
                    y += 1;
                }
                if (!cart.isOnGround() && !faced.isSolid() || facedAbove.isSolid()) {
                    y += -1;
                }

                double speed = ((RacerSpeed) getAbility(entity).getMetaData().getData(SPEED)).getSpeed();
                cart.setVelocity(entity.getEyeLocation().getDirection().multiply((cart.isOnGround() ? speed : speed / 2)).setY(y));
            }
        }
    }

    @EventHandler
    public void onVehicleDestroy(VehicleDestroyEvent e) {
        if (e.getVehicle().getType() == EntityType.MINECART && e.getVehicle().getPassenger() != null && canAbilityExecute((LivingEntity) e.getVehicle().getPassenger())
                && getAbility((LivingEntity) e.getVehicle().getPassenger()).getMetaData().hasEntity(e.getVehicle())) {
            e.setCancelled(true);
            e.getVehicle().eject();
            e.getVehicle().remove();
        }
    }

    private enum RacerSpeed implements EnumOrderAccess<RacerSpeed> {
        SLOW(0.8), MEDIUM(1.4), FAST(2D);


        private double speed;

        RacerSpeed(double speed) {
            this.speed = speed;
        }

        public double getSpeed() {
            return speed;
        }

    }

}
