/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Snowball;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.effects.ParticleEffectRotation;
import com.pvpdojo.abilities.abilities.lib.ProjectileEntity;
import com.pvpdojo.abilities.abilities.lib.ProjectileLoop;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.gamemode.BlindEffect;
import com.pvpdojo.abilities.gamemode.WitherEffect;
import com.pvpdojo.bukkit.util.TempBlock;
import com.pvpdojo.util.bukkit.ParticleEffects;

public class IcelordAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "IcelordAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        final AbilityEntity activator = ability.getAbilityEntity();

        final int uses = ability.getCooldown().getUsesLeft();

        final float freeze_time = ability.getAttribute("freeze_duration");
        final float beam_damage = ability.getAttribute("blindness_damage");
        final float blindness_duration = ability.getAttribute("blindness_duration");
        final float cage_time = ability.getAttribute("ice_duration");

        if (uses == 3) {
            ProjectileEntity projectile = new ProjectileEntity(activator, false, 1f);
            Snowball snowball = activator.getEntity().launchProjectile(Snowball.class);

            projectile.startProjectileCollision(snowball);

            projectile.setProjectileDetection(target -> target.getEntity()
                    .addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (int) (20 * freeze_time), 25)));

        } else if (uses == 2) {

            ProjectileEntity projectile = new ProjectileEntity(activator, false, 1.5f);
            activator.getEntity().getWorld().playSound(activator.getEntity().getLocation(), Sound.EXPLODE, 2.0F, -1.0F);

            projectile.setProjectileLoop(loc -> {
                Material type = Material.OBSIDIAN;
                loc.getWorld().playEffect(loc, Effect.STEP_SOUND, type);
                return false;
            });

            projectile.setProjectileDetection(target -> {

                activator.abilityDamage(target, beam_damage, ability, DamageCause.MAGIC);
                target.applyEffect(new BlindEffect((int) blindness_duration, 5));

            });
            projectile.startBlockCollision(activator.getEntity().getEyeLocation(), 1, 2, 40);

        } else if (uses == 1) {
            ProjectileEntity projectile = new ProjectileEntity(activator, false, 1.5f);
            activator.getEntity().getWorld().playSound(activator.getEntity().getLocation(), Sound.EXPLODE, 2.0F, -1.0F);

            Location playerLoc = ability.getAbilityEntity().getEntity().getEyeLocation();

            projectile.setProjectileLoop(new ProjectileLoop() {

                double t = 0;
                double r = 1.1;

                @Override
                public boolean onLoop(Location loc) {
                    t += Math.PI / 15;

                    double x, y, z;

                    for (double i = 0; i < r * 3; i += 1.23) {
                        x = Math.cos(t + i) * r;
                        y = Math.sin(t + i) * r;
                        z = t / t;

                        Vector v = new Vector(x, y, z);
                        v = ParticleEffectRotation.rotateFunction(v, playerLoc);

                        loc.add(v.getX(), v.getY(), v.getZ());
                        ParticleEffects.CLOUD.sendToLocation(loc, 0, 0, 0, 0, 1);
                        loc.subtract(v.getX(), v.getY(), v.getZ());

                        x = -x;
                        y = -y;

                        Vector v1 = new Vector(x, y, z);
                        v1 = ParticleEffectRotation.rotateFunction(v1, playerLoc);

                        loc.add(v1.getX(), v1.getY(), v1.getZ());
                        ParticleEffects.CLOUD.sendToLocation(loc, 0, 0, 0, 0, 1);
                        loc.subtract(v1.getX(), v1.getY(), v1.getZ());
                    }

                    loc.getWorld().playEffect(loc, Effect.STEP_SOUND, Material.SNOW_BLOCK);

                    return false;
                }
            });

            projectile.setProjectileDetection(target -> {

                playEffectTo(target, (int) cage_time);

                for (AbilityEntity abilityEntity : AbilityServer.get().getNearbyEntities(projectile.activator,
                        target.getEntity().getLocation(), 10, 5)) {

                    playEffectTo(abilityEntity, (int) cage_time);
                }

                int radius = 7;

                for (int x = -radius; x < radius; x++) {
                    for (int y = -3; y < 3; y++) {
                        for (int z = -radius; z < radius; z++) {

                            Location loc = target.getEntity().getLocation().clone().add(x, y, z);
                            Material below = loc.getBlock().getRelative(BlockFace.DOWN).getType();

                            if (loc.getBlock().getType().equals(Material.SNOW) || !below.isSolid()) {
                                continue;
                            }

                            TempBlock.createTempBlock(loc, Material.SNOW, (byte) 0, (long) cage_time);
                        }
                    }
                }

            });

            projectile.startBlockCollision(activator.getEntity().getEyeLocation(), 1, 1, 40);// was 1, 2, 40
        }

        return true;
    }

    private void playEffectTo(AbilityEntity target, int upgrade) {
        Location targetCenter = target.getEntity().getLocation().clone().add(0, 0, 0);
        Location targetTp = targetCenter.getWorld()
                .getBlockAt(targetCenter.getBlockX(), targetCenter.getBlockY(), targetCenter.getBlockZ()).getLocation()
                .add(.45, .45, .45);

        createIceBox(target, upgrade);

        target.getEntity().teleport(targetTp);

        targetCenter.getWorld().playEffect(targetCenter, Effect.STEP_SOUND, Material.ICE);
        target.applyEffect(new WitherEffect(20 * upgrade, 2));
    }

    private void createIceBox(AbilityEntity target, int duration) {
        Location center = target.getEntity().getLocation();
        for (int x = -1; x < 2; x++) {
            for (int y = -1; y < 3; y++) {
                for (int z = -1; z < 2; z++) {
                    Location loc = center.clone().add(x, y, z);
                    TempBlock.createTempBlock(loc, Material.PACKED_ICE, (byte) 0, duration);
                }
            }
        }
    }
}
