/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.effects.ParticleEffectRotation;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.gamemode.BlindEffect;
import com.pvpdojo.abilities.gamemode.BurnEffect;
import com.pvpdojo.abilities.gamemode.SlowEffect;
import com.pvpdojo.util.bukkit.ParticleEffects;

//THIS IS BROKEN (or the rotate functions are broken)
public class BeamIIAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "BeamIIAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        final AbilityEntity activator = ability.getAbilityEntity();
        final Player player = (Player) activator.getEntity();

        final float damage = ability.getAttribute("ability_damage");
        final float beamRange = ability.getAttribute("beam_range");
        final float slow = ability.getAttribute("slow_effect");
        final float fire = ability.getAttribute("fire_effect");
        final float blind = ability.getAttribute("blind_effect");

        final ArrayList<String> effects = new ArrayList<>();
        if (slow > 0)
            effects.add("slow");
        if (fire > 0)
            effects.add("fire");
        if (blind > 0)
            effects.add("blind");

        final Location loc = player.getLocation();
        final Location locMain = player.getLocation();
        loc.getWorld().playSound(loc, Sound.FIREWORK_LARGE_BLAST2, 1, 3);
        new AbilityRunnable(ability) {
            ArrayList<AbilityEntity> hit = new ArrayList<>();
            Location locTrack;
            double t = 0;

            public void run() {
                t = t + Math.PI / 6;
                double r = 1.0;
                // double r = 1.5;
                int beamCount = effects.size();

                Vector v0 = locMain.getDirection().normalize().multiply(1.2 * t);

                locMain.add(v0);
                locMain.add(0, 1.5, 0);
                locTrack = locMain.clone();
                ParticleEffects.FIREWORK_SPARK.sendToLocation(locMain, 0.3f, 0.3f, 0.3f, 0, 10);
                locMain.subtract(v0);
                locMain.subtract(0, 1.5, 0);
                for (int i = 0; i < beamCount; i++) {
                    double x = r * Math.cos(t + (2 * Math.PI / beamCount) * i);
                    double y = r * Math.sin(t + (2 * Math.PI / beamCount) * i);
                    double z = 2 * t;
                    Vector v = new Vector(x, y, z);
                    v = ParticleEffectRotation.rotateFunction(v, loc);
                    loc.add(v);
                    loc.add(0, 1.5, 0);
                    float spread = 0.1f;
                    int particles = 30;
                    switch (effects.get(i)) {
                    case "slow":
                        ParticleEffects.BLUE_SPARKLE.sendToLocation(loc, spread, spread, spread, 0, particles);
                        break;
                    case "fire":
                        ParticleEffects.FIRE.sendToLocation(loc, spread, spread, spread, 0, particles);
                        break;
                    case "blind":
                        ParticleEffects.GREEN_SPARKLE.sendToLocation(loc, spread, spread, spread, 0, particles);
                        break;
                    }

                    loc.subtract(v);
                    loc.subtract(0, 1.5, 0);
                }

                List<AbilityEntity> nearby = AbilityServer.get().getNearbyEntities(activator, locTrack, 3, 3);

                for (AbilityEntity target : nearby) {

                    if (hit.contains(target))
                        continue;
                    activator.abilityDamage(target, damage, ability, DamageCause.MAGIC);
                    locTrack.getWorld().playSound(locTrack, Sound.EXPLODE, .3f, 1);
                    Vector dir = loc.getDirection().multiply(1.2);
                    target.getEntity().setVelocity(new Vector(dir.getX(), Math.abs(dir.getY()) + 0.4, dir.getZ()));

                    if (effects.contains("slow")) {
                        target.applyEffect(new SlowEffect(5 * 20, -.1f));
                    }
                    if (effects.contains("fire")) {
                        target.applyEffect(new BurnEffect(8 * 20));
                    }
                    if (effects.contains("blind")) {
                        target.applyEffect(new BlindEffect(5 * 20, 2));
                    }
                    hit.add(target);
                }

                if (t > beamRange) {
                    this.cancel();
                }

            }
        }.createTimer(1, -1);

        return true;

    }

}
