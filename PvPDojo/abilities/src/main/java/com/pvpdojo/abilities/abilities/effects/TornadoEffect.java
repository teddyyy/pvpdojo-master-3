/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.effects;

import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.pvpdojo.abilities.Abilities;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.gamemode.BurnEffect;
import com.pvpdojo.util.bukkit.BlockUtil;

public class TornadoEffect {

	public static void spawnTornado(final Ability ability, final Location location, final Vector direction,
			final double speed, final int amount_of_blocks, final float time) {

		class VortexBlock {

			private FallingBlock entity;

			private float ticker_vertical = 0.0f;
			private float ticker_horisontal = (float) (Math.random() * 2 * Math.PI);

			@SuppressWarnings("deprecation")
			public VortexBlock(Location l) {
				Block groundBlock = BlockUtil.getFirstBlockDown(l, null).getBlock();

				Material baseBlock = groundBlock.getType();
				byte baseData = groundBlock.getData();
				if (groundBlock.getType() == Material.LAVA || groundBlock.getType() == Material.STATIONARY_LAVA) {
					baseData = (byte) 1;
					baseBlock = Material.STAINED_CLAY;
				}
				if (groundBlock.getType() == Material.WATER || groundBlock.getType() == Material.STATIONARY_WATER)
					baseBlock = Material.LAPIS_BLOCK;

				if (groundBlock.getType() == Material.VINE || groundBlock.getType() == Material.LONG_GRASS)
					baseBlock = Material.GRASS;

				if (groundBlock.getType() == Material.LEAVES || groundBlock.getType() == Material.LEAVES_2) {
					baseData = new Random().nextInt(2) == 1 ? (byte) 5 : (byte) 13;
					baseBlock = Material.STAINED_CLAY;
				}

				FallingBlock fallingBlock = l.getWorld().spawnFallingBlock(l, baseBlock, baseData);
				fallingBlock.setDropItem(false);
				entity = fallingBlock;

			}

			public void remove() {
				entity.remove();
			}

			public void tick() {

				double radius = Math.sin(verticalTicker()) * ability.getAttribute("radius");
				float horisontal = horisontalTicker();

				Vector v = new Vector(radius * Math.cos(horisontal), 0.8D, radius * Math.sin(horisontal));

				for (AbilityEntity le : AbilityServer.get().getNearbyEntities(ability.getAbilityEntity(),
						entity.getLocation(), ability.getAttribute("radius"), ability.getAttribute("radius"))) {

					le.getEntity().setVelocity(v);
					if (entity.getBlockId() == 51 || entity.getBlockId() == 159 && entity.getBlockData() == 1) {
						le.applyEffect(new BurnEffect(20 * 5));
					} else if (entity.getBlockId() == 22) {
						le.getEntity().setFireTicks(0);
					}

					ability.getAbilityEntity().abilityDamage(le, ability.getAttribute("ability_damage"), ability,
							DamageCause.MAGIC);

				}
				setVelocity(v);

			}

			private void setVelocity(Vector v) {
				entity.setVelocity(v);
			}

			private float verticalTicker() {
				if (ticker_vertical < 1.0f) {
					ticker_vertical += 0.05f;
				}
				return ticker_vertical;
			}

			private float horisontalTicker() {
				return (ticker_horisontal += 0.8f);
			}
		}

		if (direction != null) {
			direction.normalize().multiply(speed);
		}

		final HashSet<VortexBlock> clear = new HashSet<VortexBlock>();

		final int id = new BukkitRunnable() {

			private ArrayDeque<VortexBlock> blocks = new ArrayDeque<VortexBlock>();
			int timer = 0;

			public void run() {

				if (direction != null) {
					location.add(direction);
				}

				timer++;
				if (timer % 10 >= 3) {
					location.getWorld().playSound(location, Sound.WITHER_IDLE, new Random().nextInt(5), 0);
				}

				for (int i = 0; i < 10; i++) {
					checkListSize();
					VortexBlock vb = new VortexBlock(location);
					blocks.add(vb);
					clear.add(vb);
				}

				for (VortexBlock vb : blocks) {
					vb.tick();
				}

			}

			private void checkListSize() {
				while (blocks.size() >= amount_of_blocks) {
					VortexBlock vb = blocks.getFirst();
					vb.remove();
					blocks.remove(vb);
					clear.remove(vb);
				}
			}
		}.runTaskTimer(Abilities.get(), 5L, 5L).getTaskId();

		new AbilityRunnable(ability) {
			int timeOver = 0;

			public void run() {
				if (++timeOver >= 20 * time) {
					cancel();
				}
			}

			@Override
			public void cancel() {
				super.cancel();
				for (VortexBlock vb : clear) {
					vb.remove();
				}
				Bukkit.getServer().getScheduler().cancelTask(id);
			}
		}.createTimer(1, -1);
	}
}