/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.listeners.events;

import org.bukkit.event.Cancellable;

import com.pvpdojo.abilities.base.entities.AbilityEntity;

public class AbilityTargetEvent extends AbilityEvent implements Cancellable {
    public AbilityEntity attacker;
    public AbilityEntity victim;
    private boolean cancelled = false;

    public AbilityTargetEvent(AbilityEntity victim, AbilityEntity attacker) {
        super(victim);
        this.attacker = attacker;
    }

    public AbilityEntity getAttacker() {
        return attacker;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }
}
