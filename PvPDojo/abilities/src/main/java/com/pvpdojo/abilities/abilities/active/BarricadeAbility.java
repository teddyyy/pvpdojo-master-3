/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.util.Vector;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.bukkit.util.TempBlock;

public class BarricadeAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "BarricadeAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        final LivingEntity activator = ability.getAbilityEntity().getEntity();
        final Location loc = activator.getLocation();
        float duration = ability.getAttribute("duration");
        float width = ability.getAttribute("width");
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < 4; y++) {
                final float newZ = (float) (loc.getZ() + ((x - width / 2) * Math.sin(Math.toRadians(loc.getYaw()))));
                final float newX = (float) (loc.getX() + ((x - width / 2) * Math.cos(Math.toRadians(loc.getYaw()))));
                Location bloc = new Location(loc.getWorld(), newX, loc.getY() + .5, newZ, loc.getYaw(), loc.getPitch());
                Vector vec = bloc.getDirection().multiply(3);
                Location set = bloc.add(vec);
                TempBlock.createTempBlock(set.clone().add(0, y, 0), Material.GLASS, (byte) 0, (long) duration);
            }
        }
        return true;
    }

}
