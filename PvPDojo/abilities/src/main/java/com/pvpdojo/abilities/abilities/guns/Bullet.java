/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.guns;

import java.util.ArrayList;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.lib.ProjectileDetection;
import com.pvpdojo.abilities.abilities.lib.ProjectileEntity;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.util.bukkit.ParticleEffects;

public class Bullet {
    private int ticks;
    private int releaseTime;
    private boolean dead = false;
    private boolean active = true;
    private boolean destroyNextTick = false;
    private boolean released = false;
    private Entity projectile;
    private Vector velocity;
    private Location lastLocation;
    private Location startLocation;
    private GunPlayer shooter;
    private Gun shotFrom;

    public Bullet(GunPlayer owner, Vector vec, Gun gun) {
        this.shotFrom = gun;
        this.shooter = owner;
        this.velocity = vec;

        String check = gun.projType.replace(" ", "").replace("_", "");

        if (check.equalsIgnoreCase("egg")) {
            this.projectile = owner.getController().launchProjectile(Egg.class);
            ((Projectile) this.projectile).setShooter(owner.getController());
            this.startLocation = this.projectile.getLocation();
        }
        if (check.equalsIgnoreCase("arrow")) {
            this.projectile = owner.getController().launchProjectile(Arrow.class);
            ((Projectile) this.projectile).setShooter(owner.getController());
            this.startLocation = this.projectile.getLocation();
        } else {
            this.projectile = owner.getController().launchProjectile(Snowball.class);
            ((Projectile) this.projectile).setShooter(owner.getController());
            this.startLocation = this.projectile.getLocation();
        }
        ProjectileEntity proj = new ProjectileEntity(this.shooter.getActivator());
        proj.setProjectileDetection(getProjectileDetection(shooter.getActivator()));
        proj.startProjectileCollision(projectile);
        proj.disableKnockback();
        if (this.shotFrom.getReleaseTime() == -1)
            this.releaseTime = 80;
        else
            this.releaseTime = this.shotFrom.getReleaseTime();
    }

    private ProjectileDetection getProjectileDetection(final AbilityEntity activator) {
        return target -> {

            boolean headshot = false;
            LivingEntity hurt = target.getEntity();
            if ((isNear(projectile.getLocation(), hurt.getEyeLocation())) && (getGun().canHeadShot())) {
                headshot = true;
            }
            float damage = getGun().getGunDamage();
            float mult = 1.0f;
            if (headshot) {
                ParticleEffects.CRITICAL_HIT.sendToLocation(hurt.getEyeLocation(), 0, 0, 0, 0, 1);
                mult = getGun().getHeadShotMultiplier();
            }
            float final_damage = damage * mult;
            if (activator.abilityDamage(target, final_damage, getGun().getAbility(), DamageCause.PROJECTILE)) {
                hurt.setNoDamageTicks(1);
                getGun().doKnockback(hurt, getVelocity());
            }
            remove();

            /*
             * onHit(); setNextTickDestroy(); Block b = projectile.getLocation().getBlock(); int id = b.getTypeId(); for (double i = 0.2D; i < 4.0D; i += 0.2D) { if (id == 0) { b =
             * projectile.getLocation().add(projectile.getVelocity().normalize().multiply(i)).getBlock(); id = b.getTypeId(); } } if (id > 0) {
             * projectile.getLocation().getWorld().playEffect(b.getLocation(), Effect.STEP_SOUND, id); }
             */
        };

    }

    private boolean isNear(Location location, Location eyeLocation) {
        return Math.abs(location.getY() - eyeLocation.getY()) <= 0.26;
    }

    public void tick() {
        if (!this.dead) {
            this.ticks += 1;
            if (this.projectile != null) {
                this.lastLocation = this.projectile.getLocation();

                if (this.ticks > this.releaseTime) {
                    GunEffect eff = this.shotFrom.getReleaseEffect();
                    if (eff != null) {
                        eff.start(this.lastLocation);
                    }

                    this.dead = true;
                    return;
                }

                if (this.shotFrom.hasSmokeTrail()) {
                    this.lastLocation.getWorld().playEffect(this.lastLocation, Effect.SMOKE, 0);
                }

                if (this.active) {
                    if (this.lastLocation.getWorld().equals(this.startLocation.getWorld())) {
                        double dis = this.lastLocation.distance(this.startLocation);
                        if (dis > this.shotFrom.getMaxDistance()) {
                            this.active = false;
                            if (!this.shotFrom.canGoPastMaxDistance())
                                this.velocity.multiply(0.25D);
                        }
                    }
                    this.projectile.setVelocity(this.velocity);
                }
            } else {
                this.dead = true;
            }
            if (this.ticks > 200)
                this.dead = true;
        } else {
            remove();
        }

        if (this.destroyNextTick)
            this.dead = true;
    }

    public Gun getGun() {
        return this.shotFrom;
    }

    public GunPlayer getShooter() {
        return this.shooter;
    }

    public Vector getVelocity() {
        return this.velocity;
    }

    public void remove() {
        this.dead = true;
        this.shotFrom.removeBullet(this);
        this.projectile.remove();
        onHit();
        destroy();
    }

    public void onHit() {
        if (this.released)
            return;
        this.released = true;
        if (this.projectile != null) {
            this.lastLocation = this.projectile.getLocation();

            if (this.shotFrom != null) {
                int rad = (int) this.shotFrom.getExplodeRadius();
                int rad2 = rad;
                if (this.shotFrom.getFireRadius() > rad) {
                    rad = (int) this.shotFrom.getFireRadius();
                    rad2 = 2;
                    for (int i = -rad; i <= rad; i++) {
                        for (int ii = -rad2 / 2; ii <= rad2 / 2; ii++) {
                            for (int iii = -rad; iii <= rad; iii++) {
                                Location nloc = this.lastLocation.clone().add(i, ii, iii);
                                if ((nloc.distance(this.lastLocation) <= rad) && (PvPDojo.RANDOM.nextInt(5) == 1))
                                    this.lastLocation.getWorld().playEffect(nloc, Effect.MOBSPAWNER_FLAMES, 2);
                            }
                        }
                    }
                } else if (rad > 0) {
                    for (int i = -rad; i <= rad; i++) {
                        for (int ii = -rad2 / 2; ii <= rad2 / 2; ii++) {
                            for (int iii = -rad; iii <= rad; iii++) {
                                Location nloc = this.lastLocation.clone().add(i, ii, iii);
                                if ((nloc.distance(this.lastLocation) <= rad) && (PvPDojo.RANDOM.nextInt(10) == 1))
                                    new GunExplosion(nloc).explode();
                            }
                        }
                    }
                    new GunExplosion(this.lastLocation).explode();
                }

                explode();
                fireSpread();
                flash();
            }
        }
    }

    public void explode() {
        if (this.shotFrom.getExplodeRadius() > 0.0D) {
            this.lastLocation.getWorld().createExplosion(this.lastLocation, 0.0F);
            int c = (int) this.shotFrom.getExplodeRadius();
            ArrayList<Entity> entities = (ArrayList<Entity>) this.projectile.getNearbyEntities(c, c, c);
            for (Entity entity : entities)
                if (((entity instanceof LivingEntity)) && (((LivingEntity) entity).hasLineOfSight(this.projectile))) {
                    float dmg = this.shotFrom.getExplosionDamage();
                    if (dmg == -1) {
                        dmg = this.shotFrom.getGunDamage();
                    }
                    ((LivingEntity) entity).setLastDamage(0);
                    ((LivingEntity) entity).damage(dmg, this.shooter.getController());
                    ((LivingEntity) entity).setLastDamage(0);
                }
        }
    }

    public void fireSpread() {
        if (this.shotFrom.getFireRadius() > 0.0D) {
            this.lastLocation.getWorld().playSound(this.lastLocation, Sound.GLASS, 20.0F, 20.0F);
            int c = (int) this.shotFrom.getFireRadius();
            ArrayList<Entity> entities = (ArrayList<Entity>) this.projectile.getNearbyEntities(c, c, c);
            for (int i = 0; i < entities.size(); i++)
                if ((entities.get(i) instanceof LivingEntity)) {
                    if (((LivingEntity) entities.get(i)).hasLineOfSight(this.projectile)) {
                        entities.get(i).setFireTicks(140);
                        ((LivingEntity) entities.get(i)).damage(1, this.shooter.getController());
                    }
                }
        }
    }

    public void flash() {
        if (this.shotFrom.getFlashRadius() > 0.0D) {
            this.lastLocation.getWorld().playSound(this.lastLocation, Sound.SPLASH, 20.0F, 20.0F);
            int c = (int) this.shotFrom.getFlashRadius();
            ArrayList<Entity> entities = (ArrayList<Entity>) this.projectile.getNearbyEntities(c, c, c);
            for (int i = 0; i < entities.size(); i++)
                if ((entities.get(i) instanceof LivingEntity)) {
                    if (((LivingEntity) entities.get(i)).hasLineOfSight(this.projectile)) {
                        ((LivingEntity) entities.get(i)).addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 140, 1));
                    }
                }
        }
    }

    public void destroy() {
        this.projectile = null;
        this.velocity = null;
        this.shotFrom = null;
        this.shooter = null;
        this.startLocation = null;
        this.lastLocation = null;
    }

    public Entity getProjectile() {
        return this.projectile;
    }

    public void setNextTickDestroy() {
        this.destroyNextTick = true;
    }
}