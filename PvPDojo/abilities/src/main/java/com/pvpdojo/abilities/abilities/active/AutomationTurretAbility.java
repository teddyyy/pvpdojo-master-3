/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.BlockState;
import org.bukkit.craftbukkit.v1_7_R4.entity.disguise.CraftItemDisguise;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Minecart;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.event.vehicle.VehicleMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilities.listeners.events.AbilityActivateEvent;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.Hologram;

public class AutomationTurretAbility extends AbilityExecutor {

	private static final Map<UUID, Turret> TURRETS = new HashMap<>();
	private static final String MISSLE = "missle";
	private static final String TURRET_ARROW = "turret_arrow";

	@Override
	public String getExecutorName() {
		return "AutomationTurretAbility";
	}

	@Override
	public boolean activateAbility(Ability ability, Event event) {

		if (ability.getAbilityEntity() instanceof AbilityPlayer) {
			Player p = ((AbilityPlayer) ability.getAbilityEntity()).getPlayer();

			Location loc = p.getLocation();
			Minecart cart = p.getWorld().spawn(
					new Location(p.getWorld(), loc.getBlockX() + 0.5, loc.getBlockY() + 1.55, loc.getBlockZ() + 0.5),
					Minecart.class);

			onReset(ability);

			Turret turret = new Turret(p.getUniqueId(), cart, p.getLocation().getBlock().getState());
			p.getLocation().getBlock().setType(Material.FENCE);
			TURRETS.put(turret.minecart.getUniqueId(), turret);
			turret.init();
		}

		return true;
	}

	@Override
	public void onReset(Ability ability) {
		Iterator<Turret> iterator = TURRETS.values().iterator();
		while (iterator.hasNext()) {
			Turret turret = iterator.next();
			if (turret.owner.equals(ability.getAbilityEntity().getEntity().getUniqueId())) {
				turret.instantRemove(iterator);
				break;
			}
		}
	}

	@EventHandler
	public void onAbilityActivate(AbilityActivateEvent e) {
		if (canAbilityExecute(e.getEntity().getEntity()) && e.getEntity().getEntity().getVehicle() != null) {
			e.setSendCooldown(false);
		}
	}

	@EventHandler
	public void onTurretControl(PlayerInteractEvent e) {
		if (canAbilityExecute(e.getPlayer()) && e.getItem() != null
				&& e.getItem().getType() == getAbility(e.getPlayer()).getClickItem().getType()) {
			if (e.getPlayer().getVehicle() != null) {
				Turret turret = null;

				for (Turret cur : TURRETS.values()) {
					if (cur.owner.equals(e.getPlayer().getUniqueId())) {
						turret = cur;
						break;
					}
				}

				if (turret != null) {
					if (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK) {
						if (!turret.shootMissle(null)) {
							User.getUser(e.getPlayer()).sendOverlay(MessageKeys.OUT_OF_AMMO);
						}
					} else if (e.getAction() != Action.PHYSICAL) {
						turret.shootBullet(null);
					}
				}
			}
		}
	}

	class Turret {

		UUID owner;
		BlockState previousBlock;
		Minecart minecart;
		Item item;
		Location initialMinecartLoc;
		Hologram hologram;
		int health = 20, bullets = 120, missiles = 30, task, missiledelay = 120, autoshootdelay = 20;
		boolean update;

		boolean enableselfDestruction;
		boolean selfDestruction = false;
		int selfDestructionCount = 20 * 5;

		Turret(UUID owner, Minecart minecart, BlockState state) {
			this.owner = owner;
			this.minecart = minecart;
			this.previousBlock = state;
			this.initialMinecartLoc = minecart.getLocation();

			this.enableselfDestruction = true;// true -> enables self destruction feature
		}

		public UUID getOwner() {
			return owner;
		}

		public boolean isSelfDestruction() {
			return selfDestruction;
		}

		public boolean isEnableSelfSelfDestruction() {
			return enableselfDestruction;
		}

		public void startSelfDestruction() {
			if (enableselfDestruction) {
				new BukkitRunnable() {

					public void run() {
						if (minecart.isDead())
							this.cancel();
						if (--selfDestructionCount % 20 == 0) {
							update = true;
							if (selfDestructionCount > 0)
								initialMinecartLoc.getWorld().playSound(initialMinecartLoc, Sound.CREEPER_HISS, 1.0F,
										1.0F);
						}
						if (selfDestructionCount <= 0) {
							// initialMinecartLoc.getWorld().playSound(initialMinecartLoc, Sound.EXPLODE,
							// 1.0F, 1.0F);
							initialMinecartLoc.getWorld().createExplosion(initialMinecartLoc, 0.0f);
							initialMinecartLoc.getWorld().playEffect(initialMinecartLoc, Effect.EXPLOSION_HUGE, 0);

							Player player = Bukkit.getPlayer(owner);
							if (player == null) {
								this.cancel();
								return;
							}

							AbilityPlayer abilityPlayer = AbilityPlayer.get(player);
							Ability ability = abilityPlayer.getAbilityFromExecutor(getExecutorName());
							AbilityServer.get().getNearbyEntities(abilityPlayer, initialMinecartLoc, 7.5, 2.0)
									.forEach(near -> {
										abilityPlayer.abilityDamage(near, 250, ability, DamageCause.ENTITY_EXPLOSION);
										// Powerful kb
										near.getEntity()
												.setVelocity(near.getEntity().getVelocity()
														.add(near.getEntity().getLocation().toVector()
																.subtract(initialMinecartLoc.toVector()).normalize()
																.setY(0.6f).multiply(1.5D)));
									});

							this.cancel();
							return;
						}
					}

					public void cancel() {
						super.cancel();
						selfDestruction = false;
					}
				}.runTaskTimer(PvPDojo.get(), 0, 1);
			}
		}

		public void setHealth(int health) {
			this.health = health;
			this.update = true;
		}

		public int getHealth() {
			return health;
		}

		public Minecart getMinecart() {
			return minecart;
		}

		public int getBullets() {
			return bullets;
		}

		public void setBullets(int bullets) {
			this.bullets = bullets;
			this.update = true;
		}

		public int getMissiles() {
			return missiles;
		}

		public void setMissiles(int missiles) {
			this.missiles = missiles;
			this.update = true;
		}

		public Item getItem() {
			return item;
		}

		public Location getInitialMinecartLoc() {
			return initialMinecartLoc;
		}

		public void remove(Iterator<Turret> itr) {
			if (enableselfDestruction) {
				if (selfDestruction) {
					PvPDojo.schedule(() -> instantRemove(itr)).createTask(selfDestructionCount);
				}
			} else {
				instantRemove(itr);
			}
		}

		public void instantRemove(Iterator<Turret> itr) {
			Bukkit.getScheduler().cancelTask(task);
			previousBlock.update(true, false);
			hologram.destroy();
			minecart.remove();
			if (item != null)
				item.remove();
			if (itr == null) {
				TURRETS.remove(minecart.getUniqueId());
			} else {
				itr.remove();
			}
		}

		public void init() {

			Hologram h = new Hologram(Arrays.asList(CC.RED + "Health: " + health, CC.BLUE + "Bullets: " + bullets,
					CC.DARK_AQUA + "Missiles: " + missiles));
			h.show(minecart.getLocation().add(0, 2.2, 0));
			h.track();
			hologram = h;

			task = PvPDojo.schedule(() -> {
				if (Bukkit.getPlayer(owner) == null) {
					remove(null);
					selfDestruction = false;
					return;
				}

				if (update) {
					List<String> lines = Arrays.asList(CC.RED + "Health: " + health, CC.BLUE + "Bullets: " + bullets,
							CC.DARK_AQUA + "Missiles: " + missiles,
							selfDestruction && enableselfDestruction
									? CC.DARK_RED + "Self Destruction in " + selfDestructionCount / 20 + "s"
									: " ");
					h.change(lines);
					update = false;
				}
				if (health <= 0 || bullets <= 0) {
					remove(null);
					if (!selfDestruction && enableselfDestruction) {
						selfDestruction = true;
						startSelfDestruction();
					}

				}
				Location loc = initialMinecartLoc;
				loc.setPitch(-35);

				if (item != null && !item.isEmpty()) {
					Entity passenger = item.getPassenger();
					item.setFallDistance(0);
					loc.setYaw(Bukkit.getPlayer(owner).getLocation().getYaw() + 90);
					item.getPassenger().eject();
					item.setPassenger(passenger);
					item.setVelocity(new Vector());
				} else {
					if (missiledelay <= 0) {
						missiledelay = 60;
						Player target = getTarget();
						if (target != null) {
							shootMissle(target.getEyeLocation().subtract(getArrowStartLocation()).toVector().normalize()
									.multiply(1.7));
						}
					}
					if (autoshootdelay <= 0) {
						autoshootdelay = 8;
						Player target = getTarget();
						if (target != null) {
							shootBullet(target.getEyeLocation().subtract(getArrowStartLocation().add(0, 0.4, 0))
									.toVector().normalize().multiply(1.8));
						}
					}

					minecart.teleport(loc);
					autoshootdelay--;
					missiledelay--;
				}

			}).createTimer(1, -1);

		}

		public boolean shootMissle(Vector where) {
			if (getMissiles() <= 0) {
				return false;
			}
			if (isSelfDestruction())
				return false;
			Arrow arrow = shoot(where);
			setMissiles(getMissiles() - 1);
			arrow.setDisguise(new CraftItemDisguise(arrow, EntityType.DROPPED_ITEM, new ItemStack(Material.ANVIL)));
			arrow.setMetadata(MISSLE, new FixedMetadataValue(PvPDojo.get(), true));
			arrow.getWorld().playSound(arrow.getLocation(), Sound.CAT_HISS, 1f, 1f);
			return true;
		}

		public void shootBullet(Vector where) {
			if (isSelfDestruction())
				return;
			Arrow arrow = shoot(where);
			setBullets(getBullets() - 1);
			arrow.setDisguise(
					new CraftItemDisguise(arrow, EntityType.DROPPED_ITEM, new ItemStack(Material.ENDER_PEARL)));
			arrow.getWorld().playSound(arrow.getLocation(), Sound.WOLF_SHAKE, 1f, 1f);
		}

		public Arrow shoot(Vector where) {
			Arrow arrow = Bukkit.getPlayer(owner).launchProjectile(Arrow.class);
			arrow.setMetadata(TURRET_ARROW, new FixedMetadataValue(PvPDojo.get(), null));
			if (where != null) {
				Location start = getArrowStartLocation();
				arrow.teleport(start);
				arrow.setVelocity(where);
			}
			return arrow;
		}

		public Location getArrowStartLocation() {
			return initialMinecartLoc.clone().add(0, 1.2, 0);
		}

		Player getTarget() {
			if (isSelfDestruction())
				return null;
			double distance = Double.POSITIVE_INFINITY;

			Player target = null;
			for (Entity e : minecart.getNearbyEntities(20, 20, 20)) {

				if (!(e instanceof Player) || !AbilityPlayer.get((Player) e).isTargetable()
						|| !AbilityPlayer.get(Bukkit.getPlayer(owner)).canTarget(AbilityPlayer.get((Player) e))) {
					continue;
				}

				double distanceto = minecart.getLocation().distance(e.getLocation());

				if (distance < distanceto) {
					continue;
				}

				distance = distanceto;
				target = (Player) e;
			}
			return target;
		}
	}

	@EventHandler
	public void onTurretMove(VehicleMoveEvent e) {
		Turret turret = TURRETS.get(e.getVehicle().getUniqueId());
		if (turret != null) {
			Location to = e.getTo();
			Location from = e.getFrom();
			if (from.getZ() != to.getZ() || from.getX() != to.getX() || from.getY() != to.getY()) {
				e.getVehicle().teleport(turret.getInitialMinecartLoc());
				e.getVehicle().setFallDistance(0);
			}
		}
	}

	@EventHandler
	public void onTurretEnter(VehicleEnterEvent e) {
		Turret turret = TURRETS.get(e.getVehicle().getUniqueId());

		if (turret != null) {
			e.setCancelled(true);
			if (e.getEntered().getUniqueId().equals(turret.getOwner())) {

				if (turret.isSelfDestruction())
					e.setCancelled(true);

				if (turret.getItem() != null) {
					turret.getItem().remove();
				}

				turret.item = turret.minecart.getWorld().dropItem(turret.initialMinecartLoc.clone().add(0, 0.2, 0),
						new ItemStack(Material.STONE_BUTTON));
				turret.item.setPickupDelay(Integer.MAX_VALUE);
				turret.item.setCustomDespawnRate(Integer.MAX_VALUE);
				turret.getItem().setPassenger(e.getEntered());
			}
		}
	}

	@EventHandler
	public void onTurretDamage(VehicleDamageEvent e) {

		if (!(e.getVehicle() instanceof Minecart))
			return;

		Minecart cart = (Minecart) e.getVehicle();
		Turret turret = TURRETS.get(cart.getUniqueId());

		if (turret != null) {
			e.setCancelled(true);
			if (!turret.isSelfDestruction()) {
				if (turret.getHealth() > 0) {
					turret.setHealth(turret.getHealth() - 1);
					turret.getInitialMinecartLoc().getWorld().playSound(turret.getInitialMinecartLoc(),
							Sound.ITEM_BREAK, 1.0F, 1.0F);
				}
			}
		}
	}

	@EventHandler
	public void onMissileHit(ProjectileHitEvent e) {
		if (e.getEntity() instanceof Arrow && e.getEntity().hasMetadata(TURRET_ARROW)) {
			e.getEntity().remove();
			if (e.getEntity().hasMetadata(MISSLE)) {
				Location loc = e.getEntity().getLocation();
				e.getEntity().getWorld().createExplosion(loc.getX(), loc.getY(), loc.getZ(), 2F, false, false);
			}
		}
	}

}
