/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.gamemode;

import com.pvpdojo.abilities.base.entities.AbilityEntity;

public class ManaSilenceEffect extends AbilityEffect {

    public ManaSilenceEffect(int duration) {
        super(AbilityEffectType.MANA_SILENCE, duration);
    }

    @Override
    public void applyEffect(AbilityEntity entity) {
    }

    @Override
    public void endEffect(AbilityEntity entity) {
    }
}
