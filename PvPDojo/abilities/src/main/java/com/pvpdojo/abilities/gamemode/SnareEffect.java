/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.gamemode;

import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.userdata.User;

public class SnareEffect extends AbilityEffect {

    public SnareEffect(int duration) {
        super(AbilityEffectType.SNARE, duration);
    }

    @Override
    public void applyEffect(AbilityEntity entity) {
        if (entity instanceof AbilityPlayer) {
            AbilityPlayer player = (AbilityPlayer) entity;
            User.getUser(player.getPlayer()).setCancelMove(getTickDuration() / 20);
        }
    }

    @Override
    public void endEffect(AbilityEntity entity) {
        if (entity instanceof AbilityPlayer) {
            AbilityPlayer player = (AbilityPlayer) entity;
            User.getUser(player.getPlayer()).resumeMove();
        }
    }

}
