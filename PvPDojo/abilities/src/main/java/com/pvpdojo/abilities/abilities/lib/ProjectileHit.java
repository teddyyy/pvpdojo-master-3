

package com.pvpdojo.abilities.abilities.lib;

import org.bukkit.Location;

public interface ProjectileHit {

    void onHit(Location loc);
}
