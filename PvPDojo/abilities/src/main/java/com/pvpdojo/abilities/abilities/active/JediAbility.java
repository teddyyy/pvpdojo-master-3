/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.abilityinfo.AbilityType;

public class JediAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "JediAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        int range = (int) ability.getAttribute("range");

        if (ability.getAbilityEntity() instanceof AbilityPlayer) {

            Player player = ((AbilityPlayer) ability.getAbilityEntity()).getPlayer();
            player.sendMessage(PvPDojo.PREFIX + "An Anti-BlackHole has opened at your feet!");

            new AbilityRunnable(ability) {
                int x = 0;

                @Override
                public void run() {
                    x++;
                    if (ability.getAbilityType() == AbilityType.CLICK && player.getItemInHand().getType() != ability.getClickItem().getType()) {
                        player.sendMessage(PvPDojo.PREFIX + "The Anti-BlackHole became unstable and closed due to item switch");
                        cancel();
                        return;
                    }
                    if (x > 36) {
                        player.sendMessage(PvPDojo.PREFIX + "The Anti-BlackHole became unstable and closed");
                        cancel();
                        return;
                    }
                    pushNearby(player, range);
                }


            }.createTimer(2, 2, -1);
        }
        return true;

    }

    public void pushNearby(Player player, int range) {
        List<AbilityEntity> nearby = AbilityServer.get().getNearbyEntities(AbilityPlayer.get(player), range);
        for (AbilityEntity abilityEntity : nearby) {
            Entity e = abilityEntity.getEntity();
            Location ploc = player.getLocation();
            Location hloc = e.getLocation();
            double distance = ploc.distance(hloc);
            Vector vector = hloc.toVector().subtract(ploc.toVector());
            if (distance < range) {
                if (e.getType().equals(EntityType.DROPPED_ITEM)) {
                    e.setVelocity(vector.multiply(.4).setY(.01));
                } else {
                    e.setVelocity(vector.multiply(getRelativeSpeed(distance)).setY(.01));
                }
            }
        }
    }

    public double getRelativeSpeed(double distance) {
        return .3 / distance;
    }
}
