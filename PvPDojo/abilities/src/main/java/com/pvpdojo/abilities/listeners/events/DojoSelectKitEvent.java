/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.listeners.events;

import java.util.ArrayList;

import org.bukkit.inventory.ItemStack;

import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.userdata.PlayStyle;

public class DojoSelectKitEvent extends PlayerAbilityEvent {
    public AbilityEntity champion;
    public PlayStyle playStyle;
    public ArrayList<ItemStack> addItems = new ArrayList<>();

    public DojoSelectKitEvent(AbilityPlayer player, PlayStyle playStyle) {
        super(player);
        this.playStyle = playStyle;
    }

    public PlayStyle getPlayStyle() {
        return playStyle;
    }

    public ArrayList<ItemStack> getAddItems() {
        return addItems;
    }

}
