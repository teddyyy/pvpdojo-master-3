/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.passive;

import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.ProjectileEntity;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.listeners.events.DojoSelectKitEvent;
import com.pvpdojo.util.bukkit.CC;

public class ArcherAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "ArcherAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        return true;
    }

    @EventHandler
    public void onSelect(DojoSelectKitEvent e) {
        AbilityEntity activator = e.getEntity();
        if (activator.getAbilityFromExecutor(getExecutorName()) == null) {
            return;
        }
        ItemStack bow = new ItemStack(Material.BOW);
        e.getAddItems().add(bow);
        Ability ability = getAbility(activator.getEntity());
        final float amount = ability.getAttribute("amount");
        ItemStack arrows = new ItemStack(Material.ARROW);
        arrows.setAmount((int) amount);
        e.getAddItems().add(arrows);
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onDeath(PlayerDeathEvent e) {
        if (canAbilityExecute(e.getEntity())) {
            e.getDrops().removeIf(item -> item.getType() == Material.BOW || item.getType() == Material.ARROW);
        }
    }

    @EventHandler
    public void onBowShoot(EntityShootBowEvent e) {
        LivingEntity living = e.getEntity();
        if (getAbility(living) != null && !canAbilityExecute(living)) {
            e.setCancelled(true);
            if (living instanceof Player) {
                ((Player) living).sendMessage(CC.RED + "Cannot use abilities while silenced");
            }
            return;
        }
        if (!canAbilityExecute(living)) {
            return;
        }
        Ability ability = getAbility(living);
        ProjectileEntity projectile = new ProjectileEntity(ability.getAbilityEntity());
        final float force = e.getForce();
        final float damage = ability.getAttribute("ability_damage") * force;
        final float speed = ability.getAttribute("speed");
        projectile.setDamage(damage);

        e.getProjectile().setVelocity(living.getLocation().getDirection().multiply(1 + force * speed));
        projectile.startProjectileCollision(e.getProjectile());
        if (living instanceof Player) {
            projectile.setProjectileDetection(target -> ((Player) living).getInventory().addItem(new ItemStack(Material.ARROW)));
        }
    }
}
