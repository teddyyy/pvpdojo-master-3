package com.pvpdojo.abilities.abilities.active;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerVelocityEvent;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.KitData;
import com.pvpdojo.userdata.PlayStyle;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class GolemAbility extends AbilityExecutor {

	@Override
	public String getExecutorName() {
		return "GolemAbility";
	}

	private static final String GOLEM_ACTIVATION = "GOLEM_ACTIVATION";
	private static final Set<UUID> CANCEL_NEXT_VELOCITY = new HashSet<>();

	@Override
	public boolean activateAbility(Ability ability, Event event) {
		AbilityEntity activator = ability.getAbilityEntity();
		LivingEntity activatorEntity = activator.getEntity();

		if (activatorEntity instanceof Player) {
			User user = User.getUser((Player) activatorEntity);
			AbilityPlayer champion = AbilityPlayer.get(user.getPlayer());
			KitData data = champion.getKit();
			if (!data.getPlayStyle().equals(PlayStyle.SOUP)) {
				user.sendMessage(MessageKeys.GOLEM_NOTSOUPPVP);
				return false;
			}

			if (!ability.getMetaData().hasData(GOLEM_ACTIVATION)) {
				ability.getMetaData().addData(GOLEM_ACTIVATION, true);
				activatorEntity.getWorld().playSound(activatorEntity.getLocation(), Sound.IRONGOLEM_HIT, 1, 1);
				user.sendMessage(MessageKeys.GOLEM_ACTIVATE);
			} else {
				ability.getMetaData().removeData(GOLEM_ACTIVATION);
				activatorEntity.getWorld().playSound(activatorEntity.getLocation(), Sound.IRONGOLEM_DEATH, 1, 1);
				user.sendMessage(MessageKeys.GOLEM_DEACTIVATE);
			}
		}

		return true;
	}

	@EventHandler
	public void onEntityAttack(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player && canAbilityExecute((LivingEntity) e.getEntity())
				&& getAbility((LivingEntity) e.getEntity()).getMetaData().hasData(GOLEM_ACTIVATION))
			anchor(e);
	}

	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if (e instanceof EntityDamageByEntityEvent) {
			if (((EntityDamageByEntityEvent) e).getDamager() instanceof Player
					&& canAbilityExecute((LivingEntity) ((EntityDamageByEntityEvent) e).getDamager())
					&& getAbility((LivingEntity) ((EntityDamageByEntityEvent) e).getDamager()).getMetaData()
							.hasData(GOLEM_ACTIVATION))
				anchor((EntityDamageByEntityEvent) e);
		}
	}

	@EventHandler
	public void onVelocity(PlayerVelocityEvent e) {
		if (CANCEL_NEXT_VELOCITY.remove(e.getPlayer().getUniqueId())) {
			e.setVelocity(new Vector());
		}
	}

	private void anchor(EntityDamageByEntityEvent e) {
		if ((e.getEntity() instanceof LivingEntity && !PlayerUtil.isRealHit((LivingEntity) e.getEntity()))
				|| e.isCancelled()) {
			return;
		}

		if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {

			AbilityPlayer championEntity = AbilityPlayer.get((Player) e.getEntity());
			KitData dataEntity = championEntity.getKit();

			AbilityPlayer championTarget = AbilityPlayer.get((Player) e.getDamager());
			KitData dataTarget = championTarget.getKit();

			if (!dataEntity.getPlayStyle().equals(PlayStyle.SOUP))
				return;

			if (!dataTarget.getPlayStyle().equals(PlayStyle.SOUP))
				return;

		}

		if (e.getEntity() instanceof Player) {
			CANCEL_NEXT_VELOCITY.add(e.getEntity().getUniqueId());
		} else {
			PvPDojo.schedule(() -> e.getEntity().setVelocity(new Vector())).nextTick();
		}

		if (e.getEntity() instanceof Player) {
			((Player) e.getEntity()).playSound(e.getEntity().getLocation(), Sound.ANVIL_LAND, 0.1F, 1F);
		}
		if (e.getDamager() instanceof Player) {
			((Player) e.getDamager()).playSound(e.getEntity().getLocation(), Sound.ANVIL_LAND, 0.1F, 1F);
		}
	}
}
