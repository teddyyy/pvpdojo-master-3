/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.guns.Gun;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.listeners.events.DojoSelectKitEvent;

public class AssaultRifleAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "AssaultRifleAbility";
    }

    @Override
    public boolean activateAbility(final Ability ability, Event event) {
        final AbilityEntity activator = ability.getAbilityEntity();
        activator.getGunController().clickGunAbility(this.getExecutorName());
        return true;
    }

    @SuppressWarnings("deprecation")
    @EventHandler
    public void onSelect(DojoSelectKitEvent e) {
        AbilityEntity activator = e.getEntity();
        LivingEntity living = activator.getEntity();
        if (activator.getAbilityFromExecutor(getExecutorName()) == null)
            return;
        Ability ability = getAbility(living);
        Gun gun = new Gun(getExecutorName(), ability);
        gun.setName(getExecutorName());
        gun.setAmmoType("" + Material.SNOW_BALL.getId());
        gun.setAmmoAmountNeeded(1);
        gun.bulletDelayTime = 2;
        gun.reloadType = "NORMAL";
        gun.setRoundsPerBurst(1);
        gun.setBulletsPerClick(1);
        gun.setGunDamage(ability.getAttribute("bullet_damage"));
        gun.setMaxDistance(40);
        gun.setBulletSpeed(4);
        gun.setAccuracy(.065);
        gun.gun_recoil = .05;
        gun.setAccuracyAimed(.05);
        gun.setAccuracyCrouched(.03);
        gun.setRecoil(.3);
        gun.setKnockback(1);
        gun.setCanAimLeft(true);
        gun.setCanAimRight(false);
        gun.setCanHeadshot(false);
        gun.setCanClickLeft(false);
        gun.setCanClickRight(true);
        gun.setExplodeRadius(0);
        gun.gunSound.add("skeleton_hurt");
        gun.gunSound.add("zombie_wood");
        gun.gunSound.add("ghast_fireball");
        gun.setSmokeTrail(false);
        gun.setGunVolume(1.0);
        gun.hasClip = true;
        gun.setLocalGunSound(false);
        gun.maxClipSize = (int) ability.getAttribute("clip_size");
        gun.ammoLeft = (int) ability.getAttribute("total_clips") * gun.maxClipSize;
        gun.ammoReset = gun.ammoLeft;
        gun.setReloadTime((int) (ability.getAttribute("reload_time") * 20));

        activator.selectGunAbility(gun, ability);

    }
}
