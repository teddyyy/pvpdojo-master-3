package com.pvpdojo.abilities.abilities.active;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.base.entities.AbilityPlayer;
import com.pvpdojo.abilities.gamemode.AbilityServer;

public class FamineAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "FamineAbility";
    }

    @Override // radius, duration
    public boolean activateAbility(Ability ability, Event event) {
        AbilityEntity activator = ability.getAbilityEntity();
        final float radius = ability.getAttribute("radius");
        final float duration = ability.getAttribute("duration");

        int targets = 0;
        for (AbilityEntity near : AbilityServer.get().getNearbyEntities(activator, radius, radius)) {

            if (near instanceof AbilityPlayer) {
                Player playerNear = (Player) near.getEntity();
                targets++;
                int oldHunger = playerNear.getFoodLevel();
                playerNear.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, (int) duration * 20, 0));

                new AbilityRunnable(ability) {

                    float counter = 20 * duration;

                    public void run() {
                        if (counter <= 0) {
                            cancel();
                        }
                        counter--;
                        playerNear.setFoodLevel(playerNear.getFoodLevel() - 1);

                    }

                    @Override
                    public void cancel() {
                        super.cancel();
                        playerNear.setFoodLevel(oldHunger + 1);

                    }

                }.createTimer(1, -1);

            }
        }
        if (targets > 0) {
            if (activator.getEntity() instanceof Player) {
                Player player = (Player) activator.getEntity();
                player.sendMessage(PvPDojo.PREFIX + ChatColor.RED + "You have spread starvation and despair around you");
            }
            return true;
        }
        return false;
    }

}
