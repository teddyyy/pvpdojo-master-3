package com.pvpdojo.abilities.abilities.active;

import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.effects.ParticleEffectRotation;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.util.bukkit.ParticleEffects;

public class LoveGodAbility extends AbilityExecutor {

	
	/**
	 * 
	 * CHANGED TO OCTOPUS ABILITY IN THE OLD SCHOOL COLLECTION.
	 * 
	 * 
	 * 
	 * 
	 **/
	
	
	@Override
	public String getExecutorName() {
		return "LoveGodAbility";
	}

	@Override
	public boolean activateAbility(Ability ability, Event event) {

		float damage_duration = ability.getAttribute("heart_duration");
		float damage = ability.getAttribute("damage");

		AbilityEntity activator = ability.getAbilityEntity();
		LivingEntity entity = activator.getEntity();

		Location location = entity.getLocation();
		location.setY(location.getY() + 1.5D);
		Location dir = entity.getEyeLocation();

		new AbilityRunnable(ability) {

			double t = 0.0D;
			int targets = 0;
			public int maxTargets = 2;

			@Override
			public void run() {
				t += 1.5D;

				double r = 0.4D;
				for (double i = 0.0D; i <= Math.PI * 2; i += Math.PI / 16) {
					double x = r * 3.9D * Math.pow(Math.sin(i), 3.0D);
					double y = r * (3.0D * Math.cos(i) - 1.2D * Math.cos(2.0D * i) - 0.6D * Math.cos(3.0D * i)
							- 0.2D * Math.cos(4.0D * i));
					double z = t;
					Vector v = new Vector(x, y, z);
					v = ParticleEffectRotation.rotateFunction(v, dir);
					location.add(v.getX(), v.getY(), v.getZ());
					if (targets <= maxTargets) {
						List<AbilityEntity> nearby = AbilityServer.get().getNearbyEntities(location, 3.5, 0);

						for (AbilityEntity target : nearby) {
							if (target.equals(ability.getAbilityEntity()))
								continue;
							targets++;
							new BukkitRunnable() {

								int timer = 0;

								public void run() {
									if (++timer >= (damage_duration * 20) / 5 || entity.isDead() || target.isDead()) {
										this.cancel();
									}
									ability.getAbilityEntity().abilityDamage(target, damage, ability,
											DamageCause.ENTITY_ATTACK);
									ability.getAbilityEntity().heal(damage);
									charmEffectAt(ability, target.getEntity(), true);

								}
							}.runTaskTimer(PvPDojo.get(), 0, 5);

						}
						ParticleEffects.HEART.sendToLocation(location, 0, 0, 0, 0, 1);
						location.subtract(v.getX(), v.getY(), v.getZ());
					}

					if (this.t > 50 || entity.isDead()) {
						cancel();
					}
				}
			}
		}.createTimer(2, -1);

		return true;
	}

	public void charmEffectAt(Ability ability, LivingEntity to, boolean charmMove) {
		int radius = 2;
		Location[] locs = new Location[] { ability.getAbilityEntity().getEntity().getLocation(), to.getLocation() };
		for (Location locations : locs) {
			for (int x = -radius; x <= radius; x++) {
				for (int y = -radius; y <= radius; y++) {
					for (int z = -radius; z <= radius; z++) {
						Location loc = locations.clone().add(x, y, z);
						if (loc.distance(locations) <= radius && new Random().nextInt(20) == 1) {
							ParticleEffects.HEART.sendToLocation(loc, 0, 0, 0, 0, 1);
						}
					}
				}
			}
		}
		to.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20 * 2, 0));
		if (charmMove) {
			new AbilityRunnable(ability) {

				int x = 20;

				public void run() {
					if (--x <= 0 || ability.getAbilityEntity().getEntity().isDead() || to.isDead())
						this.cancel();
					to.setVelocity(to.getVelocity()
							.add(to.getLocation().toVector()
									.subtract(ability.getAbilityEntity().getEntity().getLocation().toVector())
									.normalize().multiply(-0.0009)));// :>
				}
			}.createTimer(1, -1);
		}
	}

}
