package com.pvpdojo.abilities.abilities.active;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.effects.AvatarEffect;
import com.pvpdojo.abilities.abilities.lib.AbilityRunnable;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerPreDeathEvent;

public class AvatarAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "AvatarAbility";
    }

    public static final String USE = "Used_Avatar";

    public static final String WATER = "Avatar_Water";
    public static final String EARTH = "Avatar_Earth";
    public static final String FIRE = "Avatar_Fire";
    public static final String AIR = "Avatar_Air";

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        AbilityEntity activator = ability.getAbilityEntity();
        AvatarEffect effect = new AvatarEffect(ability);

        if (!ability.getMetaData().hasData(USE)) {
            effect.activationEffect();
            new AbilityRunnable(ability) {

                int loop = 0;

                @Override
                public void run() {
                    if (++loop >= effect.DELAY) {
                        effect.displayAll();
                        this.cancel();
                    }
                }
            }.createTimer(0, -1);

        } else {
            if (hasMeta(activator, WATER)) {
                effect.useWaterBender();
            } else if (hasMeta(activator, EARTH)) {
                effect.useEarthBender();
            } else if (hasMeta(activator, FIRE)) {
                effect.useFireBender();
            } else if (hasMeta(activator, AIR)) {
                effect.useAirBender();
            }
        }
        return false;
    }

    private boolean hasMeta(AbilityEntity activator, String data) {
        return activator.getEntity().hasMetadata(data);
    }

    //Clean.
    @EventHandler
    public void onDeath(PlayerPreDeathEvent e) {
        Player player = e.getPlayer();
        if (canAbilityExecute(player)) {
            player.removeMetadata(AIR, PvPDojo.get());
            player.removeMetadata(EARTH, PvPDojo.get());
            player.removeMetadata(FIRE, PvPDojo.get());
            player.removeMetadata(WATER, PvPDojo.get());
            player.setFlying(false);
            player.setAllowFlight(false);
        }
    }

}
