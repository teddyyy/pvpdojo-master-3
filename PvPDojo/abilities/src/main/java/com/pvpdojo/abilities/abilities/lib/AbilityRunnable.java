/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.lib;

import java.util.ArrayList;

import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.util.DojoRunnable;

public abstract class AbilityRunnable extends DojoRunnable {

    private Ability ability;

    public AbilityRunnable(Ability ability) {
        super(null);
        this.runnable = this::run;
        ability.getMetaData().getAbilityRunnables().add(this);
        this.ability = ability;
    }

    public abstract void run();

    @Override
    public void cancel() {
        ArrayList<AbilityRunnable> runnables = ability.getMetaData().getAbilityRunnables();
        for (int i = runnables.size() - 1; i >= 0; i--) {
            AbilityRunnable run = runnables.get(i);
            if (run.equals(this))
                runnables.remove(this);
        }
        super.cancel();
    }
}
