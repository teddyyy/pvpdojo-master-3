/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.entity.Snowball;
import org.bukkit.event.Event;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.abilities.lib.ProjectileEntity;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.BurnEffect;

public class WizardFireAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "WizardFireAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {
        final AbilityEntity activator = ability.getAbilityEntity();
        final int duration = (int) ability.getAttribute("duration");
        Snowball snowball = activator.getEntity().launchProjectile(Snowball.class);
        snowball.setVelocity(snowball.getVelocity().multiply(1.2));
        ProjectileEntity projectileEntity = new ProjectileEntity(activator);
        projectileEntity.startProjectileCollision(snowball);
        projectileEntity.setProjectileDetection(target -> target.applyEffect(new BurnEffect(duration * 20)));

        return true;
    }

}
