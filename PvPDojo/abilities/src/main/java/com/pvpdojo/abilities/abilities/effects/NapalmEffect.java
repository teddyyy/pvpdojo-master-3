/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.effects;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilities.gamemode.AbilityServer;
import com.pvpdojo.bukkit.util.TempBlock;
import com.pvpdojo.util.bukkit.BlockUtil;

public class NapalmEffect {

    private Location center;
    private AbilityEntity activator;
    private Ability ability;
    private float damage;
    private int maxRadius;
    private int ticksLeft;
    private int spreadRadius;
    private boolean follow;
    private int radius;
    private Set<AbilityEntity> burning = new HashSet<>();

    public NapalmEffect(Ability ability, Location location, float damage, int maxRadius, int ticks, int spreadRadius, boolean follow) {
        this.ability = ability;
        this.activator = ability.getAbilityEntity();
        this.center = BlockUtil.getFirstBlockDown(location, null).add(0, 1, 0);
        this.damage = damage;
        this.maxRadius = maxRadius;
        this.ticksLeft = ticks / 5;
        this.spreadRadius = spreadRadius;
        this.follow = follow;
    }

    public void start() {
        PvPDojo.schedule(() -> {
            ticksLeft--;
            if (radius <= maxRadius) {
                igniteBlocks(center, radius++);
                burnNearbyEntities(radius);
            } else {
                burnNearbyEntities(maxRadius);
            }
        }).createTimer(5, ticksLeft);
    }

    public void burnNearbyEntities(int radius) {
        for (AbilityEntity entity : AbilityServer.get().getNearbyEntities(center, radius, 2)) {
            if (follow) {
                if (!burning.contains(entity)) {
                    if (entity.equals(activator)) {
                        continue;
                    }
                    if (!activator.canTarget(entity)) {
                        continue;
                    }
                    burning.add(entity);
                }
            } else {
                activator.abilityDamage(entity, damage, ability, DamageCause.FIRE);
            }
        }
        if (follow) {
            Iterator<AbilityEntity> iterator = burning.iterator();
            while (iterator.hasNext()) {
                AbilityEntity entity = iterator.next();
                if (entity.isDead() || !entity.isTargetable()) {
                    iterator.remove();
                    continue;
                }
                igniteBlocks(entity.getEntity().getLocation(), spreadRadius);
                activator.abilityDamage(entity, damage, ability, DamageCause.FIRE);
            }

            for (AbilityEntity entity : new HashSet<>(burning)) {
                for (AbilityEntity nearby : AbilityServer.get().getNearbyEntities(entity.getEntity().getLocation(), spreadRadius, 2)) {
                    if (nearby.equals(activator)) {
                        continue;
                    }
                    burning.add(nearby);
                }
            }
        }
    }

    private void igniteBlocks(Location center, int radius) {
        for (int x = -radius; x < radius; x++) {
            for (int z = -radius; z < radius; z++) {
                Location loc = center.clone().add(x, 0, z);
                Material below = loc.getBlock().getRelative(BlockFace.DOWN).getType();
                if (loc.getBlock().getType().equals(Material.FIRE) || !below.isSolid())
                    continue;
                TempBlock.createTempBlock(loc, Material.FIRE, (byte) 0, ticksLeft / 4);
            }
        }
    }

}
