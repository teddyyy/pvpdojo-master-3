/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.base.abilities;

import com.pvpdojo.abilities.base.entities.AbilityEntity;
import com.pvpdojo.abilityinfo.KitAttributeArray;
import com.pvpdojo.userdata.KitAbilityData;

public class ActiveAbility extends Ability {

    public ActiveAbility(AbilityEntity champion, String ability, String ability_executor, KitAttributeArray base_attributes, KitAbilityData data) {
        super(champion, ability, ability_executor, base_attributes, data);
    }

}
