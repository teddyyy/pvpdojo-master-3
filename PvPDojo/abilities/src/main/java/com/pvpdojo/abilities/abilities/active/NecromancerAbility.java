/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilities.abilities.active;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Silverfish;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Skeleton.SkeletonType;
import org.bukkit.event.Event;

import com.pvpdojo.abilities.abilities.AbilityExecutor;
import com.pvpdojo.abilities.base.abilities.Ability;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.util.bukkit.BlockUtil;

public class NecromancerAbility extends AbilityExecutor {

    @Override
    public String getExecutorName() {
        return "NecromancerAbility";
    }

    @Override
    public boolean activateAbility(Ability ability, Event event) {

        LivingEntity entity = ability.getAbilityEntity().getEntity();

        Block block = BlockUtil.getTargetBlock(6, entity);
        if (block == null) {
            return false;
        }
        Location loc = block.getLocation();


        Skeleton wSkel = (Skeleton) entity.getWorld().spawnEntity(loc, EntityType.SKELETON);
        wSkel.setSkeletonType(SkeletonType.WITHER);
        ability.getMetaData().addEntity(wSkel, 10, true);
        NMSUtils.get(wSkel).setFireProof(true);

        Skeleton skel = (Skeleton) entity.getWorld().spawnEntity(loc.add(2, 0, -2), EntityType.SKELETON);
        ability.getMetaData().addEntity(skel, 10, true);
        Skeleton skelTwo = (Skeleton) entity.getWorld().spawnEntity(loc.add(-2, 0, 2), EntityType.SKELETON);
        ability.getMetaData().addEntity(skelTwo, 10, true);
        NMSUtils.get(skel).setFireProof(true);
        NMSUtils.get(skelTwo).setFireProof(true);

        float rotation = 0;
        int dividen = 360 / 8;

        for (int i = 0; i < 8; i++) {
            rotation = rotation + dividen;

            Location loc1 = new Location(entity.getWorld(), loc.getX(), loc.getY(), loc.getZ(), rotation, 0);

            Silverfish ent = (Silverfish) entity.getWorld().spawnEntity(loc1, EntityType.SILVERFISH);
            ability.getMetaData().addEntity(ent, 10, true);
            NMSUtils.get(ent).setFireProof(true);

            entity.getWorld().playEffect(ent.getLocation(), Effect.POTION_SWIRL, 2);
        }
        return true;
    }
}
