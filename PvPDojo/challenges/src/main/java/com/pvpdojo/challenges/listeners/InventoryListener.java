/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.challenges.menu.ChallengesHotbarGUI;
import com.pvpdojo.challenges.userdata.ChallengesUser;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class InventoryListener implements Listener {

    public static void register() {
        Bukkit.getPluginManager().registerEvents(new InventoryListener(), PvPDojo.get());
    }

    @EventHandler
    public void onInventoryOpen(InventoryOpenEvent e) {

        ChallengesUser user = ChallengesUser.getUser((Player) e.getPlayer());
        if (e.getInventory().getType() == InventoryType.CRAFTING && user.getApplicableRegions().stream().anyMatch(rg -> rg.getId().startsWith("spawn")) && !user.isAdminMode()) {
            if (user.isRecraftTraining()) {
                PlayerUtil.refillRecraft((Player) e.getPlayer(), 64);
            } else {
                PlayerUtil.refillSoup((Player) e.getPlayer(), 35);
            }
            for (int i = 1; i < 9; i++) {
                e.getPlayer().getInventory().setItem(i, new ItemStack(Material.AIR));
            }
            e.getPlayer().getInventory().setItem(0, new ItemStack(Material.STONE_SWORD));
        }

    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent e) {
        ChallengesUser user = ChallengesUser.getUser((Player) e.getPlayer());
        if (e.getInventory().getType() == InventoryType.CRAFTING && user.getApplicableRegions().stream().anyMatch(rg -> rg.getId().startsWith("spawn")) && !user.isAdminMode()) {
            user.setHotbarGui(ChallengesHotbarGUI.get());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onSoupDrop(PlayerDropItemEvent e) {
        if (e.getItemDrop().getItemStack().getType() == Material.MUSHROOM_SOUP) {
            ChallengesUser.getUser(e.getPlayer()).getCurrentChallengeStats().dropSoup();
        }
    }

}
