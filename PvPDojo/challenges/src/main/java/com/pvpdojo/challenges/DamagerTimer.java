/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.challenges.userdata.ChallengesUser;
import com.pvpdojo.util.DojoRunnable;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class DamagerTimer extends DojoRunnable {

    private static final Material[] randomMaterials = new Material[]{Material.SEEDS, Material.WOOD_PICKAXE, Material.STONE_SWORD, Material.YELLOW_FLOWER, Material.COBBLESTONE,
            Material.BOWL, Material.WOOD, Material.MUSHROOM_SOUP, Material.FEATHER, Material.SEEDS, Material.WOOD, Material.YELLOW_FLOWER, Material.YELLOW_FLOWER, Material.WOOD,
            Material.BOWL, Material.BOWL, Material.COAL, Material.STICK, Material.LOG, Material.IRON_ORE};

    private int current;
    private int nextCrap;

    public DamagerTimer() {
        super(null);
        this.runnable = () -> {
            PlayerUtil.spreadPlayerTask(Bukkit.getOnlinePlayers(), p -> {
                ChallengesUser user = ChallengesUser.getUser(p);

                if (user.getDamagerCooldown() > System.currentTimeMillis()) {
                    return;
                }

                Damager damager = user.getDamager();
                if (damager != null) {
                    damager.handle(p);
                    if (damager == Damager.CRAP) {
                        if (current >= nextCrap) {
                            for (int i = 0; i < PvPDojo.RANDOM.nextInt(4) + 1; i++) {
                                Material mat = randomMaterials[PvPDojo.RANDOM.nextInt(randomMaterials.length)];
                                ItemStack item = new ItemStack(mat, Math.min(mat.getMaxStackSize(), PvPDojo.RANDOM.nextInt(17)));
                                Item spawned = p.getWorld().dropItem(p.getLocation().add(0, 1, 0), item);
                                spawned.setPickupDelay(5);
                            }
                        }
                    }
                }
            }, 10, 3, 9);

            if (current >= nextCrap) {
                nextCrap = PvPDojo.RANDOM.nextInt(6) + 1;
                current = 0;
            }
            current++;
        };
    }

}
