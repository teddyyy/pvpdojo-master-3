/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges.userdata;

import org.bukkit.entity.Player;

public class CurrentChallengeStats {

    private int soupsEaten;
    private double damageTaken;
    private int soupsDropped;

    public int getSoupsEaten() {
        return soupsEaten;
    }

    public void eatSoup() {
        soupsEaten++;
    }

    public double getDamageTaken() {
        return damageTaken;
    }

    public double getDamageTaken(Player player) {
        if (player == null) {
            return getDamageTaken();
        }
        return damageTaken - (player.getMaxHealth() - player.getHealth());
    }

    public void takeDamage(double damage) {
        damageTaken += damage;
    }

    public int getSoupsDropped() {
        return soupsDropped;
    }

    public void dropSoup() {
        soupsDropped++;
    }

    public int getSoupAccuracy(Player player) {
        if (player == null || soupsEaten == 0) {
            return 0;
        }
        return Math.min(100, (int) ((getDamageTaken(player) / (soupsEaten * 7)) * 100));
    }

}
