/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.bukkit.events.DojoPlayerSoupEvent;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.challenges.userdata.ChallengesUser;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class InteractListener implements DojoListener {

    @EventHandler
    public void onSoup(DojoPlayerSoupEvent e) {
        if (e.getPlayer().getHealth() < e.getPlayer().getMaxHealth()) {
            ChallengesUser.getUser(e.getPlayer()).getCurrentChallengeStats().eatSoup();
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {

        if (e.getClickedBlock() != null && e.getClickedBlock().getState() instanceof Sign) {
            Sign sign = (Sign) e.getClickedBlock().getState();
            ChallengesUser user = ChallengesUser.getUser(e.getPlayer());

            if (user.isOnRefillSignCooldown()) {
                user.sendMessage(CC.RED + "Cooling down...");
                return;
            }

            Inventory refillInv = null;

            if (sign.getLines()[1].contains("Refill")) {
                if (sign.getLines()[2].contains("Recraft")) {
                    e.getPlayer().getInventory().clear();
                    PlayerUtil.refillRecraft(e.getPlayer(), 32);
                    PlayerUtil.refillSoup(e.getPlayer(), 35);
                    user.refillSignClick();
                    e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.NOTE_PLING, 1, 1);
                    return;
                }
                refillInv = createSoupInventory();
            } else if (sign.getLines()[1].contains("Recraft")) {
                refillInv = createRecraftInventory();
            }

            if (refillInv != null) {
                e.getPlayer().openInventory(refillInv);
                user.refillSignClick();
            }
        }

    }

    public Inventory createSoupInventory() {
        Inventory inventory = Bukkit.createInventory(null, 54);
        for (int i = 0; i < inventory.getSize(); i++) {
            inventory.setItem(i, new ItemStack(Material.MUSHROOM_SOUP));
        }
        return inventory;
    }

    public Inventory createRecraftInventory() {
        Inventory inventory = Bukkit.createInventory(null, 54);
        for (int i = 0; i < inventory.getSize(); i += 3) {
            inventory.setItem(i, new ItemStack(Material.BOWL, 64));
            inventory.setItem(i + 1, new ItemStack(Material.BROWN_MUSHROOM, 64));
            inventory.setItem(i + 2, new ItemStack(Material.RED_MUSHROOM, 64));
        }
        return inventory;
    }

}
