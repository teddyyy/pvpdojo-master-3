/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.challenges.userdata.ChallengesUser;

public class DamageListener implements Listener {

    public static void register() {
        Bukkit.getPluginManager().registerEvents(new DamageListener(), PvPDojo.get());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {
            if (((Player) e.getEntity()).getInventory().contains(Material.ENDER_PORTAL_FRAME)) {
                ((Player) e.getEntity()).getInventory().remove(Material.ENDER_PORTAL_FRAME);
                ((Player) e.getEntity()).getInventory().addItem(new ItemStack(Material.MUSHROOM_SOUP));
                ChallengesUser.getUser((Player) e.getEntity()).resetCurrentChallengeStats();
            }
            ChallengesUser.getUser((Player) e.getEntity()).getCurrentChallengeStats().takeDamage(Math.min(((Player) e.getEntity()).getHealth(), e.getFinalDamage()));
        }
    }

    @EventHandler
    public void onCraftAttackCactus(EntityDamageEvent e) {
        if (e.getCause() == DamageCause.CONTACT) {
            e.setDamage(2D); // OMEGA LUL
        }
    }

}
