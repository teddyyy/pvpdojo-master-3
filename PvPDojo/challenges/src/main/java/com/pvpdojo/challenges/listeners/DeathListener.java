/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges.listeners;

import static com.pvpdojo.util.StringUtils.getLS;

import java.util.EnumSet;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.PlayerPreDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.achievement.Achievement;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.challenges.Challenges;
import com.pvpdojo.challenges.Damager;
import com.pvpdojo.challenges.userdata.ChallengesUser;
import com.pvpdojo.challenges.userdata.CurrentChallengeStats;
import com.pvpdojo.util.bukkit.CC;

public class DeathListener implements DojoListener {

    @EventHandler
    public void onPreDeath(PlayerPreDeathEvent e) {
        ChallengesUser user = ChallengesUser.getUser(e.getPlayer());
        Damager damager = user.getDamager();

        if (damager != null && damager != Damager.CONS && user.getCurrentChallengeStats().getSoupsEaten() > 70) {
            Inventory inv = e.getPlayer().getInventory();

            if (!inv.contains(Material.MUSHROOM_SOUP) && !inv.contains(Material.RED_MUSHROOM) && !inv.contains(Material.BROWN_MUSHROOM)) {
                e.getPlayer().setNoDamageTicks(10 * 20);
                e.getPlayer().setVelocity(new Vector(0, 1.6, 0));
                user.setDamagerCooldown(System.currentTimeMillis() + 5_000);
                sendStats(user, true);
                user.resetCurrentChallengeStats();
                e.getPlayer().setHealth(e.getPlayer().getMaxHealth());
                PvPDojo.schedule(() -> {
                    if (user.getDamager() == damager) {
                        e.getPlayer().teleport(Challenges.get().getSpawn());
                        if (user.getRespawnLocation() != null) {
                            user.teleport(user.getRespawnLocation());
                        }
                    }
                }).createTask(4 * 20);
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        sendStats(ChallengesUser.getUser(e.getEntity()), false);
    }


    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        if (!e.getFrom().getBlock().equals(e.getTo().getBlock())) {
            if (e.getTo().getBlock().getType() == Material.GOLD_PLATE) {
                PvPDojo.schedule(() -> {
                    ChallengesUser user = ChallengesUser.getUser(e.getPlayer());
                    sendStats(user, true);
                    user.teleport(Challenges.get().getSpawn());
                }).nextTick();
            }
        }
    }

    public void sendStats(ChallengesUser user, boolean win) {
        CurrentChallengeStats stats = user.getCurrentChallengeStats();

        user.sendMessage(CC.GRAY + getLS());

        if (win) {
            user.sendMessage(CC.GREEN + "You won the challenge");

            if (user.getDamager() != null) {

                user.getCompletedDamagers().add(user.getDamager());

                if (user.getCompletedDamagers().containsAll(EnumSet.of(Damager.EASY, Damager.MIDDLE, Damager.HARD))) {
                    Achievement.TANKING_BASICS.trigger(user.getPlayer());
                }

                switch (user.getDamager()) {
                    case CRAP:
                        Achievement.INVENTORY_MANAGEMENT.trigger(user.getPlayer());
                        break;
                    case EXTREME:
                        Achievement.TANK_GOD.trigger(user.getPlayer());
                        break;
                    case INCONS:
                        Achievement.INCONSISTENCY.trigger(user.getPlayer());
                        break;

                }
            } else {
                Block indicator = user.getPlayer().getLocation().getBlock().getRelative(BlockFace.DOWN);
                ItemStack type = new ItemStack(indicator.getType());
                type.setData(new MaterialData(indicator.getType()));
                Achievement achievement = Challenges.get().getGoldPlates().get(type);

                if (achievement != null) {
                    PvPDojo.schedule(() -> achievement.trigger(user.getPlayer())).nextTick();
                }
            }

            if (stats.getSoupAccuracy(user.getPlayer()) == 100) {
                PvPDojo.schedule(() -> Achievement.PINPOINT_PRECISION.trigger(user.getPlayer())).nextTick();
            }
        }

        user.sendMessage(CC.GRAY + "Soups eaten" + PvPDojo.POINTER + CC.GREEN + stats.getSoupsEaten());
        user.sendMessage(CC.GRAY + "Soups dropped" + PvPDojo.POINTER + CC.RED + stats.getSoupsDropped());
        user.sendMessage(CC.GRAY + "Soup accuracy" + PvPDojo.POINTER + CC.GOLD + stats.getSoupAccuracy(user.getPlayer()) + "%");
        user.sendMessage(CC.GRAY + "Damage taken" + PvPDojo.POINTER + CC.BLUE + (int) (stats.getDamageTaken(win ? user.getPlayer() : null) / 2) + " \u2764");

        user.sendMessage(CC.GRAY + getLS());
    }

}
