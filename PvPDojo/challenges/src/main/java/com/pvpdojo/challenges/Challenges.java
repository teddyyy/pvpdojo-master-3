/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.TreeSpecies;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Tree;
import org.bukkit.material.WoodenStep;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.UserList;
import com.pvpdojo.achievement.Achievement;
import com.pvpdojo.challenges.command.SpawnCommand;
import com.pvpdojo.challenges.listeners.DamageListener;
import com.pvpdojo.challenges.listeners.DeathListener;
import com.pvpdojo.challenges.listeners.InteractListener;
import com.pvpdojo.challenges.listeners.InventoryListener;
import com.pvpdojo.challenges.listeners.ItemListener;
import com.pvpdojo.challenges.listeners.JoinListener;
import com.pvpdojo.challenges.listeners.RegionChangeListener;
import com.pvpdojo.challenges.listeners.RespawnListener;
import com.pvpdojo.challenges.userdata.ChallengesUser;
import com.pvpdojo.command.lib.DojoCommandManager;
import com.pvpdojo.extension.DojoServer;
import com.pvpdojo.eyehawk.EyeHawk;
import com.pvpdojo.map.DojoMap;
import com.pvpdojo.map.MapLoader;
import com.pvpdojo.map.MapType;
import com.pvpdojo.replay.Recorder;
import com.pvpdojo.session.arena.ArenaManager;
import com.pvpdojo.session.arena.LavaDuelArena;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;

import lombok.Getter;

public class Challenges extends DojoServer<ChallengesUser> {

    private static Challenges inst;

    private ArenaManager<LavaDuelArena> arenaManager = new ArenaManager<>(MapType.LAVA_DUEL);
    @Getter
    private final Map<ItemStack, Achievement> goldPlates = new HashMap<>();

    public static Challenges get() {
        return inst;
    }

    @Override
    public void onLoad() {
        super.onLoad();
        deleteFolder("world");
    }

    @Override
    public ServerType getServerType() {
        return ServerType.CHALLENGES;
    }

    public void start() {
        inst = this;
        getSettings().setSoup(true);
        getSettings().setReducedFallDamage(false);
        getSettings().setAbilities(false);

        loadListeners();
        createDamagerTimer();
        clearRegions(Bukkit.getWorlds().get(0));
    }

    @Override
    public void postLoad() {
        loadCommands();

        goldPlates.put(new ItemBuilder(Material.WOOD_DOUBLE_STEP).data(new WoodenStep(TreeSpecies.DARK_OAK).getData()).build(), Achievement.TOP_OF_THE_WORLD);
        goldPlates.put(new ItemBuilder(Material.WOOD).data(new Tree(TreeSpecies.REDWOOD).getData()).build(), Achievement.GOING_DOWNSTAIRS);
        goldPlates.put(new ItemBuilder(Material.COBBLESTONE).build(), Achievement.CONSISTENCY);
        goldPlates.put(new ItemBuilder(Material.SMOOTH_BRICK).build(), Achievement.ESCELATION);
        goldPlates.put(new ItemBuilder(Material.OBSIDIAN).build(), Achievement.AGILITY);
        goldPlates.put(new ItemBuilder(Material.GOLD_BLOCK).build(), Achievement.SUPER_CONSISTENCY);

        // Disable auto bans
        EyeHawk.AUTO_BAN = false;
        Recorder.inst().stop();
        Bukkit.setSaveBandwidth(true);
    }

    @Override
    public DojoMap setupMap() {
        DojoMap challengesMap = MapLoader.getMap(MapType.DEFAULT, "challenges");
        if (challengesMap == null) { // If map cannot be found just make an empty server
            return null;
        }

        Location spawn = challengesMap.getLocations()[0];
        challengesMap.setWorld(Bukkit.getWorlds().get(0));
        getSettings().setItemDespawnRate(challengesMap.getWorld(), 7 * 20);
        challengesMap.shift(-spawn.getBlockX(), 0, -spawn.getBlockZ()); // Move spawn to 0,0
        challengesMap.setup(0);
        challengesMap.placeBlocks(false);
        challengesMap.getRegion().setFlag(DefaultFlag.PVP, StateFlag.State.DENY);
        return challengesMap;
    }

    @Override
    public UserList<ChallengesUser> createUserList() {
        return new UserList<>(ChallengesUser::new);
    }

    public void loadListeners() {
        DamageListener.register();
        RegionChangeListener.register();
        RespawnListener.register();
        new JoinListener().register();
        ItemListener.register();
        InventoryListener.register();
        new InteractListener().register();
        new DeathListener().register();
    }

    public void loadCommands() {
        DojoCommandManager.get().registerCommand(new SpawnCommand());
    }

    public void createDamagerTimer() {
        new DamagerTimer().createTimer(12, -1);
    }

    public Location getSpawn() {
        return PvPDojo.get().getStandardMap().getLocations()[0];
    }

    public Location getLava() {
        return PvPDojo.get().getStandardMap().getLocations()[1];
    }

    public Location getDamagers() {
        return PvPDojo.get().getStandardMap().getLocations()[2];
    }

    public Location getExtremes() {
        return PvPDojo.get().getStandardMap().getLocations()[3];
    }

    public Location getParcours() {
        return PvPDojo.get().getStandardMap().getLocations()[4];
    }

    public ArenaManager<LavaDuelArena> getArenaManager() {
        return arenaManager;
    }
}
