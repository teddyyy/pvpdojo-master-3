/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.challenges.Challenges;
import com.pvpdojo.challenges.userdata.ChallengesUser;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class RespawnListener implements Listener {

    public static void register() {
        Bukkit.getPluginManager().registerEvents(new RespawnListener(), PvPDojo.get());
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        ChallengesUser user = ChallengesUser.getUser(e.getPlayer());

        PlayerUtil.refillRecraft(e.getPlayer(), 64);
        PlayerUtil.refillSoup(e.getPlayer(), 35);
        e.getPlayer().getInventory().setItem(0, new ItemBuilder(Material.STONE_SWORD).unbreakable().build());
        e.getPlayer().getInventory().setItem(8, new ItemBuilder(Material.ENDER_PORTAL_FRAME).name(CC.GREEN + "Set Respawn Point").build());

        if (user.getRespawnLocation() != null) {
            e.setRespawnLocation(user.getRespawnLocation());
        } else {
            e.setRespawnLocation(Challenges.get().getSpawn());
        }
    }

}
