/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges.menu;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.challenges.Challenges;
import com.pvpdojo.challenges.userdata.ChallengeRequest;
import com.pvpdojo.challenges.userdata.ChallengesUser;
import com.pvpdojo.gui.HotbarGUI;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.Request;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class ChallengesHotbarGUI extends HotbarGUI {

    private static ChallengesHotbarGUI instance = new ChallengesHotbarGUI();

    public static ChallengesHotbarGUI get() {
        return instance;
    }

    public ChallengesHotbarGUI() {
        setItem(0, new ItemBuilder(Material.COMPASS).name(CC.GREEN + "Challenge Warps").build(), onRightClick(player -> new ChallengesWarpMenu(player).open()));

        setItem(2, new ItemBuilder(Material.NAME_TAG).name(CC.GREEN + "Switch to Recraft").build(), onRightClick(player -> {
            ChallengesUser user = ChallengesUser.getUser(player);
            user.setRecraftTimer(!user.isRecraftTraining());
            if (user.isRecraftTraining()) {
                player.getItemInHand().getItemMeta().setDisplayName(CC.GREEN + "Switch to Refill");
            } else {
                player.getItemInHand().getItemMeta().setDisplayName(CC.GREEN + "Switch to Recraft");
            }
            user.sendMessage(PvPDojo.PREFIX + "Recraft: " + (user.isRecraftTraining() ? CC.GREEN + "enabled" : CC.RED + "disabled"));
        }));

        setItem(4, new ItemBuilder(Material.BLAZE_ROD).name(CC.GREEN + "Challenger").build(), (player, action, entity) -> {
            if (entity instanceof Player) {
                User user = User.getUser(player);
                Player target = (Player) entity;
                ChallengesUser targetUser = ChallengesUser.getUser(target);

                if (user.getRequests().containsKey(targetUser.getUUID())) {
                    Request request = user.getRequests().get(targetUser.getUUID());
                    if (request instanceof ChallengeRequest) {
                        user.getPlayer().performCommand("accept " + targetUser.getUUID());
                        return;
                    }
                }

                String fail = targetUser.makeRequest(player.getUniqueId(), new ChallengeRequest(User.getUser(player)));
                if (fail == null) {
                    user.sendMessage(MessageKeys.INVITE_DUEL, "{target}", targetUser.getName());
                    targetUser.sendMessage(MessageKeys.INVITED_DUEL, "{inviter}", user.getName());
                } else {
                    user.sendMessage(MessageKeys.WARNING, "{text}", fail);
                }
            }
        });


        setItem(7, new ItemBuilder(Material.REDSTONE).name(CC.GREEN + "Return to Hub").build(), onRightClick(ServerType.HUB::connect));

        setItem(8, new ItemBuilder(Material.ENDER_PORTAL_FRAME).name(CC.GREEN + "Set Respawn Point " + CC.GRAY + "(Sneak click to reset)").build(), onRightClick(player -> {
            if (!player.isOnGround() || player.getFallDistance() > 3) {
                player.sendMessage(PvPDojo.WARNING + "You cannot set your respawn point mid air");
                return;
            }
            ChallengesUser.getUser(player).setRespawnLocation(player.isSneaking() ? Challenges.get().getSpawn() : player.getLocation());
            player.sendMessage(PvPDojo.PREFIX + "You are now respawning at " + CC.GREEN + (player.isSneaking() ? "spawn" : "current location"));
        }));

    }

}
