/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges.userdata;

import com.pvpdojo.challenges.Challenges;
import com.pvpdojo.challenges.session.LavaDuelSession;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.userdata.Request;
import com.pvpdojo.userdata.User;

public class ChallengeRequest extends Request {

    private User requester;

    public ChallengeRequest(User requester) {
        this.requester = requester;
    }

    @Override
    public void accept(User acceptor) {
        if (!requester.isLoggedOut()) {
            LavaDuelSession session = new LavaDuelSession(Challenges.get().getArenaManager().getRandomFreeArena(), SessionType.LAVA_DUEL, requester, acceptor);
            session.start();
        } else {
            acceptor.sendMessage(MessageKeys.COULD_NOT_FIND_PLAYER);
        }
    }

    @Override
    public void deny(User deniedBy) {}

    @Override
    public int getExpirationSeconds() {
        return 60;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ChallengeRequest;
    }
}
