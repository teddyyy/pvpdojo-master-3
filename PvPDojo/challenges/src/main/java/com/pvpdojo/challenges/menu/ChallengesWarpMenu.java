/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges.menu;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.pvpdojo.challenges.Challenges;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class ChallengesWarpMenu extends SingleInventoryMenu {

    public ChallengesWarpMenu(Player player) {
        super(player, CC.GREEN + "Challenges Warps", 27);
        setCloseOnClick(true);
    }

    @Override
    public void redraw() {
        InventoryPage inv = getCurrentPage();

        inv.setItem(4, new ItemBuilder(Material.LAVA_BUCKET).name(CC.RED + "Lava Challenges").build(), type -> getPlayer().teleport(Challenges.get().getLava()));
        inv.setItem(11, new ItemBuilder(Material.FENCE_GATE).name(CC.RED + "Parkour Challenges").build(), type -> getPlayer().teleport(Challenges.get().getParcours()));
        inv.setItem(15, new ItemBuilder(Material.BLAZE_POWDER).name(CC.RED + "Extreme Challenges").build(), type -> getPlayer().teleport(Challenges.get().getExtremes()));
        inv.setItem(22, new ItemBuilder(Material.BEDROCK).name(CC.RED + "Damager Challenges").build(), type -> getPlayer().teleport(Challenges.get().getDamagers()));
        
        inv.fillBlank();
    }

}
