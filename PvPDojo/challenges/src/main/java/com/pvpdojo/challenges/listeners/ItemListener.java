/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges.listeners;

import java.util.Map;
import java.util.UUID;
import java.util.WeakHashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

import com.pvpdojo.PvPDojo;

public class ItemListener implements Listener {

    public static void register() {
        Bukkit.getPluginManager().registerEvents(new ItemListener(), PvPDojo.get());
    }

    private Map<Item, UUID> ownedItems = new WeakHashMap<>();

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onDrop(PlayerDropItemEvent e) {
        ownedItems.put(e.getItemDrop(), e.getPlayer().getUniqueId());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPickUp(PlayerPickupItemEvent e) {
        if (ownedItems.containsKey(e.getItem()) && !ownedItems.get(e.getItem()).equals(e.getPlayer().getUniqueId())) {
            e.setCancelled(true);
        }
    }

}
