/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.RegionEnterEvent;
import com.pvpdojo.bukkit.events.RegionLeaveEvent;
import com.pvpdojo.challenges.menu.ChallengesHotbarGUI;
import com.pvpdojo.challenges.userdata.ChallengesUser;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class RegionChangeListener implements Listener {

    public static void register() {
        Bukkit.getPluginManager().registerEvents(new RegionChangeListener(), PvPDojo.get());
    }

    @EventHandler
    public void onRegionEnter(RegionEnterEvent e) {
        User user = User.getUser(e.getPlayer());

        if (user.isAdminMode()) {
            return;
        }

        if (e.getRegion().getId().startsWith("spawn")) {
            e.getPlayer().setWalkSpeed(0.65F);
            e.getPlayer().setHealth(e.getPlayer().getMaxHealth());
            user.setHotbarGui(ChallengesHotbarGUI.get());
        } else if (e.getRegion().getId().startsWith("void")) {
            e.getPlayer().teleport(e.getPlayer().getLocation().subtract(0, 200, 0));
        }
    }

    @EventHandler
    public void onRegionLeave(RegionLeaveEvent e) {
        ChallengesUser user = ChallengesUser.getUser(e.getPlayer());

        if (user.isAdminMode()) {
            return;
        }

        if (user.getSession() != null) {
            int localId = user.getSession().getArena().getLocalId();
            String[] regionSplit = e.getRegion().getId().split("_");
            if (regionSplit.length > 1 && regionSplit[regionSplit.length - 1].equals(String.valueOf(localId))) {
                e.setCancelled(true);
            }
        }

        if (e.getRegion().getId().startsWith("spawn")) {
            user.resetCurrentChallengeStats();
            e.getPlayer().setWalkSpeed(0.2F);
            e.getPlayer().getInventory().clear();
            PlayerUtil.refillRecraft(e.getPlayer(), 64);
            PlayerUtil.refillSoup(e.getPlayer(), 35);
            e.getPlayer().getInventory().setItem(0, new ItemBuilder(Material.STONE_SWORD).unbreakable().build());
            if (user.getSession() == null) {
                e.getPlayer().getInventory().setItem(8,
                        new ItemBuilder(Material.ENDER_PORTAL_FRAME).name(CC.GREEN + "Set Respawn Point " + CC.GRAY + "(Sneak click to reset)").build());
            }
        }
    }

}
