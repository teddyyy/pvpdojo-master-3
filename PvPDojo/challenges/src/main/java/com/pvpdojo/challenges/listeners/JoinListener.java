/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.DojoPlayerJoinEvent;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.challenges.Challenges;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class JoinListener implements DojoListener {

    @EventHandler
    public void onJoin(DojoPlayerJoinEvent e) {
        PlayerUtil.resetPlayer(e.getPlayer());
        e.getPlayer().teleport(Challenges.get().getSpawn());
        User user = User.getUser(e.getPlayer());
        user.setUseStats(false);
        user.setDeathDrops(false);

        if (Bukkit.getOnlinePlayers().size() > 20) {
            PvPDojo.schedule(() -> user.setVanish(true)).nextTick();
        }
    }

}
