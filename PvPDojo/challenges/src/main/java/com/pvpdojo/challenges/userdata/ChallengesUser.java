/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges.userdata;

import java.util.EnumSet;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.pvpdojo.ServerType;
import com.pvpdojo.Warp;
import com.pvpdojo.challenges.Challenges;
import com.pvpdojo.challenges.Damager;
import com.pvpdojo.userdata.impl.UserImpl;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import lombok.Getter;

public class ChallengesUser extends UserImpl {

    public static ChallengesUser getUser(Player player) {
        return Challenges.get().getUser(player);
    }

    private Location respawnLocation;
    private boolean recraftTimer;
    private long lastRefillSignClick;
    private long damagerCooldown;
    private CurrentChallengeStats currentChallengeStats = new CurrentChallengeStats();
    @Getter
    private EnumSet<Damager> completedDamagers = EnumSet.noneOf(Damager.class);

    public ChallengesUser(UUID uuid) {
        super(uuid);
    }

    @Override
    public void handleWarp(Warp warp) {
        super.handleWarp(warp);
        if (warp.getServer() == ServerType.CHALLENGES) {
            switch (warp) {
                case DAMAGER:
                    teleport(Challenges.get().getDamagers());
                    break;
                case LAVA:
                    teleport(Challenges.get().getLava());
                    break;
                case PARCOUR:
                    teleport(Challenges.get().getParcours());
                    break;
                default:
                    break;
            }
        }
    }

    public Location getRespawnLocation() {
        return respawnLocation;
    }

    public void setRespawnLocation(Location respawnLocation) {
        this.respawnLocation = respawnLocation;
    }

    public boolean isRecraftTraining() {
        return recraftTimer;
    }

    public void setRecraftTimer(boolean recraftTimer) {
        this.recraftTimer = recraftTimer;
    }

    public long getLastRefillSignClick() {
        return lastRefillSignClick;
    }

    public void refillSignClick() {
        this.lastRefillSignClick = System.currentTimeMillis();
    }

    public boolean isOnRefillSignCooldown() {
        return System.currentTimeMillis() < this.lastRefillSignClick + 5_000;
    }

    public CurrentChallengeStats getCurrentChallengeStats() {
        return currentChallengeStats;
    }

    public void resetCurrentChallengeStats() {
        this.currentChallengeStats = new CurrentChallengeStats();
    }

    public long getDamagerCooldown() {
        return damagerCooldown;
    }

    public void setDamagerCooldown(long damagerCooldown) {
        this.damagerCooldown = damagerCooldown;
    }

    public Damager getDamager() {
        for (ProtectedRegion region : getApplicableRegions()) {
            Damager damager = Damager.getDamager(region);
            if (damager != null) {
                return damager;
            }
        }
        return null;
    }

}
