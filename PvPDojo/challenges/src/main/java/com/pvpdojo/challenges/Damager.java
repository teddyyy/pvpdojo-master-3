/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public enum Damager {
    EASY(
            "damagereasy",
            4D),
    MIDDLE(
            "damagermiddle",
            5D),
    HARD(
            "damagerhard",
            7D),
    EXTREME(
            "damagerextreme",
            9D),
    CRAP(
            "damagercrap",
            5D),
    CONS(
            "damagercons",
            5D),
    INCONS(
            "damagerincons",
            0D);

    private static boolean setup;
    private static final Map<String, Damager> REGION_MAP = new HashMap<>();

    private double damage;

    Damager(String match, double damage) {
        RegionManager rm = WorldGuardPlugin.inst().getRegionManager(Bukkit.getWorlds().get(0));
        for (ProtectedRegion region : rm.getRegions().values()) {
            if (region.getId().contains(match)) {
                regions.add(region);
            }
        }

        this.damage = damage;
    }

    private Set<ProtectedRegion> regions = new HashSet<>();

    public Set<ProtectedRegion> getRegions() {
        return regions;
    }

    public void handle(Player player) {
        if (this == INCONS) {
            damage = PvPDojo.RANDOM.nextInt(7) + 4;
        }
        if (!BukkitUtil.callEntityDamageEvent(player, DamageCause.CUSTOM, damage).isCancelled()) {
            player.damage(damage);
        }
    }

    public static Damager getDamager(ProtectedRegion region) {
        setupMap();
        return REGION_MAP.get(region.getId());
    }

    public static void setupMap() {
        if (!setup) {
            REGION_MAP.clear();
            for (Damager damager : values()) {
                for (ProtectedRegion region : damager.regions) {
                    if (!REGION_MAP.containsKey(region.getId())) {
                        REGION_MAP.put(region.getId(), damager);
                    }
                }
            }
            setup = true;
        }
    }

}
