/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges.command;

import com.pvpdojo.challenges.Challenges;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("spawn")
public class SpawnCommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onSpawn(User user) {
        if (user.getSession() == null) {
            user.teleport(Challenges.get().getSpawn());
        }
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
