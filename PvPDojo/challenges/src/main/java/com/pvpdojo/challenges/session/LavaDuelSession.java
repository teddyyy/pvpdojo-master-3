/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges.session;

import static com.pvpdojo.util.StreamUtils.negate;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.achievement.Achievement;
import com.pvpdojo.challenges.Challenges;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.session.GameSession;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.arena.LavaDuelArena;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.entity.RankedArenaEntity;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.PlayerUtil;

import net.md_5.bungee.api.chat.TextComponent;

public class LavaDuelSession extends GameSession {

    public LavaDuelSession(LavaDuelArena arena, SessionType type, ArenaEntity... participants) {
        super(arena, type, participants);
    }

    @Override
    public void start() {
        super.start();
        setState(SessionState.INVINCIBILITY);
        PvPDojo.schedule(() -> {
            setState(SessionState.STARTED);
            getParticipants().forEach(entity -> entity.setFrozen(false));
        }).createTask(20 * 3);
    }

    @Override
    public void end(ArenaEntity... losers) {
        super.end(losers);
        getParticipants().forEach(entity -> entity.teleport(Challenges.get().getSpawn()));
    }

    @Override
    public void endRanked(RankedArenaEntity winner, ArenaEntity... losers) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onLose(ArenaEntity loser) {}

    @Override
    public void onWin(ArenaEntity winner) {
        broadcast(MessageKeys.DEATHMATCH_WON, "{winner}", winner.getLongName(), "{color}", CC.BLUE.toString());
        Achievement.RIVALRY.trigger(winner.getPlayer());
    }

    @Override
    public void postDeath(ArenaEntity entity) {
        super.postDeath(entity);
        triggerEnd(getCurrentlyDead().toArray(new ArenaEntity[0]));
    }

    @Override
    public void handleMatchDetails(TextComponent tc) {
        getParticipants().forEach(entity -> entity.sendMessage(tc));
    }

    @Override
    public void prepareEntity(ArenaEntity entity) {
        entity.getPlayer().getInventory().clear();
        entity.getPlayer().getInventory().setItem(0, new ItemStack(Material.STONE_SWORD));
        PlayerUtil.refillSoup(entity.getPlayer(), 35);
        PlayerUtil.refillRecraft(entity.getPlayer(), 64);
        entity.setFrozen(true);
    }

    @Override
    public boolean checkEndCondition() {
        return getParticipants().stream().filter(negate(getCurrentlyDead()::contains)).count() <= 1;
    }

    @Override
    public boolean isPersistent() {
        return true;
    }
}
