/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.eyehawk.types;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.EyeHawkEvent;
import org.bukkit.event.player.EyeHawkEvent.Level;
import org.bukkit.event.player.EyeHawkEvent.Type;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.NumberConversions;
import org.bukkit.util.Vector;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.eyehawk.EyeHawk;
import com.pvpdojo.eyehawk.types.LocationTrace.LocationTraceEntry;
import com.pvpdojo.util.TimeUtil;
import com.pvpdojo.util.bukkit.CC;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.server.v1_7_R4.EntityPlayer;
import net.minecraft.util.com.google.common.math.DoubleMath;
import net.minecraft.util.com.mojang.authlib.GameProfile;

public class CheatData {

    public static final GameProfile fakeEntityProfile = new GameProfile(UUID.randomUUID(), "§8§f§4§2§5§3§c§8");

    private static final double JUMP_VEL = 0.41999998688697815, JUMP_HEIGHT = 1.255, ON_GROUND_VEL = -0.0784000015258789, PRECISION = 0.5, SLAB_YS = 0.48444492729;
    public static final LoadingCache<Player, CheatData> cheatdataMap = CacheBuilder.newBuilder().expireAfterAccess(1, TimeUnit.MINUTES).build(new CacheLoader<Player, CheatData>() {
        @Override
        public CheatData load(Player player) {
            return new CheatData(player);
        }
    });

    private static final double _2PI = 2 * Math.PI;
    public static final DecimalFormat DEC_FORMAT = new DecimalFormat("#0.000");

    private Player p, fake, lastTarget;
    public int cps, roasted, speed, heal, lastlevel, packets, flyTypeA, flyTypeB, flyTypeC, flyTypeF, attackedBot, reset = 5, reachTask, headMovesInInv, invMoveVL;
    private Human fakeEntity, reachBot;
    public float lastWalkSpeed, lastTeleportFalldistance;
    private double directionVL, minViolation, lastYS;
    public long tptime, lastheal, icetime, potdecrease, lastjump, velocitytime, walkSpeedChange, packetStart, fakeEntityStart, hadJumpBoost, attackBotStart, ignoreHI,
            ignoreDirection, ignorePackets, ignoreReachBot, flyResetTypeA, flyResetTypeB, flyResetTypeC, flyResetTypeF, lastReachNotify, lastTeleport, firstClick = -1,
            lastBlockPlace, ignoreInvChecks = System.currentTimeMillis() + 1000;
    private boolean banned, alerts = false, debug = false, randomBan, wasAbleToFly, inventoryOpen, firstInvWalk;
    private Vector latestVelocity;
    private Location lastLocationOnGround, lastMove = new Location(Bukkit.getWorlds().get(0), 0, 0, 0);
    private Block lastBrokenBlock;
    private LocationTrace locationTrace;
    private List<Integer> slots = new ArrayList<>(20);
    @Getter
    private List<Long> killauraTypeC = new ArrayList<>(), killauraTypeH = new ArrayList<>(), killauraTypeI = new ArrayList<>(), killauraTypeHLong = new ArrayList<>(),
            killauraTypeILong = new ArrayList<>(), killauraTypeU = new ArrayList<>(), criticalsTypeB = new ArrayList<>(), miniJumps = new ArrayList<>(),
            reducedKB = new ArrayList<>(), reducedKBHorizontal = new ArrayList<>(), antiKB = new ArrayList<>(), killauraTypeF = new ArrayList<>(),
            killauraTypeE2 = new ArrayList<>(), badPackets = new ArrayList<>(), killAuraReachShort = new ArrayList<>(), killauraTypeV = new ArrayList<>(),
            killauraTypeZ = new ArrayList<>(), inventoryTypeB = new ArrayList<>(), inventoryTypeA = new ArrayList<>(), inventoryTypeC = new ArrayList<>(),
            timerTypeG = new ArrayList<>();
    @Getter
    @Setter
    private String lastTypeFMessage = "";
    private Cache<Long, Integer> recentTicksSinceLastPacket = CacheBuilder.newBuilder().expireAfterWrite(7, TimeUnit.SECONDS).build();

    public CheatData(Player p) {
        this.p = p;
        lastLocationOnGround = p.getLocation();
        this.locationTrace = new LocationTrace();
        tptime = System.currentTimeMillis() + 3000;
        lastWalkSpeed = getPlayer().getWalkSpeed();
    }

    public boolean isRandomBan() {
        return randomBan;
    }

    public void banRandomly() {
        this.randomBan = true;
    }

    public int getPackets() {
        return packets;
    }

    public void resetPackets() {
        packets = 0;
    }

    public void resetHeal() {
        this.heal = 0;
    }

    public int getHeal() {
        return heal;
    }

    public void unlegitheal() {
        this.heal++;
    }

    public void clearRoasts() {
        this.roasted = 0;
    }

    public int getFlyTypeA() {
        return flyTypeA;
    }

    public int getFlyTypeB() {
        return flyTypeB;
    }

    public int getFlyTypeC() {
        return flyTypeC;
    }

    public int getFlyTypeF() {
        return flyTypeF;
    }

    public void resetFly() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - flyResetTypeA > 10_000) {
            this.flyTypeA = 0;
        }
        if (currentTime - flyResetTypeB > 10_000) {
            this.flyTypeB = 0;
        }
        if (currentTime - flyResetTypeC > 10_000) {
            this.flyTypeC = 0;
        }
        if (currentTime - flyResetTypeF > 10_000) {
            this.flyTypeF = 0;
        }
    }

    public int getReset() {
        return reset;
    }

    public void setReset(int reset) {
        this.reset = reset;
    }

    public boolean hasAlerts() {
        return alerts;
    }

    public void setAlerts(boolean alerts) {
        this.alerts = alerts;
    }

    public boolean hasDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public List<Integer> getSlots() {
        return slots;
    }

    public Collection<Integer> getRecentTicksSinceLastPacket() {
        return recentTicksSinceLastPacket.asMap().values();
    }

    public void applyRecentLag() {
        recentTicksSinceLastPacket.put(System.currentTimeMillis(), getTicksSinceLastPacket());
    }

    public int getRecentLag() {
        applyRecentLag();
        return Collections.max(recentTicksSinceLastPacket.asMap().values());
    }

    public long getClientTime() {
        return System.currentTimeMillis() - getRecentLag() * 50;
    }

    public int getTicksSinceLastPacket() {
        return getNMSPlayer().playerConnection.networkManager.ticksSinceLastPacket;
    }

    public Player getLastTarget() {
        return lastTarget;
    }

    public void setLastTarget(Player lastTarget) {
        this.lastTarget = lastTarget;
    }

    public void attackBot() {
        fakeEntityStart = System.currentTimeMillis();
        if (System.currentTimeMillis() - attackBotStart > 5_000) {
            attackedBot = 0;
            attackBotStart = System.currentTimeMillis();
        }
        attackedBot++;
        if (attackedBot >= 10) {
            EyeHawk.informStaff(getPlayer(), "Player " + getPlayer().getName() + " attacked 10 bots in 5 seconds.", Level.MOD);
            attackedBot = 0;
            attackBotStart = System.currentTimeMillis();
            roasted();
        }
    }

    public int getFakeEntityId() {
        return fakeEntity == null ? -1 : fakeEntity.getHuman().getId();
    }

    public void removeFakeEntities() {
        if (this.fakeEntity != null) {
            this.fakeEntity.remove();
            this.fakeEntity = null;
        }
    }

    public void queueBan() {
        if (EyeHawk.QUEUED_BANS.add(getPlayer().getUniqueId())) {
            EyeHawk.informStaff(getPlayer(),
                    CC.GREEN + getPlayer().getName() + CC.GRAY + " was detected using cheats and will be " + CC.RED + " banned " + CC.GRAY + "on next wave", Level.ADMIN);
        }
    }

    public void ban() {
        ban(false);
    }

    public void ban(boolean delayed) {
        if (EyeHawk.AUTO_BAN) {
            if (!banned) {
                this.banned = true;
                PvPDojo.schedule(() -> EyeHawk.informStaff(getPlayer(), ChatColor.GRAY + "Detected " + ChatColor.GREEN + getPlayer().getName() + ChatColor.GRAY + " using "
                        + ChatColor.RED + "cheats", Level.MOD)).createTask(0);
                Runnable bancommand = () -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "ban " + getPlayer().getName() + " [AntiSkid] Cheat Detection");
                if (delayed) {
                    EyeHawk.schedule(bancommand, EyeHawk.RANDOM.nextInt(30) * 20);
                } else {
                    bancommand.run();
                }
            }
        } else {
            roasted = 0;
        }

    }

    public boolean isBanned() {
        return this.banned;
    }

    public long getLastHeal() {
        return this.lastheal;
    }

    public void heal() {
        this.lastheal = System.currentTimeMillis();
    }

    public Player getPlayer() {
        return this.p;
    }

    public void roasted() {
        this.roasted++;
        if (roasted >= 6) {
            ban();
        }
    }

    public int getRoasted() {
        return this.roasted;
    }

    public int getCps() {
        return this.cps;
    }

    public void groundHit(double d) {

        if (d > 16) {
            long now = System.currentTimeMillis();
            if (d >= 18) {
                killAuraReachShort.add(now);
                if (d >= 20) {
                    killAuraReachShort.add(now);
                    if (d >= 30) {
                        killAuraReachShort.add(now);
                        if (d >= 36) {
                            killAuraReachShort.add(now);
                        }
                    }
                }
            }
            EyeHawk.calcLegitimacy(killAuraReachShort, this, 8 * 60 * 1000, 40, false);

            if ((killAuraReachShort.size() > 7 && now - lastReachNotify > 8_000) || killAuraReachShort.size() >= 40) {
                Bukkit.getPluginManager().callEvent(new EyeHawkEvent(p, Type.KILLAURA, Level.MOD, "Type G [" + killAuraReachShort.size() + "][" + NMSUtils.getPing(getPlayer()) + "ms]"));
                lastReachNotify = now;
            }
        }
    }

    public void setLatestVelocity(Vector latestVelocity) {
        this.latestVelocity = latestVelocity;
    }

    public int getSpeed() {
        return speed;
    }

    public void resetSpeed() {
        this.speed = 0;
    }

    public void speed() {
        this.speed++;
    }

    public LocationTrace getLocationTrace() {
        return locationTrace;
    }

    public void violateInvMove() {
        if (++invMoveVL == 5) {
            Bukkit.getPluginManager().callEvent(new EyeHawkEvent(getPlayer(), Type.INVENTORY, Level.MOD, "Type B"));
        }
    }

    @SuppressWarnings("deprecation")
    public void runMoveChecks(double xs, double zs, double ys, Location from, Location to) {
        Location move = to.clone().subtract(from);

        // movepart1.startTiming();
        long currentMachineTime = System.currentTimeMillis();
        long currentClientTime = getClientTime();
        if ((currentClientTime - tptime) < 0) {
            // movepart1.stopTiming();
            lastMove = move;
            return;
        }

        if (getPlayer().getAllowFlight() || getPlayer().getGameMode() == GameMode.CREATIVE) {
            wasAbleToFly = true;
        } else if (getPlayer().isOnGround()) {
            if (wasAbleToFly) {
                tptime = System.currentTimeMillis() + 3_000;
            }
            wasAbleToFly = false;
        }

        if (wasAbleToFly) {
            lastMove = move;
            return;
        }

        if (EyeHawk.getPing(getPlayer()) < 200 && Lag.getTPS() >= 19.9) {
            packets++;
            if (packets >= 121) {
                if (currentMachineTime - packetStart < 5890 && currentClientTime - ignorePackets > 1000) {
                    ignorePackets = currentClientTime;
                    if (currentMachineTime - packetStart < 5790) {
                        EyeHawk.informStaff(getPlayer(), "Player " + getPlayer().getName() + " sent too many packets", Level.MOD);
                        roasted();
                    }
                    EyeHawk.informDebug(getPlayer(), "Player " + getPlayer().getName() + " sent too many packets [low]");
                }
                packets = 0;
                packetStart = currentClientTime;
            }
        }

        float walkSpeed = getPlayer().getWalkSpeed();

        if (lastWalkSpeed != walkSpeed) {
            walkSpeedChange = currentClientTime;
            lastWalkSpeed = walkSpeed;
        }

        if (currentClientTime - walkSpeedChange < 10_000) {
            return;
        }

        if (getPlayer().hasPotionEffect(PotionEffectType.JUMP)) {
            hadJumpBoost = currentClientTime;
        }
        // movepart1.stopTiming();

        // movepartfly.startTiming();
        if (currentClientTime - 3000 - velocitytime > 0) {

            double lastZ = Math.abs(lastMove.getZ());
            double lastX = Math.abs(lastMove.getX());

            boolean equalMoveInInv = zs > 0.2 && zs == lastZ || xs > 0.2 && xs == lastX;

            if (inventoryOpen && getPlayer().getOpenInventory().getType() == InventoryType.CRAFTING) {
                if ((zs > lastZ || xs > lastX || equalMoveInInv) && !NMSUtils.isInWater(getPlayer())) {
                    if (!firstInvWalk) {
                        violateInvMove();
                    } else {
                        firstInvWalk = false;
                    }
                }
                if (from.getYaw() != to.getYaw() || from.getPitch() != to.getPitch()) {
                    if (++headMovesInInv > 4) {
                        violateInvMove();
                    }
                }
            }


            if (currentClientTime - hadJumpBoost > 3000) {
                boolean airOnly = NMSUtils.touchesOnly(getPlayer().getWorld(), getNMSPlayer().boundingBox.grow(1, 1, 1), Material.AIR);
                boolean corruptedChunk = DoubleMath.fuzzyEquals(ys, -0.098, 0.00001); // This is a hack do detect if player is falling into a corrupted chunk.

                if (getPlayer().getFallDistance() == 0 && lastYS < 0 && airOnly && getRecentLag() <= 10) {
                    if (flyTypeF++ > 5) {
                        Bukkit.getPluginManager().callEvent(new EyeHawkEvent(p, Type.FLY, Level.MOD, "Type F"));
                        roasted();
                        flyResetTypeF = 0;
                    } else {
                        flyResetTypeF = System.currentTimeMillis();
                    }
                }
                if (ys == lastYS && airOnly && !corruptedChunk && getTicksSinceLastPacket() <= 2 && !(isFallingDeep()
                        && DoubleMath.fuzzyEquals(ys, -3.92, 0.01)) && !(!TimeUtil.hasPassed(lastBlockPlace, TimeUnit.SECONDS, 3) && ys == 0)) {
                    // EyeHawk.informDebug("Player " + getPlayer().getName() + " flagged fly Type C (glide) (" +
                    // DEC_FORMAT.format(ys) + ")");
                    if (flyTypeC++ > 6) {
                        Bukkit.getPluginManager().callEvent(new EyeHawkEvent(p, Type.FLY, Level.MOD, "Type C (" + ys + ")"));
                        roasted();
                        flyResetTypeC = 0;
                    } else {
                        flyResetTypeC = System.currentTimeMillis();
                    }
                }

                if (ys > 0 && getLastLocationOnGround().getY() + JUMP_HEIGHT < from.getY() + ys && !DoubleMath.fuzzyEquals(ys, SLAB_YS, 0.00000000001)) {
                    // EyeHawk.informDebug("Player " + getPlayer().getName() + " flagged fly Type B (higher than
                    // vanilla)");
                    if (flyTypeB++ > 6) {
                        Bukkit.getPluginManager().callEvent(new EyeHawkEvent(p, Type.FLY, Level.MOD, "Type B"));
                        flyResetTypeB = 0;
                        roasted();
                    } else {
                        flyResetTypeB = System.currentTimeMillis();
                    }
                }
                lastYS = ys;

                if ((getPlayer().getFallDistance() - lastTeleportFalldistance) > 2 && ys != lastYS && ys > -0.5 && airOnly) {
                    // EyeHawk.informDebug("Player " + getPlayer().getName() + " flagged fly Type A (deltaY must be lower
                    // at this falldist.)");
                    if (flyTypeA++ > 5) {
                        Bukkit.getPluginManager().callEvent(new EyeHawkEvent(p, Type.FLY, Level.MOD, "Type A"));
                        flyResetTypeA = 0;
                        roasted();
                    } else {
                        flyResetTypeA = System.currentTimeMillis();
                    }
                }
                resetFly();
            }
        }


        if (xs > 0 || zs > 0) {

            if (getPlayer().getWalkSpeed() < 0.05)
                return;

            double theta = Math.atan2(-move.getX(), move.getZ());
            float yaw = (float) Math.toDegrees((theta + _2PI) % _2PI);
            float playerYaw = getPlayer().getLocation().getYaw();

            if (getPlayer().getVelocity().getY() != ON_GROUND_VEL || ys == JUMP_VEL)
                lastjump = System.currentTimeMillis();
            boolean jumped = currentClientTime - lastjump < 1000;
            boolean solid = getNMSPlayer().world.c(getNMSPlayer().boundingBox.grow(0.5, 0.249D, 0.5).d(0, 0.25, 0));
            boolean ice = currentClientTime - icetime < 2000;

            double mxs = 0;

            if (jumped) {
                if (solid || ice) {
                    if (solid && ice) {
                        mxs = walkSpeed / 0.15;
                    } else {
                        mxs = walkSpeed / 0.25;
                    }
                } else {
                    mxs = walkSpeed / 0.32;
                }
            } else if (solid || ice) {
                if (solid && ice) {
                    mxs = walkSpeed / 0.32;
                } else if (solid) {
                    mxs = walkSpeed / 0.4;
                } else if (ice) {
                    mxs = walkSpeed / 0.57;
                }
            } else {
                mxs = walkSpeed / 0.68;
            }
            if (Math.abs(playerYaw - yaw) >= 120 && Math.abs(playerYaw - yaw) < 270) {
                mxs *= jumped ? 0.7 : 0.8;
            }
            if (currentClientTime - velocitytime < 0) {
                double vel = (Math.abs(latestVelocity.getX()) + Math.abs(latestVelocity.getZ())) * 1.3;
                if (mxs < vel)
                    mxs = vel;
                else if ((vel += Math.abs(latestVelocity.getY())) > mxs)
                    mxs = vel;
            }

            int level = getPotionAmpl(PotionEffectType.SPEED) + 1;

            if (level < lastlevel && currentClientTime - potdecrease > 2000) {
                potdecrease = currentClientTime;
            }

            if (currentClientTime - potdecrease < 2000) {
                level = lastlevel;
            } else {
                lastlevel = level;
            }
            if (level > 0)
                mxs *= (1 + 0.2 * level);

            double comb = mxs * 1.4;

            if (xs + zs > comb || xs > mxs || zs > mxs) {
                // EyeHawk.informDebug(p, ChatColor.GREEN + p.getName() + ChatColor.GRAY + " is using " + ChatColor.RED + "Speed" + ChatColor.GRAY + " Type A ["
                //        + DEC_FORMAT.format(mxs) + "max][" + DEC_FORMAT.format(xs) + "xs][" + DEC_FORMAT.format(zs) + "zs][" + DEC_FORMAT.format(comb) + "comb]");
                if (EyeHawk.getPing(getPlayer()) < 200) {
                    speed();
                }
            }

        }
        lastMove = move;
    }

    /**
     * This checks if the player is falling deep... like REALLY DEEP
     *
     * @return funny value
     */
    public boolean isFallingDeep() {
        return getPlayer().getLocation().getY() < 0;
    }

    public int getPotionAmpl(PotionEffectType type) {
        for (PotionEffect effect : getPlayer().getActivePotionEffects())
            if (effect.getType().equals(type))
                return effect.getAmplifier();
        return -1;
    }

    public void spawnFakeBot(Player toFake) {
        if (fakeEntity != null && toFake.getNoDamageTicks() < 11) {
            fakeEntity.damage(false);
            fakeEntity.swingArm();
            return;
        }
        this.fakeEntityStart = System.currentTimeMillis();
        this.fake = toFake;
        Location spawnLocation = getPlayer().getLocation();
        spawnLocation.add(0, getPlayer().getEyeHeight() * 1.2, 0);
        removeFakeEntities();
        spawnLocation.setPitch(0);
        spawnLocation.subtract(spawnLocation.getDirection().multiply(2));
        spawnLocation.setDirection(toFake.getLocation().getDirection());
        Human human = new Human(spawnLocation, getPlayer(), ((CraftPlayer) toFake).getProfile());
        human.spawn();
        human.setInvisible();
        human.setItemInHand(new ItemStack(Material.STONE_SWORD));
        human.updateArmor(new ItemStack(Material.IRON_BOOTS), new ItemStack(Material.CHAINMAIL_LEGGINGS), new ItemStack(Material.IRON_CHESTPLATE),
                new ItemStack(Material.LEATHER_HELMET));
        human.damage(false);
        human.swingArm();
        this.fakeEntity = human;
    }

    public void moveFakeEntity(Location from, Location to) {
        if (fakeEntity != null) {
            if (getClientTime() - fakeEntityStart > 3_500 || fake == null) {
                removeFakeEntities();
                return;
            }
            to.add(0, getPlayer().getEyeHeight() * 1.2, 0);
            to.setPitch(0);
            Vector mod = to.getDirection();
            to.subtract(rotate(mod, 100).multiply(2.5));

            Location fakeDirection = fake.getLocation();
            fakeEntity.move(fakeDirection.getYaw(), fakeDirection.getPitch(), true, to);
        }
    }

    public Vector rotate(Vector input, double n) {
        Vector rotVec = new Vector();
        double rx = (input.getX() * Math.cos(n)) - (input.getZ() * Math.sin(n));
        double rz = (input.getX() * Math.sin(n)) + (input.getZ() * Math.cos(n));
        rotVec.setX(rx);
        rotVec.setZ(rz);
        return rotVec;
    }

    public double checkAttackDirection(Iterator<LocationTraceEntry> itr) {
        final Location location = getPlayer().getLocation();
        final Vector lookingDirection = location.getDirection();
        double minOff = Double.MAX_VALUE;
        minViolation = Double.MAX_VALUE;
        while (itr.hasNext()) {
            LocationTraceEntry traceLocation = itr.next();
            double off = calcDirectionOffset(location.getX(), location.getY() + getPlayer().getEyeHeight(), location.getZ(), lookingDirection.getX(), lookingDirection.getY(),
                    lookingDirection.getZ(), traceLocation.getX(), traceLocation.getY() + traceLocation.getBoxVertical() / 2, traceLocation.getZ(),
                    traceLocation.getBoxHorizontal() * 2, traceLocation.getBoxVertical());
            minOff = Math.min(off, minOff);
        }
        if (minViolation != Double.MAX_VALUE && minOff > 0.1) {
            directionVL += minViolation;
            if (directionVL >= 7) {
                if (getClientTime() - ignoreDirection > 2000) {
                    Bukkit.getPluginManager().callEvent(new EyeHawkEvent(p, Type.KILLAURA, Level.MOD, "Type D"));
                    ignoreDirection = System.currentTimeMillis();
                }
                directionVL = 0;
            }
        } else {
            directionVL *= 0.8;
        }
        return minOff;
    }

    public double checkReach(Iterator<LocationTraceEntry> itr) {

        final Location pLoc = getPlayer().getLocation();
        double minDist = Double.MAX_VALUE;

        while (itr.hasNext()) {
            LocationTraceEntry trace = itr.next();

            final double distance = NumberConversions.square(trace.getX() - pLoc.getX()) + NumberConversions.square(trace.getZ() - pLoc.getZ());
            minDist = Math.min(minDist, distance);
        }
        if (minDist != Double.MAX_VALUE) {
            groundHit(minDist);
        }
        return minDist;
    }

    private double calcDirectionOffset(final double sourceX, final double sourceY, final double sourceZ, final double dirX, final double dirY, final double dirZ,
                                       final double targetX, final double targetY, final double targetZ, final double targetWidth, final double targetHeight) {
        double dirLength = Math.sqrt(dirX * dirX + dirY * dirY + dirZ * dirZ);
        if (dirLength == 0.0)
            dirLength = 1.0; // Some weird shit happened lawl

        final double dX = targetX - sourceX;
        final double dY = targetY - sourceY;
        final double dZ = targetZ - sourceZ;

        final double targetDist = Math.sqrt(dX * dX + dY * dY + dZ * dZ);

        final double xPrediction = targetDist * dirX / dirLength;
        final double yPrediction = targetDist * dirY / dirLength;
        final double zPrediction = targetDist * dirZ / dirLength;

        double off = 0.0D;

        off += Math.max(Math.abs(dX - xPrediction) - (targetWidth / 2 + PRECISION), 0.0D);
        off += Math.max(Math.abs(dZ - zPrediction) - (targetWidth / 2 + PRECISION), 0.0D);
        off += Math.max(Math.abs(dY - yPrediction) - (targetHeight / 2 + PRECISION), 0.0D);

        if (off >= 0.1) {
            final Vector blockEyes = new Vector(targetX - sourceX, targetY + targetHeight / 2 - sourceY - getPlayer().getEyeHeight(), targetZ - sourceZ);
            final double distance = blockEyes.crossProduct(new Vector(dirX, dirY, dirZ)).length() / dirLength;
            minViolation = Math.min(minViolation, distance);
        }

        return off;
    }

    public EntityPlayer getNMSPlayer() {
        return ((CraftPlayer) getPlayer()).getHandle();
    }

    public Human getReachBot() {
        return reachBot;
    }

    public void spawnReachBot() {
        if (reachBot == null) {
            Location loc = calculateRoundEdgeBox(getPlayer().getLocation());
            reachBot = new Human(loc, getPlayer(), fakeEntityProfile);
            reachBot.setInvisible();
            reachBot.spawn();
            reachTask = EyeHawk.schedule(() -> {
                Location calc = calculateRoundEdgeBox(getPlayer().getLocation());
                if (reachBot != null) {
                    reachBot.move(0, 0, true, calc);
                } else {
                    Bukkit.getScheduler().cancelTask(reachTask);
                }
            }, 1, 1);
        }
    }

    private Location calculateRoundEdgeBox(Location loc) {
        loc.setY(loc.getY() + getPlayer().getEyeHeight() / 2);
        // Calculate hitbox edges horizontal
        float yaw = loc.getYaw();
        if (yaw < 0) {
            yaw += 360;
        }
        int yawSection = ((int) (yaw / 45)) + 1;
        if ((yawSection & 1) == 1) {
            yawSection--;
        }

        float diffYaw = Math.abs(yaw - yawSection * 45);
        double horizontalAddition = 0.35 / Math.cos(Math.toRadians(diffYaw)) - 0.3;

        // Calculate hitbox edges vertical
        float pitch = loc.getPitch() + 90;

        float diffPitch = Math.abs(pitch - 90);
        double verticalAddition = Math.sin(Math.toRadians(diffPitch));
        loc.add(getPlayer().getLocation().getDirection().multiply(3.41 + Math.max(verticalAddition, horizontalAddition)));

        if (diffYaw > 10 && reachBot != null && !reachBot.getLocation().equals(loc)) {
            ignoreReachBot = System.currentTimeMillis();
        }

        return loc;
    }

    public void despawnReachBot() {
        if (this.reachBot != null) {
            Bukkit.getScheduler().cancelTask(reachTask);
            this.reachBot.remove();
            this.reachBot = null;
        }
    }

    public void setLastBrokenBlock(Block lastBrokenBlock) {
        this.lastBrokenBlock = lastBrokenBlock;
    }

    public Block getLastBrokenBlock() {
        return lastBrokenBlock;
    }

    public Location getLastLocationOnGround() {
        return lastLocationOnGround;
    }

    public void setLastLocationOnGround(Location lastLocationOnGround) {
        this.lastLocationOnGround = lastLocationOnGround;
        this.lastTeleportFalldistance = 0;
    }

    public boolean isInventoryOpen() {
        return inventoryOpen;
    }

    public void setInventoryOpen(boolean inventoryOpen) {
        if (inventoryOpen) {
            firstInvWalk = true;
            headMovesInInv = 0;
        }
        this.inventoryOpen = inventoryOpen;
    }
}
