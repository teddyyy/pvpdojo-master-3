/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.eyehawk.types;

import org.bukkit.Bukkit;

public class Lag {

    public static double getTPS() {
        double tps = Bukkit.spigot().getTPS()[0];
        return tps > 20.8 ? 10D : tps;
    }

}
