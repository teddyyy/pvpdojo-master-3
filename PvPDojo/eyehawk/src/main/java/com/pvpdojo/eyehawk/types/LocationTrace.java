/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.eyehawk.types;

import java.util.Iterator;
import java.util.ListIterator;

import org.bukkit.Location;

import com.pvpdojo.eyehawk.EyeHawk;
import com.pvpdojo.util.LimitedQueue;

public class LocationTrace {
    private static final int size = 200;
    private LimitedQueue<LocationTraceEntry> trace = new LimitedQueue<>(size);

    public static class LocationTraceEntry {
        private int time;
        private double x, y, z, boxHorizontal, boxVertical;

        public LocationTraceEntry(Location location, double boxHorizontal, double boxVertical) {
            this.x = location.getX();
            this.y = location.getY();
            this.z = location.getZ();
            this.boxHorizontal = boxHorizontal;
            this.boxVertical = boxVertical;
            this.time = EyeHawk.currentTick();
        }

        public long getTime() {
            return time;
        }

        public double getX() {
            return x;
        }

        public double getY() {
            return y;
        }

        public double getZ() {
            return z;
        }

        public double getBoxHorizontal() {
            return boxHorizontal;
        }

        public double getBoxVertical() {
            return boxVertical;
        }
    }

    public void addEntry(Location location, double boxHorizontal, double boxVertical) {
        if (!trace.isEmpty()) {
            LocationTraceEntry first = trace.getLast();
            if (first.x == location.getX() && first.y == location.getY() && first.x == location.getX() && first.z == location.getX()
                    && first.boxHorizontal == boxHorizontal && first.boxVertical == boxVertical) {
                first.time = EyeHawk.currentTick();
                return;
            }
        }
        trace.add(new LocationTraceEntry(location, boxHorizontal, boxVertical));
    }

    public void reset() {
        trace.clear();
    }

    public Iterator<LocationTraceEntry> maxAgeIterator(int time) {
        ListIterator<LocationTraceEntry> itr = trace.listIterator();
        while (itr.hasNext()) {
            if (itr.next().time >= time) {
                return trace.listIterator(itr.previousIndex());
            }
        }
        return trace.listIterator(0);
    }
}
