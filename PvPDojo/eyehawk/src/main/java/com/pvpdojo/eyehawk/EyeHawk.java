/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.eyehawk;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.player.EyeHawkEvent;
import org.bukkit.event.player.EyeHawkEvent.Level;
import org.bukkit.event.player.EyeHawkEvent.Type;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.ProtocolLibrary;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.eyehawk.types.CheatData;
import com.pvpdojo.eyehawk.types.EyeHawkRecord;
import com.pvpdojo.mysql.SQLBackend;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.Log;

import net.minecraft.server.v1_7_R4.MinecraftServer;

public class EyeHawk extends JavaPlugin {

    public static EyeHawk instance;
    public static final String PREFIX = ChatColor.RED + "AntiSkid > " + ChatColor.GREEN;
    public static final String PREFIX_DEBUG = ChatColor.RED + "AntiSkid Debug > " + ChatColor.GREEN;
    public static final String INTERNAL_DEBUG_PREFIX = "[DEBUG]";
    public static final Random RANDOM = new Random();
    public static boolean AUTO_BAN = true, FAKEPLAYERS = false;
    public static final Set<UUID> QUEUED_BANS = new HashSet<>();
    public static final Queue<EyeHawkRecord> QUEUED_LOGS = new ConcurrentLinkedQueue<>();

    public static int schedule(Runnable task, int delay) {
        return Bukkit.getScheduler().runTaskLater(instance, task, delay).getTaskId();
    }

    public static int schedule(Runnable task, int delay, int period) {
        return Bukkit.getScheduler().runTaskTimer(instance, task, delay, period).getTaskId();
    }

    public static EyeHawk inst() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        loadListeners();
        loadCommands();
    }

    @Override
    public void onDisable() {
        if (!QUEUED_BANS.isEmpty()) {
            QUEUED_BANS.stream().map(PvPDojo::getOfflinePlayer)
                       .forEach(off -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "ban " + off.getName() + " [AntiSkid] Cheat Detection"));
            try {
                Thread.sleep(5 * 1000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    public static int currentTick() {
        return MinecraftServer.currentTick;
    }


    public void executeBans() {
        Iterator<UUID> toBan = new HashSet<>(QUEUED_BANS).iterator();
        PvPDojo.schedule(() -> PvPDojo.newChain().asyncFirst(() -> PvPDojo.getOfflinePlayer(toBan.next()).getName())
                                      .syncLast(name -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "ban " + name + " [AntiSkid] Cheat Detection")).execute())
               .createTimer(3 * 20, QUEUED_BANS.size());
        QUEUED_BANS.clear();
    }

    public static <T> T getRandomFromCollection(Collection<T> coll) {
        int num = RANDOM.nextInt(coll.size());
        for (T t : coll) if (--num < 0) return t;
        throw new AssertionError();
    }

    public static Player getRandomOnlinePlayer() {
        return getRandomFromCollection(Bukkit.getOnlinePlayers());
    }

    private void loadCommands() {
        getCommand("cheat").setExecutor(new CheatCommand());
    }

    int queuedBanExecution = 30;

    private void loadListeners() {
        this.getServer().getMessenger().registerIncomingPluginChannel(this, "LOLIMAHCKER", new CheatListeners());
        ProtocolLibrary.getProtocolManager().addPacketListener(new CheatListeners());
        Bukkit.getPluginManager().registerEvents(new CheatListeners(), this);

        schedule(() -> {
            for (Player p : Bukkit.getOnlinePlayers()) {
                CheatData pd = getData(p);
                pd.setReset(pd.getReset() + 1);
//                if (FAKEPLAYERS && pd.getReset() % 6 == 0) {
//                    ArrayList<Player> players = new ArrayList<>(Bukkit.getOnlinePlayers());
//                    Player getRandomFromCollection = players.get(RANDOM.nextInt(players.size()));
//                    pd.spawnFakeBot(getRandomFromCollection);
//                }
                if (pd.getReset() >= 15) {
                    pd.clearRoasts();
                    pd.setReset(0);
                }
            }

            if (QUEUED_BANS.size() > 0) {
                if (--queuedBanExecution <= 0) {
                    executeBans();
                    queuedBanExecution = 30;
                }
            }

            if (!QUEUED_LOGS.isEmpty()) {
                PvPDojo.schedule(this::saveLogs).createAsyncTask();
            }
        }, 20, 20 * 20);

        schedule(() -> {
            for (Player p : Bukkit.getOnlinePlayers()) {
                CheatData pd = getData(p);
                if ((pd.getCps() / 5) > 15) {
                    Bukkit.getPluginManager().callEvent(new EyeHawkEvent(p, Type.AUTO_CLICKER, Level.MOD, "[" + pd.getCps() / 5 + "cps]"));
                }

                if (pd.getSpeed() >= 4) {

                    Bukkit.getPluginManager().callEvent(new EyeHawkEvent(p, Type.SPEED, Level.MOD, "Type A [" + pd.getSpeed() + "][" + getPing(p) + "ms]"));
                    pd.roasted();
                    if (pd.getSpeed() >= 8) {
                        pd.roasted();
                    }
                    if (pd.getSpeed() >= 12) {
                        pd.roasted();
                    }
                    if (pd.getSpeed() >= 15) {
                        pd.roasted();
                    }
                }
                pd.resetSpeed();

                if ((pd.getCps() / 5) >= 26) {
                    pd.roasted();
                }

                if (pd.getCps() / 5 >= 500) {
                    pd.ban();
                }

                pd.cps = 0;

                if (pd.getHeal() >= 3) {
                    Bukkit.getPluginManager().callEvent(new EyeHawkEvent(p, Type.REGEN, Level.MOD, "Type A"));
                    pd.roasted();
                    if (pd.getHeal() >= 10)
                        pd.roasted();
                    pd.resetHeal();
                }
            }

        }, 20, 5 * 20);
    }

    private void saveLogs() {

        StringBuilder builder = new StringBuilder(70 * QUEUED_LOGS.size());
        builder.append("INSERT INTO eyehawk_logs (uuid, name, message, date, level) VALUES ");

        List<EyeHawkRecord> toInsert = new ArrayList<>();
        Iterator<EyeHawkRecord> itr = QUEUED_LOGS.iterator();
        while (itr.hasNext()) {
            toInsert.add(itr.next());
            itr.remove();
            builder.append("(?,?,?,?,?), ");
        }

        String query = builder.substring(0, builder.length() - 2) + ";";

        try (Connection con = SQLBackend.getInstance().getConnection(); PreparedStatement ps = con.prepareStatement(query)) {
            int i = 1;
            for (EyeHawkRecord record : toInsert) {
                ps.setString(i++, record.getUuid().toString());
                ps.setString(i++, record.getName());
                ps.setString(i++, record.getMessage());
                ps.setTimestamp(i++, new Timestamp(record.getTimeStamp()));
                ps.setString(i++, record.getLevel().name());
            }
            ps.executeUpdate();
        } catch (SQLException e) {
            Log.exception("Saving EyeHawk logs", e);
        }

    }

    public static CheatData getData(Player p) {
        if (p == null) {
            return null;
        }
        return CheatData.cheatdataMap.getUnchecked(p);
    }

    public static int getPing(Player p) {
        return Math.abs(((CraftPlayer) p).getHandle().ping);
    }

    public static void informDebug(Player player, String s) {
        QUEUED_LOGS.add(new EyeHawkRecord(player, INTERNAL_DEBUG_PREFIX + " " + ChatColor.stripColor(s), Level.HAWK));
        inst().getLogger().info("[DEBUG] " + CC.stripColor(s));
        for (Player online : Bukkit.getOnlinePlayers()) {
            if (online.hasPermission("Owner")) {
                CheatData cd = getData(online);
                if (cd.hasDebug()) {
                    online.sendMessage(PREFIX_DEBUG + s);
                }
            }
        }
    }

    public static void informStaff(Player player, String s, Level level) {
        QUEUED_LOGS.add(new EyeHawkRecord(player, ChatColor.stripColor(s), level));
        inst().getLogger().info(CC.stripColor(s));
        for (Player online : Bukkit.getOnlinePlayers()) {
            CheatData data = getData(online);
            if (data.hasAlerts()) {
                Rank rank = User.getUser(online).getRank();
                if ((level == Level.HAWK && rank.inheritsRank(Rank.SERVER_ADMINISTRATOR)
                        || (level == Level.ADMIN && rank.inheritsRank(Rank.DEVELOPER) || (level == Level.MOD && rank.inheritsRank(Rank.TRIAL_MODERATOR))))) {
                    online.sendMessage(PREFIX + s);
                }
            }
        }
    }

    public static boolean calcLegitimacy(List<Long> times, CheatData cd, int length, int violationCount, boolean skip) {
        return calcLegitimacy(times, cd, length, violationCount, skip, BanType.INSTANT);
    }

    public static boolean calcLegitimacy(List<Long> times, CheatData cd, int length, int violationCount, boolean skip, BanType banType) {
        long currentTime = System.currentTimeMillis() / 1000;
        return calcLegitimacy(times, cd, length, violationCount, skip, currentTime, banType);
    }

    public static boolean calcLegitimacy(List<Long> times, CheatData cd, int length, int violationCount, boolean skip, long violationTime, BanType banType) {

        // Don't allow duplicates
        if (skip && times.contains(violationTime)) {
            return false;
        }

        int violations = 0;
        times.add(violationTime);

        Iterator<Long> iterator = times.iterator();
        while (iterator.hasNext()) {
            Long time = iterator.next();
            if (time + (length / 1000) >= violationTime) {
                ++violations;
            } else {
                iterator.remove();
            }
        }

        if (violations >= violationCount) {
            switch (banType) {
                case DELAYED:
                    cd.ban(true);
                    break;
                case INSTANT:
                    cd.ban();
                    break;
                case QUEUE:
                    cd.queueBan();
                    cd.banRandomly();
                    break;
                default:
                    break;

            }
            return true;
        }
        return false;
    }

}
