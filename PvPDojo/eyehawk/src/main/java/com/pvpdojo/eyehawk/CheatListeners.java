/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.eyehawk;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_7_R4.CraftWorld;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent.RegainReason;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.EyeHawkEvent;
import org.bukkit.event.player.EyeHawkEvent.Level;
import org.bukkit.event.player.EyeHawkEvent.Type;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerFreezeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.event.player.PlayerVelocityEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.util.Vector;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListeningWhitelist;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.events.PacketListener;
import com.comphenix.protocol.injector.GamePhase;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.eyehawk.types.CheatData;
import com.pvpdojo.eyehawk.types.Lag;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.TimeUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.PlayerUtil;

import co.aikar.idb.DB;
import co.aikar.idb.DbRow;
import net.minecraft.server.v1_7_R4.AxisAlignedBB;
import net.minecraft.server.v1_7_R4.EnumEntityUseAction;
import net.minecraft.server.v1_7_R4.Item;
import net.minecraft.server.v1_7_R4.ItemStack;
import net.minecraft.server.v1_7_R4.PacketDataSerializer;
import net.minecraft.server.v1_7_R4.PacketPlayInCustomPayload;
import net.minecraft.server.v1_7_R4.PacketPlayInUseEntity;
import net.minecraft.util.io.netty.buffer.Unpooled;

public class CheatListeners implements Listener, PacketListener, PluginMessageListener {

    public static final EnumSet<Material> ALLOWED_GLITCHING = EnumSet.of(Material.CACTUS, Material.FENCE, Material.FENCE_GATE, Material.IRON_FENCE, Material.ACACIA_STAIRS,
            Material.BIRCH_WOOD_STAIRS, Material.BRICK_STAIRS, Material.COBBLESTONE_STAIRS, Material.DARK_OAK_STAIRS, Material.JUNGLE_WOOD_STAIRS, Material.NETHER_BRICK_STAIRS,
            Material.QUARTZ_STAIRS, Material.SANDSTONE_STAIRS, Material.SMOOTH_STAIRS, Material.SPRUCE_WOOD_STAIRS, Material.WOOD_STAIRS, Material.ANVIL, Material.TORCH,
            Material.STAINED_GLASS_PANE, Material.SIGN, Material.SIGN_POST, Material.WALL_SIGN);
    private static final Executor FAKE_ENTITY_THREAD = Executors.newSingleThreadExecutor(new ThreadFactoryBuilder().setNameFormat("Fake Entity Thread - %s").build());
    private final Pattern reducesTheirKB = Pattern.compile("vertical kb \\((\\d+)");
    private final Pattern violationPattern = Pattern.compile("is using (\\w+)|\\[(\\d+)]");

    @EventHandler
    public void onSkid(EyeHawkEvent e) {
        CheatData cd = EyeHawk.getData(e.getPlayer());
        if (e.getType() == Type.ANTI_KB && e.getMessage().contains("horizontal kb") && cd.getClientTime() - cd.lastBlockPlace < 1000) {
            return;
        }

        if (!e.getMessage().contains("Type J") && !e.getMessage().contains("Type K") && !e.getMessage().contains("Type H") && !e.getMessage().contains("Type I")
                && e.getType() != EyeHawkEvent.Type.DEBUG) {
            EyeHawk.informStaff(e.getPlayer(),
                    CC.GREEN + e.getPlayer().getName() + CC.GRAY + " is using " + CC.RED + StringUtils.getEnumName(e.getType()) + CC.GRAY + " " + e.getMessage(), e.getLevel());
        } else {
            EyeHawk.informDebug(e.getPlayer(),
                    CC.GREEN + e.getPlayer().getName() + CC.GRAY + " is using " + CC.RED + StringUtils.getEnumName(e.getType()) + CC.GRAY + " " + e.getMessage());
        }

        updateCheatData(cd, e.getMessage(), e.getType(), System.currentTimeMillis() / 1000);
    }

    public void updateCheatData(CheatData cd, String message, Type type, long time) {

        if (type == EyeHawkEvent.Type.KILLAURA) {
            if (message.contains("Type C")) {
                // int lvl = Integer.parseInt(message.substring(message.length() - 1));
                // if (lvl <= 7) {
                // List<Long> times = cd.getKillauraTypeC();
                // int violations = 0;
                // Long currentTime = System.currentTimeMillis();
                // times.add(currentTime);
                // for (Iterator<Long> iterator = times.iterator(); iterator.hasNext();) {
                // Long time = iterator.next();
                // if (time + 60 * 5 * 1000 >= currentTime) {
                // ++violations;
                // } else {
                // iterator.remove();
                // }
                // }
                // if (((lvl == 1 || (lvl >= 4 && lvl <= 7)) && violations == 7) || ((lvl == 2 || lvl == 3) && violations == 8)) {
                // cd.ban(true);
                // }
                // }
            } else if (message.contains("Type E2")) {
                EyeHawk.calcLegitimacy(cd.getKillauraTypeE2(), cd, 60 * 1000, 3, false, time, BanType.INSTANT);
            } else if (message.contains("Type H")) {
                // String[] parts = message.split(" \\| ");
                // parts = parts[1].split(" ");
                // for (String part : parts) {
                // int val = Integer.parseInt(part.replace(" ", ""));
                // // Verify they aren't lagging
                // if (val > 2) {
                // cd.getKillauraTypeH().clear();
                // cd.getKillauraTypeHLong().clear();
                // cd.ignoreHI = System.currentTimeMillis();
                // } else if (cd.ignoreHI + 5000 < System.currentTimeMillis()) {
                // // Check that they weren't lagging recently
                // if (pd.getSession() != null && pd.getSession().isRanked()) {
                // if (!EyeHawk.calcLegitimacy(cd.getKillauraTypeH(), cd, 60 * 10 * 1000, 10, true, BanType.QUEUE)) {
                // EyeHawk.calcLegitimacy(cd.getKillauraTypeHLong(), cd, 60 * 60 * 1000, 30, true, BanType.QUEUE);
                // }
                // }
                // }
                // }
            } else if (message.contains("Type I")) {
                // String[] parts = message.split(" \\| ");
                // parts = parts[1].split(" ");
                // for (String part : parts) {
                // int val = Integer.parseInt(part.replace(" ", ""));
                // // Verify they aren't lagging
                // if (val > 2) {
                // cd.getKillauraTypeI().clear();
                // cd.getKillauraTypeILong().clear();
                // cd.ignoreHI = System.currentTimeMillis();
                // } else if (cd.ignoreHI + 5000 < System.currentTimeMillis()) {
                // // Check that they weren't lagging recently
                // if (pd.getSession() != null && pd.getSession().isRanked()) {
                // if (!EyeHawk.calcLegitimacy(cd.getKillauraTypeI(), cd, 60 * 10 * 1000, 10, true, BanType.QUEUE)) {
                // EyeHawk.calcLegitimacy(cd.getKillauraTypeILong(), cd, 60 * 60 * 1000, 30, true, BanType.QUEUE);
                // }
                // }
                // }
                // }
            } else if (message.contains("Type J") || message.contains("Type K")) {
                // Currently no action
            } else if (message.contains("Type U")) {
                EyeHawk.calcLegitimacy(cd.getKillauraTypeU(), cd, 60 * 1000, 6, false, time, BanType.INSTANT);
            } else if (message.contains("Type F")) {
                if (!cd.getLastTypeFMessage().equals(message)) {
                    EyeHawk.calcLegitimacy(cd.getKillauraTypeF(), cd, 60 * 60 * 1000, 3, false, time, BanType.QUEUE);
                    cd.setLastTypeFMessage(message);
                }
            } else if (message.contains("Type Z")) {
                EyeHawk.calcLegitimacy(cd.getKillauraTypeZ(), cd, 60 * 1000, 2, true, time, BanType.INSTANT);
            }
        } else if (type == EyeHawkEvent.Type.REGEN) {
            if (message.contains("Type B")) {
                cd.ban();
            }
        } else if (type == EyeHawkEvent.Type.CRIT) {
            if (message.contains("Type A")) {
                EyeHawk.calcLegitimacy(cd.getMiniJumps(), cd, 30000, 5, false, time, BanType.INSTANT);
            } else if (message.contains("Type B")) {
                EyeHawk.calcLegitimacy(cd.getCriticalsTypeB(), cd, 60000, 6, false, time, BanType.INSTANT);
            }
        } else if (type == EyeHawkEvent.Type.ANTI_KB) {
            Matcher matcher = reducesTheirKB.matcher(message);
            if (matcher.find()) {
                int amount = Integer.parseInt(matcher.group(1));
                // note this is initial velocity - taking 60% initial velocity makes you travel much less further
                // and is obvious to anyone watching
                if (amount <= 70) {
                    EyeHawk.calcLegitimacy(cd.getReducedKB(), cd, 2 * 60000, 7, false, time, BanType.DELAYED);
                } else if (amount <= 96) {
                    EyeHawk.calcLegitimacy(cd.getReducedKB(), cd, 2 * 60000, 7, false, time, BanType.QUEUE);
                }
            } else if (message.contains("horizonal kb")) {
                EyeHawk.calcLegitimacy(cd.getReducedKBHorizontal(), cd, 5 * 60000, 7, true, time, BanType.QUEUE);
            } else if (message.contains("Type A")) {
                EyeHawk.calcLegitimacy(cd.getAntiKB(), cd, 60000, 4, false, time, BanType.INSTANT);
            }
        } else if (type == EyeHawkEvent.Type.INVALID_PACKET) {
            if (message.contains("Type A")) {
                if (message.endsWith("(PacketPlayInArmAnimation)") || message.endsWith("(PacketPlayInEntityAction)")
                        || message.endsWith("(PacketPlayInUseEntity)")) {
                    EyeHawk.calcLegitimacy(cd.getBadPackets(), cd, 60 * 1000, 2, false, BanType.INSTANT);
                }
            }
        } else if (type == Type.TIMER) {
            EyeHawk.calcLegitimacy(cd.getTimerTypeG(), cd, 4 * 60 * 1000, 20, true, BanType.INSTANT);
        } else if (type == Type.INVENTORY) {
            if (message.contains("Type A")) {
                // EyeHawk.calcLegitimacy(cd.getInventoryTypeA(), cd, 10 * 60 * 1000, 6, false, BanType.QUEUE);
            } else if (message.contains("Type B")) {
                EyeHawk.calcLegitimacy(cd.getInventoryTypeB(), cd, 10 * 60 * 1000, 3, true, BanType.DELAYED);
            } else if (message.contains("Type C")) {
                EyeHawk.calcLegitimacy(cd.getInventoryTypeC(), cd, 7 * 60 * 1000, 7, false, BanType.INSTANT);
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        e.getPlayer().sendMessage("§8 §8 §1 §3 §3 §7 §8 ");

        CheatData cd = EyeHawk.getData(e.getPlayer());

        if (PlayerUtil.hasAutism(e.getPlayer())) {
            cd.setAlerts(true);
        }

        PvPDojo.schedule(() -> {
            try {
                boolean hadKillauraTypeG = false;
                List<DbRow> rows = DB.getResults("SELECT message, date FROM " + Tables.EYEHAWK + " WHERE uuid = ? AND date >= DATE_SUB(NOW(), INTERVAL 10 MINUTE) ORDER BY date DESC", e.getPlayer().getUniqueId().toString());
                for (DbRow row : rows) {
                    String message = row.get("message");

                    if (message.contains("Detected")) {
                        break;
                    }

                    if (message.contains("Killaura Type G")) {
                        if (hadKillauraTypeG) {
                            continue;
                        }
                        hadKillauraTypeG = true;
                    }

                    Matcher matcher = violationPattern.matcher(message);
                    if (matcher.find()) {
                        Type type = StringUtils.getEnumFromString(matcher.group(1), Type.class);
                        int amount = matcher.find() ? Integer.valueOf(matcher.group(2)) : 1;
                        Timestamp timestamp = row.get("date");
                        PvPDojo.schedule(() -> {
                            if (type == Type.KILLAURA || type == Type.ANTI_KB) {
                                for (int i = 0; i < amount; i++) {
                                    updateCheatData(cd, message, type, timestamp.getTime() / 1000);
                                }
                            }
                        }).sync();
                    }
                }
            } catch (SQLException ex) {
                Log.exception("EyeHawk violation persistence", ex);
            }
        }).createAsyncTask();

    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        CheatData cd = EyeHawk.getData(e.getPlayer());
        cd.removeFakeEntities();
        cd.despawnReachBot();
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onBlockDamage(BlockDamageEvent e) {
        CheatData cd = EyeHawk.getData(e.getPlayer());
        cd.cps--;
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onBlockBreak(BlockBreakEvent e) {
        CheatData cd = EyeHawk.getData(e.getPlayer());
        cd.flyTypeF--;

        if (e.isCancelled()) {
            Block b = e.getBlock();
            if ((b.getType().isSolid() || b.getType() == Material.GLASS || b.getType() == Material.STAINED_GLASS || b.getType() == Material.GLOWSTONE)
                    && !ALLOWED_GLITCHING.contains(b.getType())) {
                cd.setLastBrokenBlock(b);
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onBlockPlace(BlockPlaceEvent e) {
        Player p = e.getPlayer();
        CheatData cd = EyeHawk.getData(p);
        if (e.isCancelled() || !e.canBuild()) {
            cd.lastBlockPlace = System.currentTimeMillis();
            Block b = e.getBlockPlaced();
            AxisAlignedBB playerbox = ((CraftPlayer) p).getHandle().boundingBox;
            AxisAlignedBB blockbox = AxisAlignedBB.a(b.getX(), b.getY(), b.getZ(), b.getX() + 1, b.getY() + 2, b.getZ() + 1);
            if (playerbox.b(blockbox)) {
                if (cd.getLastLocationOnGround().getWorld() == p.getWorld() && cd.getLastLocationOnGround().distanceSquared(p.getLocation()) < 36) {
                    p.teleport(cd.getLastLocationOnGround());
                    User.getUser(p).sendOverlay(MessageKeys.BLOCK_GLITCHING_NOT_ALLOWED);
                }
            }
        } else {
            if (cd.getLastLocationOnGround().getY() < e.getBlock().getRelative(BlockFace.UP).getY()) {
                cd.setLastLocationOnGround(e.getBlock().getRelative(BlockFace.UP).getLocation());
            }
        }
    }

    @EventHandler
    public void onPlayerFreeze(PlayerFreezeEvent e) {
        if (e.isFrozen()) {
            EyeHawk.getData(e.getPlayer()).spawnReachBot();
        } else {
            EyeHawk.getData(e.getPlayer()).despawnReachBot();
        }
    }

    @EventHandler
    public void onToggleFlight(PlayerToggleFlightEvent e) {
        Player p = e.getPlayer();
        CheatData data = EyeHawk.getData(p);
        data.tptime = System.currentTimeMillis() + 8000;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onTeleport(PlayerTeleportEvent e) {
        CheatData data = EyeHawk.getData(e.getPlayer());
        data.lastTeleport = System.currentTimeMillis() + 1000;
        if (EyeHawk.FAKEPLAYERS) {
            FAKE_ENTITY_THREAD.execute(data::removeFakeEntities);
        }
        data.setLastLocationOnGround(e.getTo());
        data.lastTeleportFalldistance = e.getPlayer().getFallDistance();
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onClick(PlayerInteractEvent e) {

        Player p = e.getPlayer();
        CheatData pd = EyeHawk.getData(p);

        if (Lag.getTPS() < 19 || pd.getRecentLag() > 2)
            return;

        if (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK) {
            if (++pd.cps > 500) {
                pd.cps = -1000; // Player is will be banned let's reduce violation spam
                EyeHawk.informStaff(p, p.getName() + " has more than 100 CPS", Level.MOD);
                pd.ban();
            }
        }

    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {
            Player p = (Player) e.getEntity();
            CheatData pd = EyeHawk.getData(p);
            if (e.getCause() == DamageCause.BLOCK_EXPLOSION || e.getCause() == DamageCause.ENTITY_EXPLOSION) {
                pd.tptime = System.currentTimeMillis() + 5000;
            }
        }
    }

    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.MONITOR)
    public void onMove(PlayerMoveEvent e) {

        Player p = e.getPlayer();
        CheatData pd = EyeHawk.getData(p);
        Block b = pd.getLastBrokenBlock();
        AxisAlignedBB playerbox = NMSUtils.get(p).boundingBox;

        if (b != null && b.getType().isSolid() && !ALLOWED_GLITCHING.contains(b.getType())) {
            AxisAlignedBB blockbox = NMSUtils.getBlockBox(b);

            if (playerbox.b(blockbox) && pd.getLastLocationOnGround().getWorld() == e.getTo().getWorld() && pd.getLastLocationOnGround().distanceSquared(e.getTo()) < 36) {
                Location tele = pd.getLastLocationOnGround();
                tele.setYaw(e.getTo().getYaw());
                tele.setPitch(e.getTo().getPitch());
                p.teleport(tele);
                User.getUser(p).sendOverlay(MessageKeys.BLOCK_GLITCHING_NOT_ALLOWED);
            }
            return;
        }

        if (p.isOnGround() || ((CraftWorld) p.getWorld()).getHandle().b(playerbox, net.minecraft.server.v1_7_R4.Material.WEB) || ((CraftPlayer) p).getHandle().M()
                || ((CraftWorld) p.getWorld()).getHandle().b(playerbox, net.minecraft.server.v1_7_R4.Material.LAVA) || ((CraftPlayer) p).getHandle().h_()) {
            pd.setLastLocationOnGround(e.getFrom());
        }

        if (!e.isCancelled()) {
            pd.getLocationTrace().addEntry(e.getTo().clone(), 0.3, 1.8);
            if (EyeHawk.FAKEPLAYERS) {
                FAKE_ENTITY_THREAD.execute(() -> pd.moveFakeEntity(e.getFrom().clone(), e.getTo().clone()));
            }
        }

        if (Lag.getTPS() < 9)
            return;

        if (e.isCancelled() || p.isInsideVehicle() || p.isDead() || p.isFrozen()) {
            return;
        }

        Material down = e.getTo().getBlock().getRelative(BlockFace.DOWN).getType();
        if (down == Material.ICE || down == Material.PACKED_ICE) {
            pd.icetime = System.currentTimeMillis();
        }

        pd.runMoveChecks(Math.abs(e.getTo().getX() - e.getFrom().getX()), Math.abs(e.getTo().getZ() - e.getFrom().getZ()), e.getTo().getY() - e.getFrom().getY(),
                e.getFrom().clone(), e.getTo().clone());

    }

    @EventHandler
    public void onPistonExtend(BlockPistonExtendEvent e) {
        for (Entity entity : BukkitUtil.getNearbyEntities(e.getBlock().getRelative(e.getDirection(), e.getLength() + 1).getLocation(), 1.5, 2, 1.5)) {
            if (entity instanceof Player) {
                EyeHawk.getData((Player) entity).tptime = System.currentTimeMillis() + 4000;
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInventoryOpen(InventoryOpenEvent e) {
        CheatData cd = EyeHawk.getData((Player) e.getPlayer());
        cd.setInventoryOpen(true);
        cd.invMoveVL = 0;
        if (EyeHawk.FAKEPLAYERS) {
            cd.spawnFakeBot(EyeHawk.getRandomOnlinePlayer());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onInventoryClose(InventoryCloseEvent e) {
        CheatData cd = EyeHawk.getData((Player) e.getPlayer());
        cd.setInventoryOpen(false);
        cd.removeFakeEntities();
        cd.ignoreInvChecks = System.currentTimeMillis() + 2000; // 2 secs of grace period
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInvWalkClick(InventoryClickEvent e) {
        CheatData cd = EyeHawk.getData((Player) e.getWhoClicked());

        if (Lag.getTPS() < 15) {
            return;
        }

        if (e.getWhoClicked().getGameMode() != GameMode.CREATIVE && !cd.isInventoryOpen() && cd.getClientTime() - cd.ignoreInvChecks > 0 && EyeHawk.getPing(cd.getPlayer()) < 200) {
            Bukkit.getPluginManager().callEvent(new EyeHawkEvent((Player) e.getWhoClicked(), Type.INVENTORY, Level.MOD, "Type C"));
        }

    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onClick(InventoryClickEvent e) {

        if ((e.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY || e.getAction() == InventoryAction.NOTHING)
                && e.getWhoClicked().getOpenInventory().getType() == InventoryType.CRAFTING && e.getRawSlot() < 36 && e.getCursor().getType() == Material.AIR
                && !isFilledHotbar((Player) e.getWhoClicked())) {
            CheatData cd = EyeHawk.getData((Player) e.getWhoClicked());

            if (System.currentTimeMillis() - cd.firstClick > 2000) {
                cd.firstClick = cd.getClientTime();
                cd.getSlots().clear();
            }
            cd.getSlots().add(e.getSlot());
            if (cd.getSlots().size() >= 8) {
                long timeDiff = System.currentTimeMillis() - cd.firstClick;
                if (timeDiff < 450 && System.currentTimeMillis() - cd.firstClick > 110) {
                    Set<Integer> withOutDuplicate = new HashSet<>(cd.getSlots());
                    if (cd.getSlots().size() == withOutDuplicate.size()) { // We got a violation
                        if (timeDiff < 350) {
                            Bukkit.getPluginManager().callEvent(new EyeHawkEvent((Player) e.getWhoClicked(), Type.INVENTORY, Level.ADMIN, "Type A [" + timeDiff + "ms]"));
                        } else {
                            EyeHawk.informDebug((Player) e.getWhoClicked(),
                                    e.getWhoClicked().getName() + " is using a refill modification [Refill speed: " + timeDiff + "ms] v2");
                        }
                    }
                }
                cd.getSlots().clear();
                cd.firstClick = -1;
            }
        }
    }

    private boolean isFilledHotbar(Player player) {
        for (int i = 0; i < 9; i++) {
            if (player.getInventory().getItem(i) == null) {
                return false;
            }
        }
        return true;
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onVelocity(PlayerVelocityEvent e) {

        CheatData pd = EyeHawk.getData(e.getPlayer());
        Vector v = e.getVelocity().clone();
        pd.velocitytime = ((Math.abs(v.getX()) + Math.abs(v.getZ())) > 1.1) ? System.currentTimeMillis() + 3900 : System.currentTimeMillis() + 1600;
        pd.setLatestVelocity(v.multiply(1.2));
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onFastHeal(EntityRegainHealthEvent e) {

        if (!(e.getEntity() instanceof Player) || e.getRegainReason() != RegainReason.SATIATED)
            return;

        Player p = (Player) e.getEntity();
        CheatData pd = EyeHawk.getData(p);

        if ((System.currentTimeMillis() - pd.getLastHeal()) < 3500) {
            if (Lag.getTPS() < 19 || pd.getRecentLag() > 2)
                return;
            pd.unlegitheal();
        }

        pd.heal();
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onBucketEmpty(PlayerBucketEmptyEvent e) {
        if (e.isCancelled()) {
            CheatData cd = EyeHawk.getData(e.getPlayer());
            cd.hadJumpBoost = cd.getClientTime();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onHit(EntityDamageByEntityEvent e) {

        if (!(e.getDamager() instanceof Player))
            return;

        Player p = (Player) e.getDamager();
        CheatData cd = EyeHawk.getData(p);

        if (p.getItemInHand() != null && (p.getItemInHand().getType() == Material.BOOK_AND_QUILL || p.getItemInHand().getType() == Material.WRITTEN_BOOK)) {
            if (p.getItemInHand().getEnchantments().size() > 0) {
                EyeHawk.informStaff(p, CC.GREEN + p.getName() + CC.GRAY + " used a " + CC.RED + "Hacked Client " + CC.GRAY + "Type A (NBTGlitch)", Level.MOD);
                for (Enchantment ench : p.getItemInHand().getEnchantments().keySet()) {
                    p.getItemInHand().removeEnchantment(ench);
                }
                cd.ban();
                e.setCancelled(true);
            }
        }

        if (p.getGameMode() == GameMode.CREATIVE || Lag.getTPS() < 9 || System.currentTimeMillis() - cd.lastTeleport < 2000)
            return;
        Entity t = e.getEntity();

        if (t instanceof Player && t.getVehicle() == null && p.getVehicle() == null) {
            Player tp = (Player) t;
            cd.setLastTarget(tp);
            if (tp.getAllowFlight()) {
                return;
            }
            CheatData td = EyeHawk.getData(tp);
            td.getLocationTrace().addEntry(tp.getLocation(), 0.3, 1.8);

            int delay = (5 + cd.getRecentLag() + (EyeHawk.getPing(p) / 50));
            if (!TimeUtil.hasPassed(td.velocitytime - 1600, TimeUnit.MILLISECONDS, 200)) {
                delay += 5;
            }
            double off = 0;
            if (!NMSUtils.get(tp).boundingBox.b(NMSUtils.get(p).boundingBox)) {
                off = cd.checkAttackDirection(td.getLocationTrace().maxAgeIterator(EyeHawk.currentTick() - delay));
            }
            double reach = cd.checkReach(td.getLocationTrace().maxAgeIterator(EyeHawk.currentTick() - delay));
            if (EyeHawk.FAKEPLAYERS && (reach > 15 || off > 0.1)) {
                cd.spawnFakeBot(EyeHawk.getRandomOnlinePlayer());
            }
            if (cd.isRandomBan() && Math.random() < 1D / 300D) {
                cd.ban();
            }
        }
    }

    @Override
    public Plugin getPlugin() {
        return EyeHawk.inst();
    }

    @Override
    public ListeningWhitelist getReceivingWhitelist() {
        return ListeningWhitelist.newBuilder().monitor().gamePhase(GamePhase.PLAYING).types(PacketType.Play.Client.USE_ENTITY, PacketType.Play.Client.CUSTOM_PAYLOAD).build();
    }

    @Override
    public ListeningWhitelist getSendingWhitelist() {
        return null;
    }

    @Override
    public void onPacketReceiving(PacketEvent event) {
        Player player = event.getPlayer();

        if (player == null || !player.isOnline()) {
            return;
        }

        if (event.getPacketType() == PacketType.Play.Client.USE_ENTITY) {
            PacketContainer packet = event.getPacket();
            PacketPlayInUseEntity handle = (PacketPlayInUseEntity) packet.getHandle();
            if (handle.c() != EnumEntityUseAction.ATTACK)
                return;
            CheatData data = EyeHawk.getData(player);

            if (player.getGameMode() != GameMode.CREATIVE) {
                if (data.getReachBot() != null && packet.getIntegers().getValues().get(0) == data.getReachBot().getHuman().getId()) {
                    data.despawnReachBot();
                    if (TimeUtil.hasPassed(data.ignoreReachBot, TimeUnit.SECONDS, 1)) {
                        Bukkit.getPluginManager().callEvent(new EyeHawkEvent(player, Type.KILLAURA, Level.HAWK,
                                "Type F (yaw: " + player.getLocation().getYaw() + ", pitch: " + player.getLocation().getPitch() + ")"));
                    }
                    data.ignoreReachBot = 0;
                    EyeHawk.schedule(data::spawnReachBot, 0);
                }
                if (EyeHawk.getPing(player) <= 210 && data.getFakeEntityId() == packet.getIntegers().getValues().get(0)) {
                    data.attackBot();
                }
            }
        } else if (event.getPacketType() == PacketType.Play.Client.CUSTOM_PAYLOAD) {
            PacketPlayInCustomPayload payLoad = (PacketPlayInCustomPayload) event.getPacket().getHandle();
            if (payLoad.c().equals("MC|BEdit") || payLoad.c().equals("MC|BSign")) {
                ItemStack item = new PacketDataSerializer(Unpooled.wrappedBuffer(payLoad.e()), NMSUtils.get(player).playerConnection.networkManager.getVersion()).c();
                if ((Item.getId(item.getItem()) == 386 || Item.getId(item.getItem()) == 387) && item.getEnchantments().size() > 0) {
                    EyeHawk.informStaff(player,
                            CC.GREEN + player.getName() + CC.GRAY + " is using a " + CC.RED + "Hacked Client " + CC.GRAY + "Type A (NBTGlitch)", Level.MOD);
                    EyeHawk.getData(player).ban();
                }
            }
        }
    }

    @Override
    public void onPacketSending(PacketEvent event) {}

    @Override
    public void onPluginMessageReceived(String ch, Player p, byte[] data) {
        String str;
        try {
            str = new String(data);
        } catch (Exception ex) {
            str = "";
        }
        EyeHawk.informStaff(p, CC.GREEN + p.getName() + CC.GRAY + " logged in with a " + CC.RED + "Hacked Client " + CC.GRAY + "Type E (" + str + ")", Level.ADMIN);
        EyeHawk.getData(p).banRandomly();
    }

}
