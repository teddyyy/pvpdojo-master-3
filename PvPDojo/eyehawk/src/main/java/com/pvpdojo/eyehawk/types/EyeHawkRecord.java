/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.eyehawk.types;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.player.EyeHawkEvent.Level;

public class EyeHawkRecord {

    private UUID uuid;
    private String name, message;
    private long timeStamp;
    private Level level;

    public EyeHawkRecord(Player player, String message, Level level) {
        this.timeStamp = System.currentTimeMillis();
        this.uuid = player.getUniqueId();
        this.name = player.getName();
        this.message = message;
        this.level = level;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public Level getLevel() {
        return level;
    }

}
