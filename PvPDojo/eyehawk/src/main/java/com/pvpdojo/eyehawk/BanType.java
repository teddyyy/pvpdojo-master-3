/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.eyehawk;

public enum BanType {
    INSTANT,
    DELAYED,
    QUEUE
}
