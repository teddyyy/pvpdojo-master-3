/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.eyehawk;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.time.DateTimeException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collections;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.player.EyeHawkEvent.Level;
import org.eclipse.egit.github.core.Gist;
import org.eclipse.egit.github.core.GistFile;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.eyehawk.types.CheatData;
import com.pvpdojo.eyehawk.types.Lag;
import com.pvpdojo.mysql.SQLBuilder;

import net.minecraft.server.v1_7_R4.EntityPlayer;
import net.minecraft.server.v1_7_R4.PacketPlayOutGameStateChange;

public class CheatCommand implements CommandExecutor {

    static final DecimalFormat TPS_FORMAT = new DecimalFormat("#0.000");

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!sender.hasPermission("Owner") && !sender.hasPermission("Moderator") && !sender.hasPermission("Supporter"))
            return true;

        if (args.length == 1) {

            if (args[0].equalsIgnoreCase("lag")) {

                double tps = Lag.getTPS();
                sender.sendMessage("§aTps: " + TPS_FORMAT.format(tps));
                return true;
            }

            if (args[0].equalsIgnoreCase("toggle") && sender instanceof Player) {
                CheatData data = EyeHawk.getData((Player) sender);
                data.setAlerts(!data.hasAlerts());
                sender.sendMessage(EyeHawk.PREFIX + "§aAlerts: " + data.hasAlerts());
                return true;
            }

            if (args[0].equalsIgnoreCase("debug") && sender instanceof Player) {
                CheatData data = EyeHawk.getData((Player) sender);
                data.setDebug(!data.hasDebug());
                sender.sendMessage(EyeHawk.PREFIX + "§aAlerts debug: " + data.hasDebug());
                return true;
            }

            if (args[0].equalsIgnoreCase("ban")) {
                EyeHawk.AUTO_BAN = !EyeHawk.AUTO_BAN;
                sender.sendMessage(EyeHawk.PREFIX + "§aBanning: " + EyeHawk.AUTO_BAN);
                return true;
            }

            if (args[0].equalsIgnoreCase("fakeplayer")) {
                EyeHawk.FAKEPLAYERS = !EyeHawk.FAKEPLAYERS;
                sender.sendMessage(EyeHawk.PREFIX + "§aFakeplayers: " + EyeHawk.FAKEPLAYERS);
                return true;
            }

            PvPDojo.schedule(() -> {
                boolean showDebug = sender instanceof Player && EyeHawk.getData((Player) sender).hasDebug();
                OfflinePlayer target = PvPDojo.getOfflinePlayer(args[0]);
                StringBuilder builder = new StringBuilder();
                try (SQLBuilder sql = new SQLBuilder()) {
                    sql.setStatement("SELECT * FROM eyehawk_logs WHERE uuid = '" + target.getUniqueId().toString() + "'");
                    sql.executeQuery();
                    while (sql.next()) {
                        try {
                            LocalDateTime date = Instant.ofEpochMilli(sql.getTimestamp(4).getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
                            if (Level.valueOf(sql.getString(5)) == Level.ADMIN && !sender.hasPermission("Supporter")) {
                                continue;
                            }
                            String message = sql.getString(3);
                            if (message.startsWith(EyeHawk.INTERNAL_DEBUG_PREFIX) && !showDebug) {
                                continue;
                            }
                            String formattedDate = date.format(DateTimeFormatter.ofPattern("uuuu-MM-d"));
                            String formattedTime = date.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
                            builder.append("[").append(formattedDate);
                            builder.append(" ").append(formattedTime).append("] ");
                            builder.append(message);
                            builder.append("\n");
                        } catch (DateTimeException e) {
                            e.printStackTrace();
                        }

                    }
                } catch (SQLException e) {
                    sender.sendMessage("§cDatabase error");
                    e.printStackTrace();
                    return;
                }

                if (builder.length() == 0) {
                    sender.sendMessage("§cNo violations found");
                    return;
                }
                sender.sendMessage("§aUploading to gist...");
                Gist gist = new Gist().setDescription("Violations of " + target.getName());
                GistFile file = new GistFile().setContent(builder.substring(0, builder.length() - 1));
                gist.setFiles(Collections.singletonMap("EyeHawk#" + EyeHawk.RANDOM.nextInt(1000) + ".log", file));
                try {
                    gist = PvPDojo.GIST_SERVICE.createGist(gist);
                    sender.sendMessage(EyeHawk.PREFIX + "Violations of all time: " + gist.getHtmlUrl());
                    final Gist fgist = gist;
                    EyeHawk.schedule(() -> {
                        PvPDojo.schedule(() -> {
                            try {
                                PvPDojo.GIST_SERVICE.deleteGist(fgist.getId());
                                sender.sendMessage("§aDeleted requested log");
                            } catch (IOException e) {
                                sender.sendMessage("§cFailed to delete gist.");
                                e.printStackTrace();
                            }
                        }).createAsyncTask();
                    }, 5 * 60 * 20);

                } catch (IOException e) {
                    sender.sendMessage("§cFailed upload to gist!");
                    e.printStackTrace();
                }
            }).createAsyncTask();

            return true;
        } else if (args.length == 2 && args[0].equalsIgnoreCase("demo")) {

            if (Bukkit.getPlayer(args[1]) == null)
                return false;

            Player t = Bukkit.getPlayer(args[1]);
            EntityPlayer ep = ((CraftPlayer) t).getHandle();

            PacketPlayOutGameStateChange packet = new PacketPlayOutGameStateChange(5, 0);
            ep.playerConnection.sendPacket(packet);

        } else if (args.length == 2 && args[0].equalsIgnoreCase("credits")) {
            if (Bukkit.getPlayer(args[1]) == null)
                return false;

            Player t = Bukkit.getPlayer(args[1]);
            EntityPlayer ep = ((CraftPlayer) t).getHandle();

            PacketPlayOutGameStateChange packet = new PacketPlayOutGameStateChange(4, 0);
            ep.playerConnection.sendPacket(packet);
        }

        return false;
    }
}
