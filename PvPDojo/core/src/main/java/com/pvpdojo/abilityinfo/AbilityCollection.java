/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilityinfo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionType;

import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.PotionBuilder;

public enum AbilityCollection {

    NONE(new ItemBuilder(Material.CHEST).name(CC.BLUE + "All Abilities Collection").build(), false, -7),
    SAMURAI(new ItemBuilder(Material.DIAMOND_CHESTPLATE).name(CC.BLUE + "Samurai Collection").build(), true, -1),
    SORCERER(new ItemBuilder(Material.BOOK).name(CC.DARK_PURPLE + "Sorcerer Collection").build(), true, -2),
    MILITARY(new ItemBuilder(Material.TNT).name(CC.DARK_GRAY + "Military Collection").build(), false, -3),
    // BEAST(new DojoItemStack(Material.MONSTER_EGG,
    // CC.DARK_GREEN + "Beast Collection"), false, -4),
    DIVINITY(new ItemBuilder(Material.EMERALD).name(CC.RED + "Divinity Collection").build(), false, -5),
    BRAVERY(new ItemBuilder(Material.GOLDEN_APPLE).name(CC.GOLD + "Bravery Collection").build(), false, -6),
    BEAM(new ItemBuilder(Material.BEACON).name(CC.LIGHT_PURPLE + "Beam Collection").build(), false, -8),
    SUMMONER(new ItemBuilder(Material.BONE).name(CC.DARK_AQUA + "Summoner Collection").build(), false, -9),
    POTTER(new PotionBuilder(PotionType.INSTANT_HEAL).splash().name(CC.AQUA + "Potter Collection").build(), false, -10),
    SUPERHERO(new ItemBuilder(Material.DIAMOND).name(CC.GREEN + "Superhero Collection").build(), false, -11),
    DISASTER(new ItemBuilder(Material.RECORD_11).name(CC.DARK_GRAY + "Disaster Collection").build(), false, -12),
    OLDSCHOOL(new ItemBuilder(Material.COBBLESTONE).name(CC.AQUA + "Old School Collection").build(), false, -13),
    BENDER(new ItemBuilder(Material.QUARTZ_BLOCK).durability((byte)1).name(CC.GOLD + "Bender Collection").build(), false, -14);

    private ItemStack item;
    private int id;

    AbilityCollection(ItemStack item, boolean starter, int id) {
        this.item = item;
        this.id = id;
    }

    public ItemStack getItem() {
        return item;
    }

    public int getId() {
        return id;
    }

    public static ArrayList<AbilityLoader> getAbilitiesInCollection(AbilityCollection collection) {
        ArrayList<AbilityLoader> list = new ArrayList<>();
        Collection<AbilityLoader> abilities = AbilityLoader.getAbilities();
        for (AbilityLoader ability : abilities) {
            if (ability.getCollection().equals(collection)) {
                list.add(ability);
            }
        }
        list.sort((a1, a2) -> {
            if (a1.getRarity().ordinal() == a2.getRarity().ordinal()) {
                return 0;
            } else if (a1.getRarity().ordinal() > a2.getRarity().ordinal()) {
                return 1;
            }
            return -1;
        });
        return list;
    }

    public static AbilityCollection getCollectionById(int id) {
        for (AbilityCollection collection : AbilityCollection.values()) {
            if (collection.getId() == id)
                return collection;
        }
        return null;
    }

    public static AbilityCollection getRandomCollection() {
        int random = ThreadLocalRandom.current().nextInt(AbilityCollection.values().length);
        AbilityCollection collection = AbilityCollection.values()[random];
        if (collection.equals(AbilityCollection.NONE)) {
            return getRandomCollection();
        }
        return AbilityCollection.values()[random];
    }

}