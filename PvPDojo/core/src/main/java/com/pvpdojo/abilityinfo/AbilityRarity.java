/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilityinfo;

import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.EnumOrderAccess;

public enum AbilityRarity implements EnumOrderAccess<AbilityRarity> {
    WHITE(
            500,
            "" + CC.WHITE,
            "White"),
    BLUE(
            5000,
            "" + CC.BLUE,
            "Blue"),
    PURPLE(
            30000,
            "" + CC.DARK_PURPLE,
            "Purple"),
    RED(
            100000,
            "" + CC.RED,
            "Red"),
    DARK_RED(
            200000,
            "" + CC.DARK_RED,
            "Dark Red"),
    GOLD(
            700000,
            "" + CC.GOLD,
            "Gold"),
    LEGENDARY(
            1500000,
            "" + CC.DARK_GRAY,
            "Legendary");

    String name;
    String color;
    int max;

    AbilityRarity(int max, String color, String name) {
        this.max = max;
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return color + name;
    }

    public int getMaxMoney() {
        return max;
    }

    public String getColor() {
        return color;
    }

}
