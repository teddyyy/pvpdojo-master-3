/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilityinfo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.mysql.SQL;
import com.pvpdojo.mysql.SQLBuilder;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.util.Log;

public class AbilityLog implements Comparable<AbilityLog> {

    public static List<AbilityLog> getLogChain(UUID abilityUUID) {
        List<AbilityLog> abilityLogs = new ArrayList<>();
        try (SQLBuilder sql = new SQLBuilder()) {
            sql.select(Tables.ABILITY_LOG, "*").where("ability_uuid = ?").orderBy("date", true).executeQuery(abilityUUID.toString());
            while (sql.next()) {
                abilityLogs.add(fromSQL(sql));
            }
        } catch (SQLException e) {
            Log.exception("Pulling ability log " + abilityUUID, e);
        }
        return abilityLogs;
    }

    public static AbilityLog fromSQL(SQLBuilder sql) {
        AbilityLog log = new AbilityLog();

        log.id = sql.getInteger("id");
        log.abilityUUID = UUID.fromString(sql.getString("ability_uuid"));
        log.previousOwner = UUID.fromString(sql.getString("previous_owner"));
        log.newOwner = UUID.fromString(sql.getString("new_owner"));
        log.reason = sql.getString("reason");
        log.date = sql.getTimestamp("date").getTime();

        return log;
    }

    private int id;
    private UUID abilityUUID;
    private UUID previousOwner;
    private UUID newOwner;
    private String reason;
    private long date;

    private AbilityLog() {}

    public AbilityLog(UUID abilityUUID, UUID previousOwner, UUID newOwner, String reason) {
        this.abilityUUID = abilityUUID;
        this.previousOwner = previousOwner;
        this.newOwner = newOwner;
        this.reason = reason;
    }

    public int getId() {
        return id;
    }

    public UUID getAbilityUUID() {
        return abilityUUID;
    }

    public UUID getPreviousOwner() {
        return previousOwner;
    }

    public UUID getNewOwner() {
        return newOwner;
    }

    public String getReason() {
        return reason;
    }

    public long getDate() {
        return date;
    }

    public void save() {
        PvPDojo.schedule(() -> {
            try {
                SQL.insert(Tables.ABILITY_LOG, "0,'" + abilityUUID + "', '" + previousOwner + "', '" + newOwner + "', '" + reason + "', NOW()");
            } catch (SQLException e) {
                Log.exception("Could not log ability change", e);
            }
        }).createNonMainThreadTask();
    }

    @Override
    public int compareTo(@NotNull AbilityLog abilityLog) {
        return Long.compare(date, abilityLog.date);
    }
}
