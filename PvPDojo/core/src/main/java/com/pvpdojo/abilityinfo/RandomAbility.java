/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilityinfo;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.util.Log;

public class RandomAbility {

    private static final SecureRandom SECURE_RANDOM = new SecureRandom();
    private static final RandomAbility instance = new RandomAbility();
    private HashMap<AbilityRarity, Double> random_main = new HashMap<>();

    public static RandomAbility get() {
        return instance;
    }

    public RandomAbility() {
        this.registerRarityMain(AbilityRarity.WHITE, 42);
        this.registerRarityMain(AbilityRarity.BLUE, 36);
        this.registerRarityMain(AbilityRarity.PURPLE, 16);
        this.registerRarityMain(AbilityRarity.RED, 4);
        this.registerRarityMain(AbilityRarity.DARK_RED, 1.5);
        this.registerRarityMain(AbilityRarity.GOLD, 0.35);
        this.registerRarityMain(AbilityRarity.LEGENDARY, 0.15);
    }

    private void registerRarityMain(AbilityRarity color, double percentage) {
        random_main.put(color, percentage);
    }

    public static AbilityRarity getRandomRarity(boolean print) {
        int num = SECURE_RANDOM.nextInt(10001);

        if (print) {
            Log.fine(String.valueOf(num));
        }

        // irl pic of guy who wrote this code: https://cdn.discordapp.com/emojis/406649107487129600.gif?v=1
        int white = (int) (get().random_main.get(AbilityRarity.WHITE) * 100); // 5000
        int blue = (int) (get().random_main.get(AbilityRarity.BLUE) * 100) + white; // 7800
        int purple = (int) (get().random_main.get(AbilityRarity.PURPLE) * 100) + blue; // 9400
        int red = (int) (get().random_main.get(AbilityRarity.RED) * 100) + purple; // 9800
        int dark_red = (int) (get().random_main.get(AbilityRarity.DARK_RED) * 100) + red; // 9950
        int gold = (int) (get().random_main.get(AbilityRarity.GOLD) * 100) + dark_red; // 9985

        if (white >= num) {
            return AbilityRarity.WHITE;
        } else if (blue >= num) {
            return AbilityRarity.BLUE;
        } else if (purple >= num) {
            return AbilityRarity.PURPLE;
        } else if (red >= num) {
            return AbilityRarity.RED;
        } else if (dark_red >= num) {
            return AbilityRarity.DARK_RED;
        } else if (gold >= num) {
            return AbilityRarity.GOLD;
        } else
            return AbilityRarity.LEGENDARY;
    }

    public static int getAbilityFromRarity(AbilityRarity rarity, AbilityCollection collection) {
        return getAbilityFromRarity(rarity, rarity, collection);
    }

    public static int getAbilityFromRarity(AbilityRarity rarity, AbilityRarity curSearch, AbilityCollection collection) {
        Collection<AbilityLoader> abilities = AbilityLoader.getAbilities();
        ArrayList<Integer> list = new ArrayList<>();
        for (AbilityLoader ability : abilities) {
            if (!ability.isEnabled())
                continue;
            if (ability.getRarity().equals(curSearch) && ability.getCollection().equals(collection)) {
                list.add(ability.getId());
            }
        }

        if (!list.isEmpty()) {
            return list.get(SECURE_RANDOM.nextInt(list.size()));
        }
        return getAbilityFromRarity(rarity, getOtherRarity(rarity, curSearch), collection);
    }

    public static AbilityRarity getOtherRarity(AbilityRarity rarity, AbilityRarity curSearch) {
        AbilityRarity other;
        if ((rarity == curSearch || curSearch.ordinal() < rarity.ordinal()) && curSearch.lower() != null) {
            other = curSearch.lower();
        } else if (curSearch.lower() == null) {
            other = rarity.next();
        } else {
            other = curSearch.next();
        }
        return other;
    }

}