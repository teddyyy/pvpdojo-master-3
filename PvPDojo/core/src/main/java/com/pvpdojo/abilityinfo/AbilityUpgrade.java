/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilityinfo;

import org.bukkit.Material;

public class AbilityUpgrade {

    public String description;
    public String name;
    public KitAttributeArray stats;
    public Float points;
    public int upgrade_number;
    public Material material;

    public AbilityUpgrade(int upgrade_number, String name) {
        this.upgrade_number = upgrade_number;
        this.name = name;
        stats = new KitAttributeArray();
    }

    public String getDescription() {
        return description;
    }

    public float getPoints() {
        return points;
    }

    public int getUpgradeNumber() {
        return upgrade_number;
    }

    public Material getMaterial() {
        return material;
    }

    public KitAttributeArray getStats() {
        return stats;
    }

    public String getName() {
        return name;
    }
}
