/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilityinfo;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public enum AbilityType {
    PASSIVE(
            0,
            new ItemStack(Material.WATCH)),
    CLICK(
            1,
            new ItemBuilder(Material.EMERALD_BLOCK).name(CC.GREEN + "Click Activation").build()),
    SHIFT(
            2,
            new ItemBuilder(Material.IRON_BOOTS).name(CC.GREEN + "Shift Activation").build()),
    BLOCK(
            3,
            new ItemBuilder(Material.DIAMOND_SWORD).name(CC.GREEN + "SwordBlock Activation").build());

    int id;
    ItemStack icon;

    AbilityType(int id, ItemStack icon) {
        this.id = id;
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public ItemStack getIcon() {
        return icon;
    }

    public static AbilityType getAbilityTypeById(int id) {
        for (AbilityType type : values()) {
            if (type.id == id)
                return type;
        }
        return AbilityType.CLICK;
    }

}
