/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.abilityinfo;

import java.util.ArrayList;
import java.util.Iterator;

import com.pvpdojo.util.MathUtil;

public class KitAttributeArray {

    private ArrayList<KitAttribute> attribute_array;

    public static KitAttributeArray create(String[] attributes) {
        return create(AttributeArrayType.BASE_ATTRIBUTES, attributes);
    }

    public static KitAttributeArray create(AttributeArrayType array_type, String[] attributes) {
        ArrayList<KitAttribute> array = new ArrayList<>(attributes.length);
        for (String attribute : attributes) {
            String[] colon_split = attribute.split("=");
            String type = colon_split[0];
            String value = colon_split[1];
            int attribute_type = array_type.ordinal();
            if (colon_split.length == 3)
                attribute_type = Integer.valueOf(colon_split[2]);
            KitAttribute final_attribute;
            if (MathUtil.isFloat(value)) {
                final_attribute = new KitAttribute(type, Float.valueOf(value), AttributeArrayType.values()[attribute_type]);
            } else {
                final_attribute = new KitStringAttribute(attribute, value);
            }
            array.add(final_attribute);
        }
        return new KitAttributeArray(array);
    }

    public static KitAttributeArray[] createArray(String[]... attributes) {
        KitAttributeArray[] array = new KitAttributeArray[attributes.length];
        int index = -1;
        for (String[] string_array : attributes) {
            index++;
            array[index] = create(string_array);
        }
        return array;
    }

    public KitAttributeArray() {
        this.attribute_array = new ArrayList<>();
    }

    private KitAttributeArray(ArrayList<KitAttribute> attribute_array) {
        this.attribute_array = attribute_array;
    }

    public ArrayList<KitAttribute> getAttributeArray() {
        return attribute_array;
    }

    public void addAttribute(KitAttribute attribute) {
        this.attribute_array.add(attribute);
    }

    public void removeAttribute(KitAttribute attribute) {
        Iterator<KitAttribute> iter = this.attribute_array.iterator();
        while (iter.hasNext()) {
            KitAttribute attr = iter.next();
            if (attr.attribute_name.equals(attribute.attribute_name) && attr.attribute_value == attribute.attribute_value) {
                iter.remove();
                return;
            }
        }
    }

    public KitAttribute getKitAttribute(String attribute_name) {
        for (KitAttribute attribute : getAttributeArray()) {
            if (attribute.getAttributeName().equals(attribute_name))
                return attribute;
        }
        throw new IllegalArgumentException("Invalid attribute name : " + attribute_name);
    }

    public boolean hasAttribute(String attribute_name) {
        for (KitAttribute attribute : getAttributeArray()) {
            if (attribute.getAttributeName().equals(attribute_name))
                return true;
        }
        return false;
    }

    public static class KitAttribute {
        private String attribute_name;
        private float attribute_value;
        private AttributeArrayType type;

        public KitAttribute(String attribute_name, float attribute_value, AttributeArrayType type) {
            this.attribute_name = attribute_name;
            this.attribute_value = attribute_value;
            this.type = type;
        }

        public KitAttribute(String attribute_name) {
            this(attribute_name, 0f, AttributeArrayType.BASE_ATTRIBUTES);
        }

        public AttributeArrayType getAttributeArrayType() {
            return type;
        }

        public float getAttributeValue() {
            return attribute_value;
        }

        public String getAttributeName() {
            return attribute_name;
        }

        public boolean isValid() {
            return true;
        }
    }

    public static class KitBooleanAttribute extends KitAttribute {
        private boolean attribute_value;

        public KitBooleanAttribute(String attribute_name, boolean attribute_value) {
            super(attribute_name);
            this.attribute_value = attribute_value;
        }

        public boolean getBooleanAttributeValue() {
            return attribute_value;
        }
    }

    public static class KitStringAttribute extends KitAttribute {
        private String attribute_value;

        public KitStringAttribute(String attribute_name, String attribute_value) {
            super(attribute_name);
            this.attribute_value = attribute_value;
        }

        public String getStringAttributeValue() {
            return attribute_value;
        }
    }

    public static class KitExpireAttribute extends KitAttribute {
        private Long finish_time;

        public KitExpireAttribute(String attribute_name, Float attribute_value, AttributeArrayType type, int milliseconds) {
            super(attribute_name, attribute_value, type);
            this.finish_time = System.currentTimeMillis() + milliseconds;
        }

        public Long getFinishTime() {
            return finish_time;
        }

        @Override
        public boolean isValid() {
            return finish_time < System.currentTimeMillis();
        }
    }

    public enum AttributeArrayType {
        BASE_ATTRIBUTES,
        BASE_ATTRIBUTES_INCREMENT,
        BONUS_ATTRIBUTES,
        APPLIED_BONUS_ATTRIBUTES,
        APPLIED_BASE_ATTRIBUTES,
        APPLIED_TOTAL_ATTRIBUTES,
    }

}
