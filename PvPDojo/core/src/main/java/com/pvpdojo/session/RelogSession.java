/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.userdata.User;

public interface RelogSession<T extends RelogData, U extends User> {

    Map<UUID, RelogData> RELOG_MAP = new HashMap<>();

    T createRelogData(U user);

    int getRelogTimeout();

    boolean hasNPC();

    boolean isAllowCombatLog();

    void onLogout(U user);

    void onTimeout(U user);

    default void onSessionEnd() {
        Iterator<RelogData> itr = RELOG_MAP.values().iterator();

        while (itr.hasNext()) {
            RelogData data = itr.next();
            if (data.getSession() == this) {
                PvPDojo.schedule(() -> BungeeSync.setReconnectServer(data.getUniqueId(), null)).createAsyncTask();
                itr.remove();
            }
        }
    }

}
