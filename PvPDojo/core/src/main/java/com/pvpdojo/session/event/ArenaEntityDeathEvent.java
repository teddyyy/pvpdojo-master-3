/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.event;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.session.entity.ArenaEntity;

public class ArenaEntityDeathEvent extends ArenaEntityEvent {

    private static final HandlerList handlerList = new HandlerList();

    private List<LocaleMessage> messages;

    public ArenaEntityDeathEvent(ArenaEntity arenaEntity, Event parentEvent) {
        super(arenaEntity, parentEvent);
    }

    public List<LocaleMessage> getMessages() {
        return messages != null ? messages : Collections.emptyList();
    }

    public void addMessage(LocaleMessage message) {
        if (messages == null) {
            messages = new ArrayList<>();
        }
        messages.add(message);
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList() {
        return handlerList;
    }

}
