/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.util.Vector;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.command.general.NickCommand;
import com.pvpdojo.gui.HotbarGUI;
import com.pvpdojo.session.entity.RankedArenaEntity;
import com.pvpdojo.session.entity.TeamArenaEntity;
import com.pvpdojo.userdata.NickData;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.User;
import com.pvpdojo.userdata.stats.Stats;

import net.minecraft.util.com.mojang.authlib.GameProfile;
import net.minecraft.util.com.mojang.authlib.properties.Property;

public class RelogData implements RankedArenaEntity, Reloggable {

    RelogNPC relogNPC;

    GameSession session;
    Stats stats;
    TeamArenaEntity team;
    UUID uuid;
    String name;
    PersistentData persistentData;
    HotbarGUI hotbarGUI;
    // We want to persist nicknames through a session
    NickData nickData;
    boolean useStats;

    ItemStack[] inventory, armor;
    float exp, falldistance, saturation;
    int level, foodLevel;
    double health, maxHealth;
    Location location;
    Vector velocity;
    Collection<PotionEffect> effects;

    public RelogData(User user) {
        this(user, user.getPlayer());
    }

    public RelogData(User user, Player player) {
        this(user.getSession(), user.getTeam(), user.getUUID(), user.getName(), user.getStats(), user.getPersistentData(), user.getHotbarGui(), player.getInventory().getContents(),
                player.getInventory().getArmorContents(), player.getExp(), player.getFallDistance(), player.getSaturation(), player.getLevel(), player.getFoodLevel(), player.getHealth(),
                player.getMaxHealth(), user.getLastValidLocation(), player.getVelocity(), player.getActivePotionEffects());

        if (user.getPlayer().isNicked()) {
            GameProfile nickProfile = NMSUtils.getCraftPlayer(user.getPlayer()).nickGameProfile;
            Property prop = nickProfile.getProperties().get("textures").iterator().next();
            nickData = new NickData(nickProfile.getId(), user.getName(), prop.getValue(), prop.getSignature());
        }
        useStats = user.useStats();
    }

    public RelogData(GameSession session, TeamArenaEntity team, UUID uuid, String name, Stats stats, PersistentData persistentData, HotbarGUI hotbarGUI, ItemStack[] inventory,
                     ItemStack[] armor, float exp,float falldistance, float saturation, int level, int foodLevel, double health, double maxHealth, Location location, Vector velocity,
                     Collection<PotionEffect> effects) {
        this.session = session;
        this.stats = stats;
        this.team = team;
        this.uuid = uuid;
        this.name = name;
        this.persistentData = persistentData;
        this.hotbarGUI = hotbarGUI;

        PvPDojo.schedule(persistentData::pullUserDataSneaky).createAsyncTask();

        this.inventory = inventory;
        this.armor = armor;
        this.exp = exp;
        this.falldistance = falldistance;
        this.saturation = saturation;
        this.level = level;
        this.foodLevel = foodLevel;
        this.health = health;
        this.maxHealth = maxHealth;
        this.location = location;
        this.velocity = velocity;
        this.effects = effects;
    }

    public void apply(User user) {
        user.setSession(session);
        user.setTeam(team);
        user.setHotbarGui(hotbarGUI);

        // Swap ArenaEntity
        if (session.isTeamFight()) {
            team.getMembers().set(team.getMembers().indexOf(this), user);
        } else {
            session.getParticipants().set(session.getParticipants().indexOf(this), user);
        }

        if (session.getCurrentlyDead().remove(this)) {
            session.getCurrentlyDead().add(user);
        }

        if (relogNPC != null) {
            this.health = relogNPC.getEntity().getHealth();
            this.falldistance = relogNPC.getEntity().getFallDistance();
            this.location = relogNPC.getEntity().getLocation();
            this.velocity = relogNPC.getEntity().getVelocity();
            this.effects = relogNPC.getEntity().getActivePotionEffects();
            relogNPC.remove();
        }

        Player player = user.getPlayer();

        // If they were dead in the game session specific handling is required
        if (health > 0) {
            player.teleport(location);
            player.getInventory().setContents(inventory);
            player.getInventory().setArmorContents(armor);
            player.setExp(exp);
            player.setFallDistance(falldistance);
            player.setSaturation(saturation);
            player.setLevel(level);
            player.setFoodLevel(foodLevel);
            player.setMaxHealth(maxHealth);
            player.setHealth(health);
            player.setVelocity(velocity);
            effects.forEach(player::addPotionEffect);
        }

        if (nickData != null && !player.getNick().equals(nickData.getName())) {
            NickCommand.nick(user, nickData);
        }
    }

    @Override
    public GameSession getSession() {
        return session;
    }

    @Override
    public void setSession(GameSession session) {
        if (session == null) {
            if (getRelogNPC() != null) {
                getRelogNPC().remove();
            }
        }
        this.session = session;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public UUID getUniqueId() {
        return uuid;
    }

    @Override
    public List<Player> getPlayers() {
        return Collections.emptyList();
    }

    @Override
    public Player getPlayer() {
        return null;
    }

    @Override
    public TeamArenaEntity getTeam() {
        return team;
    }

    @Override
    public void setTeam(TeamArenaEntity team) {
        this.team = team;
    }

    @Override
    public void teleport(Location location) {
        if (getRelogNPC() != null) {
            getRelogNPC().getEntity().teleport(location);
        }
        this.location = location;
    }

    @Override
    public int getElo() {
        return persistentData.getElo();
    }

    @Override
    public void addElo(int elo) {
        persistentData.incrementElo(elo);
    }

    public PersistentData getPersistentData() {
        return persistentData;
    }

    public Collection<PotionEffect> getEffects() {
        return effects;
    }

    public RelogNPC getRelogNPC() {
        return relogNPC;
    }

    public void setRelogNPC(RelogNPC relogNPC) {
        this.relogNPC = relogNPC;
    }

    public ItemStack[] getInventory() {
        return inventory;
    }

    public ItemStack[] getArmor() {
        return armor;
    }

    public double getHealth() {
        return getRelogNPC() != null ? getRelogNPC().getEntity().getHealth() : health;
    }

    public Stats getStats() {
        return stats;
    }

    public boolean isUseStats() {
        return useStats;
    }

}
