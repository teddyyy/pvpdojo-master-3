/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.entity.TeamArenaEntity;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemUtil;

import lombok.Getter;

public class PastGameSession {

    private Map<UUID, AfterFightSnapshot> inventories = new HashMap<>();
    private Set<PersistentArenaEntity> winners = new HashSet<>();
    private Set<PersistentArenaEntity> losers = new HashSet<>();
    private int eloChange, replayId;
    private transient int id;
    private long date, end;
    private SessionType type;
    private boolean valid;
    private List<String> gameMessages = new ArrayList<>();
    @Getter
    private String serverFolder;

    public PastGameSession() {
        id = -1;
        date = System.currentTimeMillis();
        end = -1;
        replayId = -1;
        valid = true;
        serverFolder = PvPDojo.get().getFolder();
    }

    public PastGameSession(SessionType type) {
        this();
        this.type = type;
    }

    public Map<UUID, AfterFightSnapshot> getInventories() {
        return inventories;
    }

    public Set<PersistentArenaEntity> getParticipants() {
        return Stream.of(winners, losers).flatMap(Set::stream).collect(Collectors.toSet());
    }

    public Set<PersistentArenaEntity> getWinners() {
        return winners;
    }

    public Set<PersistentArenaEntity> getLosers() {
        return losers;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public PastGameSession setId(int id) {
        this.id = id;
        return this;
    }

    public int getReplayId() {
        return replayId;
    }

    public long getEnd() {
        return end == -1 ? System.currentTimeMillis() : end;
    }

    public SessionType getType() {
        return type;
    }

    public int getDurationSeconds() {
        return (int) ((getEnd() - date) / 1000);
    }

    public void setEloChange(int eloChange) {
        this.eloChange = eloChange;
    }

    public int getEloChange() {
        return eloChange;
    }

    public void finish(GameSession session, Consumer<PastGameSession> callback) {
        if (session.hasReplay()) {
            this.replayId = session.getReplayId();
        }
        this.end = System.currentTimeMillis();
        session.getParticipants().forEach(entity -> entity.getPlayers()
                                                          .stream()
                                                          .filter(player -> !inventories.containsKey(player.getUniqueId()))
                                                          .forEach(player -> inventories.put(player.getUniqueId(), new AfterFightSnapshot(player))));

        PvPDojo.schedule(() -> {
            try {
                id = DBCommon.savePastGameSession(this);
            } catch (SQLException e) {
                Log.exception("Saving game session", e);
            }
            callback.accept(this);
        }).createAsyncTask();
    }

    public void deathSnapshot(Player player, AfterFightSnapshot snapshot) {
        inventories.put(player.getUniqueId(), snapshot);
    }

    public void deathSnapshot(RelogData entity, AfterFightSnapshot snapshot) {
        inventories.put(entity.getUniqueId(), snapshot);
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public List<String> getGameMessages() {
        return gameMessages;
    }

    public void changeValidation() {
        // TODO DOJO
    }

    public static class PersistentArenaEntity {

        private CC color;
        private Set<UUID> members;

        private transient ArenaEntity ref;

        public PersistentArenaEntity(ArenaEntity entity) {
            if (entity instanceof TeamArenaEntity) {
                this.color = ((TeamArenaEntity) entity).getColor();
            }
            if (entity instanceof RelogData) {
                this.members = new HashSet<>();
                this.members.add(entity.getUniqueId());
            } else {
                this.members = entity.getPlayers().stream().map(Player::getUniqueId).collect(Collectors.toSet());
            }
            this.ref = entity;
        }

        public CC getColor() {
            return color;
        }

        public boolean hasColor() {
            return color != null;
        }

        public Set<UUID> getMembers() {
            return members;
        }

        @Override
        public int hashCode() {
            if (ref != null) {
                return ref.hashCode();
            }
            return super.hashCode();
        }
    }

    public static class AfterFightSnapshot implements Comparable<AfterFightSnapshot> {

        private ItemStack[] inv, armor;
        private double health;
        private long date = System.currentTimeMillis();
        private UUID killer;

        private int ranking;
        private int kills;
        private String kits;

        public AfterFightSnapshot(RelogData relogData) {
            inv = relogData.getInventory();
            armor = relogData.getArmor();
            health = relogData.getHealth();
            if (relogData.getRelogNPC() != null && relogData.getRelogNPC().getEntity().getKiller() != null) {
                killer = relogData.getRelogNPC().getEntity().getKiller().getUniqueId();
            }
        }

        public AfterFightSnapshot(Player player) {
            if (player == null) {
                health = -1;
            } else {
                inv = ItemUtil.cloneContents(player.getInventory().getContents());
                armor = ItemUtil.cloneContents(player.getInventory().getArmorContents());
                health = player.getHealth();
                if (player.getKiller() != null) {
                    killer = player.getKiller().getUniqueId();
                }
            }
        }

        public double getHealth() {
            return health;
        }

        public ItemStack[] getInv() {
            return inv;
        }

        public ItemStack[] getArmor() {
            return armor;
        }

        public UUID getKiller() {
            return killer;
        }

        public long getDate() {
            return date;
        }

        public void setKills(int kills) {
            this.kills = kills;
        }

        public void setRanking(int ranking) {
            this.ranking = ranking;
        }

        public void setKits(String kits) {
            this.kits = kits;
        }

        @Override
        public int compareTo(@NotNull AfterFightSnapshot o) {
            return Long.compare(date, o.date);
        }
    }


}
