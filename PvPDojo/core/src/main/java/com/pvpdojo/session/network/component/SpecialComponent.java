/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.network.component;

import com.pvpdojo.session.GameSession;
import com.pvpdojo.session.entity.ArenaEntity;

public abstract class SpecialComponent {

    public abstract void handlePreSession(GameSession session);

    public abstract void handleAfterSession(GameSession session);

    public abstract void handleWinner(ArenaEntity winner);

    public abstract void handleLoser(ArenaEntity entity);
}
