/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.network.component;

public enum SessionComponent {

    TYPE,
    PLAYER,
    PARTY,
    MAP,
    PLAYSTYLE,
    SPECIAL_CLAN,
    SPECIAL_POKER

}
