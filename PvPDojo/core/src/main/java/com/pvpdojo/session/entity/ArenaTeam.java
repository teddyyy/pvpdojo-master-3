/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.entity;

import static com.pvpdojo.util.StringUtils.getEnumName;
import static com.pvpdojo.util.StringUtils.getStringFromArray;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.entity.Player;

import com.pvpdojo.session.GameSession;
import com.pvpdojo.util.bukkit.CC;

public class ArenaTeam implements TeamArenaEntity {

    private ArenaEntity leader;
    private List<ArenaEntity> members = new LinkedList<>(); // Linkedlist fits best -> no random access, insert at beginning
    private GameSession session;
    private CC color;
    private boolean friendlyFire;

    public ArenaTeam() {}

    public ArenaTeam(CC color, ArenaEntity... arenaEntities) {
        this.color = color;
        for (ArenaEntity entity : arenaEntities) {
            addMember(entity);
        }
    }

    public ArenaTeam(ArenaEntity leader, CC color, ArenaEntity... arenaEntities) {
        addMember(leader);
        setLeader(leader);
        this.color = color;
        for (ArenaEntity entity : arenaEntities) {
            addMember(entity);
        }
    }

    @Override
    public GameSession getSession() {
        return session;
    }

    @Override
    public void setSession(GameSession session) {
        TeamArenaEntity.super.setSession(session);
        this.session = session;
    }

    @Override
    public String getName() {
        return leader != null ? leader.getName() + "'s Team" : color + getEnumName(color);
    }

    @Override
    public String getLongName() {
        if (members.size() > 3) {
            return leader != null ? leader.getName() + "'s Team" : color + getEnumName(color);
        }
        return getStringFromArray(getMembers(), ArenaEntity::getName, ", ");
    }

    @Override
    public ArenaEntity getLeader() {
        return leader;
    }

    @Override
    public void setLeader(ArenaEntity leader) {
        if (!getMembers().contains(leader)) {
            throw new IllegalArgumentException("New leader must be in the team");
        }
        this.leader = leader;
        members.remove(leader);
        members.add(0, leader);
    }

    @Override
    public void addMember(ArenaEntity entity) {
        entity.setTeam(this);
        if (!members.contains(entity)) {
            members.add(entity);
        }
    }

    @Override
    public void removeMember(ArenaEntity entity) {
        entity.setTeam(null);
        members.remove(entity);
        if (leader == entity && !members.isEmpty()) {
            setLeader(members.get(0));
        }
    }

    public List<ArenaEntity> getMembers() {
        return members;
    }

    @Override
    public List<Player> getPlayers() {
        return members.stream().flatMap(ae -> ae.getPlayers().stream()).collect(Collectors.toList());
    }

    @Override
    public CC getColor() {
        return color;
    }

    public void setColor(CC color) {
        this.color = color;
    }

    @Override
    public boolean isFriendlyFire() {
        return friendlyFire;
    }

    @Override
    public void setFriendlyFire(boolean friendlyFire) {
        this.friendlyFire = friendlyFire;
    }
}
