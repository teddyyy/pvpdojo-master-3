/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session;

import java.util.UUID;

import com.pvpdojo.userdata.User;

public interface Reloggable {

    void apply(User user);

    UUID getUniqueId();

}
