/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.arena;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.map.DynamicMapPlacer;
import com.pvpdojo.map.MapLoader;
import com.pvpdojo.map.MapType;

public class ArenaManager<A extends Arena> {

    private final MapType mapType;
    private final String[] extraFlags;

    protected List<A> arenas = new ArrayList<>();

    public ArenaManager(MapType type, String... extraFlags) {
        this.mapType = type;
        this.extraFlags = extraFlags;
    }

    public void buildNewArenas() {
        List<A> newArenas = MapLoader.getMaps(mapType, extraFlags);
        newArenas.forEach(DynamicMapPlacer::generateMap);
        arenas.addAll(newArenas);
    }

    public A getRandomFreeArena() {
        return getRandomFreeArena(a -> false);
    }

    public A getRandomFreeArena(Predicate<A> filter) {

        if (!arenas.isEmpty() && arenas.stream().allMatch(filter)) {
            return getRandomFreeArena(filter.negate());
        }

        List<A> available = arenas.stream().filter(Arena::isFree).filter(filter.negate()).collect(Collectors.toList());

        if (available.isEmpty()) {
            buildNewArenas();
            return getRandomFreeArena(filter);
        }

        A arena = available.get(PvPDojo.RANDOM.nextInt(available.size()));
        arena.setOccupied();

        return arena;
    }

}
