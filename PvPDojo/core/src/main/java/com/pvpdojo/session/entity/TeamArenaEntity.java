/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.entity;

import java.util.List;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import com.pvpdojo.session.GameSession;
import com.pvpdojo.util.bukkit.CC;

public interface TeamArenaEntity extends ArenaEntity, Comparable<TeamArenaEntity> {

    @Override
    default void setSession(GameSession session) {
        getMembers().forEach(member -> member.setSession(session));
    }

    List<ArenaEntity> getMembers();

    void addMember(ArenaEntity entity);

    void removeMember(ArenaEntity entity);

    ArenaEntity getLeader();

    void setLeader(ArenaEntity leader);

    @Override
    default TeamArenaEntity getTeam() {
        return this;
    }

    @Override
    default void setTeam(TeamArenaEntity team) {
        throw new UnsupportedOperationException("Cannot assign a team to a team");
    }

    @Override
    default UUID getUniqueId() {
        return null;
    }

    CC getColor();

    void setColor(CC color);

    boolean isFriendlyFire();

    void setFriendlyFire(boolean friendlyFire);

    @Override
    default Player getPlayer() {
        return null;
    }

    @Override
    default int compareTo(@NotNull TeamArenaEntity teamArenaEntity) {
        return Integer.compare(teamArenaEntity.getColor().ordinal(), getColor().ordinal());
    }
}
