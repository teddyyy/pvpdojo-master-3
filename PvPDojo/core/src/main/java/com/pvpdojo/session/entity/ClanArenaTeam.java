/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.entity;

import java.util.UUID;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.clan.Clan;
import com.pvpdojo.util.bukkit.CC;

public class ClanArenaTeam extends PreparedArenaTeam implements RankedArenaEntity {

    private Clan clan;

    public ClanArenaTeam(UUID potentialLeader, Clan clan, CC color, UUID... potentialPlayers) {
        super(potentialLeader, color, potentialPlayers);
        this.clan = clan;
    }

    public Clan getClan() {
        return clan;
    }

    @Override
    public int getElo() {
        return clan.getElo();
    }

    @Override
    public void addElo(int elo) {
        PvPDojo.schedule(() -> clan.transaction(() -> clan.setElo(clan.getElo() + elo))).createNonMainThreadTask();
    }

    @Override
    public String getName() {
        return getColor() + clan.getName();
    }
}
