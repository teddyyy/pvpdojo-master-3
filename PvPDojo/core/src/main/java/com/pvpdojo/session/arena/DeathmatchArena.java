/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.arena;

import static com.pvpdojo.util.StreamUtils.negate;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.entity.Entity;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.map.MapType;

public class DeathmatchArena extends Arena {

    public static final String CLAN_MAP_FLAG = "clan";

    public DeathmatchArena() {
        super("none", 2);
    }

    public DeathmatchArena(int spawnLocations) {
        super("none", spawnLocations);
    }

    @Override
    public MapType getType() {
        return MapType.DEATHMATCH;
    }

    @Override
    public void cleanup() {
        Set<Entity> entityList = this.entities;
        this.entities = new HashSet<>();
        PvPDojo.schedule(() -> entityList.stream().filter(negate(Entity::isDead)).forEach(Entity::remove)).nextTick();
    }
}
