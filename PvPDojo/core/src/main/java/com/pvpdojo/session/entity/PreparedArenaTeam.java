/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.entity;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import com.pvpdojo.util.bukkit.CC;

public class PreparedArenaTeam extends ArenaTeam {

    private UUID potentialLeader;
    private Set<UUID> potentialPlayers = new HashSet<>();

    public PreparedArenaTeam(UUID potentialLeader, CC color, UUID... potentialPlayers) {
        this.potentialLeader = potentialLeader;
        this.potentialPlayers.addAll(Arrays.asList(potentialPlayers));
        setColor(color);
    }

    @Override
    public void addMember(ArenaEntity entity) {
        super.addMember(entity);
        if (potentialLeader != null && potentialLeader.equals(entity.getUniqueId())) {
            setLeader(entity);
        }
    }

    public Set<UUID> getPotentialPlayers() {
        return potentialPlayers;
    }
    
    public UUID getPotentialLeader() {
        return potentialLeader;
    }

}
