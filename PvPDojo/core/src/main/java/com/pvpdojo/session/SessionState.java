/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session;

public enum SessionState {
    PREGAME,
    INVINCIBILITY,
    STARTED,
    FINISHED
}
