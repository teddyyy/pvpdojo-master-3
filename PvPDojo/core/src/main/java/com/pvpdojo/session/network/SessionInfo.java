/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.network;

import com.pvpdojo.session.SessionState;
import com.pvpdojo.session.SessionType;

import lombok.Data;

@Data
public class SessionInfo {

    private final SessionType sessionType;
    private final SessionState sessionState;
    private final int time;
    private final int maxPlayers;
    private final int sessionPlayerCount;

    private final String arena;
    private final int gameId;

}
