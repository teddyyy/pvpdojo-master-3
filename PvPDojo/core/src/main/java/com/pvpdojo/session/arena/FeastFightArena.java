/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.arena;

import com.pvpdojo.map.MapType;

public class FeastFightArena extends DeathmatchArena {

    public FeastFightArena() {
        super(4);
    }

    @Override
    public MapType getType() {
        return MapType.FEAST_FIGHT;
    }
}
