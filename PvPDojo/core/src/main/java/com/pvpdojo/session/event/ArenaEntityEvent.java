/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.event;

import org.bukkit.event.Event;

import com.pvpdojo.session.entity.ArenaEntity;

public abstract class ArenaEntityEvent extends Event {

    private ArenaEntity arenaEntity;
    private Event parentEvent;

    public ArenaEntityEvent(ArenaEntity arenaEntity, Event parentEvent) {
        this.arenaEntity = arenaEntity;
        this.parentEvent = parentEvent;
    }

    public Event getParentEvent() {
        return parentEvent;
    }

    public ArenaEntity getArenaEntity() {
        return arenaEntity;
    }
}
