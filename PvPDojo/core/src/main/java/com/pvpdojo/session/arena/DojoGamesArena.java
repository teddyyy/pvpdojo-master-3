/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.arena;

import org.bukkit.Location;

import com.pvpdojo.map.MapType;

public class DojoGamesArena extends Arena {

    public DojoGamesArena() {
        super("none", 1);
    }

    public DojoGamesArena(String name) {
        super(name, 1);

    }

    public Location getSpawnPoint() {
        return locations[0];
    }

    @Override
    public MapType getType() {
        return MapType.DOJOGAMES;
    }

    @Override
    public void cleanup() {}
}
