/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session;

import static com.pvpdojo.util.StringUtils.createComponent;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.replay.Recorder;
import com.pvpdojo.replay.Replay;
import com.pvpdojo.session.PastGameSession.AfterFightSnapshot;
import com.pvpdojo.session.arena.Arena;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.entity.RankedArenaEntity;
import com.pvpdojo.session.entity.TeamArenaEntity;
import com.pvpdojo.session.event.ArenaEntityDeathEvent;
import com.pvpdojo.session.network.component.SpecialComponent;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.PlayerUtil;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.StateFlag.State;

import co.aikar.locales.MessageKeyProvider;
import net.md_5.bungee.api.chat.TextComponent;

public abstract class GameSession {

    public static Collection<GameSession> getSessions() {
        return Bukkit.getOnlinePlayers().stream().map(User::getUser).map(User::getSession).filter(Objects::nonNull).distinct().collect(Collectors.toList());
    }

    private boolean ranked;
    protected List<ArenaEntity> participants = new ArrayList<>();
    private Set<ArenaEntity> currentlyDead = new HashSet<>();
    protected Replay replay;
    protected PastGameSession pendingSave;
    private SessionType type;
    private SessionState state;
    private SpecialComponent specialComponent;
    protected int recorderDelay = 2 * 20;

    private Arena arena;

    public GameSession(Arena arena, SessionType type, ArenaEntity... participants) {
        if (arena.isFree()) {
            throw new IllegalStateException("Provided arena must be set occupied");
        }
        this.pendingSave = new PastGameSession(type);
        Stream.of(participants).forEach(this::addParticipant);
        this.arena = arena;
        this.type = type;
    }

    public void start() {
        if (specialComponent != null) {
            specialComponent.handlePreSession(this);
        }
        initialTeleport();
        prepareEntities();
        if (hasReplay()) {
            this.replay = new Replay(arena);
            replay.setInitialLoc(arena.getLocations()[0].clone());
            Recorder.inst().startRecording(replay, null);
        }
    }

    public List<ArenaEntity> getParticipants() {
        return participants;
    }

    public ArenaEntity getParticipant(UUID uuid) {
        return participants.stream().filter(entity -> entity.getUniqueId().equals(uuid)).findFirst().orElse(null);
    }

    public List<Player> getPlayers() {
        return getParticipants().stream().flatMap(en -> en.getPlayers().stream()).filter(Objects::nonNull).filter(Player::isOnline).collect(Collectors.toList());
    }

    public void executeForPlayers(Consumer<Player> action) {
        getPlayers().forEach(action);
    }

    public void addParticipant(ArenaEntity ae) {
        if (isTeamFight() && ae instanceof TeamArenaEntity) {
            participants.add(ae);
        } else if (!isTeamFight()) {
            participants.add(ae);
        }
        ae.setSession(this);
    }

    public Arena getArena() {
        return arena;
    }

    public SessionType getType() {
        return type;
    }

    public int getReplayId() {
        return replay.getId();
    }

    public void initialTeleport() {
        int locations = arena.getLocations().length;
        for (int i = 0; i < participants.size(); i++) {
            ArenaEntity part = participants.get(i);
            part.teleport(arena.getLocations()[i % locations]);
        }
    }

    public Location getSpawnLocation(ArenaEntity entity) {
        int index = isTeamFight() ? participants.indexOf(entity.getTeam()) : participants.indexOf(entity);
        return arena.getLocations()[index % arena.getLocations().length];
    }

    public abstract void endRanked(RankedArenaEntity winner, ArenaEntity... losers);

    public abstract void onLose(ArenaEntity loser);

    public abstract void onWin(ArenaEntity winner);

    public abstract void handleMatchDetails(TextComponent tc);

    public void prepareEntities() {
        getParticipants().forEach(this::prepareEntity);
    }

    public void prepareEntities(int startSpread, int perTick, int maxTicks) {
        PlayerUtil.spreadPlayerTask(new ArrayList<>(getParticipants()), this::prepareEntity, startSpread, perTick, maxTicks);
    }

    public abstract void prepareEntity(ArenaEntity entity);

    public void end(ArenaEntity... losers) {

        boolean wasRunning = state == SessionState.STARTED;
        setState(SessionState.FINISHED);

        if (this instanceof RelogSession) {
            ((RelogSession) this).onSessionEnd();
        }

        for (ArenaEntity entity : participants) {
            if (Stream.of(losers).noneMatch(loser -> loser.equals(entity))) { // Do not change to lambda this is a hack to possibly match everyone as loser
                onWin(entity);
                pendingSave.getWinners().add(new PastGameSession.PersistentArenaEntity(entity));
                if (ranked && entity instanceof RankedArenaEntity) {
                    endRanked((RankedArenaEntity) entity, losers);
                }
                if (specialComponent != null) {
                    specialComponent.handleWinner(entity);
                }
            } else {
                onLose(entity);
                pendingSave.getLosers().add(new PastGameSession.PersistentArenaEntity(entity));
                if (specialComponent != null) {
                    specialComponent.handleLoser(entity);
                }
            }

            entity.setSession(null);
        }

        arena.free();

        // Save pending
        if (isPersistent() && wasRunning) {
            if (replay != null) {
                PvPDojo.schedule(() -> Recorder.inst().stopRecording(replay)).createTask(recorderDelay);
            }
            pendingSave.finish(this, session -> handleMatchDetails(createComponent(CC.GOLD + "Click here for inventories and replay",
                    "*click*", "session " + Integer.toHexString(session.getId()))));
        }

        if (specialComponent != null) {
            specialComponent.handleAfterSession(this);
        }
    }

    public void broadcast(LocaleMessage msg) {
        participants.forEach(ae -> ae.sendMessage(msg));
        if (replay != null) {
            long time = System.currentTimeMillis();
            Recorder.inst().getTasks().add(() -> Arrays.stream(msg.getDefaultLocale()).forEach(message -> replay.chat(message, time)));
        }
    }

    public void broadcast(MessageKeyProvider key, String... replacements) {
        broadcast(LocaleMessage.of(key, replacements));
    }

    public void disconnect(ArenaEntity entity) {
        postDeath(entity);
    }

    public void death(ArenaEntity entity, Event parentEvent) {
        currentlyDead.add(entity);

        ArenaEntityDeathEvent event = new ArenaEntityDeathEvent(entity, parentEvent);
        Bukkit.getPluginManager().callEvent(event);
        event.getMessages().forEach(this::broadcast);

    }

    public void postDeath(ArenaEntity entity) {
        switch (getState()) {
            case PREGAME:
                getParticipants().remove(entity);
                if (entity.getTeam() != null) {
                    entity.getTeam().removeMember(entity);
                }
                break;
            case STARTED:
            case INVINCIBILITY:
                currentlyDead.add(entity);

                if (entity.getPlayer() != null) {
                    pendingSave.deathSnapshot(entity.getPlayer(), getSnapshot(entity.getPlayer()));
                } else if (entity instanceof RelogData) {
                    pendingSave.deathSnapshot((RelogData) entity, getSnapshot((RelogData) entity));
                    RelogSession.RELOG_MAP.remove(entity.getUniqueId());
                    PvPDojo.schedule(() -> BungeeSync.setReconnectServer(entity.getUniqueId(), null)).createAsyncTask();
                }
                if (isTeamFight() && currentlyDead.containsAll(entity.getTeam().getMembers())) {
                    currentlyDead.removeAll(entity.getTeam().getMembers());
                    currentlyDead.add(entity.getTeam());
                }
                break;
        }
    }

    public void triggerEnd(ArenaEntity... potentialLosers) {
        if (checkEndCondition()) {
            end(potentialLosers);
        }
    }

    public abstract boolean checkEndCondition();

    public abstract boolean isPersistent();

    public boolean hasReplay() {
        return isPersistent();
    }

    public Set<ArenaEntity> getCurrentlyDead() {
        return currentlyDead;
    }

    public void setRanked(boolean ranked) {
        this.ranked = ranked;
    }

    public boolean isRanked() {
        return ranked;
    }

    public PastGameSession getPendingSave() {
        return pendingSave;
    }

    public SessionState getState() {
        return state;
    }

    public boolean isTeamFight() {
        return !getParticipants().isEmpty() && getParticipants().get(0) instanceof TeamArenaEntity;
    }

    public SpecialComponent getSpecialComponent() {
        return specialComponent;
    }

    public void setSpecialComponent(SpecialComponent specialComponent) {
        this.specialComponent = specialComponent;
    }

    public void handleSpecialComponentGameSpecific() {}

    public void setState(SessionState state) {
        if (this.state == SessionState.PREGAME && state != SessionState.PREGAME) {
            pendingSave.setDate(System.currentTimeMillis());
        }
        this.state = state;
        switch (state) {
            case INVINCIBILITY:
            case FINISHED:
            case PREGAME:
                getArena().getRegion().setFlag(DefaultFlag.PVP, null);
                getArena().getRegion().setFlag(DefaultFlag.INVINCIBILITY, StateFlag.State.ALLOW);
                break;
            case STARTED:
                getArena().getRegion().setFlag(DefaultFlag.INVINCIBILITY, null);
                getArena().getRegion().setFlag(DefaultFlag.PVP, State.ALLOW);
                break;
            default:
                break;

        }
    }

    public AfterFightSnapshot getSnapshot(Player player) {
        return new AfterFightSnapshot(player);
    }

    public AfterFightSnapshot getSnapshot(RelogData relogData) {
        return new AfterFightSnapshot(relogData);
    }

}
