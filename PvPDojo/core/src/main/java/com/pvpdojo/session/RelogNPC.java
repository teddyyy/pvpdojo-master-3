/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Zombie;
import org.bukkit.metadata.FixedMetadataValue;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.NMSUtils;

public class RelogNPC {

    public static final String RELOG_NPC = "relog_npc";

    private Zombie zombie;

    public RelogNPC(RelogData relogData) {
        zombie = relogData.location.getWorld().spawn(relogData.location, Zombie.class);
        NMSUtils.clearAI(zombie);

        zombie.setCustomName(relogData.getName());
        zombie.setCustomNameVisible(true);
        zombie.setMaxHealth(relogData.maxHealth);
        zombie.setHealth(relogData.health);
        zombie.setBaby(false);
        zombie.setVillager(false);

        // Just experienced a zombie riding a chicken, we don't want that to happen...
        if (zombie.getVehicle() != null) {
            zombie.getVehicle().remove();
        }

        zombie.setCanPickupItems(true);
        zombie.getEquipment().clear();
        zombie.getEquipment().setArmorContents(relogData.armor);
        zombie.getEquipment().setHelmetDropChance(0);
        zombie.getEquipment().setChestplateDropChance(0);
        zombie.getEquipment().setLeggingsDropChance(0);
        zombie.getEquipment().setBootsDropChance(0);
        zombie.getEquipment().setItemInHand(relogData.inventory[0]);
        zombie.getEquipment().setItemInHandDropChance(0);
        zombie.setCanPickupItems(false);

        zombie.setRespectUndead(false); // GSpigot feature
        zombie.addPotionEffects(relogData.getEffects());

        zombie.setMetadata(RELOG_NPC, new FixedMetadataValue(PvPDojo.get(), relogData));
    }

    public void remove() {
        if (!zombie.isDead()) {
            zombie.remove();
        }
    }

    public Zombie getEntity() {
        return zombie;
    }

    public static RelogData getData(Entity entity) {
        if (entity.hasMetadata(RELOG_NPC)) {
            return (RelogData) entity.getMetadata(RELOG_NPC).get(0).value();
        }
        return null;
    }

}
