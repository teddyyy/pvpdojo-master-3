/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.arena;

import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.function.Consumer;

import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.block.Dropper;
import org.bukkit.entity.Entity;

import com.pvpdojo.map.DojoMap;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public abstract class Arena extends DojoMap {

    protected transient boolean free = true;
    protected transient Set<Entity> entities = Collections.newSetFromMap(new WeakHashMap<>());

    public Arena(String name, int spawnLocations) {
        super(name, spawnLocations);
    }

    public void free() {
        cleanup();
        this.free = true;
    }

    public boolean isFree() {
        return free;
    }

    public void setOccupied() {
        this.free = false;
    }

    public Set<Entity> getEntityList() {
        return entities;
    }

    public void setup(int localId) {
        free();
        super.setup(localId);
    }

    public abstract void cleanup();

    public void forEachChest(Consumer<Chest> chestConsumer) {
        // Let's search for chests in this arena
        ProtectedRegion region = getRegion();
        for (int x = region.getMinimumPoint().getBlockX(); x < region.getMaximumPoint().getBlockX(); x += 16) {
            for (int z = region.getMinimumPoint().getBlockZ(); z < region.getMaximumPoint().getBlockZ(); z += 16) {
                for (BlockState tileEntity : getWorld().getChunkAt(x >> 4, z >> 4).getTileEntities()) {
                    if (tileEntity instanceof Chest) {
                        chestConsumer.accept((Chest) tileEntity);
                    }
                }
            }
        }
    }

    public void forEachDropper(Consumer<Dropper> chestConsumer) {
        // Let's search for droppers in this arena
        ProtectedRegion region = getRegion();
        for (int x = region.getMinimumPoint().getBlockX(); x < region.getMaximumPoint().getBlockX(); x += 16) {
            for (int z = region.getMinimumPoint().getBlockZ(); z < region.getMaximumPoint().getBlockZ(); z += 16) {
                for (BlockState tileEntity : getWorld().getChunkAt(x >> 4, z >> 4).getTileEntities()) {
                    if (tileEntity instanceof Dropper) {
                        chestConsumer.accept((Dropper) tileEntity);
                    }
                }
            }
        }
    }

}
