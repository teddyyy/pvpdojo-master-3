/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.arena;

import com.pvpdojo.map.MapType;

public class HGArena extends Arena {

    public HGArena() {
        super("none", 1);
    }

    // no cleanup needed
    @Override
    public void cleanup() {}

    @Override
    public MapType getType() {
        return MapType.HG;
    }
}
