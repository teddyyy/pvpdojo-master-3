/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.arena;

import org.bukkit.entity.Entity;

import com.pvpdojo.map.MapType;

public class LavaDuelArena extends Arena {

    public LavaDuelArena() {
        super("none", 2);
    }

    public LavaDuelArena(String name, int spawnLocations) {
        super(name, spawnLocations);
    }

    @Override
    public void cleanup() {
        getEntityList().forEach(Entity::remove);
    }

    @Override
    public MapType getType() {
        return MapType.LAVA_DUEL;
    }
}
