/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.network.component;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.google.common.collect.HashBiMap;
import com.google.common.collect.Iterables;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.clan.Clan;
import com.pvpdojo.party.PartyList;
import com.pvpdojo.session.GameSession;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.entity.ClanArenaTeam;
import com.pvpdojo.session.network.SessionRequest;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.Log;

public class ClanComponent extends SpecialComponent {

    private ClanGame currentGame;
    private Clan higherRatedClan, lowerRatedClan;
    private Map<Clan, AtomicInteger> currentScore = new HashMap<>();
    private Map<Clan, UUID> clanPartyMap = new HashMap<>();
    private Map<Clan, List<UUID>> lineup = new HashMap<>();
    /**
     * Order is based on clan leaders selection of the gamemmodes.
     */
    private Queue<ClanGame> gameModesToPlay = new LinkedList<>();
    private boolean running = true;

    public ClanComponent(Clan higherRatedClan, Clan lowerRatedClan) {
        this.higherRatedClan = higherRatedClan;
        this.lowerRatedClan = lowerRatedClan;
        this.currentScore.put(higherRatedClan, new AtomicInteger());
        this.currentScore.put(lowerRatedClan, new AtomicInteger());
    }

    @Override
    public void handlePreSession(GameSession session) {
        session.handleSpecialComponentGameSpecific();
    }

    @Override
    public void handleAfterSession(GameSession session) {
        sendCurrentScore();

        if (running) {
            sendMessage(CC.GRAY + "Starting next gamemode in 30 seconds");
            sendMessage(CC.GRAY + "Players that reconnected during the last game will be readded to the game");

            advance(30);
        }
    }

    @Override
    public void handleWinner(ArenaEntity winner) {
        if (!(winner instanceof ClanArenaTeam)) {
            endWithWinner(null);
            throw new IllegalStateException("Expected ClanArenaTeam for a clan fight");
        }
        ClanArenaTeam team = (ClanArenaTeam) winner;

        Log.info(team.getClan().getName());

        if (!currentScore.containsKey(team.getClan())) {
            endWithWinner(null);
            throw new IllegalStateException("Could not find current score");
        }
        if (currentScore.get(team.getClan()).incrementAndGet() >= 2) {
            endWithWinner(team.getClan());
        }

    }

    public void endWithWinner(Clan winner) {
        running = false;

        if (winner == null) {
            sendMessage(CC.RED + "An unexpected error occured ending clan fight as a draw");
        } else {
            sendMessage(CC.GREEN + winner.getName() + " won the clan fight!");
            sendMessage(CC.GREEN + "Final Score:");
        }
    }

    @Override
    public void handleLoser(ArenaEntity entity) {}

    public Clan getHigherRatedClan() {
        return higherRatedClan;
    }

    public Clan getLowerRatedClan() {
        return lowerRatedClan;
    }

    public Queue<ClanGame> getGameModesToPlay() {
        return gameModesToPlay;
    }

    public Map<Clan, UUID> getClanPartyMap() {
        return clanPartyMap;
    }

    public Map<UUID, Clan> getPartyClanMap() {
        return HashBiMap.create(clanPartyMap).inverse();
    }

    public ClanGame getCurrentGame() {
        return currentGame;
    }

    public void sendCurrentScore() {
        sendMessage(CC.DARK_GRAY + "[" + CC.AQUA + higherRatedClan.getName() + CC.DARK_GRAY + "] " + CC.RED + currentScore.get(higherRatedClan).get()
                + CC.WHITE + CC.BOLD + " - "
                + CC.DARK_GRAY + "[" + CC.AQUA + lowerRatedClan.getName() + CC.DARK_GRAY + "] " + CC.RED + currentScore.get(lowerRatedClan).get());
    }

    public void setLineup(Clan clan, Set<UUID> lineup) {
        for (UUID uuid : lineup) {
            if (!this.lineup.containsKey(clan)) {
                this.lineup.put(clan, new ArrayList<>());
            }
            this.lineup.get(clan).add(uuid);
        }
    }

    public void createParties() {
        PvPDojo.schedule(() -> lineup.keySet().forEach(this::createParty)).createAsyncTask();
    }

    public void createParty(Clan clan) {
        UUID leader = Iterables.getFirst(lineup.get(clan), null);
        clanPartyMap.put(clan, PartyList.createParty(leader, lineup.get(clan)).getPartyId());
    }

    public void advance(int delay) {
        ClanGame nextGame = gameModesToPlay.poll();

        PvPDojo.newChain().delay(delay, TimeUnit.SECONDS).async(() -> {
            sendMessage(CC.GREEN + "Starting game " + nextGame.toString());
            lineup.entrySet().forEach(lineupEntry -> {
                if (!PartyList.contains(clanPartyMap.get(lineupEntry.getKey()))) {
                    createParty(lineupEntry.getKey());
                }
                for (UUID uuid : lineupEntry.getValue()) {
                    PartyList.addPartyUser(PartyList.getPartyById(clanPartyMap.get(lineupEntry.getKey())), uuid);
                }
            });
        }).sync(() -> {
            SessionRequest request = new SessionRequest();

//            switch (nextGame) {
//                case ABILITY_FIGHT:
//                case NO_ABILITY_FIGHT:
//                    request.type(SessionType.TEAM_DUEL_RANKED);
//                    break;
//            }

            this.currentGame = nextGame;
            request.players(lineup.values().stream().flatMap(Collection::stream).collect(Collectors.toList())).parties(clanPartyMap.values()).special(this).sendToServer(ServerType.DEATHMATCH, () -> {
                Log.severe("Server for clan fight not found");
                endWithWinner(null);
            });
        }).execute();
    }

    public void sendMessage(String message) {
        for (Clan clan : getClans()) {
            clan.channelMessage(message);
        }
    }

    public List<Clan> getClans() {
        return Arrays.asList(higherRatedClan, lowerRatedClan);
    }

    public enum ClanGame {
        ABILITY_FIGHT, CTM
    }

}
