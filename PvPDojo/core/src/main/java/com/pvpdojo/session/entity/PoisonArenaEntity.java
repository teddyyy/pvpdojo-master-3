package com.pvpdojo.session.entity;

import java.util.List;
import java.util.UUID;

import org.bukkit.entity.Player;

import com.pvpdojo.session.GameSession;

public class PoisonArenaEntity implements ArenaEntity {

    protected PoisonArenaEntity() {}

    // Hack to match all
    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object o) {
        return true;
    }

    @Override
    public GameSession getSession() {
        return null;
    }

    @Override
    public void setSession(GameSession session) {

    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public UUID getUniqueId() {
        return null;
    }

    @Override
    public List<Player> getPlayers() {
        return null;
    }

    @Override
    public TeamArenaEntity getTeam() {
        return null;
    }

    @Override
    public void setTeam(TeamArenaEntity team) {

    }
}
