/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.arena;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.Location;

import com.google.common.collect.Sets;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.map.MapType;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.util.BiMultiMap;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.HashBiMultiMap;
import com.pvpdojo.util.Log;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag.State;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class CTMArena extends Arena {

    public static final String SPAWN_REGION_RED = "spawn_region_red", SPAWN_REGION_BLUE = "spawn_region_blue", CAPTURE_ZONE_RED = "capture_zone_red",
            CAPTURE_ZONE_BLUE = "capture_zone_blue", BUILD_ZONE = "build_zone", NO_BUILD_ZONE = "no_build_zone", COW_HOME_RED = "cow_home_red", COW_HOME_BLUE = "cow_home_blue",
            COW_SPAWN_RED = "cow_spawn_red_", COW_SPAWN_BLUE = "cow_spawn_blue_";

    public static final String PLAYERS = "players";
    public static final int DEFAULT_PLAYER_AMOUNT = 20; // 10 per team

    private transient ProtectedRegion spawnRegionRed, spawnRegionBlue;
    private transient Set<Location> miniFeasts = new HashSet<>();
    private transient Set<ProtectedRegion> captureZonesRed = new HashSet<>(), captureZonesBlue = new HashSet<>();
    private transient Set<ProtectedRegion> buildZones = new HashSet<>(), noBuildZones = new HashSet<>();
    private transient BiMultiMap<CTMDirection, ProtectedRegion> directionMap = HashBiMultiMap.create();

    public CTMArena() {
        this(null, 2);
    }

    public CTMArena(String name, int spawnLocations) {
        super(name, spawnLocations);
    }

    @Override
    public void cleanup() {}

    @Override
    public MapType getType() {
        return MapType.CTM;
    }

    public Location getSpawnRed() {
        return locations[0];
    }

    public Location getSpawnBlue() {
        return locations[1];
    }

    public Location getSpectatorSpawn() {
        return getLocationMap().containsKey("spec") ? getLocationMap().get("spec") : getSpawnRed();
    }

    public ProtectedRegion getSpawnRegionRed() {
        return spawnRegionRed;
    }

    public ProtectedRegion getSpawnRegionBlue() {
        return spawnRegionBlue;
    }

    public List<ProtectedRegion> getSpawnRegions() {
        return Arrays.asList(getSpawnRegionRed(), getSpawnRegionBlue());
    }

    public Set<Location> getMiniFeasts() {
        return miniFeasts;
    }

    public Set<ProtectedRegion> getCaptureZonesRed() {
        return captureZonesRed;
    }

    public Set<ProtectedRegion> getCaptureZonesBlue() {
        return captureZonesBlue;
    }

    public Set<ProtectedRegion> getCaptureZones() {
        return Sets.union(getCaptureZonesBlue(), getCaptureZonesRed());
    }

    public Set<ProtectedRegion> getBuildZones() {
        return buildZones;
    }

    public Set<ProtectedRegion> getNoBuildZones() {
        return noBuildZones;
    }

    public BiMultiMap<CTMDirection, ProtectedRegion> getDirectionMap() {
        return directionMap;
    }

    public int getMaxPlayers() {
        return getMetaData().containsKey(PLAYERS) ? Integer.valueOf(getMetaData().get(PLAYERS)) : DEFAULT_PLAYER_AMOUNT;
    }

    @Override
    public void setup(int localId) {
        super.setup(localId);

        try {

            spawnRegionRed = getRegionMap().get(SPAWN_REGION_RED);
            spawnRegionBlue = getRegionMap().get(SPAWN_REGION_BLUE);

            for (ProtectedRegion region : getSpawnRegions()) {
                region.setFlag(DefaultFlag.WATER_FLOW, State.DENY);
                region.setFlag(DefaultFlag.LAVA_FLOW, State.DENY);
                region.setFlag(DefaultFlag.BUILD, State.DENY);
                region.setFlag(DefaultFlag.INTERACT, State.ALLOW);
            }

            addBlocksGeneratedTask(() -> forEachDropper(dropper -> miniFeasts.add(dropper.getLocation())));

            // Capture Zone Blue is on Red side and Capture Zone Red is on Blue side
            for (Entry<String, ProtectedRegion> entry : getRegionMap().entrySet()) {
                if (entry.getKey().startsWith(CAPTURE_ZONE_RED)) {
                    directionMap.put(CTMDirection.getDirectionFromRegionString(entry.getKey()), entry.getValue());
                    captureZonesRed.add(entry.getValue());
                } else if (entry.getKey().startsWith(CAPTURE_ZONE_BLUE)) {
                    directionMap.put(CTMDirection.getDirectionFromRegionString(entry.getKey()), entry.getValue());
                    captureZonesBlue.add(entry.getValue());
                } else if (entry.getKey().startsWith(BUILD_ZONE)) {
                    buildZones.add(entry.getValue());
                } else if (entry.getKey().startsWith(NO_BUILD_ZONE)) {
                    noBuildZones.add(entry.getValue());
                }
            }

            for (ProtectedRegion region : getCaptureZones()) {
                region.setFlag(DefaultFlag.LAVA_FLOW, State.DENY);
                region.setFlag(DefaultFlag.WATER_FLOW, State.DENY);
                region.setFlag(DefaultFlag.BLOCK_PLACE, State.DENY);
                region.setFlag(DefaultFlag.INTERACT, State.ALLOW);
            }

        } catch (Throwable throwable) {
            Log.exception(getName() + " is broken", throwable);
            if (PvPDojo.get().getOwner() != null) {
                BungeeSync.sendMessage(PvPDojo.get().getOwner(), CC.DARK_RED + "An error occurred while loading CTM map " + getName());
            }
            BukkitUtil.shutdown();
        }

    }

    public enum CTMDirection {
        LEFT, MIDDLE, RIGHT, FRONT, BACK;

        public static CTMDirection getDirectionFromRegionString(String region) {
            for (CTMDirection direction : values()) {
                if (region.contains(direction.name().toLowerCase())) {
                    return direction;
                }
            }
            return null;
        }

    }

}
