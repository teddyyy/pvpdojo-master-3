/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.SpawnEgg;

import com.google.common.base.CaseFormat;

public enum SessionType {
    NONE,
    DUEL_CASUAL(new ItemStack(Material.STONE_SWORD)),
    DUEL_CASUAL_NO_ABILITY(new ItemStack(Material.WOOD_SWORD)),
    TEAM_DUEL(new ItemStack(Material.IRON_SWORD)),
    BOT_DUEL(new ItemStack(Material.SKULL_ITEM)),
    TEAM_DUEL_RANKED(new ItemStack(Material.IRON_CHESTPLATE)),
    C_T_M(new SpawnEgg(EntityType.MUSHROOM_COW).toItemStack()),
    DUEL_RANKED(new ItemStack(Material.DIAMOND_SWORD)),
    FEAST_FIGHT(new ItemStack(Material.ENCHANTMENT_TABLE)),
    TOURNAMENT,
    DOJOGAMES(new ItemStack(Material.CHEST)),
    HG(new ItemStack(Material.MUSHROOM_SOUP)),
    LAVA_DUEL(new ItemStack(Material.LAVA_BUCKET)),
    POKER(new ItemStack(Material.GOLD_INGOT));

    private ItemStack icon;

    SessionType() {}

    SessionType(ItemStack icon) {
        this.icon = icon;
    }

    public ItemStack getIcon() {
        return icon;
    }

    public String getName() {
        return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, name());
    }
}
