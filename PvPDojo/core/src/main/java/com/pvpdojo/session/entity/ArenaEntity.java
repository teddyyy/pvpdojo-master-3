/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.entity;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.lang.LocaleReceiver;
import com.pvpdojo.session.GameSession;
import com.pvpdojo.session.RelogData;
import com.pvpdojo.userdata.User;

import net.md_5.bungee.api.chat.BaseComponent;

public interface ArenaEntity extends LocaleReceiver {

    // This is used to gracefully shutdown a session without a winner
    ArenaEntity EVERYONE = new PoisonArenaEntity();

    GameSession getSession();

    void setSession(GameSession session);

    @Override
    default void sendMessage(LocaleMessage message) {
        getPlayers().stream().map(User::getUser).filter(Objects::nonNull).forEach(player -> player.sendMessage(message));
    }

    default void sendMessage(BaseComponent... components) {
        getPlayers().forEach(player -> player.spigot().sendMessage(components));
    }

    default void teleport(Location location) {
        getPlayers().forEach(player -> player.teleport(location));
    }

    default void setFrozen(boolean freeze) {
        getPlayers().forEach(player -> player.setFrozen(freeze));
    }

    default void playSound(Location location, Sound sound, float volume, float pitch) {
        getPlayers().forEach(player -> player.playSound(location, sound, volume, pitch));
    }

    String getName();

    default String getLongName() {
        return getName();
    }

    UUID getUniqueId();

    List<Player> getPlayers();

    TeamArenaEntity getTeam();

    void setTeam(TeamArenaEntity team);

    default Player getPlayer() {
        return getPlayers().get(0);
    }

    default boolean isOnline() {
        if (getPlayer() != null) {
            return getPlayer().isOnline();
        }
        return !(this instanceof RelogData);
    }

}
