/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.network;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.pvpdojo.DBCommon;
import com.pvpdojo.party.Party;
import com.pvpdojo.party.PartyList;
import com.pvpdojo.session.GameSession;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.arena.Arena;
import com.pvpdojo.session.network.component.ClanComponent;
import com.pvpdojo.session.network.component.PokerComponent;
import com.pvpdojo.session.network.component.SessionComponent;
import com.pvpdojo.session.network.component.SpecialComponent;
import com.pvpdojo.settings.SettingsType;
import com.pvpdojo.settings.UserSettings;
import com.pvpdojo.userdata.PlayStyle;
import com.pvpdojo.util.bukkit.CC;

public abstract class SessionParser<T extends GameSession, A extends Arena> {

    private static final Set<String> COMPONENT_NAMES = Stream.of(SessionComponent.values()).map(SessionComponent::name).collect(Collectors.toSet());

    private String[] source;
    private SessionComponent currentSearch;
    private int cursor;
    private boolean resetOnTypeRead;

    protected Reader reader;
    protected A arena;
    protected SpecialComponent specialComponent;
    protected String forcedMap;
    protected PlayStyle playStyle;

    public SessionParser(String[] source) {
        this.source = source;
    }

    public T parse() {

        // Pass components to desired reader
        for (cursor = 0; cursor < source.length; cursor++) {
            String input = source[cursor];
            if (COMPONENT_NAMES.contains(input)) {
                currentSearch = SessionComponent.valueOf(input);
            } else {
                if (currentSearch != null) {
                    apply(input);
                }
            }
        }

        // Search for a suitable arena. Respect players settings regarding maps
        this.arena = computeArena(reader.getPlayers().stream()
                                        .map(uuid -> DBCommon.loadSettings(SettingsType.USER, uuid))
                                        .filter(Objects::nonNull)
                                        .map(UserSettings::getDisabledMaps)
                                        .flatMap(Set::stream)
                                        .collect(Collectors.toSet()));

        T session = reader.finish();
        session.setSpecialComponent(specialComponent);

        return session;
    }

    protected abstract A computeArena(Set<String> disabledBySettings);

    public Set<UUID> getPlayers() {
        return reader.getPlayers();
    }

    protected void apply(String input) {
        if (reader == null && currentSearch != null && currentSearch != SessionComponent.TYPE) {
            resetOnTypeRead = true;
            return;
        }

        switch (currentSearch) {
            case TYPE:
                if (reader == null) {
                    this.reader = getReader(SessionType.valueOf(input));
                    if (resetOnTypeRead) {
                        cursor = 0;
                    }
                }
                break;
            case PARTY:
                reader.readParty(UUID.fromString(input));
                break;
            case PLAYER:
                reader.readPlayer(UUID.fromString(input));
                break;
            case MAP:
                this.forcedMap = input;
                break;
            case PLAYSTYLE:
                this.playStyle = PlayStyle.valueOf(input);
                break;
            case SPECIAL_CLAN:
                this.specialComponent = DBCommon.GSON.fromJson(input, ClanComponent.class);
                break;
            case SPECIAL_POKER:
                this.specialComponent = DBCommon.GSON.fromJson(input, PokerComponent.class);
                break;
            default:
                break;
        }
    }

    protected abstract Reader getReader(SessionType type);

    protected abstract class Reader {

        protected SessionType sessionType;
        protected Set<UUID> players = new HashSet<>();

        Reader(SessionType sessionType) {
            this.sessionType = sessionType;
        }

        abstract void readParty(UUID party);

        void readPlayer(UUID player) {
            players.add(player);
        }

        public Set<UUID> getPlayers() {
            return players;
        }

        public SessionType getSessionType() {
            return sessionType;
        }

        protected abstract T finish();

    }

    protected abstract class DefaultReader extends Reader {

        protected DefaultReader(SessionType sessionType) {
            super(sessionType);
        }

        @Override
        void readParty(UUID party) {
            throw new UnsupportedOperationException();
        }

    }

    protected abstract class TeamReader extends Reader {

        protected List<CC> colors = new ArrayList<>(Arrays.asList(CC.RED, CC.BLUE, CC.YELLOW, CC.GREEN));
        protected List<Party> parties = new ArrayList<>();

        protected TeamReader(SessionType sessionType) {
            super(sessionType);
        }

        @Override
        void readParty(UUID partyId) {
            parties.add(PartyList.getPartyById(partyId));
        }

    }

}
