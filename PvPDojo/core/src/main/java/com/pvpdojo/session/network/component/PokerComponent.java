/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.network.component;

import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.session.GameSession;
import com.pvpdojo.session.RelogData;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.userdata.AbilityData;
import com.pvpdojo.userdata.KitAbilityData;
import com.pvpdojo.userdata.KitData;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.Log;

import lombok.Getter;

public class PokerComponent extends SpecialComponent {

    private Map<UUID, Integer> kits;

    @Getter
    private transient PersistentData winnerData, loserData;

    public PokerComponent(Map<UUID, Integer> kits) {
        this.kits = kits;
    }

    @Override
    public void handlePreSession(GameSession session) {
        session.handleSpecialComponentGameSpecific();
    }

    @Override
    public void handleAfterSession(GameSession session) {
        try {
            if (winnerData != null && loserData != null) {
                KitData loserKit = loserData.getKit(kits.get(loserData.getUUID()));
                KitAbilityData loserAbility = loserKit.getAbilities().get(PvPDojo.RANDOM.nextInt(loserKit.getAbilities().size()));

                AbilityData ability = loserData.getAbility(loserAbility.getId());

                loserKit.removeAbility(loserAbility.getId());
                loserData.updateKit(loserKit);

                PvPDojo.schedule(() -> {
                    PersistentData.updateAbilityOwnership(winnerData.getUUID(), ability.getAbilityUUID(), "Poker");
                    BungeeSync.updateUser(winnerData.getUUID());
                    BungeeSync.updateUser(loserData.getUUID());
                }).createAsyncTask();

                new PseudoSelection(Bukkit.getPlayer(winnerData.getUUID()), ability.getLoader(), loserKit.getAbilities().size() + 1).open();
                User.getUser(loserData.getUUID()).sendMessage(MessageKeys.POKER_LOST, "{ability}", ability.getLoader().getDisplayName() + " Ability");
            } else {
                session.broadcast(LocaleMessage.of(CC.DARK_RED + "An error occurred while handling this Poker match"));
            }
        } catch (Throwable thrown) {
            Log.exception(thrown);
            session.broadcast(MessageKeys.ERROR);
        }
        session.handleSpecialComponentGameSpecific();
    }

    @Override
    public void handleWinner(ArenaEntity winner) {
        if (winner instanceof User) {
            User user = (User) winner;
            winnerData = user.getPersistentData();
        } else if (winner instanceof RelogData) {
            winnerData = ((RelogData) winner).getPersistentData();
        }
    }

    @Override
    public void handleLoser(ArenaEntity entity) {
        if (entity instanceof User) {
            User user = (User) entity;
            loserData = user.getPersistentData();
        } else if (entity instanceof RelogData) {
            loserData = ((RelogData) entity).getPersistentData();
        }
    }

    public Integer getKit(UUID uuid) {
        return kits.get(uuid);
    }

    private static class PseudoSelection extends SingleInventoryMenu {

        private final AbilityLoader loader;
        private final int amount;

        public PseudoSelection(Player player, AbilityLoader loader, int amount) {
            super(player, "Choose a random ability", 9);
            this.loader = loader;
            this.amount = amount;
            setCloseable(false);
        }

        @Override
        public void redraw() {
            super.redraw();

            for (int i = 0; i < amount; i++) {
                page.setItem(i, new ItemBuilder(Material.CHEST).name(CC.RED + "???").build(), type -> {
                    User.getUser(getPlayer()).sendMessage(MessageKeys.POKER_WON, "{ability}", loader.getDisplayName());

                    closeSafely();
                    PvPDojo.schedule(() -> ServerType.HUB.connect(getPlayer())).createTask(20);
                });
            }

        }
    }

}
