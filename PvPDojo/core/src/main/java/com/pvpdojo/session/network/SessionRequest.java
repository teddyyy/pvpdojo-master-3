/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.network;

import java.time.Duration;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.StringJoiner;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import com.google.common.collect.Iterables;
import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.map.DojoMap;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.network.component.ClanComponent;
import com.pvpdojo.session.network.component.PokerComponent;
import com.pvpdojo.session.network.component.SessionComponent;
import com.pvpdojo.userdata.PlayStyle;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class SessionRequest {

    private SessionType type;
    private PlayStyle playStyle;
    private Set<UUID> players = new HashSet<>();
    private StringJoiner requestBuilder = new StringJoiner("|");
    private SessionComponent lastComponent;
    private String server;

    public SessionRequest() {}

    private SessionRequest(String string) {
        requestBuilder.add(string);
    }

    public static SessionRequest fromString(String string) {
        return new SessionRequest(string);
    }

    protected void ensureComponent(SessionComponent component) {
        if (lastComponent != component) {
            requestBuilder.add(component.name());
            this.lastComponent = component;
        }
    }

    public SessionRequest type(SessionType type) {
        ensureComponent(SessionComponent.TYPE);
        this.type = type;
        requestBuilder.add(type.name());
        return this;
    }

    public SessionRequest players(UUID... players) {
        return players(Arrays.asList(players));
    }

    public SessionRequest players(Iterable<UUID> players) {
        ensureComponent(SessionComponent.PLAYER);
        for (UUID player : players) {
            requestBuilder.add(player.toString());
        }
        Iterables.addAll(this.players, players);
        return this;
    }

    public SessionRequest parties(UUID... parties) {
        return parties(Arrays.asList(parties));
    }

    public SessionRequest parties(Iterable<UUID> parties) {
        ensureComponent(SessionComponent.PARTY);
        for (UUID party : parties) {
            requestBuilder.add(party.toString());
        }
        return this;
    }

    public SessionRequest map(DojoMap map) {
        ensureComponent(SessionComponent.MAP);
        requestBuilder.add(map.getName());
        return this;
    }

    public SessionRequest special(PokerComponent pokerComponent) {
        ensureComponent(SessionComponent.SPECIAL_POKER);
        String json = DBCommon.GSON.toJson(pokerComponent);
        Log.info(json);
        requestBuilder.add(json);
        return this;
    }

    public SessionRequest special(ClanComponent clanComponent) {
        ensureComponent(SessionComponent.SPECIAL_CLAN);
        String json = DBCommon.GSON.toJson(clanComponent);
        Log.info(json);
        requestBuilder.add(json);
        return this;
    }

    public SessionRequest playStyle(PlayStyle playStyle) {
        if (playStyle != null) {
            ensureComponent(SessionComponent.PLAYSTYLE);
            this.playStyle = playStyle;
            requestBuilder.add(playStyle.toString());
        }
        return this;
    }

    public SessionRequest server(String server) {
        this.server = server;
        return this;
    }

    public SessionType getType() {
        return type;
    }

    public Set<UUID> getPlayers() {
        return players;
    }

    public PlayStyle getPlayStyle() {
        return playStyle;
    }

    public String build() {
        return requestBuilder.toString();
    }

    public void sendToServer(ServerType type) {
        sendToServer(type, null);
    }

    public void sendToServer(ServerType type, Runnable onFail) {

        CompletableFuture<Boolean> future = new CompletableFuture<>();
        BungeeSync.addTimeOut(future, Duration.ofSeconds(7));

        PvPDojo.newChain()
               .asyncFirst(() -> {

                   String leastPlayerCount = server != null ? server : BungeeSync.findServerTypeWithLeastPlayers(type);

                   if (leastPlayerCount == null) {
                       future.complete(false);
                       return null;
                   }

                   int serverId = Integer.valueOf(leastPlayerCount.split("_")[1]);
                   int requestId = BungeeSync.getNextId(BungeeSync.SESSION_ID_COUNTER, 1_000_000);

                   BungeeSync.SESSION_REQUESTS.put(requestId, future);
                   Redis.get().publish(BungeeSync.SESSION, BungeeSync.REQUEST + "|" + serverId + "|" + requestId + "|" + build()); // Magic happens here

                   return leastPlayerCount;
               }).storeAsData("server")
               .future(future)
               .async(input -> {
                   if (input == null) {
                       getPlayers().forEach(uuid -> BungeeSync.sendMessage(uuid, PvPDojo.WARNING + CC.RED + "Request timed out"));
                       PvPDojo.schedule(onFail).sync();
                   }
                   return input;
               }).abortIfNull()
                .<String>returnData("server")
                .asyncLast(serverName -> {
                    if (serverName == null && onFail != null) {
                        onFail.run();
                    }
                    getPlayers().forEach(uuid -> {
                        if (serverName == null) { // lol could not find a server
                            BungeeSync.sendMessage(uuid, PvPDojo.WARNING + CC.RED + "Could not reach server");
                        } else {
                            PlayerUtil.sendToServer(uuid, serverName);
                        }
                    });

                }).execute();
    }

}
