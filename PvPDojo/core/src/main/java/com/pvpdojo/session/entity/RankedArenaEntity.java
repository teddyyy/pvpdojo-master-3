/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.session.entity;

public interface RankedArenaEntity extends ArenaEntity {

    int getElo();

    void addElo(int elo);

}
