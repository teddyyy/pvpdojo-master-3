/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menus;

import java.util.Map;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.settings.ServerSwitchSettings;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class ServerMenu extends SingleInventoryMenu {


    public ServerMenu(Player player) {
        super(player, "Server Menu", 9);
        setUpdateInterval(100);
    }

    int loop = 0;

    @Override
    public void redraw() {

        Map<String, Integer> servers = BungeeSync.getServerPlayerCount();

        InventoryPage inv = getCurrentPage();
        inv.setSizeForNeededSlots(servers.size());

        servers.forEach((name, playerCount) -> inv.setItem(loop++, new ItemBuilder(Material.COMMAND)
                        .name(CC.GREEN + name)
                        .amount(playerCount)
                        .lore("", CC.YELLOW + "" + playerCount + " players online")
                        .build(),
                clickType -> PlayerUtil.sendToServer(getPlayer(), name, new ServerSwitchSettings(User.getUser(getPlayer()))
                        .adminMode(User.getUser(getPlayer()).isAdminMode()))
        ));
        loop = 0;
        inv.fillBlank();
    }
}
