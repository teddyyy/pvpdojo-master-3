/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menus;

import java.util.Collection;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.DeveloperSettings;
import com.pvpdojo.menu.SortType;
import com.pvpdojo.menu.SortedInventoryMenu;
import com.pvpdojo.menu.SortedItem;
import com.pvpdojo.userdata.AbilityData;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder.ItemEditor;

public class AbilitiesMenu extends SortedInventoryMenu {

    private Collection<AbilityData> data;
    private BiConsumer<AbilityData, ClickType> response;
    private Predicate<AbilityData> skipper;
    private Function<AbilityData, List<String>> loreFunction;
    private boolean highestGrade = false;

    public AbilitiesMenu(Player player, Collection<AbilityData> data, BiConsumer<AbilityData, ClickType> response, Predicate<AbilityData> skipper) {
        this(player, data, response, skipper, null);
    }

    public AbilitiesMenu(Player player, Collection<AbilityData> data, BiConsumer<AbilityData, ClickType> response, Predicate<AbilityData> skipper, Function<AbilityData, List<String>> loreFunction) {
        super(player, "Abilities", 54, SortType.ALPHABETICAL, SortType.RARITY);
        this.data = data;
        this.response = response;
        this.skipper = skipper;
        this.loreFunction = loreFunction;
    }

    public AbilitiesMenu highestGrade() {
        highestGrade = !DeveloperSettings.freeAbilities(User.getUser(getPlayer()));
        return this;
    }

    @Override
    public void redraw() {
        super.redraw();
        for (AbilityData ability : data) {

            if (ability.getId() < 0) {
                continue;
            }

            if (ability.getLoader() == null) {
                getPlayer().sendMessage(CC.RED + "You have an invalid ability, report to a staff member to fix this.");
                continue;
            }

            if (skipper != null && skipper.test(ability)) {
                continue;
            }

            ItemStack item = ability.getLoader().getAbilityIcon();
            List<String> lore = loreFunction != null ? loreFunction.apply(ability) : ability.getLoader().fixAbilityDescription(User.getUser(getPlayer()), null);

            new ItemEditor(item).lore(lore).build();
            SortedItem sort = new SortedItem(item, clickType -> response.accept(ability, clickType));
            sort.setSortName(ability.getLoader().getName());
            sort.setSortValue(ability.getLoader().getRarity().ordinal());

            addSortItem(sort);
        }
    }

    public static SortedInventoryMenu getAbilitiesMenu(Player player, Consumer<AbilityData> response) {
        return new AbilitiesMenu(player, User.getUser(player).getPersistentData().getAbilities(), ((abilityData, clickType) -> response.accept(abilityData)), null);
    }

}
