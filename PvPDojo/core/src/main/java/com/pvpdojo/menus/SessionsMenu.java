/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menus;

import static com.pvpdojo.util.StringUtils.getDateFromMillis;
import static com.pvpdojo.util.StringUtils.getEnumName;
import static com.pvpdojo.util.StringUtils.getReadableSeconds;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.MapMaker;
import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.menu.AsyncSortedInventoryMenu;
import com.pvpdojo.menu.AsyncSortedItem;
import com.pvpdojo.menu.SortType;
import com.pvpdojo.session.PastGameSession;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class SessionsMenu extends AsyncSortedInventoryMenu {

    private Map<Integer, Long> sessions;
    private Map<UUID, DojoOfflinePlayer> offlinePlayerCache = new MapMaker().weakValues().makeComputingMap(PvPDojo::getOfflinePlayer);

    public SessionsMenu(Player player, Map<Integer, Long> sessions) {
        super(player, "Sessions", 54, SortType.NEW_TO_OLD, SortType.OLD_TO_NEW);
        this.sessions = sessions;
    }

    @Override
    public void redraw() {
        super.redraw();

        for (Map.Entry<Integer, Long> session : sessions.entrySet()) {
            AsyncSessionItem item = new AsyncSessionItem(clickType -> new SessionMenu(getPlayer(), session.getKey()).open(), session.getKey());
            item.setSortValue(session.getValue());
            addSortItem(item);
        }
    }

    public class AsyncSessionItem extends AsyncSortedItem<Integer> {

        public AsyncSessionItem(Consumer<ClickType> action, Integer key) {
            super(action, key);
        }

        @Override
        public ItemStack processItemAsync() {
            PastGameSession session = DBCommon.getGameSession(getKey());
            if (session != null) {
                ItemBuilder builder = new ItemBuilder(session.getType().getIcon());

                List<String> lore = new ArrayList<>();
                lore.add(CC.GRAY + "Date" + PvPDojo.POINTER + CC.DARK_AQUA + getDateFromMillis(session.getDate()));
                lore.add(CC.GRAY + "Length" + PvPDojo.POINTER + CC.DARK_AQUA + getReadableSeconds(session.getDurationSeconds()));

                lore.add("");

                if (session.getWinners().size() <= 8) {
                    lore.add(CC.GREEN + "Winners " + (session.getEloChange() != 0 ? "(+" + session.getEloChange() + ")" : ""));
                    session.getWinners().forEach(winner -> {
                        if (winner.hasColor()) {
                            lore.add(CC.GRAY + " Team " + winner.getColor() + getEnumName(winner.getColor()));
                        }
                        winner.getMembers().forEach(uuid -> lore.add((winner.hasColor() ? CC.GRAY + "  - " + winner.getColor() : CC.GRAY + " ") + offlinePlayerCache.get(uuid).getNick()));
                    });
                } else {
                    lore.add(CC.GREEN + "Winners (...)");
                }

                lore.add("");

                if (session.getLosers().size() <= 8) {
                    lore.add(CC.RED + "Losers " + (session.getEloChange() != 0 ? "(-" + session.getEloChange() + ")" : ""));
                    session.getLosers().forEach(loser -> {
                        if (loser.hasColor()) {
                            lore.add(CC.GRAY + " Team " + loser.getColor() + getEnumName(loser.getColor()));
                        }
                        loser.getMembers().forEach(uuid -> lore.add((loser.hasColor() ? CC.GRAY + "  - " + loser.getColor() : CC.GRAY + " ") + offlinePlayerCache.get(uuid).getNick()));
                    });
                } else {
                    lore.add(CC.GREEN + "Losers (...)");
                }

                builder.name(CC.GREEN + getEnumName(session.getType(), true)).lore(lore);

                return builder.build();
            }
            return null;
        }
    }

}
