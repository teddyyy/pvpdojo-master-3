/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menus;

import static com.pvpdojo.util.StringUtils.getDateFromMillis;

import java.sql.SQLException;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimaps;
import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilityinfo.AbilityLog;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.mysql.SQL;
import com.pvpdojo.mysql.SQLBuilder;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.userdata.AbilityData;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.impl.DojoOfflinePlayerImpl;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.SkullBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.WoolBuilder;
import com.pvpdojo.util.Log;

import co.aikar.taskchain.TaskChainDataWrappers.Data2;

public class ProfileMenu extends SingleInventoryMenu {

    private ProfileView view;
    private DojoOfflinePlayer other;

    public ProfileMenu(Player player) {
        super(player, "Your Profile", 27);
        view = ProfileView.OWN;
    }

    public ProfileMenu(Player player, DojoOfflinePlayer other) {
        super(player, "Profile of " + CC.BLUE + other.getName(), 27);
        this.other = other;
        view = ProfileView.OTHER;
    }

    @Override
    public void redraw() {
        super.redraw();
        switch (view) {
            case OWN:
                drawOwn();
                break;
            case OTHER:
                drawOther();
                break;
            case ACCOUNT_GRID:
                drawAccountGrid();
                break;
        }
    }

    private void drawOwn() {
        InventoryPage page = getCurrentPage();
        User user = User.getUser(getPlayer());

        page.setItem(10, new SkullBuilder(getPlayer().getName()).name(CC.RED + "Stats").build(),
                type -> new StatsMenu(getPlayer(), new DojoOfflinePlayerImpl(User.getUser(getPlayer()))).open());

        page.setItem(12, new ItemBuilder(Material.TRIPWIRE_HOOK).name(CC.GREEN + "Settings").build(), type -> new SettingsMenu(getPlayer()).open());

        page.setItem(14, new ItemBuilder(Material.BLAZE_POWDER)
                .name(user.getClan() != null ? CC.DARK_GRAY + "[" + CC.AQUA + user.getClan().getName() + CC.DARK_GRAY + "]" : CC.RED + "None").build(), null);

        page.setItem(16, new ItemBuilder(Material.CAKE).name(CC.RED + "Party").build(), null);

        page.fillBlank();
    }

    private void drawOther() {
        InventoryPage page = getCurrentPage();

        page.setItem(9, new ItemBuilder(Material.IRON_SWORD).name(CC.RED + "Stats").build(), clickType -> new StatsMenu(getPlayer(), other).open());
        page.setItem(14, new ItemBuilder(Material.WORKBENCH).name(CC.YELLOW + "Abilities").lore(CC.RED + "Admins can right click to remove them").build(), ignored -> {
            setLocked(true);
            Set<AbilityData> filter = new HashSet<>();
            PvPDojo.newChain().asyncFirst(() -> {

                Map<UUID, DojoOfflinePlayer> offlinePlayerCache = new HashMap<>();
                ListMultimap<AbilityData, AbilityLog> log = Multimaps.newListMultimap(new HashMap<>(), ArrayList::new);
                other.getPersistentData().readAbilities(null);
                try (SQLBuilder sql = new SQLBuilder()) {

                    sql.select(Tables.ABILITY_LOG, "*")
                       .where("ability_uuid")
                       .in(other.getPersistentData().getAbilities().stream()
                                .map(AbilityData::getAbilityUUID)
                                .map(uuid -> "'" + uuid + "'")
                                .collect(Collectors.joining(",")))
                       .orderBy("date", true)
                       .executeQuery();

                    while (sql.next()) {
                        AbilityLog abilityLog = AbilityLog.fromSQL(sql);
                        log.put(other.getPersistentData().getAbility(abilityLog.getAbilityUUID()), abilityLog);
                        if (!offlinePlayerCache.containsKey(abilityLog.getPreviousOwner())) {
                            offlinePlayerCache.put(abilityLog.getPreviousOwner(), PvPDojo.getOfflinePlayer(abilityLog.getPreviousOwner()));
                        }
                        if (!offlinePlayerCache.containsKey(abilityLog.getNewOwner())) {
                            offlinePlayerCache.put(abilityLog.getNewOwner(), PvPDojo.getOfflinePlayer(abilityLog.getNewOwner()));
                        }
                    }
                } catch (SQLException e) {
                    Log.exception("Pulling ability logs", e);
                }
                return new Data2<>(log, offlinePlayerCache);
            }).syncLast(abilities -> new AbilitiesMenu(getPlayer(), abilities.var1.keySet(), (abilityData, clickType) -> {
                if (clickType == ClickType.RIGHT && User.getUser(getPlayer()).getRank().inheritsRank(Rank.DEVELOPER)) {
                    PersistentData.updateAbilityOwnership(null, abilityData.getAbilityUUID(), getPlayer().getName());
                    filter.add(abilityData);
                    User.getUser(getPlayer()).getMenu().redrawAndOpen();
                }
            }, filter::contains, abilityData -> {
                List<AbilityLog> log = abilities.var1.get(abilityData);
                Collections.sort(log);
                return log.stream().map(abilityLog -> CC.GRAY + abilities.var2.get(abilityLog.getPreviousOwner()).getName()
                        + " > " + abilities.var2.get(abilityLog.getNewOwner()).getName() + " (" + abilityLog.getReason() + ") "
                        + getDateFromMillis(abilityLog.getDate(), FormatStyle.SHORT)).collect(Collectors.toList());
            }).open()).execute();
        });

        page.setItem(13, new ItemBuilder(Material.BOOK).name(CC.RED + "Show Punishments").build(), clickType -> new PunishmentsMenu(getPlayer(), other).open());
        page.setItem(7, new ItemBuilder(Material.WATCH).name(CC.RED + "First Join").lore(CC.GREEN + getDateFromMillis(other.getFirstPlayed())).build(), null);
        page.setItem(8, new ItemBuilder(Material.DETECTOR_RAIL).name(CC.RED + "Last Server Connect").lore(CC.GREEN + getDateFromMillis(other.getLastPlayed())).build(), null);

        PvPDojo.newChain().async(other.getPersistentData()::pullUserDataSneaky).sync(() -> {
            PersistentData db = other.getPersistentData();

            if (db.getRank() == Rank.SERVER_ADMINISTRATOR && !User.getUser(getPlayer()).getRank().inheritsRank(Rank.SERVER_ADMINISTRATOR)) {
                closeSafely();
                getPlayer().sendMessage(PvPDojo.WARNING + "You are not allowed to view this profile");
                return;
            }

            page.setItem(16, new ItemBuilder(Material.DIAMOND).name(CC.RED + "Rank" + PvPDojo.POINTER + db.getRank().getPrefix() + db.getRank().getName()).build(), null);
            page.setItem(17, new ItemBuilder(Material.GOLD_INGOT).name(CC.RED + "Money" + PvPDojo.POINTER + CC.YELLOW + db.getMoney()).build(), null);
            String clan = db.getClan();
            page.setItem(10, new ItemBuilder(Material.BLAZE_POWDER).name(clan != null ? CC.DARK_GRAY + "[" + CC.AQUA + clan + CC.DARK_GRAY + "]" : CC.RED + "None").build(), null);
            open();
        }).execute();

        page.setItem(12, new SkullBuilder(other.getName()).name(CC.RED + "Account Grid").build(), clickType -> {
            view = ProfileView.ACCOUNT_GRID;
            redrawAndOpen();
        });

        page.fillBlank();
    }

    private void drawAccountGrid() {
        InventoryPage page = getCurrentPage();
        page.setHasBar(true);
        setBackFunction(clickType -> {
            view = ProfileView.OTHER;
            page.setSize(27);
            redrawAndOpen();
        });
        PvPDojo.newChain().asyncFirst(() -> {
            try {
                return DBCommon.getAccountGrid(other.getUniqueId()).stream().map(PvPDojo::getOfflinePlayer)
                               .peek(account -> account.getPersistentData().pullUserDataSneaky()).collect(Collectors.toList());
            } catch (SQLException e) {
                Log.exception("Pulling account grid", e);
            }
            return null;
        }).abortIfNull().syncLast(accounts -> {
            User user = User.getUser(getPlayer());

            if (accounts.stream().map(DojoOfflinePlayer::getPersistentData).allMatch(db -> user.getRank().inheritsRank(db.getRank()))) {
                page.setSizeForNeededSlots(9 + accounts.size());
                for (int i = 0; i < accounts.size(); i++) {
                    DojoOfflinePlayer acc = accounts.get(i);
                    page.setItem(i + 9, new SkullBuilder(acc.getName()).name(CC.RED + acc.getName()).lore(
                            CC.RED + "First Join" + PvPDojo.POINTER + CC.GOLD + getDateFromMillis(acc.getFirstPlayed()),
                            CC.RED + "Last Connect" + PvPDojo.POINTER + CC.GOLD + getDateFromMillis(acc.getLastPlayed()),
                            CC.RED + "Last IP" + PvPDojo.POINTER + CC.GOLD + acc.getLastIpAddress(),
                            CC.RED + "Banned" + PvPDojo.POINTER + (acc.isBanned() ? CC.DARK_RED + acc.getCurrentBan().getReason() : CC.GREEN + "No")).build(),
                            clickType -> {
                                if (clickType == ClickType.RIGHT && User.getUser(getPlayer()).getRank().inheritsRank(Rank.MODERATOR_PLUS)) {
                                    PvPDojo.newChain()
                                           .async(() -> {
                                               try {
                                                   SQL.remove(Tables.ACCOUNT_GRID, "uuid = ?", acc.getUniqueId().toString());
                                               } catch (SQLException e) {
                                                   Log.exception("Removing from account grid " + acc.getUniqueId(), e);
                                                   getPlayer().sendMessage(CC.DARK_RED + "DB ERROR");
                                               }
                                           })
                                           .sync(this::redrawAndOpen)
                                           .execute();
                                } else {
                                    getPlayer().performCommand("profile " + acc.getName());
                                }
                            });
                }
            } else {
                page.setItem(9, new WoolBuilder(DyeColor.RED).name(CC.RED + "Denied").build(), null);
            }

            page.fillBlank();
            open();
        }).execute();
    }

    public enum ProfileView {
        OWN, OTHER, ACCOUNT_GRID,
    }

}
