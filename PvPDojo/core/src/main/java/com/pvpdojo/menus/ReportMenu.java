/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menus;

import java.util.Comparator;
import java.util.stream.Collectors;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.redis.Report;
import com.pvpdojo.redis.Report.ReportType;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class ReportMenu extends SingleInventoryMenu {


    public ReportMenu(Player player) {
        super(player, "Report Menu", 9);
        setUpdateInterval(20);
    }

    @Override
    public void redraw() {
        InventoryPage inv = getCurrentPage();

        PvPDojo.newChain()
               .asyncFirst(() -> Multimaps.index(
                       Redis.get().getHAll(BungeeSync.REPORT)
                            .values().stream()
                            .map(json -> DBCommon.GSON.fromJson(json, Report.class))
                            .sorted()
                            .collect(Collectors.toList()),
                       Report::getTarget))
               .syncLast(result -> {
                   inv.setSizeForNeededSlots(result.keySet().size());
                   super.redraw();

                   Multimap<DojoOfflinePlayer, Report> reports = ImmutableMultimap.<DojoOfflinePlayer, Report>builder().orderKeysBy(valueOrder(result)).putAll(result).build();

                   int i = 0;
                   for (DojoOfflinePlayer target : reports.keySet()) {

                       inv.setItem(i++, new ItemBuilder.SkullBuilder(target.getName())
                                       .name(CC.RED + "Suspect: " + target.getName())
                                       .appendLore("")
                                       .appendLore(reports.get(target).stream()
                                                          .map(report -> CC.YELLOW + report.getReporter().getName() + " (" + CC.RED + report.getReason() + CC.YELLOW + ")" +
                                                                  (report.getAdditional() != null ? " - " + report.getAdditional() : ""))
                                                          .collect(Collectors.toList())
                                       )
                                       .appendLore("", CC.GOLD + "Left " + CC.GRAY + "click to teleport", CC.GOLD + "Shift " + CC.GRAY + "click to remove")
                                       .build(),
                               clickType -> {
                                   if (clickType == ClickType.SHIFT_RIGHT || clickType == ClickType.SHIFT_LEFT) {
                                       inv.clear();
                                       removeReports(reports, target);
                                   } else {

                                       if (reports.get(target).stream().anyMatch(report -> report.getReason().equals(ReportType.CHAT))) {
                                           getPlayer().performCommand("chatlog " + target.getName());
                                       }

                                       getPlayer().performCommand("check " + target.getName());
                                   }
                               });
                       if (target.isBanned()) {
                           removeReports(reports, target);
                       }
                       if (target.isMuted()) {
                           removeReports(reports, target, ReportType.CHAT);  // idk, maybe you need something like that?
                       }
                   }
                   open();
               })
               .execute();

    }

    private Comparator<DojoOfflinePlayer> valueOrder(ImmutableListMultimap<DojoOfflinePlayer, Report> reports) {
        return (player, t1) -> reports.get(t1).get(0).compareTo(reports.get(player).get(0));
    }

    private void removeReports(Multimap<DojoOfflinePlayer, Report> reports, DojoOfflinePlayer target, ReportType reason) {
        PvPDojo.newChain()
               .async(() -> Redis.get().removeHValue(BungeeSync.REPORT,
                       reports.get(target).stream()
                              .filter(report -> reason == null || report.getReason() == reason)
                              .filter(report -> !report.getReporter().getUniqueId().equals(PvPDojo.SERVER_UUID)
                                      || User.getUser(getPlayer()).getRank().inheritsRank(Rank.DEVELOPER))
                              .map(report -> String.valueOf(report.getId()))
                              .toArray(String[]::new)
               ))
               .sync(this::redraw)
               .execute();
    }

    private void removeReports(Multimap<DojoOfflinePlayer, Report> reports, DojoOfflinePlayer target) {
        removeReports(reports, target, null);
    }
}
