/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menus;

import static com.pvpdojo.util.StringUtils.getEnumName;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.dataloader.KitLoader;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.userdata.KitAbilityData;
import com.pvpdojo.userdata.KitData;
import com.pvpdojo.userdata.PlayStyle;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.ItemEditor;

public class KitSelectorMenu extends SingleInventoryMenu {

    private Consumer<KitData> callback;
    private boolean forceEmptyOption;
    private PlayStyle forcedPlayStyle;

    public KitSelectorMenu(Player player, boolean closeable, Consumer<KitData> callback, boolean forceEmptyOption, PlayStyle forcedPlayStyle) {
        this(player, closeable, callback, forceEmptyOption);
        this.forcedPlayStyle = forcedPlayStyle;
    }

    public KitSelectorMenu(Player player, boolean closeable, Consumer<KitData> callback, boolean forceEmptyOption) {
        this(player, closeable, callback);
        this.forceEmptyOption = forceEmptyOption;
    }

    public KitSelectorMenu(Player player, boolean closeable, Consumer<KitData> callback) {
        super(player, "Select your custom kit", 9);
        this.callback = callback;
        setCloseable(closeable);
        setCancelCloseMessage(CC.RED + "You must select your champion first");
    }

    @Override
    public void redraw() {
        InventoryPage inv = getCurrentPage();
        User user = User.getUser(getPlayer());

        if (!isCloseable() && !user.isDataLoaded()) {
            PvPDojo.schedule(this::redrawAndOpen).createTask(5);
        }

        List<KitData> kits = user.getPersistentData().getKits();
        for (int i = 0; i < kits.size(); i++) {
            KitData data = kits.get(i).clone();

            // If playstyle is forced set it now
            if (forcedPlayStyle != null) {
                data.setPlayStyle(forcedPlayStyle);
            }

            if (i > 7)
                break;
            KitLoader loader = KitLoader.getKitData(1);
            ItemStack icon = loader.getChampionIcon();
            List<String> lore = new ArrayList<>();
            lore.add(CC.GRAY + "PlayStyle: " + CC.DARK_PURPLE + data.getPlayStyle().getName());
            lore.add(CC.GRAY + "Max Mana: " + CC.BLUE + loader.getKitBaseAttributes().getKitAttribute("max_mana").getAttributeValue() + " mana");
            lore.add(CC.GRAY + "Abilities: " + listAbilities(data));
            new ItemEditor(icon).name( CC.GREEN + data.getName()).lore(lore).build();
            inv.setItem(i, icon, p -> {
                boolean wasCloseable = isCloseable();
                setCloseable(true);
                callback.accept(data);
                if (!wasCloseable) {
                    closeSafely();
                }
            });
        }

        if (!isCloseable() || forceEmptyOption) {
            inv.setItem(8, new ItemBuilder(Material.REDSTONE).name(CC.RED + "Empty kit").build(), clickType -> {
                if (forcedPlayStyle != null) {
                    callback.accept(getEmptyKit(forcedPlayStyle));
                    closeSafely();
                } else {
                    setCloseable(true);
                    new PlaystyleSelectorMenu(getPlayer()).open();
                }
            });
        }

        inv.fillBlank();
    }

    private class PlaystyleSelectorMenu extends SingleInventoryMenu {

        public PlaystyleSelectorMenu(Player player) {
            super(player, "Select your playstyle", 9);
            setCloseable(false);
            setBackFunction(getBackFunction().andThen(type -> User.getUser(player).getMenu().setCloseable(false)));
        }

        @Override
        public void redraw() {
            InventoryPage inv = getCurrentPage();
            inv.setHasBar(true);

            PlayStyle[] values = PlayStyle.values();
            for (int i = 0; i < values.length; i++) {
                PlayStyle style = values[i];
                inv.setItem((i + 2 * i) + 1, new ItemBuilder(style.getIcon()).name(CC.GREEN + getEnumName(style)).build(), clickType -> {
                    callback.accept(getEmptyKit(style));
                    closeSafely();
                });
            }
            inv.fillBlank();
        }

    }

    private KitData getEmptyKit(PlayStyle playStyle) {
        KitData data = new KitData("Empty Kit", KitData.EMTPY_DATA);
        data.setPlayStyle(playStyle);
        return data;
    }

    private static String listAbilities(KitData data) {
        StringBuilder abilities = new StringBuilder();
        for (KitAbilityData ability : data.getAbilities()) {
            AbilityLoader loader = AbilityLoader.getAbilityData(ability.getId());
            abilities.append(loader.getRarity().getColor()).append(loader.getName()).append(", ");
        }
        if (abilities.length() != 0)
            return abilities.substring(0, abilities.length() - 2);
        return abilities.toString();
    }
}
