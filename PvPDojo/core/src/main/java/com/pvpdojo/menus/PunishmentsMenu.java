/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menus;

import static com.pvpdojo.util.PunishmentManager.Punishment;
import static com.pvpdojo.util.StringUtils.MOJANG_UUID;
import static com.pvpdojo.util.StringUtils.formatMillisDiff;
import static com.pvpdojo.util.StringUtils.getDateFromMillis;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.staff.BanCommand;
import com.pvpdojo.gui.InputGUI;
import com.pvpdojo.menu.SortType;
import com.pvpdojo.menu.SortedInventoryMenu;
import com.pvpdojo.menu.SortedItem;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.PlayerUtil;
import com.pvpdojo.util.PunishmentManager;

public class PunishmentsMenu extends SortedInventoryMenu {

    private DojoOfflinePlayer target;

    public PunishmentsMenu(Player player, DojoOfflinePlayer target) {
        super(player, "Punishment of " + CC.BLUE + target.getName(), 54, SortType.NEW_TO_OLD, SortType.OLD_TO_NEW);
        this.target = target;
    }

    @Override
    public void redraw() {
        addSortItem(new SortedItem(new ItemStack(Material.AIR), null));

        PvPDojo.newChain().asyncFirst(() -> {
            return PunishmentManager.getPunishments(target.getUniqueId()).stream().collect(Collectors.toMap(Function.identity(), punishment -> Optional.ofNullable(canEdit(punishment))));
        }).syncLast(punishments -> {
            super.redraw();

            for (Map.Entry<Punishment, Optional<Rank>> entry : punishments.entrySet()) {
                Punishment pun = entry.getKey();

                List<String> lore = new ArrayList<>();
                lore.add("");
                lore.add(CC.YELLOW + "Type" + PvPDojo.POINTER + CC.RED + pun.getType());
                lore.add(CC.YELLOW + "Date" + PvPDojo.POINTER + CC.RED + getDateFromMillis(pun.getTimestamp()));
                lore.add(CC.YELLOW + "Executor" + PvPDojo.POINTER + CC.RED + pun.getExecutor());
                lore.add(CC.YELLOW + "Reason" + PvPDojo.POINTER + CC.RED + pun.getOriginalReason());
                lore.add(CC.YELLOW + "Length" + PvPDojo.POINTER + CC.RED + (pun.getOriginalLength() == -1 ? "Permanent" : formatMillisDiff(pun.getOriginalLength() * 1000)));
                lore.add("");

                if (pun.isUpdated()) {
                    lore.add(CC.YELLOW + "Update Executor" + PvPDojo.POINTER + CC.RED + pun.getUpdateExecutor());
                    lore.add(CC.YELLOW + "Update Reason" + PvPDojo.POINTER + CC.RED + pun.getUpdateReason());
                    if (pun.isValid()) {
                        lore.add(CC.YELLOW + "Length" + PvPDojo.POINTER + CC.RED + (pun.getLength() == -1 ? "Permanent" : formatMillisDiff(pun.getLength() * 1000)));
                    } else {
                        lore.add(CC.DARK_RED + "INVALIDATED");
                    }
                    lore.add("");
                }

                if (pun.isExpired() && PlayerUtil.hasDyscalculia(getPlayer())) {
                    lore.add(CC.DARK_RED + "EXPIRED");
                    lore.add("");
                }

                if (entry.getValue().isPresent()) {
                    lore.add(CC.GRAY + "You need to be at least " + entry.getValue().get() + " to edit this punishment");
                } else {
                    lore.add(CC.GOLD + "Left " + CC.GRAY + "click to change punishment length");
                    lore.add(CC.GOLD + "Right " + CC.GRAY + "click to invert validation");
                }

                SortedItem item = new SortedItem(new ItemBuilder(pun.getType().getIcon()).name(CC.RED + "Punishment #" + pun.getId()).lore(lore).build(), clickType -> {
                    if (!entry.getValue().isPresent()) {
                        if (clickType == ClickType.LEFT) {
                            new InputGUI(CC.RED + "Give a new length (e.g. 20d)", time -> {
                                TimeUnit unit = BanCommand.UNIT_SHORTCUTS.get(time.substring(time.length() - 1));
                                if (unit == null) {
                                    setCurrentPage(currentPage);
                                    return;
                                }
                                try {
                                    int number = Integer.parseInt(time.substring(0, time.length() - 1));
                                    Duration duration = Duration.ofSeconds(unit.toSeconds(number));
                                    new InputGUI(CC.RED + "Give a reason", reason -> {
                                        pun.setUpdateExecutor(getPlayer().getName());
                                        pun.setUpdateReason(reason);
                                        pun.setUpdateLength(duration.getSeconds());
                                        PvPDojo.newChain().async(() -> PunishmentManager.updatePunishment(pun)).sync(this::redrawAndOpen).execute();
                                    }, () -> setCurrentPage(currentPage)).open(getPlayer());
                                } catch (NumberFormatException e) {
                                    setCurrentPage(currentPage);
                                }
                            }, () -> setCurrentPage(currentPage)).open(getPlayer());
                        } else if (clickType == ClickType.RIGHT) {
                            new InputGUI(CC.RED + "Give a reason", reason -> {
                                pun.setUpdateExecutor(getPlayer().getName());
                                pun.setUpdateReason(reason);
                                pun.setValid(!pun.isValid());
                                PvPDojo.newChain().async(() -> PunishmentManager.updatePunishment(pun)).sync(this::redrawAndOpen).execute();
                            }, () -> setCurrentPage(currentPage)).open(getPlayer());
                        }
                    }
                });
                item.setSortValue(pun.getTimestamp());
                addSortItem(item);
            }
            sort();
        }).execute();

    }

    public Rank canEdit(Punishment pun) {
        String uuidExecutor = PunishmentManager.getUUIDExecutor(pun.getExecutor());
        Rank requiredRank = Rank.HELPER;
        if (MOJANG_UUID.matcher(uuidExecutor).matches()) {
            if (UUID.fromString(uuidExecutor).equals(getPlayer().getUniqueId()) && User.getUser(getPlayer()).getRank().inheritsRank(requiredRank)) {
                return null;
            }
            PersistentData data = new PersistentData(UUID.fromString(uuidExecutor));
            data.pullUserDataSneaky();
            if (data.getRank().inheritsRank(requiredRank)) {
                requiredRank = data.getRank();
            }
        } else {
            requiredRank = Rank.MODERATOR_PLUS;
        }
        return User.getUser(getPlayer()).getRank().inheritsRank(requiredRank) ? null : requiredRank;
    }
}
