/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menus;

import static com.pvpdojo.util.bukkit.ItemBuilder.ItemEditor;
import static com.pvpdojo.util.StringUtils.getEnumName;

import java.util.function.Consumer;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.settings.UserSettings;
import com.pvpdojo.settings.UserSettings.PrivacyMode;
import com.pvpdojo.settings.UserSettings.ScoreboardMode;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.LeatherArmorBuilder;

public class SettingsMenu extends SingleInventoryMenu {

    private UserSettings settings;

    public SettingsMenu(Player player) {
        super(player, CC.GRAY + "Settings", 45);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void redraw() {
        InventoryPage page = getCurrentPage();
        page.setHasBar(true);
        User user = User.getUser(getPlayer());
        settings = user.getSettings();

        PrivacyMode pmMode = settings.getPrivateMessages();
        page.setItem(10, new ItemBuilder(Material.INK_SACK)
                .name(pmMode.getChatColor() + "Private Messages: " + getEnumName(pmMode)).durability(pmMode.getDyeColor().getDyeData())
                .lore("", CC.GRAY + pmMode.getDescription()).build(), type -> {
            settings.setPrivateMessages(pmMode.next());
            changeSettings(settings);
        });

        PrivacyMode inviteMode = settings.getInviteMode();
        page.setItem(12, new LeatherArmorBuilder(Material.LEATHER_CHESTPLATE, inviteMode.getDyeColor().getColor())
                .name(inviteMode.getChatColor() + "Invite Mode: " + getEnumName(inviteMode)).lore("", CC.GRAY + inviteMode.getDescription()).build(), type -> {
            settings.setInviteMode(inviteMode.next());
            changeSettings(settings);
        });

        page.setItem(14,
                new ItemBuilder(settings.isParticles() ? Material.EYE_OF_ENDER : Material.ENDER_PEARL)
                        .name((settings.isParticles() ? CC.GREEN : CC.RED) + "Particles: " + settings.isParticles())
                        .lore("", CC.GRAY + "Disable this if you are getting very low fps when abilities are used").build(),
                type -> {
                    settings.setParticles(!settings.isParticles());
                    changeSettings(settings);
                });

        addBooleanSetting(16, "Lightning Animation", Material.FIRE, Material.THIN_GLASS, settings.isLightningAnimation(), settings::setLightningAnimation,
                "A silent lightning will be shown on death if enabled");

        page.setItem(20,
                new ItemBuilder(Material.MAP).name(CC.GREEN + "Map Settings").lore("", CC.GRAY + "Opens another menu where you can disable some maps").build(),
                type -> new MapSettingsMenu(getPlayer()).open());

        addBooleanSetting(22, "PM Sound", Material.GOLD_RECORD, settings.isPrivateMessagesSound(), settings::setPrivateMessagesSound,
                "By enabling this setting a cat sound is played when a player sends you a private message");


        addBooleanSetting(24, "Scoreboard", Material.PAINTING, settings.isScoreboard(), settings::setScoreboard,
                "By enabling this setting every game supporting a scoreboard optionally will be shown");

        addBooleanSetting(29, "Clan Tag", Material.NAME_TAG, settings.isShowClan(), showClan -> {
                    settings.setShowClan(showClan);
                    if (PvPDojo.SERVER_TYPE == ServerType.HUB) {
                        Bukkit.getOnlinePlayers().stream().map(User::getUser).forEach(all -> all.updateUserFor(user));
                    }
                },
                "By enabling this setting clan identifiers are shown in name tags while in Hub");
        addBooleanSetting(31, "Elo Rank Tag", Material.DIAMOND_SWORD, settings.isShowElo(), showElo -> {
                    settings.setShowElo(showElo);
                    if (PvPDojo.SERVER_TYPE == ServerType.HUB) {
                        Bukkit.getOnlinePlayers().stream().map(User::getUser).forEach(all -> all.updateUserFor(user));
                    }
                },
                "By enabling this setting elo rank is shown in name tags while in Hub");

        page.setItem(33, new ItemBuilder(Material.MUSHROOM_SOUP).name(CC.GREEN + "HG Settings").build(), type -> new CustomHGSettingsMenu(getPlayer(), settings).open());


        page.fillBlank();

    }

    public void addBooleanSetting(int pos, String name, Material item, boolean state, Consumer<Boolean> method, String description) {
        addBooleanSetting(pos, name, new ItemStack(item), state, method, description);
    }

    public void addBooleanSetting(int pos, String name, ItemStack item, boolean state, Consumer<Boolean> method, String description) {
        addBooleanSetting(pos, name, item, item, state, method, description);
    }

    public void addBooleanSetting(int pos, String name, Material enabled, Material disabled, boolean state, Consumer<Boolean> method, String description) {
        addBooleanSetting(pos, name, new ItemStack(enabled), new ItemStack(disabled), state, method, description);
    }

    public void addBooleanSetting(int pos, String name, ItemStack enabled, ItemStack disabled, boolean state, Consumer<Boolean> method, String description) {
        page.setItem(pos, new ItemEditor(state ? enabled : disabled).name((state ? CC.GREEN : CC.RED) + name + ": " + state).lore("", CC.GRAY + description).build(), type -> {
            method.accept(!state);
            changeSettings(settings);
        });
    }

    public static class CustomHGSettingsMenu extends SingleInventoryMenu {

        private UserSettings settings;

        public CustomHGSettingsMenu(Player player, UserSettings settings) {
            super(player, "HG Settings", 18);
            this.settings = settings;
        }

        @Override
        public void redraw() {
            super.redraw();

            page.setHasBar(true);

            addBooleanSetting(9, "HG Instant Rejoin", Material.TRIPWIRE_HOOK, settings.getHGSettings().isRejoin(), settings.getHGSettings()::setRejoin,
                    "By enabling this setting you will be automatically put into the next HG round upon your death");
            addBooleanSetting(10, "Spectate", Material.GLASS, settings.getHGSettings().isSpectate(), settings.getHGSettings()::setSpectate,
                    "By enabling this setting you will automatically spectate HG rounds after your death if your rank allows you to do so");

            ScoreboardMode scoreboardMode = settings.getHGSettings().getScoreboardMode();
            page.setItem(11, new ItemBuilder(Material.PAINTING).name(CC.GREEN + "Scoreboard: " + getEnumName(scoreboardMode)).lore("", CC.GRAY + "Scoreboard look").build(), type -> {
                settings.getHGSettings().setScoreboardMode(scoreboardMode.next());
                changeSettings(settings);
            });
            page.fillBlank();
        }

        public void addBooleanSetting(int pos, String name, Material item, boolean state, Consumer<Boolean> method, String description) {
            addBooleanSetting(pos, name, new ItemStack(item), state, method, description);
        }

        public void addBooleanSetting(int pos, String name, ItemStack item, boolean state, Consumer<Boolean> method, String description) {
            addBooleanSetting(pos, name, item, item, state, method, description);
        }


        public void addBooleanSetting(int pos, String name, ItemStack enabled, ItemStack disabled, boolean state, Consumer<Boolean> method, String description) {
            page.setItem(pos, new ItemEditor(state ? enabled : disabled).name((state ? CC.GREEN : CC.RED) + name + ": " + state).lore("", CC.GRAY + description).build(), type -> {
                method.accept(!state);
                changeSettings(settings);
            });
        }

        public void changeSettings(UserSettings settings) {
            PvPDojo.schedule(() -> settings.save(getPlayer().getUniqueId())).createAsyncTask();
            redrawAndOpen();
        }
    }

    public void changeSettings(UserSettings settings) {
        PvPDojo.schedule(() -> settings.save(getPlayer().getUniqueId())).createAsyncTask();
        redrawAndOpen();
    }
}
