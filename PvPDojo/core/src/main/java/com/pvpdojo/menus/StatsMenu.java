/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menus;

import static com.pvpdojo.util.StringUtils.getEnumName;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.EloRank;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class StatsMenu extends SingleInventoryMenu {

    private DojoOfflinePlayer stats;

    public StatsMenu(Player player, DojoOfflinePlayer stats) {
        super(player, CC.GRAY + "Stats: " + CC.GREEN + stats.getName(), 36);
        this.stats = stats;
    }

    @Override
    public void redraw() {
        InventoryPage page = getCurrentPage();
        EloRank eloRank = EloRank.getRank(stats.getPersistentData().getElo());
        page.setHasBar(true);

        page.setItem(19, new ItemBuilder(Material.IRON_SWORD).name(CC.GRAY + "Kills" + PvPDojo.POINTER + CC.GREEN + stats.getPersistentData().getKills()).build(), null);
        page.setItem(21, new ItemBuilder(Material.SKULL_ITEM).name(CC.GRAY + "Deaths" + PvPDojo.POINTER + CC.RED + stats.getPersistentData().getDeaths()).build(), null);
        page.setItem(23, new ItemBuilder(Material.FIREWORK_CHARGE).name(CC.GRAY + "Elo" + PvPDojo.POINTER + CC.WHITE + stats.getPersistentData().getElo() + CC.DARK_GRAY + " ("
                + eloRank.getColor() + getEnumName(eloRank) + CC.DARK_GRAY + ")").build(), null);
        page.setItem(25, new ItemBuilder(Material.STONE_SWORD).name(CC.RED + "View sessions").build(), type -> {
            setLocked(true);
            PvPDojo.newChain()
                   .asyncFirst(() -> DBCommon.getGameSessions(stats.getUniqueId()))
                   .syncLast(input -> new SessionsMenu(getPlayer(), input).open())
                   .execute();
        });

        page.fillBlank();
    }

}
