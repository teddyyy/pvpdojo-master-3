/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menus;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.pvpdojo.achievement.Achievement;
import com.pvpdojo.menu.SortType;
import com.pvpdojo.menu.SortedInventoryMenu;
import com.pvpdojo.menu.SortedItem;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.StringUtils;

public class AchievementMenu extends SortedInventoryMenu {

    public static final int CHEST_COST = 5;

    public AchievementMenu(Player player) {
        super(player, "Achievements", 54, SortType.NONE);

        PersistentData data = User.getUser(player).getPersistentData();
        setDefaultActions(page -> {
            page.setItem(2, new ItemBuilder(Material.DIAMOND)
                    .name(CC.GOLD + "Info")
                    .lore(CC.GRAY + "Achievement Points: " + CC.AQUA + data.getAchievementPoints(),
                            "" + CC.AQUA + data.getAchievements().size() + CC.GRAY + "/" + Achievement.values().length + " Achievements")
                    .build(), null);

            boolean canAfford = data.getAchievementPoints() >= CHEST_COST;
            CC color = canAfford ? CC.GREEN : CC.RED;
            String canAffordMessage = canAfford ? "You can afford this" : "You cannot afford this";
            page.setItem(6, new ItemBuilder(Material.CHEST)
                            .name(color + "Chest")
                            .lore(CC.GRAY + "Chests are used by users to unlock abilities. Click here to purchase.", "",
                                    CC.GRAY + "Cost: " + color + CHEST_COST + " Points", "",
                                    color + "[    " + canAffordMessage + "     ]")
                            .build(),
                    type -> {
                        if (canAfford) {
                            data.incrementAchievementPoints(-CHEST_COST);
                            ChallengeMenu.rewardCollections(User.getUser(player), 1);
                            open();
                        }
                    });
        });

    }

    @Override
    public void redraw() {
        super.redraw();

        PersistentData data = User.getUser(getPlayer()).getPersistentData();
        for (Achievement achievement : Achievement.values()) {
            boolean hasAchievement = data.getAchievements().contains(achievement);
            addSortItem(new SortedItem(new ItemBuilder(hasAchievement ? Material.DIAMOND_BLOCK : Material.COAL_BLOCK)
                    .name((hasAchievement ? CC.GREEN : CC.RED) + StringUtils.getEnumName(achievement, true) + " Achievement")
                    .lore(CC.GRAY + achievement.getDescription(), "", CC.GOLD + "Worth: " + CC.AQUA + achievement.getPoints() + " Points")
                    .build(), null).setSortName(StringUtils.getEnumName(achievement)));
        }
    }
}
