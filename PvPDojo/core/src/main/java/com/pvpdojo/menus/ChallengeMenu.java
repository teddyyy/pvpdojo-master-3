/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menus;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilityinfo.AbilityCollection;
import com.pvpdojo.bukkit.util.GuiItem;
import com.pvpdojo.challenges.ChallengeManager;
import com.pvpdojo.challenges.DojoChallenge;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.Holder;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.StringUtils;

public class ChallengeMenu {

    public static SingleInventoryMenu getChallengeMenu(Player player) {
        return new ChallengeBaseMenu(player);
    }

    private static class ChallengeBaseMenu extends SingleInventoryMenu {

        public ChallengeBaseMenu(Player player) {
            super(player, CC.BLUE + "Select Challenge Tier", 9);
        }

        @Override
        public void redraw() {

            InventoryPage inv = getCurrentPage();
            User user = User.getUser(getPlayer());

            inv.setItem(0,
                    new ItemBuilder(Material.STAINED_GLASS_PANE)
                            .name(CC.GREEN + "Tier 1 Challenges")
                            .lore(CC.GRAY + "Completion reward granted after completing all challenges in this tier", "",
                                    CC.BLUE + "Each challenge:" + CC.AQUA + " 1 Achievement Point",
                                    CC.GOLD + "Completion Reward:" + CC.DARK_AQUA + " 1 random collection")
                            .durability((short) 5)
                            .build(),
                    p -> new ChallengeViewMenu(user, 1).open());
            inv.setItem(4, new ItemBuilder(Material.STAINED_GLASS_PANE)
                            .durability((short) 10).name(CC.DARK_PURPLE + "Tier 2 Challenges")
                            .lore(CC.GRAY + "Completion reward granted after completing all challenges in this tier", "",
                                    CC.BLUE + "Each challenge:" + CC.GRAY + " 2 Achievement Points",
                                    CC.GOLD + "Completion Reward:" + CC.GRAY + " 4 random collections")
                            .build(),
                    p -> new ChallengeViewMenu(user, 2).open());
            inv.setItem(8, new ItemBuilder(Material.STAINED_GLASS_PANE)
                            .name(CC.RED + "Tier 3 Challenges")
                            .lore(CC.GRAY + "Completion reward granted after completing all challenges in this tier", "",
                                    CC.BLUE + "Each challenge:" + CC.GRAY + " 3 Achievement Points",
                                    CC.GOLD + "Completion Reward:" + CC.GRAY + " 10 random collections")
                            .durability((short) 14)
                            .build(),
                    p -> new ChallengeViewMenu(user, 3).open());

        }

    }

    private static class ChallengeViewMenu extends SingleInventoryMenu {

        private int tier;
        private User user;

        public ChallengeViewMenu(User user, int tier) {
            super(user.getPlayer(), CC.BLUE + "Tier " + tier + " Challenges", 27);
            this.tier = tier;
            this.user = user;
            setUpdateInterval(5 * 20);
        }

        @SuppressWarnings("ConstantConditions")
        @Override
        public void redraw() {

            final InventoryPage inv = getCurrentPage();

            inv.setHasBar(true);

            ChallengeManager.get().getAllChallenges(user.getUUID(), map -> {
                final Holder<Boolean> complete_all = new Holder<>(true);
                final ArrayList<ItemStack> icons = new ArrayList<>();
                for (DojoChallenge challenge : DojoChallenge.values()) {
                    if (challenge.getTier() != tier)
                        continue;
                    boolean complete = false;
                    ItemStack icon = challenge.getIcon().clone();
                    List<String> lore = GuiItem.getLore(icon);
                    if (!map.containsKey(challenge)) {
                        lore.add(0, CC.RED + "0" + CC.GRAY + "/" + CC.GREEN + challenge.getValue());
                    } else {
                        int value = map.get(challenge);
                        if (value < -10000) {
                            lore.add(0, CC.GREEN.toString() + challenge.getValue() + CC.GRAY + "/" + CC.GREEN + challenge.getValue());
                            icon.setType(Material.WATCH);
                        } else if (value >= challenge.getValue()) {
                            complete = true;
                            lore.add(0, CC.GREEN.toString() + challenge.getValue() + CC.GRAY + "/" + CC.GREEN + challenge.getValue());
                            icon.setType(Material.DIAMOND);
                        } else
                            lore.add(0, CC.RED.toString() + map.get(challenge) + CC.GRAY + "/" + CC.GREEN + challenge.getValue());
                        lore.add("");
                        lore.add(CC.YELLOW + "Time left" + PvPDojo.POINTER + CC.GRAY
                                + StringUtils.formatMillisDiff(ChallengeManager.get().getTimeLeft(user.getUUID(), challenge) * 1000));
                    }

                    if (!complete) {
                        complete_all.set(false);
                    }
                    new ItemBuilder.ItemEditor(icon).lore(lore).build();
                    icons.add(icon);
                }

                int x = 9;
                for (ItemStack icon : icons) {
                    x++;
                    inv.setItem(x, icon, null);
                }
                if (complete_all.get()) {
                    inv.setItem(4,
                            new ItemBuilder(Material.STAINED_GLASS)
                                    .name(CC.GREEN + "Claim Challenge Bonus")
                                    .lore(CC.GRAY + "You may now claim your challenge bonus").durability((short) 5).build(),
                            p -> claimChallengeBouns(user, tier, this::redrawAndOpen));
                } else {
                    inv.setItem(4, new ItemBuilder(Material.STAINED_GLASS).name(CC.RED + "Cannot Claim Challenge Bonus").lore(CC.GRAY + "You must complete all the challenges",
                            CC.GRAY + "before you can claim the bonus").durability((short) 14).build(),
                            null);
                }
                PvPDojo.schedule(this::open).sync();
            });
        }

    }

    private static void claimChallengeBouns(User user, int tier, Runnable completition) {
        ChallengeManager.get().getAllChallenges(user.getUUID(), map -> {
            boolean completed = true;
            for (DojoChallenge challenge : DojoChallenge.values()) {
                if (challenge.getTier() != tier)
                    continue;
                if (!map.containsKey(challenge)) {
                    completed = false;
                } else {
                    int value = map.get(challenge);
                    if (value < challenge.getValue()) {
                        completed = false;
                    }
                }
            }
            if (completed) {
                DojoChallenge someChallengeOfTier = null;
                for (DojoChallenge challenge : DojoChallenge.values()) {
                    if (challenge.getTier() != tier)
                        continue;
                    someChallengeOfTier = challenge;
                    ChallengeManager.get().nullifyChallenge(user.getUUID(), challenge);
                }

                if (someChallengeOfTier != null) {
                    rewardCollections(user, someChallengeOfTier.getChestReward());
                }
                PvPDojo.schedule(completition).sync();
            }
        });
    }

    public static void rewardCollections(User user, int amount) {
        for (int i = 0; i < amount; i++) {
            AbilityCollection collection = AbilityCollection.getRandomCollection();
            user.getPlayer().sendMessage(CC.GRAY + "You were rewarded a " + GuiItem.getDisplayName(collection.getItem()));
            user.getPersistentData().addCollection(collection.getId());
        }
    }

}
