/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menus;

import java.util.Map;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.google.common.collect.MapMaker;
import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.session.PastGameSession;
import com.pvpdojo.session.PastGameSession.AfterFightSnapshot;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.SkullBuilder;

import co.aikar.taskchain.TaskChainTasks;

public class SessionMenu extends SingleInventoryMenu {

    private Map<UUID, DojoOfflinePlayer> offlinePlayerCache = new MapMaker().weakValues().makeComputingMap(PvPDojo::getOfflinePlayer);
    private int sessionId;

    public SessionMenu(Player player, int sessionId) {
        super(player, "Session " + Integer.toHexString(sessionId), 18);
        this.sessionId = sessionId;
    }

    @Override
    public void redraw() {
        InventoryPage inv = getCurrentPage();
        inv.setHasBar(true);

        PvPDojo.newChain().async(() -> {
            PastGameSession session = DBCommon.getGameSession(sessionId);

            if (session.getInventories().size() < 44) {
                int i = 9;
                inv.setSizeForNeededSlots(session.getInventories().size() + 10);
                for (Map.Entry<UUID, AfterFightSnapshot> entry : session.getInventories().entrySet()) {
                    inv.setItem(i, new SkullBuilder(offlinePlayerCache.get(entry.getKey()).getNick())
                            .name(CC.BLUE + offlinePlayerCache.get(entry.getKey()).getNick() + CC.GRAY + "'s inventory").build(), clickType ->
                            new InventoryViewerMenu(getPlayer(), entry.getValue()).open());
                    i++;
                }
            }
            if (User.getUser(getPlayer()).getRank().inheritsRank(Rank.TRIAL_MODERATOR)) {
                inv.setItem(inv.getSize() - 2, new ItemBuilder(Material.IRON_DOOR).name(CC.GREEN + "Folder" + PvPDojo.POINTER + CC.BLUE + session.getServerFolder()).build(), null);
            }
            inv.setItem(inv.getSize() - 1, new ItemBuilder(Material.RECORD_10).name(CC.GREEN + "Replay").build(),
                    clickType -> {
                        getPlayer().performCommand("replay " + Integer.toHexString(session.getReplayId()));
                        closeSafely();
                    });

        }).sync((TaskChainTasks.GenericTask) this::open).execute();
    }

    public static class InventoryViewerMenu extends SingleInventoryMenu {

        private AfterFightSnapshot snapshot;

        public InventoryViewerMenu(Player player, AfterFightSnapshot snapshot) {
            super(player, "Inventory Viewer", 54);
            this.snapshot = snapshot;
        }

        @Override
        public void redraw() {
            super.redraw();

            InventoryPage inv = getCurrentPage();
            inv.setHasBar(true);

            for (int i = 9; i < 45; i++) {
                inv.setItem(i, snapshot.getInv()[i - 9], null);
            }
            for (int i = 45; i < 49; i++) {
                inv.setItem(i, snapshot.getArmor()[i - 45], null);
            }


            inv.setItem(49, new ItemBuilder(snapshot.getHealth() <= 0 ? Material.SKULL_ITEM : Material.APPLE)
                            .amount((int) snapshot.getHealth() / 2)
                            .name(CC.GREEN + "Health points: " + String.format("%.2f", snapshot.getHealth() / 2))
                            .build(),
                    null);

        }


    }

}
