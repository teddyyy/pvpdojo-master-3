/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menus;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.map.DojoMap;
import com.pvpdojo.map.MapLoader;
import com.pvpdojo.map.MapType;
import com.pvpdojo.menu.SortType;
import com.pvpdojo.menu.SortedInventoryMenu;
import com.pvpdojo.menu.SortedItem;
import com.pvpdojo.settings.UserSettings;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class MapSettingsMenu extends SortedInventoryMenu {

    private List<DojoMap> maps = Stream.concat(MapLoader.getMaps(MapType.DEATHMATCH).stream(), MapLoader.getMaps(MapType.FEAST_FIGHT).stream()).collect(Collectors.toList());

    public MapSettingsMenu(Player player) {
        super(player, "Map Settings", 54, SortType.ALPHABETICAL);
    }

    @Override
    public void redraw() {
        super.redraw();
        User user = User.getUser(getPlayer());
        UserSettings settings = user.getSettings();

        for (DojoMap map : maps) {
            boolean enabled = !settings.getDisabledMaps().contains(map.getName());

            // Use different material depending on DojoMap#getType ??
            Material material = map.getType() == MapType.FEAST_FIGHT ? Material.GRASS : Material.MAP;
            addSortItem(new SortedItem(
                    new ItemBuilder(material)
                            .glow(enabled)
                            .name(CC.BLUE + map.getName() + PvPDojo.POINTER + (enabled ? CC.GREEN + "enabled" : CC.RED + "disabled"))
                            .build(),
                    type -> {
                        if (enabled) {
                            settings.getDisabledMaps().add(map.getName());
                        } else {
                            settings.getDisabledMaps().remove(map.getName());
                        }
                        changeSettings(settings);
                    }).setSortName(map.getName())
            );
        }
    }

    public void changeSettings(UserSettings settings) {
        PvPDojo.schedule(() -> settings.save(getPlayer().getUniqueId())).createAsyncTask();
        redrawAndOpen();
    }

}
