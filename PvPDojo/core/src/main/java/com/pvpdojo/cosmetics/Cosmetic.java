/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.cosmetics;

public abstract class Cosmetic {

    private int currentTick;

    private int renderTick;

    protected Cosmetic(int renderTick) {this.renderTick = renderTick;}

    public void tick() {
        if (renderTick != -1 && ++currentTick % renderTick == 0) {
            render();
        }
    }

    public abstract void render();

}
