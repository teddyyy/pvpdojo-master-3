/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.cosmetics;

public interface MovingCosmetic {

    default void onMove() {
        render();
    }

    void render();

}
