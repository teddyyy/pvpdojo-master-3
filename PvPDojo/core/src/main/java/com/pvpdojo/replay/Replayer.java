/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.replay;

import com.boydti.fawe.util.TaskManager;
import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.Human;
import net.minecraft.util.com.mojang.authlib.GameProfile;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class Replayer extends Thread {

    public static final String PREFIX = CC.DARK_PURPLE + "Replay" + PvPDojo.POINTER;

    private Replay replay;
    private Player viewer;
    private Map<Integer, Human> humanId = new ConcurrentHashMap<>();
    private boolean paused, restart, running;
    private double timeFactor = 1;
    private long forwardTime, lastActionTime, lastProcessTime; // lastBarUpdate

    public Replayer(Replay replay, Player viewer) {
        this.replay = replay;
        this.viewer = viewer;
    }

    public Replay getReplay() {
        return replay;
    }

    public Player getViewer() {
        return viewer;
    }

    public boolean isRunning() {
        return running;
    }

    public Collection<Human> getHumans() {
        return humanId.values();
    }

    public boolean isPaused() {
        return paused;
    }

    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    public void setForwardTime(int l) {
        if (l > replay.getDuration()) {
            viewer.sendMessage("Given time is greater than the replay length");
        }
        this.forwardTime = replay.getDate() + l;
        if (forwardTime < lastActionTime) {
            restart();
        }
    }

    public double changeTimeFactor() {
        if ((timeFactor += 0.25) > 2) {
            timeFactor = 0.25;
        }
        return timeFactor;
    }

    @Override
    public synchronized void start() {
        running = true;
        super.start();
    }

    public void endReplay() {
        running = false;
        interrupt();
        PvPDojo.schedule(() -> {
            ServerType.HUB.connect(viewer);
            replay.getRecordedMap().remove(true);
            // UserImpl pd = User.getUser(viewer);
            // pd.setReplayViewer(null);
            // pd.removeDragon();
            // pd.getFakeBlocks().clear();
            // pd.spawn();
        }).createTask(20);
    }

    public void restart() {
        restart = true;
    }

    public void reset() {
        for (Human human : humanId.values()) {
            human.remove();
        }
        humanId.clear();
        lastActionTime = 0;
        paused = false;
        replay.close();
    }

    @Override
    public void run() {
        // UserImpl pd = User.getUser(viewer);
        while (running && viewer.isOnline()) {
            if (restart) {
                try {
                    reset();
                    replay.initReplay();
                } catch (IOException e) {
                    viewer.sendMessage(PREFIX + "§cCrashed while restarting...");
                    e.printStackTrace();
                    break;
                }
                restart = false;
            }

            if (paused) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                continue;
            }

            ReplayAction action = null;
            try {
                action = ReplayAction.get(replay.getInput().readInt());
                long occurring = replay.getInput().readLong();
                // if (pd.getFakeDragon() == null) {
                // PvPDojo.schedule(() -> pd.setBossbar(
                // "§c" + StringUtils.millisToMinutesAndSeconds(occurring - replay.getDate()) + " / " + StringUtils.millisToMinutesAndSeconds(replay.getDuration()),
                // (float) (occurring - replay.getDate()) / (float) replay.getDuration())).nextTick();
                // } else if (System.currentTimeMillis() - lastBarUpdate > 250) {
                // pd.setBossbar("§c" + StringUtils.millisToMinutesAndSeconds(occurring - replay.getDate()) + " / " + StringUtils.millisToMinutesAndSeconds(replay.getDuration()),
                // (float) (occurring - replay.getDate()) / (float) replay.getDuration());
                // lastBarUpdate = System.currentTimeMillis();
                // }
                if (lastActionTime != 0 && forwardTime < occurring) {
                    long wait = occurring - lastActionTime - lastProcessTime;
                    if (wait > 7_000) {
                        viewer.sendMessage(Replayer.PREFIX + "Skipped " + (int) (wait / 1000D) + " seconds as that time is action free");
                        wait = 0;
                    }
                    if (wait > 0) {
                        Thread.sleep((long) (wait / timeFactor));
                    }
                }
                lastActionTime = occurring;
                long start = System.currentTimeMillis();
                switch (action) {
                    case BLOCK_BREAK:
                        Location breakLocation = new Location(viewer.getWorld(), replay.getInput().readInt(), replay.getInput().readInt(),
                                replay.getInput().readInt());
                        replay.shiftLocationToMap(breakLocation);
                        if (viewer.getLocation().distanceSquared(breakLocation) < 400) {
                            // Material mat = pd.getFakeBlocks().containsKey(breakLocation) ? pd.getFakeBlocks().get(breakLocation).getFirst() : breakLocation.getBlock().getType();
                            // if (mat.isBlock()) {
                            // viewer.playEffect(breakLocation, Effect.STEP_SOUND, mat);
                            // }
                        }

                        // pd.addFakeBlock(breakLocation, Material.AIR);
                        break;
                    case BLOCK_PLACE:
                        Location placeLocation = new Location(viewer.getWorld(), replay.getInput().readInt(), replay.getInput().readInt(),
                                replay.getInput().readInt());
                        Material mat = Material.values()[replay.getInput().readInt()];
                        if (viewer.getLocation().distanceSquared(placeLocation) < 400) {
                            switch (mat) {
                                case WOOL:
                                case CARPET:
                                    viewer.playSound(placeLocation, Sound.STEP_WOOL, 1, 1);
                                    break;
                                case SNOW:
                                case SNOW_BLOCK:
                                    viewer.playSound(placeLocation, Sound.STEP_SNOW, 1, 1);
                                    break;
                                case GRAVEL:
                                    viewer.playSound(placeLocation, Sound.STEP_GRAVEL, 1, 1);
                                    break;
                                case SAND:
                                case SOUL_SAND:
                                    viewer.playSound(placeLocation, Sound.STEP_SAND, 1, 1);
                                    break;
                                case GRASS:
                                case LONG_GRASS:
                                    viewer.playSound(placeLocation, Sound.STEP_GRASS, 1, 1);
                                    break;
                                case WOOD:
                                case WOOD_BUTTON:
                                case WOOD_DOUBLE_STEP:
                                case WOOD_PLATE:
                                case WOOD_STAIRS:
                                case WOOD_STEP:
                                case WOODEN_DOOR:
                                case SPRUCE_WOOD_STAIRS:
                                case JUNGLE_WOOD_STAIRS:
                                case BIRCH_WOOD_STAIRS:
                                case LOG:
                                case LOG_2:
                                case CHEST:
                                case TRAPPED_CHEST:
                                case LADDER:
                                    viewer.playSound(placeLocation, Sound.STEP_WOOD, 1, 1);
                                    break;
                                default:
                                    if (mat.isBlock()) {
                                        viewer.playSound(placeLocation, Sound.STEP_STONE, 1, 1);
                                    }
                                    break;
                            }
                        }
                        // byte data = replay.getInput().readByte();
                        // pd.addFakeBlock(placeLocation, mat, data);
                        break;
                    case CHAT:
                        viewer.sendMessage(PREFIX + replay.getInput().readUTF());
                        break;
                    case CRITICAL_HIT:
                        Human critHuman = humanId.get(replay.getInput().readInt());
                        if (critHuman != null) {
                            critHuman.crit();
                        } else {
                            viewer.sendMessage(PREFIX + "§cInvalid ReplayAction." + action.name());
                        }
                        break;
                    case DAMAGE:
                        Human damageHuman = humanId.get(replay.getInput().readInt());
                        if (damageHuman != null) {
                            damageHuman.damage(true);
                        } else {
                            viewer.sendMessage(PREFIX + "§cInvalid ReplayAction." + action.name());
                        }
                        break;
                    case DEATH_PLAYER:
                        Human deathHuman = humanId.get(replay.getInput().readInt());
                        if (deathHuman != null) {
                            deathHuman.death();
                        } else {
                            viewer.sendMessage(PREFIX + "§cInvalid ReplayAction." + action.name());
                        }
                        break;
                    case DESPAWN:
                        Human despawnHuman = humanId.remove(replay.getInput().readInt());
                        if (despawnHuman != null) {
                            if (!despawnHuman.isDead()) {
                                despawnHuman.remove();
                            }
                        } else {
                            viewer.sendMessage(PREFIX + "§cInvalid ReplayAction." + action.name());
                        }
                        break;
                    case END_OF_REPLAY:
                        viewer.sendMessage(PREFIX + "End of replay");
                        running = false;
                        break;
                    case HELD_ITEM:
                        Human itemHuman = humanId.get(replay.getInput().readInt());
                        ItemStack hand = new ItemStack(Material.values()[replay.getInput().readInt()]);
                        if (replay.getInput().readBoolean()) {
                            hand.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 1);
                        }
                        if (itemHuman != null) {
                            itemHuman.setItemInHand(hand);
                        } else {
                            viewer.sendMessage(PREFIX + "§cInvalid ReplayAction." + action.name());
                        }
                        break;
                    case METADATA:
                        throw new IOException("Unexpected metadata action");
                    case MOVE:
                        Human moveHuman = humanId.get(replay.getInput().readInt());
                        Location to = new Location(viewer.getWorld(), replay.getInput().readDouble(), replay.getInput().readDouble(), replay.getInput().readDouble());
                        float yaw = replay.getInput().readFloat();
                        float pitch = replay.getInput().readFloat();
                        to.setYaw(yaw);
                        to.setPitch(pitch);
                        replay.shiftLocationToMap(to);
                        boolean blocking = replay.getInput().readBoolean();
                        if (moveHuman != null) {
                            moveHuman.move(yaw, pitch, false, to);
                            moveHuman.setBlocking(blocking);
                        } else {
                            viewer.sendMessage(PREFIX + "§cInvalid ReplayAction." + action.name());
                        }
                        break;
                    case SPAWN_PLAYER:
                        int id = replay.getInput().readInt();
                        UUID uuid = UUID.fromString(replay.getInput().readUTF());
                        Location loc = new Location(viewer.getWorld(), replay.getInput().readDouble(), replay.getInput().readDouble(), replay.getInput().readDouble());
                        replay.shiftLocationToMap(loc);
                        GameProfile profile = DBCommon.getProfile(uuid);
                        Human spawnHuman = TaskManager.IMP.sync(() -> new Human(loc, viewer, profile));
                        humanId.put(id, spawnHuman);
                        spawnHuman.spawn();
                        break;
                    case SPRINT:
                        Human sprintHuman = parseHuman();
                        boolean sprint = replay.getInput().readBoolean();
                        if (sprintHuman != null) {
                            sprintHuman.setSprinting(sprint);
                        } else {
                            viewer.sendMessage(PREFIX + "§cInvalid ReplayAction." + action.name());
                        }
                        break;
                    case SWING:
                        parseHuman().swingArm();
                        break;
                    case SNEAK:
                        Human sneakHuman = humanId.get(replay.getInput().readInt());
                        boolean sneaking = replay.getInput().readBoolean();
                        if (sneakHuman != null) {
                            sneakHuman.setCrouched(sneaking);
                        } else {
                            viewer.sendMessage(PREFIX + "§cInvalid ReplayAction." + action.name());
                        }
                        break;
                    case ARMOR:
                        Human armorHuman = humanId.get(replay.getInput().readInt());
                        ItemStack boots = new ItemStack(Material.values()[replay.getInput().readInt()]);
                        if (replay.getInput().readBoolean()) {
                            boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
                        }
                        ItemStack leggins = new ItemStack(Material.values()[replay.getInput().readInt()]);
                        if (replay.getInput().readBoolean()) {
                            leggins.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
                        }
                        ItemStack chestplate = new ItemStack(Material.values()[replay.getInput().readInt()]);
                        if (replay.getInput().readBoolean()) {
                            chestplate.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
                        }
                        ItemStack helmet = new ItemStack(Material.values()[replay.getInput().readInt()]);
                        if (replay.getInput().readBoolean()) {
                            helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
                        }
                        if (armorHuman != null) {
                            armorHuman.updateArmor(boots, leggins, chestplate, helmet);
                        } else {
                            viewer.sendMessage(PREFIX + "§cInvalid ReplayAction" + action.name());
                        }
                        break;
                    case MAGIC_CRIT:
                        Human magicHuman = humanId.get(replay.getInput().readInt());
                        if (magicHuman != null) {
                            magicHuman.magicCrit();
                        } else {
                            viewer.sendMessage(PREFIX + "§cInvalid ReplayAction" + action.name());
                        }
                        break;
                    default:
                        throw new IOException("Unexpected action");

                }
                long end = System.currentTimeMillis();
                lastProcessTime = end - start;
            } catch (IOException e) {
                viewer.sendMessage(PREFIX + "§cCrashed while processing ReplayAction." + action);
                e.printStackTrace();
                break;
            } catch (NullPointerException e) {
                viewer.sendMessage(PREFIX + "Could not read action: " + action);
            } catch (InterruptedException ignore) {}
        }
        endReplay();
    }

    private Human parseHuman() throws IOException {
        return humanId.get(replay.getInput().readInt());
    }

}
