/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.replay.commands;

import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.replay.Recorder;
import com.pvpdojo.replay.Replay;
import com.pvpdojo.replay.Replayer;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.CompBuilder;
import com.pvpdojo.util.Log;

import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Flags;
import co.aikar.commands.annotation.HelpCommand;
import co.aikar.commands.annotation.Single;
import co.aikar.commands.annotation.Subcommand;

@CommandAlias("record|rec")
public class RecordCommand extends DojoCommand {

    @HelpCommand
    public void onHelp(CommandHelp help) {
        help.showHelp();
    }

    @Subcommand("start")
    public void onRecord(User user, @Flags("other") Player[] targets) {

        if (PvPDojo.get().getStandardMap() == null) {
            user.sendMessage(PvPDojo.WARNING + "Manual recordings are not supported on this server");
            return;
        }

        Replay replay = new Replay(PvPDojo.get().getStandardMap());

        replay.setAutoCommit(true);
        replay.setInitialLoc(targets[0].getLocation());
        for (Player player : targets) {
            replay.recordPlayer(player);
        }
        replay.setStaff(true);

        Log.info("Manual recording");

        Recorder.inst().startRecording(replay, replayId -> user.sendMessage(new CompBuilder(Replayer.PREFIX + "Recording started *click* to copy")
                .hoverMessage(Integer.toHexString(replayId))
                .command("replay " + Integer.toHexString(replayId))
                .create()));
    }

    @Subcommand("stop")
    public void onRecord(User user, @Single String hexId) {
        int id;
        try {
            id = Integer.parseInt(hexId, 16);
        } catch (NumberFormatException ex) {
            user.sendMessage(PvPDojo.WARNING + "This is not a replay id");
            return;
        }
        user.sendMessage(Replayer.PREFIX + "Trying to save replay...");
        Recorder.inst().stopRecording(id, found -> user.sendMessage(Replayer.PREFIX + (found ? CC.GREEN + "Success" : CC.RED + "Not found")));

    }

    @Override
    public Rank getRank() {
        return Rank.TRIAL_MODERATOR;
    }

}
