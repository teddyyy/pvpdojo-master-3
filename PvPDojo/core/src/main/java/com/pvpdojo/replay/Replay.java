/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.replay;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.IntConsumer;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Collections2;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.map.DojoMap;
import com.pvpdojo.map.MapLoader;
import com.pvpdojo.map.MapType;
import com.pvpdojo.mysql.SQLBuilder;
import com.pvpdojo.util.Log;
import com.sk89q.worldedit.bukkit.BukkitUtil;

import net.minecraft.util.org.apache.commons.io.IOUtils;

public class Replay {

    private DataOutputStream output;
    private DataInputStream input;
    private File dest, temp;

    private DojoMap recordedMap;
    private long date;
    private int duration;
    private int id = -1;
    private int x, y, z;
    private boolean staff;

    private transient Set<UUID> recordedPlayers;
    private transient Set<Location> liquidTracker = new HashSet<>();
    private transient boolean autoCommit;

    /**
     * This constructor is used if we want to represent a finished {@link Replay} that can be replayed using {@link Replayer}
     *
     * @param id
     */
    public Replay(int id) {
        this.id = id;
        this.dest = new File(Recorder.DEST, Integer.toHexString(id) + ".replay");
    }

    public Replay(DojoMap map) {
        this.recordedMap = map;
    }

    public boolean isRecording(Player player) {
        return getRecordedPlayers().contains(player);
    }

    public void recordPlayer(Player player) {
        if (recordedPlayers == null) {
            recordedPlayers = new HashSet<>();
        }
        long time = System.currentTimeMillis();
        Location location = player.getLocation();
        ItemStack item = player.getItemInHand();
        ItemStack[] armor = player.getInventory().getArmorContents();
        Recorder.inst().getTasks().add(() -> join(player, location, item, armor, time));

        recordedPlayers.add(player.getUniqueId());
    }

    public void removePlayer(Player player) {
        if (recordedPlayers.remove(player.getUniqueId())) {
            long time = System.currentTimeMillis();
            Recorder.inst().getTasks().add(() -> despawn(player, time));
        }
    }

    public Collection<Player> getRecordedPlayers() {
        if (recordedPlayers == null) {
            return recordedMap.getPlayers();
        }
        return Collections2.transform(recordedPlayers, Bukkit::getPlayer);
    }

    public int getId() {
        return id;
    }

    public long getDate() {
        return date;
    }

    public int getDuration() {
        return duration;
    }

    public DojoMap getRecordedMap() {
        return recordedMap;
    }

    public void setStaff(boolean staff) {
        this.staff = staff;
    }

    public boolean isStaff() {
        return staff;
    }

    public Set<Location> getLiquidTracker() {
        return liquidTracker;
    }

    public DataOutputStream getOutput() {
        return output;
    }

    public DataInputStream getInput() {
        return input;
    }

    /**
     * A {@link Replay} that is instructed to automatically commit will stop
     * and save the recording after the last player left.
     *
     * @return {@code true} if the replay is auto commiting
     */
    public boolean isAutoCommit() {
        return autoCommit;
    }

    /**
     * A {@link Replay} that is instructed to automatically commit will stop
     * and save the recording after the last player left.
     *
     * @param autoCommit Set this to {@code true} if the replay should auto commit
     */
    public void setAutoCommit(boolean autoCommit) {
        this.autoCommit = autoCommit;
    }

    public void setInitialLoc(Location location) {
        unshiftLocationFromMap(location);
        this.x = location.getBlockX();
        this.y = location.getBlockY();
        this.z = location.getBlockZ();
    }

    public Location getInitialLocation() {
        return shiftLocationToMap(new Location(recordedMap.getWorld(), x, y, z));
    }

    public void close() {
        if (output != null) {
            try {
                output.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (input != null) {
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void initRec(IntConsumer callback) throws IOException {
        UUID tempId = UUID.randomUUID();
        this.temp = new File(Recorder.DEST, tempId + ".tmp");
        temp.createNewFile();
        this.output = new DataOutputStream(new GZIPOutputStream(new FileOutputStream(temp), 8192));
        this.date = System.currentTimeMillis();
        initSql(callback);
    }

    private void initSql(IntConsumer callback) {
        PvPDojo.schedule(() -> {
            try (SQLBuilder sql = new SQLBuilder()) {
                sql.setStatement("INSERT INTO replay  (date, staff) VALUES(FROM_UNIXTIME(" + date / 1000 + "), " + staff + ")")
                   .createStatement(Statement.RETURN_GENERATED_KEYS)
                   .executeUpdate();
                ResultSet keyrs = sql.getPreparedStatement().getGeneratedKeys();
                if (keyrs.next()) {
                    this.id = keyrs.getInt(1);
                    this.dest = new File(Recorder.DEST, Integer.toHexString(id) + ".replay");
                    if (callback != null) {
                        callback.accept(id);
                    }
                } else {
                    throw new IllegalStateException("No Replay key generated");
                }
            } catch (SQLException e) {
                Log.exception("Write Replay to DB", e);
            }

        }).createAsyncTask();
    }

    public void initReplay() throws IOException {
        this.input = new DataInputStream(new GZIPInputStream(new FileInputStream(dest), 8192));
        if (ReplayAction.get(input.readInt()) == ReplayAction.METADATA) {
            try {
                this.date = input.readLong();
                this.duration = input.readInt();
                this.recordedMap = MapLoader.getMap(MapType.valueOf(input.readUTF()), input.readUTF());
                this.x = input.readInt();
                this.y = input.readInt();
                this.z = input.readInt();
                this.staff = input.readBoolean();
            } catch (IOException e) {
                throw new IOException("This replay is either corrupted or incompatible with the running software", e);
            }
        } else {
            throw new IOException("This replay is missing metadata");
        }
    }

    public void save(Consumer<Boolean> callback) throws IOException {
        this.duration = (int) (System.currentTimeMillis() - date);
        close();
        dest.createNewFile();
        this.output = new DataOutputStream(new GZIPOutputStream(new FileOutputStream(dest), 8192));
        this.input = new DataInputStream(new GZIPInputStream(new FileInputStream(temp), 8192));

        output.writeInt(ReplayAction.METADATA.ordinal());
        output.writeLong(date);
        output.writeInt(duration);
        output.writeUTF(recordedMap.getType().name());
        output.writeUTF(recordedMap.getName());
        output.writeInt(x);
        output.writeInt(y);
        output.writeInt(z);
        output.writeBoolean(staff);

        IOUtils.copy(input, output);
        output.writeInt(ReplayAction.END_OF_REPLAY.ordinal());
        output.writeLong(System.currentTimeMillis());

        close();
        temp.delete();
        if (callback != null) {
            callback.accept(true);
        }
    }

    private void writeTime(long time) throws IOException {
        output.writeLong(time);
    }

    private Location unshiftLocationFromMap(Location location) {
        return location.subtract(BukkitUtil.toLocation(recordedMap.getWorld(), recordedMap.getRegion().getMinimumPoint()));
    }

    public Location shiftLocationToMap(Location location) {
        return location.add(BukkitUtil.toLocation(recordedMap.getWorld(), recordedMap.getRegion().getMinimumPoint()));
    }

    public void chat(String message, long time) {
        try {
            output.writeInt(ReplayAction.CHAT.ordinal());
            writeTime(time);
            output.writeUTF(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void death(Player entity, long time) {
        try {
            output.writeInt(ReplayAction.DEATH_PLAYER.ordinal());
            writeTime(time);
            output.writeInt(entity.getEntityId());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void move(Player player, Location to, boolean blocking, long time) {
        try {
            unshiftLocationFromMap(to);
            output.writeInt(ReplayAction.MOVE.ordinal());
            writeTime(time);
            output.writeInt(player.getEntityId());
            output.writeDouble(to.getX());
            output.writeDouble(to.getY());
            output.writeDouble(to.getZ());
            output.writeFloat(to.getYaw());
            output.writeFloat(to.getPitch());
            output.writeBoolean(blocking);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void breakBlock(Location location, long time) {
        try {
            unshiftLocationFromMap(location);
            output.writeInt(ReplayAction.BLOCK_BREAK.ordinal());
            writeTime(time);
            output.writeInt(location.getBlockX());
            output.writeInt(location.getBlockY());
            output.writeInt(location.getBlockZ());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void blockPlace(Location location, Material mat, byte data, long time) {
        try {
            unshiftLocationFromMap(location);
            output.writeInt(ReplayAction.BLOCK_PLACE.ordinal());
            writeTime(time);
            output.writeInt(location.getBlockX());
            output.writeInt(location.getBlockY());
            output.writeInt(location.getBlockZ());
            output.writeInt(mat.ordinal());
            output.writeByte(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void join(Player player, Location location, ItemStack itemInHand, ItemStack[] armor, long time) {
        try {
            unshiftLocationFromMap(location);
            output.writeInt(ReplayAction.SPAWN_PLAYER.ordinal());
            writeTime(time);
            output.writeInt(player.getEntityId());
            output.writeUTF(player.getUniqueId().toString());
            output.writeDouble(location.getX());
            output.writeDouble(location.getY());
            output.writeDouble(location.getZ());
            heldItem(player, itemInHand, time);
            armor(player, armor, time);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void despawn(Entity ent, long time) {
        try {
            output.writeInt(ReplayAction.DESPAWN.ordinal());
            writeTime(time);
            output.writeInt(ent.getEntityId());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (getRecordedPlayers().stream().noneMatch(Player::isOnline) && isAutoCommit()) {
            Recorder.inst().stopRecording(this);
        }
    }

    public void damage(Entity ent, long time) {
        try {
            output.writeInt(ReplayAction.DAMAGE.ordinal());
            writeTime(time);
            output.writeInt(ent.getEntityId());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void swing(Player player, long time) {
        try {
            output.writeInt(ReplayAction.SWING.ordinal());
            writeTime(time);
            output.writeInt(player.getEntityId());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sprint(Player player, boolean sprinting, long time) {
        try {
            output.writeInt(ReplayAction.SPRINT.ordinal());
            writeTime(time);
            output.writeInt(player.getEntityId());
            output.writeBoolean(sprinting);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void heldItem(Player player, ItemStack item, long time) {
        try {
            output.writeInt(ReplayAction.HELD_ITEM.ordinal());
            writeTime(time);
            output.writeInt(player.getEntityId());
            output.writeInt(item != null ? item.getType().ordinal() : 0);
            output.writeBoolean(item != null && item.getEnchantments().size() > 0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void critical(Player player, long time) {
        try {
            output.writeInt(ReplayAction.CRITICAL_HIT.ordinal());
            writeTime(time);
            output.writeInt(player.getEntityId());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sneak(Player player, boolean sneaking, long time) {
        try {
            output.writeInt(ReplayAction.SNEAK.ordinal());
            writeTime(time);
            output.writeInt(player.getEntityId());
            output.writeBoolean(sneaking);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void armor(Player player, ItemStack[] armor, long time) {
        try {
            output.writeInt(ReplayAction.ARMOR.ordinal());
            writeTime(time);
            output.writeInt(player.getEntityId());
            for (ItemStack anArmor : armor) {
                Material type;
                if (anArmor != null) {
                    type = anArmor.getType();
                } else {
                    type = Material.AIR;
                }
                output.writeInt(type.ordinal());
                output.writeBoolean(!anArmor.getEnchantments().isEmpty());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void magicdamage(Player player, long time) {
        try {
            output.writeInt(ReplayAction.MAGIC_CRIT.ordinal());
            writeTime(time);
            output.writeInt(player.getEntityId());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
