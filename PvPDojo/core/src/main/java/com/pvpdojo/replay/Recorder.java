/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.replay;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.IntConsumer;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.RegionEnterEvent;
import com.pvpdojo.bukkit.events.RegionLeaveEvent;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.util.Log;

import net.minecraft.server.v1_7_R4.EntityHuman;
import net.minecraft.server.v1_7_R4.EnumAnimation;

public class Recorder implements Listener {

    public static final File DEST = new File("../replays");
    public static final ReplayWriter writer = new ReplayWriter();
    public static final Runnable POISON_PILL = () -> {};

    private static Recorder instance;
    private BlockingQueue<Runnable> tasks = new LinkedBlockingQueue<>();
    private List<Replay> currentRecordings = new ArrayList<>();

    public static Recorder inst() {
        return instance == null ? instance = new Recorder() : instance;
    }

    public BlockingQueue<Runnable> getTasks() {
        return tasks;
    }

    public void load() {
        if (!DEST.exists()) {
            DEST.mkdir();
        }
        writer.start();
        Bukkit.getPluginManager().registerEvents(this, PvPDojo.get());
        // Trigger enum loads
        ReplayAction.values();
    }

    public void stop() {
        // Concurrency: we may only access currentRecordings from our writer thread
        HandlerList.unregisterAll(this);
        new ArrayList<>(currentRecordings).forEach(this::stopRecording);
        writer.shutdown();
    }

    public void startRecording(Replay replay, IntConsumer callback) {
        try {
            replay.initRec(callback);
        } catch (IOException e) {
            Log.exception("Starting recording", e);
        }

        tasks.add(() -> currentRecordings.add(replay));
        if (replay.getRecordedPlayers() != null) {
            for (Player player : replay.getRecordedPlayers()) {
                long time = System.currentTimeMillis();
                Location location = player.getLocation();
                ItemStack item = player.getItemInHand();
                ItemStack[] armor = player.getInventory().getArmorContents();
                tasks.add(() -> replay.join(player, location, item, armor, time));
            }
        }
    }

    public void stopRecording(Replay replay) {
        tasks.add(() -> {
            try {
                if (currentRecordings.remove(replay)) {
                    replay.save(null);
                }
            } catch (IOException e) {
                Log.exception("Saving Replay", e);
            }
        });
    }

    public void stopRecording(int id, Consumer<Boolean> callback) {
        tasks.add(() -> {
            Replay toSave = currentRecordings.stream().filter(replay -> replay.getId() == id).findFirst().orElse(null);
            if (toSave != null) {
                currentRecordings.remove(toSave);
                try {
                    toSave.save(callback);
                } catch (IOException e) {
                    Log.exception("Saving Replay", e);
                }
            } else {
                callback.accept(false);
            }
        });
    }

    public void applyAction(Player player, Consumer<Replay> action) {
        applyAction(player, action, Replay::isRecording);
    }

    public void applyAction(Player player, Consumer<Replay> action, BiPredicate<Replay, Player> predicate) {
        tasks.add(() -> {
            for (Replay replay : currentRecordings) {
                if (predicate == null || predicate.test(replay, player)) {
                    action.accept(replay);
                }
            }
        });
    }

    @Deprecated
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onChat(AsyncPlayerChatEvent e) {
        long time = System.currentTimeMillis();
        PvPDojo.schedule(() -> applyAction(e.getPlayer(), replay -> replay.chat(String.format(e.getFormat(), e.getPlayer().getName(), e.getMessage()), time))).sync();
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onRespawn(PlayerRespawnEvent e) {
        long time = System.currentTimeMillis();
        applyAction(e.getPlayer(), replay -> replay.despawn(e.getPlayer(), time));
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDeath(PlayerDeathEvent e) {
        long time = System.currentTimeMillis();
        applyAction(e.getEntity(), replay -> replay.death(e.getEntity(), time));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onMove(PlayerMoveEvent e) {
        long time = System.currentTimeMillis();
        EntityHuman eh = NMSUtils.get(e.getPlayer());
        boolean blocking = (eh.by()) && (eh.f.getItem().d(eh.f) != EnumAnimation.NONE);
        applyAction(e.getPlayer(), replay -> replay.move(e.getPlayer(), e.getTo().clone(), blocking, time));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onTeleport(PlayerTeleportEvent e) {
        long time = System.currentTimeMillis();
        @SuppressWarnings("deprecation")
        boolean onGround = e.getPlayer().isOnGround();
        applyAction(e.getPlayer(), replay -> replay.move(e.getPlayer(), e.getTo().clone(), onGround, time));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent e) {
        long time = System.currentTimeMillis();
        applyAction(e.getPlayer(), replay -> replay.breakBlock(e.getBlock().getLocation(), time));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBucketFill(PlayerBucketFillEvent e) {
        long time = System.currentTimeMillis();
        applyAction(e.getPlayer(), replay -> {
            replay.breakBlock(e.getBlockClicked().getRelative(e.getBlockFace()).getLocation(), time);
            replay.getLiquidTracker().add(e.getBlockClicked().getRelative(e.getBlockFace()).getLocation());
        });
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent e) {
        long time = System.currentTimeMillis();
        Material mat = e.getBlock().getType();
        @SuppressWarnings("deprecation")
        byte data = e.getBlock().getData();
        applyAction(e.getPlayer(), replay -> replay.blockPlace(e.getBlock().getLocation(), mat, data, time));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBucketEmpty(PlayerBucketEmptyEvent e) {
        long time = System.currentTimeMillis();
        applyAction(e.getPlayer(), replay -> {
            replay.blockPlace(e.getBlockClicked().getRelative(e.getBlockFace()).getLocation(), Material.WATER, (byte) 0, time);
            replay.getLiquidTracker().add(e.getBlockClicked().getRelative(e.getBlockFace()).getLocation());
        });
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onRegionEnter(RegionEnterEvent e) {
        long time = System.currentTimeMillis();
        Location location = e.getPlayer().getLocation();
        ItemStack item = e.getPlayer().getItemInHand();
        ItemStack[] armor = e.getPlayer().getInventory().getArmorContents();
        applyAction(e.getPlayer(), replay -> replay.join(e.getPlayer(), location, item, armor, time), (replay, p) -> replay.getRecordedMap().getRegion().equals(e.getRegion()));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onRegionLeave(RegionLeaveEvent e) {
        long time = System.currentTimeMillis();
        applyAction(e.getPlayer(), replay -> replay.despawn(e.getPlayer(), time), (replay, p) -> replay.getRecordedMap().getRegion().equals(e.getRegion()));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {
            Player player = (Player) e.getEntity();
            if (((LivingEntity) e.getEntity()).getNoDamageTicks() <= ((LivingEntity) e.getEntity()).getMaximumNoDamageTicks() / 2) {
                long time = System.currentTimeMillis();
                applyAction(player, replay -> replay.damage(player, time));
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onSwing(PlayerAnimationEvent e) {
        long time = System.currentTimeMillis();
        applyAction(e.getPlayer(), replay -> replay.swing(e.getPlayer(), time));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onSprint(PlayerToggleSprintEvent e) {
        long time = System.currentTimeMillis();
        applyAction(e.getPlayer(), replay -> replay.sprint(e.getPlayer(), e.isSprinting(), time));
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onInventoryClick(InventoryClickEvent e) {
        if (!e.isCancelled() && e.getClickedInventory() != null && e.getClickedInventory().getType() == InventoryType.CRAFTING) {
            boolean updateHeldItem = false, updateArmor = false;
            if (e.getClick() == ClickType.SHIFT_LEFT || e.getClick() == ClickType.SHIFT_RIGHT) {
                ArmorType armor = ArmorType.matchType(e.getCurrentItem());
                if (armor != null) {
                    boolean equipping = true;
                    if (e.getRawSlot() == armor.getSlot()) {
                        equipping = false;
                    }
                    if (armor.equals(ArmorType.HELMET) && (equipping == (e.getWhoClicked().getInventory().getHelmet() == null))
                            || armor.equals(ArmorType.CHESTPLATE)
                            && (equipping == (e.getWhoClicked().getInventory().getChestplate() == null))
                            || armor.equals(ArmorType.LEGGINGS)
                            && (equipping == (e.getWhoClicked().getInventory().getLeggings() == null))
                            || armor.equals(ArmorType.BOOTS)
                            && (equipping == (e.getWhoClicked().getInventory().getBoots() == null))) {
                        updateArmor = true;
                    }
                }
            } else {
                ItemStack hotbar = e.getClick() == ClickType.NUMBER_KEY ? e.getClickedInventory().getItem(e.getHotbarButton()) : null;
                ArmorType armor;
                if (hotbar != null) {
                    armor = ArmorType.matchType(hotbar);
                } else {
                    armor = ArmorType.matchType(e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR ? e.getCurrentItem() : e.getCursor());
                }
                if (armor != null && e.getRawSlot() == armor.getSlot()) {
                    updateArmor = true;
                }

            }

            if (e.getRawSlot() - e.getClickedInventory().getSize() == e.getWhoClicked().getInventory().getHeldItemSlot() && e.getCurrentItem().getType() != Material.AIR
                    || (e.getClick() == ClickType.NUMBER_KEY && e.getHotbarButton() == e.getWhoClicked().getInventory().getHeldItemSlot()
                    && (e.getCurrentItem().getType() != Material.AIR || e.getClickedInventory().getItem(e.getHotbarButton()) != null))) {
                updateHeldItem = true;
            }
            if (updateHeldItem || updateArmor) {
                Player player = (Player) e.getWhoClicked();
                long time = System.currentTimeMillis();
                if (updateHeldItem) {
                    PvPDojo.schedule(() -> applyAction(player, replay -> replay.heldItem(player, player.getItemInHand(), time))).nextTick();
                }
                if (updateArmor) {
                    PvPDojo.schedule(() -> applyAction(player, replay -> replay.armor(player, player.getInventory().getArmorContents(), time))).nextTick();
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onItemBreak(PlayerItemBreakEvent e) {
        ArmorType armorType = ArmorType.matchType(e.getBrokenItem());
        if (armorType != null) {
            PvPDojo.schedule(() -> {
                long time = System.currentTimeMillis();
                ItemStack[] armor = e.getPlayer().getInventory().getArmorContents();
                applyAction(e.getPlayer(), replay -> replay.armor(e.getPlayer(), armor, time));
            }).nextTick();
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onItemHeld(PlayerItemHeldEvent e) {
        long time = System.currentTimeMillis();
        ItemStack item = e.getPlayer().getInventory().getContents()[e.getNewSlot()];
        applyAction(e.getPlayer(), replay -> replay.heldItem(e.getPlayer(), item, time));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onDrop(PlayerDropItemEvent e) {
        if (e.getPlayer().getItemInHand() == null || e.getPlayer().getItemInHand().getAmount() == 0) {
            long time = System.currentTimeMillis();
            applyAction(e.getPlayer(), replay -> replay.heldItem(e.getPlayer(), null, time));
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (!(e.getDamager() instanceof Player) || !(e.getEntity() instanceof Player))
            return;
        if (!e.isCancelled()) {
            Player t = (Player) e.getEntity();
            Player p = (Player) e.getDamager();
            long time = System.currentTimeMillis();
            if (isCritical(p)) {
                applyAction(t, replay -> replay.critical(t, time));
            }
            if (p.getItemInHand().getEnchantments().containsKey(Enchantment.DAMAGE_ALL)) {
                applyAction(t, replay -> replay.magicdamage(t, time));
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onSneak(PlayerToggleSneakEvent e) {
        long time = System.currentTimeMillis();
        applyAction(e.getPlayer(), replay -> replay.sneak(e.getPlayer(), e.isSneaking(), time));
    }

    private boolean isCritical(Player p) {
        return (p.getVelocity().getY() + 0.0784000015258789) < 0;
    }

}
