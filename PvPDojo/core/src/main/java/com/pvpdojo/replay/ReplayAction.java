/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.replay;

public enum ReplayAction {

    METADATA,
    SPAWN_PLAYER,
    DESPAWN,
    DEATH_PLAYER,
    MOVE,
    DAMAGE,
    SWING,
    SPRINT,
    CRITICAL_HIT,
    HELD_ITEM,
    CHAT,
    BLOCK_BREAK,
    BLOCK_PLACE,
    SNEAK,
    END_OF_REPLAY,
    ARMOR,
    MAGIC_CRIT,
    BLOCK_EAT_DRINK,
    SOUND,
    SCOREBOARD,
    PARTICLE;

    public static ReplayAction get(int i) {
        return values()[i];
    }

}
