/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.replay.commands;

import static com.pvpdojo.util.StringUtils.getDateFromMillis;
import static com.pvpdojo.util.StringUtils.getLS;
import static com.pvpdojo.util.StringUtils.getReadableSeconds;

import java.io.IOException;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.replay.Replay;
import com.pvpdojo.replay.Replayer;
import com.pvpdojo.settings.ServerSwitchSettings;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.PlayerUtil;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Single;

@CommandAlias("replay")
public class ReplayCommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onReplay(User user, @Single String hexId) {
        int id;
        try {
            id = Integer.parseInt(hexId, 16);
        } catch (NumberFormatException ex) {
            user.sendMessage(PvPDojo.WARNING + "This is not a valid replay id");
            return;
        }

        if (PvPDojo.SERVER_TYPE == ServerType.HUB) {
            Replay replay = new Replay(id);
            PvPDojo.schedule(() -> {
                try {
                    replay.initReplay();
                    if (replay.isStaff() && !user.getPlayer().hasPermission("staff")) {
                        user.sendMessage(PvPDojo.WARNING + "This replay is only available for staff members");
                        return;
                    }
                    user.sendMessage(CC.GRAY + getLS());
                    user.sendMessage(Replayer.PREFIX + "Id" + PvPDojo.POINTER + CC.GRAY + hexId);
                    user.sendMessage(Replayer.PREFIX + "Date" + PvPDojo.POINTER + CC.GRAY + getDateFromMillis(replay.getDate()));
                    user.sendMessage(Replayer.PREFIX + "Duration" + PvPDojo.POINTER + CC.GRAY + getReadableSeconds(replay.getDuration() / 1000));
                    user.sendMessage(CC.GRAY + getLS());

                    user.sendMessage(Replayer.PREFIX + CC.GRAY + "Rebuilding map...");
                    replay.getRecordedMap().setWorld(user.getPlayer().getWorld());
                    replay.getRecordedMap().shift(1000, 0, 1000);
                    replay.getRecordedMap().setup(0);
                    replay.getRecordedMap().placeBlocks(false);
                    user.teleport(replay.getInitialLocation());
                    Replayer replayStudio = new Replayer(replay, user.getPlayer());
                    replayStudio.start();
                } catch (IOException e) {
                    user.sendMessage(PvPDojo.WARNING + e.getMessage());
                    e.printStackTrace();
                }
            }).createAsyncTask();
        } else {
            ServerSwitchSettings switchSettings = new ServerSwitchSettings(user);
            switchSettings.addCommand("replay " + hexId);
            PlayerUtil.sendToServer(user.getPlayer(), ServerType.HUB, switchSettings);
        }
    }

    @Override
    public Rank getRank() {
        return Rank.SERVER_ADMINISTRATOR;
    }
}
