/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.replay;

import com.pvpdojo.util.Log;

public class ReplayWriter extends Thread {

    private volatile boolean shutdown;

    public ReplayWriter() {
        super("Replay-writer");
    }

    @Override
    public void run() {
        while (!shutdown || !Recorder.inst().getTasks().isEmpty()) {
            processNextTask();
        }
    }

    public void shutdown() {
        shutdown = true;
        Recorder.inst().getTasks().add(Recorder.POISON_PILL);
        try {
            join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public void processNextTask() {
        try {
            Runnable task = Recorder.inst().getTasks().take();
            try {
                task.run();
            } catch (Throwable thrown) {
                Log.exception("Writing replay", thrown);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

}
