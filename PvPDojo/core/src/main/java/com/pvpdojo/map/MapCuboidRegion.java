/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.map;

import java.util.HashMap;

import com.sk89q.worldedit.Vector;

public class MapCuboidRegion extends MapRegion {

    public MapCuboidRegion(String name, Vector firstBound, Vector secondBound) {
        super(name, firstBound, secondBound);
    }

    @Override
    public MapRegion clone() {
        MapRegion clone = new MapCuboidRegion(name, new Vector(firstBound), new Vector(secondBound));
        clone.flags = new HashMap<>(flags);
        return clone;
    }
}
