/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.map;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.mysql.SQL;
import com.pvpdojo.mysql.SQLBuilder;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.util.Log;

public class MapLoader {

    private static final Multimap<MapType, DojoMap> loadedMaps = HashMultimap.create();

    public static void loadMaps() throws SQLException {
        try (SQLBuilder builder = new SQLBuilder()) {
            builder.setStatement("SELECT type, data FROM maps").executeQuery();
            while (builder.getResultSet().next()) {
                MapType type = MapType.valueOf(builder.getResultSet().getString(1));
                loadedMaps.put(type, DBCommon.GSON.fromJson(builder.getResultSet().getString(2), type.getClazz()));
            }
        }
    }

    public static void removeMap(DojoMap map) {
        loadedMaps.get(map.getType()).remove(map);
    }

    @SuppressWarnings("unchecked")
    public static <T extends DojoMap> List<T> getMaps(MapType type, String... extraFlags) {
        return loadedMaps.get(type).stream()
                         .map(map -> (T) map.copyTo(type.createInstance()))
                         .filter(map -> map.hasExtraFlags(extraFlags))
                         .collect(Collectors.toList());
    }

    public static <T extends DojoMap> T getMap(MapType mapType, String mapName) {
        return MapLoader.<T>getMaps(mapType, DojoMap.ALL_FLAGS).stream()
                                            .filter(map -> map.getName().equalsIgnoreCase(mapName))
                                            .findFirst()
                                            .orElse(null);
    }

    @SuppressWarnings("unchecked")
    public static <T extends DojoMap> T getRandomMap(MapType type, String... extraFlags) {
        Collection<T> matches = MapLoader.getMaps(type, extraFlags);
        return (T) matches.stream()
                          .skip((int) (matches.size() * Math.random()))
                          .findFirst()
                          .map(dojoMap -> dojoMap.copyTo(type.createInstance()))
                          .orElse(null);
    }

    /**
     * This method is used if we add a map after finishing a map setup
     */
    public static <T extends DojoMap> void addMap(T map, Consumer<Throwable> callback) {
        loadedMaps.put(map.getType(), map);
        PvPDojo.schedule(() -> {
            try {
                SQL.remove(Tables.MAPS, "name = ? AND type = ?", map.getName(), map.getType().toString());
            } catch (SQLException e) {
                Log.exception("Deleting map", e);
                return;
            }

            try (SQLBuilder builder = new SQLBuilder()) {
                builder.setStatement("INSERT INTO maps (name, type, data) VALUES(?,?,?)")
                       .createStatement()
                       .setString(1, map.getName())
                       .setString(2, map.getType().name())
                       .setString(3, DBCommon.GSON.toJson(map))
                       .executeUpdate();
            } catch (SQLException e) {
                SQL.handleException(e, callback);
                return;
            }
            callback.accept(null);
        }).createAsyncTask();
    }

}
