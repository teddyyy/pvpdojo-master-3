/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.map;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.boydti.fawe.FaweAPI;
import com.boydti.fawe.util.EditSessionBuilder;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.TrackedObject;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Holder;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.Hologram;
import com.pvpdojo.util.bukkit.Human;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.blocks.BaseBlock;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldguard.bukkit.BukkitUtil;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedPolygonalRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

@SuppressWarnings("deprecation")
public abstract class DojoMap {

    public static final File MAP_SCHEMATICS = new File("../maps");
    public static final String EXTRA_FLAGS = "extra_flags";
    public static final String ALL_FLAGS = "ALL_FLAGS";

    protected String name;
    protected Location[] locations;
    protected Map<String, Location> locationMap = new HashMap<>();
    protected Map<String, String> metaData = new HashMap<>();
    protected Vector firstBound, secondBound;
    protected List<MapRegion> regions = new ArrayList<>();
    protected List<MapHologram> holograms = new ArrayList<>();
    protected List<MapNPC> npcs = new ArrayList<>();
    protected transient int localId;
    protected transient ProtectedRegion region;
    protected transient Map<String, ProtectedRegion> regionMap = new HashMap<>();
    protected transient volatile boolean blocksGenerated;
    protected transient Queue<Runnable> blocksGeneratedQueue = new LinkedList<>();

    public DojoMap(String name, int spawnLocations) {
        this.name = name;
        this.locations = new Location[spawnLocations];
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getExtraFlags() {
        return Arrays.asList(StringUtils.PIPE.split(getMetaData().get(EXTRA_FLAGS)));
    }

    public boolean hasEmptyExtraFlags() {
        return !getMetaData().containsKey(EXTRA_FLAGS);
    }

    public boolean hasExtraFlags(String... extraFlags) {
        if (extraFlags.length == 1 && extraFlags[0].equals(ALL_FLAGS)) {
            return true;
        }

        return extraFlags.length == 0 ? hasEmptyExtraFlags() : getExtraFlags().containsAll(Arrays.asList(extraFlags));
    }

    public Location[] getLocations() {
        return locations;
    }

    public void setLocations(Location[] locations) {
        this.locations = locations;
    }

    public World getWorld() {
        if (locations[0].getWorld() == null) {
            setWorld(Bukkit.getWorlds().get(0));
        }
        return locations[0].getWorld();
    }

    public void setSpawn(int index, Location spawn) {
        if (index >= locations.length) {
            locations = Stream.concat(Stream.of(locations), Stream.of(spawn)).toArray(Location[]::new);
            return;
        }
        locations[index] = spawn;
    }

    public void setSpawn(String title, Location location) {
        locationMap.put(title, location);
    }

    public boolean removeSpawn(String title) {
        return locationMap.remove(title) != null;
    }

    public Location getLocation(String title) {
        return locationMap.get(title);
    }

    public ProtectedRegion getRegion() {
        return region;
    }

    public Vector getFirstBound() {
        return firstBound;
    }

    public void setFirstBound(Vector firstBound) {
        this.firstBound = firstBound;
    }

    public Vector getSecondBound() {
        return secondBound;
    }

    public void setSecondBound(Vector secondBound) {
        this.secondBound = secondBound;
    }

    public List<MapRegion> getRegions() {
        return regions;
    }

    public List<MapHologram> getHolograms() {
        return holograms;
    }

    public List<MapNPC> getNpcs() {
        return npcs;
    }

    public Map<String, Location> getLocationMap() {
        return locationMap;
    }

    public Map<String, String> getMetaData() {
        return Collections.unmodifiableMap(metaData);
    }

    public int getLocalId() {
        return localId;
    }

    public boolean isBlocksGenerated() {
        return blocksGenerated;
    }

    public void addBlocksGeneratedTask(Runnable runnable) {
        if (isBlocksGenerated()) {
            runnable.run();
        } else {
            blocksGeneratedQueue.add(runnable);
        }
    }

    public abstract MapType getType();

    public void setWorld(World world) {
        Stream.of(locations).forEach(location -> location.setWorld(world));
        holograms.forEach(holo -> holo.getLocation().setWorld(world));
        npcs.forEach(npc -> npc.getLocation().setWorld(world));
        locationMap.values().forEach(location -> location.setWorld(world));
    }

    public void shift(int shiftX, int shiftY, int shiftZ) {
        Stream.of(locations).forEach(location -> location.add(shiftX, shiftY, shiftZ));
        firstBound = firstBound.add(shiftX, shiftY, shiftZ);
        secondBound = secondBound.add(shiftX, shiftY, shiftZ);
        regions.forEach(rg -> rg.shift(shiftX, shiftY, shiftZ));
        holograms.forEach(holo -> holo.getLocation().add(shiftX, shiftY, shiftZ));
        npcs.forEach(npc -> npc.getLocation().add(shiftX, shiftY, shiftZ));
        locationMap.values().forEach(location -> location.add(shiftX, shiftY, shiftZ));
    }

    public <T extends DojoMap> T copyTo(T map) {
        map.npcs = npcs.stream().map(MapNPC::clone).collect(Collectors.toList());
        map.holograms = holograms.stream().map(MapHologram::clone).collect(Collectors.toList());
        map.locations = Stream.of(locations).map(Location::clone).toArray(Location[]::new);
        map.regions = regions.stream().map(MapRegion::clone).collect(Collectors.toList());
        map.firstBound = new Vector(firstBound);
        map.secondBound = new Vector(secondBound);
        map.name = name;
        map.locationMap = locationMap.entrySet().stream().collect(Collectors.toMap(Entry::getKey, stringLocationEntry -> stringLocationEntry.getValue().clone()));
        map.metaData = new HashMap<>(metaData); // Strings are immutable
        return map;
    }

    public void setup(int localId) {
        this.localId = localId;
        this.region = new ProtectedCuboidRegion(name + localId, new BlockVector(firstBound), new BlockVector(secondBound));
        RegionManager manager = WGBukkit.getRegionManager(getWorld());
        region.setFlag(DefaultFlag.PASSTHROUGH, StateFlag.State.ALLOW);
        manager.addRegion(region);

        for (MapRegion rg : regions) {
            Holder<ProtectedRegion> region = new Holder<>();
            if (rg instanceof MapCuboidRegion) {
                region.set(new ProtectedCuboidRegion(rg.getName() + "_" + localId, new BlockVector(rg.getFirstBound()), new BlockVector(rg.getSecondBound())));
            } else if (rg instanceof MapPolygonalRegion) {
                region.set(new ProtectedPolygonalRegion(rg.getName() + "_" + localId, new ArrayList<>(((MapPolygonalRegion) rg).getPoints()), rg.getFirstBound().getBlockY(),
                        rg.getSecondBound().getBlockY()));
            }
            rg.getFlags().forEach((key, value) -> region.get().setFlag((StateFlag) DefaultFlag.fuzzyMatchFlag(key), value));
            manager.addRegion(region.get());

            regionMap.put(rg.getName(), region.get());
        }

        for (MapHologram holo : holograms) {
            Hologram h = new Hologram(holo.getText());
            h.show(holo.getLocation());
            h.track();
        }

    }

    public void placeBlocks(boolean async) {
        // Paste that arena into world
        Runnable task = () -> {

            npcs.forEach(npc -> PvPDojo.newChain().asyncFirst(npc::getProfile).syncLast(profile -> new Human(npc.getLocation(), null, profile, npc.getName()).track()).execute());

            File file = new File(MAP_SCHEMATICS, name + "_" + getType() + ".schematic");
            try {
                EditSession session = ClipboardFormat.SCHEMATIC.load(file).paste(FaweAPI.getWorld(getWorld().getName()), firstBound, false, false, null);
                session.addNotifyTask(() -> PvPDojo.schedule(this::finalizeBlocks).sync());
                PvPDojo.schedule(this::finalizeBlocks).createTask(15);
            } catch (IOException e) {
                e.printStackTrace();
            }
        };

        if (async) {
            PvPDojo.schedule(task).createAsyncTask();
        } else {
            task.run();
        }
    }

    private void finalizeBlocks() {
        if (!blocksGenerated) {
            blocksGenerated = true;
            blocksGeneratedQueue.forEach(Runnable::run);
            blocksGeneratedQueue.clear();
        }
    }

    public void remove(boolean removeBlocks) {
        RegionManager manager = WGBukkit.getRegionManager(getWorld());
        manager.removeRegion(region.getId());
        regions.forEach(rg -> manager.removeRegion(rg.getName() + "_" + localId));
        TrackedObject.getTrackedObjects().stream().filter(trackedObject -> region.contains(BukkitUtil.toVector(trackedObject.getLocation()))).forEach(TrackedObject::untrack);
        blocksGenerated = false;

        if (removeBlocks) {
            PvPDojo.schedule(() -> {
                EditSession session = new EditSessionBuilder(getWorld().getName()).fastmode(true).build();
                session.setBlocks(new CuboidRegion(firstBound, secondBound), new BaseBlock(0));
                session.flushQueue();
            }).createAsyncTask();
        }
    }

    public void resetBlocks() {
        PvPDojo.schedule(() -> {
            File file = new File(MAP_SCHEMATICS, name + "_" + getType() + ".schematic");
            try {
                EditSession session = ClipboardFormat.SCHEMATIC.load(file).paste(FaweAPI.getWorld(getWorld().getName()), firstBound, false, true, null);
                session.addNotifyTask(() -> {
                    blocksGenerated = true;
                    PvPDojo.schedule(() -> blocksGeneratedQueue.forEach(Runnable::run)).sync();
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).createAsyncTask();
    }

    public Map<String, ProtectedRegion> getRegionMap() {
        return regionMap;
    }

    /**
     * Loops through all players in the world this map is placed and collects players playing on this map
     *
     * @return The players actively playing on this map
     */
    public Set<Player> getPlayers() {
        if (region != null) {
            return Bukkit.getOnlinePlayers().stream()
                         .map(User::getUser)
                         .filter(Objects::nonNull)
                         .filter(usr -> usr.getApplicableRegions().contains(region))
                         .map(User::getPlayer)
                         .collect(Collectors.toSet());
        }
        return Collections.emptySet();
    }

}
