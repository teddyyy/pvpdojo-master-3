/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.map;

import java.util.UUID;

import org.bukkit.Location;

import com.pvpdojo.DBCommon;

import net.minecraft.util.com.mojang.authlib.GameProfile;
import net.minecraft.util.com.mojang.authlib.properties.Property;

public class MapNPC implements Cloneable {

    private Location location;
    private String name;
    private String npcName;
    private UUID skinUUID;

    public MapNPC(String name, Location location, GameProfile profile) {
        this.name = name;
        this.location = location;
        skinUUID = profile.getId();
    }

    public MapNPC(String name, Location location, String npcName, UUID skinUUID) {
        this.name = name;
        this.location = location;
        this.npcName = npcName;
        this.skinUUID = skinUUID;
    }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public String getNpcName() {
        return npcName;
    }

    public UUID getSkinUUID() {
        return skinUUID;
    }

    public GameProfile getProfile() {
        GameProfile skinProfile = DBCommon.getProfile(skinUUID);
        GameProfile profile = new GameProfile(UUID.randomUUID(), npcName != null ? npcName : skinProfile.getName());
        Property property = skinProfile.getProperties().get("textures").iterator().next();
        profile.getProperties().put("textures", new Property("textures", property.getValue(), property.getSignature()));
        return profile;
    }

    @Override
    protected final MapNPC clone() {
        return new MapNPC(name, location.clone(), npcName, skinUUID);
    }

}
