/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.map;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;

public class MapHologram implements Cloneable {

    private String name;
    private List<String> text;
    private Location location;

    public MapHologram(String name, Location location, List<String> text) {
        this.name = name;
        this.text = new ArrayList<>();
        this.text.addAll(text);
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public List<String> getText() {
        return text;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    protected final MapHologram clone() {
        return new MapHologram(name, location.clone(), new ArrayList<>(text));
    }

}
