/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.map;

public class DefaultMap extends DojoMap {

    public DefaultMap(String name, int spawnLocations) {
        super(name, spawnLocations);
    }

    public DefaultMap() {
        super("none", 1);
    }

    @Override
    public MapType getType() {
        return MapType.DEFAULT;
    }

}
