/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.map;

import com.pvpdojo.session.arena.CTMArena;
import com.pvpdojo.session.arena.DeathmatchArena;
import com.pvpdojo.session.arena.DojoGamesArena;
import com.pvpdojo.session.arena.FeastFightArena;
import com.pvpdojo.session.arena.HGArena;
import com.pvpdojo.session.arena.LavaDuelArena;

public enum MapType {
    DEATHMATCH(DeathmatchArena.class),
    DOJOGAMES(DojoGamesArena.class),
    DEFAULT(DefaultMap.class),
    FEAST_FIGHT(FeastFightArena.class),
    HG(HGArena.class),
    LAVA_DUEL(LavaDuelArena.class),
    CTM(CTMArena.class);

    private Class<? extends DojoMap> clazz;

    MapType(Class<? extends DojoMap> clazz) {
        this.clazz = clazz;
    }

    public Class<? extends DojoMap> getClazz() {
        return clazz;
    }

    public DojoMap createInstance() {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
