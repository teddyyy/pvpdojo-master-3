/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.sk89q.worldedit.BlockVector2D;
import com.sk89q.worldedit.Vector;

public class MapPolygonalRegion extends MapRegion {

    private List<BlockVector2D> points;

    public MapPolygonalRegion(String name, List<BlockVector2D> points, Vector firstBound, Vector secondBound) {
        super(name, firstBound, secondBound);
        this.points = points;
    }

    public List<BlockVector2D> getPoints() {
        return points;
    }

    @Override
    public void shift(int shiftX, int shiftY, int shiftZ) {
        super.shift(shiftX, shiftY, shiftZ);
        points = points.stream().map(blockVector2D -> blockVector2D.add(shiftX, shiftZ).toBlockVector2D()).collect(Collectors.toList());
    }

    @Override
    public MapRegion clone() {
        MapRegion clone = new MapPolygonalRegion(name, new ArrayList<>(points), new Vector(firstBound), new Vector(secondBound));
        clone.flags = new HashMap<>(flags);
        return clone;
    }

}
