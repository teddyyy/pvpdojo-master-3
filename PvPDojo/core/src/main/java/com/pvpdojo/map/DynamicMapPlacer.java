/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.map;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import org.bukkit.event.EventHandler;

import com.pvpdojo.bukkit.events.RegionEnterEvent;
import com.pvpdojo.bukkit.listeners.DojoListener;

public class DynamicMapPlacer implements DojoListener {

    private static int lastId;
    private static final List<DojoMap> PLACEMENT_QUEUE = new CopyOnWriteArrayList<>();

    public static int generateMap(DojoMap map) {
        return generateMap(map, true);
    }

    public static synchronized int generateMap(DojoMap map, boolean asyncBlockPlacement) {
        int localId = ++lastId; // starts with 1 cuz 0 is dedicated to standard map
        int shift = localId * 10_000;

        map.shift(shift, 0, shift);
        map.setup(localId);
        if (asyncBlockPlacement) {
            PLACEMENT_QUEUE.add(map);
        } else {
            map.placeBlocks(false);
        }

        return localId;
    }

    @EventHandler
    public void onRegionEnter(RegionEnterEvent e) {
        Optional<DojoMap> toPlace = PLACEMENT_QUEUE.stream().filter(map -> map.getRegion().getId().equals(e.getRegion().getId())).findFirst();
        if (toPlace.isPresent()) {
            PLACEMENT_QUEUE.remove(toPlace.get());
            toPlace.get().placeBlocks(true);
        }
    }

}
