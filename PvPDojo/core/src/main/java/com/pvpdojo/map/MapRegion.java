/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.map;

import java.util.HashMap;
import java.util.Map;

import com.sk89q.worldedit.Vector;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.StateFlag.State;

public abstract class MapRegion implements Cloneable {

    protected String name;
    protected Vector firstBound, secondBound;
    protected Map<String, StateFlag.State> flags;

    public MapRegion(String name, Vector firstBound, Vector secondBound) {
        this.name = name;
        this.firstBound = firstBound;
        this.secondBound = secondBound;
        this.flags = new HashMap<>();
        flags.put(DefaultFlag.PASSTHROUGH.getName(), State.ALLOW);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Vector getFirstBound() {
        return firstBound;
    }

    public void setFirstBound(Vector firstBound) {
        this.firstBound = firstBound;
    }

    public Vector getSecondBound() {
        return secondBound;
    }

    public void setSecondBound(Vector secondBound) {
        this.secondBound = secondBound;
    }

    public Map<String, StateFlag.State> getFlags() {
        return flags;
    }

    public void shift(int shiftX, int shiftY, int shiftZ) {
        firstBound = firstBound.add(shiftX, shiftY, shiftZ);
        secondBound = secondBound.add(shiftX, shiftY, shiftZ);
    }
    
    public abstract MapRegion clone();

}
