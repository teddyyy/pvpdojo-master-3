/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.map;

import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.StateFlag.State;

public class StateFlagCombo {

    private StateFlag flag;
    private State state;

    public StateFlagCombo(StateFlag flag, State state) {
        this.flag = flag;
        this.state = state;
    }

    public StateFlag getFlag() {
        return flag;
    }

    public State getState() {
        return state;
    }

}
