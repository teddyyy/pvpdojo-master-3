/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.bukkit.entity.Player;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.pvpdojo.userdata.User;

public class UserList<T extends User> {

    private final Cache<UUID, T> users = CacheBuilder.newBuilder().expireAfterAccess(1, TimeUnit.MINUTES).build();
    private Function<UUID, T> userSupplier;

    public UserList(Function<UUID, T> userSupplier) {
        this.userSupplier = userSupplier;
    }

    public T loginUser(UUID uuid) {
        T user = userSupplier.apply(uuid);
        users.put(uuid, user);
        return user;
    }

    public void logoutUser(Player player) {
        getUser(player).setLoggedOut(true);
    }

    public T getUser(UUID uuid) {
        return users.getIfPresent(uuid);
    }

    public T getUser(Player player) {
        return player == null ? null : getUser(player.getUniqueId());
    }

    public Map<UUID, T> getUsers() {
        return users.asMap();
    }

}
