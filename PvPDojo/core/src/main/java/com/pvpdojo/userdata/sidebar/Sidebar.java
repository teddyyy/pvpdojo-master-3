/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata.sidebar;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

public class Sidebar {

    private final Scoreboard scoreboard;
    private final Map<Integer, SpecialTeam> teams;
    private final Objective objective;

    /**
     * Creates a new sidebar with a new scoreboard attachment.
     */
    public Sidebar() {
        this(Bukkit.getScoreboardManager().getNewScoreboard());
    }

    /**
     * Creates a new titled sidebar with a new scoreboard attachment.
     *
     * @param title
     */
    public Sidebar(String title) {
        this(Bukkit.getScoreboardManager().getNewScoreboard(), title);
    }

    /**
     * Creates a new sidebar with the given scoreboard.
     */
    public Sidebar(Scoreboard scoreboard) {
        this(scoreboard, UUID.randomUUID().toString().substring(0, 6));
    }

    /**
     * Creates a new titles sidebar with the given scoreboard.
     *
     * @param title
     */
    public Sidebar(Scoreboard scoreboard, String title) {
        Preconditions.checkNotNull(scoreboard, "Sidebar scoreboard must not be null");
        Preconditions.checkNotNull(title, "Sidebar title must not be null");

        this.scoreboard = scoreboard;
        this.teams = new HashMap<>();

        this.objective = scoreboard.registerNewObjective(title, "dummy");
        this.objective.setDisplayName(title);

        this.objective.setDisplaySlot(DisplaySlot.SIDEBAR);
    }

    public void show() {
        this.objective.setDisplaySlot(DisplaySlot.SIDEBAR);
    }

    public void hide() {
        this.objective.setDisplaySlot(null);
    }

    /**
     * Get the scoreboard from this sidebar.
     *
     * @return scoreboard
     */
    public Scoreboard getScoreboard() {
        return scoreboard;
    }

    /**
     * Get the objective of this sidebar.
     *
     * @return objective
     */
    public Objective getObjective() {
        return objective;
    }

    /**
     * Retrieve the title of the sidebar.
     *
     * @return title
     */
    public String getTitle() {
        return this.objective.getDisplayName();
    }

    /**
     * Changes the title of the sidebar.
     *
     * @param text
     */
    public void setTitle(String text) {
        this.objective.setDisplayName(text);
    }

    public int getSize() {
        return teams.size();
    }

    /**
     * Adds a line to the sidebar at the given line (score) number.
     *
     * @param line
     * @param text
     * @throws IllegalArgumentException If Sidebar exceeds limit of lines (16)
     */
    public boolean set(int line, String text) {
        // text = fixDuplicates(text, line);
        SpecialTeam specialTeam;
        if (teams.containsKey(line)) {
            specialTeam = teams.get(line);
        } else {
            if (teams.size() >= 16) {
                throw new IllegalStateException("Exceeded limit of 16 lines");
            }
            specialTeam = createSpecialTeam(line);
            teams.put(line, specialTeam);
        }
        updateSpecialTeam(text, specialTeam, line);
        if (!this.objective.getScore(specialTeam.getLineText()).isScoreSet()) {
            this.objective.getScore(specialTeam.getLineText()).setScore(line);
        }
        return true;
    }

    public void set(SidebarProvider sidebarProvider) {
        sidebarProvider.render(this).forEachEntry(this::set);
    }

    public void changeLine(int prevLine, int newLine) {
        Preconditions.checkArgument(teams.containsKey(prevLine), "Line " + prevLine + " does not exist");

        SpecialTeam specialTeam = teams.remove(prevLine);
        this.objective.getScore(specialTeam.getLineText()).setScore(newLine);
        teams.put(newLine, specialTeam);
    }

    /**
     * Get all text at the given line (score) number.
     *
     * @param line
     * @return text
     */
    public String get(int line) {
        return teams.containsKey(line) ? teams.get(line).getFullText() : null;
    }

    /**
     * Get the line (score) number of the given text.
     *
     * @param text
     * @return line
     * @throws IllegalArgumentException If the text cannot be found in the sidebar.
     */
    public int getLine(String text) throws IllegalArgumentException {
        for (Entry<Integer, SpecialTeam> entry : this.teams.entrySet()) {
            if (entry.getValue().getFullText().equals(text)) {
                return entry.getKey();
            }
        }
        throw new IllegalArgumentException("line not found");
    }

    /**
     * Removes all entries with the given text.
     *
     * @param text
     */
    public void remove(String text) {
        remove(text, null);
    }

    /**
     * Removes all entries with the given line (score) number.
     *
     * @param line
     */
    public void remove(int line) {
        remove(null, line);
    }

    /**
     * Removes all entries in the sidebar.
     */
    public void removeAll() {
        remove(null, null);
    }

    /**
     * Removes all entries with the given text and line (score) number.
     *
     * @param text
     */
    public void remove(String text, Integer line) {
        Iterator<Entry<Integer, SpecialTeam>> iterator = this.teams.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<Integer, SpecialTeam> entry = iterator.next();
            if (text != null && !entry.getValue().getFullText().equals(text)) {
                continue;
            }
            if (line != null && !entry.getKey().equals(line)) {
                continue;
            }
            this.scoreboard.resetScores(entry.getValue().getLineText());
            entry.getValue().getTeam().unregister();
            iterator.remove();
        }
    }

    private String fixDuplicates(String text, int line) {
        boolean unique = false;
        while (!unique) {
            unique = true;
            for (Entry<Integer, SpecialTeam> team : this.teams.entrySet()) {
                if (team.getKey() != line && team.getValue().getLineText().equals(text)) {
                    text = ChatColor.RESET + text;
                    unique = false;
                }
            }
        }
        return text;
    }

    private SpecialTeam createSpecialTeam(int lineNum) {
        Team team = this.scoreboard.registerNewTeam("sb-" + lineNum + "-" + UUID.randomUUID().toString().substring(0, 6));
        return new SpecialTeam(team);
    }

    private void updateSpecialTeam(String text, SpecialTeam team, int number) {
        String[] split = getSplitText(text, number);

        String prefix = split[0] == null ? "" : split[0];
        String line = split[1];
        String suffix = split[2] == null ? "" : split[2];

        Team sbTeam = team.getTeam();

        if (sbTeam.getEntries().size() == 1) {
            String currentEntry = sbTeam.getEntries().iterator().next();
            if (!currentEntry.equals(line)) {
                scoreboard.resetScores(currentEntry);
                sbTeam.removeEntry(sbTeam.getEntries().iterator().next());
                sbTeam.addEntry(line);
            }
        } else {
            for (String entry : sbTeam.getEntries()) {
                scoreboard.resetScores(entry);
                sbTeam.removeEntry(entry);
            }
            sbTeam.addEntry(line);
        }

        if (!prefix.equals(sbTeam.getPrefix())) {
            sbTeam.setPrefix(prefix);
        }

        if (!suffix.equals(sbTeam.getSuffix())) {
            sbTeam.setSuffix(suffix);
        }

        team.fullText = text;
        team.lineText = line;
    }

    private String[] getSplitText(String text, int number) {
        Splitter splitter = Splitter.fixedLength(16);
        List<String> split = Lists.newArrayList(splitter.split(text).iterator());

        String prefix = split.get(0);
        String line = fixDuplicates(ChatColor.getLastColors(prefix), number);
        String suffix = null;

        if (text.length() > 16) {
            if (line.length() > 16) {
                line = split.get(1);
            } else {
                suffix = split.get(1);
            }
        } else if (line.length() > 16) {
            prefix = null;
            line = split.get(0);
        }
        if (text.length() > 32) {
            line = split.get(1);
            suffix = split.get(2);
        }
        return new String[]{prefix, line, suffix};
    }

    private class SpecialTeam {

        private final Team team;
        private String lineText;
        private String fullText;

        public SpecialTeam(Team team) {
            this.team = team;
        }

        public Team getTeam() {
            return team;
        }

        public String getLineText() {
            return lineText;
        }

        public String getFullText() {
            return fullText;
        }

    }
}
