/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata;

import static com.pvpdojo.util.StreamUtils.negate;
import static com.pvpdojo.util.StreamUtils.or;

import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Material;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.gui.HotbarGUI;
import com.pvpdojo.menus.ReportMenu;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class AdminModeHotbar extends HotbarGUI {

    private static AdminModeHotbar hotbar = new AdminModeHotbar();

    public static AdminModeHotbar get() {
        return hotbar;
    }

    private AdminModeHotbar() {

        setItem(0, new ItemBuilder(Material.COMPASS).name(CC.GREEN + "Random Teleport").build(), onRightClick(player -> {
            List<User> foundPlayers = Bukkit.getOnlinePlayers().stream()
                                            .filter(negate(player::equals))
                                            .map(User::getUser)
                                            .filter(negate(or(User::isAdminMode, or(User::isAFK, User::isVanish))))
                                            .collect(Collectors.toList());
            User user = User.getUser(player);
            if (!foundPlayers.isEmpty()) {
                player.teleport(foundPlayers.get(PvPDojo.RANDOM.nextInt(foundPlayers.size())).getPlayer());
                user.sendMessage(PvPDojo.PREFIX + CC.GREEN + "You have been teleported to a random player on your server");
            } else {
                user.sendMessage(PvPDojo.WARNING + "Could not find a suitable target on this server");
            }
        }));

        setItem(1, new ItemBuilder(Material.BOOK).name(CC.DARK_GREEN + "Reports").build(), onRightClick(player -> new ReportMenu(player).open()));

        setItem(3, new ItemBuilder(Material.CHEST).name(CC.GREEN + "Invsee Player").build(), onClickPlayer((player, target) -> player.performCommand("inv " + target.getNick())));

        setItem(4, new ItemBuilder(Material.BLAZE_ROD).name(CC.GREEN + "Player Info").build(), onClickPlayer((player, target) -> player.performCommand("pinfo " + target.getNick())));

        setItem(5, new ItemBuilder(Material.WATCH).name(CC.GREEN + "Freeze Player").build(),
                onClickPlayer((player, target) -> player.performCommand("freeze " + target.getNick())));

        setItem(8, new ItemBuilder(Material.MELON).name(CC.GREEN + "Visibility").build(), onRightClick(player -> player.performCommand("v")));
    }

}
