/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata.stats;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.mysql.SQL;
import com.pvpdojo.mysql.SQLBuilder;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.util.Log;
import lombok.Getter;

import java.sql.SQLException;
import java.util.UUID;

public class PersistentHGStats implements Stats {

    public static final int GAME_CREDIT = 2;
    public static final int KILL_CREDIT = 10;
    public static final int KILL_BONUS_MODIFIER = 2;
    public static final int WIN_CREDIT = 20;

    public static final int SP_MODIFIER_WIN = 7;
    public static final double SP_MODIFIER_KILL = 0.5;
    public static final double SP_MODIFIER_GAMES_PLAYED = -0.5;
    public static final int SP_MODIFIER_BONUS_KIT = 3;

    private UUID uuid;

    @Getter
    private int kills, gamesPlayed, wins, bonusKitWins, maxKillstreak = -1, money;
    private long playTime;

    public PersistentHGStats(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public int getKillstreak() {
        return 0;
    }

    @Override
    public int getDeaths() {
        return gamesPlayed - wins;
    }

    @Override
    public void resetKillstreak() {
    }

    @Override
    public int incrKillstreak() {
        return 0;
    }

    @Override
    public void loadKillstreak(int loadedKillstreak) {
    }

    @Override
    public long getCurrentPlayTime() {
        return 0;
    }

    @Override
    public long getPlayTime() {
        return playTime + getCurrentPlayTime();
    }

    @Override
    public void addKill() {
        kills++;
        PvPDojo.schedule(() -> {
            SQL.updateMultiple(Tables.HG_STATS, "kills = kills + 1", "uuid = ?", uuid.toString());
            addMoney(KILL_CREDIT);
        }).createNonMainThreadTask();
    }

    @Override
    public void addDeath() {

    }

    public void addGamePlayed() {
        gamesPlayed++;
        PvPDojo.schedule(() -> {
            SQL.updateMultiple(Tables.HG_STATS, "gamesplayed = gamesplayed + 1", "uuid = ?", uuid.toString());
            addMoney(GAME_CREDIT);
        }).createNonMainThreadTask();
    }

    public void addWin() {
        wins++;
        PvPDojo.schedule(() -> {
            SQL.updateMultiple(Tables.HG_STATS, "wins = wins + 1", "uuid = ?", uuid.toString());
            addMoney(WIN_CREDIT);
        }).createNonMainThreadTask();
    }

    public void addBonusKitWin() {
        bonusKitWins++;
        PvPDojo.schedule(() -> {
            SQL.updateMultiple(Tables.HG_STATS, "bonuskit_wins = bonuskit_wins + 1", "uuid = ?", uuid.toString());
        }).createNonMainThreadTask();
    }

    @Override
    public void applyMaxKillstreak(int currentKillstreak) {
        if (maxKillstreak == -1) {
            Log.warn("Could not apply max killstreak for " + uuid);
            return;
        }
        if (currentKillstreak > maxKillstreak) {
            maxKillstreak = currentKillstreak;
            PvPDojo.schedule(() -> SQL.updateInteger(Tables.HG_STATS, "killrecord", "uuid = ?", maxKillstreak, uuid.toString())).createNonMainThreadTask();
        }
    }

    public void addMoney(int amount) {
        money += amount;
        PvPDojo.schedule(() -> SQL.updateMultiple(Tables.HG_STATS, "money = money + " + amount, "uuid = ?", uuid.toString())).createNonMainThreadTask();
    }

    @Override
    public void addPlayTime(long playTime) {
        PvPDojo.schedule(() -> SQL.updateMultiple(Tables.HG_STATS, "playtime = playtime + " + playTime, "uuid = ?", uuid.toString())).createNonMainThreadTask();
    }

    public int getSkillPoints() {
        return (int) (wins * SP_MODIFIER_WIN + bonusKitWins * SP_MODIFIER_BONUS_KIT + kills * SP_MODIFIER_KILL + gamesPlayed * SP_MODIFIER_GAMES_PLAYED);
    }

    @Override
    public void load() throws SQLException {
        boolean create = false;
        try (SQLBuilder sql = new SQLBuilder()) {
            sql.select(Tables.HG_STATS, "*").where("uuid = ?").executeQuery(uuid.toString());

            if (sql.next()) {
                this.kills = sql.getInteger("kills");
                this.gamesPlayed = sql.getInteger("gamesplayed");
                this.wins = sql.getInteger("wins");
                this.bonusKitWins = sql.getInteger("bonuskit_wins");
                this.maxKillstreak = sql.getInteger("killrecord");
                this.money = sql.getInteger("money");
                this.playTime = sql.getLong("playtime");
            } else {
                create = true;
            }
        }
        if (create) {
            try (SQLBuilder sql = new SQLBuilder()) {
                sql.insert(Tables.HG_STATS, "uuid").values("?").executeUpdate(uuid.toString());
            }
        }
    }

    @Override
    public boolean isLoadable() {
        return true;
    }
}
