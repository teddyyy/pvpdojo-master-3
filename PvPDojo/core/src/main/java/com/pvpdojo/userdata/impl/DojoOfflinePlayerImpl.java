/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata.impl;

import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.PunishmentManager.Punishment;

import net.minecraft.util.com.mojang.authlib.GameProfile;

public class DojoOfflinePlayerImpl implements DojoOfflinePlayer {

    private Punishment currentBan, currentMute;
    private GameProfile profile;
    private PersistentData persistentData;
    private String lastIpAddress;
    private long firstlogin;
    private long lastlogin;
    private long lastClanSwitch;

    public DojoOfflinePlayerImpl(User user) {
        this(NMSUtils.get(user.getPlayer()).getProfile(), null, null, user.getPlayer().getAddress().getHostString(), -1, -1, -1);
        this.persistentData = user.getPersistentData();
    }

    public DojoOfflinePlayerImpl(GameProfile profile) {
        this(profile, null, null, "", -1, -1, -1);
    }

    public DojoOfflinePlayerImpl(GameProfile profile, Punishment currentBan, Punishment currentMute, String lastIpAddress, long firstlogin, long lastlogin, long lastClanSwitch) {
        this.currentBan = currentBan;
        this.currentMute = currentMute;
        this.profile = profile;
        this.lastIpAddress = lastIpAddress;
        this.firstlogin = firstlogin;
        this.lastlogin = lastlogin;
        this.persistentData = new PersistentData(getUniqueId());
        this.lastClanSwitch = lastClanSwitch;
    }

    @Override
    public GameProfile getProfile() {
        return profile;
    }

    @Override
    public boolean isOp() {
        return false;
    }

    @Override
    public void setOp(boolean value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Map<String, Object> serialize() {
        return null;
    }

    @Override
    public boolean isOnline() {
        return Bukkit.getPlayer(profile.getId()) != null;
    }

    @Override
    public String getName() {
        return profile.getName();
    }

    @Override
    public UUID getUniqueId() {
        return profile.getId();
    }

    @Override
    public boolean isBanned() {
        return currentBan != null;
    }

    @Override
    public void setBanned(boolean banned) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isWhitelisted() {
        return false;
    }

    @Override
    public void setWhitelisted(boolean value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Player getPlayer() {
        return Bukkit.getPlayer(profile.getId());
    }

    @Override
    public long getFirstPlayed() {
        return firstlogin;
    }

    @Override
    public long getLastPlayed() {
        return lastlogin;
    }

    @Override
    public long getLastClanSwitch() {
        return lastClanSwitch;
    }

    @Override
    public String getLastIpAddress() {
        return lastIpAddress;
    }

    @Override
    public boolean hasPlayedBefore() {
        return firstlogin != -1;
    }

    @Override
    public Location getBedSpawnLocation() {
        return null;
    }

    @Override
    public Punishment getCurrentBan() {
        return currentBan;
    }

    @Override
    public PersistentData getPersistentData() {
        return persistentData;
    }

    @Override
    public boolean isOnlineProxy() {
        return BungeeSync.getOnlinePlayers().contains(getName());
    }

    @Override
    public String getServerName() {
        return BungeeSync.findPlayer(getUniqueId());
    }

    @Override
    public boolean isMuted() {
        return currentMute != null;
    }

    @Override
    public Punishment getCurrentMute() {
        return currentMute;
    }

    @Override
    public boolean isNicked() {
        return Redis.get().getHValue(BungeeSync.NICK_TO_REAL_NAME, getName().toLowerCase()) != null;
    }

    @Override
    public String getNick() {
        String nick = Redis.get().getValue(BungeeSync.NICK_TO_REAL_NAME + "_" + getName());
        return nick != null ? nick : getName();
    }

    @Override
    public UUID getNickFakeStats() {
        return UUID.fromString(Redis.get().getValue(BungeeSync.NICK_FAKE_STATS + getUniqueId().toString()));
    }

    @Override
    public int hashCode() {
        return getUniqueId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof DojoOfflinePlayer && ((DojoOfflinePlayer) obj).getUniqueId().equals(getUniqueId());
    }
}
