/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Material;

import com.pvpdojo.abilityinfo.AbilityType;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.MathUtil;

public class KitAbilityData {

    private int id;
    private Set<Integer> upgrades;
    private AbilityType type;
    private Material clickItem;

    public KitAbilityData(int ability, Set<Integer> upgrades, AbilityType type, Material clickItem) {
        this.id = ability;
        this.upgrades = upgrades;
        this.type = type;
        this.clickItem = clickItem;
    }

    public KitAbilityData(int id) {
        this.id = id;
        upgrades = new HashSet<>();
        type = AbilityLoader.getAbilityData(id).isActive() ? AbilityType.CLICK : AbilityType.PASSIVE;
    }

    public int getId() {
        return id;
    }

    public Set<Integer> getUpgrades() {
        return upgrades;
    }

    public AbilityType getAbilityType() {
        return type;
    }

    public void setAbilityType(AbilityType type) {
        this.type = type;
    }

    public boolean hasUpgrade(int upgrade) {
        for (int u : upgrades) {
            if (u == upgrade)
                return true;
        }
        return false;
    }

    public void setAbility(int ability) {
        this.id = ability;
    }

    public void setClickItem(Material clickItem) {
        this.clickItem = clickItem;
    }

    public Material getClickItem() {
        return clickItem;
    }

    public KitAbilityData cloneAbility() {
        return new KitAbilityData(getId(), new HashSet<>(getUpgrades()), getAbilityType(), clickItem);
    }

    public static ArrayList<KitAbilityData> cloneArray(ArrayList<KitAbilityData> cloned) {
        ArrayList<KitAbilityData> list = new ArrayList<>();
        for (KitAbilityData data : cloned) {
            list.add(data.cloneAbility());
        }
        return list;
    }

    public static ArrayList<KitAbilityData> getAbilityDataFromString(String ability_string) {
        // ability_id:type:1/2/3/4:clickitem,ability_id:type:1/2/3/4
        ArrayList<KitAbilityData> list = new ArrayList<>();
        String[] abilities = ability_string.split(",");
        for (String ability : abilities) {
            if (ability.isEmpty())
                continue;
            String[] split = ability.split(":");
            int abilityId = Integer.valueOf(split[0]);
            if (AbilityLoader.getAbilityData(abilityId) == null) {
                Log.warn("invalid ability_id : " + abilityId);
                continue;
            }
            AbilityType type = AbilityType.getAbilityTypeById(Integer.valueOf(split[1]));

            Set<Integer> upgradeList = new HashSet<>();
            if (split.length > 2) {
                String[] upgrades = split[2].split("/");
                for (String array : upgrades) {
                    if (array.isEmpty() || !MathUtil.isInteger(array))
                        continue;
                    int upgrade_id = Integer.valueOf(array);
                    upgradeList.add(upgrade_id);
                }
            }
            Material clickItem = null;
            if (split.length > 3) {
                clickItem = Material.values()[Integer.valueOf(split[3])];
            }

            list.add(new KitAbilityData(abilityId, upgradeList, type, clickItem));
        }
        return list;
    }

    public static String getStringFromAbilityData(List<KitAbilityData> data) {
        StringBuilder builder = new StringBuilder();
        for (KitAbilityData abilities : data) {
            StringBuilder ability = new StringBuilder(abilities.getId() + ":" + abilities.type.getId() + ":");
            for (int upgrades : abilities.getUpgrades()) {
                ability.append(upgrades).append("/");
            }
            if (abilities.clickItem != null) {
                ability.append(":").append(abilities.clickItem.ordinal());
            }
            builder.append(ability).append(",");
        }
        return builder.toString();
    }
}