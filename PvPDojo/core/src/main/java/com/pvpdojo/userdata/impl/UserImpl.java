/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata.impl;

import co.aikar.locales.MessageKeyProvider;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.Warp;
import com.pvpdojo.achievement.Achievement;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.clan.Clan;
import com.pvpdojo.command.staff.AdminCommand;
import com.pvpdojo.gui.HotbarGUI;
import com.pvpdojo.gui.InputGUI;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.menu.InventoryMenu;
import com.pvpdojo.party.Party;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.session.GameSession;
import com.pvpdojo.session.entity.TeamArenaEntity;
import com.pvpdojo.settings.CooldownSettings;
import com.pvpdojo.settings.FastDB;
import com.pvpdojo.settings.ServerSwitchSettings;
import com.pvpdojo.settings.UserSettings;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.Request;
import com.pvpdojo.userdata.User;
import com.pvpdojo.userdata.sidebar.Sidebar;
import com.pvpdojo.userdata.stats.DefaultStats;
import com.pvpdojo.userdata.stats.Stats;
import com.pvpdojo.util.ExpireHashMap;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.Hologram;
import com.sk89q.worldguard.bukkit.RegionQuery;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import eu.the5zig.mod.server.api.ModUser;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.util.com.mojang.authlib.GameProfile;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class UserImpl implements User {

    private Player player;
    private PersistentData db;
    private UUID uuid;

    private boolean dataLoaded = false;
    private boolean loggedOut = false;
    private boolean autoRespawn = true;
    private boolean showSpectators = true;
    @Getter
    @Setter
    private boolean adminMode, build, vanishTablist, vanish, spectator, frozenByStaff, cancelNextFall, checkNextMove;
    private boolean useStats = true;
    private boolean deathDrops = true;

    private Rank spoofedRank = null;
    private GameSession session;
    private TeamArenaEntity team;
    private InventoryMenu menu;
    private InputGUI inputGui;
    private HotbarGUI hotbarGui;
    private Sidebar sidebar;
    private Stats stats;

    private Set<ProtectedRegion> applicableRegions = new HashSet<>();
    private Location lastValidLocation, lastTrackedObjectsUpdate;

    private int noMoveSeconds;

    private ExpireHashMap<UUID, Request> requests = new ExpireHashMap<>(1, TimeUnit.MINUTES);

    private Clan clan;
    private Party party;
    private ChatChannel chatChannel = ChatChannel.GLOBAL;
    private UserSettings settings = new UserSettings();
    private FastDB fastDB = new FastDB();
    private CooldownSettings cooldownSettings = new CooldownSettings();
    @Getter
    @Setter
    private List<UUID> currentAccountGrid = new LinkedList<>();

    private Hologram attachedHologram;
    private int hologramTaskId;
    private long cancelMoveUntil;

    public UserImpl(UUID uuid) {
        this.db = new PersistentData(uuid);
        this.uuid = uuid;
        this.player = Bukkit.getPlayer(uuid);
        this.player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
        this.sidebar = new Sidebar(player.getScoreboard());
        this.stats = new DefaultStats(db);
    }

    public GameSession getSession() {
        return session;
    }

    @Override
    public PersistentData getPersistentData() {
        return db;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public UUID getUUID() {
        return uuid;
    }

    @Override
    public boolean isLoggedOut() {
        return loggedOut;
    }

    @Override
    public boolean isDataLoaded() {
        return dataLoaded;
    }

    @Override
    public void setDataLoaded(boolean dataLoaded) {
        this.dataLoaded = dataLoaded;
    }

    @Override
    public Rank getRank() {
        return fastDB.isRequireAuth() ? Rank.DEFAULT : db.getRank();
    }

    @Override
    public Rank getSpoofedRank() {
        return fastDB.isRequireAuth() ? db.getRank() : spoofedRank != null ? spoofedRank : getRank();
    }

    @Override
    public Rank getSpoofedRankDirect() {
        return spoofedRank;
    }

    @Override
    public void setSpoofedRank(Rank spoofedRank) {
        this.spoofedRank = spoofedRank;
    }

    @Override
    public void setSpectator(boolean spectator) {
        if (spectator) {
            getPlayer().setAllowFlight(true);
            getPlayer().setFlying(true);
            getPlayer().setGameMode(GameMode.CREATIVE);
            getPlayer().getInventory().clear();
            getPlayer().spigot().setCollidesWithEntities(false);
            setVanish(true, true);
        } else if (this.spectator) {
            getPlayer().setGameMode(GameMode.SURVIVAL);
            getPlayer().spigot().setCollidesWithEntities(true);
            setVanish(false);
        }
        this.spectator = spectator;
    }

    @Override
    public boolean isShowSpectators() {
        return showSpectators;
    }

    @Override
    public void setShowSpectators(boolean showSpectators) {
        this.showSpectators = showSpectators;
    }

    @Override
    public void setVanish(boolean vanish) {
        setVanish(vanish, true);
    }

    @Override
    public void setVanish(boolean vanish, boolean tablist) {
        this.vanish = vanish;
        PvPDojo.schedule(() -> {
            if (vanish) {
                getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1));
            } else {
                getPlayer().removePotionEffect(PotionEffectType.INVISIBILITY);
            }
        }).nextTick();
        vanishTablist = vanish && tablist;
        updateForEveryOne();
    }

    @Override
    public void removeUserFor(User td) {
        Team spectatorTeam = td.getPlayer().getScoreboard().getTeam("Spectators");

        if (spectatorTeam == null) {
            spectatorTeam = td.getPlayer().getScoreboard().registerNewTeam("Spectators");
            spectatorTeam.setCanSeeFriendlyInvisibles(true);
        }

        if (spectatorTeam.hasEntry(td.getName())) {
            if (spectatorTeam.getEntries().stream()
                    .map(Bukkit::getPlayer)
                    .filter(other -> other != null && (other == td.getPlayer() || other.isTrackedBy(td.getPlayer())))
                    .map(User::getUser)
                    .noneMatch(User::isVanish)) {
                spectatorTeam.removeEntry(td.getName());
                td.updateUserFor(td);
            }
        }
    }

    @Override
    public void updateUserFor(User td) {
        updateUserFor(td, false);
    }

    @Override
    public void updateUserFor(User td, boolean tracker) {
        if (vanish) {
            if (!td.getRank().inheritsRank(getRank()) || !td.isShowSpectators() || (!td.getRank().inheritsRank(Rank.TRIAL_MODERATOR) && !td.isSpectator())) {
                td.getPlayer().hidePlayer(getPlayer(), vanishTablist);
                if (td != this) { // Still handle ghost spectator if player is self updated and has showSpectators disabled
                    return;
                }
            } else {
                td.getPlayer().showPlayer(getPlayer());
            }
        } else {
            td.getPlayer().showPlayer(getPlayer());
        }

        Scoreboard scoreboard = td.getPlayer().getScoreboard();
        Team team = scoreboard.getTeam(getPlayer().getName());

        if (team == null && !tracker && (!getPlayer().isTrackedBy(td.getPlayer()) && td != this)) {
            return;
        } else if (team == null) {
            team = scoreboard.registerNewTeam(getPlayer().getName());
        }

        Team spectatorTeam = scoreboard.getTeam("Spectators");

        if (spectatorTeam == null) {
            spectatorTeam = scoreboard.registerNewTeam("Spectators");
            spectatorTeam.setCanSeeFriendlyInvisibles(true);
        }

        // Handle spectators
        if (vanish) {
            spectatorTeam.addEntry(td.getName());
            spectatorTeam.addEntry(getName());
            return;
        } else if (spectatorTeam.getEntries().stream()
                .map(Bukkit::getPlayer)
                .filter(other -> other != null && (other == td.getPlayer() || other.isTrackedBy(td.getPlayer())))
                .map(User::getUser).noneMatch(User::isVanish)) {
            spectatorTeam.removeEntry(td.getName());
            if (td != this) {
                td.updateUserFor(td);
            }
        }

        String prefix = generatePrefix(new StringBuilder(), td).toString();
        if (!team.getPrefix().equals(prefix)) {
            team.setPrefix(prefix);
        }

        String suffix = generateSuffix(new StringBuilder(), td).toString();
        if (!team.getSuffix().equals(suffix)) {
            team.setSuffix(suffix);
        }

        if (!team.getEntries().contains(getPlayer().getNick())) {
            team.addEntry(getPlayer().getNick());
        }
    }

    @Override
    public StringBuilder generatePrefix(StringBuilder prefix, User td) {
        if (getTeam() != null) {
            prefix.append(getTeam().getColor()).append("[").append(StringUtils.getEnumName(getTeam().getColor())).append("] ");
        }
        return prefix.append(getSpoofedRank().getPrefix());
    }

    @Override
    public StringBuilder generateSuffix(StringBuilder suffix, User td) {

        if (getPlayer().isNicked() && td.getPlayer().hasPermission("Staff") && td.getRank().inheritsRank(getRank())) {
            if (suffix.length() > 0) {
                suffix.setLength(0);
            }
            suffix.append(CC.GRAY).append(" [").append(player.getName().length() > 11 ? player.getName().substring(0, 11) : player.getName()).append("]");
        }
        return suffix;
    }

    @Override
    public void updateForEveryOne() {
        Bukkit.getOnlinePlayers().stream().map(User::getUser).forEach(this::updateUserFor);
    }

    @Override
    public void setLoggedOut(boolean loggedOut) {
        this.loggedOut = loggedOut;
    }

    @Override
    public void updateTablist() {
        String tabname = BukkitUtil.getTabName(getSpoofedRank().getPrefix(), getPlayer().getNick());
        getPlayer().setPlayerListName(tabname);
        GameProfile nickProfile = NMSUtils.getCraftPlayer(getPlayer()).nickGameProfile;
        PvPDojo.schedule(() -> Redis.get().publish(BungeeSync.TABLIST, getPlayer().getUniqueId() + "|" + getSpoofedRank().getPrefix() + getPlayer().getNick()
                + (nickProfile != null ? "|" + nickProfile.getId().toString() : ""))).createAsyncTask();
    }

    @Override
    public InventoryMenu getMenu() {
        return menu;
    }

    @Override
    public void setMenu(InventoryMenu menu) {
        this.menu = menu;
    }

    @Override
    public void setAdminMode(boolean adminMode) {
        if (adminMode != this.adminMode) {
            if (adminMode) {
                AdminCommand.enableAdminMode(getPlayer());
            } else {
                AdminCommand.disableAdminMode(getPlayer());
            }
        }
        this.adminMode = adminMode;
        getRank().refreshPermissions(this);
    }

    @Override
    public boolean canBuild() {
        return build;
    }

    @Override
    public void setSession(GameSession session) {
        this.session = session;
    }

    @Override
    public String getName() {
        return getPlayer().getNick();
    }

    @Override
    public Set<ProtectedRegion> getApplicableRegions() {
        return applicableRegions;
    }

    @Override
    public void setApplicableRegions(Set<ProtectedRegion> applicableRegions) {
        this.applicableRegions = applicableRegions;
    }

    @Override
    public Location getLastValidLocation() {
        return lastValidLocation;
    }

    @Override
    public void setLastValidLocation(Location lastValidLocation) {
        this.lastValidLocation = lastValidLocation;
    }

    @Override
    public boolean canPvP() {
        RegionQuery query = WGBukkit.getPlugin().getRegionContainer().createQuery();
        return query.testState(getLastValidLocation() != null ? getLastValidLocation() : getPlayer().getLocation(), getPlayer(), DefaultFlag.PVP);
    }

    @Override
    public Clan getClan() {
        return clan;
    }

    @Override
    public void setClan(Clan clan) {
        if (clan != null) {
            PvPDojo.schedule(() -> Achievement.GANG_MEMBER.trigger(getPlayer())).nextTick();
        }
        this.clan = clan;
    }

    @Override
    public List<Player> getPlayers() {
        return getPlayer() != null ? Collections.singletonList(getPlayer()) : Collections.emptyList();
    }

    @Override
    public int getElo() {
        return db.getElo();
    }

    @Override
    public void addElo(int elo) {
        db.incrementElo(elo);
    }

    @Override
    public UserSettings getSettings() {
        return settings;
    }

    @Override
    public void setSettings(UserSettings settings) {
        if (settings != null) {
            this.settings = settings;
        }
    }

    @Override
    public FastDB getFastDB() {
        return fastDB;
    }

    @Override
    public void setFastDB(FastDB fastDB) {
        if (fastDB != null) {
            this.fastDB = fastDB;
        }
    }

    public CooldownSettings getCooldownSettings() {
        return cooldownSettings;
    }

    public void setCooldownSettings(CooldownSettings cooldownSettings) {
        if (cooldownSettings != null) {
            this.cooldownSettings = cooldownSettings;
        }
    }

    @Override
    public ExpireHashMap<UUID, Request> getRequests() {
        return requests;
    }

    @Override
    public String makeRequest(UUID requester, Request request) {
        if (!getSettings().getInviteMode().test(this, User.getUser(requester))) {
            return PvPDojo.LANG.createDynamicKey(MessageKeys.NOT_ACCEPTING_REQUEST);
        }
        boolean sameType = requests.containsKey(requester) ? requests.get(requester).equals(request) : requester.equals(uuid);
        if (!sameType) {
            requests.put(requester, request, request.getExpirationSeconds(), TimeUnit.SECONDS);
        } else {
            return PvPDojo.LANG.createDynamicKey(MessageKeys.REQUEST_ALREADY_PENDING);
        }
        return null;
    }

    @Override
    public boolean isAutoRespawn() {
        return autoRespawn;
    }

    @Override
    public void setAutoRespawn(boolean autoRespawn) {
        this.autoRespawn = autoRespawn;
    }

    @Override
    public InputGUI getInputGui() {
        return inputGui;
    }

    @Override
    public void setInputGui(InputGUI inputGui) {
        this.inputGui = inputGui;
    }

    @Override
    public HotbarGUI getHotbarGui() {
        return hotbarGui;
    }

    @Override
    public void setHotbarGui(HotbarGUI hotbarGui) {
        if (hotbarGui != null) {
            getPlayer().getInventory().setContents(hotbarGui.getItems(getPlayer()));
            getPlayer().updateInventory();
        }
        this.hotbarGui = hotbarGui;
    }

    @Override
    public int getNoMoveSeconds() {
        return noMoveSeconds;
    }

    @Override
    public void addNoMoveSecond() {
        noMoveSeconds++;
    }

    @Override
    public void resetNoMoveSeconds() {
        noMoveSeconds = 0;
    }

    @Override
    public boolean isAFK() {
        return noMoveSeconds > 120;
    }

    @Override
    public ChatChannel getChatChannel() {
        return chatChannel;
    }

    @Override
    public void setChatChannel(ChatChannel chatChannel) {
        if (this.chatChannel != chatChannel) {
            sendOverlay(MessageKeys.CHAT_CHANNEL, "{channel}", StringUtils.getEnumName(chatChannel));
        }
        this.chatChannel = chatChannel;
    }

    @Override
    public void detachHologram() {
        if (attachedHologram != null) {
            Bukkit.getScheduler().cancelTask(hologramTaskId);
            attachedHologram.destroy();
            attachedHologram = null;
            hologramTaskId = -1;
        }
        ModUser modUser = ModUser.getUser(getPlayer());
        if (modUser != null) {
            modUser.getStatsManager().resetLargeText();
        }
    }

    @Override
    public void attachHologram(Hologram hologram, int distance, int despawn) {
        if (attachedHologram != null) {
            detachHologram();
        }
        this.attachedHologram = hologram;

        Location initloc = getPlayer().getEyeLocation();
        initloc.add(initloc.getDirection().multiply(distance));
        hologram.show(initloc, getPlayer(), despawn);

        hologramTaskId = PvPDojo.schedule(() -> {
            if (getPlayer().isOnline() && hologram.isShowing()) {
                Location loc = getPlayer().getEyeLocation();
                loc.add(loc.getDirection().multiply(distance));
                hologram.move(loc, getPlayer());
            } else {
                detachHologram();
            }
        }).createTimer(2, -1);

    }

    @Override
    public Stats getStats() {
        return stats;
    }

    @Override
    public boolean useStats() {
        return useStats;
    }

    @Override
    public void setUseStats(boolean useStats) {
        this.useStats = useStats;
    }

    @Override
    public Location getLastTrackedObjectsUpdate() {
        return lastTrackedObjectsUpdate;
    }

    @Override
    public void setLastTrackedObjectsUpdate(Location lastTrackedObjectsUpdate) {
        this.lastTrackedObjectsUpdate = lastTrackedObjectsUpdate;
    }

    @Override
    public UUID getUniqueId() {
        return getUUID();
    }

    @Override
    public TeamArenaEntity getTeam() {
        return team;
    }

    @Override
    public void setTeam(TeamArenaEntity team) {
        this.team = team;
    }

    @Override
    public Party getParty() {
        return party;
    }

    @Override
    public void setParty(Party party) {
        this.party = party;
    }

    @Override
    public void cancelNextFall() {
        cancelNextFall = true;
    }

    @Override
    public void handleWarp(Warp warp) {
        ServerSwitchSettings settings = new ServerSwitchSettings(this);
        if (warp.isExtraTeleport()) {
            settings.warp(warp);
        }
        warp.getServer().connect(getPlayer(), settings);
    }

    @Override
    public boolean isDeathDrops() {
        return deathDrops;
    }

    @Override
    public void setDeathDrops(boolean deathDrops) {
        this.deathDrops = deathDrops;
    }

    @Override
    public EnumSet<Material> getBypassBlockedInteraction() {
        return EnumSet.noneOf(Material.class);
    }

    @Override
    public void sendOverlay(MessageKeyProvider key, String... replacements) {
        String message = PvPDojo.LANG.formatMessage(this, key, replacements);
        ModUser modUser = ModUser.getUser(getPlayer());
        if (modUser != null) {
            modUser.sendOverlay(message.replaceFirst(Pattern.quote(PvPDojo.PREFIX), CC.GRAY.toString()).replaceFirst(PvPDojo.WARNING, CC.GRAY.toString()));
        } else {
            sendMessage(message);
        }
    }

    @Override
    public boolean isCancelMove() {
        return System.currentTimeMillis() < cancelMoveUntil;
    }

    @Override
    public void setCancelMove(int seconds) {
        long cancelMoveUntil = System.currentTimeMillis() + seconds * 1000;
        if (cancelMoveUntil > this.cancelMoveUntil) {
            this.cancelMoveUntil = cancelMoveUntil;
        }
    }

    @Override
    public void resumeMove() {
        this.cancelMoveUntil = 0;
    }

    @Override
    public void sendMessage(MessageKeyProvider key, String... replacements) {
        sendMessage(LocaleMessage.of(key, replacements));
    }

    // When translating messages we have to wait for clients settings packet containing their locale
    // We queue messages until packet is recieved
    private final Queue<LocaleMessage> messageQueue = new LinkedList<>();
    private volatile boolean localeReceived;

    public void setLocaleReceived() {
        if (!localeReceived) {
            localeReceived = true;
            while (!messageQueue.isEmpty()) {
                LocaleMessage message = messageQueue.poll();
                sendMessage(message);
            }
        }
    }

    @Override
    public void sendMessage(LocaleMessage message) {
        if (localeReceived) {
            PvPDojo.LANG.sendMessage(this, message);
        } else {
            synchronized (messageQueue) {
                messageQueue.add(message);
            }
        }
    }

    @Override
    public Sidebar getSidebar() {
        return sidebar;
    }

    private boolean kicked;

    @Override
    public void kick(String reason) {
        if (!isLoggedOut() && !kicked) {
            kicked = true;
            PvPDojo.schedule(() -> {
                new ServerSwitchSettings(this).save(getUUID());
                PvPDojo.schedule(() -> getPlayer().kickPlayer(reason)).sync();
            }).createNonMainThreadTask();
        }
    }

    @Override
    public boolean isInCombat(int ignoreAmount) {
        return canPvP() && NMSUtils.get(getPlayer()).lastDamageByPlayerTime > ignoreAmount && !getPlayer().isDead() && getPlayer().getGameMode() != GameMode.CREATIVE;
    }

    private boolean fakeAbilities;

    @Override
    public boolean hasFakeAbilities() {
        return fakeAbilities;
    }

    @Override
    public void setFakeAbilities(boolean fakeAbilities) {
        this.fakeAbilities = fakeAbilities;
    }
}
