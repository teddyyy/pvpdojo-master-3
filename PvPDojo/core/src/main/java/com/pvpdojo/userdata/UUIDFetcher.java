/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.stream.Stream;

import com.pvpdojo.util.Log;

import co.aikar.commands.ACFBukkitUtil;
import net.minecraft.util.com.google.gson.Gson;
import net.minecraft.util.com.google.gson.GsonBuilder;
import net.minecraft.util.com.mojang.util.UUIDTypeAdapter;

public class UUIDFetcher {

    /**
     * Date when name changes were introduced
     * 
     * @see UUIDFetcher#getUUIDAt(String, long)
     */
    public static final long FEBRUARY_2015 = 1422748800000L;

    private static Gson gson = new GsonBuilder().registerTypeAdapter(UUID.class, new UUIDTypeAdapter()).create();

    private static final String UUID_URL = "https://api.mojang.com/users/profiles/minecraft/%s?at=%d";
    private static final String NAME_URL = "https://api.mojang.com/user/profiles/%s/names";

    private static ExecutorService pool = Executors.newCachedThreadPool();

    private String name;
    private UUID id;

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    /**
     * Fetches the uuid asynchronously and passes it to the consumer
     * 
     * @param name The name
     * @param action Do what you want to do with the uuid her
     */
    public static void getUUID(String name, Consumer<UUID> action) {
        pool.execute(() -> action.accept(getUUID(name)));
    }

    /**
     * Fetches the uuid synchronously and returns it
     * 
     * @param name The name
     * @return The uuid
     */
    public static UUID getUUID(String name) {
        return getUUIDAt(name, System.currentTimeMillis());
    }

    /**
     * Fetches the uuid synchronously for a specified name and time and passes the result to the consumer
     * 
     * @param name The name
     * @param timestamp Time when the player had this name in milliseconds
     * @param action Do what you want to do with the uuid her
     */
    public static void getUUIDAt(String name, long timestamp, Consumer<UUID> action) {
        pool.execute(() -> action.accept(getUUIDAt(name, timestamp)));
    }

    /**
     * Fetches the uuid synchronously for a specified name and time
     * 
     * @param name The name
     * @param timestamp Time when the player had this name in milliseconds
     * @see UUIDFetcher#FEBRUARY_2015
     */
    public static UUID getUUIDAt(String name, long timestamp) {
        name = name.toLowerCase();

        if (!ACFBukkitUtil.isValidName(name)) {
            Log.warn("Skipped Invalid Name Lookup");
            return null;
        }

        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(String.format(UUID_URL, name, timestamp / 1000)).openConnection();
            connection.setReadTimeout(5000);
            UUIDFetcher data = gson.fromJson(new BufferedReader(new InputStreamReader(connection.getInputStream())), UUIDFetcher.class);
            if (data == null) {
                return null;
            }
            return data.id;
        } catch (Exception e) {
            Log.exception("Fetching uuid from Mojang " + name + " " + timestamp, e);
        }

        return null;
    }

    /**
     * Fetches the name asynchronously and passes it to the consumer
     * 
     * @param uuid The uuid
     * @param action Do what you want to do with the name her
     */
    public static void getName(UUID uuid, Consumer<String[]> action) {
        pool.execute(() -> action.accept(getNameHistory(uuid)));
    }

    /**
     * Fetches the name synchronously and returns it
     * 
     * @param uuid The uuid
     * @return The name
     */
    public static String[] getNameHistory(UUID uuid) {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(String.format(NAME_URL, UUIDTypeAdapter.fromUUID(uuid))).openConnection();
            connection.setReadTimeout(5000);
            UUIDFetcher[] nameHistory = gson.fromJson(new BufferedReader(new InputStreamReader(connection.getInputStream())), UUIDFetcher[].class);
            return Stream.of(nameHistory).map(UUIDFetcher::getName).toArray(String[]::new);
        } catch (Exception e) {
            Log.exception("Fetching Name History from Mojang", e);
        }

        return null;
    }
}
