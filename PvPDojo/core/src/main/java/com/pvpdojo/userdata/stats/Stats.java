/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata.stats;

import java.sql.SQLException;

public interface Stats {

    int getKills();

    int getDeaths();

    int getKillstreak();

    void resetKillstreak();

    int incrKillstreak();

    void loadKillstreak(int loadedKillstreak);

    int getMaxKillstreak();

    long getPlayTime();

    long getCurrentPlayTime();

    void addKill();

    void addDeath();

    void applyMaxKillstreak(int currentKillstreak);

    void addPlayTime(long playTime);

    void load() throws SQLException;

    boolean isLoadable();

}
