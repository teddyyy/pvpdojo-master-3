/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionType;

import com.pvpdojo.util.EnumOrderAccess;
import com.pvpdojo.util.bukkit.ItemBuilder.PotionBuilder;
import com.pvpdojo.util.StringUtils;

public enum PlayStyle implements EnumOrderAccess<PlayStyle> {

    SOUP(new ItemStack(Material.MUSHROOM_SOUP)),
    SURVIVALIST(new ItemStack(Material.FISHING_ROD)),
    POTTER(new PotionBuilder(PotionType.INSTANT_HEAL).splash().level(2).build());

    private ItemStack icon;

    PlayStyle(ItemStack icon) {
        this.icon = icon;
    }

    public ItemStack getIcon() {
        return icon.clone();
    }

    public int getId() {
        return this.ordinal();
    }

    public String getName() {
        return StringUtils.getEnumName(this);
    }

    public static PlayStyle getPlayStyleById(int id) {
        return values()[id];
    }

}
