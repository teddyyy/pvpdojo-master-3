/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import com.google.common.base.Charsets;
import com.google.common.io.ByteStreams;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.util.EnumOrderAccess;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.bukkit.CC;

public enum Rank implements EnumOrderAccess<Rank> {
    OWNER("Owner"),
    SERVER_ADMINISTRATOR("Administrator"),
    DEVELOPER("Developer"),
    MODERATOR_PLUS("Mod+"),
    MODERATOR("Mod"),
    CHAT_MODERATOR("Chat-Mod"),
    TRIAL_MODERATOR("Trial-Mod"),
    HELPER("Helper"),
    BUILDER("Builder"),
    FAMOUS("Famous"),
    YOUTUBER("Youtuber"),
    GRANDMASTER("Grandmaster"),
    SENSEI("Sensei"),
    FIRST_DAN("FirstDan"),
    BLACKBELT("BlackBelt"),
    BLUEBELT("BlueBelt"),
    GREENBELT("GreenBelt"),
    DEFAULT("Default");

    public boolean inheritsRank(Rank rank) {
        return rank != null && this.ordinal() <= rank.ordinal();
    }

    Rank(String name) {
        this.name = name;
        this.permissions = PermFile.permfile.getStringList(name());
    }

    String name;

    public String getName() {
        return name;
    }

    public static Rank getRankByPrefix(String prefix) {
        for (Rank rank : values()) {
            if (rank.getPrefix().equals(prefix) && rank != CHAT_MODERATOR) {
                return rank;
            }
        }
        return Rank.DEFAULT;
    }

    public String getPrefix() {
        switch (this) {
            case OWNER:
                return CC.DARK_RED + "" + CC.BOLD;
            case SERVER_ADMINISTRATOR:
                return "" + CC.RED + "" + CC.BOLD;
            case DEVELOPER:
                return "" + CC.DARK_AQUA + CC.BOLD;
            case MODERATOR_PLUS:
                return CC.DARK_PURPLE + "" + CC.ITALIC;
            case MODERATOR:
                return "" + CC.DARK_PURPLE;
            case TRIAL_MODERATOR:
                return "" + CC.LIGHT_PURPLE;
            case HELPER:
                return "" + CC.DARK_AQUA;
            case BUILDER:
                return "" + CC.YELLOW;
            case FAMOUS:
                return CC.GOLD + "" + CC.ITALIC;
            case YOUTUBER:
                return CC.GOLD + "";
            case CHAT_MODERATOR:
                return "" + CC.AQUA + CC.ITALIC;
            case GRANDMASTER:
                return CC.AQUA + "";
            case SENSEI:
                return "" + CC.GREEN;
            case FIRST_DAN:
                return "" + CC.DARK_GRAY + CC.BOLD;
            case BLACKBELT:
                return "" + CC.DARK_GRAY;
            case BLUEBELT:
                return "" + CC.BLUE;
            case GREENBELT:
                return "" + CC.DARK_GREEN;
            case DEFAULT:
                return "" + CC.WHITE;

            default:
                break;
        }
        return "" + CC.WHITE;
    }

    public String getMessageColor() {
        switch (this) {
            case OWNER:
            case SERVER_ADMINISTRATOR:
            case DEVELOPER:
                return "" + CC.RED;
            case MODERATOR_PLUS:
            case MODERATOR:
            case TRIAL_MODERATOR:
            case HELPER:
                return "" + CC.LIGHT_PURPLE;
            case CHAT_MODERATOR:
            case YOUTUBER:
            case GRANDMASTER:
            case SENSEI:
            case FIRST_DAN:
            case BLACKBELT:
            case BLUEBELT:
            case GREENBELT:
            case DEFAULT:
            default:
                return "" + CC.GRAY;
        }
    }

    public int getKitLimit() {
        switch (this) {
            case OWNER:
            case SERVER_ADMINISTRATOR:
            case DEVELOPER:
            case MODERATOR_PLUS:
            case MODERATOR:
            case TRIAL_MODERATOR:
            case HELPER:
            case CHAT_MODERATOR:
            case BUILDER:
            case YOUTUBER:
            case GRANDMASTER:
            case SENSEI:
            case FIRST_DAN:
                return 6;
            case BLACKBELT:
            case BLUEBELT:
                return 3;
            case GREENBELT:
                return 2;
            case DEFAULT:
            default:
                return 1;
        }
    }

    private List<String> permissions;

    public List<String> getPermissions() {
        return permissions;
    }

    private void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }

    public void refreshPermissions(User user) {
        user.getPlayer().getEffectivePermissions().forEach(permissionAttachmentInfo -> {
            if (permissionAttachmentInfo.getAttachment() != null) {
                permissionAttachmentInfo.getAttachment().remove();
            }
        });
        getPermissions().forEach(perm -> user.getPlayer().addAttachment(PvPDojo.get(), perm, true));
        if (!user.isAdminMode()) {
            user.getPlayer().addAttachment(PvPDojo.get(), "worldedit.navigation.jumpto.tool", false);
            user.getPlayer().addAttachment(PvPDojo.get(), "worldedit.navigation.thru.tool", false);
        }
    }

    static class PermFile {

        private static YamlConfiguration permfile;

        static {
            permfile = YamlConfiguration.loadConfiguration(new File(PvPDojo.get().getDataFolder(), "permissions.yml"));
            YamlConfiguration defaults = new YamlConfiguration();
            InputStream stream = PvPDojo.get().getResource("permissions.yml");

            byte[] contents = null;
            try {
                contents = ByteStreams.toByteArray(stream);
            } catch (IOException e) {
                Log.exception("perms", e);
            }

            final String text = new String(contents, Charset.defaultCharset());
            if (!text.equals(new String(contents, Charsets.UTF_8))) {
                try {
                    throw new IOException("Error encoding permissions.yml");
                } catch (IOException e) {
                    Log.exception("perms", e);
                }
            }

            try {
                defaults.loadFromString(text);
            } catch (InvalidConfigurationException e) {
                Log.exception("perms", e);
            }
            permfile.options().copyDefaults(true);
            permfile.setDefaults(defaults);
            try {
                permfile.save(new File(PvPDojo.get().getDataFolder(), "permissions.yml"));
            } catch (IOException e) {
                Log.exception("perms", e);
            }
        }
    }

}
