/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata;

import static com.pvpdojo.mysql.Tables.ABILITIES;

import java.lang.reflect.Type;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.libs.com.google.gson.reflect.TypeToken;
import org.bukkit.inventory.ItemStack;
import org.spigotmc.SneakyThrow;

import com.pvpdojo.DBCommon;
import com.pvpdojo.DeveloperSettings;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilityinfo.AbilityLog;
import com.pvpdojo.abilityinfo.AbilityRarity;
import com.pvpdojo.achievement.Achievement;
import com.pvpdojo.achievement.AchievementManager;
import com.pvpdojo.bukkit.events.DataLoadedEvent;
import com.pvpdojo.bukkit.events.DataUpdateEvent;
import com.pvpdojo.clan.Clan;
import com.pvpdojo.clan.ClanList;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.dataloader.KitLoader;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.mysql.SQL;
import com.pvpdojo.mysql.SQLBuilder;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.idb.DB;

public class PersistentData {

    private UUID uuid;
    private List<KitData> kits = new ArrayList<>();
    private Map<UUID, AbilityData> abilities = new HashMap<>();
    private EnumSet<Achievement> achievements = EnumSet.allOf(Achievement.class); // Assume every achievement done if not loaded
    private int money;
    private int elo;
    private int kills, deaths;
    private int maxKillstreak = Integer.MAX_VALUE;
    private int achievementPoints;
    private int additionalKitSlots;
    private long playTime;
    private String clan;
    private Rank rank = Rank.DEFAULT;

    public PersistentData(UUID uuid) {
        this.uuid = uuid;
    }

    public UUID getUUID() {
        return uuid;
    }

    public int countAbilities() {
        return (int) getAbilities().stream().filter(data -> data.getId() >= 0).count();
    }

    public Collection<AbilityData> getAbilities() {
        return abilities.values();
    }

    public AbilityData getAbility(UUID uuid) {
        return abilities.get(uuid);
    }

    public AbilityData getAbility(int id) {
        for (AbilityData data : abilities.values()) {
            if (id == data.getId()) {
                return data;
            }
        }

        throw new NullPointerException("Null Ability Id : " + id);
    }

    public void updateAbility(UUID abilityUUID, int abilityId) {
        AbilityData data = this.getAbility(abilityUUID);
        data.setAbilityId(abilityId);

        PvPDojo.schedule(() -> {
            AbilityRarity rarity = AbilityLoader.getAbilityData(abilityId).getRarity();
            if (rarity == AbilityRarity.GOLD) {
                AchievementManager.inst().trigger(Achievement.EL_DORADO, Bukkit.getPlayer(uuid));
            } else if (rarity == AbilityRarity.LEGENDARY) {
                AchievementManager.inst().trigger(Achievement.LOTTERY, Bukkit.getPlayer(uuid));
            }
        }).createTask(3 * 20);
        PvPDojo.schedule(() -> {
            String update = "ability_id = ?";
            int rows = SQL.updateMultiple(ABILITIES, update, "uuid = ? AND ability_uuid = ?", abilityId, getUUID().toString(), abilityUUID.toString());
            if (rows != 0) {
                new AbilityLog(abilityUUID, getUUID(), getUUID(), "Unboxed").save();
            } else {
                Log.warn("Unboxing failed user:" + getUUID() + " ability:" + abilityUUID + " abilityid: " + abilityId);
            }
        }).createNonMainThreadTask();
    }

    public static void updateAbilityOwnership(UUID newOwner, UUID abilityUUID, String reason) {
        PvPDojo.schedule(() -> {
            try {
                UUID previous = UUID.fromString(SQL.getFirstColumn(Tables.ABILITIES, "uuid", "ability_uuid = ?", abilityUUID.toString()));
                if (SQL.updateString(Tables.ABILITIES, "uuid", "ability_uuid = ?", newOwner != null ? newOwner.toString() : null, abilityUUID.toString()) != 0) {
                    new AbilityLog(abilityUUID, previous, newOwner, reason).save();
                }
            } catch (SQLException e) {
                defaultExceptionHandlerStatic(e);
            }
        }).createNonMainThreadTask();
    }

    public UUID addCollection(int abilityId) {
        return addCollection(UUID.randomUUID(), abilityId);
    }

    public UUID addCollection(UUID abilityUUID, int ability_id) {
        AbilityData data = new AbilityData(getUUID(), abilityUUID, ability_id);
        this.abilities.put(data.getAbilityUUID(), data);
        PvPDojo.schedule(() -> {
            try {
                SQL.insert(Tables.ABILITIES, "'" + getUUID() + "', 0, '" + abilityUUID + "', " + ability_id + ", 1");
            } catch (SQLException e) {
                Log.exception("Adding collection", e);
            }
        }).createNonMainThreadTask();
        return abilityUUID;
    }

    public boolean hasAbility(int ability_id) {
        for (AbilityData data : abilities.values()) {
            if (data.getId() == ability_id) {
                return true;
            }
        }
        return false;
    }

    public void hasAbility(int ability_id, BiConsumer<Boolean, Throwable> call) {
        PvPDojo.schedule(() -> {
            try (SQLBuilder builder = new SQLBuilder()) {
                builder.setStatement("SELECT * FROM `" + "abilities" + "` WHERE " + "uuid = '" + getUUID() + "' AND ability_id = " + ability_id + ";");
                builder.executeQuery();
                boolean has = builder.next();
                if (call != null) {
                    call.accept(has, null);
                }
            } catch (SQLException e) {
                SQL.handleException(e, call);
            }
        }).createAsyncTask();
    }

    public AbilityData readAbility(UUID ability_uuid) throws SQLException {
        try (SQLBuilder builder = new SQLBuilder()) {
            builder.setStatement("SELECT * FROM `" + "abilities" + "` WHERE " + "uuid = '" + getUUID() + "' AND ability_uuid = '" + ability_uuid + "';");
            builder.executeQuery();

            if (builder.next()) {
                return getAbilityData(builder);
            } else {
                throw new SQLException("Ability not found " + ability_uuid);
            }
        }
    }

    public AbilityData getAbilityData(SQLBuilder sql) {
        UUID ability_uuid = UUID.fromString(sql.getString("ability_uuid"));
        int ability_id = sql.getInteger("ability_id");

        return new AbilityData(getUUID(), ability_uuid, ability_id);
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public int getMoney() {
        return money;
    }

    public void incrementInteger(String key, int amount) {
        PvPDojo.schedule(() -> SQL.updateMultiple(Tables.USERS, key + " = " + key + " + " + amount, "uuid = ?", getUUID().toString())).createNonMainThreadTask();
    }

    public int readInteger(String key) {
        try {
            return SQL.getInteger(Tables.USERS, key, "uuid = ?", getUUID().toString()).orElseThrow(SQLException::new);
        } catch (SQLException e) {
            Log.exception("Reading integer " + key, e);
        }
        return -1;
    }

    public void incrementMoney(int amount) {
        money = money + amount;
        incrementInteger("money", amount);
    }

    public void readMoney() {
        PvPDojo.schedule(() -> money = readInteger("money")).createNonMainThreadTask();
    }

    public int getKills() {
        return kills;
    }

    public void incrementKill() {
        kills++;
        incrementInteger("kills", 1);
    }

    public int getDeaths() {
        return deaths;
    }

    public void incrementDeath() {
        deaths++;
        incrementInteger("deaths", 1);
    }

    public int getElo() {
        return elo;
    }

    public void incrementElo(int amount) {
        elo += amount;
        PvPDojo.schedule(() -> {
            try (SQLBuilder sql = new SQLBuilder()) {
                sql.updateRaw(DeveloperSettings.SEASON)
                   .join(Tables.USERS, "userid = id")
                   .set()
                   .incrInteger(DeveloperSettings.SEASON.getName() + ".elo", amount)
                   .where("uuid = ?")
                   .executeUpdate(getUUID().toString());
            } catch (SQLException e) {
                Log.exception(e);
            }
        }).createAsyncTask();
    }

    public int getMaxKillstreak() {
        return maxKillstreak;
    }

    public void setMaxKillstreak(int curKillstreak) {
        if (maxKillstreak < curKillstreak) {
            SQL.updateInteger(Tables.USERS, "maxkillstreak", "uuid = '" + getUUID() + "'", curKillstreak);
        }
    }

    public int getAchievementPoints() {
        return achievementPoints;
    }

    public void incrementAchievementPoints(int amount) {
        achievementPoints += amount;
        incrementInteger("achievement_points", amount);
    }

    public long getPlayTime() {
        return playTime;
    }

    public void addPlayTime(long playTime) {
        this.playTime += playTime;
        incrementInteger("playtime", Math.toIntExact(playTime));
    }

    public String getClan() {
        return clan;
    }

    public void setClan(String clanName) {
        PvPDojo.schedule(() -> SQL.updateString(Tables.USERS, "clan", "uuid = '" + getUUID() + "'", clanName)).createAsyncTask();
    }

    public void pullUserDataSneaky() {
        try {
            pullUserData();
        } catch (SQLException e) {
            SneakyThrow.sneaky(e);
        }
    }

    public void pullUserData() throws SQLException {
        try (SQLBuilder builder = new SQLBuilder()) {

            builder.select(Tables.USERS, "money,rank,rankexpire," + DeveloperSettings.SEASON.getName()
                    + ".elo,clan,kills,deaths,maxkillstreak,achievement_points,kitslots,playtime")
                   .leftJoin(DeveloperSettings.SEASON, "userid = id")
                   .where("uuid = ?")
                   .executeQuery(getUUID().toString());

            if (builder.next()) {
                money = builder.getInteger("money");
                achievementPoints = builder.getInteger("achievement_points");
                if (builder.getInteger("elo") != null) {
                    elo = builder.getInteger("elo");
                }
                clan = builder.getString("clan");
                kills = builder.getInteger("kills");
                deaths = builder.getInteger("deaths");
                maxKillstreak = builder.getInteger("maxkillstreak");
                Rank rank = Rank.valueOf(builder.getString("rank"));
                if (rank != Rank.DEFAULT) {
                    Timestamp expireStamp = builder.getTimestamp("rankexpire");
                    if (expireStamp.getTime() > 0 && expireStamp.toLocalDateTime().isBefore(LocalDateTime.now())) {
                        BungeeSync.sendMessage(uuid, "" + CC.RED + CC.BOLD + "Your rank " + rank.getPrefix() + rank.getName() + CC.RED + CC.BOLD + " expired");
                        SQL.updateString(Tables.USERS, "rank", "uuid = ?", Rank.DEFAULT.name(), uuid.toString());
                        rank = Rank.DEFAULT;
                    }
                }
                setRank(rank);
                additionalKitSlots = builder.getInteger("kitslots");
                playTime = builder.getLong("playtime");
            } else {
                Log.warn("User data is missing for " + getUUID());
            }
        }
    }

    public void readAbilities(BiConsumer<Map<UUID, AbilityData>, Throwable> call) {
        Map<UUID, AbilityData> loadedAbilities = new HashMap<>();
        try (SQLBuilder builder = new SQLBuilder()) {
            builder.setStatement("SELECT * FROM `" + "abilities" + "` WHERE " + "uuid = '" + getUUID() + "'" + ";");
            builder.executeQuery();
            while (builder.next()) {
                AbilityData data = getAbilityData(builder);
                loadedAbilities.put(data.getAbilityUUID(), data);
            }
            abilities = loadedAbilities;
            if (call != null) {
                call.accept(abilities, null);
            }
        } catch (SQLException e) {
            SQL.handleException(e, call);
        }
    }

    public void readAchievements() {
        PvPDojo.schedule(() -> {
            Type type = new TypeToken<EnumSet<Achievement>>() {}.getType();
            try {
                EnumSet<Achievement> achievements = DBCommon.GSON.fromJson(SQL.getString(Tables.USERS, "achievements", "uuid = ?", getUUID().toString()), type);
                if (achievements != null) {
                    this.achievements = achievements;
                } else {
                    this.achievements = EnumSet.noneOf(Achievement.class);
                }
            } catch (SQLException e) {
                Log.exception("Loading achievements for " + getUUID(), e);
            }
        }).createNonMainThreadTask();
    }

    public void saveAchievements(Consumer<Boolean> consumer) {
        PvPDojo.schedule(() -> {
            try {
                DB.executeUpdate("UPDATE " + Tables.USERS + " SET achievements = ? WHERE uuid = ?", DBCommon.GSON.toJson(achievements), getUUID().toString());
                consumer.accept(true);
            } catch (SQLException e) {
                Log.exception("", e);
                consumer.accept(false);
            }
        }).createNonMainThreadTask();
    }

    public EnumSet<Achievement> getAchievements() {
        return achievements;
    }

    public void readKits() {
        PvPDojo.schedule(() -> {
            try (SQLBuilder builder = new SQLBuilder()) {
                List<KitData> loadedKits = new ArrayList<>();

                builder.select(Tables.KITS, "*")
                       .where("uuid = ?")
                       .orderBy("ID", true)
                       .limit(rank.getKitLimit() + additionalKitSlots)
                       .executeQuery(getUUID().toString());

                while (builder.next()) {
                    int champion_id = builder.getInteger("champion");
                    if (!KitLoader.isChampionLoaded(champion_id)) {
                        continue;
                    }
                    int id = builder.getInteger("ID");
                    String name = builder.getString("champion_name");
                    String ability_data = builder.getString("abilities");
                    PlayStyle playstyle = PlayStyle.getPlayStyleById(builder.getInteger("playstyle"));
                    ArrayList<KitAbilityData> abilities = KitAbilityData.getAbilityDataFromString(ability_data);

                    ItemStack[] inventory = DBCommon.GSON.fromJson(builder.getString("inventory"), ItemStack[].class);
                    KitData kit = new KitData(name, id, playstyle, abilities, inventory);

                    loadedKits.add(kit);
                }
                kits = loadedKits;
            } catch (SQLException ex) {
                Log.exception("Reading kits", ex);
            }
        }).createNonMainThreadTask();
    }

    public void updateKit(KitData kit) {
        KitData oldKit = getKit(kit.getId());
        kits.remove(oldKit);
        kits.add(kit);
        kits.sort(Comparator.comparingInt(KitData::getId));
        saveKit(kit);
    }

    private void saveKit(KitData kit) {
        String inventory = DBCommon.GSON.toJson(kit.getInventory());
        String abilityData = KitAbilityData.getStringFromAbilityData(kit.getAbilities());
        String update = "abilities = ?, " + "playstyle = ?, champion_name = ?, inventory = ?";
        PvPDojo.schedule(() -> SQL.updateMultiple(Tables.KITS, update, "ID = " + kit.getId(), abilityData, kit.getPlayStyle().getId(), kit.getName(), inventory)).createAsyncTask();
    }

    public List<KitData> getKits() {
        return this.kits;
    }

    public KitData getKit(int id) {
        for (KitData champion : kits) {
            if (champion.getId() == id) {
                return champion;
            }
        }
        return null;
    }

    public void loadData(boolean refreshOnlineUser) {
        try {
            pullUserData();
        } catch (SQLException e) {
            defaultExceptionHandler(e);
        }
        readAchievements();
        readKits();
        readAbilities((abilities, thrown) -> defaultExceptionHandler(thrown));

        Clan clan = ClanList.getClan(this.clan);

        if (refreshOnlineUser) {
            PvPDojo.schedule(() -> {
                User user = User.getUser(getUUID());

                if (user == null || user.getPlayer() == null || user.isLoggedOut()) {
                    return;
                }

                if (clan != null && clan.getAllMembers().contains(uuid)) {
                    user.setClan(clan);
                }

                // Check if point calculation changed and reset if it exceeds limits
                for (KitData kit : getKits()) {
                    if (kit.getPoints() > DeveloperSettings.MAX_KIT_POINTS) {
                        user.sendMessage(MessageKeys.WARNING, "{text}", CC.DARK_RED + "You kit " + CC.YELLOW + kit.getName() + CC.DARK_RED + " has been reset due to updated point values");
                        kit.reset();
                        saveKit(kit);
                    }
                }

                // If we have a pending auth for this player and he is not a staff member anymore just drop auth
                if (user.getFastDB().isRequireAuth()) {
                    if (!rank.inheritsRank(Rank.TRIAL_MODERATOR) || rank == Rank.CHAT_MODERATOR) {
                        user.getFastDB().setAuthKey(null);
                        user.getFastDB().save(user.getUUID());
                    } else {
                        user.sendMessage(PvPDojo.WARNING + CC.DARK_RED + "Your permissions are locked until auth via Discord");
                    }
                }

                user.getRank().refreshPermissions(user);
                user.updateTablist();

                if (!user.isDataLoaded()) {
                    user.setDataLoaded(true);
                    DataLoadedEvent event = new DataLoadedEvent(user.getPlayer(), this);
                    Bukkit.getPluginManager().callEvent(event);
                }

                Bukkit.getPluginManager().callEvent(new DataUpdateEvent(user.getPlayer(), this));

                Bukkit.getOnlinePlayers().forEach(online -> {
                    User onlineUser = User.getUser(online);
                    onlineUser.updateUserFor(user);
                    user.updateUserFor(onlineUser);
                });

                AchievementManager.inst().trigger(Achievement.WELCOME, user.getPlayer());
            }).sync();
        }
    }

    public void defaultExceptionHandler(Throwable thrown) {
        if (thrown != null) {
            defaultExceptionHandlerStatic(thrown);
            if (Bukkit.getPlayer(uuid) != null) {
                Bukkit.getPlayer(uuid).sendMessage(PvPDojo.WARNING + CC.DARK_RED + "An error occurred within your user data report this issue to a staff member");
            }
        }
    }

    public static void defaultExceptionHandlerStatic(Throwable thrown) {
        if (thrown != null) {
            Log.exception(thrown.getMessage(), thrown);
        }
    }

    public Rank getRank() {
        return rank;
    }
}
