/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata;

import java.util.UUID;

import lombok.Data;

@Data
public class NickData {

    private final UUID uniqueId;
    private final String name;
    private final String skinValue, skinSignature;

}
