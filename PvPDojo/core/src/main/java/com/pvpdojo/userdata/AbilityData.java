/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata;

import java.util.UUID;

import com.pvpdojo.dataloader.AbilityLoader;

public class AbilityData {
    private UUID playerUUID;
    private UUID abilityUUID;
    private int abilityId;
    private AbilityLoader loader;

    public AbilityData(UUID playerUUID, UUID abilityUUID, int abilityId) {
        this.playerUUID = playerUUID;
        this.abilityUUID = abilityUUID;
        this.abilityId = abilityId;
        this.loader = AbilityLoader.getAbilityData(abilityId);
    }

    public UUID getPlayerUUID() {
        return playerUUID;
    }

    public UUID getAbilityUUID() {
        return abilityUUID;
    }

    public int getId() {
        return abilityId;
    }

    public void setAbilityId(int id) {
        this.abilityId = id;
        this.loader = AbilityLoader.getAbilityData(id);
    }

    public AbilityLoader getLoader() {
        return loader;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof AbilityData && ((AbilityData) obj).abilityUUID.equals(abilityUUID);
    }

    @Override
    public int hashCode() {
        return abilityUUID.hashCode();
    }
}