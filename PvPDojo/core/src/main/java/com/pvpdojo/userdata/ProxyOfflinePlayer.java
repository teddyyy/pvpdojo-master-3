/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata;

import java.util.UUID;

import org.bukkit.OfflinePlayer;

public interface ProxyOfflinePlayer extends OfflinePlayer {

    /**
     * {@link ProxyOfflinePlayer} methods are blocking
     *
     * @return True if player is online on network
     */
    boolean isOnlineProxy();

    /**
     * {@link ProxyOfflinePlayer} methods are blocking
     * <p>
     * This returns <code>null</code> if the player cannot be located
     *
     * @return BungeeCord server name where player is online
     */
    String getServerName();

    /**
     * {@link ProxyOfflinePlayer} methods are blocking
     * <p>
     * Checks if {@link #getName()} returns their representive disguised name
     *
     * @return true if nicked, otherwise false
     */
    boolean isNicked();

    /**
     * {@link ProxyOfflinePlayer} methods are blocking
     * <p>
     * This method call ONLY makes sense after retrieving the OfflinePlayer via UUID and specifically need to hide their real name
     * Returns the players disguised name. If the player is not nicked {@link #getName()}
     *
     * @return disguised name
     */
    String getNick();

    /**
     * {@link ProxyOfflinePlayer} methods are blocking
     * <p>
     * Returns the players fake stats account.
     *
     * @return fake account
     */
    UUID getNickFakeStats();

}
