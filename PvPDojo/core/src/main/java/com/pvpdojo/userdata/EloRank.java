/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata;

import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.StringUtils;

public enum EloRank {
    GLOBAL(
            2300,
            CC.GOLD),
    KING(
            2200,
            CC.GOLD),
    FIGHTER(
            2100,
            CC.RED),
    MENTOR(
            2000,
            CC.RED),
    MASTER(
            1900,
            CC.DARK_PURPLE),
    DUELIST(
            1800,
            CC.DARK_PURPLE),
    ROOKIE(
            1700,
            CC.WHITE),
    TRAINEE(
            1600,
            CC.WHITE),
    STARTER(
            1500,
            CC.WHITE),
    NOVICE(
            1300,
            CC.GRAY),
    NOOBIE(
            Integer.MIN_VALUE,
            CC.GRAY);

    private int minElo;
    private CC color;

    EloRank(int minElo, CC color) {
        this.minElo = minElo;
        this.color = color;
    }

    public static EloRank getRank(int elo) {
        for (EloRank rank : values()) {
            if (elo >= rank.minElo) {
                return rank;
            }
        }
        return EloRank.NOOBIE;
    }

    public CC getColor() {
        return color;
    }

    public String getName() {
        if (this == GLOBAL) {
            return "\u262F";
        }
        return StringUtils.getEnumName(this);
    }

}
