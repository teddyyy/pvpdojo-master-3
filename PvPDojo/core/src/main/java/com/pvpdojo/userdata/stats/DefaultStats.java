/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata.stats;

import com.pvpdojo.userdata.PersistentData;

public class DefaultStats implements Stats {

    private PersistentData db;
    private int killstreak;
    private long loginTime = System.currentTimeMillis();

    public DefaultStats(PersistentData db) {
        this.db = db;
    }

    @Override
    public int getKills() {
        return db.getKills();
    }

    @Override
    public int getDeaths() {
        return db.getDeaths();
    }

    @Override
    public int getKillstreak() {
        return killstreak;
    }

    @Override
    public void resetKillstreak() {
        killstreak = 0;
    }

    @Override
    public int incrKillstreak() {
        return ++killstreak;
    }

    @Override
    public void loadKillstreak(int loadedKillstreak) {
        this.killstreak = loadedKillstreak;
    }

    @Override
    public int getMaxKillstreak() {
        return db.getMaxKillstreak();
    }

    @Override
    public long getPlayTime() {
        return db.getPlayTime() + getCurrentPlayTime();
    }

    @Override
    public void addPlayTime(long playTime) {
        db.addPlayTime(playTime);
    }

    @Override
    public void addKill() {
        db.incrementMoney(Math.min(5 + 5 * killstreak, 50));
        db.incrementKill();
    }

    @Override
    public void addDeath() {
        db.incrementDeath();
    }

    @Override
    public long getCurrentPlayTime() {
        return System.currentTimeMillis() - loginTime;
    }

    @Override
    public void applyMaxKillstreak(int currentKillstreak) {
        db.setMaxKillstreak(currentKillstreak);
    }

    @Override
    public void load() {}

    @Override
    public boolean isLoadable() {
        return false;
    }

}
