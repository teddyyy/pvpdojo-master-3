/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.Warp;
import com.pvpdojo.clan.Clan;
import com.pvpdojo.command.lib.DojoCommandManager;
import com.pvpdojo.gui.HotbarGUI;
import com.pvpdojo.gui.InputGUI;
import com.pvpdojo.menu.InventoryMenu;
import com.pvpdojo.party.Party;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.session.entity.RankedArenaEntity;
import com.pvpdojo.settings.CooldownSettings;
import com.pvpdojo.settings.FastDB;
import com.pvpdojo.settings.UserSettings;
import com.pvpdojo.userdata.sidebar.Sidebar;
import com.pvpdojo.userdata.stats.Stats;
import com.pvpdojo.util.ExpireHashMap;
import com.pvpdojo.util.bukkit.Hologram;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import co.aikar.locales.MessageKeyProvider;

public interface User extends RankedArenaEntity, ArenaEntity {

    static User getUser(Player player) {
        return PvPDojo.get().getDojoServer().getUser(player);
    }

    static User getUser(UUID uuid) {
        return PvPDojo.get().getDojoServer().getUser(uuid);
    }

    static Locale getLocale(User user) {
        if (user == null) {
            return PvPDojo.LANG.getDefaultLocale();
        }
        return DojoCommandManager.get().getUUIDLocale(user.getUUID());
    }

    static boolean containsUser(UUID uuid) {
        return PvPDojo.get().getDojoServer().containsUser(uuid);
    }

    static User loginUser(UUID uuid) {
        return PvPDojo.get().getDojoServer().loginUser(uuid);
    }

    static void logoutUser(Player player) {
        PvPDojo.get().getDojoServer().logoutUser(player);
    }

    PersistentData getPersistentData();

    Player getPlayer();

    UUID getUUID();

    boolean isLoggedOut();

    boolean isDataLoaded();

    void setDataLoaded(boolean dataLoaded);

    Rank getRank();

    Rank getSpoofedRank();

    Rank getSpoofedRankDirect();

    void setSpoofedRank(Rank spoofedRank);

    boolean isSpectator();

    void setSpectator(boolean spectator);

    boolean isShowSpectators();

    void setShowSpectators(boolean showSpectators);

    boolean isVanish();

    void setVanish(boolean vanish);

    void setVanish(boolean vanish, boolean tablist);

    void removeUserFor(User td);

    void updateUserFor(User td);

    void updateUserFor(User td, boolean tracker);

    StringBuilder generatePrefix(StringBuilder prefix, User td);

    StringBuilder generateSuffix(StringBuilder suffix, User td);

    void updateForEveryOne();

    void setLoggedOut(boolean loggedOut);

    void updateTablist();

    InventoryMenu getMenu();

    void setMenu(InventoryMenu menu);

    boolean isAdminMode();

    void setAdminMode(boolean adminmode);

    boolean canBuild();

    void setBuild(boolean build);

    Set<ProtectedRegion> getApplicableRegions();

    void setApplicableRegions(Set<ProtectedRegion> applicableRegions);

    Location getLastValidLocation();

    void setLastValidLocation(Location lastValidLocation);

    boolean canPvP();

    Clan getClan();

    void setClan(Clan clan);

    UserSettings getSettings();

    void setSettings(UserSettings settings);

    FastDB getFastDB();

    void setFastDB(FastDB fastDB);

    CooldownSettings getCooldownSettings();

    void setCooldownSettings(CooldownSettings cooldownSettings);

    ExpireHashMap<UUID, Request> getRequests();

    String makeRequest(UUID requester, Request request);

    boolean isAutoRespawn();

    void setAutoRespawn(boolean autoRespawn);

    InputGUI getInputGui();

    void setInputGui(InputGUI inputGui);

    HotbarGUI getHotbarGui();

    void setHotbarGui(HotbarGUI hotbarGui);

    int getNoMoveSeconds();

    void addNoMoveSecond();

    void resetNoMoveSeconds();

    boolean isAFK();

    ChatChannel getChatChannel();

    void setChatChannel(ChatChannel chatChannel);

    boolean isFrozenByStaff();

    void setFrozenByStaff(boolean frozenByStaff);

    void detachHologram();

    void attachHologram(Hologram hologram, int distance, int despawn);

    Stats getStats();

    boolean useStats();

    void setUseStats(boolean useStats);

    Location getLastTrackedObjectsUpdate();

    void setLastTrackedObjectsUpdate(Location lastTrackedObjectsUpdate);

    Party getParty();

    void setParty(Party party);

    boolean isCancelNextFall();

    void cancelNextFall();

    void setCancelNextFall(boolean cancelNextFall);

    void handleWarp(Warp warp);

    boolean isDeathDrops();

    void setDeathDrops(boolean deathDrops);

    EnumSet<Material> getBypassBlockedInteraction();

    void sendOverlay(MessageKeyProvider key, String... replacements);

    boolean isCancelMove();

    void setCancelMove(int seconds);

    void resumeMove();

    void sendMessage(MessageKeyProvider key, String... replacements);

    Sidebar getSidebar();

    void kick(String reason);

    default boolean isInCombat() {
        return isInCombat(0);
    }

    boolean isInCombat(int ignoreAmount);

    boolean hasFakeAbilities();

    void setFakeAbilities(boolean fakeAbilities);

    boolean isCheckNextMove();

    void setCheckNextMove(boolean checkNextMove);

    List<UUID> getCurrentAccountGrid();

    void setCurrentAccountGrid(List<UUID> accountGrid);

}
