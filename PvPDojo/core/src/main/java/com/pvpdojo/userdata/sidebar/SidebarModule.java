/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata.sidebar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;

public class SidebarModule implements SidebarProvider {

    private TIntObjectMap<String> currentContent = new TIntObjectHashMap<>(0);
    private List<String> workingSet = new ArrayList<>(MAX_ROWS);

    public SidebarModule append(String... lines) {
        if (checkSize()) {
            workingSet.addAll(Arrays.asList(lines));
        }
        return this;
    }

    public TIntObjectMap<String> render(Sidebar sidebar) {

        // Fast return
        if (sidebar.getSize() == currentContent.size() && CollectionUtils.isEqualCollection(currentContent.valueCollection(), workingSet)) {
            // Reset for next round
            reset(currentContent);
            return currentContent;
        }

        TIntObjectMap<String> content = new TIntObjectHashMap<>(MAX_ROWS);

        // Reverse as sidebars have their highest index number at top
        Collections.reverse(workingSet);
        for (int i = 1; i <= workingSet.size(); i++) {
            content.put(i, workingSet.get(i - 1));
        }

        // If line count shrinked act accordingly for sidebar
        if (sidebar.getSize() > workingSet.size()) {
            for (int i = sidebar.getSize(); i > workingSet.size(); i--) {
                sidebar.remove(i);
            }
        }

        // Reset working state
        reset(content);

        return currentContent;

    }

    private boolean checkSize() {
        return workingSet.size() < MAX_ROWS;
    }

    private void reset(TIntObjectMap<String> content) {
        currentContent = content;
        workingSet = new ArrayList<>(MAX_ROWS);
    }

}
