/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata;

import com.pvpdojo.util.PunishmentManager.Punishment;

import net.minecraft.util.com.mojang.authlib.GameProfile;

public interface DojoOfflinePlayer extends ProxyOfflinePlayer {

    /**
     * The loaded Mojang profile of that player. Containing skin and corrected name
     * 
     * @return GameProfile of the player
     */
    GameProfile getProfile();

    /**
     * Details about the players current ban. Returns <code>null</code> if player is not banned
     * 
     * @return Punishment object of the current ban
     */
    Punishment getCurrentBan();

    /**
     * Gets the dojo wrapper for persistent storage There is no data loaded by default user {@link PersistentData#loadData()} to load the data into memory
     * 
     * @return dojo's persistent storage wrapper
     */
    PersistentData getPersistentData();

    /**
     * Checks if this player is muted or not
     *
     * @return true if muted, otherwise false
     */
    boolean isMuted();

    /**
     * Details about the players current mute. Returns <code>null</code> if player is not muted
     * 
     * @return Punishment object of the current mute
     */
    Punishment getCurrentMute();

    /**
     * Gets the last date and time that this player changed their clan.
     * <p>
     * If the player has never played before, this will return 0. Otherwise, it will be the amount of milliseconds since midnight, January 1, 1970 UTC.
     *
     * @return Date of last clan change for this player, or 0
     */
    long getLastClanSwitch();

    /**
     * Gets the last IP-Address that was used by this player connecting to the server
     * <p>
     * If the player has never player before, this will return <code>null</code>
     *
     * @return last IP-Address for this player
     */
    String getLastIpAddress();

}
