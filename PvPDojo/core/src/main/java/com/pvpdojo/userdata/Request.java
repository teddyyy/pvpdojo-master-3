/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata;

public abstract class Request {

    public abstract void accept(User acceptor);

    public abstract void deny(User deniedBy);

    public abstract int getExpirationSeconds();

    public boolean isPersistent() {
        return false;
    }

}
