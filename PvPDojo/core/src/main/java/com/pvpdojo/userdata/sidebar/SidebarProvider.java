/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata.sidebar;

import gnu.trove.map.TIntObjectMap;

public interface SidebarProvider {

    int MAX_ROWS = 16;

    TIntObjectMap<String> render(Sidebar sidebar);

}
