/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.userdata;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.inventory.ItemStack;

import com.pvpdojo.abilityinfo.AbilityUpgrade;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.util.bukkit.ItemUtil;

public class KitData {

    public static final int DEFAULT_DATA = -1;
    public static final int EMTPY_DATA = -2;

    private ArrayList<KitAbilityData> abilities;
    private PlayStyle style = PlayStyle.SURVIVALIST;
    private String name;
    private int id;
    private ItemStack[] inventory;

    public KitData(String name, int id) {
        abilities = new ArrayList<>();
        this.name = name;
        this.id = id;
    }

    public KitData(String name, int id, PlayStyle playstyle, ArrayList<KitAbilityData> abilities, ItemStack[] inventory) {
        this.name = name;
        this.id = id;
        style = playstyle;
        this.abilities = abilities;
        this.inventory = inventory;
    }

    public KitData clone() {
        return new KitData(this);
    }

    public KitData(KitData other) {
        this.abilities = KitAbilityData.cloneArray(other.abilities);
        this.style = other.style;
        this.name = other.name;
        this.id = other.id;
        this.inventory = other.inventory != null ? ItemUtil.cloneContents(other.inventory) : null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public PlayStyle getPlayStyle() {
        return style;
    }

    public void setPlayStyle(PlayStyle style) {
        if (this.style != style) {
            inventory = null;
        }
        this.style = style;
    }

    public void removeAbility(int ability_id) {
        if (abilities.remove(this.getAbility(ability_id))) {
            inventory = null;
        }
    }

    public void addAbility(KitAbilityData kitAbilityData) {
        if (abilities.add(kitAbilityData)) {
            inventory = null;
        }
    }

    public void replaceAbility(KitAbilityData updated) {
        if (!containsAbility(updated.getId())) {
            throw new IllegalStateException("Cannot update non existent ability");
        }
        abilities.remove(getAbility(updated.getId()));
        abilities.add(updated);
    }

    public List<KitAbilityData> getAbilities() {
        return Collections.unmodifiableList(abilities);
    }

    public void reset() {
        abilities.clear();
        name = "Custom Kit";
        style = PlayStyle.SOUP;
        inventory = null;
    }

    public boolean containsAbility(int ability_id) {
        for (KitAbilityData data : abilities) {
            if (data.getId() == ability_id) {
                return true;
            }
        }
        return false;
    }

    public KitAbilityData getAbility(int ability_id) {
        for (KitAbilityData ability : abilities) {
            if (ability.getId() == ability_id) {
                return ability;
            }
        }
        throw new IllegalArgumentException("invalid ability id : " + ability_id);
    }

    public ItemStack[] getInventory() {
        return inventory;
    }

    public void setInventory(ItemStack[] inventory) {
        this.inventory = inventory;
    }

    public double getPoints() {
        return getAbilities().stream().mapToDouble(KitData::getPointsForAbility).sum();
    }

    public static double getPointsForAbility(KitAbilityData ability) {
        double points;
        AbilityLoader loader = AbilityLoader.getAbilityData(ability.getId());
        points = loader.getAbilityBasePoints();

        for (AbilityUpgrade upgrade : loader.getAbilityUpgrades()) {
            if (ability.hasUpgrade(upgrade.getUpgradeNumber())) {
                points = points + upgrade.getPoints();
            }
        }
        return points;
    }

}
