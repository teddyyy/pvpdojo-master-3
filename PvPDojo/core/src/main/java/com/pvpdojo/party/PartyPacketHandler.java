/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.party;

import java.util.UUID;

import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.StringUtils;

public class PartyPacketHandler {

    public static final int CREATE = 0;
    public static final int DISBAND = 1;
    public static final int JOIN = 2;
    public static final int LEAVE = 3;

    public static void handle(String[] split) {
        Party party;
        UUID userId;
        switch (Integer.parseInt(split[0])) {
            case CREATE:
                party = DBCommon.GSON.fromJson(split[1], Party.class);
                PartyList.addParty(party);
                break;
            case DISBAND:
                PartyList.removeParty(UUID.fromString(split[1]));
                break;
            case JOIN:
                party = PartyList.getPartyById(UUID.fromString(split[1]));
                userId = UUID.fromString(split[2]);
                if (party != null) {
                    synchronized (party) {
                        party.addUser(userId);
                    }
                    User user = User.getUser(userId);
                    if (user != null) {
                        PvPDojo.schedule(() -> user.setParty(party)).sync();
                    }
                }
                break;
            case LEAVE:
                party = PartyList.getPartyById(UUID.fromString(split[1]));
                userId = UUID.fromString(split[2]);
                if (party != null) {
                    synchronized (party) {
                        party.removeUser(userId);
                        if (StringUtils.UUID.matcher(split[3]).matches()) {
                            party.setLeader(UUID.fromString(split[3]));
                        }
                    }
                    User user = User.getUser(userId);
                    if (user != null && user.getParty() == party) {
                        PvPDojo.schedule(() -> user.setParty(null)).sync();
                    }
                }
                break;
            default:
                Log.warn("Unknown party packet");
        }

    }

    public static void sendCreatePacket(Party party) {
        Redis.get().publish(BungeeSync.PARTY, CREATE + "|" + DBCommon.GSON.toJson(party));
    }

    public static void sendDisbandPacket(Party party) {
        party.channelMessage("Your party has been disbanded");
        Redis.get().publish(BungeeSync.PARTY, DISBAND + "|" + party.getPartyId());
    }

    public static void sendJoinPacket(Party party, UUID user) {
        Redis.get().publish(BungeeSync.PARTY, JOIN + "|" + party.getPartyId() + "|" + user);
    }

    public static void sendLeavePacket(Party party, UUID user, UUID newLeader) {
        Redis.get().publish(BungeeSync.PARTY, LEAVE + "|" + party.getPartyId() + "|" + user + "|" + newLeader);
    }
}
