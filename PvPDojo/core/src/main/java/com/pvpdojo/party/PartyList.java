/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.party;

import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.userdata.User;

public class PartyList {

    private static Map<UUID, Party> loadedParties = new ConcurrentHashMap<>();

    public static boolean contains(UUID party) {
        return loadedParties.containsKey(party);
    }

    public static void addParty(Party party) {
        if (!loadedParties.containsKey(party.getPartyId())) {
            loadedParties.put(party.getPartyId(), party);
        }
    }

    public static void removeParty(UUID partyUUID) {
        Party party = loadedParties.remove(partyUUID);
        if (party != null) {
            party.getUsers().stream().map(User::getUser).filter(Objects::nonNull).forEach(user -> user.setParty(null));
        }
    }

    public static Party getPartyById(UUID partyId) {
        if (partyId == null) {
            return null;
        }
        return loadedParties.get(partyId);
    }

    public static boolean registerParty(Party party) {
        addParty(party);
        PartyPacketHandler.sendCreatePacket(party);
        return party.isValid();
    }

    public static Party createParty(UUID leader, Iterable<UUID> users) {
        return createParty(leader, users, false);
    }

    public static Party createParty(UUID leader, Iterable<UUID> users, boolean temporary) {
        Party party = new Party(UUID.randomUUID());
        party.addUser(leader);
        party.setLeader(leader);
        party.setTemporary(temporary);
        for (UUID user : users) {
            party.addUser(user);
        }

        registerParty(party);

        PvPDojo.schedule(() -> {
            for (UUID uuid : party.getUsers()) {
                User user = User.getUser(uuid);
                if (user != null) {
                    user.setParty(party);
                }
            }
            party.channelMessage("Party has been created");
        }).sync();

        return party;
    }

    public static void addPartyUser(Party party, UUID userUUID) {
        boolean sendPacket = false;
        synchronized (party) {
            if (party.addUser(userUUID)) {
                sendPacket = true;
            }
        }

        if (sendPacket) {
            PartyPacketHandler.sendJoinPacket(party, userUUID);
        }
    }

    public static void removePartyUser(Party party, UUID userUUID) {
        boolean sendPacket = false;
        boolean empty = false;
        UUID newLeader = null;
        synchronized (party) {
            if (party.removeUser(userUUID)) {
                if (party.getLeader().equals(userUUID)) {
                    if (party.getUsers().isEmpty()) {
                        empty = true;
                    } else {
                        newLeader = party.getUsers().iterator().next();
                    }
                }
                sendPacket = true;
            }
        }
        if (empty) {
            PartyPacketHandler.sendDisbandPacket(party);
        } else if (sendPacket) {
            PartyPacketHandler.sendLeavePacket(party, userUUID, newLeader);
        }
    }

}
