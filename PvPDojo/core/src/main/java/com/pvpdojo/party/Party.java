/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.party;

import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.util.StringUtils;

import lombok.Getter;
import lombok.Setter;

public class Party {

    private UUID partyId;
    private UUID leader;
    @Getter
    @Setter
    private boolean temporary;
    private ConcurrentHashMap<UUID, Boolean> users = new ConcurrentHashMap<>();

    public Party() {}

    public Party(UUID partyId) {
        this.partyId = partyId;
    }

    public UUID getPartyId() {
        return partyId;
    }

    public void sendMessage(String... msg) {
        getUsers().stream()
                  .filter(uuid -> Bukkit.getPlayer(uuid) != null)
                  .map(Bukkit::getPlayer)
                  .forEach(player -> player.sendMessage(msg));
    }

    public UUID getLeader() {
        return leader;
    }

    public void setLeader(UUID leader) {
        if (!getUsers().contains(leader)) {
            throw new IllegalArgumentException("Leader must be a member");
        }
        this.leader = leader;

    }

    public boolean addUser(UUID user) {
        return getUsers().add(user);
    }

    public boolean removeUser(UUID user) {
        return getUsers().remove(user);

    }

    public int size() {
        return getUsers().size();
    }

    public Set<UUID> getUsers() {
        return users.keySet(Boolean.TRUE);
    }

    public boolean isValid() {
        return PartyList.contains(getPartyId()) && leader != null;
    }

    public void channelMessage(String msg) {
        PvPDojo.schedule(() -> Redis.get().publish(BungeeSync.CHANNEL_CHAT, ChatChannel.PARTY + "|" + getPartyId() + "|" + StringUtils.toBase64(msg))).createNonMainThreadTask();
    }

    @Override
    public int hashCode() {
        return partyId.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Party && ((Party) obj).partyId.equals(partyId);
    }
}
