/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo;

import com.pvpdojo.mysql.EloSeason;
import com.pvpdojo.userdata.User;

public class DeveloperSettings {

    public static final double MAX_KIT_POINTS = 25.0;
    public static final float HEALTH_CONVERTER = 25;
    public static final EloSeason SEASON = EloSeason.FOUR;

    public static boolean freeAbilities(User user) {
        return user.hasFakeAbilities();
    }
}
