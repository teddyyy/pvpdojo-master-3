/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.clan;

import static com.pvpdojo.mysql.Tables.CLANS;
import static com.pvpdojo.util.StringUtils.COMMA;
import static com.pvpdojo.util.StringUtils.getSetFromString;
import static com.pvpdojo.util.StringUtils.getStringFromArray;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.bukkit.Bukkit;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.mysql.SQL;
import com.pvpdojo.mysql.SQLBuilder;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.StringUtils;

import net.md_5.bungee.api.chat.TextComponent;

public class Clan {

    public static final int MEMBER_LIMIT = 15;

    private transient ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    private int id;
    private String name;
    private Set<UUID> members, leaders;
    private int elo;
    private boolean valid;

    public Clan(String name) {
        this.name = name;
        this.members = new HashSet<>();
        this.leaders = new HashSet<>();
    }

    public ReadWriteLock getLock() {
        return lock;
    }

    public void transaction(Runnable action) {
        lock.writeLock().lock();
        try {
            action.run();
            update();
        } finally {
            lock.writeLock().unlock();
        }
    }

    private void checkLock() {
        if (valid && !lock.isWriteLockedByCurrentThread()) {
            throw new IllegalStateException("Party can only be modified through Party#transaction");
        }
    }

    public int getId() {
        lock.readLock().lock();
        try {
            return id;
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * This method should be used after calling {@link Clan#reload()} to see if the clan still exists. An existing clan has at least one leader.
     *
     * @return True if clan exists
     */
    public boolean isValid() {
        lock.readLock().lock();
        try {
            return valid;
        } finally {
            lock.readLock().unlock();
        }
    }

    public Set<UUID> getMembers() {
        lock.readLock().lock();
        try {
            return members;
        } finally {
            lock.readLock().unlock();
        }
    }

    public Set<UUID> getLeaders() {
        lock.readLock().lock();
        try {
            return leaders;
        } finally {
            lock.readLock().unlock();
        }
    }

    public Set<UUID> getAllMembers() {
        lock.readLock().lock();
        try {
            Set<UUID> members = new HashSet<>();
            members.addAll(this.members);
            members.addAll(this.leaders);
            return members;
        } finally {
            lock.readLock().unlock();
        }
    }

    public int getElo() {
        lock.readLock().lock();
        try {
            return elo;
        } finally {
            lock.readLock().unlock();
        }
    }

    public void setElo(int elo) {
        checkLock();
        this.elo = elo;
    }

    public void addMember(UUID uuid) {
        checkLock();
        leaders.remove(uuid);
        members.add(uuid);
    }

    public void addLeader(UUID uuid) {
        checkLock();
        members.remove(uuid);
        leaders.add(uuid);
    }

    public String getName() {
        lock.readLock().lock();
        try {
            return name;
        } finally {
            lock.readLock().unlock();
        }
    }

    public void removePlayer(UUID uuid) {
        checkLock();
        if (leaders.remove(uuid)) {
            if (leaders.size() == 0) {
                valid = false;
                PvPDojo.schedule(this::unRegister).createAsyncTask();
            }
        } else {
            members.remove(uuid);
        }
    }

    public void sendMessage(String... msg) {
        lock.readLock().lock();
        try {
            getAllMembers().stream()
                           .filter(uuid -> Bukkit.getPlayer(uuid) != null)
                           .map(Bukkit::getPlayer)
                           .forEach(player -> player.sendMessage(msg));
        } finally {
            lock.readLock().unlock();
        }
    }

    public void sendMessage(TextComponent... components) {
        lock.readLock().lock();
        try {
            getAllMembers().stream()
                           .filter(uuid -> Bukkit.getPlayer(uuid) != null)
                           .map(Bukkit::getPlayer)
                           .forEach(player -> player.spigot().sendMessage(components));
        } finally {
            lock.readLock().unlock();
        }
    }

    public void update() {
        if (valid) {
            String update = "leaders = ?, members = ?, elo = ?";
            SQL.updateMultiple(CLANS, update, "name = ?", getStringFromArray(getLeaders(), UUID::toString, ","),
                    getStringFromArray(getMembers(), UUID::toString, ","), elo, name);
        }
    }

    public void reload() {
        try (SQLBuilder sql = new SQLBuilder()) {
            sql.select(Tables.CLANS, "*").where("name = ?").executeQuery(name);

            if (sql.next()) {
                try {
                    lock.writeLock().lock();

                    name = sql.getString("name");
                    id = sql.getInteger("id");
                    members = getSetFromString(sql.getString("members"), COMMA, UUID::fromString);
                    leaders = getSetFromString(sql.getString("leaders"), COMMA, str -> str.equals("null") ? null : UUID.fromString(str));
                    elo = sql.getInteger("elo");
                    valid = true;

                } finally {
                    lock.writeLock().unlock();
                }
            } else {
                valid = false;
            }

        } catch (SQLException e) {
            Log.exception("Reloading clan", e);
            valid = false;
        }
    }

    public boolean register() {
        if (!name.matches("^[A-Za-z0-9_]+$")) {
            return false;
        }
        try {
            SQL.insert(Tables.CLANS, "0,'" + name + "','" + getStringFromArray(getMembers(), UUID::toString, ",") + "','"
                    + getStringFromArray(getLeaders(), uuid -> uuid != null ? uuid.toString() : null, ",") + "', 0");
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public void unRegister() {
        try {
            SQL.remove(Tables.CLANS, "name = ?", name);
            valid = false;
        } catch (SQLException e) {
            Log.exception("Unregistering clan " + name, e);
        }
    }

    public void channelMessage(String msg) {
        PvPDojo.schedule(() -> Redis.get().publish(BungeeSync.CHANNEL_CHAT, ChatChannel.CLAN + "|" + getName() + "|" + StringUtils.toBase64(msg))).createNonMainThreadTask();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Clan && ((Clan) obj).getId() == id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
