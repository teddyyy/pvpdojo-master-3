/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.clan;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import com.pvpdojo.mysql.SQL;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.util.Log;

public class ClanList {

    private static Map<String, Clan> loadedClans = new ConcurrentHashMap<>();

    public static synchronized Clan getClan(String clanName) {
        if (clanName == null) {
            return null;
        }
        Clan clan = loadedClans.containsKey(clanName.toLowerCase()) ? loadedClans.get(clanName.toLowerCase()) : new Clan(clanName);
        clan.reload();
        if (clan.isValid()) {
            if (!loadedClans.containsKey(clanName.toLowerCase())) {
                loadedClans.put(clanName.toLowerCase(), clan);
            }
            return clan;
        } else {
            loadedClans.remove(clanName.toLowerCase());
        }
        return null;
    }

    public static Clan tryRegisterClan(String clanName, UUID creator) {
        Clan clan = new Clan(clanName);
        clan.addLeader(creator);
        if (clan.register()) {
            loadedClans.put(clanName.toLowerCase(), clan);
            return clan;
        }
        return null;
    }

    public static List<String> getAllClanNames() {

        try {
            List<String>clans = SQL.getStrings(Tables.CLANS, "name", "");
            clans.sort(String.CASE_INSENSITIVE_ORDER);
            return clans;
        } catch (SQLException e) {
            Log.exception("Pulling all clan names", e);
            return Collections.emptyList();
        }

    }

}
