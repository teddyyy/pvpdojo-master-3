/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.clan;

import static com.pvpdojo.util.StringUtils.createComponent;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.pvpdojo.Discord;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.ProxyOfflinePlayer;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.CompBuilder;
import com.pvpdojo.util.TriConsumer;

public class ClanManager {

    public static void manageClan(User user, String clanName, BiConsumer<User, Clan> action) {
        PvPDojo.schedule(() -> action.accept(user, ClanList.getClan(clanName))).createAsyncTask();
    }

    public static void manageClanMember(User user, String clanMember, TriConsumer<User, Clan, OfflinePlayer> action) {
        PvPDojo.schedule(() -> action.accept(user, user.getClan(), PvPDojo.getOfflinePlayer(clanMember))).createAsyncTask();
    }

    public static boolean isMemberOfClan(User user, Clan clan, OfflinePlayer player) {
        if (clan.getAllMembers().contains(player.getUniqueId())) {
            return true;
        } else {
            user.sendMessage(PvPDojo.WARNING + "This player is not in your clan");
            return false;
        }
    }

    public static void createClan(User user, String clanName) {
        PvPDojo.newChain().asyncFirst(() -> ClanList.tryRegisterClan(clanName, user.getUUID())).syncLast(clan -> {
            if (clan != null) {
                user.setClan(clan);
                clan.channelMessage(CC.GRAY + "Clan has been created");
                user.updateForEveryOne();
                user.getPersistentData().setClan(clan.getName());
            } else {
                user.sendMessage(PvPDojo.WARNING + "Clan already exists or invalid characters");
            }
        }).execute();
    }

    public static void invite(User user, Player target) {
        User targetUser = User.getUser(target);
        if (targetUser.getClan() != null) {
            user.sendMessage(PvPDojo.WARNING + "That player is already in a clan");
            return;
        }

        if (user.getClan().getAllMembers().size() >= Clan.MEMBER_LIMIT) {
            user.sendMessage(PvPDojo.WARNING + "Your clan reached the maximum amount of " + Clan.MEMBER_LIMIT + " members (you can kick someone with /clan kick)");
            return;
        }

        String failReason = targetUser.makeRequest(user.getUUID(), new ClanRequest(user.getClan()));
        if (failReason == null) {
            user.sendMessage(MessageKeys.INVITE_CLAN,"{target}", targetUser.getName() );
            targetUser.sendMessage(createComponent(PvPDojo.PREFIX + CC.BLUE + user.getName() + CC.GRAY + " invited you to their clan " + CC.AQUA
                            + user.getClan().getName() + CC.GRAY + " (click to accept)",
                    CC.GREEN + "accept", "accept " + user.getUUID().toString()));
        } else {
            user.sendMessage(PvPDojo.WARNING + failReason);
        }
    }

    public static void leaveClan(User user) {
        Clan clan = user.getClan();
        clan.channelMessage(CC.BLUE + user.getName() + CC.GRAY + " left the clan");
        user.setClan(null);
        user.getPersistentData().setClan(null);
        user.updateForEveryOne();
        PvPDojo.schedule(() -> clan.transaction(() -> clan.removePlayer(user.getUniqueId()))).createAsyncTask();
    }

    public static void showInfo(User user, Clan clan) {
        if (clan != null) {
            user.sendMessage(CC.GRAY + "Name" + PvPDojo.POINTER + CC.AQUA + clan.getName());
            if (clan.getLeaders().contains(null)) {
                user.sendMessage(CC.RED + "This clan name is blocked");
                return;
            }

            Consumer<ProxyOfflinePlayer> playerLister = off -> user.sendMessage(CC.GRAY + "- " + CC.BLUE + off.getName() + CC.GRAY + ": "
                    + (off.isOnlineProxy() ? CC.GREEN + "Online" : CC.RED + "Offline"));

            user.sendMessage(CC.GRAY + "Leaders:");
            clan.getLeaders().stream().map(PvPDojo::getOfflinePlayer).forEach(playerLister);

            user.sendMessage(CC.GRAY + "Members:");
            clan.getMembers().stream().map(PvPDojo::getOfflinePlayer).forEach(playerLister);
        } else {
            user.sendMessage(PvPDojo.WARNING + "Clan not found");
        }
    }

    public static void promote(User user, Clan clan, OfflinePlayer player) {
        if (isMemberOfClan(user, clan, player)) {
            if (clan.getLeaders().contains(player.getUniqueId())) {
                user.sendMessage(PvPDojo.WARNING + "User is already a leader");
            } else {
                clan.transaction(() -> clan.addLeader(player.getUniqueId()));
                clan.channelMessage(CC.BLUE + player.getName() + CC.GRAY + " has been promoted to a " + CC.RED + "leader");
            }
        }
    }

    public static void demote(User user, Clan clan, OfflinePlayer player) {
        if (isMemberOfClan(user, clan, player)) {
            if (clan.getMembers().contains(player.getUniqueId())) {
                user.sendMessage(PvPDojo.WARNING + "User is already a member");
            } else if (clan.getLeaders().size() == 1) {
                user.sendMessage(PvPDojo.WARNING + "You are the only leader! You can leave the clan to delete it, but you cannot demote yourself");
            } else {
                clan.transaction(() -> clan.addMember(player.getUniqueId()));
                clan.channelMessage(CC.BLUE + player.getName() + CC.GRAY + " has been demoted to a " + CC.GREEN + "member");
            }
        }
    }

    public static void kick(User user, Clan clan, OfflinePlayer player) {
        if (isMemberOfClan(user, clan, player)) {
            if (clan.getLeaders().contains(player.getUniqueId())) {
                user.sendMessage(PvPDojo.WARNING + "You cannot kick clan leaders demote them first");
            } else {
                PvPDojo.newChain().async(() -> {
                    clan.channelMessage(CC.BLUE + player.getName() + CC.GRAY + " has been kicked out of the clan by " + CC.BLUE + user.getName());
                }).delay(5).asyncFirst(() -> { // Delay by 5 ticks to give the message a chance to pass to kicked user
                    clan.transaction(() -> clan.removePlayer(player.getUniqueId()));
                    new PersistentData(player.getUniqueId()).setClan(null);
                    return player;
                }).abortIfNot(OfflinePlayer::isOnline).syncLast(online -> { // Update user if online
                    User targetUser = User.getUser(online.getUniqueId());
                    targetUser.setClan(null);
                    targetUser.updateForEveryOne();
                }).execute();
            }
        }
    }

    public static void delete(User user, Clan clan) {
        if (clan != null) {
            clan.transaction(clan::unRegister);
            user.sendMessage(new CompBuilder(PvPDojo.PREFIX + CC.RED + "You deleted " + clan.getName() + " (click to block clan name)")
                    .hoverMessage(CC.DARK_RED + "block")
                    .command("clan block " + clan.getName())
                    .create());

            if (!PvPDojo.get().isTest()) {
                Discord.text().sendMessage("Clan " + clan.getName() + " has been deleted by " + user.getPlayer().getName()).complete();
            }
        } else {
            user.sendMessage(PvPDojo.WARNING + "Clan not found");
        }
    }

    public static void block(User user, String clanName) {
        PvPDojo.schedule(() -> {
            if (ClanList.getClan(clanName) != null) {
                user.sendMessage(PvPDojo.WARNING + "You have to delete the clan first");
                return;
            }
            if (ClanList.tryRegisterClan(clanName, null) != null) {
                user.sendMessage(PvPDojo.PREFIX + CC.GREEN + "Blocked " + clanName);

                if (!PvPDojo.get().isTest()) {
                    Discord.text().sendMessage("Clan name " + clanName + " has been blocked by " + user.getPlayer().getName()).complete();
                }
            } else {
                user.sendMessage(PvPDojo.WARNING + "Error");
            }
        }).createAsyncTask();
    }

}
