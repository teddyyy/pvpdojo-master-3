/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.clan;

import static com.pvpdojo.mysql.Tables.USERS;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.ActionHandlers;
import com.pvpdojo.mysql.SQL;
import com.pvpdojo.userdata.Request;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;

public class ClanRequest extends Request {

    String clanName;

    public ClanRequest(Clan clan) {
        this.clanName = clan.getName();
    }

    @Override
    public void accept(User acceptor) {
        PvPDojo.newChain().asyncFirst(() -> ClanList.getClan(clanName)).abortIfNull(ActionHandlers.MESSAGE, acceptor.getPlayer(), PvPDojo.WARNING + "Clan not found").sync(clan -> {
            if (acceptor.getClan() != null) {
                acceptor.sendMessage(PvPDojo.WARNING + "You are already in a clan");
                return null;
            }

            if (clan.getAllMembers().size() >= Clan.MEMBER_LIMIT) {
                acceptor.sendMessage(PvPDojo.WARNING + "Clan reached the limit of " + Clan.MEMBER_LIMIT + " members");
                return null;
            }

            PvPDojo.schedule(() -> SQL.updateMultiple(USERS, "lastclanswitch = NOW()", "uuid = ?", acceptor.getUUID().toString())).createAsyncTask();
            acceptor.setClan(clan);
            acceptor.updateForEveryOne();
            acceptor.getPersistentData().setClan(clan.getName());
            clan.channelMessage(CC.BLUE + acceptor.getName() + CC.GRAY + " joined the clan");
            return clan;
        }).abortIfNull().asyncLast(clan -> {
            clan.transaction(() -> clan.addMember(acceptor.getUUID()));
        }).execute();
    }

    @Override
    public void deny(User deniedBy) {}

    @Override
    public int getExpirationSeconds() {
        return 60;
    }

    @Override
    public boolean isPersistent() {
        return true;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ClanRequest;
    }
}
