/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo;

public enum Warp {

    SPAWN(
            ServerType.HUB,
            true),
    F_F_A(
            ServerType.FFA,
            false),
    CLASSIC(
            ServerType.CLASSIC,
            false),
    CHALLENGES(
            ServerType.CHALLENGES,
            false),
    DAMAGER(
            ServerType.CHALLENGES,
            true),
    LAVA(
            ServerType.CHALLENGES,
            true),
    PARCOUR(
            ServerType.CHALLENGES,
            true),
    MARKET(
            ServerType.HUB,
            true),
    KITCREATOR(
            ServerType.HUB,
            true),
    STAFF(
            ServerType.HUB,
            true),
    LEADERBOARD(
            ServerType.HUB,
            true);

    private boolean extraTeleport;
    private ServerType server;

    Warp(ServerType server, boolean extraTeleport) {
        this.server = server;
        this.extraTeleport = extraTeleport;
    }

    public ServerType getServer() {
        return server;
    }

    public boolean isExtraTeleport() {
        return extraTeleport;
    }

}
