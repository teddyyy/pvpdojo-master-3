/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.achievement;

import org.bukkit.entity.Player;

public enum Achievement {

    WELCOME("Join the server for your first time", 10),
    FIRST_BLOOD("Kill your first player", 5),
    CREATOR("Create your first kit", 2),
    RAMPAGE("Get a killstreak of 5", 6),
    UNSTOPPABLE("Get a killstreak of 10", 8),
    KILLING_FRENZY("Get a killstreak of 15", 12),
    PATH_OF_THE_SOUPER("Get a kill while on the Path of the Souper", 3),
    PATH_OF_THE_POTTER("Get a kill while on the Path of the Potter", 3),
    PATH_OF_THE_SURVIVOR("Get a kill while on the Path of the Survivor", 3),
    ESCAPEE("Survive your first Prisoner trap", 2),
    EL_DORADO("Get your first gold ability", 4),
    LOTTERY("Get your first legendary ability", 10),
    CLASSICAL("Get a kill using no abilities", 3),
    MASSACRE("Reach 100 kills", 8),
    DOMINATION("Reach 500 kills", 16),
    GODLIKE("Reach 1000 kills", 25),
    SALESMAN("Sell your first ability", 5),
    CUSTOMER("Buy your first ability", 5),
    NEGOTIATOR("Complete a private trade for the first time", 2),
    DUEL_PLAYER("Win a Deathmatch", 4),
    TEAM_PLAYER("Win a team fight", 4),
    SMURF("Kill a player that is a lower rank than you", 5),
    ELO_HELL("Lose your first ranked 1v1 match", 5),
    THE_BIG_LEAGUE("Enter the leaderboard", 15),
    GREETINGS_FROM_THE_AFTERLIFE("Kill a player after your death", 10),
    CLOSE_CALL("Win a fight with only half a heart left", 3),
    GENEROSITY("Gift an Ability to someone", 2),
    GANG_MEMBER("Join a clan", 2),
    RECYCLE("Sign your first Trade Up Contract", 5),

    RIVALRY("Win a Lava 1v1", 5),
    INVENTORY_MANAGEMENT("Survive the Crap Damager", 8),
    INCONSISTENCY("Survive the Inconsistency Damager", 8),
    TANK_GOD("Survive the Giga Damager", 25),
    PINPOINT_PRECISION("Complete a Challenge with 100% Soup accuracy", 5),
    TANKING_BASICS("Survive the Easy-, Medium-, and Hard Damager", 10),

    TOP_OF_THE_WORLD("Reach the top of the Tower Challenge", 8),
    GOING_DOWNSTAIRS("Reach the end of the Upsidedown Tower", 5),
    CONSISTENCY("Reach the end of the Consistency Challenge", 12),
    SUPER_CONSISTENCY("Reach the end of the Super Consistency Challenge", 20),
    AGILITY("Reach the end of the Damager Parcour", 12),
    ESCELATION("Reach the end of the Ladder Challenge", 15),

    CATTLE_FARMER("Leave a like on NameMC to earn the Milkman kit for free", 1),
    CARBON_COPY("Reach 100 total kills in HG to earn Copycat kit for free", 3),
    TRAPPING_STARTER_PACK("Win your very first round of HG to earn the Endermage kit for free", 3),
    TEN_FOR_ONE("Purchase 10 kits and earn the Worm kit for free", 3);

    String description;
    int points;

    Achievement(String description, int points) {
        this.description = description;
        this.points = points;
    }

    public int getPoints() {
        return points;
    }

    public String getDescription() {
        return description;
    }

    public void trigger(Player player) {
        AchievementManager.inst().trigger(this, player);
    }
}
