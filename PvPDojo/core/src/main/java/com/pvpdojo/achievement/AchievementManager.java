/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.achievement;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.StringUtils;

public class AchievementManager {

    private static final AchievementManager INST = new AchievementManager();

    public static AchievementManager inst() {
        return INST;
    }

    public void trigger(Achievement achievement, Player player) {
        User user = User.getUser(player);

        if (user != null && !user.isLoggedOut() && user.isDataLoaded()) {
            if (user.getPersistentData().getAchievements().add(achievement)) {
                user.getPersistentData().saveAchievements(success -> {
                    if (success) {
                        user.getPersistentData().incrementAchievementPoints(achievement.getPoints());
                    }
                });
                sendCosmetics(achievement, player);
            }
        } else {
            Log.warn("Could not pass achievement " + achievement + " (logged out or not loaded)");
        }
    }

    public void sendCosmetics(Achievement achievement, Player player) {

        player.sendMessage(CC.DARK_GRAY + StringUtils.getLS());
        player.sendMessage(CC.AQUA + " You unlocked the " + CC.GREEN + StringUtils.getEnumName(achievement, true) + " Achievement");
        player.sendMessage(CC.AQUA + " and earned " + CC.YELLOW + achievement.getPoints() + " Achievement Points");
        player.sendMessage(CC.DARK_GRAY + StringUtils.getLS());

        player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 0);

        FireworkEffect firework = FireworkEffect.builder().with(FireworkEffect.Type.BURST).withColor(Color.GREEN).withColor(Color.LIME).withFlicker().withTrail().build();
        BukkitUtil.launchFireworkEffect(player.getLocation(), firework);

    }

    public void handleKill(User user, int streak) {
        AchievementManager.inst().trigger(Achievement.FIRST_BLOOD, user.getPlayer());

        if (user.getPlayer().isDead() && user.getPlayer().getKiller() != user.getPlayer()) {
            AchievementManager.inst().trigger(Achievement.GREETINGS_FROM_THE_AFTERLIFE, user.getPlayer());
        }

        if (user.getPlayer().getHealth() < 1 && !user.getPlayer().isDead()) {
            AchievementManager.inst().trigger(Achievement.CLOSE_CALL, user.getPlayer());
        }

        if (user.getPersistentData().getKills() >= 100) {
            AchievementManager.inst().trigger(Achievement.MASSACRE, user.getPlayer());
            if (user.getPersistentData().getKills() >= 500) {
                AchievementManager.inst().trigger(Achievement.DOMINATION, user.getPlayer());
                if (user.getPersistentData().getKills() >= 1000) {
                    AchievementManager.inst().trigger(Achievement.GODLIKE, user.getPlayer());
                }
            }
        }

        if (streak >= 5) {
            AchievementManager.inst().trigger(Achievement.RAMPAGE, user.getPlayer());
            if (streak >= 10) {
                AchievementManager.inst().trigger(Achievement.UNSTOPPABLE, user.getPlayer());
                if (streak >= 15) {
                    AchievementManager.inst().trigger(Achievement.KILLING_FRENZY, user.getPlayer());
                }
            }
        }
    }

}
