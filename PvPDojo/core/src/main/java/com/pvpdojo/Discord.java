/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo;

import com.pvpdojo.mysql.SQL;
import com.pvpdojo.mysql.SQLBuilder;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.settings.FastDB;
import com.pvpdojo.settings.SettingsType;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.TimeUtil;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import org.bukkit.entity.Player;

import javax.security.auth.login.LoginException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.OptionalInt;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.pvpdojo.util.StringUtils.getRandomHexString;

public class Discord {

    public static final Discord instance = new Discord();

    private JDA jda;

    private static final long TEXT = 470767633315266571L;
    private static final long STAFF_DISCORD = 356393230381744129L;
    private static final long BASTI_DISCORD = 484676017513037844L;
    private static final long PUBLIC_DISCORD = 489125956586307585L;

    public static final long BASTI_SUB = 530420298026319882L;
    public static final long DOJO_SUB = 569500291242786856L;

    private Discord() {
        connect();
    }

    public void connect() {
        try {
            JDA jda = new JDABuilder(AccountType.BOT).setToken("NDY3NjMyODc4MDEzMTIwNTIz.DitlLA.je3kF-s5v3L_-LhYf0ExETvdPlQ").build();
            jda.awaitReady();
            jda.getPresence().setActivity(Activity.listening("appeals"));
            this.jda = jda;
        } catch (LoginException | InterruptedException e) {
            e.printStackTrace();
            // No retry logic needed. JDA has internal retry methods and really if the discord or channels changed everything here is fucked anyway
        }
    }

    public static Discord get() {
        return instance;
    }

    public static TextChannel text() {
        return get().jda.getTextChannelById(TEXT);
    }

    public static TextChannel test() {
        return getJda().getTextChannelById(467642687990923264L);
    }

    public static TextChannel stasi() {
        return getJda().getTextChannelById(490212528807215105L);
    }

    public static TextChannel capital() {
        return getJda().getTextChannelById(494862995651035141L);
    }

    public static TextChannel appeals() {
        return getJda().getTextChannelById(581925786081296412L);
    }

    public static TextChannel blamePayback() {
        return getJda().getTextChannelById(497006947120185345L);
    }

    public static JDA getJda() {
        return get().jda;
    }

    public void handleAuth(Player player) {
        if (!PvPDojo.get().isTest()) {
            String authKey = getRandomHexString(6);

            FastDB fastDB = DBCommon.loadSettings(SettingsType.FAST_DB, player.getUniqueId());
            fastDB.setAuthKey(authKey);
            fastDB.save(player.getUniqueId());

            sendAuth(player, authKey);
        }
    }

    public void sendAuth(Player player, String authKey) {
        String authMessage = "/auth " + authKey;

        MessageChannel targetChannel = text();
        Member target = Discord.text().getMembers().stream().filter(member -> member.getEffectiveName().equalsIgnoreCase(player.getName())).findAny().orElse(null);
        if (target != null && target.getUser() != null) {
            targetChannel = target.getUser().openPrivateChannel().complete();
        }
        if (targetChannel.getHistory().retrievePast(10).complete().stream().noneMatch(message -> message.getContentRaw().startsWith(authMessage))) {
            MessageBuilder builder = new MessageBuilder(authMessage);
            if (target == null) {
                builder.append(" ").append(player.getName());
            }
            Message message = builder.build();
            targetChannel.sendMessage(message).queue();
            test().sendMessage(message).queue();
        }
    }

    public void register(int id, String discordUUID) throws SQLException {
        SQL.remove(Tables.DISCORD_LINK, "userid = ? OR discordid = ?", id, discordUUID);
        try (SQLBuilder sql = new SQLBuilder()) {
            sql.insert(Tables.DISCORD_LINK, "userid, discordid, date").values("?,?,NOW()").executeUpdate(id, discordUUID);
        }
    }

    public boolean canRegister(int id, String discordUUID) throws SQLException {
        try (SQLBuilder sql = new SQLBuilder()) {
            sql.select(Tables.DISCORD_LINK, "date").where("userid = ?").or("discordid = ?").executeQuery(id, discordUUID);
            if (sql.next()) {
                Timestamp dateCreated = sql.getTimestamp("date");

                // If we have another entry take the newest
                if (sql.next()) {
                    Timestamp dateCreatedSecond = sql.getTimestamp("date");
                    dateCreated = dateCreated.before(dateCreatedSecond) ? dateCreatedSecond : dateCreated;
                }

                return TimeUtil.hasPassed(dateCreated.getTime(), TimeUnit.DAYS, 30);
            } else {
                return true;
            }
        }
    }

    public String getDiscordLink(UUID uuid) throws SQLException {
        return SQL.getString(Tables.DISCORD_LINK, "discordid", "userid = ?", DBCommon.getUserId(uuid).orElseThrow(SQLException::new));
    }

    public OptionalInt getUserLink(String discordUUID) {
        try {
            return SQL.getInteger(Tables.DISCORD_LINK, "userid", "discordid = ?", discordUUID);
        } catch (SQLException e) {
            Log.exception(e);
        }
        return OptionalInt.empty();
    }

    public boolean isBastiSub(String discordUUID) {
        Guild bastiDc = jda.getGuildById(BASTI_DISCORD);
        if (bastiDc != null) {
            Member member = bastiDc.getMemberById(discordUUID);
            return member != null && member.getRoles().contains(bastiDc.getRoleById(BASTI_SUB));
        }
        return false;
    }

    public boolean isDojoSub(String discordUUID) {
        Guild publicDc = jda.getGuildById(PUBLIC_DISCORD);
        if (publicDc != null) {
            Member member = publicDc.getMemberById(discordUUID);
            return member != null && member.getRoles().contains(publicDc.getRoleById(DOJO_SUB));
        }
        return false;
    }

}
