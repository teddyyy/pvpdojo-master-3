/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.lang;

import co.aikar.locales.MessageKeyProvider;

public interface LocaleReceiver {

    void sendMessage(LocaleMessage localeMessage);

    default void sendMessage(String... messages) {
        for (String message : messages) {
            sendMessage(LocaleMessage.of(message));
        }
    }

    default void sendMessage(MessageKeyProvider key, String... replacements) {
        sendMessage(LocaleMessage.of(key, replacements));
    }

}
