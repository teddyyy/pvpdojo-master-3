/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.lang;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.userdata.User;

import co.aikar.locales.MessageKeyProvider;
import lombok.Data;

@Data
public class LocaleMessage {

    public static LocaleMessage of(MessageKeyProvider key, String... replacements) {
        return new LocaleMessage(key, replacements);
    }

    public static LocaleMessage of(String message) {
        return of(MessageKeys.GENERIC, "{text}", message);
    }

    MessageKeyProvider key;
    String[] replacements;
    List<Function<String, String>> transformers;

    private LocaleMessage(MessageKeyProvider key, String... replacements) {
        this.key = key;
        this.replacements = replacements;
    }

    private String[] transform(String[] localized) {
        if (transformers != null) {
            for (Function<String, String> transformer : transformers) {
                for (int i = 0; i < localized.length; i++) {
                    localized[i] = transformer.apply(localized[i]);
                }
            }
        }
        return localized;
    }

    public LocaleMessage transformer(Function<String, String> transformer) {
        if (transformers == null) {
            transformers = new ArrayList<>();
        }
        transformers.add(transformer);
        return this;
    }

    public String[] getDefaultLocale() {
        return get(null);
    }

    public String[] get(User user) {
        return transform(PvPDojo.LANG.getMessage(user, key, replacements));
    }

}
