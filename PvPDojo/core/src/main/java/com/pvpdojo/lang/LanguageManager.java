/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.lang;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.regex.Matcher;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.Log;

import co.aikar.commands.ACFPatterns;
import co.aikar.commands.ACFUtil;
import co.aikar.locales.LocaleManager;
import co.aikar.locales.MessageKey;
import co.aikar.locales.MessageKeyProvider;

public class LanguageManager {

    // Locales for reference since Locale doesn't have as many, add our own here for ease of use.
    public static final Locale ENGLISH = Locale.ENGLISH;
    public static final Locale GERMAN = Locale.GERMAN;
    public static final Locale FRENCH = Locale.FRENCH;
    public static final Locale POLISH = new Locale("pl");

    public static final Set<Locale> SUPPORTED_LANGUAGES = new HashSet<>(Arrays.asList(ENGLISH, GERMAN, FRENCH, POLISH));

    private static final String[] DEFAULT_REPLACEMENTS;

    static {
        DEFAULT_REPLACEMENTS = new String[CC.values().length * 2 + 6];

        DEFAULT_REPLACEMENTS[0] = "{pvpdojo}";
        DEFAULT_REPLACEMENTS[1] = PvPDojo.PREFIX;
        DEFAULT_REPLACEMENTS[2] = "{warning}";
        DEFAULT_REPLACEMENTS[3] = PvPDojo.WARNING;
        DEFAULT_REPLACEMENTS[4] = "{tip}";
        DEFAULT_REPLACEMENTS[5] = CC.DARK_GRAY + "[" + CC.YELLOW + "TIP" + CC.DARK_GRAY + "] " + CC.GRAY;

        CC[] values = CC.values();
        for (int i = 0; i < values.length; i++) {
            CC color = values[i];
            DEFAULT_REPLACEMENTS[i * 2 + 6] = "{" + color.name().toLowerCase() + "}";
            DEFAULT_REPLACEMENTS[i * 2 + 1 + 6] = color.toString();
        }
    }

    private final LocaleManager<User> localeManager;
    private final Map<ClassLoader, SetMultimap<String, Locale>> loadedBundles = new HashMap<>();
    private final List<ClassLoader> registeredClassLoaders = new ArrayList<>();

    public LanguageManager() {
        this.localeManager = LocaleManager.create(User::getLocale);
        this.addBundleClassLoader(this.getClass().getClassLoader());
    }

    public void loadLanguages() {
        addMessageBundles("pvpdojo", "pvpdojo-abilities");
    }

    public Locale getDefaultLocale() {
        return this.localeManager.getDefaultLocale();
    }

    public Locale setDefaultLocale(Locale locale) {
        return this.localeManager.setDefaultLocale(locale);
    }

    /**
     * Looks for all previously loaded bundles, and if any new Supported Languages have been added, load them.
     */
    public void loadMissingBundles() {
        //noinspection unchecked
        for (Locale locale : SUPPORTED_LANGUAGES) {
            for (SetMultimap<String, Locale> localeData : this.loadedBundles.values()) {
                for (String bundleName : new HashSet<>(localeData.keys())) {
                    addMessageBundle(bundleName, locale);
                }
            }

        }
    }

    public void addMessageBundles(String... bundleNames) {
        for (String bundleName : bundleNames) {
            for (Locale locale : SUPPORTED_LANGUAGES) {
                addMessageBundle(bundleName, locale);
            }
        }
    }

    public boolean addMessageBundle(String bundleName, Locale locale) {
        boolean found = false;
        for (ClassLoader classLoader : this.registeredClassLoaders) {
            if (this.addMessageBundle(classLoader, bundleName, locale)) {
                found = true;
            }
        }

        return found;
    }

    public boolean addMessageBundle(ClassLoader classLoader, String bundleName, Locale locale) {
        SetMultimap<String, Locale> classLoadersLocales = this.loadedBundles.getOrDefault(classLoader, HashMultimap.create());
        if (!classLoadersLocales.containsEntry(bundleName, locale)) {
            if (this.localeManager.addMessageBundle(classLoader, bundleName, locale)) {
                classLoadersLocales.put(bundleName, locale);
                this.loadedBundles.put(classLoader, classLoadersLocales);
                return true;
            }
        }

        return false;
    }

    public void addMessageStrings(Locale locale, @NotNull Map<String, String> messages) {
        Map<MessageKey, String> map = new HashMap<>(messages.size());
        messages.forEach((key, value) -> map.put(MessageKey.of(key), value));
        this.localeManager.addMessages(locale, map);
    }

    public void addMessages(Locale locale, @NotNull Map<? extends MessageKeyProvider, String> messages) {
        Map<MessageKey, String> messagesMap = new LinkedHashMap<>();
        for (Map.Entry<? extends MessageKeyProvider, String> entry : messages.entrySet()) {
            messagesMap.put(entry.getKey().getMessageKey(), entry.getValue());
        }

        this.localeManager.addMessages(locale, messagesMap);
    }

    public String addMessage(Locale locale, MessageKeyProvider key, String message) {
        return this.localeManager.addMessage(locale, key.getMessageKey(), message);
    }

    public String getMessage(CommandSender sender, MessageKeyProvider key) {
        return getMessage(sender instanceof Player ? User.getUser((Player) sender) : null, key);
    }

    public String getMessage(User user, MessageKeyProvider key) {
        final MessageKey msgKey = key.getMessageKey();
        String message = this.localeManager.getMessage(user, msgKey);
        if (message == null || message.isEmpty()) {
            Log.log(Level.SEVERE, "Missing Language Key: " + msgKey.getKey());
            message = "<MISSING:" + msgKey.getKey() + ">";
        }
        return message;
    }

    public String[] getMessage(User user, MessageKeyProvider key, String... replacements) {
        return ACFPatterns.NEWLINE.split(formatMessage(user, key, replacements));
    }

    public void sendMessage(CommandSender sender, MessageKeyProvider key, String... replacements) {
        sendMessage(sender, LocaleMessage.of(key, replacements));
    }

    public void sendMessage(CommandSender sender, LocaleMessage message) {
        if (sender instanceof Player) {
            sendMessage(User.getUser(((Player) sender)), message);
        } else {
            sender.sendMessage(message.getDefaultLocale());
        }
    }

    public void sendMessage(User user, LocaleMessage message) {
        user.getPlayer().sendMessage(message.get(user));
    }

    public String formatMessage(@Nullable User user, MessageKeyProvider key, String... replacements) {
        String message = getMessage(user, key);
        if (replacements.length > 0) {
            message = ACFUtil.replaceStrings(message, replacements);
        }
        message = replaceI18NStrings(user, message);
        return ACFUtil.replaceStrings(message, DEFAULT_REPLACEMENTS);
    }

    public String createDynamicKey(MessageKeyProvider key) {
        return "{@@" + key.getMessageKey().getKey() + "}";
    }

    public String replaceI18NStrings(User user, String message) {
        if (message == null) {
            return null;
        }
        Matcher matcher = ACFPatterns.I18N_STRING.matcher(message);
        if (!matcher.find()) {
            return message;
        }

        matcher.reset();
        StringBuffer sb = new StringBuffer(message.length());
        while (matcher.find()) {
            MessageKey key = MessageKey.of(matcher.group("key"));
            matcher.appendReplacement(sb, Matcher.quoteReplacement(getMessage(user, key)));
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    public boolean addBundleClassLoader(ClassLoader classLoader) {
        return !this.registeredClassLoaders.contains(classLoader) && this.registeredClassLoaders.add(classLoader);

    }


}
