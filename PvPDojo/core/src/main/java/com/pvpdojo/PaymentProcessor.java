/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo;

import java.io.File;
import java.math.BigInteger;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;

import com.pvpdojo.mysql.SQL;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.util.FileUtil;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.PunishmentManager;
import com.pvpdojo.util.PunishmentManager.PunishmentType;

import net.buycraft.plugin.bukkit.BuycraftPlugin;
import net.buycraft.plugin.data.Package;

public class PaymentProcessor {

    public static final Map<UUID, OldPayment> SUMMARY = new HashMap<>();

    public static final Pattern CSV = Pattern.compile("(?:^|,)\\s*(?:(?:(?=\")\"([^\"].*?)\")|(?:(?!\")(.*?)))(?=,|$)", Pattern.MULTILINE);
    public static final LocalDateTime SUPPORTER_RANK_START = LocalDateTime.of(2016, Month.APRIL, 4, 0, 0);

    public static void proccessPayments() {
        Log.info("STARTING PAYMENT PROCESSOR");
        BuycraftPlugin buycraftPlugin = (BuycraftPlugin) Bukkit.getPluginManager().getPlugin("BuycraftX");
        ArrayList<String> lines = FileUtil.getFileLines(new File(FileUtil.getAbsolutePath() + "/payments.csv"));

        for (String line : lines) {
            String[] split = new String[18];
            Matcher matcher = CSV.matcher(line);

            for (int i = 0; i < split.length; i++) {
                matcher.find();
                if (matcher.group(1) != null) {
                    split[i] = matcher.group(1);
                } else {
                    split[i] = matcher.group(2);
                }
            }

            if (split[6].equalsIgnoreCase("uuid") || split[6].isEmpty()) {
                continue;
            }
            String status = split[3];
            UUID uuid = insertDashUUID(split[6]);

            try {
                if (SQL.checkRowExist(Tables.USERS, "uuid = ?", uuid.toString())) {
                    Log.info("User exists in DB skipping...");
                    continue;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if (status.equals("Chargeback")) {
                Log.info("Caught Chargeback");
                if (!PvPDojo.getOfflinePlayer(uuid).isBanned()) {
                    PunishmentManager.punish(Bukkit.getConsoleSender(), PvPDojo.getOfflinePlayer(uuid).getName(), "Chargebacks are not allowed", -1, PunishmentType.BAN);
                }
                continue;
            }

            if (!status.equals("Complete")) {
                continue;
            }

            String packages = split[15];
            String name = split[5];
            String price = split[7];
            LocalDateTime dateTime = LocalDateTime.parse(split[2], DateTimeFormatter.ISO_DATE_TIME);

            if (Double.valueOf(price) == 300D) {
                Log.info("Caught Grandmaster for " + name);
                SUMMARY.put(uuid, OldPayment.GRANDMASTER);
                continue;
            }

            if (packages.trim().isEmpty() && Double.valueOf(price) == 100D) {
                Log.info("Caught First Dan for " + name);
                SUMMARY.put(uuid, OldPayment.FIRST_DAN);
            }

            if (packages.trim().isEmpty() && dateTime.isAfter(SUPPORTER_RANK_START) && Double.valueOf(price) == 5D) {
                Log.info("Caught Supporter Rank for " + name);
                SUMMARY.put(uuid, OldPayment.SUPPORTER);
            }

            // buycraftPlugin.getListingUpdateTask().getPackageById();

            if (!packages.isEmpty()) {
                List<Package> buycraftPacks;
                if (packages.contains(",")) {
                    buycraftPacks = new ArrayList<>();
                    for (String packageSplit : packages.split(", ")) {
                        buycraftPacks.add(buycraftPlugin.getListingUpdateTask().getPackageById(Integer.valueOf(packageSplit)));
                    }
                } else {
                    Package found = buycraftPlugin.getListingUpdateTask().getPackageById(Integer.valueOf(packages));
                    buycraftPacks = Collections.singletonList(found);
                }
                for (Package buycraftPack : buycraftPacks) {
                    if (buycraftPack != null) {
                        String aPackageName = buycraftPack.getName();
                        if (aPackageName.equals("1st Dan [Lifetime]")) {
                            Log.info("Caught First Dan for " + name);
                            SUMMARY.put(uuid, OldPayment.FIRST_DAN);
                        }
                    }
                }

            }
        }

        Log.info("FINISHED PAYMENT PROCESSOR");
    }

    public static UUID insertDashUUID(String uuid) {
        BigInteger bi1 = new BigInteger(uuid.substring(0, 16), 16);
        BigInteger bi2 = new BigInteger(uuid.substring(16, 32), 16);
        return new UUID(bi1.longValue(), bi2.longValue());
    }

    public static void handleTransaction(String transactionID, String name, UUID uuid, float price) {
        switch (transactionID) {
            default:
                Log.severe("cant find transaction id: " + transactionID);
                break;
        }
    }

}
