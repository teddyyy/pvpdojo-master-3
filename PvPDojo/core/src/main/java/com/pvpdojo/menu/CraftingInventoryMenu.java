/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menu;

import org.bukkit.entity.Player;

public abstract class CraftingInventoryMenu extends InventoryMenu {

    private CraftingInventoryPage page;

    public CraftingInventoryMenu(Player player, String title) {
        super(player);
        page = new CraftingInventoryPage(this, title);
    }

    @Override
    public InventoryPage getCurrentPage() {
        return page;
    }

    @Override
    public void redraw() {
        page.clear();
    }
}
