/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menu;

import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;

public class AsyncSortedInventoryMenu extends SortedInventoryMenu {

    public AsyncSortedInventoryMenu(Player player, String title, int size, SortType... sort) {
        super(player, title, size, sort);
    }

    @Override
    public void addSortItem(SortedItem item) {
        throw new UnsupportedOperationException();
    }

    public void addSortItem(AsyncSortedItem item) {
        super.addSortItem(item);
    }

    @Override
    public void setCurrentPage(int currentPage) {
        super.setCurrentPage(currentPage);

        if (sortcache.isEmpty()) {
            return;
        }

        if (pages.containsKey(currentPage)) {
            setLocked(true);
            PvPDojo.schedule(() -> setLocked(true)).nextTick(); // Workaround for locking in this context
            int slotsUnderBar = this.size - 9;
            int itemsPerPage = slotsUnderBar - 2 * (slotsUnderBar / 9);
            int startingIndex = currentPage * itemsPerPage;
            PvPDojo.newChain()
                   .async(() -> toProcess.subList(startingIndex, Math.min(toProcess.size(), startingIndex + itemsPerPage))
                                         .stream()
                                         .map(sortedItem -> (AsyncSortedItem) sortedItem)
                                         .forEach(item -> getCurrentPage().setItem(item.getInventorySlot(), item.performAsyncActions(), item.getAction())))
                   .sync(() -> super.setCurrentPage(currentPage))
                   .execute();
        }
    }

}
