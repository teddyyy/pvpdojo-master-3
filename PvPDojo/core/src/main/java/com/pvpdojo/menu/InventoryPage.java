/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menu;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.bukkit.util.GuiUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class InventoryPage {

    protected InventoryMenu menu;
    protected String title;
    protected Map<Integer, Consumer<ClickType>> actions = new HashMap<>();
    protected int size;
    protected ItemStack[] items;
    protected boolean hasBar = false;

    public InventoryPage(InventoryMenu menu, String title, int size) {
        this.menu = menu;
        this.title = title;
        this.size = size;
        this.items = new ItemStack[size];
    }

    public Consumer<ClickType> getAction(int slot) {
        return actions.get(slot);
    }

    public InventoryMenu getMenu() {
        return menu;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getSize() {
        return size;
    }

    public void setSizeForNeededSlots(int neededSlots) {
        if (neededSlots % 9 == 0) {
            setSize(neededSlots);
        } else {
            setSize(neededSlots + (9 - neededSlots % 9));
        }
    }

    public void setSize(int size) {
        if (this.size != size) {
            this.items = new ItemStack[size];
        }
        this.size = size;
    }

    public ItemStack[] getContents() {
        return items;
    }

    public boolean hasBar() {
        return hasBar;
    }

    public void setHasBar(boolean hasBar) {
        this.hasBar = hasBar;
    }

    public ItemStack getItem(int pos) {
        if (pos < items.length) {
            return items[pos];
        } else {
            return null;
        }
    }

    public void setItem(int pos, ItemStack item, Consumer<ClickType> action) {
        actions.put(pos, action);
        items[pos] = item;
    }

    public Inventory asInventory() {
        Inventory inv = menu.getInventory();
        if (inv == null || inv.getSize() != size || !inv.getTitle().equals(title)) {
            inv = Bukkit.createInventory(null, size, title);
        }
        if (hasBar) {
            createBar();
        }
        inv.setMaxStackSize(1000);
        inv.setContents(items);

        return inv;
    }

    public void clear() {
        items = new ItemStack[size];
        actions.clear();
    }

    protected void createBar() {
        if (menu != null) {
            if (menu.hasBackButton()) {
                setItem(0, new ItemBuilder(Material.REDSTONE).name(CC.DARK_RED + ">       Back       <").build(), menu.getBackFunction());
            }
            if (menu.getDefaultActions() != null) {
                menu.getDefaultActions().accept(this);
            }
        }
    }

    // Fill Blank Inventory Spaces
    public void fillBlank() {
        if (hasBar) {
            fillBlank(9, GuiUtil.getBlankItem());
            fillBlank(GuiUtil.getBlankBar());
        } else {
            fillBlank(GuiUtil.getBlankItem());
        }
    }

    public void fillBlank(ItemStack item) {
        fillBlank(0, item);
    }

    public void fillBlank(int start) {
        fillBlank(start, GuiUtil.getBlankItem());
    }

    public void fillBlank(int start, ItemStack item) {
        fillBlank(start, items.length, item);
    }

    public void fillBlank(int start, int end) {
        fillBlank(start, end, GuiUtil.getBlankItem());
    }

    public void fillBlank(int start, int end, ItemStack item) {
        for (int i = start; i < end; i++) {
            if (items[i] == null || items[i].getType() == Material.AIR) {
                setItem(i, item, null);
            }
        }
    }
}