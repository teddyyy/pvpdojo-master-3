/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menu;

import java.util.HashMap;
import java.util.List;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.bukkit.util.GuiUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public abstract class PagedInventoryMenu extends InventoryMenu {

    protected int currentPage = 0;

    protected HashMap<Integer, InventoryPage> pages = new HashMap<>();

    public PagedInventoryMenu(Player player) {
        super(player);
    }

    @Override
    public InventoryMenu open(boolean redraw) {
        updatePageTurners(getCurrentPage());
        super.open(false);
        return this;
    }

    @Override
    public InventoryPage getCurrentPage() {
        return pages.get(currentPage);
    }

    public void setCurrentPage(int currentPage) {
        if (pages.containsKey(currentPage)) {
            this.currentPage = currentPage;
            updatePageTurners(getCurrentPage());
            super.open(false);
        }
    }

    public ItemStack getForward() {
        return new ItemBuilder(Material.STAINED_GLASS_PANE)
                .name(CC.GREEN + "Next Page")
                .lore("", CC.GRAY + "" + (currentPage + 1) + " / " + pages.size())
                .durability(DyeColor.LIME.getWoolData())
                .build();
    }

    public boolean hasForward() {
        return pages.containsKey(currentPage + 1);
    }

    public ItemStack getBack() {
        return new ItemBuilder(Material.STAINED_GLASS_PANE)
                .name(CC.GREEN + "Previous Page")
                .lore("", CC.GRAY + "" + (currentPage + 1) + " / " + pages.size())
                .durability(DyeColor.RED.getWoolData())
                .build();
    }

    public boolean hasBack() {
        return pages.containsKey(currentPage - 1);
    }

    public void setPage(int pageNumber, InventoryPage page) {
        pages.put(pageNumber, page);
        updatePageTurners(page);
    }

    public void setPages(List<InventoryPage> newPages) {
        pages.clear();
        for (int i = 0; i < newPages.size(); i++) {
            setPage(i, newPages.get(i));
        }
        if (currentPage >= pages.size()) {
            setCurrentPage(pages.size() - 1);
        }
    }

    public void updatePageTurners(InventoryPage page) {
        if (hasBack()) {
            page.setItem(27, this.getBack(), player -> setCurrentPage(currentPage - 1));
        } else {
            page.setItem(27, GuiUtil.getBlankItem(), null);
        }
        if (hasForward()) {
            page.setItem(35, this.getForward(), player -> setCurrentPage(currentPage + 1));
        } else {
            page.setItem(35, GuiUtil.getBlankItem(), null);
        }
    }

}
