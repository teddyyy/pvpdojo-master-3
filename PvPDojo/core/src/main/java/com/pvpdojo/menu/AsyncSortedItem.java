/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menu;

import java.util.function.Consumer;

import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

public abstract class AsyncSortedItem<K> extends SortedItem {

    private K key;
    private ItemStack cachedItem;

    public AsyncSortedItem(Consumer<ClickType> action, K key) {
        super(null, action);
        this.key = key;
    }

    public K getKey() {
        return key;
    }

    public ItemStack performAsyncActions() {
        return cachedItem != null ? cachedItem : (cachedItem = processItemAsync());
    }

    public abstract ItemStack processItemAsync();

}
