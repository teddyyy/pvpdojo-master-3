/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menu;

import java.util.function.Consumer;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.GuiUtil;
import com.pvpdojo.gui.InputGUI;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.DojoRunnable;

public abstract class InventoryMenu {

    private Player player;
    private InventoryMenu previousMenu;
    protected Inventory inv;
    protected Consumer<ClickType> backFunction;
    protected Consumer<InventoryPage> defaultActions;
    protected boolean closeable = true;
    protected boolean closeOnClick;
    protected boolean closing;
    protected boolean firstCall = true;
    protected boolean locked;
    protected String closeMessage = "";
    protected DojoRunnable updateTask;

    protected InventoryMenu(final Player player) {

        this.player = player;
        User user = User.getUser(getPlayer());

        if (user.getMenu() != null) {
            this.backFunction = p -> {
                if (previousMenu != null) {
                    previousMenu.redrawAndOpen();
                }
            };
        }

        this.updateTask = new DojoRunnable(() -> {
            if (user.isLoggedOut()) {
                updateTask.cancel();
                return;
            }
            InventoryMenu menu = user.getMenu();
            if (menu == this) {
                redrawAndOpen();
            } else if (menu == null) {
                updateTask.cancel();
            }
        });

    }

    public InventoryMenu redrawAndOpen() {
        return open(true);
    }

    public InventoryMenu open() {
        return open(false);
    }

    public InventoryMenu open(boolean redraw) {
        User user = User.getUser(getPlayer());
        if (previousMenu == null && user.getMenu() != null && user.getMenu() != this && user.getMenu().getPreviousMenu() != this) {
            this.setPreviousMenu(user.getMenu());
        }

        if (getPlayer() != null) {
            if (redraw || firstCall) {
                redraw();
                firstCall = false;
            }

            user.setMenu(this);

            DojoRunnable update = PvPDojo.schedule(() -> {
                if (!closing && user.getMenu() == this) {
                    inv = getCurrentPage().asInventory();
                    GuiUtil.openFast(getPlayer(), inv, this, false);
                    setLocked(false);
                }
            });

            if (user.getMenu() == this) {
                update.nextTick();
            } else {
                update.now();
            }

        }
        return this;
    }

    public Player getPlayer() {
        return player;
    }

    public Inventory getInventory() {
        return inv;
    }

    public boolean isDead() {
        return inv == null;
    }

    public boolean hasBackFunc() {
        return backFunction != null;
    }

    public Consumer<ClickType> getBackFunction() {
        return backFunction;
    }

    public void setCancelCloseMessage(String message) {
        this.closeMessage = message;
    }

    public void setBackFunction(Consumer<ClickType> function) {
        backFunction = function;
    }

    public boolean hasBackButton() {
        return hasBackFunc();
    }

    public boolean isCloseable() {
        return closeable;
    }

    public void setCloseable(boolean closeable) {
        this.closeable = closeable;
    }

    public InventoryMenu getPreviousMenu() {
        return previousMenu;
    }

    public void setPreviousMenu(InventoryMenu previousMenu) {
        this.previousMenu = previousMenu;
    }

    public void setDefaultActions(Consumer<InventoryPage> defaultActions) {
        this.defaultActions = defaultActions;
    }

    public Consumer<InventoryPage> getDefaultActions() {
        return defaultActions;
    }

    public boolean isCloseOnClick() {
        return closeOnClick;
    }

    public void setCloseOnClick(boolean closeOnClick) {
        this.closeOnClick = closeOnClick;
    }

    public void setUpdateInterval(int tick) {
        updateTask.cancel();

        if (tick > 0) {
            updateTask.createTimer(tick, -1);
        }
    }

    // If InventoryMenu is locked clicking on items has no effect
    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public String getCloseMessage() {
        return closeMessage;
    }

    public abstract InventoryPage getCurrentPage();

    public abstract void redraw();

    public void onClose() {}

    /**
     * From click context we have to call this
     */
    public void closeSafely() {
        if (!closing) {
            closing = true;
            setLocked(true);
            setCloseable(true);
            PvPDojo.schedule(() -> {
                if (player.getOpenInventory().getTopInventory().equals(inv)) {
                    player.closeInventory();
                }
            }).nextTick();
        }
    }

    public Consumer<ClickType> getString(String description, Consumer<String> consumer) {
        return clickType -> new InputGUI(description, string -> {
            consumer.accept(string);
            redrawAndOpen();
        }, this::open).open(getPlayer());
    }

}