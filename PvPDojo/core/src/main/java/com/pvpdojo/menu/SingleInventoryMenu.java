/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menu;

import org.bukkit.entity.Player;

public abstract class SingleInventoryMenu extends InventoryMenu {
    protected InventoryPage page;

    public SingleInventoryMenu(Player player, String title, int size) {
        super(player);

        page = new InventoryPage(this, title, size);
    }

    @Override
    public void redraw() {
        page.clear();
    }


    public InventoryPage getCurrentPage() {
        return page;
    }
}
