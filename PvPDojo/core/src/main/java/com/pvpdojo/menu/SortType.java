/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menu;

public enum SortType {
    HIGHEST_LOWEST_PRICE("Highest To Lowest Price"),
    LOWEST_HIGHEST_PRICE("Lowest To Highest Price"),
    ALPHABETICAL("Alphabetical"),
    RARITY("Rarity"),
    NEW_TO_OLD("Newest to Oldest"),
    OLD_TO_NEW("Oldest to Newest"),
    NONE("none");

    String name;

    SortType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
