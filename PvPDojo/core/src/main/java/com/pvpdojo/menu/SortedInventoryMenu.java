/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menu;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.bukkit.util.GuiUtil;
import com.pvpdojo.gui.InputGUI;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public abstract class SortedInventoryMenu extends PagedInventoryMenu {

    protected List<SortedItem> sortcache = new ArrayList<>();
    protected List<SortedItem> toProcess = sortcache;
    protected Predicate<SortedItem> searchFilter;
    protected int size;
    private String title;
    private SortType[] sorter;
    private int current;

    public SortedInventoryMenu(Player player, String title, int size, SortType... sort) {
        super(player);
        this.title = title;
        this.size = size;
        this.sorter = sort;
        if (sorter.length == 0) {
            sorter = new SortType[]{SortType.NONE};
        }
        current = 0;
    }

    public void addSortItem(SortedItem item) {
        this.sortcache.add(item);
    }

    public void sort() {
        if (sorter[current] != SortType.NONE) {
            sortcache.sort((sort1, sort2) -> {
                if (sorter[current].equals(SortType.ALPHABETICAL)) {
                    return sort1.getSortName().compareTo(sort2.getSortName());
                } else if (sorter[current] == SortType.LOWEST_HIGHEST_PRICE || sorter[current] == SortType.OLD_TO_NEW) {
                    return Double.compare(sort1.getSortValue(), sort2.getSortValue());
                } else if (sorter[current] == SortType.HIGHEST_LOWEST_PRICE || sorter[current] == SortType.NEW_TO_OLD || sorter[current] == SortType.RARITY) {
                    return Double.compare(sort2.getSortValue(), sort1.getSortValue());
                }
                return 0;
            });
        }
        pages.clear();
        this.fillPages();
        if (!pages.containsKey(currentPage)) {
            currentPage = 0;
        }
        setCurrentPage(currentPage);
    }

    public void fillPages() {

        toProcess = searchFilter != null ? sortcache.stream().filter(searchFilter).collect(Collectors.toList()) : sortcache;

        int size = toProcess.size();
        int slotsUnderBar = this.size - 9;
        int itemsPerPage = slotsUnderBar - 2 * (slotsUnderBar / 9);
        int numberPages = (int) Math.ceil((double) size / (double) itemsPerPage);
        numberPages = numberPages == 0 ? 1 : numberPages;
        for (int i = 0; i < numberPages; i++) {
            InventoryPage page;
            if (this.pages.containsKey(i)) {
                page = this.pages.get(i);
            } else {
                page = new InventoryPage(this, title, this.size);
                this.pages.put(i, page);
            }
            page.setHasBar(true);
            if (sorter.length > 1) {
                page.setItem(3, new ItemBuilder(Material.TRAP_DOOR).name(CC.AQUA + "Sort Type").lore(CC.GRAY + "Click to change the sort type").build(), type -> {
                    current++;
                    if (current > (sorter.length - 1)) {
                        current = 0;
                    }
                    this.sort();
                    getPlayer().sendMessage(CC.GRAY + "Sorting by: " + CC.GREEN + sorter[current].getName());
                });
            }
            page.setItem(sorter.length > 1 ? 5 : 4, new ItemBuilder(Material.COMPASS).name(CC.RED + "Search").build(), clickType -> openSearchWindow());
        }
        int x = 0;
        int y = 8;
        for (SortedItem aSortcache : toProcess) {
            y++;
            y = GuiUtil.getNoEdgePattern(y);
            if (y >= this.size) {
                x++;
                y = 10;
            }
            InventoryPage page = this.pages.get(x);
            ItemStack itemstack = aSortcache.getItemStack();
            if (page.getItem(y) != null) {
                if (page.getItem(y).equals(itemstack)) {
                    return;
                }
            }
            aSortcache.setInventorySlot(y);
            page.setItem(y, itemstack, aSortcache.getAction());
        }
        pages.values().forEach(InventoryPage::fillBlank);
    }

    public void openSearchWindow() {
        new InputGUI(CC.RED + "Insert your search query", input -> {
            Consumer<ClickType> prevBackFunction = getBackFunction();
            Predicate<SortedItem> prevSearchFilter = searchFilter;
            setBackFunction(clickType -> {
                searchFilter = prevSearchFilter;
                setBackFunction(prevBackFunction);
                open();
            });
            searchFilter = sortedItem -> sortedItem.getSortName().toLowerCase().contains(input.toLowerCase());
            open();
        }, this::open).open(getPlayer());
    }

    @Override
    public void redraw() {
        sortcache.clear();
    }

    @Override
    public InventoryMenu open(boolean redraw) {
        if (redraw || firstCall) {
            redraw();
        }
        firstCall = false;
        sort();
        return this;
    }

}
