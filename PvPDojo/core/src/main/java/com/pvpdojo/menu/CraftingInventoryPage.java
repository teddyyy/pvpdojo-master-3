/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menu;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;

import com.pvpdojo.bukkit.util.GuiUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class CraftingInventoryPage extends InventoryPage {

    public CraftingInventoryPage(InventoryMenu menu, String title) {
        super(menu, title, 10);
    }

    @Override
    public Inventory asInventory() {
        Inventory inv = menu.getInventory();
        if (inv == null || inv.getSize() != size || !inv.getTitle().equals(title)) {
            inv = Bukkit.createInventory(null, InventoryType.WORKBENCH, title);
        }
        if (hasBar) {
            createBar();
        }
        inv.setMaxStackSize(1000);
        inv.setContents(items);

        return inv;
    }

    @Override
    protected void createBar() {
        if (menu != null) {
            if (menu.hasBackButton()) {
                setItem(1, new ItemBuilder(Material.REDSTONE).name(CC.DARK_RED + ">       Back       <").build(), menu.getBackFunction());
            }
            if (menu.getDefaultActions() != null) {
                menu.getDefaultActions().accept(this);
            }
        }
    }

    @Override
    public void setSizeForNeededSlots(int neededSlots) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setSize(int size) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void fillBlank() {
        if (hasBar) {
            fillBlank(4, GuiUtil.getBlankItem());
            fillBlank(1, GuiUtil.getBlankBar());
        } else {
            fillBlank(1, GuiUtil.getBlankItem());
        }
    }
}
