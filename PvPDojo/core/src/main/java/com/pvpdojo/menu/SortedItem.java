/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.menu;

import java.util.function.Consumer;

import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

public class SortedItem {
    private String sortName = "";
    private double sortValue;
    private ItemStack item;
    private Consumer<ClickType> action;
    private int inventorySlot;

    public SortedItem(ItemStack item, Consumer<ClickType> action) {
        this.item = item;
        this.action = action;
    }

    public SortedItem setSortName(String sort) {
        this.sortName = sort;
        return this;
    }

    public String getSortName() {
        return sortName;
    }

    public double getSortValue() {
        return sortValue;
    }

    public void setSortValue(double value) {
        this.sortValue = value;
    }

    public void setItemStack(ItemStack item) {
        this.item = item;
    }

    public ItemStack getItemStack() {
        return item;
    }

    public Consumer<ClickType> getAction() {
        return action;
    }

    public void setAction(Consumer<ClickType> action) {
        this.action = action;
    }

    public int getInventorySlot() {
        return inventorySlot;
    }

    public void setInventorySlot(int inventorySlot) {
        this.inventorySlot = inventorySlot;
    }
}
