/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public abstract class RegionEvent extends PlayerEvent {
    private static final HandlerList handlerList = new HandlerList();
    private ProtectedRegion region;
    private MovementWay movement;
    private CancelResult cancelResult;

    public RegionEvent(ProtectedRegion region, Player player, MovementWay movement) {
        super(player);
        this.region = region;
        this.movement = movement;
        this.cancelResult = CancelResult.PUSH;
    }

    public HandlerList getHandlers() {
        return handlerList;
    }

    public ProtectedRegion getRegion() {
        return this.region;
    }

    public static HandlerList getHandlerList() {
        return handlerList;
    }

    public MovementWay getMovementWay() {
        return this.movement;
    }

    public CancelResult getCancelResult() {
        return cancelResult;
    }

    public void setCancelResult(CancelResult cancelResult) {
        this.cancelResult = cancelResult;
    }

    public enum CancelResult {
        PUSH,
        TELEPORT
    }

    public enum MovementWay {
        MOVE,
        TELEPORT,
        SPAWN,
        DISCONNECT
    }

}
