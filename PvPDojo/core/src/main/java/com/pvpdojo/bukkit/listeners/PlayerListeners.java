/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.listeners;

import static com.pvpdojo.ServerType.GAME;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent.RegainReason;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerAchievementAwardedEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerChangeLocaleEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTrackEntityEvent;
import org.bukkit.event.player.PlayerUntrackEntityEvent;
import org.bukkit.event.vehicle.VehicleMoveEvent;
import org.bukkit.util.StringUtil;
import org.bukkit.util.Vector;
import org.spigotmc.SpigotConfig;
import org.spigotmc.event.entity.EntityMountEvent;

import com.destroystokyo.paper.event.server.AsyncTabCompleteEvent;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.pvpdojo.DBCommon;
import com.pvpdojo.Discord;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.bukkit.events.DojoEvent;
import com.pvpdojo.bukkit.events.DojoEventExecutor;
import com.pvpdojo.bukkit.events.DojoPlayerClickBlockEvent;
import com.pvpdojo.bukkit.events.DojoPlayerJoinEvent;
import com.pvpdojo.bukkit.events.DojoPlayerSoupEvent;
import com.pvpdojo.bukkit.events.EntityFallEvent;
import com.pvpdojo.bukkit.events.ListenType;
import com.pvpdojo.bukkit.events.RegionEnterEvent;
import com.pvpdojo.bukkit.events.RegionEvent.CancelResult;
import com.pvpdojo.bukkit.events.RegionEvent.MovementWay;
import com.pvpdojo.bukkit.events.RegionLeaveEvent;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.bukkit.util.TrackedObject;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.settings.ServerSwitchSettings;
import com.pvpdojo.settings.SettingsType;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.userdata.impl.UserImpl;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.PunishmentManager;
import com.pvpdojo.util.PunishmentManager.Punishment;
import com.pvpdojo.util.PunishmentManager.PunishmentType;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.VectorUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder.ItemEditor;
import com.pvpdojo.util.bukkit.PlayerUtil;
import com.sk89q.worldguard.bukkit.RegionQuery;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class PlayerListeners implements DojoListener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPreCommand(PlayerCommandPreprocessEvent e) {
        if (PlayerUtil.hasSoltismus(e.getPlayer())) {
            Discord.stasi().sendMessage(e.getPlayer().getName() + " command: " + e.getMessage()).complete();
        }
    }

    @EventHandler
    public void onPlayerAchievementAwardedEvent(PlayerAchievementAwardedEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerBedEnterEvent(PlayerBedEnterEvent e) {
        e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onDeath(PlayerDeathEvent e) {
        e.setDeathMessage(null);
        Player victim = e.getEntity();
        User user = User.getUser(e.getEntity());

        if (!user.isDeathDrops()) {
            e.getDrops().clear();
        }

        if (user.isAutoRespawn()) {
            PvPDojo.schedule(e.getEntity().spigot()::respawn).createTask(25);
        }

        NMSUtils.strikeLightningEffect(victim);
        TrackedObject.getTrackedObjects().forEach(trackedObject -> trackedObject.updateFor(victim));

    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerTeleportEvent(PlayerTeleportEvent e) {
        final Player player = e.getPlayer();
        User user = User.getUser(player);

        if (player.getVehicle() == null) {
            updateRegions(player, MovementWay.TELEPORT, e.getTo());
        }

        if (e.getTo().getWorld() != user.getLastTrackedObjectsUpdate().getWorld() || e.getTo().distanceSquared(user.getLastTrackedObjectsUpdate()) > 36) {
            PvPDojo.schedule(() -> TrackedObject.getTrackedObjects().forEach(obj -> obj.updateFor(player))).nextTick();
            user.setLastTrackedObjectsUpdate(e.getTo());
        }

    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onRespawn(PlayerRespawnEvent e) {
        // After respawn we always reenter every region, so we have to clear the old ones
        User.getUser(e.getPlayer()).getApplicableRegions().clear();

        PvPDojo.schedule(() -> TrackedObject.getTrackedObjects().forEach(trackedObject -> trackedObject.updateFor(e.getPlayer()))).nextTick();
        updateRegions(e.getPlayer(), MovementWay.SPAWN, e.getRespawnLocation());
    }

    private boolean updateRegions(Player player, MovementWay movement, Location to) {
        User pd = User.getUser(player);

        if (movement == MovementWay.DISCONNECT) {
            Set<ProtectedRegion> regions = pd.getApplicableRegions();
            if (regions != null) {
                for (ProtectedRegion region : regions) {
                    RegionLeaveEvent leaveEvent = new RegionLeaveEvent(region, player, MovementWay.DISCONNECT);
                    Bukkit.getPluginManager().callEvent(leaveEvent);
                }
                regions.clear();
            }
            return true;
        }

        Set<ProtectedRegion> regions = new HashSet<>(pd.getApplicableRegions());
        RegionQuery rm = WGBukkit.getPlugin().getRegionContainer().createQuery();
        if (rm == null)
            return true;

        Set<ProtectedRegion> appRegions = rm.getApplicableRegions(to).getRegions();

        Iterator<ProtectedRegion> itr = regions.iterator();
        while (itr.hasNext()) {
            ProtectedRegion region = itr.next();
            if (!appRegions.contains(region)) {
                RegionLeaveEvent e = new RegionLeaveEvent(region, player, movement);
                Bukkit.getPluginManager().callEvent(e);
                if (e.isCancelled()) {
                    regions.clear();
                    regions.addAll(pd.getApplicableRegions());

                    if (movement == MovementWay.MOVE || movement == MovementWay.TELEPORT) {
                        if (e.getCancelResult() == CancelResult.PUSH) {
                            Vector maxpoint = new Vector(region.getMaximumPoint().getX(), region.getMaximumPoint().getY(), region.getMaximumPoint().getZ());
                            Vector midpoint = VectorUtil.getMidpoint(region.getMinimumPoint(), region.getMaximumPoint());
                            Vector push = midpoint.subtract(to.toVector()).normalize().setY((maxpoint.getY() < to.getY()) ? -0.5 : 0.3);
                            if (player.getVehicle() != null) {
                                player.getVehicle().setVelocity(push);
                            } else {
                                player.setVelocity(push);
                            }
                        } else {
                            Location loc = pd.getLastValidLocation();
                            loc.setYaw(to.getYaw());
                            loc.setPitch(to.getPitch());
                            if (player.getVehicle() != null) {
                                player.getVehicle().teleport(loc);
                            } else {
                                player.teleport(loc);
                            }
                        }
                    }
                    return false;
                }
                itr.remove();
            }
        }

        for (ProtectedRegion region : appRegions) {
            if (!regions.contains(region)) {
                RegionEnterEvent e = new RegionEnterEvent(region, player, movement);
                Bukkit.getPluginManager().callEvent(e);
                if (e.isCancelled()) {

                    regions.clear();
                    regions.addAll(pd.getApplicableRegions());

                    if (movement == MovementWay.MOVE || movement == MovementWay.TELEPORT) {
                        if (e.getCancelResult() == CancelResult.PUSH) {
                            Vector maxpoint = VectorUtil.toBukkitVector(region.getMaximumPoint());
                            Vector midpoint = VectorUtil.getMidpoint(region.getMinimumPoint(), region.getMaximumPoint());
                            Vector push = to.toVector().subtract(midpoint).normalize().multiply(0.75).setY((maxpoint.getY() < to.getY()) ? -0.5 : 0.3);
                            if (player.getVehicle() != null) {
                                player.getVehicle().setVelocity(push);
                            } else {
                                player.setVelocity(push);
                            }
                        } else {
                            Location loc = pd.getLastValidLocation();
                            loc.setYaw(to.getYaw());
                            loc.setPitch(to.getPitch());
                            if (player.getVehicle() != null) {
                                player.getVehicle().teleport(loc);
                            } else {
                                player.teleport(loc);
                            }
                        }
                    }
                    return false;
                }
                regions.add(region);
            }
        }
        if (movement == MovementWay.MOVE || movement == MovementWay.SPAWN) {
            pd.setLastValidLocation(to);
        }
        pd.setApplicableRegions(regions);
        return true;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onSoup(PlayerInteractEvent e) {
        Player p = e.getPlayer();

        if (!PvPDojo.get().getServerSettings().isSoup()
                || (PvPDojo.get().getServerSettings().getSoupCondition() != null && !PvPDojo.get().getServerSettings().getSoupCondition().test(p))) {
            return;
        }

        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            trySoup(p);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onSoupEntity(PlayerInteractEntityEvent e) {
        Player p = e.getPlayer();

        if (!PvPDojo.get().getServerSettings().isSoup()
                || (PvPDojo.get().getServerSettings().getSoupCondition() != null && !PvPDojo.get().getServerSettings().getSoupCondition().test(p))) {
            return;
        }

        trySoup(p);
    }


    private void trySoup(Player p) {
        if (p.getItemInHand().getType() == Material.MUSHROOM_SOUP) {

            Bukkit.getPluginManager().callEvent(new DojoPlayerSoupEvent(p));

            if ((p.getHealth() < p.getMaxHealth()) && (p.getHealth() > 0)) {
                if (p.getHealth() <= p.getMaxHealth() - 7) {
                    p.setHealth(p.getHealth() + 7);
                    soup(p);
                } else if ((p.getHealth() < p.getMaxHealth()) && (p.getHealth() > p.getMaxHealth() - 7)) {
                    p.setHealth(p.getMaxHealth());
                    soup(p);
                }
            } else if (p.getFoodLevel() < 20) {
                if (p.getFoodLevel() <= 13) {
                    p.setFoodLevel(p.getFoodLevel() + 7);
                    p.setSaturation(p.getSaturation() + 10);
                    soup(p);
                } else if (p.getFoodLevel() > 20 - 7) {
                    p.setFoodLevel(20);
                    p.setSaturation(p.getSaturation() + 10);
                    soup(p);
                }
            }
        }
    }

    private void soup(Player p) {
        Bukkit.getPluginManager().callEvent(new PlayerItemConsumeEvent(p, p.getItemInHand()));
        new ItemEditor(p.getItemInHand()).name(null).type(Material.BOWL).build();
    }

    private Cache<UUID, ServerSwitchSettings> switchSettings = CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.MINUTES).build();

    @EventHandler
    public void onLogin(PlayerLoginEvent e) {
        if (SpigotConfig.bungee && !e.getRealAddress().isAnyLocalAddress() && !e.getRealAddress().isLoopbackAddress()) {
            e.setResult(PlayerLoginEvent.Result.KICK_BANNED);
        }
    }

    @EventHandler
    public void onAsyncPreLoginEvent(AsyncPlayerPreLoginEvent e) {
        Punishment ban;
        try {
            if ((ban = PunishmentManager.getActivePunishment(e.getUniqueId(), PunishmentType.BAN)) != null) {
                e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED, "" + CC.RED + CC.STRIKETHROUGH + "\n-------------\n" + CC.RED + "You are banned from the server.\n\nReason: "
                        + ban.getOriginalReason() + "\nRemaining time: " + ban.getRemainingTime() + "\n\n" + CC.YELLOW
                        + "Appeal on pvpdojo.com/discord\nSend this message to DojoBot '" + CC.RED + "!appeal " + Integer.toHexString(ban.getId()) + CC.YELLOW + "'"
                        + "\n" + CC.RED + CC.STRIKETHROUGH + "-------------");
                return;
            }
        } catch (SQLException ex) {
            Log.exception("Handle Punishments for" + e.getUniqueId(), ex);
            e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, CC.RED + "Internal error: " + ex.getMessage());
        }
        ServerSwitchSettings settings = DBCommon.loadSettings(SettingsType.SERVER_SWITCH, e.getUniqueId());
        DBCommon.deleteSettings(SettingsType.SERVER_SWITCH, e.getUniqueId());
        if (settings != null) {
            switchSettings.put(e.getUniqueId(), settings);
        }
    }

    @EventHandler
    public void onAsyncPlayerChatEvent(AsyncPlayerChatEvent event) {

        Player player = event.getPlayer();
        User user = User.getUser(player);

        if (PunishmentManager.handleMutedPlayer(player)) {
            event.setCancelled(true);
            return;
        }

        if (!user.getRank().inheritsRank(Rank.BUILDER) && user.getCooldownSettings().getChatThrottle().throttle()) {
            event.setCancelled(true);
            user.sendMessage(MessageKeys.SLOW_DOWN_CHAT);
            return;
        }

        if (!user.getRank().inheritsRank(Rank.BUILDER) && user.getCooldownSettings().checkSimilarChatMessage(event.getMessage())) {
            event.setCancelled(true);
            user.sendMessage(MessageKeys.SIMILAR_MESSAGE);
            return;
        }

        Rank displayRank = user.getSpoofedRank();

        if (displayRank == null) {
            displayRank = Rank.DEFAULT;
        }

        String messageStr = event.getMessage();

        if (displayRank.inheritsRank(PvPDojo.SERVER_TYPE == GAME ? Rank.BUILDER : Rank.BLACKBELT)) {
            messageStr = CC.translateAlternateColorCodes('&', messageStr);
            if (!displayRank.inheritsRank(Rank.BUILDER)) {
                messageStr = CC.stripMagic(messageStr);
            }
        }

        // Quick channel access
        ChatChannel channel = user.getChatChannel();
        ChatChannel activationChannel = ChatChannel.getByActivation(messageStr);

        if (activationChannel != null && channel != activationChannel) {
            channel = activationChannel;
            messageStr = messageStr.substring(1);
        }

        if (!channel.getCondition().apply(user)) {
            event.setCancelled(true);
            user.sendMessage(PvPDojo.WARNING + "Your chat channel has no target use /c to switch to global chat");
            return;
        }

        event.setMessage(messageStr);

        if (PlayerUtil.hasSoltismus(player)) {
            Discord.stasi().sendMessage("[" + channel + "] " + player.getName() + ": " + messageStr).queue();
        }

        String info = "none";
        switch (channel) {
            case CLAN:
                info = user.getClan().getName();
                event.setFormat(user.getSpoofedRank().getPrefix() + "%s" + PvPDojo.POINTER + CC.RESET + "%s");
                break;
            case GLOBAL:
                info = "chat";
                if (BungeeSync.isGlobalMute() && !user.getRank().inheritsRank(Rank.TRIAL_MODERATOR)) {
                    player.sendMessage(PvPDojo.WARNING + "The global chat is currently muted");
                    event.setCancelled(true);
                    return;
                }
                event.setFormat((user.getClan() != null && !player.isNicked() ? CC.DARK_GRAY + "[" + CC.AQUA + user.getClan().getName() + CC.DARK_GRAY + "] " : "")
                        + displayRank.getPrefix() + "%s" + PvPDojo.POINTER + displayRank.getMessageColor() + "%s");
                break;
            case STAFF:
                event.setFormat(CC.LIGHT_PURPLE + "[" + CC.DARK_PURPLE + "SC" + CC.LIGHT_PURPLE + "] " + user.getRank().getPrefix() + "%s" + PvPDojo.POINTER + CC.YELLOW + "%s");
                break;
            case PARTY:
                info = user.getParty().getPartyId().toString();
                event.setFormat(user.getSpoofedRank().getPrefix() + "%s" + PvPDojo.POINTER + CC.RESET + "%s");
                break;
            default:
                user.sendMessage(PvPDojo.WARNING + "You are in an invalid chat channel");
                break;
        }

        if (PvPDojo.SERVER_TYPE == GAME && channel == ChatChannel.GLOBAL) {
            String prefix = null;
            if (user.getTeam() != null && user.getTeam().getColor() != null) {
                prefix = "[" + user.getTeam().getName() + CC.WHITE + "] ";
            } else if (user.getClan() != null && !player.isNicked()) {
                prefix = "[" + CC.GRAY + user.getClan().getName() + CC.WHITE + "] ";
            }
            event.setFormat((prefix != null ? prefix : "<") + displayRank.getPrefix() + "%s" + CC.RESET + "> " + "%s");
        } else {
            event.setCancelled(true);
            String formattedChatMessage = String.format(event.getFormat(), channel == ChatChannel.STAFF ? player.getName() : player.getNick(), event.getMessage());
            String message = StringUtils.toBase64(formattedChatMessage);
            BungeeSync.sendToChannel(channel, info, message);
            Log.info(CC.stripColor(formattedChatMessage));
        }

        // Chat Log
        Collection<UUID> recipients = null;

        switch (channel) {
            case GLOBAL:
                if (PvPDojo.SERVER_TYPE == GAME) {
                    recipients = event.getRecipients().stream()
                                      .map(Entity::getUniqueId)
                                      .collect(Collectors.toList());
                } else {
                    recipients = BungeeSync.getOnlinePlayersUUID().stream()
                                           .filter(uuid -> !BungeeSync.findPlayer(uuid).contains("game"))
                                           .collect(Collectors.toList());
                }
                break;
            case STAFF:
                recipients = BungeeSync.getOnlinePlayersUUID().stream()
                                       .map(PvPDojo::getOfflinePlayer)
                                       .peek(off -> off.getPersistentData().pullUserDataSneaky())
                                       .filter(off -> off.getPersistentData().getRank().inheritsRank(Rank.BUILDER))
                                       .map(OfflinePlayer::getUniqueId)
                                       .collect(Collectors.toList());
                break;
            case PARTY:
                recipients = user.getParty().getUsers();
                break;
            case CLAN:
                recipients = user.getClan().getAllMembers();
                break;
        }

        if (recipients != null) {

            String logCh = PvPDojo.SERVER_TYPE == GAME && channel == ChatChannel.GLOBAL ? "GAME" : channel.toString();
            DBCommon.logChatMessage(user.getUUID(), recipients, "[" + logCh + "] " + CC.stripColor(event.getMessage()));
        }

    }

    @EventHandler
    public void onPlayerInteractEntityEvent(final PlayerInteractEntityEvent e) {
        Player player = e.getPlayer();
        User user = User.getUser(player);

        if (user.getHotbarGui() != null && user.getHotbarGui().getEntityAction(player.getItemInHand().getType()) != null) {
            user.getHotbarGui().getEntityAction(e.getPlayer().getItemInHand().getType()).accept(player, Action.RIGHT_CLICK_AIR, e.getRightClicked());
            if (user.getHotbarGui().isCancel()) {
                e.setCancelled(true);
            }
        }

    }

    @EventHandler
    public void onPlayerClickEvent(final PlayerInteractEvent e) {
        final Player player = e.getPlayer();
        User user = User.getUser(player);

        if (user.getHotbarGui() != null && user.getHotbarGui().getAction(e.getMaterial()) != null) {
            user.getHotbarGui().getAction(e.getMaterial()).accept(player, e.getAction());
            if (user.getHotbarGui().isCancel()) {
                e.setCancelled(true);
            }
        }

        if (!user.isAdminMode()) {
            if (e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getClickedBlock() != null
                    && PvPDojo.get().getServerSettings().getBlockedInteraction().contains(e.getClickedBlock().getType())
                    && !user.getBypassBlockedInteraction().contains(e.getClickedBlock().getType())) {
                e.setCancelled(true);
            }
        }

        if (e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getClickedBlock().getType() == Material.FENCE) {
            e.setCancelled(true);
        }

        if (e.getClickedBlock() != null && (e.getAction().equals(Action.LEFT_CLICK_BLOCK) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))) {

            DojoEvent.executeEvent(ListenType.PLAYER_CLICK_BLOCK_EVENT, new DojoEventExecutor<DojoPlayerClickBlockEvent>() {
                @Override
                public void preExecute(DojoPlayerClickBlockEvent event) {
                    event.player = player;
                    event.block = e.getClickedBlock();
                    event.item = e.getItem();
                    event.setCancelled(false);
                }

                @Override
                public void postExecute(DojoPlayerClickBlockEvent event) {
                    if (event.isCancelled()) {
                        e.setUseInteractedBlock(Result.DENY);
                    }
                    event.player = null;
                    event.block = null;
                    event.item = null;
                }
            });
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerJoinEvent(final PlayerJoinEvent e) {
        Player player = e.getPlayer();
        User user = User.loginUser(player.getUniqueId());

        user.setLastTrackedObjectsUpdate(player.getLocation());

        ServerSwitchSettings settings = switchSettings.getIfPresent(player.getUniqueId());

        if (settings != null) {
            settings.applyFirst(user);
        }

        user.setUseStats(PvPDojo.get().getServerSettings().isStats());
        user.sendMessage(MessageKeys.SERVER_CHANGE, "{server}", PvPDojo.SERVER_TYPE.getName());

        DojoPlayerJoinEvent dojoEvent = new DojoPlayerJoinEvent(e.getPlayer(), e.getJoinMessage(), settings);
        Bukkit.getPluginManager().callEvent(dojoEvent);
        e.setJoinMessage(dojoEvent.getJoinMessage());

        if (!PvPDojo.get().getServerSettings().isAllowJoinMessage()) {
            e.setJoinMessage(null);
        }

        if (settings != null) {
            settings.applyAfter(user);
        }

        PvPDojo.schedule(() -> TrackedObject.getTrackedObjects().forEach(obj -> obj.updateFor(player))).nextTick();

        if (!user.isAdminMode()) {
            BukkitUtil.getWorldEdit().getSession(player).setToolControl(false);
        }

        updateRegions(player, MovementWay.SPAWN, player.getLocation());
    }

    @EventHandler
    public void onLocaleChange(PlayerChangeLocaleEvent e) {
        ((UserImpl) User.getUser(e.getPlayer())).setLocaleReceived();
    }

    @EventHandler
    public void onTabComplete(AsyncTabCompleteEvent e) {
        if (!e.isCommand()) {
            String lastToken = e.getBuffer().indexOf(' ') < 0 ? e.getBuffer() : e.getBuffer().substring(e.getBuffer().indexOf(' ') + 1);
            e.getCompletions().addAll(BungeeSync.getOnlinePlayers()
                                                .stream()
                                                .filter(name -> StringUtil.startsWithIgnoreCase(name, lastToken))
                                                .sorted(String.CASE_INSENSITIVE_ORDER)
                                                .collect(Collectors.toList()));
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onQuit(PlayerQuitEvent e) {
        User.logoutUser(e.getPlayer());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuitEvent(final PlayerQuitEvent e) {
        TrackedObject.getTrackedObjects().forEach(obj -> obj.getViewers().remove(e.getPlayer()));

        if (PvPDojo.SERVER_TYPE != ServerType.NONE && !PvPDojo.get().getServerSettings().isAllowQuitMessage()) {
            e.setQuitMessage(null);
        }

        updateRegions(e.getPlayer(), MovementWay.DISCONNECT, e.getPlayer().getLocation());
    }

    @EventHandler
    public void onHealthRegen(EntityRegainHealthEvent e) {
        if (!PvPDojo.get().getServerSettings().isNaturalRegeneration()) {
            if (e.getEntityType() == EntityType.PLAYER && e.getRegainReason() == RegainReason.SATIATED) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onTrack(PlayerTrackEntityEvent e) {
        if (e.getTracked() instanceof Player) {
            User.getUser((Player) e.getTracked()).updateUserFor(User.getUser(e.getPlayer()), true);
        }
    }

    @EventHandler
    public void onUntrack(PlayerUntrackEntityEvent e) {
        if (e.getTracked() instanceof Player && User.containsUser(e.getTracked().getUniqueId()) && User.containsUser(e.getPlayer().getUniqueId())) {
            User.getUser((Player) e.getTracked()).removeUserFor(User.getUser(e.getPlayer()));
        }
    }

    @EventHandler
    public void onFall(EntityFallEvent e) {
        if (e.getEntity() instanceof Player) {
            User user = User.getUser((Player) e.getEntity());
            boolean cancel = user.isCancelNextFall();
            if (cancel) {
                user.setCancelNextFall(false);
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlayerMoveEvent(final PlayerMoveEvent e) {
        Player player = e.getPlayer();
        User user = User.getUser(player);

        if (e.getFrom().getBlock() != e.getTo().getBlock()) {
            user.resetNoMoveSeconds();
        }

        if (e.getTo().getWorld() != user.getLastTrackedObjectsUpdate().getWorld() || e.getTo().distanceSquared(user.getLastTrackedObjectsUpdate()) > 16) {
            TrackedObject.getTrackedObjects().forEach(obj -> obj.updateFor(player));
            user.setLastTrackedObjectsUpdate(player.getLocation());
        }

        if (user.isCancelMove()) {
            player.setVelocity(new Vector());
        }

        if (!e.getFrom().getBlock().equals(e.getTo().getBlock()) || user.isCheckNextMove()) {
            user.setCheckNextMove(!updateRegions(player, MovementWay.MOVE, e.getTo()));
            if (PvPDojo.get().getServerSettings().getLaunchMaterials().contains(e.getTo().getBlock().getRelative(BlockFace.DOWN).getType())) {
                player.setVelocity(new Vector(0, 2.8, 0));
                user.cancelNextFall();
                player.playSound(player.getLocation(), Sound.CHEST_OPEN, (float) 5, 5);

            } else if (user.isCancelNextFall() && e.isOnGround()) {
                PvPDojo.schedule(() -> user.setCancelNextFall(false)).nextTick();
            }
        }
    }

    @EventHandler
    public void onVehicleMove(VehicleMoveEvent e) {
        if (e.getVehicle().getPassenger() != null && e.getVehicle().getPassenger() instanceof Player) {
            User user = User.getUser((Player) e.getVehicle().getPassenger());

            if (!e.getFrom().getBlock().equals(e.getTo().getBlock()) || user.isCheckNextMove()) {
                user.setCheckNextMove(!updateRegions(user.getPlayer(), MovementWay.MOVE, e.getTo()));
            }
        }
    }

    @EventHandler
    public void onVehicleMount(EntityMountEvent e) {
        if (e.getEntity() instanceof Player && ((Player) e.getEntity()).isFrozen()) {
            e.setCancelled(true);
        }
    }

}
