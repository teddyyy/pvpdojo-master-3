/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.util;

import org.bukkit.entity.Player;

import co.aikar.taskchain.TaskChain;
import co.aikar.taskchain.TaskChainAbortAction;

public class ActionHandlers {

    public static final TaskChainAbortAction<Player, String, Void> MESSAGE = new TaskChainAbortAction<Player, String, Void>() {
        @Override
        public void onAbort(TaskChain<?> chain, Player player, String msg) {
            player.sendMessage(msg);
        }
    };

}
