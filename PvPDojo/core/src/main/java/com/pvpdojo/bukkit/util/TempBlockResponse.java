/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.util;

import org.bukkit.Location;
import org.bukkit.Material;

public abstract class TempBlockResponse extends TempBlock {

    public TempBlockResponse(Location location, Material fake, byte data, long seconds) {
        super(location, fake, data, seconds);
    }

    public abstract void onDespawn(Location location);

    @Override
    public void reset() {
        super.reset();
        this.onDespawn(this.loc);
    }
}