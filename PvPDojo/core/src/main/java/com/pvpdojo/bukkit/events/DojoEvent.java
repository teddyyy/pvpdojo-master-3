/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.events;

import java.util.HashMap;

public abstract class DojoEvent {

    private static final HashMap<ListenType, DojoEventHolder> registry = new HashMap<>();

    @SuppressWarnings("unchecked")
    public static <T extends DojoEvent> void executeEvent(ListenType type, DojoEventExecutor<T> execute) {
        DojoEventHolder holder = DojoEvent.getEventHolder(type);
        if (holder == null)
            return;
        for (DojoEvent event : holder.getEvents()) {
            execute.preExecute((T) event);
            if (event.canExecute()) {
                try {
                    event.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                execute.postExecute((T) event);
            }
        }
    }

    public static void register(DojoEvent event) {
        ListenType type = event.getType();
        DojoEventHolder holder = registry.get(type);
        if (holder == null) {
            holder = new DojoEventHolder();
            registry.put(type, holder);
        }
        holder.addEvent(event);
    }

    public static DojoEventHolder getEventHolder(ListenType type) {
        DojoEventHolder holder = registry.get(type);
        if (holder == null) {
            holder = new DojoEventHolder();
            registry.put(type, holder);
        }
        return holder;
    }

    private ListenType type;
    private DojoEventHolder holder;
    private boolean canExecute = true;

    public DojoEvent(ListenType type) {
        this.type = type;
        this.holder = getEventHolder(type);
    }

    public ListenType getType() {
        return type;
    }

    protected DojoEventHolder getHolder() {
        return holder;
    }

    public boolean canExecute() {
        return canExecute;
    }

    public void setCanExecutef(boolean value) {
        this.canExecute = value;
    }

    public abstract void execute();

}
