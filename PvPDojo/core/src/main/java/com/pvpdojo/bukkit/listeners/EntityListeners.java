/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.entity.EntityPortalEvent;
import org.bukkit.event.entity.EntityTameEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerLeashEntityEvent;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.DojoEvent;
import com.pvpdojo.bukkit.events.DojoEventExecutor;
import com.pvpdojo.bukkit.events.DojoProjectileHitEntityEvent;
import com.pvpdojo.bukkit.events.EntityFallEvent;
import com.pvpdojo.bukkit.events.ListenType;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class EntityListeners implements DojoListener {

    @EventHandler
    public void onEntityDamageEvent(final EntityDamageByEntityEvent e) {
        final Entity damager = e.getDamager();
        if (!(e.getEntity() instanceof LivingEntity))
            return;

        final LivingEntity victim = (LivingEntity) e.getEntity();

        if (damager instanceof Player) {
            User duser = User.getUser((Player) damager);
            if (duser.getHotbarGui() != null && duser.getHotbarGui().getEntityAction(((Player) damager).getItemInHand().getType()) != null) {
                duser.getHotbarGui().getEntityAction(((Player) damager).getItemInHand().getType()).accept((Player) damager, Action.LEFT_CLICK_AIR, victim);
            }
            e.setDamage(e.getDamage() * 2 / 3);
        }

        if (PlayerUtil.isCrit(victim)) {
            e.setDamage(e.getDamage() / 1.5 * 1.337);
        }

        if (!PlayerUtil.isRealHit(victim) && victim.getLastDamageCause() instanceof EntityDamageByEntityEvent
                && ((EntityDamageByEntityEvent) victim.getLastDamageCause()).getDamager() instanceof Player) {
            e.setDamage(e.getDamage() / 4);
        }

        if (e.getDamager() instanceof Projectile) {
            DojoEvent.executeEvent(ListenType.PROJECTILE_HIT_ENTITY_EVENT, new DojoEventExecutor<DojoProjectileHitEntityEvent>() {
                @Override
                public void preExecute(DojoProjectileHitEntityEvent event) {
                    event.event = e;
                    event.projectile = e.getDamager();
                    event.entityHit = (LivingEntity) e.getEntity();
                    event.shooter = (LivingEntity) ((Projectile) e.getDamager()).getShooter();
                    event.setCancelled(false);
                }

                @Override
                public void postExecute(DojoProjectileHitEntityEvent event) {
                    if (event.isCancelled()) {
                        e.setCancelled(true);
                    }
                    event.projectile = null;
                    event.entityHit = null;
                    event.shooter = null;
                }
            });
        }
    }


    @EventHandler(priority = EventPriority.HIGH)
    public void onEntityDamageEvent(final EntityDamageEvent e) {
        if (!(e.getEntity() instanceof LivingEntity))
            return;

        final Entity entity = e.getEntity();
        if (entity instanceof LivingEntity) {
            if (e.getCause().equals(DamageCause.FALL)) {
                if (PvPDojo.get().getServerSettings().isReducedFallDamage()) {
                    if (e.getFinalDamage() > 17D) {
                        e.setDamage(17D);
                    }
                }

                EntityFallEvent event = new EntityFallEvent(entity, e.getFinalDamage());
                Bukkit.getPluginManager().callEvent(event);
                if (event.isCancelled()) {
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onEntityFoodLevelChangeEvent(FoodLevelChangeEvent e) {
        if (!PvPDojo.get().getServerSettings().isHunger()) {
            e.setCancelled(true);
            e.setFoodLevel(20);
        }
    }

    @EventHandler
    public void onEntityInteractEvent(EntityInteractEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onEntityPortalEvent(EntityPortalEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onEntityTameEvent(EntityTameEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerLeashEntityEvent(PlayerLeashEntityEvent e) {
        e.setCancelled(true);
    }


}
