/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import com.pvpdojo.userdata.PersistentData;

public class DataLoadedEvent extends PlayerEvent {

    private static final HandlerList handlerList = new HandlerList();

    private PersistentData data;
    
    public DataLoadedEvent(Player who, PersistentData data) {
        super(who);
        this.data = data;
    }
    
    public PersistentData getData() {
        return data;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList() {
        return handlerList;
    }

}
