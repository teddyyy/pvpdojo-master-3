/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.events;

import org.bukkit.entity.Entity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityEvent;

public class EntityFallEvent extends EntityEvent implements Cancellable {

    private static final HandlerList handlerList = new HandlerList();

    private double distance;
    private double fallDamage;
    private boolean cancelled;

    public EntityFallEvent(Entity what, double fallDamage) {
        super(what);
        distance = what.getFallDistance();
        this.fallDamage = fallDamage;
    }

    public double getDistance() {
        return distance;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList() {
        return handlerList;
    }

    public double getFallDamage() {
        return fallDamage;
    }
}
