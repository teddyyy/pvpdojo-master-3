/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import com.pvpdojo.PvPDojo;

public class WorldListeners implements DojoListener {

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event) {
        boolean rain = event.toWeatherState();
        if (rain) {
            if (!PvPDojo.get().getServerSettings().isRain()) {
                event.setCancelled(true);
            }
        } else if (PvPDojo.get().getServerSettings().isRain()) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onStop(PluginDisableEvent e) {
        PvPDojo.get().setSyncCatcherEnabled(false);
    }

}
