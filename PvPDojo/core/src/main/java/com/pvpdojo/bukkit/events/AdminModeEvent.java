/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import lombok.Getter;

public class AdminModeEvent extends PlayerEvent {

    private static final HandlerList handlers = new HandlerList();

    @Getter
    private boolean enable;

    public AdminModeEvent(Player player, boolean enable) {
        super(player);
        this.enable = enable;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}


