/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.listeners;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.Plugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.PacketType.Play.Client;
import com.comphenix.protocol.PacketType.Play.Server;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerOptions;
import com.comphenix.protocol.events.ListeningWhitelist;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.events.PacketListener;
import com.comphenix.protocol.injector.GamePhase;
import com.google.common.base.Charsets;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.userdata.User;

public class InputGuiListener implements PacketListener, Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent e) {

        Player p = e.getPlayer();
        User pd = User.getUser(p);

        if (pd.getInputGui() != null) {
            pd.getInputGui().setCancelled(p);
        }

    }

    @Override
    public Plugin getPlugin() {
        return PvPDojo.get();
    }

    @Override
    public ListeningWhitelist getSendingWhitelist() {
        return ListeningWhitelist.newBuilder().normal().gamePhase(GamePhase.PLAYING).options(new ListenerOptions[] {})
                                 .types(Server.BLOCK_CHANGE, Server.OPEN_WINDOW, Server.CLOSE_WINDOW, Server.RESPAWN).build();
    }

    @Override
    public ListeningWhitelist getReceivingWhitelist() {
        return ListeningWhitelist.newBuilder().normal().gamePhase(GamePhase.PLAYING).options(new ListenerOptions[] {})
                                 .types(Client.CUSTOM_PAYLOAD, Client.CHAT, Client.ARM_ANIMATION, Client.BLOCK_PLACE, Client.WINDOW_CLICK, Client.USE_ENTITY, Client.BLOCK_DIG,
                                         Client.CLOSE_WINDOW, Client.SET_CREATIVE_SLOT, Client.UPDATE_SIGN, Client.HELD_ITEM_SLOT)
                                 .build();
    }

    @Override
    public void onPacketReceiving(PacketEvent e) {
        PacketContainer packet = e.getPacket();
        Player player = e.getPlayer();

        if (player == null || !User.containsUser(player.getUniqueId())) {
            return;
        }

        User pd = User.getUser(player);

        PacketType type = e.getPacketType();
        if (type.equals(Client.CUSTOM_PAYLOAD)) {
            String tag = packet.getStrings().read(0);
            if (tag.equals("MC|AdvCdm")) {

                byte[] data = packet.getByteArrays().read(0);

                try (ByteArrayInputStream bis = new ByteArrayInputStream(data); DataInputStream dis = new DataInputStream(bis)) {
                    /*
                     * Return on invalid action id
                     */
                    if (dis.readByte() == 1)
                        return;

                    int x = dis.readInt();
                    int y = dis.readInt();
                    int z = dis.readInt();

                    byte[] bytes = new byte[dis.available()];
                    dis.read(bytes);
                    String string = new String(bytes, Charsets.UTF_8).trim();

                    if (pd.getInputGui() != null) {
                        /*
                         * Broken case handler
                         */
                        Location l = pd.getInputGui().getFakeBlock();
                        if (l.getBlockX() != x || l.getBlockY() != y || l.getBlockZ() != z) {
                            pd.getInputGui().setCancelled(player);
                            System.err.println("Something might be broken in input gui handling. stacktrace:");
                            Thread.dumpStack();
                            return;
                        }
                        e.setCancelled(true);
                        pd.getInputGui().setConfirmed(player, string);
                    }

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        } else if (pd.getInputGui() != null) {
            pd.getInputGui().setCancelled(player);
            if (e.getPacketType() != Client.ARM_ANIMATION) {
                e.setCancelled(true);
            }
            if (type == PacketType.Play.Client.HELD_ITEM_SLOT) {
                PacketContainer held_item = ProtocolLibrary.getProtocolManager().createPacket(PacketType.Play.Server.HELD_ITEM_SLOT);
                held_item.getIntegers().write(0, player.getInventory().getHeldItemSlot());
                try {
                    ProtocolLibrary.getProtocolManager().sendServerPacket(player, held_item);
                } catch (InvocationTargetException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onPacketSending(PacketEvent e) {
        PacketContainer packet = e.getPacket();
        User player = User.getUser(e.getPlayer());

        if (player.getInputGui() == null) {
            return;
        }

        PacketType type = e.getPacketType();
        if (type.equals(Server.BLOCK_CHANGE)) {
            int x = packet.getIntegers().read(0);
            int y = packet.getIntegers().read(1);
            int z = packet.getIntegers().read(2);

            Material material = packet.getBlocks().read(0);

            Location l = player.getInputGui().getFakeBlock();
            if (l.getBlockX() == x && l.getBlockY() == y && l.getBlockZ() == z && material != Material.COMMAND) {
                e.setCancelled(true);
            }
        } else if (type.equals(Server.CLOSE_WINDOW) || type.equals(Server.OPEN_WINDOW)) {
            player.sendMessage("Server closed");
            player.getInputGui().setCancelled(e.getPlayer());
        }
    }

}
