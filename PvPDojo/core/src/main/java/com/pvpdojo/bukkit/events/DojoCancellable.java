/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.events;

public interface DojoCancellable {

    boolean isCancelled();

    void setCancelled(boolean value);
}
