/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.util;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_7_R4.event.CraftEventFactory;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.menu.InventoryMenu;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.util.bukkit.ItemBuilder;

import net.minecraft.server.v1_7_R4.Container;
import net.minecraft.server.v1_7_R4.EntityPlayer;

public class GuiUtil {

    private static final ItemStack BLANK = new ItemBuilder(Material.STAINED_GLASS_PANE).name(" ").durability(DyeColor.GRAY.getWoolData()).build();
    private static final ItemStack BLANK_BAR = new ItemBuilder(Material.STAINED_GLASS_PANE).name(" ").durability(DyeColor.BLACK.getWoolData()).build();

    public static ItemStack getBlankItem() {
        return BLANK;
    }

    public static ItemStack getBlankBar() {
        return BLANK_BAR;
    }

    public static void fillBlank(Inventory inv) {
        fillBlank(inv, BLANK);
    }

    public static void fillBlank(Inventory inv, ItemStack item) {
        fillBlank(inv, 0, item);
    }

    public static void fillBlank(Inventory inv, int start) {
        fillBlank(inv, start, BLANK);
    }

    public static void fillBlank(Inventory inv, int start, ItemStack item) {
        fillBlank(inv, start, inv.getSize(), item);
    }

    public static void fillBlank(Inventory inv, int start, int end) {
        fillBlank(inv, start, end, BLANK);
    }

    public static void fillBlank(Inventory inv, int start, int end, ItemStack item) {
        for (int i = start; i < end; i++) {
            if (inv.getItem(i) == null || inv.getItem(i).getType() == Material.AIR) {
                inv.setItem(i, item);
            }
        }
    }

    public static void fillRow(InventoryPage inv, ItemStack item, int column_start, int column_end, int row) {
        int loops = column_end - column_start + 1;
        for (int column = 0; column < loops; column++) {
            int slot = calcSlot(row, column_start + column);
            inv.setItem(slot, item, null);
        }
    }

    public static int calcSlot(int row, int column) {
        return (column + row * 9) - 10;
    }

    public static int getNoEdgePattern(int amount) {
        if (amount % 9 == 0) {
            amount++;
        }
        if ((amount + 1) % 9 == 0) {
            amount++;
        }
        if (amount % 9 == 0) {
            amount++;
        }
        if ((amount + 1) % 9 == 0) {
            amount++;
        }
        return amount;
    }

    public static void openFast(Player p, Inventory inv, InventoryMenu menu, boolean closeEvent) {

        if (p.getOpenInventory().getTopInventory().equals(inv)) {
            return;
        }

        try {
            EntityPlayer ep = NMSUtils.get(p);
            Container defaultContainer = ep.defaultContainer;
            Container activeContainer = ep.activeContainer;

            if (defaultContainer == activeContainer) {
                p.openInventory(inv);
            } else {
                if (closeEvent) {
                    CraftEventFactory.handleInventoryCloseEvent(ep);
                }
                ep.activeContainer = defaultContainer;
                p.openInventory(inv);
            }
        } catch (Exception e) {
            e.printStackTrace();
            p.openInventory(inv);
        }
    }
}