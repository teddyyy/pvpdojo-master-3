/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import com.comphenix.protocol.wrappers.EnumWrappers.EntityUseAction;
import com.pvpdojo.util.bukkit.Human;

public class BotInteractEvent extends PlayerEvent {

    private static final HandlerList handlers = new HandlerList();

    private Human human;
    private EntityUseAction action;

    public BotInteractEvent(Player player, Human human, EntityUseAction action) {
        super(player);
        this.human = human;
        this.action = action;
    }

    public Human getHuman() {
        return human;
    }

    public EntityUseAction getAction() {
        return action;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
