/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public abstract class TrackedObject {

    protected static final List<TrackedObject> TRACKED_OBJECTS = new CopyOnWriteArrayList<>();

    public static List<TrackedObject> getTrackedObjects() {
        return TRACKED_OBJECTS;
    }

    protected Location location;
    protected Set<Player> viewers;

    public TrackedObject() {
        this.viewers = new HashSet<>();
    }

    public TrackedObject(Location location) {
        this();
        this.location = location;
    }

    public void updateFor(Player player) {
        if (getTargetGroup().contains(player)) {
            double xDiff = player.getLocation().getX() - this.location.getX();
            double zDiff = player.getLocation().getZ() - this.location.getZ();

            if (Math.abs(xDiff) < 45 && Math.abs(zDiff) < 45 && !player.isDead()) {
                if (!viewers.contains(player)) {
                    viewers.add(player);
                    createFor(player);
                }
            } else if (viewers.remove(player)) {
                destroyFor(player);
            }
        }
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Set<Player> getViewers() {
        return viewers;
    }

    public void track() {
        TRACKED_OBJECTS.add(this);
        getTargetGroup().forEach(this::updateFor);
    }

    public TrackedObject untrack() {
        viewers.forEach(this::destroyFor);
        TRACKED_OBJECTS.remove(this);
        return this;
    }

    public abstract void createFor(Player player);

    public abstract void destroyFor(Player player);

    public abstract Collection<Player> getTargetGroup();

}
