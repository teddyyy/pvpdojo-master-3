/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.events;

import java.util.ArrayList;

public class DojoEventHolder {

    private ArrayList<DojoEvent> events = new ArrayList<>();

    public ArrayList<DojoEvent> getEvents() {
        return events;
    }

    public void addEvent(DojoEvent event) {
        events.add(event);
    }

}
