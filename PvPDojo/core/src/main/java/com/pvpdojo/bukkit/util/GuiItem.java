/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.util;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class GuiItem {

    private GuiItem() {

    }

    public static ItemStack setAmount(ItemStack item, int amount) {
        item.setAmount(amount);
        return item;
    }

    @SuppressWarnings("deprecation")
    public static int getMaterialDataId(ItemStack item_stack) {
        if (item_stack == null)
            return 0;
        if (item_stack.getData() == null)
            return 0;
        return item_stack.getData().getData();
    }

    @SuppressWarnings("deprecation")
    public static ItemStack setData(ItemStack item, byte data) {
        item.setData(item.getType().getNewData(data));
        return item;
    }

    public static void addGlow(ItemStack item) {
        item.addEnchantment(NMSUtils.GLOW, 1);
    }

    public static void removeGlow(ItemStack item) {
        item.removeEnchantment(NMSUtils.GLOW);
    }

    public static boolean hasGlow(ItemStack item) {
        return item.containsEnchantment(NMSUtils.GLOW);
    }

    public static String getDisplayName(ItemStack item_stack) {
        String display = "";
        if (item_stack == null)
            return display;
        if (!item_stack.hasItemMeta())
            return display;
        ItemMeta meta = item_stack.getItemMeta();
        if (!meta.hasDisplayName())
            return display;
        return meta.getDisplayName();
    }

    public static String getDisplayNameStripped(ItemStack item_stack) {
        return CC.stripColor(getDisplayName(item_stack));
    }

    public static List<String> getLore(ItemStack item_stack) {
        if (!item_stack.hasItemMeta())
            return null;
        ItemMeta meta = item_stack.getItemMeta();
        if (!meta.hasLore())
            return null;
        return meta.getLore();
    }


    public static ItemStack fromString(String raw_data, String display) {
        String[] colon_split = raw_data.split(":");
        Material material = Material.valueOf(colon_split[0]);
        int matdata = Integer.valueOf(colon_split[1]);
        return new ItemBuilder(material).durability((short) matdata).name(display).build();
    }

}
