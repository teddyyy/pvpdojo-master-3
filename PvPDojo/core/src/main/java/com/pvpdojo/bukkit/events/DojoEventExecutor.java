/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.events;

public interface DojoEventExecutor<T extends DojoEvent> {

    void preExecute(T event);

    void postExecute(T event);
}
