/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.events;

import java.util.LinkedList;
import java.util.List;
import java.util.function.BooleanSupplier;

import org.bukkit.event.HandlerList;
import org.bukkit.event.server.ServerEvent;

public class DojoServerRestartEvent extends ServerEvent {

    private static final HandlerList handlers = new HandlerList();

    private final int secondsTillRestart;
    private List<BooleanSupplier> restartConditions = new LinkedList<>();

    public DojoServerRestartEvent(int secondsTillRestart) {
        this.secondsTillRestart = secondsTillRestart;
    }

    public void addRestartCondition(BooleanSupplier condition) {
        this.restartConditions.add(condition);
    }

    public boolean canRestart() {
        return restartConditions.stream().allMatch(BooleanSupplier::getAsBoolean);
    }

    public int getSecondsTillRestart() {
        return secondsTillRestart;
    }

    public void cancel() {
        restartConditions.add(() -> false);
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
