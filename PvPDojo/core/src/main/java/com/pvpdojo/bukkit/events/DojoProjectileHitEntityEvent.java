/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.events;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public abstract class DojoProjectileHitEntityEvent extends DojoEvent implements DojoCancellable {

    public EntityDamageByEntityEvent event;
    public Entity projectile;
    public LivingEntity entityHit;
    public LivingEntity shooter;
    private boolean cancelled;

    public DojoProjectileHitEntityEvent() {
        super(ListenType.PROJECTILE_HIT_ENTITY_EVENT);
    }

    public Entity getProjectile() {
        return projectile;
    }

    public LivingEntity getShooter() {
        return shooter;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }
}