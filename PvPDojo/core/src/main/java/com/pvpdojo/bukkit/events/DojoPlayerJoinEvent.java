/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerJoinEvent;

import com.pvpdojo.settings.ServerSwitchSettings;

import lombok.Getter;

public class DojoPlayerJoinEvent extends PlayerJoinEvent {

    private static final HandlerList handlers = new HandlerList();

    @Getter
    private final ServerSwitchSettings serverSwitch;

    public DojoPlayerJoinEvent(Player playerJoined, String joinMessage, ServerSwitchSettings serverSwitch) {
        super(playerJoined, joinMessage);
        this.serverSwitch = serverSwitch;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}