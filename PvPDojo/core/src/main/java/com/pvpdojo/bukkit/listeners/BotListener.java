/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.listeners;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.PacketType.Play.Client;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerOptions;
import com.comphenix.protocol.events.ListeningWhitelist;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.events.PacketListener;
import com.comphenix.protocol.injector.GamePhase;
import com.comphenix.protocol.wrappers.EnumWrappers.EntityUseAction;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.BotInteractEvent;
import com.pvpdojo.util.bukkit.Human;

import net.minecraft.server.v1_7_R4.EnumEntityUseAction;
import net.minecraft.server.v1_7_R4.PacketPlayInUseEntity;

public class BotListener implements PacketListener {

    public static void register() {
        ProtocolLibrary.getProtocolManager().addPacketListener(new BotListener());
    }

    @Override
    public Plugin getPlugin() {
        return PvPDojo.get();
    }

    @Override
    public ListeningWhitelist getReceivingWhitelist() {
        return ListeningWhitelist.newBuilder().normal().gamePhase(GamePhase.PLAYING).options(new ListenerOptions[] {}).types(Client.USE_ENTITY).build();
    }

    @Override
    public ListeningWhitelist getSendingWhitelist() {
        return null;
    }

    @Override
    public void onPacketReceiving(PacketEvent e) {
        if (e.getPacketType() == PacketType.Play.Client.USE_ENTITY) {
            if (e.getPacket().getEntityUseActions().size() > 0) {

                PacketPlayInUseEntity packet = (PacketPlayInUseEntity) e.getPacket().getHandle();
                Human human = Human.HUMAN_MAP.get(e.getPacket().getIntegers().read(0));
                EntityUseAction action = packet.c() == EnumEntityUseAction.INTERACT ? EntityUseAction.INTERACT : EntityUseAction.INTERACT;

                if (human != null) {
                    PvPDojo.schedule(() -> Bukkit.getPluginManager().callEvent(new BotInteractEvent(e.getPlayer(), human, action))).sync();
                }
            }
        }
    }

    @Override
    public void onPacketSending(PacketEvent e) {}

}
