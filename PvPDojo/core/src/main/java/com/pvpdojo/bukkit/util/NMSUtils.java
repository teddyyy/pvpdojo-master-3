/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.util;

import com.pvpdojo.userdata.User;
import net.minecraft.server.v1_7_R4.AxisAlignedBB;
import net.minecraft.server.v1_7_R4.DamageSource;
import net.minecraft.server.v1_7_R4.Entity;
import net.minecraft.server.v1_7_R4.EntityCreature;
import net.minecraft.server.v1_7_R4.EntityHuman;
import net.minecraft.server.v1_7_R4.EntityInsentient;
import net.minecraft.server.v1_7_R4.EntityLightning;
import net.minecraft.server.v1_7_R4.EntityLiving;
import net.minecraft.server.v1_7_R4.EntityPlayer;
import net.minecraft.server.v1_7_R4.EntityTrackerEntry;
import net.minecraft.server.v1_7_R4.EntityTypes;
import net.minecraft.server.v1_7_R4.MathHelper;
import net.minecraft.server.v1_7_R4.MinecraftServer;
import net.minecraft.server.v1_7_R4.Packet;
import net.minecraft.server.v1_7_R4.PacketPlayOutSpawnEntityWeather;
import net.minecraft.server.v1_7_R4.PathfinderGoal;
import net.minecraft.server.v1_7_R4.PathfinderGoalSelector;
import net.minecraft.server.v1_7_R4.World;
import net.minecraft.server.v1_7_R4.WorldServer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_7_R4.CraftServer;
import org.bukkit.craftbukkit.v1_7_R4.CraftWorld;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftCreature;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_7_R4.util.UnsafeList;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentWrapper;
import org.bukkit.entity.Creature;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class NMSUtils {

    public static final EnchantmentGlow GLOW = new EnchantmentGlow(120);

    public static int getTick() {
        return MinecraftServer.currentTick;
    }

    public static EntityPlayer get(Player player) {
        return ((CraftPlayer) player).getHandle();
    }

    public static CraftPlayer getCraftPlayer(Player player) {
        return ((CraftPlayer) player);
    }

    public static MinecraftServer getServer() {
        return ((CraftServer) Bukkit.getServer()).getServer();
    }

    public static WorldServer get(org.bukkit.World world) {
        return ((CraftWorld) world).getHandle();
    }

    public static float getHeadRotation(LivingEntity living) {
        return get(living).aO;
    }

    public static float getaMValue(LivingEntity living) {
        return get(living).aM;
    }

    public static boolean isInWater(Player player) {
        return ((CraftPlayer) player).getHandle().M();
    }

    public static AxisAlignedBB getBlockBox(Block b) {
        net.minecraft.server.v1_7_R4.Block block = ((CraftWorld) b.getWorld()).getHandle().getType(b.getX(), b.getY(), b.getZ());
        return AxisAlignedBB.a(b.getX() + block.x(), b.getY() + block.y(), b.getZ() + block.B(), b.getX() + block.y(),
                (block.A() < 0.1) ? b.getY() : b.getY() + block.A(), b.getZ() + block.C());
    }

    public static Set<Block> getBlocksBelow(Player player, double yDown, Vector diff) {
        AxisAlignedBB boundingBox = get(player).boundingBox.clone().d(diff.getX(), diff.getY(), diff.getZ());
        org.bukkit.World world = player.getWorld();
        double yBelow = player.getLocation().getY() - yDown;
        Block northEast = new Location(world, boundingBox.d, yBelow, boundingBox.c).getBlock();
        Block northWest = new Location(world, boundingBox.a, yBelow, boundingBox.c).getBlock();
        Block southEast = new Location(world, boundingBox.d, yBelow, boundingBox.f).getBlock();
        Block southWest = new Location(world, boundingBox.a, yBelow, boundingBox.f).getBlock();
        return new HashSet<>(Arrays.asList(northEast, northWest, southEast, southWest));
    }

    public static boolean touches(org.bukkit.World world, AxisAlignedBB box, net.minecraft.server.v1_7_R4.Material material) {
        return get(world).b(box, material);
    }

    public static boolean touchesOnly(org.bukkit.World world, AxisAlignedBB axisalignedbb, Material material) {
        int i = MathHelper.floor(axisalignedbb.a);
        int j = MathHelper.floor(axisalignedbb.d + 1.0D);
        int k = MathHelper.floor(axisalignedbb.b);
        int l = MathHelper.floor(axisalignedbb.e + 1.0D);
        int i1 = MathHelper.floor(axisalignedbb.c);
        int j1 = MathHelper.floor(axisalignedbb.f + 1.0D);

        for (int k1 = i; k1 < j; ++k1) {
            for (int l1 = k; l1 < l; ++l1) {
                for (int i2 = i1; i2 < j1; ++i2) {
                    if (world.getBlockAt(k1, l1, i2).getType() != material) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public static void setHeadRotation(LivingEntity living, float yaw, float pitch) {
        EntityLiving entity = get(living);
        entity.aO = yaw;
        entity.pitch = pitch;
    }

    @SuppressWarnings("unchecked")
    public static void removeWorld(String name) {
        try {
            CraftServer server = NMSUtils.getServer().server;
            Field f = server.getClass().getDeclaredField("worlds");
            f.setAccessible(true);
            Object o = f.get(server);
            Map<String, World> worlds = (Map<String, World>) o;
            worlds.remove(name);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public static void registerEntity(String name, int id, Class<? extends EntityInsentient> customClass) {
        try {
            List<Map<?, ?>> dataMaps = new ArrayList<>();
            for (Field f : EntityTypes.class.getDeclaredFields()) {
                if (f.getType().getSimpleName().equals(Map.class.getSimpleName())) {
                    f.setAccessible(true);
                    dataMaps.add((Map<?, ?>) f.get(null));
                }
            }
            ((Map<Class<? extends EntityInsentient>, String>) dataMaps.get(1)).put(customClass, name);
            ((Map<Class<? extends EntityInsentient>, Integer>) dataMaps.get(3)).put(customClass, id);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Field getField(String name, Class<?> clazz) {
        try {
            return clazz.getDeclaredField(name);
        } catch (NoSuchFieldException | SecurityException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setField(Class<?> type, Object object, String name, Object value) {
        try {
            Field f = type.getDeclaredField(name);
            f.setAccessible(true);
            f.set(object, value);
            f.setAccessible(false);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public static UnsafeList<PathfinderGoal> getGoalSelectorListB(PathfinderGoalSelector p) {
        try {
            Field f = p.getClass().getDeclaredField("b");
            f.setAccessible(true);
            return (UnsafeList<PathfinderGoal>) f.get(p);
        } catch (SecurityException | NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static EntityLiving get(LivingEntity living) {
        return ((CraftLivingEntity) living).getHandle();
    }

    public static EntityCreature get(Creature creature) {
        return ((CraftCreature) creature).getHandle();
    }

    public static Entity get(org.bukkit.entity.Entity e) {
        return ((CraftEntity) e).getHandle();
    }

    public static void registerNewEnchantment() {
        try {
            Field f = Enchantment.class.getDeclaredField("acceptingNew");
            f.setAccessible(true);
            f.set(null, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        EnchantmentWrapper.registerEnchantment(GLOW);
    }

    public static void playEffectPacket(org.bukkit.entity.Entity e, int effect) {
        if (e == null)
            return;
        Entity handle = get(e);
        handle.world.broadcastEntityEffect(handle, (byte) effect);
    }

    public static void applyKnockback(LivingEntity victim, org.bukkit.entity.Entity source) {
        EntityLiving nmsVictim = get(victim);
        Entity nmsSource = get(source);
        double d0 = nmsSource.locX - nmsVictim.locX;

        double d1;

        for (d1 = nmsSource.locZ - nmsVictim.locZ; d0 * d0 + d1 * d1 < 1.0E-4D; d1 = (Math.random() - Math.random()) * 0.01D) {
            d0 = (Math.random() - Math.random()) * 0.01D;
        }

        nmsVictim.az = (float) (Math.atan2(d1, d0) * 180.0D / 3.1415927410125732D) - nmsVictim.yaw;
        nmsVictim.a(nmsSource, 0, d0, d1);
    }

    public static int getPing(Player player) {
        return get(player).ping;
    }

    public static DamageSource getSourceFromCause(DamageCause bukkitCause, org.bukkit.entity.Entity bukkitSource) {
        switch (bukkitCause) {
            case BLOCK_EXPLOSION:
            case ENTITY_EXPLOSION:
                return DamageSource.explosion(null);
            case CONTACT:
                return DamageSource.CACTUS;
            case CUSTOM:
                return DamageSource.GENERIC;
            case DROWNING:
                return DamageSource.DROWN;
            case ENTITY_ATTACK:
                if (bukkitSource instanceof Player) {
                    return DamageSource.playerAttack((EntityHuman) get(bukkitSource));
                } else if (bukkitSource instanceof LivingEntity) {
                    return DamageSource.mobAttack((EntityLiving) get(bukkitSource));
                }
            case FALL:
                return DamageSource.FALL;
            case FALLING_BLOCK:
                return DamageSource.FALLING_BLOCK;
            case FIRE:
                return DamageSource.FIRE;
            case FIRE_TICK:
                return DamageSource.BURN;
            case LAVA:
                return DamageSource.LAVA;
            case MAGIC:
            case POISON:
                return DamageSource.MAGIC;
            case PROJECTILE:
                return DamageSource.projectile(get(bukkitSource), get(bukkitSource));
            case STARVATION:
                return DamageSource.STARVE;
            case SUFFOCATION:
                return DamageSource.STUCK;
            case VOID:
                return DamageSource.OUT_OF_WORLD;
            case WITHER:
                return DamageSource.WITHER;
            default:
                return DamageSource.GENERIC;

        }
    }

    public static void damageEntity(org.bukkit.entity.Entity victim, org.bukkit.entity.Entity source, DamageCause cause, double damage) {
        if (victim == null) {
            return;
        }
        if (victim instanceof LivingEntity) {
            if (!BukkitUtil.callEntityDamageEvent(victim, cause, damage).isCancelled()) {
                combatTag((LivingEntity) victim, source, cause, damage);
                ((LivingEntity) victim).damage(damage);
            }
        } else {
            get(victim).damageEntity(getSourceFromCause(cause, source), (float) damage);
        }
    }

    public static void combatTag(org.bukkit.entity.LivingEntity victim, org.bukkit.entity.Entity source, DamageCause cause, double damage) {
        NMSUtils.get(victim).aW().a(NMSUtils.getSourceFromCause(cause, source), (float) victim.getHealth(), (float) damage);
        if (source instanceof Player) {
            NMSUtils.get(victim).killer = (EntityHuman) NMSUtils.get(source);
            NMSUtils.get(victim).lastDamageByPlayerTime = 200;
        }
    }

    public static CraftServer getCraftServer() {
        return (CraftServer) Bukkit.getServer();
    }

    public static void strikeLightningEffect(Player victim) {
        Location loc = victim.getLocation();
        EntityTrackerEntry entry = (EntityTrackerEntry) NMSUtils.get(victim.getWorld()).getTracker().trackedEntities.get(victim.getEntityId());
        if (entry != null) {
            Packet packet = new PacketPlayOutSpawnEntityWeather(new EntityLightning(NMSUtils.get(victim.getWorld()), loc.getX(), loc.getY(), loc.getZ(), true));
            entry.trackedPlayers
                    .stream()
                    .map(EntityPlayer::getBukkitEntity)
                    .map(User::getUser)
                    .filter(Objects::nonNull)
                    .filter(User::isDataLoaded)
                    .filter(user -> user.getSettings().isLightningAnimation())
                    .map(User::getPlayer)
                    .map(NMSUtils::get)
                    .forEach(entityPlayer -> entityPlayer.playerConnection.sendPacket(packet));
        }

    }

    public static void clearAI(Creature creature) {
        get(creature).getGoalSelector().clear();
        get(creature).getTargetSelector().clear();
    }

}