/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

import com.pvpdojo.PvPDojo;

public interface DojoListener extends Listener {

    default void register() {
        register(PvPDojo.get());
    }

    default void register(Plugin plugin) {
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    default void unregister() {
        HandlerList.unregisterAll(this);
    }
}
