/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Firework;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.metadata.Metadatable;
import org.spigotmc.RestartCommand;
import org.spigotmc.SpigotConfig;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.userdata.User;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;

import net.minecraft.server.v1_7_R4.AxisAlignedBB;
import net.minecraft.server.v1_7_R4.EntityTNTPrimed;

public class BukkitUtil {

    private static final Map<String, Location> configLocations = new HashMap<>();

    public static Collection<Entity> getNearbyEntities(Location location, double x, double y, double z) {
        if (location == null) {
            return Collections.emptyList();
        }

        AxisAlignedBB bb = AxisAlignedBB.a(location.getX() - x, location.getY() - y, location.getZ() - z, location.getX() + x, location.getY() + y, location.getZ() + z);
        @SuppressWarnings("unchecked")
        List<net.minecraft.server.v1_7_R4.Entity> entities = NMSUtils.get(location.getWorld()).getEntities(null, bb, null);

        return entities.stream().map(net.minecraft.server.v1_7_R4.Entity::getBukkitEntity).collect(Collectors.toList());
    }

    public static int countNearbyEntities(Location location, double x, double y, double z) {
        if (location == null) {
            return 0;
        }

        AxisAlignedBB bb = AxisAlignedBB.a(location.getX() - x, location.getY() - y, location.getZ() - z, location.getX() + x, location.getY() + y, location.getZ() + z);
        return NMSUtils.get(location.getWorld()).getEntities(null, bb, null).size();
    }

    public static void removeEntity(World world, int id) {
        for (int i = 0; i < world.getEntities().size(); i++) {
            Entity entity = world.getEntities().get(i);
            if (entity.getEntityId() == id) {
                entity.eject();
                entity.remove();
            }
        }
    }

    public static ArrayList<File> getServerFolder(String subset) {
        ArrayList<File> files = new ArrayList<>();
        for (File serverfile : Bukkit.getServer().getWorldContainer().listFiles()) {
            if (serverfile.getName().startsWith(subset)) {
                files.add(serverfile);
            }
        }
        return files;
    }

    public static String getTabName(String color, String name) {
        String finalstring = color + name;
        int length = finalstring.length();
        if (length > 16) {
            finalstring = finalstring.substring(0, 16);
        }
        return finalstring;
    }

    public static void reloadLocations() {
        configLocations.clear();
    }

    public static Location getLocationFromConfig(String path) {
        return getLocationFromConfig(path, Bukkit.getWorlds().get(0));
    }

    public static Location getLocationFromConfig(String path, World world) {
        if (configLocations.containsKey(path)) {
            return configLocations.get(path);
        }
        if (!PvPDojo.get().getConfig().contains(path)) {
            return new Location(world, 0, 10, 0);
        }
        FileConfiguration config = PvPDojo.get().getConfig();
        Location loc = new Location(world, config.getDouble(path + ".x"), config.getDouble(path + ".y"), config.getDouble(path + ".z"), config.getFloat(path + ".yaw"),
                config.getFloat(path + ".pitch"));
        configLocations.put(path, loc);
        return loc;
    }

    public static void launchFireworkEffect(Location loc, FireworkEffect effect) {
        Firework fw = loc.getWorld().spawn(loc, Firework.class);
        FireworkMeta meta = fw.getFireworkMeta();

        meta.clearEffects();
        meta.setPower(1);
        meta.addEffect(effect);

        fw.setFireworkMeta(meta);
        PvPDojo.schedule(fw::detonate).createTask(8);
    }

    public static void createExplosionByEntity(LivingEntity source, Location location, float yield, boolean breakBlocks, boolean fire) {
        EntityTNTPrimed tnt = new EntityTNTPrimed(location, NMSUtils.get(location.getWorld()), location.getX(), location.getY(), location.getZ(), source != null ? NMSUtils.get(source) : null);
        NMSUtils.get(location.getWorld()).createExplosion(tnt, location.getX(), location.getY(), location.getZ(), yield, fire, breakBlocks);
    }

    public static EntityDamageEvent callEntityDamageEvent(Entity entity, DamageCause cause, double damage) {
        EntityDamageEvent event = new EntityDamageEvent(entity, cause, damage);
        Bukkit.getPluginManager().callEvent(event);
        entity.setLastDamageCause(event);
        return event;
    }

    public static LivingEntity getSourceDamager(Entity damager) {
        return damager instanceof Projectile ?
                ((Projectile) damager).getShooter() instanceof LivingEntity ?
                        (LivingEntity) ((Projectile) damager).getShooter() : null
                : damager instanceof LivingEntity ?
                (LivingEntity) damager : null;
    }

    public static List<MetadataValue> consumeMeta(Metadatable metadatable, String meta) {
        List<MetadataValue> object = metadatable.getMetadata(meta);
        metadatable.removeMetadata(meta, PvPDojo.get());
        return object;
    }

    public static void restart() {
        RestartCommand.restart(new File(SpigotConfig.restartScript), false);
        Bukkit.getOnlinePlayers().stream().map(User::getUser).forEach(user -> user.kick("Server is restarting"));
        try {
            Thread.sleep(100L);
        } catch (InterruptedException ignored) {
        }
    }

    public static void shutdown() {
        Bukkit.shutdown();
        Bukkit.getOnlinePlayers().stream().map(User::getUser).forEach(user -> user.kick("Server closed"));
    }

    private static final WorldEditPlugin WORLD_EDIT = (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("GEdit");

    public static WorldEditPlugin getWorldEdit() {
        return WORLD_EDIT;
    }

}
