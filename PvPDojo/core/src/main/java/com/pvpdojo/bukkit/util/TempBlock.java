/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.util;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.sk89q.worldguard.bukkit.RegionQuery;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.flags.DefaultFlag;

public class TempBlock {

    protected Material fake, real;
    protected byte fakeData, realData;
    protected Location loc;
    protected long expire;
    private boolean reset = false;
    private static ArrayList<TempBlock> temp_blocks = new ArrayList<>();

    public static void createTempBlock(Location location, Material fake, byte mat, long seconds) {
        RegionQuery query = WGBukkit.getPlugin().getRegionContainer().createQuery();
        if (query.testState(location, (Player) null, DefaultFlag.PVP)) {
            temp_blocks.add(new TempBlock(location, fake, mat, seconds));
        }
    }

    public static boolean checkTempBlocks(TempBlock temp) {
        long time = temp.getExpire();
        if (time != -1) {
            return time < System.currentTimeMillis();
        }
        return false;
    }

    public static ArrayList<TempBlock> getTempBlocks() {
        return temp_blocks;
    }

    @SuppressWarnings("deprecation")
    protected TempBlock(Location location, Material fake, byte data, long seconds) {
        if (seconds < 0)
            seconds = 0;
        if (location.getBlock().getType().equals(Material.AIR)) {
            this.loc = new Location(location.getWorld(), location.getBlockX(), location.getBlockY(), location.getBlockZ());
            this.fake = fake;
            this.real = location.getBlock().getType();
            this.realData = data;
            this.expire = System.currentTimeMillis() + seconds * 1000;
            if (fake != null) {
                /*
                 * for (Player player : location.getWorld().getPlayers()){ player.sendBlockChange(location, fake, data); }
                 */
                this.loc.getBlock().setTypeIdAndData(fake.getId(), data, false);
            }
        }
    }

    public void reset() {
        temp_blocks.remove(this);
        if (reset) {
            return;
        }
        reset = true;
        if (loc == null)
            return;
        Block b = loc.getBlock();
        b.setType(Material.AIR);
    }

    public Location getLocation() {
        return this.loc;
    }

    public Material getFakeMaterial() {
        return this.fake;
    }

    public Material getRealMaterial() {
        return this.real;
    }

    public byte getRealData() {
        return realData;
    }

    public byte getFakeData() {
        return fakeData;
    }

    public long getExpire() {
        return expire;
    }

}