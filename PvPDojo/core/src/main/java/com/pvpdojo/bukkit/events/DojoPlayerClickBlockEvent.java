/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.events;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class DojoPlayerClickBlockEvent extends DojoEvent implements DojoCancellable {

    public Player player;
    public ItemStack item;
    public Block block;
    private boolean cancelled;

    public DojoPlayerClickBlockEvent() {
        super(ListenType.PLAYER_CLICK_BLOCK_EVENT);
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }
}
