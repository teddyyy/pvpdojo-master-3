/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.events;

import java.util.UUID;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ProxyQuitEvent extends Event {

    private static final HandlerList handlerList = new HandlerList();

    private final UUID uniqueId;

    public ProxyQuitEvent(UUID uniqueId) {
        super(true);
        this.uniqueId = uniqueId;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    public static HandlerList getHandlerList() {
        return handlerList;
    }
}
