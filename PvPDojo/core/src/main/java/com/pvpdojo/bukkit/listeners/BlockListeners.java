/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.userdata.User;

public class BlockListeners implements DojoListener {

	@EventHandler
	public void BlockChanges(BlockFromToEvent e) {
	    if (!PvPDojo.get().getServerSettings().isAllowNaturalBlockChange()) {
            e.setCancelled(true);
        }
	}
	
    @EventHandler
    public void onSignChange(SignChangeEvent e) {
        if (e.getPlayer().hasPermission("Staff")) {
            for (int i = 0; i < e.getLines().length; i++) {
                e.setLine(i, ChatColor.translateAlternateColorCodes('&', e.getLine(i)));
            }
        }
    }

    @EventHandler
    public void onBlockBreakEvent(BlockBreakEvent e) {
        if (!User.getUser(e.getPlayer()).canBuild()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockPlaceEvent(BlockPlaceEvent e) {
        if (!User.getUser(e.getPlayer()).canBuild()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerBucketFillEvent(PlayerBucketFillEvent e) {
        if (!User.getUser(e.getPlayer()).canBuild()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerBucketEmptyBucketEvent(PlayerBucketEmptyEvent e) {
        if (!User.getUser(e.getPlayer()).canBuild()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onFireExtinguish(PlayerInteractEvent e) {
        if (e.getAction() == Action.LEFT_CLICK_BLOCK && !User.getUser(e.getPlayer()).canBuild() && e.getClickedBlock().getRelative(e.getBlockFace()).getType() == Material.FIRE) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockBurnEvent(BlockBurnEvent e) {
        if (!PvPDojo.get().getServerSettings().isAllowNaturalBlockChange()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockDispenseEvent(BlockDispenseEvent e) {
        if (!PvPDojo.get().getServerSettings().isAllowNaturalBlockChange()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockExpEvent(BlockExpEvent e) {
        if (!PvPDojo.get().getServerSettings().isAllowNaturalBlockChange()) {
            e.setExpToDrop(0);
        }
    }

    @EventHandler
    public void onBlockIgniteEvent(BlockIgniteEvent e) {
        if (!PvPDojo.get().getServerSettings().isAllowNaturalBlockChange()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onHangingBreakByEntityEvent(HangingBreakByEntityEvent e) {
        if (!PvPDojo.get().getServerSettings().isAllowNaturalBlockChange()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockFadeEvent(BlockFadeEvent e) {
        if (!PvPDojo.get().getServerSettings().isAllowNaturalBlockChange()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onLeavesDecayEvent(LeavesDecayEvent e) {
        if (!PvPDojo.get().getServerSettings().isAllowNaturalBlockChange()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockPistonExtendEvent(BlockPistonExtendEvent e) {
        if (!PvPDojo.get().getServerSettings().isAllowNaturalBlockChange()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockRedStoneEvent(BlockRedstoneEvent e) {
        if (!PvPDojo.get().getServerSettings().isAllowNaturalBlockChange()) {
            e.setNewCurrent(0);
        }
    }

    @EventHandler
    public void onBlockGrowEvent(BlockGrowEvent e) {
        if (!PvPDojo.get().getServerSettings().isAllowNaturalBlockChange()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityChangeBlock(EntityChangeBlockEvent e) {
        if (!PvPDojo.get().getServerSettings().isAllowNaturalBlockChange()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockChange(BlockFormEvent e) {
        if (!PvPDojo.get().getServerSettings().isAllowNaturalBlockChange()) {
            e.setCancelled(true);
        }
    }

}
