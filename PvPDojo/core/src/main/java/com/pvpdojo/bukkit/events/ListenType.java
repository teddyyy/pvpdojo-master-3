/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.bukkit.events;

public enum ListenType {
    ENTITY_CHANGE_BLOCK_EVENT,
    ENTITY_DAMAGE_ENTITY_EVENT,
    ENTITY_DAMAGE_PLAYER_EVENT,
    ENTITY_DAMAGE_ANY_EVENT,
    ENTITY_DEATH_EVENT,
    ENTITY_FALL_EVENT,
    ENTITY_REGAIN_HEALTH_EVENT,
    ENTITY_SPAWN_EVENT,
    ENTITY_TARGET_EVENT,
    ENTITY_BOW_SHOOT_EVENT,
    POTION_SPLASH_EVENT,
    PROJECTILE_HIT_ENTITY_EVENT,

    ITEM_SPAWN_EVENT,
    ITEM_DESPAWN_EVENT,
    PROJECTILE_HIT_EVENT,
    REGION_CHANGE_EVENT,

    PLAYER_CHANGE_WORLD_EVENT,
    PLAYER_COMMAND_PREPROCESS_EVENT,
    PLAYER_CHAT_EVENT,
    PLAYER_CLICK_EVENT,
    PLAYER_CLICK_BLOCK_EVENT,
    PLAYER_CLICK_ENTITY_EVENT,
    PLAYER_DAMAGE_ANY_EVENT,
    PLAYER_DAMAGE_ENTITY_EVENT,
    PLAYER_DAMAGE_PLAYER_EVENT,
    PLAYER_DROP_ITEM_EVENT,
    PLAYER_ITEM_HELD_EVENT,
    PLAYER_JOIN_EVENT,
    PLAYER_LOGIN_EVENT,
    PLAYER_PRE_LOGIN_EVENT,
    PLAYER_PICKUP_ITEM_EVENT,
    PLAYER_RESPAWN_EVENT,
    PLAYER_TELEPORT_EVENT,
    PLAYER_QUIT_EVENT,
    PLAYER_TOGGLE_SNEAK_EVENT,
    PLAYER_DEATH_EVENT,
    PLAYER_MOVE_EVENT,

    CHAMPION_DAMAGE_EVENT,
    CHAMPION_ABILITY_EVENT,
    CHAMPION_ABILITY_ONHIT_EVENT,
    CHAMPION_DEATH_EVENT,
    CHAMPION_RESPAWN_EVENT,
    CHAMPION_SELECT_EVENT,
    CHAMPION_TARGET_EVENT,

    INVENTORY_CLICK_EVENT,
    INVENTORY_CLOSE_EVENT,
    CRAFT_ITEM_EVENT
}
