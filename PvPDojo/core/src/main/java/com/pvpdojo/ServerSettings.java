/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapelessRecipe;

import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

import lombok.Getter;
import lombok.Setter;

public class ServerSettings {

    private static final AlternativeRecraftModule ALTERNATIVE_RECRAFT_MODULE = new AlternativeRecraftModule();

    private boolean soup;
    private boolean hunger;
    private boolean allowQuitMessage, allowJoinMessage;
    private boolean terrainControl;
    private boolean naturalRegeneration;
    private boolean allowNaturalBlockChange;
    @Getter
    @Setter
    private boolean rain, stats = true;
    private Predicate<Player> soupCondition;
    private boolean reducedFallDamage = true;
    private boolean abilities = true;
    private EnumSet<Material> launchMaterials = EnumSet.of(Material.SPONGE);
    private EnumSet<Material> blockedInteraction = EnumSet.of(Material.BEACON, Material.CHEST, Material.HOPPER, Material.BED_BLOCK, Material.ENDER_CHEST, Material.ANVIL,
            Material.TRAP_DOOR, Material.FURNACE, Material.BURNING_FURNACE, Material.ENCHANTMENT_TABLE, Material.DROPPER, Material.DISPENSER);

    public ServerSettings() {}

    public EnumSet<Material> getLaunchMaterials() {
        return launchMaterials;
    }

    public EnumSet<Material> getBlockedInteraction() {
        return blockedInteraction;
    }

    public boolean isSoup() {
        return soup;
    }

    public void setSoup(boolean soup) {
        this.soup = soup;
    }

    public Predicate<Player> getSoupCondition() {
        return soupCondition;
    }

    public void addSoupCondition(Predicate<Player> soupCondition) {
        if (this.soupCondition != null) {
            this.soupCondition = this.soupCondition.and(soupCondition);
        } else {
            this.soupCondition = soupCondition;
        }
    }

    public boolean isReducedFallDamage() {
        return reducedFallDamage;
    }

    public void setReducedFallDamage(boolean reducedFallDamage) {
        this.reducedFallDamage = reducedFallDamage;
    }

    public boolean isAbilities() {
        return abilities;
    }

    public void setAbilities(boolean abilities) {
        this.abilities = abilities;
    }

    public boolean isHunger() {
        return hunger;
    }

    public void setHunger(boolean hunger) {
        this.hunger = hunger;
    }

    public void setItemDespawnRate(World world, int despawnRate) {
        NMSUtils.get(world).spigotConfig.itemDespawnRate = despawnRate;
    }

    public boolean isTerrainControl() {
        return terrainControl;
    }

    public void setTerrainControl(boolean terrainControl) {
        this.terrainControl = terrainControl;
    }

    public boolean isNaturalRegeneration() {
        return naturalRegeneration;
    }

    public void setNaturalRegeneration(boolean naturalRegeneration) {
        this.naturalRegeneration = naturalRegeneration;
    }

    public boolean isAllowNaturalBlockChange() {
        return allowNaturalBlockChange;
    }

    public void setAllowNaturalBlockChange(boolean allowNaturalBlockChange) {
        this.allowNaturalBlockChange = allowNaturalBlockChange;
    }

    public boolean isAllowQuitMessage() {
        return allowQuitMessage;
    }

    public void setAllowQuitMessage(boolean allowQuitMessage) {
        this.allowQuitMessage = allowQuitMessage;
    }

    public boolean isAllowJoinMessage() {
        return allowJoinMessage;
    }

    public void setAllowJoinMessage(boolean allowJoinMessage) {
        this.allowJoinMessage = allowJoinMessage;
    }

    public void enableAlternativeRecraft() {
        ALTERNATIVE_RECRAFT_MODULE.register();
    }

    public void disableAlternativeRecraft() {
        ALTERNATIVE_RECRAFT_MODULE.unregister();
    }

    public static class CustomRecipeModule implements DojoListener {

        protected ShapelessRecipe recipe;

        public CustomRecipeModule(ShapelessRecipe recipe) {
            this.recipe = recipe;
        }

        @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
        public void onCraft(CraftItemEvent e) {
            if (e.getRecipe().equals(recipe)) {
                PvPDojo.schedule(() -> ((Player) e.getWhoClicked()).updateInventory()).nextTick();
            }
        }

        @Override
        public void register() {
            DojoListener.super.register();
            Bukkit.addRecipe(recipe);
        }

        @Override
        public void unregister() {
            DojoListener.super.unregister();

            Iterator<Recipe> recipeIterator = Bukkit.recipeIterator();
            while (recipeIterator.hasNext()) {
                Recipe recipe = recipeIterator.next();
                if (recipe.equals(this.recipe)) {
                    recipeIterator.remove();
                }
            }
        }

    }

    private static class AlternativeRecraftModule {

        List<CustomRecipeModule> recipeModules = new ArrayList<>();

        {
            ShapelessRecipe milkrecipe;
            ShapelessRecipe cmilkrecipe;

            milkrecipe = new ShapelessRecipe(new ItemBuilder(Material.MUSHROOM_SOUP).name(CC.WHITE + "Cacti Juice").build());
            milkrecipe.addIngredient(2, Material.CACTUS);
            milkrecipe.addIngredient(Material.BOWL);

            cmilkrecipe = new ShapelessRecipe(new ItemBuilder(Material.MUSHROOM_SOUP).name(CC.WHITE + "Chocolate Milk").build());
            cmilkrecipe.addIngredient(1, Material.INK_SACK, 3);
            cmilkrecipe.addIngredient(Material.BOWL);

            recipeModules.addAll(Arrays.asList(new CustomRecipeModule(milkrecipe), new CustomRecipeModule(cmilkrecipe)));
        }


        public void register() {
            recipeModules.forEach(CustomRecipeModule::register);
        }

        public void unregister() {
            recipeModules.forEach(CustomRecipeModule::unregister);
        }
    }
}
