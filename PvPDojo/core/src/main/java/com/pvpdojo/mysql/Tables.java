/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.mysql;

public enum Tables implements Table {

    USERS("users", "uuid VARCHAR(36), " + "name VARCHAR(16), " + "ip VARCHAR(15), " + "rank VARCHAR(25), " + "rankexpire DATETIME, " + "id INTEGER PRIMARY KEY AUTO_INCREMENT, "
            + "elo INTEGER, " + "clan VARCHAR(7), " + "kills INTEGER, " + "deaths INTEGER, " + "maxkillstreak INTEGER, " + "money INTEGER, " + "achievement_points INTEGER, "
            + "firstlogin DATETIME, " + "lastlogin DATETIME, " + "lastclanswitch DATETIME, " + "achievements TEXT, " + "kitslots INTEGER, playtime BIGINT(20), " + "KEY(uuid)"),

    CLANS("clans", "id INT(11) PRIMARY KEY AUTO_INCREMENT, name VARCHAR(7), members VARCHAR(500), leaders VARCHAR(500), elo INTEGER, UNIQUE (name)"),

    KITS("kits", "uuid VARCHAR(36), " + "ID INTEGER PRIMARY KEY AUTO_INCREMENT, " + "champion SMALLINT, " + "champion_name VARCHAR(20), " + "playstyle TINYINT, "
            + "abilities VARCHAR(75), " + "inventory TEXT"),

    ABILITIES("abilities", "uuid VARCHAR(36), " + "ID INTEGER PRIMARY KEY AUTO_INCREMENT, " + "ability_uuid VARCHAR(36), " + "ability_id SMALLINT, "
            + "ability_grade TINYINT DEFAULT 1, " + "KEY (uuid), " + "UNIQUE  KEY (ability_uuid)"),

    MARKET("market", "ability_uuid VARCHAR(36), " + "ID INTEGER PRIMARY KEY AUTO_INCREMENT, " + "owner VARCHAR(36), " + "price INTEGER, " + "date DATETIME"),

    ACCOUNT_GRID("alternativeaccs", "uuid VARCHAR(36), id INT(11) AUTO_INCREMENT, ip VARCHAR(15), lastupdate DATETIME, PRIMARY KEY (uuid), KEY (id), KEY (ip)"),

    PUNISHMENT("punishment", "id INT(11) PRIMARY KEY AUTO_INCREMENT, " + "punished VARCHAR(36), " + "executor VARCHAR(36), " + "reason VARCHAR(255), " + "type TINYINT, "
            + "date DATETIME, " + "length BIGINT(20), " + "update_length BIGINT(20) DEFAULT '-2', " + "update_executor VARCHAR(36) DEFAULT NULL, "
            + "update_reason VARCHAR(255) DEFAULT NULL, " + "valid BIT(1) DEFAULT b'1'," + "KEY (punished)"),

    EYEHAWK("eyehawk_logs", "uuid VARCHAR(36), " + "name VARCHAR(16), " + "message VARCHAR(100), " + "date DATETIME, " + "level VARCHAR(36), "
            + "id INT(11) PRIMARY KEY AUTO_INCREMENT, " + "KEY (uuid)"),

    MAPS("maps", "id INT(11) PRIMARY KEY AUTO_INCREMENT, name VARCHAR(255), type VARCHAR(20), data TEXT"),

    REPLAY("replay", "id INT PRIMARY KEY AUTO_INCREMENT, date DATETIME, staff BIT(1)"),

    SESSION("gamesession", "id INT PRIMARY KEY AUTO_INCREMENT, date DATETIME, data LONGTEXT"),

    SESSION_UUID("gamesession_uuid", "id INT PRIMARY KEY AUTO_INCREMENT, uuid VARCHAR(36), session_id INT, KEY (uuid)"),

    ABILITY_LOG("abilitylog", "id INT PRIMARY KEY AUTO_INCREMENT, " + "ability_uuid VARCHAR(36), " + "previous_owner VARCHAR(36), "
            + "new_owner VARCHAR(36), " + "reason VARCHAR(255), " + "date DATETIME, KEY (ability_uuid)"),

    HG_STATS("hgstats", "id INT PRIMARY KEY AUTO_INCREMENT, " + "uuid VARCHAR(36), " + "kills INT DEFAULT 0, " + "deaths INT DEFAULT 0, " + "wins INT DEFAULT 0, "
            + "bonuskit_wins INT DEFAULT 0," + "killrecord INT DEFAULT 0, " + "gamesplayed INT DEFAULT 0, " + "money INT DEFAULT 100, " + "playtime BIGINT(20) DEFAULT 0, "
            + "UNIQUE KEY (uuid)"),

    HG_KITS("hgkits", "id INT PRIMARY KEY AUTO_INCREMENT, " + "uuid VARCHAR(36), " + "kit VARCHAR(20), " + "UNIQUE KEY kit (uuid, kit)"),

    CHAT_MESSAGES("chat_log", "messageid INT PRIMARY KEY AUTO_INCREMENT, senderid INTEGER, message VARCHAR(255), date DATETIME, KEY (senderid)"),
    CHAT_RECEIVERS("chat_receivers", "receiverid INT, messageid INT, KEY (receiverid)"),

    DISCORD_LINK("discord_link", "userid INT PRIMARY KEY, discordid VARCHAR(50), UNIQUE KEY (discordid), date DATETIME");

    private String name;
    private String tableAttributes;

    Tables(String name, String tableAttributes) {
        this.name = name;
        this.tableAttributes = tableAttributes;
    }

    public String getName() {
        return name;
    }

    public String getTableAttributes() {
        return tableAttributes;
    }

    @Override
    public String toString() {
        return getName();
    }
}
