/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.mysql;

import java.sql.SQLException;
import java.util.UUID;

import com.pvpdojo.DBCommon;
import com.pvpdojo.util.Log;

public enum EloSeason implements Table {

    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE;

    @Override
    public String getName() {
        return "eloseason_" + name().toLowerCase();
    }

    @Override
    public String getTableAttributes() {
        return "userid INT PRIMARY KEY, elo INT DEFAULT 1400, KEY (elo)";
    }

    @Override
    public String toString() {
        return getName();
    }
    
    public void resetElo(UUID uuid) {
        try (SQLBuilder sql = new SQLBuilder()) {
            sql.delete(this).where("userid = ?").executeUpdate(DBCommon.getUserId(uuid).orElseThrow(SQLException::new));
        } catch (SQLException e) {
            Log.exception(e);
        }
    }
}
