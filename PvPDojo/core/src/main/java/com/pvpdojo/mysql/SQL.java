/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.mysql;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.util.Log;

import co.aikar.idb.DB;

public class SQL {

    public static int updateMultiple(Table table, String update, String condition, Object... params) {
        String query = "UPDATE " + table + " SET " + update + " WHERE " + condition;
        try {
            return DB.executeUpdate(query, params);
        } catch (SQLException ex) {
            Log.exception(query, ex);
        }
        return 0;
    }

    public static int updateObject(Table table, String column, String condition, Object... params) {
        String query = "UPDATE " + table + " SET " + column + " = ? WHERE " + condition;
        try {
            return DB.executeUpdate(query, params);
        } catch (SQLException ex) {
            Log.exception(query + " | " + Arrays.toString(params), ex);
        }
        return 0;
    }

    /**
     * @param table
     * @param column
     * @param condition
     * @param params
     * @return the Result
     * @throws SQLException if the query failed.
     */
    public static <T> T getFirstColumn(Table table, String column, String condition, Object... params) throws SQLException {
        return DB.getFirstColumn("SELECT " + column + " FROM " + table + " WHERE " + condition, params);
    }

    public static void updateInteger(Table table, String column, String condition, Object... params) {
        updateObject(table, column, condition, params);
    }

    public static int updateString(Table table, String column, String condition, Object... params) {
        return updateObject(table, column, condition, params);
    }

    public static boolean checkRowExist(Table table, String condition, Object... params) throws SQLException {
        try (SQLBuilder builder = new SQLBuilder()) {
            return builder.select(table, "1").where(condition).executeQuery(params).next();
        }
    }

    public static Object getObject(Table table, String column, String condition, Object... params) throws SQLException {
        try (SQLBuilder builder = new SQLBuilder()) {
            builder.select(table, column).where(condition).executeQuery(params);
            if (builder.next()) {
                return builder.getObject(column);
            }
            return null;
        }
    }

    public static List<?> getObjects(Table table, String column, String condition, Object... params) throws SQLException {
        try (SQLBuilder builder = new SQLBuilder()) {
            List<Object> elements = new ArrayList<>();
            builder.select(table, column).where(condition).executeQuery(params);
            while (builder.next()) {
                elements.add(builder.getObject(column));
            }
            return elements;
        }
    }

    public static OptionalInt getInteger(Table table, String column, String condition, Object... params) throws SQLException {
        Object obj = getObject(table, column, condition, params);
        return obj == null ? OptionalInt.empty() : OptionalInt.of((int) obj);
    }

    @SuppressWarnings("unchecked")
    public static List<Integer> getIntegers(Table table, String column, String condition, Object... params) throws SQLException {
        return (List<Integer>) getObjects(table, column, condition, params);
    }

    public static String getString(Table table, String column, String condition, Object... params) throws SQLException {
        return (String) getObject(table, column, condition, params);
    }

    @SuppressWarnings("unchecked")
    public static List<String> getStrings(Table table, String column, String condition, Object... params) throws SQLException {
        return (List<String>) getObjects(table, column, condition, params);
    }

    public static void insert(Table table, String values, Object... params) throws SQLException {
        String query = "INSERT INTO " + table + " VALUES(" + values + ")";
        try {
            DB.executeInsert(query, params);
        } catch (SQLException ex) {
            throw new SQLException(query, ex);
        }
    }

    public static Long insertOnDuplicate(Table table, String values, String duplicateKeyAction, Object... params) throws SQLException {
        String query = "INSERT INTO " + table + " VALUES(" + values + ") ON DUPLICATE KEY " + duplicateKeyAction;
        try {
            return DB.executeInsert(query, params);
        } catch (SQLException ex) {
            throw new SQLException(query, ex);
        }
    }

    public static void handleException(Throwable thrown, Consumer<Throwable> call) {
        if (call != null) {
            call.accept(thrown);
        } else {
            Log.exception("", thrown);
        }
    }

    public static void handleException(Throwable thrown, BiConsumer<?, Throwable> call) {
        if (call != null) {
            call.accept(null, thrown);
        } else {
            Log.exception("", thrown);
        }
    }

    public static void createTable(Table table) throws SQLException {
        PvPDojo.catchMainThread("SQL");
        SQLBackend.getInstance().createTable(table.getName(), table.getTableAttributes());
    }

    public static int remove(Table table, String condition, Object... params) throws SQLException {
        PvPDojo.catchMainThread("SQL");
        try (SQLBuilder builder = new SQLBuilder()) {
            return builder.delete(table).where(condition).executeUpdate(params);
        }
    }

    public static void checkDatabase() {
        try {
            for (Tables tables : Tables.values()) {
                SQL.createTable(tables);
            }
            for (Table table : EloSeason.values()) {
                SQL.createTable(table);
            }
        } catch (SQLException ex) {
            Log.exception("Init DB Frames", ex);
            BukkitUtil.shutdown();
        }
    }
}
