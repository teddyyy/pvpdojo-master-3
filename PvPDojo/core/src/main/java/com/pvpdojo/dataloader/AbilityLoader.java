/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.dataloader;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.inventory.ItemStack;

import com.pvpdojo.DeveloperSettings;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilityinfo.AbilityCollection;
import com.pvpdojo.abilityinfo.AbilityRarity;
import com.pvpdojo.abilityinfo.AbilityUpgrade;
import com.pvpdojo.abilityinfo.KitAttributeArray;
import com.pvpdojo.abilityinfo.KitAttributeArray.KitAttribute;
import com.pvpdojo.bukkit.util.GuiItem;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.Log;

public class AbilityLoader {

    private String name;
    private String abilityDescription;
    private String abilityExecutor;
    private boolean enabled;
    private KitAttributeArray abilityBaseAttributes;
    private Set<AbilityUpgrade> abilityUpgrades;
    private AbilityRarity abilityRarity;
    private AbilityCollection abilityCollection;
    private int abilityId;
    private Float abilityBasePoints;
    private boolean active;
    private ItemStack abilityIcon;

    public String getAbilityDescription() {
        return abilityDescription;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return getRarity().getColor() + getName();
    }

    public AbilityCollection getCollection() {
        return abilityCollection;
    }

    public AbilityRarity getRarity() {
        return abilityRarity;
    }

    public float getAbilityBasePoints() {
        return abilityBasePoints;
    }

    public String getAbilityExecutor() {
        return abilityExecutor;
    }

    public int getId() {
        return abilityId;
    }

    public ItemStack getAbilityIcon() {
        return abilityIcon.clone();
    }

    public boolean isActive() {
        return active;
    }

    public Set<AbilityUpgrade> getAbilityUpgrades() {
        return abilityUpgrades;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public AbilityUpgrade getAbilityUpgrade(int number) {
        for (AbilityUpgrade upgrade : abilityUpgrades) {
            if (upgrade.upgrade_number == number)
                return upgrade;
        }
        return null;
    }

    public float getAbilityAttribute(String attribute, Set<Integer> enabled_upgrades) {
        float baseValue = this.abilityBaseAttributes.getKitAttribute(attribute).getAttributeValue();
        float upgradeValue = 0.0f;
        Set<AbilityUpgrade> abilityUpgrades = getAbilityUpgrades();
        if (enabled_upgrades != null) {
            for (AbilityUpgrade abilityUpgrade : abilityUpgrades) {
                if (!enabled_upgrades.contains(abilityUpgrade.upgrade_number))
                    continue;
                KitAttributeArray array = abilityUpgrade.stats;
                if (array.hasAttribute(attribute)) {
                    upgradeValue += array.getKitAttribute(attribute).getAttributeValue();
                }
            }
        }
        return baseValue + upgradeValue;
    }

    public KitAttributeArray getAbilityBaseAttributes() {
        return abilityBaseAttributes;
    }

    private static Map<Integer, AbilityLoader> abilities = new HashMap<>();
    private static Map<String, AbilityLoader> abilityname = new HashMap<>();

    public static Collection<AbilityLoader> getAbilities() {
        return abilities.values();
    }

    public static AbilityLoader getAbilityData(String name) {
        return abilityname.get(name);
    }

    public static AbilityLoader getAbilityData(int id) {
        return abilities.get(id);
    }

    public static AbilityLoader loadAbility(String ability) {
        AbilityFileLoader loader = new AbilityFileLoader("/abilitydata/" + ability + ".txt");
        loader.loadDataMap();
        HashMap<String, String> map = loader.getDataMap();
        AbilityLoader data = new AbilityLoader();
        data.name = map.get("name");
        data.abilityId = Integer.valueOf(map.get("id"));
        if (AbilityLoader.getAbilityData(data.abilityId) != null)
            throw new IllegalArgumentException("An ability with the id: " + data.abilityId + " already exists: " + ability);
        data.abilityDescription = map.get("description");
        data.abilityExecutor = map.get("executor");
        data.abilityRarity = AbilityRarity.valueOf(map.get("rarity"));
        data.abilityCollection = AbilityCollection.valueOf(map.get("collection"));
        data.abilityBaseAttributes = loader.getBaseStats();
        data.abilityBasePoints = Float.valueOf(map.get("points"));
        data.abilityUpgrades = loader.getUpgradeStats();
        String abilityType = map.get("ability_type");
        if (abilityType.equals("active")) {
            data.active = true;
        } else if (!abilityType.equals("passive")) {
            throw new IllegalArgumentException("An ability with the id: " + data.abilityId + " has invalid abilityType: " + ability);
        }

        data.enabled = Boolean.valueOf(map.get("enabled"));
        data.abilityIcon = new ItemBuilder(GuiItem.fromString(map.get("icon"), data.getRarity().getColor() + data.name + " Ability")).lore(data.fixAbilityDescription(null, null)).build();
        abilities.put(data.abilityId, data);
        abilityname.put(data.name, data);
        return data;
    }

    public List<String> fixAbilityDescription(User user, Set<Integer> upgrades) {
        boolean includeUpgrades = upgrades != null;
        String type = includeUpgrades ? "Total " : "Base ";
        String description = abilityDescription;

        if (!User.getLocale(user).equals(PvPDojo.LANG.getDefaultLocale())) {
            String localeDesc = PvPDojo.LANG.formatMessage(user, MessageKeys.GENERIC, "{text}", "{@@pvpdojo.ability_description." + getAbilityExecutor().toLowerCase() + "}");
            if (!localeDesc.equals("_")) {
                description = localeDesc;
            }
        }

        ArrayList<String> lines = new ArrayList<>();
        String line = description;
        for (KitAttribute stat : abilityBaseAttributes.getAttributeArray()) {
            if (line.contains("~" + stat.getAttributeName())) {
                line = line.replace("~" + stat.getAttributeName(), getColorByAttribute(stat) + Math.abs(getAbilityAttribute(stat.getAttributeName(), upgrades)));
                if (line.contains("(hearts)")) {
                    line = line.replace("(hearts)", CC.GRAY + "(" + CC.RED + (getAbilityAttribute(stat.getAttributeName(), upgrades) / (DeveloperSettings.HEALTH_CONVERTER * 2))
                            + " ❤ 's" + CC.GRAY + ")");
                }
            }

        }
        lines.add(CC.GRAY + line);
        lines.add("");
        float points = getAbilityBasePoints();
        if (includeUpgrades) {
            for (AbilityUpgrade upgrade : getAbilityUpgrades()) {
                if (upgrades.contains(upgrade.getUpgradeNumber())) {
                    points = points + upgrade.getPoints();
                }
            }
        }
        lines.add(CC.GRAY + type + "Points: " + CC.GOLD + points + " points");
        if (getAbilityBaseAttributes().hasAttribute("mana_use")) {
            if (getAbilityBaseAttributes().getKitAttribute("mana_use").getAttributeValue() > 0) {
                float mana_use = getAbilityAttribute("mana_use", upgrades);
                lines.add(CC.GRAY + type + "Mana Use: " + CC.BLUE + mana_use + " mana");
            }
        }
        if (getAbilityBaseAttributes().hasAttribute("cooldown")) {
            float cooldown = getAbilityAttribute("cooldown", upgrades);
            lines.add(CC.GRAY + type + "Cooldown: " + CC.DARK_PURPLE + cooldown + " seconds");
        }
        if (getAbilityBaseAttributes().hasAttribute("ability_damage")) {
            float damage = getAbilityAttribute("ability_damage", upgrades);
            if (damage > 0)
                lines.add(CC.GRAY + type + "Damage: " + CC.RED + (damage / (DeveloperSettings.HEALTH_CONVERTER * 2)) + " ❤ 's");
        }

        return lines;
    }

    public static String getColorByAttribute(KitAttribute attribute) {
        if (attribute.getAttributeName().equals("cooldown")) {
            return CC.DARK_PURPLE.toString();
        } else if (attribute.getAttributeName().equals("mana_use")) {
            return CC.BLUE.toString();
        } else if (attribute.getAttributeName().endsWith("damage") || attribute.getAttributeName().equals("dps")) {
            return CC.DARK_RED.toString();
        } else
            return CC.GREEN.toString();
    }

    public static void loadAllAbilities() {
        abilities.clear();
        File f = new File("");
        File[] files = new File(f.getAbsolutePath() + "/abilitydata/").listFiles();
        int i = 0;
        for (File file : files) {
            if (file.getName().equalsIgnoreCase("example.txt") || file.isDirectory())
                continue;
            try {
                loadAbility(file.getName().replace(".txt", ""));
                i++;
            } catch (Throwable throwable) {
                Log.exception("Could not load ability file " + file.getName(), throwable);
            }
        }
        Log.info("Successfully loaded " + i + " abilities");
    }

}