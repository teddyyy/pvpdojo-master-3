/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.dataloader;

import java.util.ArrayList;

import org.bukkit.inventory.ItemStack;

import com.pvpdojo.abilityinfo.KitAttributeArray;
import com.pvpdojo.abilityinfo.KitAttributeArray.AttributeArrayType;
import com.pvpdojo.bukkit.util.GuiItem;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public class KitLoader {

    private String kitName;
    private String kitDescription;
    private float kitId;
    private ItemStack kitIcon;
    private KitAttributeArray kitBaseAttributes;
    private KitAttributeArray kitIncrementAttributes;

    private KitLoader() {}

    public String getKitName() {
        return kitName;
    }

    public float getId() {
        return kitId;
    }

    public String getKitDescription() {
        return kitDescription;
    }

    public KitAttributeArray getKitBaseAttributes() {
        return kitBaseAttributes;
    }

    public KitAttributeArray getKitIncrementAttributes() {
        return kitIncrementAttributes;
    }

    public ItemStack getChampionIcon() {
        return kitIcon.clone();
    }

    public static void loadChampion(String kitName, float kitId, String kitDescription, String kitIcon, KitAttributeArray kitBaseAttributes, KitAttributeArray kitIncrementAttributes) {
        KitLoader data = new KitLoader();
        data.kitName = kitName;
        data.kitId = kitId;
        data.kitDescription = kitDescription;
        data.kitBaseAttributes = kitBaseAttributes;
        data.kitIncrementAttributes = kitIncrementAttributes;
        data.kitIcon = GuiItem.fromString(kitIcon, CC.GREEN + kitName);
        new ItemBuilder.ItemEditor(data.kitIcon).lore(kitDescription).build();
        KIT_BASE.add(data);
    }

    private static ArrayList<KitLoader> KIT_BASE = new ArrayList<>();

    static {
        loadChampion("Starter", 1, "description", "NETHER_STAR:0",
                KitAttributeArray.create(new String[]{"max_mana=250", "mana_regen=15.0", "max_health=500.0", "health_regen=0.0", "physical_damage=0.0", "move_speed=.2",
                        "attack_speed=2.0", "armor=0.0", "magic_resistance=0.0"}),
                KitAttributeArray.create(AttributeArrayType.BASE_ATTRIBUTES_INCREMENT, new String[]{"max_mana=20", "mana_regen=.3", "max_health=30.0", "health_regen=0.2",
                        "physical_damage=5.0", "move_speed=0.0", "attack_speed=0.1", "armor=2.0", "magic_resistance=2.0"}));
    }

    public static boolean isChampionLoaded(int id) {
        for (KitLoader champion : KIT_BASE) {
            if (champion.getId() == id) {
                return true;
            }
        }
        return false;
    }

    public static KitLoader getKitData(int id) {
        for (KitLoader champion : KIT_BASE) {
            if (champion.getId() == id) {
                return champion;
            }
        }
        throw new IllegalArgumentException("Invalid Champion or Gamemode: " + id);
    }

}
