/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.dataloader;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.pvpdojo.abilityinfo.AbilityUpgrade;
import com.pvpdojo.abilityinfo.KitAttributeArray;
import com.pvpdojo.abilityinfo.KitAttributeArray.AttributeArrayType;
import com.pvpdojo.abilityinfo.KitAttributeArray.KitAttribute;
import com.pvpdojo.abilityinfo.KitAttributeArray.KitBooleanAttribute;
import com.pvpdojo.util.FileUtil;
import com.pvpdojo.util.MathUtil;

public class AbilityFileLoader {

    private HashMap<String, String> map = new HashMap<>();
    private String file_name;

    public AbilityFileLoader(String file) {
        this.file_name = file;
    }

    public HashMap<String, String> getDataMap() {
        return map;
    }

    public void loadDataMap() {
        File file = new File("");
        file = new File(file.getAbsolutePath() + file_name);
        ArrayList<String> lines = FileUtil.getFileLines(file);
        for (String line : lines) {
            if (line.isEmpty())
                continue;
            String[] split = line.split("=");
            String key = split[0];
            String value = split[1];
            map.put(key, value);
        }
    }

    public KitAttributeArray getBaseStats() {
        KitAttributeArray array = new KitAttributeArray();
        for (String keys : map.keySet()) {
            if (keys.startsWith("base-stat-")) {
                String[] split = keys.split("-");
                String attribute = split[2];
                String value = map.get(keys);
                if (MathUtil.isBoolean(value)) {
                    KitBooleanAttribute bool = new KitBooleanAttribute(attribute, Boolean.valueOf(value));
                    array.addAttribute(bool);
                } else
                    array.addAttribute(new KitAttribute(attribute, Float.valueOf(value), AttributeArrayType.BASE_ATTRIBUTES));
            }
        }
        return array;
    }

    public Set<AbilityUpgrade> getUpgradeStats() {
        Set<AbilityUpgrade> upgrades = this.createUpgradeStats();
        for (String keys : map.keySet()) {
            if (keys.startsWith("upgrade-")) {
                String[] split = keys.split("-");
                int number = Integer.valueOf(split[1]);
                String type = split[2];
                String value = map.get(keys);
                if (type.equals("name"))
                    continue;
                AbilityUpgrade upgrade = null;
                for (AbilityUpgrade u : upgrades) {
                    if (u.getUpgradeNumber() == number) {
                        upgrade = u;
                    }
                }
                switch (type) {
                    case "stat":
                        String attribute = split[3];
                        if (MathUtil.isBoolean(value)) {
                            KitBooleanAttribute bool = new KitBooleanAttribute(attribute, Boolean.valueOf(value));
                            upgrade.getStats().addAttribute(bool);
                        } else {
                            upgrade.getStats().addAttribute(new KitAttribute(attribute, Float.valueOf(value), AttributeArrayType.BASE_ATTRIBUTES));
                        }
                        break;
                    case "points":

                        upgrade.points = Float.valueOf(value.replace(".0", ""));
                        break;
                    case "description":
                        upgrade.description = value;
                        break;
                }
            }

        }
        return upgrades;
    }

    private Set<AbilityUpgrade> createUpgradeStats() {
        Set<AbilityUpgrade> upgrades = new HashSet<>();
        for (String keys : map.keySet()) {
            if (keys.startsWith("upgrade-")) {
                String[] split = keys.split("-");
                int number = Integer.valueOf(split[1]);
                String type = split[2];
                if (type.equals("name")) {
                    upgrades.add(new AbilityUpgrade(number, map.get(keys)));
                }
            }
        }
        return upgrades;
    }

}
