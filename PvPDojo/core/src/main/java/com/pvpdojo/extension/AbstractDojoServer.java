/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.extension;

import java.util.UUID;

import org.bukkit.entity.Player;

import com.pvpdojo.ServerType;
import com.pvpdojo.UserList;
import com.pvpdojo.map.DojoMap;
import com.pvpdojo.userdata.User;

public interface AbstractDojoServer<T extends User> {

    ServerType getServerType();

    void start();

    void postLoad();

    DojoMap setupMap();

    UserList<T> createUserList();

    UserList<T> getUserList();

    String getSubdomain();

    default T getUser(Player player) {
        return getUserList().getUser(player);
    }

    default T getUser(UUID uuid) {
        return getUserList().getUser(uuid);
    }

    default boolean containsUser(UUID uuid) {
        return getUserList().getUsers().containsKey(uuid);
    }

    default T loginUser(UUID uuid) {
        return getUserList().loginUser(uuid);
    }

    default void logoutUser(Player player) {
        getUserList().logoutUser(player);
    }

}
