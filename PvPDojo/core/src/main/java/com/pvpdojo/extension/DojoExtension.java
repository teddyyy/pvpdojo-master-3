/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.extension;

import com.pvpdojo.UserList;
import com.pvpdojo.map.DojoMap;
import com.pvpdojo.userdata.User;

public abstract class DojoExtension<T extends User> {

    private DojoServer<T> server;

    public DojoExtension(DojoServer<T> server) {
        this.server = server;
    }

    public abstract void start();

    public abstract void postLoad();

    public abstract UserList<T> createUserList();

    public abstract DojoMap setupMap();

    public DojoServer<T> getServer() {
        return server;
    }

    public UserList<T> getUserList() {
        return getServer().getUserList();
    }
}
