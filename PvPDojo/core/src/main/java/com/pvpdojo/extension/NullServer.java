/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.extension;

import com.pvpdojo.ServerType;
import com.pvpdojo.UserList;
import com.pvpdojo.map.DojoMap;
import com.pvpdojo.userdata.User;
import com.pvpdojo.userdata.impl.UserImpl;

public class NullServer implements AbstractDojoServer<User> {

    private UserList<User> userList = new UserList<>(UserImpl::new);

    @Override
    public ServerType getServerType() {
        return ServerType.NONE;
    }

    @Override
    public void start() {}

    @Override
    public void postLoad() {}

    @Override
    public DojoMap setupMap() {
        return null;
    }

    @Override
    public UserList<User> createUserList() {
        return userList;
    }

    @Override
    public UserList<User> getUserList() {
        return userList;
    }

    @Override
    public String getSubdomain() {
        return null;
    }
}
