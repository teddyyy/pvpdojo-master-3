/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.extension;

import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerSettings;
import com.pvpdojo.UserList;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.FileUtil;
import com.pvpdojo.util.Log;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.managers.RegionManager;

public abstract class DojoServer<T extends User> extends JavaPlugin implements AbstractDojoServer<T> {

    private UserList<T> userList;

    @Override
    public void onLoad() {
        userList = createUserList();
        PvPDojo.get().setDojoServer(this);
        PvPDojo.SERVER_TYPE = getServerType();
        PvPDojo.get().register();
    }

    @Override
    public void onEnable() {
        try {
            start();
        } catch (Throwable throwable) {
            Log.exception("Starting Server", throwable);
        }
        PvPDojo.get().start();
        PvPDojo.get().setStandardMap(setupMap());
        postLoad();
    }


    public ServerSettings getSettings() {
        return PvPDojo.get().getServerSettings();
    }

    public void deleteFolder(String folder) {
        FileUtil.deleteFolder(folder);
    }

    public void clearRegions(World world) {
        RegionManager manager = WGBukkit.getRegionManager(world);
        manager.getRegions().keySet().forEach(manager::removeRegion);
    }

    public UserList<T> getUserList() {
        return userList;
    }

    public String getSubdomain() {
        return null;
    }

}
