/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.settings;

import java.util.HashMap;
import java.util.Map;

public class SettingsType<T extends Settings> {

    private static Map<Class<? extends Settings>, SettingsType<?>> classToType = new HashMap<>();

    public static final SettingsType<UserSettings> USER = new SettingsType<>(UserSettings.class, "usersettings_", -1);
    public static final SettingsType<FastDB> FAST_DB = new SettingsType<>(FastDB.class, "fastdb_", -1);
    public static final SettingsType<BotSettings> BOT = new SettingsType<>(BotSettings.class, "botsettings_", -1);
    public static final SettingsType<ServerSwitchSettings> SERVER_SWITCH = new SettingsType<>(ServerSwitchSettings.class, "serverswitchsettings_", 10);
    public static final SettingsType<MessageQueue> MESSAGE_QUEUE = new SettingsType<>(MessageQueue.class, "messagequeue_", 7 * 24 * 60 * 60);
    public static final SettingsType<CooldownSettings> COOLDOWN = new SettingsType<>(CooldownSettings.class, "cooldownsettings_", 7 * 24 * 60 * 60);

    public static <T extends Settings> SettingsType<?> getSettingsType(Class<T> clazz) {
        return classToType.get(clazz);
    }

    private Class<T> clazz;
    private String key;
    private int expireSeconds;

    SettingsType(Class<T> clazz, String key, int expireSeconds) {
        this.clazz = clazz;
        this.key = key;
        this.expireSeconds = expireSeconds;
        classToType.put(clazz, this);
    }

    public Class<T> getClazz() {
        return clazz;
    }

    public String getKey() {
        return key;
    }

    public int getExpireSeconds() {
        return expireSeconds;
    }
}
