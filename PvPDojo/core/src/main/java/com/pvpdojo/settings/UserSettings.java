/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.settings;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.DyeColor;

import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.EnumOrderAccess;

import lombok.Data;

@Data
public class UserSettings implements Settings {

    private Set<String> disabledMaps = new HashSet<>();
    private PrivacyMode privateMessages = PrivacyMode.ALL;
    private PrivacyMode inviteMode = PrivacyMode.ALL;
    private boolean particles = true;
    private boolean lightningAnimation;
    private boolean privateMessagesSound;
    private boolean scoreboard;

    private boolean showElo = true, showClan = true;

    private HGSettings hGSettings = new HGSettings();

    private Set<UUID> ignored = new HashSet<>();

    @Data
    public class HGSettings {
        private boolean rejoin = true;
        private boolean spectate = true;
        private ScoreboardMode scoreboardMode = ScoreboardMode.DEFAULT;

        public ScoreboardMode getScoreboardMode() {
            return scoreboardMode == null ? ScoreboardMode.DEFAULT : scoreboardMode;
        }
    }

    public enum ScoreboardMode implements EnumOrderAccess<ScoreboardMode> {
        NONE, SMALL, DEFAULT
    }

    public enum PrivacyMode implements EnumOrderAccess<PrivacyMode> {
        ALL(
                CC.GREEN,
                DyeColor.GREEN,
                "Accepting interaction from everyone you did not /ignore"),
        KNOWN(
                CC.DARK_PURPLE,
                DyeColor.PURPLE,
                "Only accepting interaction from known users (e.g. clan and party)"),
        NONE(
                CC.RED,
                DyeColor.RED,
                "Any interaction, no matter by whom it was requested, will be denied");

        private CC chatColor;
        private DyeColor dyeColor;
        private String description;

        PrivacyMode(CC chatColor, DyeColor dyeColor, String description) {
            this.chatColor = chatColor;
            this.dyeColor = dyeColor;
            this.description = description;
        }

        public CC getChatColor() {
            return chatColor;
        }

        public DyeColor getDyeColor() {
            return dyeColor;
        }

        public String getDescription() {
            return description;
        }

        public boolean test(User user, User requester) {
            if (requester.getRank().inheritsRank(Rank.TRIAL_MODERATOR)) {
                return true;
            }
            switch (this) {
                case ALL:
                    return !user.getSettings().getIgnored().contains(requester.getUniqueId());
                case NONE:
                    return false;
                case KNOWN:
                    return (user.getClan() != null && user.getClan().equals(requester.getClan())) || (user.getParty() != null && user.getParty().equals(requester.getParty()));
            }
            throw new IllegalStateException();
        }

        public boolean test(User user, PersistentData requester) {
            if (requester.getRank().inheritsRank(Rank.TRIAL_MODERATOR)) {
                return true;
            }
            switch (this) {
                case ALL:
                    return !user.getSettings().getIgnored().contains(requester.getUUID());
                case NONE:
                    return false;
                case KNOWN:
                    return (user.getClan() != null && user.getClan().getName().equals(requester.getClan()))
                            || (user.getParty() != null && user.getParty().getUsers().contains(requester.getUUID()));
            }
            throw new IllegalStateException();
        }
    }

}
