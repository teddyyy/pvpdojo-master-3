/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.settings;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import com.pvpdojo.Discord;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.menus.ChallengeMenu;
import com.pvpdojo.userdata.User;
import com.pvpdojo.userdata.stats.PersistentHGStats;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.NameMCUtil;
import com.pvpdojo.util.TimeUtil;

import lombok.Data;

@Data
public class FastDB implements Settings {

    private int currentSalesmanTransaction = 1;
    private long likedNameMC;
    private boolean wonHG, lostHG;

    /**
     * Basti Subs
     */
    private long lastSubCheck; // Last check to Basti Sub
    private long lastBastiReward;
    private boolean bastiSub; // Basti shit

    /**
     * Dojo Subs
     */
    private long lastDojoSubCheck;
    private long lastDojoReward;
    private boolean dojoSub;

    /**
     * This controls the auth key that has to be read via discord for a staff member
     */
    private String authKey;

    public boolean isRequireAuth() {
        return authKey != null;
    }

    public boolean checkNameMC(UUID uuid) {
        try {
            if (!hasLikedNameMC() && NameMCUtil.hasLiked(uuid)) {
                likedNameMC = System.currentTimeMillis();
                save(uuid);
                PvPDojo.schedule(() -> {
                    if (User.containsUser(uuid)) {
                        User.getUser(uuid).sendMessage(MessageKeys.NAMEMC_REWARD);
                    }
                    ChallengeMenu.rewardCollections(User.getUser(uuid), 1);
                    new PersistentHGStats(uuid).addMoney(50);
                }).sync();
            }
        } catch (IOException e) {
            Log.warn("Could not fetch NameMC like for " + uuid + " due to " + e.getMessage());
        }
        return hasLikedNameMC();
    }

    public boolean checkBastiSub(UUID uuid, String discordUUID) {
        if (TimeUtil.hasPassed(lastSubCheck, TimeUnit.DAYS, 1)) {
            bastiSub = Discord.get().isBastiSub(discordUUID);
            lastSubCheck = System.currentTimeMillis();
            save(uuid);
        }
        return isBastiSub();
    }

    public boolean checkDojoSub(UUID uuid, String discordUUID) {
        if (TimeUtil.hasPassed(lastDojoSubCheck, TimeUnit.DAYS, 1)) {
            dojoSub = Discord.get().isDojoSub(discordUUID);
            lastDojoSubCheck = System.currentTimeMillis();
            save(uuid);
        }
        return isDojoSub();
    }

    public boolean hasLikedNameMC() {
        return !TimeUtil.hasPassed(likedNameMC, TimeUnit.DAYS, 3);
    }
    
}
