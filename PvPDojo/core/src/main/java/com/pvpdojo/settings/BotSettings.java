/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.settings;

import com.pvpdojo.util.bukkit.CC;

public class BotSettings implements Settings {

    private Difficulty difficulty = Difficulty.EASY;
    private int soups = 32;
    private boolean oldBot = false;

    public int getSoups() {
        return soups;
    }

    public void addSoups(int amount) {
        soups += amount;
        if (soups < 32 || soups > 999) {
            soups = 32;
        }
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public boolean isOldBot() {
		return oldBot;
	}
    
    public void setOldBot(boolean oldBot) {
		this.oldBot = oldBot;
	}
    
    public enum Difficulty {
        EASY(CC.GREEN),
        MEDIUM(CC.YELLOW),
        HARD(CC.RED);

        private CC color;

        Difficulty(CC color) {
            this.color = color;
        }

        public CC getColor() {
            return color;
        }

        public static Difficulty getNext(Difficulty diff) {
            if (diff.ordinal() + 1 >= values().length) {
                return Difficulty.EASY;
            }
            return values()[diff.ordinal() + 1];
        }
    }

}
