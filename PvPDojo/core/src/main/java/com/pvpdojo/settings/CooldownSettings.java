/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.settings;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import com.pvpdojo.util.ExpireArrayList;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.Throttle;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CooldownSettings implements Settings {

    private long lastReport;
    private long lastLobbyStart;
    private transient Throttle chatThrottle = new Throttle(3, Duration.ofSeconds(10));
    private transient ExpireArrayList<String> lastChatMessages = new ExpireArrayList<>();

    public boolean canReport() {
        return System.currentTimeMillis() > this.lastReport + 30_000;
    }

    public synchronized boolean checkSimilarChatMessage(String message) {
        for (String lastMessage : lastChatMessages) {
            if (StringUtils.getJaroWinklerDistance(message, lastMessage) >= 0.85) {
                return true;
            }
        }
        lastChatMessages.add(message, 10, TimeUnit.SECONDS);
        return false;
    }

}
