/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.settings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.Warp;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.command.staff.AdminCommand;
import com.pvpdojo.command.staff.AdminCommand.AdminModeData;
import com.pvpdojo.party.Party;
import com.pvpdojo.party.PartyList;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.userdata.NickData;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.Request;
import com.pvpdojo.userdata.User;

import net.minecraft.util.com.mojang.authlib.GameProfile;
import net.minecraft.util.com.mojang.authlib.properties.Property;

public class ServerSwitchSettings implements Settings {

    private ChatChannel chatChannel;
    private boolean adminMode, specMode, showSpectators, fakeAbilities;
    private UUID teleportTo;
    private Party party;
    private NickData nickData;
    private Rank spoofedRank;
    private Warp warp;
    private List<String> commands;
    private Map<UUID, Request> persistentRequests = new HashMap<>();

    public ServerSwitchSettings(User user) {
        AdminModeData adminModeData = AdminCommand.DATA_SAVE.remove(user.getUUID());
        chatChannel = user.isAdminMode() && adminModeData != null ? adminModeData.getChatChannel() : user.getChatChannel();

        spoofedRank = user.getSpoofedRankDirect();
        showSpectators = user.isShowSpectators();
        party = user.getParty();
        fakeAbilities = user.hasFakeAbilities();
        if (user.getPlayer().isNicked()) {
            GameProfile nickProfile = NMSUtils.getCraftPlayer(user.getPlayer()).nickGameProfile;
            Property prop = nickProfile.getProperties().get("textures").iterator().next();
            nickData = new NickData(nickProfile.getId(), user.getName(), prop.getValue(), prop.getSignature());
        }
        user.getRequests().forEach((uuid, request) -> {
            if (request.isPersistent()) {
                persistentRequests.put(uuid, request);
            }
        });
    }

    public void applyFirst(User user) {
        if (nickData != null) {
            user.getPlayer().setNameAndSkin(nickData.getUniqueId(), nickData.getName(), nickData.getSkinValue(), nickData.getSkinSignature());
        }
        user.setSpoofedRank(spoofedRank);
        user.setFakeAbilities(fakeAbilities);

        if (party != null) {
            PartyList.addParty(party);
            Party listedParty = PartyList.getPartyById(party.getPartyId());
            if (listedParty.getUsers().contains(user.getUUID())) {
                user.setParty(listedParty);
            }
        }
    }

    public void applyAfter(User user) {
        user.setShowSpectators(showSpectators);
        if (specMode) {
            user.setSpectator(true);
        }
        user.setChatChannel(chatChannel);
        user.setAdminMode(adminMode);
        user.getRequests().putAll(persistentRequests);
        if (warp != null) {
            user.handleWarp(warp);
        }
        if (teleportTo != null) {
            Player target = Bukkit.getPlayer(teleportTo);
            if (target != null) {
                user.teleport(target.getLocation());
            } else {
                user.sendMessage(PvPDojo.WARNING + "Could not teleport to desired target. Did they log out?");
            }
        }
        if (commands != null && !commands.isEmpty()) {
            commands.forEach(user.getPlayer()::performCommand);
        }
    }

    public ChatChannel getChatChannel() {
        return chatChannel;
    }

    public ServerSwitchSettings chatChannel(ChatChannel chatChannel) {
        this.chatChannel = chatChannel;
        return this;
    }

    public boolean isAdminMode() {
        return adminMode;
    }

    public ServerSwitchSettings adminMode(boolean adminMode) {
        this.adminMode = adminMode;
        return this;
    }

    public boolean isSpecMode() {
        return specMode;
    }

    public void setSpecMode(boolean specMode) {
        this.specMode = specMode;
    }

    public UUID getTeleportTo() {
        return teleportTo;
    }

    public ServerSwitchSettings teleportTo(UUID teleportTo) {
        this.teleportTo = teleportTo;
        return this;
    }

    public Warp getWarp() {
        return warp;
    }

    public ServerSwitchSettings warp(Warp warp) {
        this.warp = warp;
        return this;
    }

    public ServerSwitchSettings addCommand(String command) {
        if (this.commands == null) {
            this.commands = new ArrayList<>();
        }
        this.commands.add(command);
        return this;
    }

}
