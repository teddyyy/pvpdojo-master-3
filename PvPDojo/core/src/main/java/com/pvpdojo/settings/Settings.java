/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.settings;

import java.util.UUID;

import com.pvpdojo.DBCommon;

public interface Settings {

    default void save(UUID uuid) {
        DBCommon.saveSettings(this, uuid);
    }

}
