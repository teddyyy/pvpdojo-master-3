/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.settings;

import java.util.ArrayList;
import java.util.List;

public class MessageQueue implements Settings {

    private List<String> messages = new ArrayList<>();

    public void addMessage(String msg) {
        messages.add(msg);
    }
    
    public List<String> getMessages() {
        return messages;
    }

}
