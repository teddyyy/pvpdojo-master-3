/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.gui;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.conversations.Conversation;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.util.Vector;

import com.comphenix.protocol.PacketType.Play.Server;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.nbt.NbtBase;
import com.comphenix.protocol.wrappers.nbt.NbtFactory;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.Log;

import net.minecraft.server.v1_7_R4.ChatComponentText;
import net.minecraft.server.v1_7_R4.ChatSerializer;

public class InputGUI {

    private String defaultText;
    protected ConfirmAction confirmAction;
    private CancelAction cancelAction;
    private Location fakeBlock;

    public InputGUI(String defaultText, ConfirmAction confirmAction, CancelAction cancelAction) {
        this.defaultText = defaultText;
        this.confirmAction = confirmAction;
        this.cancelAction = cancelAction;
    }

    public InputGUI(String defaultText, ConfirmAction confirmAction) {
        this(defaultText, confirmAction, () -> {});
    }

    public String getDefaultText() {
        return defaultText;
    }

    public void setDefaultText(String defaultText) {
        this.defaultText = defaultText;
    }

    public ConfirmAction getConfirmAction() {
        return confirmAction;
    }

    public CancelAction getCancelAction() {
        return cancelAction;
    }

    public void setFakeBlock(Location fakeBlock) {
        this.fakeBlock = fakeBlock;
    }

    public Location getFakeBlock() {
        return fakeBlock;
    }

    @SuppressWarnings("deprecation")
    public void setConfirmed(Player player, String input) {
        PvPDojo.schedule(() -> {
            Block block = this.fakeBlock.getBlock();
            User.getUser(player).setInputGui(null);
            player.sendBlockChange(this.fakeBlock, block.getTypeId(), block.getData());
            confirmAction.onConfirm(input);
        }).sync();
    }

    @SuppressWarnings("deprecation")
    public void setCancelled(Player player) {
        PvPDojo.schedule(() -> {
            User.getUser(player).setInputGui(null);
            cancelAction.onCancel();
            Block block = this.fakeBlock.getBlock();
            player.sendBlockChange(this.fakeBlock, block.getTypeId(), block.getData());
        }).sync();
    }

    public interface ConfirmAction {

        void onConfirm(String input);
    }

    public interface CancelAction {

        void onCancel();
    }

    @SuppressWarnings("deprecation")
    public void open(Player player) {

        if (player.getOpenInventory().getType() != InventoryType.CRAFTING && player.getOpenInventory().getType() != InventoryType.CREATIVE) {
            Log.info(player.getName() + " had " + player.getOpenInventory().getType() + " before InputGUI");
            PvPDojo.schedule(() -> {
                if (User.getUser(player).getMenu() != null) {
                    User.getUser(player).getMenu().setCloseable(true);
                }
                player.closeInventory();
                open(player);
            }).nextTick();
            return;
        }

        if (!player.isProtocolHack()) {
            User user = User.getUser(player);
            user.setInputGui(this);

            Location playerLoc = player.getLocation();
            Vector direction = playerLoc.getDirection().normalize().multiply(-3);
            setFakeBlock(playerLoc.add(direction));
            if (getFakeBlock().getY() <= 0)
                getFakeBlock().setY(1);

            player.sendBlockChange(getFakeBlock(), Material.COMMAND, (byte) 0);

            String text = getDefaultText();
            try {
                PacketContainer packet = new PacketContainer(Server.TILE_ENTITY_DATA);
                Location location = getFakeBlock();

                List<NbtBase<?>> tags = new ArrayList<>();
                tags.add(NbtFactory.of("id", "Control"));
                tags.add(NbtFactory.of("LastOutput", ChatSerializer.a(new ChatComponentText(text))));
                tags.add(NbtFactory.of("x", location.getBlockX()));
                tags.add(NbtFactory.of("y", location.getBlockY()));
                tags.add(NbtFactory.of("z", location.getBlockZ()));

                packet.getIntegers().write(0, location.getBlockX());
                packet.getIntegers().write(1, location.getBlockY());
                packet.getIntegers().write(2, location.getBlockZ());
                packet.getIntegers().write(3, 2);
                packet.getNbtModifier().write(0, NbtFactory.ofCompound("", tags));

                PacketContainer open = new PacketContainer(Server.OPEN_SIGN_ENTITY);

                open.getIntegers().write(0, location.getBlockX());
                open.getIntegers().write(1, location.getBlockY());
                open.getIntegers().write(2, location.getBlockZ());

                ProtocolLibrary.getProtocolManager().sendServerPacket(player, packet);
                ProtocolLibrary.getProtocolManager().sendServerPacket(player, open);

            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        } else {
            convertInputGUI(player).begin();
        }
    }

    public Conversation convertInputGUI(Player player) {
        return new ConversationFactory(PvPDojo.get()).withFirstPrompt(new Prompt() {

            @Override
            public String getPromptText(ConversationContext context) {
                return CC.DARK_PURPLE + "Chat Input" + PvPDojo.POINTER + CC.RESET + getDefaultText();
            }

            @Override
            public boolean blocksForInput(ConversationContext context) {
                return true;
            }

            @Override
            public Prompt acceptInput(ConversationContext context, String input) {
                getConfirmAction().onConfirm(input);
                return END_OF_CONVERSATION;
            }
        }).withModality(false).withTimeout(20).withEscapeSequence("cancel").withLocalEcho(false).buildConversation(player);
    }

}