/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.gui;

import java.util.function.IntConsumer;

import org.bukkit.entity.Player;

import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.ACFUtil;

public class NumberInputGUI extends InputGUI {

    private IntConsumer intConsumer;

    public NumberInputGUI(String defaultText, IntConsumer intConsumer, CancelAction cancelAction) {
        super(defaultText, null, cancelAction);
        this.intConsumer = intConsumer;
    }

    @Override
    public void open(Player player) {

        confirmAction = input -> {
            if (ACFUtil.isInteger(input)) {
                intConsumer.accept(Integer.valueOf(input));
            } else {
                setDefaultText(CC.RED + "Please enter a number");
                open(player);
            }
        };

        super.open(player);
    }
}
