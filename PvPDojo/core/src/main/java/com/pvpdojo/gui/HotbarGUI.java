/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.gui;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.util.TriConsumer;

public class HotbarGUI {

    private Map<Material, BiConsumer<Player, Action>> actionMap = new EnumMap<>(Material.class);
    private Map<Material, TriConsumer<Player, Action, Entity>> entityActionMap = new EnumMap<>(Material.class);
    private Map<Integer, Function<Player, ItemStack>> playerSpecificItemMap = new HashMap<>();
    private ItemStack[] items = new ItemStack[9];
    private boolean cancel = true;

    public ItemStack[] getItems(Player player) {
        playerSpecificItemMap.forEach((slot, item) -> items[slot] = item.apply(player));
        return items;
    }

    public BiConsumer<Player, Action> getAction(Material material) {
        return actionMap.get(material);
    }

    public TriConsumer<Player, Action, Entity> getEntityAction(Material material) {
        return entityActionMap.get(material);
    }

    public void setItem(int slot, ItemStack item, BiConsumer<Player, Action> action) {
        items[slot] = item;
        actionMap.put(item.getType(), action);
    }

    public void setItem(int slot, ItemStack item, TriConsumer<Player, Action, Entity> action) {
        items[slot] = item;
        entityActionMap.put(item.getType(), action);
    }

    public void setItem(int slot, Material mat, Function<Player, ItemStack> item, BiConsumer<Player, Action> action) {
        playerSpecificItemMap.put(slot, item);
        actionMap.put(mat, action);
    }

    public void setItem(int slot, Material mat, Function<Player, ItemStack> item, TriConsumer<Player, Action, Entity> action) {
        playerSpecificItemMap.put(slot, item);
        entityActionMap.put(mat, action);
    }

    public static TriConsumer<Player, Action, Entity> onClickPlayer(BiConsumer<Player, Player> consumer) {
        return (player, action, entity) -> {
            if (action == Action.RIGHT_CLICK_AIR && entity instanceof Player) {
                consumer.accept(player, (Player) entity);
            }
        };
    }

    public static BiConsumer<Player, Action> onRightClick(Consumer<Player> consumer) {
        return (player, action) -> {
            if (action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
                consumer.accept(player);
            }
        };
    }

    public boolean isCancel() {
        return cancel;
    }

    public void setCancel(boolean cancel) {
        this.cancel = cancel;
    }
}
