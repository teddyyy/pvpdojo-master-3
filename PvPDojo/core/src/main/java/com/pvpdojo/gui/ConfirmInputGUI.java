/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.gui;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder.WoolBuilder;

public class ConfirmInputGUI extends SingleInventoryMenu {

    private final String defaultText;
    private final Runnable action;

    public ConfirmInputGUI(Player player, String defaultText, Runnable action) {
        super(player, "Confirm", 9);
        this.defaultText = defaultText;
        this.action = action;
    }

    @Override
    public void redraw() {
        super.redraw();

        InventoryPage inv = getCurrentPage();

        inv.setItem(0, new WoolBuilder(DyeColor.RED).name(CC.RED + "Cancel").build(), getBackFunction() != null ? getBackFunction() : type -> closeSafely());
        inv.setItem(4, new ItemBuilder(Material.BOOK).name(CC.GRAY + "Confirm:").lore(CC.GRAY + defaultText).build(), null);
        inv.setItem(8, new WoolBuilder(DyeColor.GREEN).name(CC.GREEN + "Confirm").build(), type -> {
            action.run();
            closeSafely();
        });

        inv.fillBlank();
    }
}
