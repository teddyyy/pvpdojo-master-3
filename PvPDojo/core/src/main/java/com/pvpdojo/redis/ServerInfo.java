/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.redis;

import org.bukkit.entity.Player;

import com.pvpdojo.DBCommon;
import com.pvpdojo.session.network.SessionInfo;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.PlayerUtil;

import lombok.Data;

@Data
public class ServerInfo {

    private final String name;
    private final int players;

    private final String motd;
    private SessionInfo sessionInfo;

    public boolean hasSessionInfo() {
        return sessionInfo != null;
    }

    public void tryParseSession() {
        if (StringUtils.isJSONValid(motd)) {
            try {
                sessionInfo = DBCommon.GSON.fromJson(motd, SessionInfo.class);
            } catch (Exception ex) {
                Log.warn("Failed parsing SessionInfo from " + motd);
            }
        }
    }

    public void connect(Player player) {
        PlayerUtil.sendToServer(player.getUniqueId(), name);
    }

}
