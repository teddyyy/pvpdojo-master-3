/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.redis;

import com.pvpdojo.PvPDojo;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;
import redis.clients.jedis.params.SetParams;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@SuppressWarnings("resource")
public class Redis {

    private static Redis instance;
    JedisPool jedisPool;
    BungeeSync sync;
    Thread syncThread;
    boolean test;
    boolean usProxy;

    public static Redis get() {
        return instance;
    }

    private static JedisPoolConfig buildPoolConfig() {
        final JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(8);
        poolConfig.setMaxIdle(5);
        poolConfig.setMinIdle(1);
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(true);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setMinEvictableIdleTimeMillis(Duration.ofSeconds(60).toMillis());
        poolConfig.setTimeBetweenEvictionRunsMillis(Duration.ofSeconds(30).toMillis());
        poolConfig.setNumTestsPerEvictionRun(3);
        poolConfig.setBlockWhenExhausted(true);
        return poolConfig;
    }

    @SuppressWarnings("resource")
    public static void start() {
        if (instance != null)
            throw new IllegalStateException("Cannot create 2 redis backends");
        try {
            instance = new Redis();
            JedisPoolConfig config = buildPoolConfig();
            instance.sync = new BungeeSync();

            instance.test = PvPDojo.get().getConfig().getBoolean("test");
            instance.usProxy = PvPDojo.get().getConfig().getBoolean("usproxy");
            String host = PvPDojo.get().getConfig().getString("redis.host");
            int port = PvPDojo.get().getConfig().getInt("redis.port");
            String password = PvPDojo.get().getConfig().getString("redis.password");
            int db = PvPDojo.get().getConfig().getInt("redis.database");

            instance.jedisPool = new JedisPool(config, host, port, 0, password, db);

            instance.subscribeDefault();
            BungeeSync.retrieveServers();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void subscribeDefault() {
        syncThread = new Thread(() -> getResource().subscribe(instance.sync, getCh(BungeeSync.KICK), getCh(BungeeSync.USER_UPDATE), getCh(BungeeSync.MSG), getCh(BungeeSync.CHANNEL_CHAT),
                getCh(BungeeSync.FIND_PLAYER), getCh(BungeeSync.RESTART), getCh(BungeeSync.PROXY_JOIN_LEAVE), getCh(BungeeSync.CONNECT), getCh(BungeeSync.SESSION),
                getCh(BungeeSync.PARTY), getCh(BungeeSync.HEARTBEAT), getCh(BungeeSync.SHUTDOWN)), "BungeeSync-Read-Thread");
        syncThread.start();
    }

    public boolean isTest() {
        return test;
    }

    public boolean isUsProxy() {
        return usProxy;
    }

    public String getCh(String channel) {
        if (usProxy) {
            return BungeeSync.US_PROXY_PREFIX + channel;
        }
        return test ? "test_" + channel : channel;
    }

    public void shutdown() {
        publish(BungeeSync.UNREGISTER_SERVER, PvPDojo.get().getNetworkPointer());
        if (jedisPool != null) {
            sync.unsubscribe(getCh(BungeeSync.KICK), getCh(BungeeSync.USER_UPDATE), getCh(BungeeSync.MSG), getCh(BungeeSync.CHANNEL_CHAT),
                    getCh(BungeeSync.FIND_PLAYER), getCh(BungeeSync.RESTART), getCh(BungeeSync.PROXY_JOIN_LEAVE), getCh(BungeeSync.CONNECT), getCh(BungeeSync.SESSION),
                    getCh(BungeeSync.PARTY), getCh(BungeeSync.HEARTBEAT), getCh(BungeeSync.SHUTDOWN));
            try {
                syncThread.join();
            } catch (InterruptedException ignored) {
            }
            jedisPool.close();
        }
    }

    // https://www.programcreek.com/java-api-examples/index.php?source_dir=spring-redis-plugin-master/src/test/java/org/reindeer/redis/JedisTest.java

    public static Jedis getResource() {
        PvPDojo.catchMainThread("Redis access");
        return instance.jedisPool.getResource();
    }

    public void publish(String channel, String message) {
        PvPDojo.catchMainThread("Redis access");
        Jedis jedis = jedisPool.getResource();
        try {
            jedis.publish(getCh(channel), message);
        } finally {
            release(jedis);
        }
    }

    public void setNotExistIncrement(String key, int value, int increment, int expire) {
        PvPDojo.catchMainThread("Redis access");
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            String response = jedis.set(key, "" + value, SetParams.setParams().ex(expire).nx());
            if (response == null) {
                jedis.incrBy(key, increment);
            }
        } finally {
            release(jedis);
        }
    }

    public void setValue(String key, String value) {
        PvPDojo.catchMainThread("Redis access");
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.set(key, value);
        } finally {
            release(jedis);
        }
    }

    public void setValue(String key, String value, int seconds) {
        if (seconds == -1) {
            setValue(key, value);
            return;
        }
        PvPDojo.catchMainThread("Redis access");
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.setex(key, seconds, value);
        } finally {
            release(jedis);
        }
    }

    public void delete(String key) {
        PvPDojo.catchMainThread("Redis access");
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.del(key);
        } finally {
            release(jedis);
        }
    }

    public long getTTL(String key) {
        PvPDojo.catchMainThread("Redis access");
        Jedis jedis = null;
        long value;
        try {
            jedis = jedisPool.getResource();
            value = jedis.ttl(key);
        } finally {
            release(jedis);
        }
        return value;
    }

    public String getValue(String key) {
        PvPDojo.catchMainThread("Redis access");
        Jedis jedis = null;
        String value;
        try {
            jedis = jedisPool.getResource();
            value = jedis.get(key);
        } finally {
            release(jedis);
        }
        return value;
    }

    public void setHValue(String key, String field, String value) {
        PvPDojo.catchMainThread("Redis access");

        Jedis jedis = jedisPool.getResource();
        try {
            jedis.hset(key, field, value);
        } finally {
            release(jedis);
        }
    }

    public void removeHValue(String key, String... fields) {
        PvPDojo.catchMainThread("Redis access");

        try (Jedis jedis = jedisPool.getResource()) {
            jedis.hdel(key, fields);
        }
    }

    public String getHValue(String key, String field) {
        PvPDojo.catchMainThread("Redis access");
        Jedis jedis = null;
        String value;
        try {
            jedis = jedisPool.getResource();
            value = jedis.hget(key, field);
        } finally {
            release(jedis);
        }
        return value;
    }

    public Map<String, String> getHAll(String key) {
        PvPDojo.catchMainThread("Redis access");
        Jedis jedis = null;
        Map<String, String> value;
        try {
            jedis = jedisPool.getResource();
            value = jedis.hgetAll(key);
        } finally {
            release(jedis);
        }
        return value;
    }

    public void subscribe(JedisPubSub pubSub, String... channels) {
        // Convert our channel for test server
        channels = Arrays.stream(channels).map(this::getCh).toArray(String[]::new);
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.subscribe(pubSub, channels);
        } finally {
            release(jedis);
        }
    }

    public List<String> mGet(String keys[]) {
        PvPDojo.catchMainThread("Redis access");

        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.mget(keys);
        }
    }

    private void release(Jedis jedis) {
        if (jedis != null) {
            jedis.close();
        }
    }

}