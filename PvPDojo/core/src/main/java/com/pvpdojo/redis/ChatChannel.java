/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.redis;

import java.util.function.Function;

import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;

public enum ChatChannel {

    GLOBAL(user -> true, '?'),
    STAFF(user -> user.getRank().inheritsRank(Rank.TRIAL_MODERATOR), '!'),
    CLAN(user -> user.getClan() != null, '$'),
    PARTY(user -> user.getParty() != null, '%');


    private final Function<User, Boolean> condition;
    private final char activationCharacter;

    ChatChannel(Function<User, Boolean> condition, char activationCharacter) {
        this.condition = condition;
        this.activationCharacter = activationCharacter;
    }

    public Function<User, Boolean> getCondition() {
        return condition;
    }

    public char getActivationCharacter() {
        return activationCharacter;
    }

    public static ChatChannel getByActivation(String message) {
        for (ChatChannel chatChannel : values()) {
            if (message.startsWith(String.valueOf(chatChannel.getActivationCharacter()))) {
                return chatChannel;
            }
        }
        return null;
    }

}
