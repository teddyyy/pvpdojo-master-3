/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.redis;

import org.bukkit.Material;
import org.jetbrains.annotations.NotNull;

import com.pvpdojo.userdata.DojoOfflinePlayer;

import lombok.Getter;
import lombok.Setter;

@Getter
public class Report implements Comparable<Report> {

    @Setter
    private int id;
    private DojoOfflinePlayer target;
    private DojoOfflinePlayer reporter;
    private ReportType reason;
    private String additional;
    private long creation = System.currentTimeMillis();

    public Report(DojoOfflinePlayer target, DojoOfflinePlayer reporter, ReportType reason, String additional) {
        this.target = target;
        this.reporter = reporter;
        this.reason = reason;
        this.additional = additional;
    }

    @Override
    public int compareTo(@NotNull Report report) {
        return Long.compare(report.creation, creation);
    }

    public enum ReportType {
        CHAT(Material.COMMAND),
        KILLAURA(Material.DIAMOND),
        SPEED(Material.DIAMOND_BOOTS),
        FLY(Material.FEATHER),
        AUTOSOUP(Material.MUSHROOM_SOUP),
        CAMPING(Material.BOAT),
        OTHER(Material.SIGN);

        private Material material;

        ReportType(Material material) {
            this.material = material;
        }

        public Material getMaterial() {
            return material;
        }
    }

}
