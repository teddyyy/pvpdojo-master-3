/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.redis;

import static com.pvpdojo.util.StringUtils.COLON;
import static com.pvpdojo.util.StringUtils.PIPE;
import static com.pvpdojo.util.StringUtils.getListFromString;
import static com.pvpdojo.util.StringUtils.getSetFromString;

import java.net.InetAddress;
import java.time.Duration;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.server.ServerListPingEvent;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.pvpdojo.Gamemode;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.bukkit.events.DojoServerRestartEvent;
import com.pvpdojo.bukkit.events.ProxyQuitEvent;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.clan.Clan;
import com.pvpdojo.clan.ClanList;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.party.Party;
import com.pvpdojo.party.PartyList;
import com.pvpdojo.party.PartyPacketHandler;
import com.pvpdojo.session.network.SessionInfo;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.BroadcastUtil;
import com.pvpdojo.util.Countdown;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.PlayerUtil;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

public class BungeeSync extends JedisPubSub {

    // US PROXY
    public static final String MULTI_PROXY_DENY = "users_on_all_proxies";
    public static final String US_PROXY_PREFIX = "usproxy_";

    // Redis stuff
    public static final String GLOBAL_MUTE = "globalmute";
    public static final String CHANNEL_CHAT = "channelchat";
    public static final String MSG = "privatemessage";
    public static final String MSG_REPLY = "messagereply_";
    public static final String KICK = "kick";
    public static final String TABLIST = "tablist";
    public static final String KILLSTREAK = "killstreak_";
    public static final String USERCACHE = "usercache_";
    public static final String REPORT = "reports";
    public static final String REPORT_ID_COUNTER = "reportid";
    public static final String ONLINE_PLAYERS = "onlineplayers";
    public static final String ONLINE_PLAYERS_UUID = "onlineplayersuuid";
    public static final String ONLINE_PLAYERS_DISPLAYNAME = "onlineplayersdisplay";
    public static final String SESSION = "session";
    public static final String SESSION_ID_COUNTER = "sessionid";
    public static final String PARTY = "party";
    public static final String USER_UPDATE = "userupdate";
    public static final String SERVER_ID_COUNTER = "serverid";
    public static final String SERVER_MAP = "servermap";
    public static final String SERVER_INFO = "serverinfomap";
    public static final String REGISTER_SERVER = "registerserver";
    public static final String UNREGISTER_SERVER = "unregisterserver";
    public static final String FIND_PLAYER = "findplayer";
    public static final String RESTART = "restart";
    public static final String CONNECT = "connect";
    public static final String PROXY_JOIN_LEAVE = "proxy_join_leave";
    public static final String NICK_TO_REAL_NAME = "nick_to_real_name";
    public static final String NICK_FAKE_STATS = "nick_fake_stats_";
    public static final String HEARTBEAT = "heartbeat";
    public static final String RECONNECT = "reconnect_server_";
    public static final String SHUTDOWN = "shutdown";

    public static final int REQUEST = 0;
    public static final int RESPONSE = 1;
    public static final int SERVER_REQUEST = 2;
    public static final int PROXY_REQUEST = 3;
    public static final int PLAYERCOUNT = 4;

    public static final Map<Integer, CompletableFuture<Boolean>> SESSION_REQUESTS = new ConcurrentHashMap<>();

    @Override
    public void onMessage(String channel, String message) {
        try {

            if (Redis.get().isTest()) {
                channel = channel.replaceFirst("test_", "");
            }

            if (Redis.get().isUsProxy()) {
                channel = channel.replaceFirst(US_PROXY_PREFIX, "");
            }

            String[] part = PIPE.split(message);
            Player player;
            switch (channel) {
                case CHANNEL_CHAT:
                    ChatChannel chatChannel = ChatChannel.valueOf(part[0]);
                    String info = part[1];
                    String chatMessage = info.equals(BroadcastUtil.TRANSLATABLE) ? part[2] : StringUtils.fromBase64(part[2]);
                    switch (chatChannel) {
                        case CLAN:
                            Clan clan = ClanList.getClan(info);
                            if (clan != null) {
                                clan.sendMessage(CC.DARK_GRAY + "[" + CC.YELLOW + "CC" + CC.DARK_GRAY + "] " + CC.GRAY + chatMessage);
                            }
                            break;
                        case GLOBAL:
                            if (PvPDojo.SERVER_TYPE == ServerType.GAME && info.equals("chat")) {
                                break;
                            }
                            if (info.equals(BroadcastUtil.TRANSLATABLE)) {
                                if (part.length > 3) {
                                    BroadcastUtil.broadcast(MessageKeys.valueOf(chatMessage), getListFromString(part[3], COLON, StringUtils::fromBase64).toArray(new String[0]));
                                } else {
                                    BroadcastUtil.broadcast(MessageKeys.valueOf(chatMessage));
                                }
                            } else {
                                BroadcastUtil.broadcast(chatMessage);
                            }
                            break;
                        case STAFF:
                            if (info.equals(BroadcastUtil.TRANSLATABLE)) {
                                BroadcastUtil.broadcastPerm(MessageKeys.valueOf(chatMessage), "Staff", getListFromString(part[3], COLON, StringUtils::fromBase64).toArray(new String[0]));
                            } else {
                                BroadcastUtil.broadcastPerm("Staff", chatMessage);
                            }
                            break;
                        case PARTY:
                            Party party = PartyList.getPartyById(UUID.fromString(info));
                            if (party != null) {
                                party.sendMessage(CC.DARK_GRAY + "[" + CC.DARK_PURPLE + "Party" + CC.DARK_GRAY + "] " + CC.GRAY + chatMessage);
                            }
                            break;
                        default:
                            break;

                    }
                    break;
                case KICK:
                    UUID uuid = UUID.fromString(part[0]);
                    player = Bukkit.getPlayer(uuid);
                    if (player != null) {
                        PvPDojo.schedule(() -> player.kickPlayer(StringUtils.fromBase64(part[1]))).sync();
                    }
                    break;
                case MSG:
                    if (Integer.parseInt(part[0]) == REQUEST) {
                        String receiver = part[3];
                        player = Bukkit.getPlayerExact(receiver);
                        if (player != null) {
                            UUID senderUUID = UUID.fromString(part[1]);
                            PersistentData senderData = new PersistentData(senderUUID);
                            senderData.pullUserData();
                            String senderDisplayName = part[2];

                            User user = User.getUser(player);
                            String msg = StringUtils.fromBase64(part[4]);
                            String responseMessage;
                            if (user.getSettings().getPrivateMessages().test(user, senderData)) {
                                responseMessage = senderDisplayName + CC.GRAY + " --> " + user.getSpoofedRank().getPrefix() + user.getName() + CC.GRAY + "> " + CC.WHITE + msg;
                                Redis.get().setValue(BungeeSync.MSG_REPLY + player.getUniqueId().toString(), CC.stripColor(senderDisplayName), 600);
                                if (user.getSettings().isPrivateMessagesSound()) {
                                    player.playSound(player.getLocation(), Sound.NOTE_BASS, 1, 1);
                                }
                                player.sendMessage(responseMessage);
                            } else {
                                responseMessage = PvPDojo.WARNING + "Player has their pm's disabled";
                            }
                            sendMessage(senderUUID, responseMessage);
                        }
                    } else if (Integer.parseInt(part[0]) == RESPONSE) {
                        player = Bukkit.getPlayer(UUID.fromString(part[1]));
                        if (player != null) {
                            player.sendMessage(StringUtils.fromBase64(part[2]));
                        }
                    }
                    break;
                case USER_UPDATE:
                    UUID userUUID = UUID.fromString(message);
                    User user = User.getUser(userUUID);
                    if (user != null) {
                        user.getPersistentData().loadData(true);
                    }
                    break;
                case FIND_PLAYER:
                    if (Integer.parseInt(part[0]) == RESPONSE) {
                        Collection<CompletableFuture<String>> requests = FIND_PLAYER_REQUESTS.removeAll(UUID.fromString(part[1]));
                        requests.forEach(fut -> fut.complete(part[2].equals("null") ? "" : part[2]));
                    }
                    break;
                case RESTART:

                    if (part[0].equals("all") || ServerType.valueOf(part[0]) == PvPDojo.SERVER_TYPE || part[0].equals(PvPDojo.get().getNetworkPointer())) {

                        boolean now = part[1].equals("NOW");
                        boolean random = part[1].equalsIgnoreCase("RANDOM");
                        boolean empty = part[1].equalsIgnoreCase("EMPTY");

                        int additionalSeconds = random ? PvPDojo.RANDOM.nextInt(300) : empty ? Integer.MIN_VALUE : 0;
                        if (!now && !random && !empty) {
                            additionalSeconds = Integer.parseInt(part[1]);
                        }
                        switch (PvPDojo.SERVER_TYPE) {
                            case NONE:
                                scheduleRestart(Integer.MIN_VALUE);
                                break;
                            case HUB:
                                scheduleRestart(now ? 0 : 10 + additionalSeconds);
                                break;
                            case GAME:
                                scheduleRestart(now ? 0 : 45 + additionalSeconds);
                                break;
                            case FALLBACK:
                                scheduleRestart(300 + additionalSeconds);
                                break;
                            default:
                                scheduleRestart(now ? 5 : 15 + additionalSeconds);
                                break;
                        }

                    }

                    break;
                case PROXY_JOIN_LEAVE:
                    if (!Boolean.parseBoolean(part[1])) {

                        uuid = UUID.fromString(part[0]);
                        Bukkit.getPluginManager().callEvent(new ProxyQuitEvent(uuid));

                        User partyUser = User.getUser(uuid);
                        if (partyUser != null && partyUser.getParty() != null) {
                            PartyList.removePartyUser(partyUser.getParty(), partyUser.getUUID());
                        }
                    }
                    break;
                case CONNECT:
                    if (Integer.parseInt(part[0]) == SERVER_REQUEST) {
                        player = Bukkit.getPlayer(UUID.fromString(part[1]));
                        if (player != null) {
                            PlayerUtil.sendToServer(player, part[2]);
                        }
                    }
                    break;
                case SESSION:
                    int packetId = Integer.parseInt(part[0]);
                    if (packetId == RESPONSE) {
                        int requestId = Integer.parseInt(part[1]);
                        if (SESSION_REQUESTS.containsKey(requestId)) {
                            SESSION_REQUESTS.remove(requestId).complete(true); // Future just became present
                        }
                    }
                    break;
                case PARTY:
                    PartyPacketHandler.handle(part);
                    break;
                case HEARTBEAT:
                    if (message.isEmpty() && PvPDojo.get().isEnabled()) {
                        retrieveServers();
                        PvPDojo.schedule(() -> {
                            ServerListPingEvent e = new ServerListPingEvent(InetAddress.getLoopbackAddress(), "A Minecraft Server", Bukkit.getOnlinePlayers().size(), 300);
                            Bukkit.getPluginManager().callEvent(e);

                            Map<String, String> serverInfo = getServerMessages();
                            String pointer = PvPDojo.get().getNetworkPointer();
                            if (!serverInfo.containsKey(pointer) || !serverInfo.get(pointer).equals(e.getMotd())) {
                                PvPDojo.schedule(() -> Redis.get().setHValue(BungeeSync.SERVER_INFO, PvPDojo.get().getNetworkPointer(), e.getMotd())).createAsyncTask();
                            }
                        }).createTask(18);
                    }
                    break;
                case SHUTDOWN:
                    if (message.equals(PvPDojo.get().getNetworkPointer()) || message.isEmpty()) {
                        PvPDojo.schedule(BukkitUtil::shutdown).sync();
                    }
                default:
                    Log.warn("Could not recognize redis channel");
                    break;
            }
        } catch (Throwable t) {
            Log.exception("Handle redis packet ch: " + channel + " m:" + message, t);
        }
    }

    public static Countdown scheduledRestart;

    public synchronized static void scheduleRestart(int seconds) {
        if (scheduledRestart != null) {
            return;
        }

        DojoServerRestartEvent restartEvent = new DojoServerRestartEvent(seconds);

        if (seconds < 0) {
            restartEvent.addRestartCondition(() -> Bukkit.getOnlinePlayers().isEmpty());
            seconds = 1;
        }

        Bukkit.getPluginManager().callEvent(restartEvent);

        scheduledRestart = new Countdown(seconds)
                .userCounter(count -> {
                    if (restartEvent.canRestart()) {
                        BroadcastUtil.broadcast(MessageKeys.SERVER_RESTART, "{time}", "" + count);
                    }
                })
                .finish(() -> PvPDojo.schedule(() -> {
                    if (restartEvent.canRestart()) {
                        if (PvPDojo.get().getOwner() != null || StringUtils.UUID.matcher(PvPDojo.get().getFolder()).matches()) {
                            BukkitUtil.shutdown();
                        } else {
                            BukkitUtil.restart();
                        }
                    }
                }).createTimer(20, -1))
                .start();
    }

    public static void sendMessage(UUID uuid, String message) {
        PvPDojo.schedule(() -> Redis.get().publish(MSG, RESPONSE + "|" + uuid + "|" + StringUtils.toBase64(message))).createNonMainThreadTask();
    }

    public static void updateUser(UUID uniqueId) {
        Redis.get().publish(BungeeSync.USER_UPDATE, uniqueId.toString());
    }

    public static boolean isGlobalMute() {
        return Redis.get().getValue(GLOBAL_MUTE) != null;
    }

    public static void setGlobalMute(boolean globalMute) {
        if (globalMute) {
            Redis.get().setValue(GLOBAL_MUTE, "dummy");
        } else {
            Redis.get().delete(GLOBAL_MUTE);
        }
    }

    public static String getUSKey(String key) {
        if (Redis.get().isUsProxy()) {
            return US_PROXY_PREFIX + key;
        }
        return key;
    }

    public static final Predicate<Integer> OFFLINE = count -> count == -1;
    public static final Predicate<String> CLOSING = s -> s.equals("closing");
    public static final Predicate<Integer> ONLINE = count -> count != -1;

    private static volatile Map<String, Integer> playerCount = new HashMap<>();
    private static volatile Map<String, String> serverMessages = new HashMap<>();

    public static void retrieveServers() {
        try (Jedis jedis = Redis.getResource()) {
            playerCount = jedis.hgetAll(getUSKey(SERVER_MAP)).entrySet().stream().collect(Collectors.toMap(Entry::getKey, entry -> Integer.valueOf(entry.getValue())));
            serverMessages = jedis.hgetAll(getUSKey(SERVER_INFO));
            Gamemode.acceptServerInfos(getServerInfo());
        }
    }

    /**
     * @return private copy of the playercount map
     */
    public static Map<String, Integer> getServerPlayerCount() {
        return new HashMap<>(playerCount);
    }

    /**
     * @return private copy of the server motd map
     */
    public static Map<String, String> getServerMessages() {
        return new HashMap<>(serverMessages);
    }

    /**
     * @return SessionInfo relying in ServerInfo (motd)
     */
    public static List<ServerInfo> getServerInfo() {
        Map<String, Integer> playerCount = getServerPlayerCount();
        Map<String, String> serverInfo = getServerMessages();
        return playerCount.entrySet().stream()
                          .map(entry -> new ServerInfo(entry.getKey(), entry.getValue(), serverInfo.get(entry.getKey())))
                          .peek(ServerInfo::tryParseSession)
                          .collect(Collectors.toList());
    }

    public static Set<String> getOnlinePlayers() {
        return getSetFromString(Redis.get().getValue(getUSKey(ONLINE_PLAYERS)), PIPE, String::toString);
    }

    public static Set<UUID> getOnlinePlayersUUID() {
        return getSetFromString(Redis.get().getValue(getUSKey(ONLINE_PLAYERS_UUID)), PIPE, UUID::fromString);
    }

    public static Set<String> getOnlinePlayersDisplayName() {
        return getSetFromString(Redis.get().getValue(getUSKey(ONLINE_PLAYERS_DISPLAYNAME)), PIPE, String::toString);
    }

    public static <T> void addTimeOut(CompletableFuture<T> future, Duration timeout) {
        PvPDojo.schedule(() -> {
            if (!future.isDone()) {
                future.complete(null);
            }
        }).createTask((int) (timeout.getSeconds() * 20));
    }

    private static final Multimap<UUID, CompletableFuture<String>> FIND_PLAYER_REQUESTS = Multimaps.synchronizedMultimap(HashMultimap.create());

    /**
     * This method returns the bungee server of the uuid or null
     */
    public static String findPlayer(UUID uuid) {
        CompletableFuture<String> future = new CompletableFuture<>();
        addTimeOut(future, Duration.ofSeconds(5));
        FIND_PLAYER_REQUESTS.put(uuid, future);
        Redis.get().publish(FIND_PLAYER, REQUEST + "|" + uuid);
        try {
            String server = future.get();
            return server == null ? "" : server;
        } catch (InterruptedException | ExecutionException e) {
            Log.exception(e);
        } finally {
            FIND_PLAYER_REQUESTS.remove(uuid, future);
        }
        return "";
    }

    public static String findServerTypeWithLeastPlayers(ServerType type) {
        Entry<String, Integer> leastPlayerCount = null;
        for (Entry<String, Integer> entry : getServerPlayerCount().entrySet()) {
            String server = entry.getKey();
            Map<String, String> info = getServerMessages();
            if (ONLINE.test(entry.getValue()) && server.startsWith(type.getName().toLowerCase()) && info.containsKey(server) && !CLOSING.test(info.get(server))) {
                if (leastPlayerCount == null) {
                    leastPlayerCount = entry;
                } else if (entry.getValue() < leastPlayerCount.getValue()) {
                    leastPlayerCount = entry;
                }
            }
        }
        return leastPlayerCount != null ? leastPlayerCount.getKey() : null;
    }

    public static int getNextId(String type, int limit) {
        try (Jedis jedis = Redis.getResource()) {
            Long id = jedis.incr(type);
            if (id >= limit) {
                jedis.del(type);
            }
            return Math.toIntExact(id);
        }
    }

    public static int getNextServerId() {
        Set<Integer> usedIds = getServerPlayerCount().keySet().stream()
                                                     .map(name -> name.split("_").length == 2 ? name.split("_")[1] : "0")
                                                     .map(Integer::valueOf)
                                                     .collect(Collectors.toSet());
        return getNextUnusedId(SERVER_ID_COUNTER, usedIds, 99);
    }

    public static final String GAME_ID_COUNTER = "game_id";
    public static final int GAME_ID_LIMIT = 25;

    public static int getNextGameId() {
        Set<Integer> usedIds = getServerInfo().stream().map(ServerInfo::getSessionInfo).filter(Objects::nonNull).map(SessionInfo::getGameId).collect(Collectors.toSet());
        return getNextUnusedId(GAME_ID_COUNTER, usedIds, GAME_ID_LIMIT);
    }

    public static int getNextUnusedId(String type, Set<Integer> usedIds, int limit) {
        int nextId = getNextId(type, limit);

        if (usedIds.contains(nextId)) {
            do {
                nextId++;
            } while (usedIds.contains(nextId));
            Redis.get().setValue(type, String.valueOf(nextId));
        }
        return nextId;
    }

    public static void sendToChannel(ChatChannel channel, String info, String message) {
        Redis.get().publish(BungeeSync.CHANNEL_CHAT, channel.name() + "|" + info + "|" + message);
    }

    public static String getReconnectServer(UUID uuid) {
        return Redis.get().getValue(RECONNECT + uuid.toString());
    }

    public static void setReconnectServer(UUID uuid, String server) {
        if (server == null) {
            Redis.get().delete(RECONNECT + uuid.toString());
            return;
        }
        Redis.get().setValue(RECONNECT + uuid.toString(), server, 1200);
    }

    static final char[] ALPHABET = "abcdefghijklmnopqrstuvwxyz".toCharArray();

    public static String getSubdomainById(int gameId) {
        int section = (gameId - 1) / 5;
        int id = gameId % 5;
        if (id == 0) {
            id = 5;
        }
        return ALPHABET[section] + String.valueOf(id);
    }
}
