/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo;

import static com.pvpdojo.util.StringUtils.MOJANG_UUID;

import java.io.File;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.entity.Player;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;
import org.eclipse.egit.github.core.client.GitHubClient;
import org.eclipse.egit.github.core.service.GistService;
import org.spigotmc.SpigotConfig;

import com.comphenix.protocol.ProtocolLibrary;
import com.pvpdojo.bukkit.listeners.BlockListeners;
import com.pvpdojo.bukkit.listeners.BotListener;
import com.pvpdojo.bukkit.listeners.EntityListeners;
import com.pvpdojo.bukkit.listeners.InputGuiListener;
import com.pvpdojo.bukkit.listeners.PlayerListeners;
import com.pvpdojo.bukkit.listeners.WorldListeners;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.bukkit.util.VoidGenerator;
import com.pvpdojo.command.lib.DojoCommandManager;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.extension.AbstractDojoServer;
import com.pvpdojo.extension.NullServer;
import com.pvpdojo.lang.LanguageManager;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.listeners.CombatLogListener;
import com.pvpdojo.listeners.InventoryListener;
import com.pvpdojo.listeners.PvPDojoJoinListener;
import com.pvpdojo.listeners.PvPDojoModeListener;
import com.pvpdojo.listeners.SessionListener;
import com.pvpdojo.listeners.StatsListener;
import com.pvpdojo.map.DojoMap;
import com.pvpdojo.map.DynamicMapPlacer;
import com.pvpdojo.map.MapLoader;
import com.pvpdojo.mysql.SQL;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.replay.Recorder;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.BroadcastUtil;
import com.pvpdojo.util.DojoRunnable;
import com.pvpdojo.util.DojoRunnable.DojoAsyncQueue;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.idb.DB;
import co.aikar.taskchain.BukkitTaskChainFactory;
import co.aikar.taskchain.TaskChain;
import co.aikar.taskchain.TaskChainFactory;
import co.aikar.timings.Timings;
import eu.the5zig.mod.server.The5zigMod;
import lombok.Getter;

public class PvPDojo extends JavaPlugin implements CommandExecutor {

    public static final UUID SERVER_UUID = UUID.fromString("b9a46a37-6795-4a8d-a4a3-2b614ae733a4");

    public static final String POINTER = CC.DARK_GRAY + " » ";
    public static final String PREFIX = CC.DARK_PURPLE + "PvPDojo" + POINTER + CC.GRAY;
    public static final String WARNING = CC.RED + "Warning" + POINTER + CC.GRAY;
    public static final LanguageManager LANG = new LanguageManager();
    public static final Random RANDOM = new Random();

    public static final GitHubClient GITHUB_CLIENT = new GitHubClient();
    public static final GistService GIST_SERVICE = new GistService(GITHUB_CLIENT);

    public static ServerType SERVER_TYPE;
    public static long SERVER_ID = -1;

    private static PvPDojo instance;
    private static TaskChainFactory taskChainFactory;

    private AbstractDojoServer dojoServer = new NullServer();
    private ServerSettings serverSettings = new ServerSettings();
    private DojoMap standardMap;
    private boolean syncCatcherEnabled;

    public static <T> TaskChain<T> newChain() {
        return taskChainFactory.newChain();
    }

    public static PvPDojo get() {
        return instance;
    }

    public ServerSettings getServerSettings() {
        return serverSettings;
    }

    private boolean registered, test;
    private UUID owner;
    @Getter
    private String folder;

    public AbstractDojoServer getDojoServer() {
        return dojoServer;
    }

    public void setDojoServer(AbstractDojoServer dojoServer) {
        this.dojoServer = dojoServer;
    }

    @Override
    public ChunkGenerator getDefaultWorldGenerator(String worldName, String id) {
        if (getServerSettings().isTerrainControl()) {
            return Bukkit.getPluginManager().getPlugin("TerrainControl").getDefaultWorldGenerator(worldName, id);
        }
        getLogger().info("setup void generator for : " + worldName);
        return new VoidGenerator();
    }

    @Override
    public void onLoad() {
        instance = this;
        this.test = getConfig().getBoolean("test");
        Redis.start();
    }

    @Override
    public void onEnable() {
        LANG.loadLanguages();
        taskChainFactory = BukkitTaskChainFactory.create(this, new DojoAsyncQueue());
        GITHUB_CLIENT.setCredentials(getConfig().getString("github.username"), getConfig().getString("github.password"));
        saveDefaultConfig();
        if (getConfig().getBoolean("selfstart", false)) {
            start();
        }
    }

    @Override
    public void onDisable() {
        getLogger().info("Shutting down executor");
        DojoRunnable.EXECUTOR.shutdown();
        try {
            if (!DojoRunnable.EXECUTOR.awaitTermination(1, TimeUnit.MINUTES)) {
                getLogger().warning("Some tasks did not finish in time");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        getLogger().info("Shutting down Recorder");
        Recorder.inst().stop();
        getLogger().info("Shutting down Redis");
        Redis.get().shutdown();
        DB.close();
    }

    public void register() {
        if (SERVER_TYPE != null && !registered) {
            if (SERVER_ID == -1) {
                SERVER_ID = BungeeSync.getNextServerId();
            }
            Redis.get().publish(BungeeSync.REGISTER_SERVER, SERVER_ID + "|" + SERVER_TYPE.toString().toLowerCase() + "|" + Bukkit.getPort()
                    + (dojoServer.getSubdomain() != null ? "|" + dojoServer.getSubdomain() : ""));
            registered = true;
        }
    }

    public void start() {
        SQL.checkDatabase();

        if (SERVER_TYPE == null) {
            SERVER_TYPE = ServerType.NONE;
            Log.info("Server is of type none");
        }
        register();

        if (!serverSettings.isAbilities()) {
            Bukkit.getPluginManager().disablePlugin(Bukkit.getPluginManager().getPlugin("Abilities"));
        }

        AbilityLoader.loadAllAbilities();
        loadCommands();
        loadListeners();

        NMSUtils.registerNewEnchantment();
        NMSUtils.getCraftServer().setGameProfileProvider(DBCommon::getProfile);
        NMSUtils.getCraftServer().getServer().getLogHandler().setExceptionHandler(Log::discordException);

        InputGuiListener inputGuiListener = new InputGuiListener();
        ProtocolLibrary.getProtocolManager().addPacketListener(inputGuiListener);
        Bukkit.getPluginManager().registerEvents(inputGuiListener, this);
        createBroadcast();
        createUserTimer();

        Recorder.inst().load();
        try {
            MapLoader.loadMaps();
        } catch (SQLException e) {
            Log.exception("Init Maps", e);
            BukkitUtil.shutdown();
            return;
        }

        schedule(() -> {
            syncCatcherEnabled = true;
            Timings.reset();
        }).createTask(10 * 20);

        String folder = new File(System.getProperty("user.dir")).getName();
        this.folder = folder;
        String ownerName = MOJANG_UUID.matcher(folder).matches() ? getOfflinePlayer(UUID.fromString(folder)).getName() : null;
        if (ownerName != null) {
            SpigotConfig.restartOnCrash = false;
            this.owner = UUID.fromString(folder);
        }

        The5zigMod.getInstance().onEnable();
    }

    private void loadListeners() {
        new BlockListeners().register();
        new EntityListeners().register();
        new PlayerListeners().register();
        new WorldListeners().register();
        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        new PvPDojoJoinListener().register();
        new InventoryListener().register();
        new PvPDojoModeListener().register();
        new SessionListener().register();
        new StatsListener().register();
        new CombatLogListener().register();
        new DynamicMapPlacer().register();

        BotListener.register();
    }

    private void loadCommands() {
        DojoCommandManager.initialize();
    }

    public static void catchMainThread(String misconception) {
        if (get().syncCatcherEnabled && Bukkit.isPrimaryThread()) {
            throw new IllegalStateException("Synchronous " + misconception);
        }
    }

    public static DojoRunnable schedule(Runnable run) {
        return new DojoRunnable(run);
    }

    int namemcLoop = 0;

    public void createUserTimer() {
        schedule(() -> {
            for (Player player : Bukkit.getOnlinePlayers()) {
                User user = User.getUser(player);
                user.addNoMoveSecond();
            }
        }).createTimer(20, -1);

        schedule(() -> Redis.get().publish(BungeeSync.HEARTBEAT, getNetworkPointer())).createTimerAsync(5 * 20, -1);

        schedule(() -> {
            namemcLoop = 0;
            Bukkit.getOnlinePlayers().stream().map(User::getUser).filter(User::isDataLoaded).forEach(user -> {
                PvPDojo.schedule(() -> {
                    if (!user.isLoggedOut()) {
                        user.getFastDB().checkNameMC(user.getUUID());
                    }
                }).createTaskAsync(++namemcLoop * 5);
            });
        }).createTimerAsync(60 * 20, -1);
    }

    int increment = 0;
    static final List<MessageKeys> MESSAGES = Arrays.asList(MessageKeys.BROADCAST_CAPITALISM, MessageKeys.BROADCAST_DISCORD, MessageKeys.BROADCAST_NAMEMC,
            MessageKeys.BROADCAST_TWITTER, MessageKeys.BROADCAST_WEBSITE);

    public void createBroadcast() {
        if (SERVER_TYPE != ServerType.HUB) {
            return;
        }

        schedule(() -> {

            BroadcastUtil.global(ChatChannel.GLOBAL, MESSAGES.get(increment), "");
            increment++;
            if (increment >= MESSAGES.size()) {
                increment = 0;
            }

        }).createTimer(180 * 20, -1);
    }

    public static DojoOfflinePlayer getOfflinePlayer(UUID uuid) {
        return DBCommon.getOfflinePlayer(uuid);
    }

    public static DojoOfflinePlayer getOfflinePlayer(String name) {
        if (MOJANG_UUID.matcher(name).matches()) {
            return getOfflinePlayer(UUID.fromString(name));
        }
        return DBCommon.getOfflinePlayer(name);
    }

    public void setStandardMap(DojoMap standardMap) {
        this.standardMap = standardMap;
    }

    public DojoMap getStandardMap() {
        return standardMap;
    }

    public UUID getOwner() {
        return owner;
    }

    public boolean isTest() {
        return test;
    }

    public String getNetworkPointer() {
        return SERVER_TYPE.name().toLowerCase() + "_" + SERVER_ID;
    }

    public void setSyncCatcherEnabled(boolean syncCatcherEnabled) {
        this.syncCatcherEnabled = syncCatcherEnabled;
    }
}