/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo;

import static com.pvpdojo.mysql.Tables.ACCOUNT_GRID;
import static com.pvpdojo.mysql.Tables.SESSION;
import static com.pvpdojo.mysql.Tables.USERS;
import static com.pvpdojo.util.StreamUtils.function;
import static com.pvpdojo.util.StringUtils.getStringFromArray;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.clan.Clan;
import com.pvpdojo.clan.ClanList;
import com.pvpdojo.clan.ClanRequest;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.map.MapCuboidRegion;
import com.pvpdojo.map.MapPolygonalRegion;
import com.pvpdojo.map.MapRegion;
import com.pvpdojo.mysql.SQL;
import com.pvpdojo.mysql.SQLBuilder;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.session.PastGameSession;
import com.pvpdojo.settings.MessageQueue;
import com.pvpdojo.settings.Settings;
import com.pvpdojo.settings.SettingsType;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.Request;
import com.pvpdojo.userdata.UUIDFetcher;
import com.pvpdojo.userdata.impl.DojoOfflinePlayerImpl;
import com.pvpdojo.util.BroadcastUtil;
import com.pvpdojo.util.LimitedQueue;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.PunishmentManager;
import com.pvpdojo.util.PunishmentManager.Punishment;
import com.pvpdojo.util.PunishmentManager.PunishmentType;
import com.pvpdojo.util.Reflection;
import com.pvpdojo.util.RuntimeTypeAdapterFactory;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import co.aikar.idb.DB;
import co.aikar.idb.DbRow;
import net.minecraft.server.v1_7_R4.MinecraftServer;
import net.minecraft.util.com.mojang.authlib.GameProfile;
import net.minecraft.util.com.mojang.authlib.properties.Property;
import redis.clients.jedis.Jedis;

public class DBCommon {

    public static final Gson GSON;

    static {
        GsonBuilder builder = new GsonBuilder();
        GSON = builder.enableComplexMapKeySerialization()
                      .registerTypeHierarchyAdapter(Clan.class, (JsonSerializer<Clan>) (clan, type, context) -> new JsonPrimitive(clan.getName()))
                      .registerTypeHierarchyAdapter(Clan.class, wrapDeserial((json, type, context) -> ClanList.getClan(json.getAsString())))
                      .registerTypeHierarchyAdapter(DojoOfflinePlayer.class, (JsonSerializer<DojoOfflinePlayer>) (off, type, context) -> new JsonPrimitive(off.getUniqueId().toString()))
                      .registerTypeHierarchyAdapter(DojoOfflinePlayer.class, wrapDeserial((json, type, context) -> PvPDojo.getOfflinePlayer(UUID.fromString(json.getAsString()))))
                      .registerTypeAdapterFactory(RuntimeTypeAdapterFactory.of(Request.class).registerSubtype(ClanRequest.class))
                      .registerTypeAdapterFactory(RuntimeTypeAdapterFactory.of(MapRegion.class).registerSubtype(MapCuboidRegion.class).registerSubtype(MapPolygonalRegion.class))
                      .registerTypeHierarchyAdapter(ProtectedRegion.class, new RegionSerializer())
                      .registerTypeAdapter(PotionEffectType.class, wrapDeserial((json, type, context) -> PotionEffectType.getByName(json.getAsString())))
                      .registerTypeAdapter(PotionEffectType.class,
                              (JsonSerializer<PotionEffectType>) (potionType, type, context) -> new JsonPrimitive(potionType.getName()))
                      .registerTypeHierarchyAdapter(World.class, new WorldSerializer())
                      .registerTypeHierarchyAdapter(ItemStack[].class, wrapDeserial((json, type, context) -> fromBase64(json.getAsString())))
                      .registerTypeAdapter(ItemStack[].class, (JsonSerializer<ItemStack[]>) (inv, type, context) -> new JsonPrimitive(toBase64(inv)))
                      .registerTypeAdapter(Multimap.class,
                              (JsonSerializer<Multimap>) (multimap, type, jsonSerializationContext) -> jsonSerializationContext.serialize(multimap.asMap()))
                      .registerTypeAdapter(Multimap.class,
                              (JsonDeserializer<Multimap>) (jsonElement, type, jsonDeserializationContext) -> {
                                  final SetMultimap<String, String> map = Multimaps.newSetMultimap(new HashMap<>(), Sets::newHashSet);
                                  for (Entry<String, JsonElement> entry : ((JsonObject) jsonElement).entrySet()) {
                                      for (JsonElement element : (JsonArray) entry.getValue()) {
                                          map.get(entry.getKey()).add(element.getAsString());
                                      }
                                  }
                                  return map;
                              })
                      .create();
    }

    public static <T> JsonDeserializer<T> wrapDeserial(JsonDeserializer<T> deserial) {
        return deserial;
    }

    public static final LoadingCache<UUID, GameProfile> PROFILE_CACHE = CacheBuilder.newBuilder()
                                                                                    .maximumSize(1000)
                                                                                    .expireAfterWrite(1, TimeUnit.HOURS)
                                                                                    .build(CacheLoader.from(uuid -> MinecraftServer.getServer().av().fillProfileProperties(
                                                                                            new GameProfile(uuid, null), true)));

    public static void connectPlayerToDB(Player player) {
        String name = player.getName();
        String ip = player.getAddress().getHostString();
        try {
            boolean exists = SQL.checkRowExist(Tables.USERS, "uuid = ?", player.getUniqueId().toString());

            if (!exists) {
                SQL.insert(Tables.USERS, "'" + player.getUniqueId() + "','" + name + "','" + ip + "', 'DEFAULT', FROM_UNIXTIME(0), 0, 1400, 0, 0, 0, 0, 1000, 0, NOW(), NOW(), NOW(), '', 0, 0");

                for (int i = 0; i < 10; i++) {
                    SQL.insert(Tables.KITS, "'" + player.getUniqueId() + "', 0, 1, 'Custom Kit', 0, '', NULL");
                }

                for (int i = 0; i < 5; i++) {
                    String uuid = UUID.randomUUID().toString();
                    SQL.insert(Tables.ABILITIES, "'" + player.getUniqueId().toString() + "', 0,'" + uuid + "', -1, 1");
                }
                for (int i = 0; i < 5; i++) {
                    String uuid = UUID.randomUUID().toString();
                    SQL.insert(Tables.ABILITIES, "'" + player.getUniqueId().toString() + "', 0,'" + uuid + "', -2, 1");
                }

                SQL.insert(Tables.ABILITIES, "'" + player.getUniqueId().toString() + "', 0,'" + UUID.randomUUID().toString() + "', -3, 1");
                SQL.insert(Tables.ABILITIES, "'" + player.getUniqueId().toString() + "', 0,'" + UUID.randomUUID().toString() + "', -5, 1");
                SQL.insert(Tables.ABILITIES, "'" + player.getUniqueId().toString() + "', 0,'" + UUID.randomUUID().toString() + "', -6, 1");

                player.sendMessage(CC.GRAY + StringUtils.getLS());
                player.sendMessage(CC.DARK_PURPLE + "Welcome to PvPDojo!");
                player.sendMessage(CC.GRAY + "You were given " + CC.GREEN + "10 free starter chests");
                player.sendMessage(CC.GRAY + StringUtils.getLS());

                BroadcastUtil.global(ChatChannel.GLOBAL, MessageKeys.SERVER_WELCOME, "{target}", player.getName());
            }

            try (SQLBuilder sql = new SQLBuilder()) {
                sql.insertIgnore(DeveloperSettings.SEASON, "userid")
                   .select(USERS, "id")
                   .where("uuid = ?")
                   .executeUpdate(player.getUniqueId().toString());
            }

            DbRow row = DB.getFirstRow("SELECT ip,rank FROM users WHERE uuid = ?", player.getUniqueId().toString());
            Rank rank = Rank.valueOf(row.get("rank"));
            if (rank.inheritsRank(Rank.TRIAL_MODERATOR) && rank != Rank.CHAT_MODERATOR && !player.getAddress().getHostString().equals(row.get("ip"))) {
                Discord.get().handleAuth(player);
            }

            if (exists) {
                SQL.updateMultiple(USERS, "name = ?, ip = ?, lastlogin = NOW()", "uuid = ?", player.getName(), ip, player.getUniqueId().toString());
            }

            Timestamp now = Timestamp.from(Instant.now());
            SQL.insertOnDuplicate(Tables.ACCOUNT_GRID, "?,?,?,?", "UPDATE ip = ?, lastupdate = ?", player.getUniqueId().toString(), 0, ip, now, ip, now);

            List<Integer> ids = SQL.getIntegers(Tables.ACCOUNT_GRID, "id", "uuid = ? OR (ip = ? AND lastupdate >= CAST(NOW() AS DATE))", player.getUniqueId().toString(), ip);
            if (ids.size() > 1) {
                SQL.updateInteger(ACCOUNT_GRID, "id", "id IN (" + getStringFromArray(ids, Objects::toString, ",") + ")", ids.get(0));
            }

            List<UUID> accounts = getAccountGrid(player.getUniqueId());
            Punishment pun = accounts.stream()
                                     .map(PvPDojo::getOfflinePlayer)
                                     .map(DojoOfflinePlayer::getCurrentBan)
                                     .filter(Objects::nonNull)
                                     .filter(punishment -> !punishment.getOriginalReason().startsWith("Ban Evasion"))
                                     .max(Comparator.comparingLong(Punishment::getLengthCompare))
                                     .orElse(null);

            if (pun != null) {
                PunishmentManager.punish(Bukkit.getConsoleSender(), player.getName(), "Ban Evasion (" + pun.getOriginalReason() + ")", pun.getSecondsLeft(), PunishmentType.BAN, true);
            } else if (accounts.size() > 12) {
                PunishmentManager.punish(Bukkit.getConsoleSender(), player.getName(), "Alting", -1, PunishmentType.BAN);
            }

            OldPayment payment = PaymentProcessor.SUMMARY.remove(player.getUniqueId());
            if (payment != null) {
                PvPDojo.schedule(() -> {
                    switch (payment) {
                        case GRANDMASTER:
                            PvPDojo.schedule(() -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "rank " + player.getName() + " Grandmaster")).sync();
                        case FIRST_DAN:
                            PvPDojo.schedule(() -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "server-purchase " + player.getName() + " firstdan")).sync();
                            break;
                        case SUPPORTER:
                            PvPDojo.schedule(() -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "server-purchase " + player.getName() + " blackbelt")).sync();
                            break;
                    }
                }).sync();
            }
        } catch (SQLException e) {
            Log.exception("Connecting to DB", e);
            PvPDojo.schedule(() -> player.kickPlayer("Database failure")).sync();
        }

    }

    public static List<UUID> getAccountGrid(UUID source) throws SQLException {
        return SQL.getStrings(Tables.ACCOUNT_GRID, "uuid", "id = (SELECT id FROM " + Tables.ACCOUNT_GRID + " WHERE uuid = ?)", source.toString()).stream()
                  .map(UUID::fromString)
                  .collect(Collectors.toList());
    }

    public static void updateUserCache(GameProfile profile) {
        if (!profile.isComplete() || profile.getProperties().get("textures").isEmpty()) {
            return;
        }
        // Cache for a week
        Property skin = profile.getProperties().get("textures").iterator().next();
        try (Jedis jedis = Redis.getResource()) {
            jedis.setex(BungeeSync.USERCACHE + profile.getName().toLowerCase(), 60 * 60 * 24 * 7, profile.getId().toString());
            jedis.setex(BungeeSync.USERCACHE + profile.getId().toString(), 60 * 60 * 24 * 7, profile.getName() + "," + skin.getValue() + "," + skin.getSignature());
        }

    }

    protected static DojoOfflinePlayerImpl getOfflinePlayer(String name) {
        return getOfflinePlayer(getProfile(name));
    }

    protected static DojoOfflinePlayerImpl getOfflinePlayer(UUID uuid) {
        return getOfflinePlayer(getProfile(uuid));
    }

    private static DojoOfflinePlayerImpl getOfflinePlayer(GameProfile profile) {

        // Nick name lookup. Reveal real UUID, but use fake name
        if (profile.getName() != null) {
            String realName = Redis.get().getHValue(BungeeSync.NICK_TO_REAL_NAME, profile.getName().toLowerCase());
            if (realName != null) {
                profile = new GameProfile(getProfile(realName).getId(), profile.getName());
            }
        }

        String lastIpAddress = null;
        long firstlogin = -1, lastLogin = -1, lastClanChange = -1;
        try (SQLBuilder builder = new SQLBuilder()) {
            builder.setStatement("SELECT firstlogin, lastlogin, lastclanswitch, ip FROM users WHERE uuid = '" + profile.getId().toString() + "'");
            if (builder.executeQuery().next()) {
                firstlogin = builder.getResultSet().getTimestamp(1).getTime();
                lastLogin = builder.getResultSet().getTimestamp(2).getTime();
                lastClanChange = builder.getResultSet().getTimestamp(3).getTime();
                lastIpAddress = builder.getResultSet().getString(4);
            }
        } catch (SQLException e) {
            Log.exception("Pulling OfflinePlayer Info", e);
        }
        Punishment currentBan = null;
        Punishment currentMute = null;
        try {
            currentBan = PunishmentManager.getActivePunishment(profile.getId(), PunishmentType.BAN);
            currentMute = PunishmentManager.getActivePunishment(profile.getId(), PunishmentType.MUTE);
        } catch (SQLException e) {
            Log.exception("Pulling Punishments", e);
        }
        return new DojoOfflinePlayerImpl(profile, currentBan, currentMute, lastIpAddress, firstlogin, lastLogin, lastClanChange);
    }

    public static GameProfile getProfile(String name) {
        // Nick name lookup. Reveal real UUID, but use fake name
        String realName = Redis.get().getHValue(BungeeSync.NICK_TO_REAL_NAME, name.toLowerCase());
        if (realName != null) {
            Log.info("Found nick real name " + name + " -> " + realName);
            name = Redis.get().getValue(BungeeSync.NICK_TO_REAL_NAME + "_" + realName); // Case correction
            return new GameProfile(getProfile(realName).getId(), name);
        }

        String response = Redis.get().getValue(BungeeSync.USERCACHE + name.toLowerCase());
        if (response == null) {
            UUID fetched = Bukkit.getPlayer(name) != null ? Bukkit.getPlayer(name).getUniqueId() : UUIDFetcher.getUUID(name);
            fetched = fetched != null ? fetched : getLastKnownUniqueId(name);
            if (fetched == null) {
                Log.warn("Fetched null UUID for " + name);
            }
            return getProfile(fetched);
        }
        return getProfile(UUID.fromString(response));
    }

    public static GameProfile getProfile(UUID uuid) {
        if (uuid == null) {
            Log.warn("Received null UUID to fetch - returning random");
            return new GameProfile(UUID.randomUUID(), null);
        }
        if (uuid.equals(PvPDojo.SERVER_UUID)) {
            return new GameProfile(PvPDojo.SERVER_UUID, "[Server]");
        }

        Player player = Bukkit.getPlayer(uuid);
        if (player != null) {
            GameProfile profile = NMSUtils.get(player).getProfile();
            updateUserCache(profile);
            return profile;
        }
        GameProfile profile;
        String response = Redis.get().getValue(BungeeSync.USERCACHE + uuid.toString());
        if (response == null) {
            profile = PROFILE_CACHE.getUnchecked(uuid);
            if (!profile.isComplete()) { // Mojang API down? Use fallback name from DB
                return new GameProfile(uuid, getLastKnownUserName(uuid));
            } else {
                updateUserCache(profile);
                return profile;
            }
        } else {
            String[] part = response.split(",");
            profile = new GameProfile(uuid, part[0]);
            profile.getProperties().put("textures", new Property("textures", part[1], part[2]));
            return profile;
        }
    }

    public static UUID getLastKnownUniqueId(String name) {
        String uuid = null;
        try {
            uuid = SQL.getString(Tables.USERS, "uuid", "name = ?", name);
        } catch (SQLException e) {
            Log.exception("Pulling last known unique id from name", e);
        }
        return uuid != null ? UUID.fromString(uuid) : null;
    }

    public static String getLastKnownUserName(UUID uuid) {
        String name = null;
        try {
            name = SQL.getString(Tables.USERS, "name", "uuid = ?", uuid.toString());
        } catch (SQLException e) {
            Log.exception("Pulling last user name", e);
        }
        return name != null ? name : "[Unknown]";
    }

    public static List<String> getAllLastKnownUserNames() {
        try {
            return SQL.getStrings(Tables.USERS, "name", "");
        } catch (SQLException e) {
            Log.exception("Pulling all last known user names", e);
        }
        return Collections.emptyList();
    }

    public static int savePastGameSession(PastGameSession pastGameSession) throws SQLException {
        String strSession = GSON.toJson(pastGameSession);

        if (pastGameSession.getId() == -1) {
            try (SQLBuilder sql = new SQLBuilder()) {
                sql.setStatement("INSERT INTO gamesession (date, type, data) VALUES (FROM_UNIXTIME(" + pastGameSession.getDate() / 1000 + "),'" + pastGameSession.getType().name() + "','" + strSession + "')")
                   .createStatement(Statement.RETURN_GENERATED_KEYS)
                   .executeUpdate();
                ResultSet key = sql.getPreparedStatement().getGeneratedKeys();
                if (key.next()) {
                    int sessionId = key.getInt(1);
                    pastGameSession.getParticipants().stream().flatMap(persistentArenaEntity -> persistentArenaEntity.getMembers().stream()).forEach(uuid -> {
                        try {
                            SQL.insert(Tables.SESSION_UUID, "0, ?, ?", uuid.toString(), sessionId);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    });
                    return sessionId;
                } else {
                    throw new IllegalStateException("No key generated");
                }
            }

        } else {
            SQL.updateString(SESSION, "data", "id = " + pastGameSession.getId(), strSession);
            return pastGameSession.getId();
        }
    }

    /**
     * This returns the ids of all game sessions the given player participated.
     * The id is mapped to it's date that can be used for appropriate sorting
     *
     * @param uuid of player - if <code>null</code> every session is returned
     * @return Map sessionId, timestamp
     */
    public static Map<Integer, Long> getGameSessions(UUID uuid) {
        Map<Integer, Long> sessions = new HashMap<>();

        try (SQLBuilder builder = new SQLBuilder()) {
            builder.select(Tables.SESSION, "gamesession.id, date");

            if (uuid != null) {
                builder.join(Tables.SESSION_UUID, "gamesession_uuid.session_id = gamesession.id").where("uuid = ?").executeQuery(uuid.toString());
            } else {
                builder.executeQuery();
            }

            while (builder.next()) {
                sessions.put(builder.getInteger("id"), builder.getTimestamp("date").getTime());
            }

        } catch (SQLException ex) {
            Log.exception("Pulling game sessions for " + uuid, ex);
        }

        return sessions;
    }

    public static PastGameSession getGameSession(int sessionId) {
        try {
            return GSON.fromJson(SQL.getString(Tables.SESSION, "data", "id = ?", sessionId), PastGameSession.class);
        } catch (SQLException e) {
            Log.exception("Pulling game session " + sessionId, e);
        }
        throw new NullPointerException();
    }

    public static String toBase64(ItemStack[] inventory) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            GZIPOutputStream gzipStream = new GZIPOutputStream(outputStream);
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(gzipStream);

            // Write the size of the inventory
            dataOutput.writeInt(inventory.length);

            // Save every element in the list
            for (ItemStack anInventory : inventory) {
                dataOutput.writeObject(anInventory);
            }

            // Serialize that array
            dataOutput.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (Exception e) {
            throw new IllegalStateException("Unable to save item stacks.", e);
        }
    }

    public static ItemStack[] fromBase64(String data) {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
            GZIPInputStream gzipStream = new GZIPInputStream(inputStream);
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(gzipStream);
            ItemStack[] inventory = new ItemStack[dataInput.readInt()];

            // Read the serialized inventory
            for (int i = 0; i < inventory.length; i++) {
                inventory[i] = (ItemStack) dataInput.readObject();
            }
            dataInput.close();
            return inventory;
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T extends Settings> T loadSettings(SettingsType<T> settings, UUID uuid) {
        T loaded = GSON.fromJson(Redis.get().getValue(settings.getKey() + uuid.toString()), settings.getClazz());
        try {
            if (loaded != null) {
                return loaded;
            }
            return Reflection.hasNoArgConstructor(settings.getClazz()) ? settings.getClazz().getConstructor().newInstance() : null;
        } catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
    }

    public static <T extends Settings> void deleteSettings(SettingsType<T> settings, UUID uuid) {
        Redis.get().delete(settings.getKey() + uuid.toString());
    }

    public static <T extends Settings> void saveSettings(T data, UUID uuid) {
        PvPDojo.schedule(() -> {
            SettingsType<?> settingsType = SettingsType.getSettingsType(data.getClass());
            Redis.get().setValue(settingsType.getKey() + uuid.toString(), GSON.toJson(data), settingsType.getExpireSeconds());
        }).createNonMainThreadTask();
    }

    public static void sendOfflineMessage(UUID uuid, String... msgs) {
        PvPDojo.schedule(() -> {
            MessageQueue queue = loadSettings(SettingsType.MESSAGE_QUEUE, uuid);
            for (String msg : msgs) {
                queue.addMessage(msg);
            }
            saveSettings(queue, uuid);
        }).createNonMainThreadTask();
    }

    public static void logChatMessage(UUID sender, Collection<UUID> receivers, String message) {
        PvPDojo.schedule(() -> {
            try {
                OptionalInt senderId = getUserId(sender);

                // If we cannot find an id for sender, don't log
                if (!senderId.isPresent()) {
                    Log.warn("No user id found for chat sender");
                    return;
                }

                int[] receiverIds = receivers.stream().filter(Objects::nonNull).map(function(DBCommon::getUserId)).filter(OptionalInt::isPresent).mapToInt(OptionalInt::getAsInt).toArray();

                int messageId;
                try (SQLBuilder sql = new SQLBuilder()) {
                    sql.insert(Tables.CHAT_MESSAGES, "date, senderid, message")
                       .values("NOW(),?,?")
                       .createStatement(Statement.RETURN_GENERATED_KEYS)
                       .executeUpdate(senderId.getAsInt(), message);

                    ResultSet key = sql.getPreparedStatement().getGeneratedKeys();
                    key.next();
                    messageId = key.getInt(1);
                }

                try (SQLBuilder sql = new SQLBuilder()) {
                    sql.insertIgnore(Tables.CHAT_RECEIVERS, "receiverid, messageid").values("?,?");
                    for (int receiver : receiverIds) {
                        sql.queue(receiver, messageId);
                    }
                    sql.executeBatch();
                }
            } catch (SQLException e) {
                Log.exception(e);
            }
        }).createNonMainThreadTask();
    }

    public static OptionalInt getUserId(UUID user) throws SQLException {
        try (SQLBuilder sql = new SQLBuilder()) {
            if (sql.select(Tables.USERS, "id").where("uuid = ?").executeQuery(user.toString()).next()) {
                return OptionalInt.of(sql.getInteger("id"));
            }
        }
        return OptionalInt.empty();
    }

    public static Optional<UUID> getUserById(int id) throws SQLException {
        try (SQLBuilder sql = new SQLBuilder()) {
            if (sql.select(Tables.USERS, "uuid").where("id = ?").executeQuery(id).next()) {
                return Optional.of(UUID.fromString(sql.getString("uuid")));
            }
        }
        return Optional.empty();
    }

    public static class WorldSerializer implements JsonSerializer<World>, JsonDeserializer<World> {

        @Override
        public World deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
            return Bukkit.getWorld(json.getAsString());
        }

        @Override
        public JsonElement serialize(World world, Type type, JsonSerializationContext context) {
            return new JsonPrimitive(world.getName());
        }

    }

    public static class RegionSerializer implements JsonSerializer<ProtectedRegion>, JsonDeserializer<ProtectedRegion> {

        @Override
        public ProtectedRegion deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
            return WorldGuardPlugin.inst().getRegionManager(Bukkit.getWorlds().get(0)).getRegion(json.getAsString());
        }

        @Override
        public JsonElement serialize(ProtectedRegion region, Type type, JsonSerializationContext context) {
            return new JsonPrimitive(region.getId());
        }

    }

    class LimitedQueueSerializer implements JsonSerializer<LimitedQueue<?>>, JsonDeserializer<LimitedQueue<?>> {

        @Override
        public LimitedQueue<?> deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            return null;
        }

        @Override
        public JsonElement serialize(LimitedQueue<?> objects, Type type, JsonSerializationContext jsonSerializationContext) {

            return null;
        }
    }

}
