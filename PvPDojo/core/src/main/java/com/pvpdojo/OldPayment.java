/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo;

public enum OldPayment {
    FIRST_DAN, GRANDMASTER, SUPPORTER
}
