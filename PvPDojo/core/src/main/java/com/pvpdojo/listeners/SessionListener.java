/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.listeners;

import java.util.Arrays;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityPostDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPostDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.world.ChunkUnloadEvent;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.DojoCombatLogEvent;
import com.pvpdojo.bukkit.events.DojoPlayerJoinEvent;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.session.GameSession;
import com.pvpdojo.session.RelogData;
import com.pvpdojo.session.RelogNPC;
import com.pvpdojo.session.RelogSession;
import com.pvpdojo.session.SessionState;
import com.pvpdojo.session.entity.ArenaEntity;
import com.pvpdojo.userdata.User;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldguard.bukkit.BukkitUtil;

public class SessionListener implements DojoListener {

    @EventHandler(priority = EventPriority.HIGH)
    public void onJoin(DojoPlayerJoinEvent e) {

        RelogData relogData = RelogSession.RELOG_MAP.remove(e.getPlayer().getUniqueId());

        if (relogData != null) {
            if (relogData.getSession() != null) {
                relogData.apply(User.getUser(e.getPlayer()));
            }
            // RelogData consumed -> Remove from Redis
            PvPDojo.schedule(() -> BungeeSync.setReconnectServer(e.getPlayer().getUniqueId(), null)).createAsyncTask();
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onQuitHighest(PlayerQuitEvent e) {
        User user = User.getUser(e.getPlayer());

        if (user.getSession() != null && !user.getSession().getCurrentlyDead().contains(user)) {
            GameSession session = user.getSession();

            if (session instanceof RelogSession && session.getState() != SessionState.PREGAME) {

                RelogData relogData = ((RelogSession) session).createRelogData(user);
                RelogSession.RELOG_MAP.put(user.getUUID(), relogData);

                if (((RelogSession) session).hasNPC()) {
                    relogData.setRelogNPC(new RelogNPC(relogData));
                }

                // Swap ArenaEntity
                if (session.isTeamFight()) {
                    List<ArenaEntity> teamMembers = user.getTeam().getMembers();
                    teamMembers.set(teamMembers.indexOf(user), relogData);
                } else {
                    List<ArenaEntity> participants = session.getParticipants();
                    participants.set(participants.indexOf(user), relogData);
                }

                if (session.getCurrentlyDead().remove(user)) {
                    session.getCurrentlyDead().add(relogData);
                }

                int relogTimeout = ((RelogSession) session).getRelogTimeout();

                if (relogTimeout != -1) {
                    ((RelogSession) session).onLogout(user);

                    PvPDojo.schedule(() -> {
                        if (session.getParticipants().contains(relogData) && session.getState() != SessionState.FINISHED) {
                            ((RelogSession) session).onTimeout(user);

                            if (relogData.getRelogNPC() != null) {
                                relogData.getRelogNPC().getEntity().damage(Integer.MAX_VALUE);
                            } else {
                                session.postDeath(relogData);
                                if (relogData.isUseStats()) {
                                    relogData.getStats().addDeath();
                                }
                            }
                        }
                    }).createTask(relogTimeout * 20);
                }

                PvPDojo.schedule(() -> BungeeSync.setReconnectServer(user.getUUID(), PvPDojo.get().getNetworkPointer())).createAsyncTask();
            } else {
                session.disconnect(user);
            }
        }

    }

    @EventHandler
    public void onCombatLog(DojoCombatLogEvent e) {
        User user = User.getUser(e.getPlayer());

        if (user.getSession() != null && user.getSession() instanceof RelogSession && ((RelogSession) user.getSession()).isAllowCombatLog()) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onDamage(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player && (e.getDamager() instanceof Player || (e.getDamager() instanceof Projectile && ((Projectile) e.getDamager()).getShooter() instanceof Player))) {

            ArenaEntity damager = User.getUser(e.getDamager() instanceof Player ? (Player) e.getDamager() : (Player) ((Projectile) e.getDamager()).getShooter());
            ArenaEntity entity = User.getUser((Player) e.getEntity());

            if (damager.getTeam() != null && entity.getTeam() != null && damager.getTeam() == entity.getTeam() && !entity.getTeam().isFriendlyFire()) {
                e.setCancelled(true);
            }

        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntitySpawn(CreatureSpawnEvent e) {
        Vector weVector = BukkitUtil.toVector(e.getLocation());
        GameSession.getSessions()
                   .stream()
                   .map(GameSession::getArena)
                   .filter(arena -> arena.getRegion().contains(weVector) && arena.getWorld().equals(e.getEntity().getWorld()))
                   .forEach(arena -> arena.getEntityList().add(e.getEntity()));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onDrop(PlayerDropItemEvent e) {
        User user = User.getUser(e.getPlayer());

        if (user.getSession() != null) {
            user.getSession().getArena().getEntityList().add(e.getItemDrop());
        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        User user = User.getUser(e.getEntity());

        if (user.getSession() != null) {
            user.getSession().death(user, e);
        }

    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void postDeath(PlayerPostDeathEvent e) {
        User user = User.getUser(e.getEntity());

        if (user.getSession() != null) {
            user.getSession().getArena().getEntityList().addAll(e.getItems());
            user.getSession().postDeath(user);
        }

    }

    @EventHandler
    public void onDeath(EntityDeathEvent e) {

        RelogData relogData = RelogNPC.getData(e.getEntity());

        if (relogData != null) {
            e.getDrops().clear();
            e.getDrops().addAll(Arrays.asList(relogData.getArmor()));
            e.getDrops().addAll(Arrays.asList(relogData.getInventory()));
            relogData.getSession().death(relogData, e);
        }

    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void postDeath(EntityPostDeathEvent e) {
        RelogData relogData = RelogNPC.getData(e.getEntity());

        if (relogData != null) {
            relogData.getSession().getArena().getEntityList().addAll(e.getItems());
            relogData.getSession().postDeath(relogData);
        }

    }

    @EventHandler
    public void onCombust(EntityCombustEvent e) {

        if (e.getEntity().hasMetadata(RelogNPC.RELOG_NPC)) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onRelogNPCChunk(ChunkUnloadEvent e) {

        if (Arrays.stream(e.getChunk().getEntities()).anyMatch(entity -> entity.hasMetadata(RelogNPC.RELOG_NPC))) {
            e.setCancelled(true);
        }

    }

}
