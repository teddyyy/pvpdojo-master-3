/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerPreDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.pvpdojo.bukkit.events.DojoCombatLogEvent;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.userdata.User;

public class CombatLogListener implements DojoListener {

    @EventHandler(priority = EventPriority.HIGH)
    public void onQuit(PlayerQuitEvent e) {

        User user = User.getUser(e.getPlayer());

        if (user.isInCombat()) {

            DojoCombatLogEvent combatLogEvent = new DojoCombatLogEvent(e.getPlayer());
            Bukkit.getPluginManager().callEvent(combatLogEvent);

            if (!combatLogEvent.isCancelled()) {
                Bukkit.getPluginManager().callEvent(new EntityDamageEvent(e.getPlayer(), DamageCause.CUSTOM, Integer.MAX_VALUE));
                Bukkit.getPluginManager().callEvent(new PlayerPreDeathEvent(e.getPlayer()));
                e.getPlayer().setHealth(0D);
            }

        }

    }

}
