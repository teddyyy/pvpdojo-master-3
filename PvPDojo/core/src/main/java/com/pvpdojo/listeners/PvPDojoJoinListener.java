/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.DojoPlayerJoinEvent;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.challenges.ChallengeManager;
import com.pvpdojo.challenges.DojoChallenge;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.settings.MessageQueue;
import com.pvpdojo.settings.SettingsType;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Log;

public class PvPDojoJoinListener implements DojoListener {

    @EventHandler
    public void onJoin(DojoPlayerJoinEvent e) {
        Player player = e.getPlayer();
        User user = User.getUser(player);

        PvPDojo.schedule(() -> {
            String killstreak = Redis.get().getValue(BungeeSync.KILLSTREAK + player.getUniqueId().toString());
            if (killstreak != null) {
                user.getStats().loadKillstreak(Integer.parseInt(killstreak));
            }
            DBCommon.updateUserCache(NMSUtils.get(player).getProfile());
            DBCommon.connectPlayerToDB(player);
            try {
                ChallengeManager.get().callChallenge(user, DojoChallenge.T1_DAILY_JOIN, 1);
                user.setSettings(DBCommon.loadSettings(SettingsType.USER, player.getUniqueId()));
                user.setFastDB(DBCommon.loadSettings(SettingsType.FAST_DB, player.getUniqueId()));
                user.setCooldownSettings(DBCommon.loadSettings(SettingsType.COOLDOWN, player.getUniqueId()));
                user.setCurrentAccountGrid(DBCommon.getAccountGrid(user.getUUID()));
            } catch (Throwable throwable) {
                Log.exception("Redis pull", throwable);
                PvPDojo.schedule(() -> player.kickPlayer("Failed to load settings")).sync();
            }
            user.getPersistentData().loadData(true);

            // Send offline messages
            MessageQueue queue = DBCommon.loadSettings(SettingsType.MESSAGE_QUEUE, player.getUniqueId());
            queue.getMessages().forEach(player::sendMessage);
            DBCommon.deleteSettings(SettingsType.MESSAGE_QUEUE, player.getUniqueId());

        }).createAsyncTask();
    }
}
