/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.listeners;

import java.sql.SQLException;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.achievement.AchievementManager;
import com.pvpdojo.bukkit.events.DojoPlayerJoinEvent;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.challenges.ChallengeManager;
import com.pvpdojo.challenges.DojoChallenge;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.session.RelogData;
import com.pvpdojo.session.RelogNPC;
import com.pvpdojo.userdata.User;
import com.pvpdojo.userdata.stats.Stats;
import com.pvpdojo.util.Log;

public class StatsListener implements DojoListener {

    @EventHandler
    public void onJoin(DojoPlayerJoinEvent e) {
        User user = User.getUser(e.getPlayer());
        if (user.getStats().isLoadable()) {
            PvPDojo.schedule(() -> {
                try {
                    user.getStats().load();
                } catch (SQLException ex) {
                    Log.exception(ex);
                }
            }).createAsyncTask();
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onQuit(PlayerQuitEvent e) {
        User user = User.getUser(e.getPlayer());
        user.getStats().addPlayTime(user.getStats().getCurrentPlayTime());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDeath(PlayerDeathEvent e) {

        Player victim = e.getEntity();
        User user = User.getUser(victim);

        if (user.useStats()) {
            user.getStats().resetKillstreak();
            handleDeath(user.getUUID(), user.getStats());
        }

        if (victim.getKiller() != null) {
            User kuser = User.getUser(victim.getKiller());
            handleKill(kuser);
        }
    }

    @EventHandler
    public void onDeath(EntityDeathEvent e) {

        RelogData relogData = RelogNPC.getData(e.getEntity());

        if (relogData != null) {
            handleDeath(relogData.getUniqueId(), relogData.getStats());

            if (e.getEntity().getKiller() != null) {
                handleKill(User.getUser(e.getEntity().getKiller()));
            }

        }
    }

    private void handleDeath(UUID uuid, Stats stats) {
        stats.addDeath();
        PvPDojo.schedule(() -> Redis.get().delete(BungeeSync.KILLSTREAK + uuid.toString())).createAsyncTask();
    }

    private void handleKill(User kuser) {

        if (kuser.useStats()) {
            int streak = kuser.getStats().incrKillstreak();
            kuser.getStats().addKill();


            PvPDojo.schedule(() -> {
                kuser.getStats().applyMaxKillstreak(streak);
                Redis.get().setValue(BungeeSync.KILLSTREAK + kuser.getUniqueId(), String.valueOf(streak), 60 * 60);

                ChallengeManager.get().callChallenge(kuser, DojoChallenge.T1_DAILY_KILLS, 1);
                ChallengeManager.get().callChallenge(kuser, DojoChallenge.T2_DAILY_KILLS, 1);
            }).createAsyncTask();

            AchievementManager.inst().handleKill(kuser, streak);
        }

    }

}
