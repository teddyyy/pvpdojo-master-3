/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.listeners;

import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.command.staff.InvseeCommand;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class PvPDojoModeListener implements DojoListener {

    @EventHandler(priority = EventPriority.LOW)
    public void onPickup(PlayerPickupItemEvent e) {
        User user = User.getUser(e.getPlayer());
        if (user.isAdminMode() || user.isSpectator()) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onDrop(PlayerDropItemEvent e) {
        User user = User.getUser(e.getPlayer());
        if (user.isAdminMode() || user.isSpectator()) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onClick(InventoryClickEvent e) {
        if (User.getUser((Player) e.getWhoClicked()).isSpectator()) {
            e.setCancelled(true);
        }

        if (InvseeCommand.VIEWING.contains(e.getWhoClicked().getUniqueId()) && !User.getUser((Player) e.getWhoClicked()).getRank().inheritsRank(Rank.MODERATOR_PLUS)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        InvseeCommand.VIEWING.remove(e.getPlayer().getUniqueId());
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {
            User user = User.getUser((Player) e.getEntity());
            if (user.isSpectator() || user.isFrozenByStaff()) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onDamage(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player) {
            User user = User.getUser((Player) e.getDamager());
            if (user.isSpectator() || user.isAdminMode() || user.isFrozenByStaff()) {
                e.setCancelled(true);
            }
        } else if (e.getDamager() instanceof Projectile && ((Projectile) e.getDamager()).getShooter() instanceof Player) {
            User user = User.getUser((Player) ((Projectile) e.getDamager()).getShooter());
            if (user.isSpectator() || user.isAdminMode() || user.isFrozenByStaff()) {
                e.setCancelled(true);
            }
        }

        if (e.getEntity() instanceof Player) {
            User victim = User.getUser((Player) e.getEntity());
            if (victim.isFrozenByStaff()) {
                e.setCancelled(true);
                if (e.getDamager() instanceof Player) {
                    User.getUser((Player) e.getDamager()).sendMessage(MessageKeys.PLAYER_FROZEN);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onInteract(PlayerInteractEvent e) {
        User user = User.getUser(e.getPlayer());
        if (user.isSpectator() || (e.getAction() == Action.PHYSICAL && user.isAdminMode())) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onInteractEntity(PlayerInteractEntityEvent e) {
        if (User.getUser(e.getPlayer()).isSpectator()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityTarget(EntityTargetEvent e) {
        if (e.getTarget() != null && e.getTarget() instanceof Player && ((Player) e.getTarget()).isOnline() && User.getUser((Player) e.getTarget()).isSpectator()) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onSpectatorSpeedLimit(PlayerMoveEvent e) {
        User user = User.getUser(e.getPlayer());
        if (user.isSpectator() && !user.getRank().inheritsRank(Rank.MODERATOR)) {
            // Going at 45 blocks/s is enough for them. Don't fuck with chunk loading!
            if (Math.pow(e.getFrom().getX() - e.getTo().getX(), 2) + Math.pow(e.getFrom().getZ() - e.getTo().getZ(), 2) > 5) {
                e.setCancelled(true);
                user.setFrozen(true);
                user.setFrozen(false);
                user.sendMessage(CC.RED + "Slow down");
            }
        }
    }

}
