/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.listeners.DojoListener;
import com.pvpdojo.menu.InventoryMenu;
import com.pvpdojo.userdata.User;

import eu.the5zig.mod.server.api.ModUser;

public class InventoryListener implements DojoListener {

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        User user = User.getUser((Player) e.getWhoClicked());

        if (user.getMenu() != null) {
            InventoryMenu menu = user.getMenu();
            if (!menu.isLocked() && menu.getCurrentPage().getAction(e.getRawSlot()) != null) {
                menu.getCurrentPage().getAction(e.getRawSlot()).accept(e.getClick());
                if (menu.isCloseOnClick()) {
                    menu.closeSafely();
                }
            }
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        Player player = (Player) e.getPlayer();
        User user = User.getUser(player);
        InventoryMenu menu = user.getMenu();
        if (menu == null) {
            return;
        }
        if (!menu.isCloseable()) {
            PvPDojo.schedule(() -> {
                if (player.isOnline() && !player.isDead()) {
                    menu.open();
                    if (menu.getCloseMessage() != null && !menu.getCloseMessage().trim().isEmpty()) {
                        ModUser modUser = ModUser.getUser(player);
                        if (modUser != null) {
                            modUser.sendOverlay(menu.getCloseMessage());
                        } else {
                            player.sendMessage(menu.getCloseMessage());
                        }
                    }
                }
            }).nextTick();
        } else {
            menu.onClose();
            user.setMenu(null);
        }
    }

}
