/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;

public class ChallengeManager {

    private static final ChallengeManager instance = new ChallengeManager();

    public static ChallengeManager get() {
        return instance;
    }

    public void getChallenge(UUID uuid, DojoChallenge challenge, Consumer<Integer> call) {
        PvPDojo.schedule(() -> call.accept(Integer.valueOf(Redis.get().getValue("challenges:" + challenge.getName() + ":" + uuid)))).createNonMainThreadTask();
    }

    public void getAllChallenges(UUID uuid, Consumer<HashMap<DojoChallenge, Integer>> call) {
        final DojoChallenge[] array = DojoChallenge.values();
        final String[] keys = new String[array.length];
        int i = -1;
        for (DojoChallenge challenge : array) {
            i++;
            keys[i] = "challenges:" + challenge.getName() + ":" + uuid;
        }
        PvPDojo.schedule(() -> {
            List<String> values = Redis.get().mGet(keys);
            int idx = -1;
            HashMap<DojoChallenge, Integer> map = new HashMap<>();
            for (DojoChallenge challenge : array) {
                idx++;
                String value = values.get(idx);
                if (value == null)
                    continue;
                map.put(challenge, Integer.valueOf(value));
            }
            call.accept(map);
        }).createNonMainThreadTask();
    }

    public void nullifyChallenge(UUID uuid, DojoChallenge challenge) {
        PvPDojo.schedule(() -> Redis.get().setNotExistIncrement("challenges:" + challenge.getName() + ":" + uuid, 0, -10000000, challenge.getExpire())).createNonMainThreadTask();
    }

    /*
     * public void rewardChallenge(String uuid, final DojoChallenge challenge){ if (!DojoChampion.containsChampion(uuid))return; final DojoChampion champion = DojoChampion.getChampion(uuid); final
     * Player player = champion.getPlayer(); new DojoRunnable(){ public void run(){ RedisBackend.get().setNotExistIncrement("challenges:" + challenge.getName() + ":" + uuid, 0, 200000,
     * challenge.getExpire()); int credits = 100; champion.getChampionData().incrementMoney(credits); if (player != null){ if (player.isOnline())player.sendMessage(CC.GRAY.color() + "You recieved " +
     * CC.GREEN.color() + credits + CC.GRAY.color() + " for completing " + challenge.getName()); } } }.createAsyncTask(); }
     */

    public long getTimeLeft(UUID uuid, DojoChallenge challenge) {
        return Redis.get().getTTL("challenges:" + challenge.getName() + ":" + uuid);
    }

    public void callChallenge(User user, DojoChallenge challenge, int increment) {

        if (true) {
            return;
        }

        if (challenge == null || user == null) {
            return;
        }
        final Player player = user.getPlayer();
        if (player == null) {
            return;
        }

        String uuid = player.getUniqueId().toString();
        PvPDojo.schedule(() -> {
            String string = Redis.get().getValue("challenges:" + challenge.getName() + ":" + uuid);
            int value = 0;
            if (string != null) {
                value = Integer.valueOf(string);
            }
            Redis.get().setNotExistIncrement("challenges:" + challenge.getName() + ":" + uuid, 1, increment, challenge.getExpire());
            if (value + 1 == challenge.getValue()) {
                BungeeSync.sendMessage(player.getUniqueId(), CC.GRAY + "You completed - " + challenge.getFriendlyName() + CC.GRAY + ". "
                        + "Rewarded " + CC.GREEN + challenge.getAchievementPointReward() + " Achievement Points");
                user.getPersistentData().incrementAchievementPoints(challenge.getAchievementPointReward());
            } else if ((value + 1) % challenge.getDisplayFrequency() == 0 && value < challenge.getValue() && value > 0) {
                BungeeSync.sendMessage(player.getUniqueId(), challenge.getFriendlyName() + CC.GRAY + " > " + CC.RED + (value + 1) + CC.GRAY + "/" + CC.GREEN + challenge.getValue());
            }

        }).createNonMainThreadTask();
    }
}
