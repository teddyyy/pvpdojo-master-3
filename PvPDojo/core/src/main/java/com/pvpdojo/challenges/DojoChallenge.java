/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.challenges;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.bukkit.util.GuiItem;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public enum DojoChallenge {

    // tier 1 challenges
    T1_DAILY_JOIN(
            60 * 60 * 16,
            new ItemBuilder(Material.COAL).name(CC.GREEN + "Join Challenge").lore(CC.GRAY + "Unlocked when you join the server.").build(),
            1,
            1), // join
    // the
    // server
    T1_DAILY_KILLS(
            60 * 60 * 24 * 2,
            new ItemBuilder(Material.COAL).name(CC.GREEN + "Kills Challenge").lore(CC.GRAY + "Unlocked when you kill 10 players.").build(),
            10,
            1),
    T1_DAILY_DEATHMATCH(
            60 * 60 * 24 * 2,
            new ItemBuilder(Material.COAL).name(CC.GREEN + "Deathmatch Challenge").lore(CC.GRAY + "Unlocked when you complete 20 deathmatches.",
                    CC.GRAY + "Can be any casual or ranked 1v1 and 2v2").build(),
            20,
            1), // play
    // 10
    // deathmatches
    T1_DAILY_DOJOBOT(
            60 * 60 * 24 * 2,
            new ItemBuilder(Material.COAL).name(CC.GREEN + "Dojobot Challenge").lore(CC.GRAY + "Unlocked when you beat the dojobot", CC.GRAY + "3 times").build(),
            3,
            1), // play
    // 10
    // deathmatches
    //T1_DAILY_DOJOGAMES(
    //       60 * 60 * 24 * 2,
    //      new ItemBuilder(Material.COAL).name(CC.GREEN + "DojoGames Challenge").lore(CC.GRAY + "Unlocked after joining 5 dojogames").build(),
    //        5,
    //        1), // play
    // 3
    // dojo
    // games

    T2_DAILY_KILLS(
            60 * 60 * 24 * 7,
            new ItemBuilder(Material.COAL).name(CC.DARK_PURPLE + "Rampage Challenge").lore(CC.GRAY + "Unlocked when you kill 50 players").build(),
            50,
            2),
    // T2_DAILY_RANKED2V2WIN(60 * 60 * 24 * 5,GuiItem.create(Material.COAL,
    // CC.DARK_PURPLE + "Duo Ranked Challenge", new
    // String[]{CC.GRAY + "Unlocked after winning 25 ranked
    // 2v2's"}), 25, 2),
    T2_DAILY_RANKED1V1WIN(
            60 * 60 * 24 * 7,
            new ItemBuilder(Material.COAL).name(CC.DARK_PURPLE + "1v1 Ranked Challenge").lore(CC.GRAY + "Unlocked after winning 30 ranked 1v1's").build(),
            30,
            2),
    T2_DAILY_DOJOGAMESTOP5(
            60 * 60 * 24 * 5,
            new ItemBuilder(Material.COAL).name(CC.DARK_PURPLE + "Final 5 Challenge").lore(CC.GRAY + "Unlocked after placing in the top 5", CC.GRAY + "of DojoGames 5 times").build(),
            5,
            2),
    T2_DAILY_DOJOGAMESTOP3(
            60 * 60 * 24 * 5,
            new ItemBuilder(Material.COAL).name(CC.DARK_PURPLE + "Final 3 Challenge").lore(CC.GRAY + "Unlocked after placing in the top 3", CC.GRAY + "of DojoGames 3 times").build(),
            3,
            2);

    // T1_WEEKLY_KILLS(150, 1),//kill 150 players
    // T1_WEEKLY_DEATHMATCH(50, 1),//play 50 1v1's 2v2's casual or ranked
    // T1_WEEKLY_DOJOGAMES(10, 1),// play 10 dojo games
    // 3 bonus chest for completing

    // tier 2 challenges
    // T2_DAILY_RANKED1V1STREAK(3, 2),// win 3 1v1's ranked in a row
    // T2_DAILY_RANKED2V2STREAK(3, 2),// win 3 2v2's ranked in a row
    // T2_WEEKLY_RANKED1V1WIN(50, 2),// win 50 1v1's ranked
    // T2_WEEKLY_RANKED2V2WIN(25, 2),// win 25 2v2's ranked
    // T2_WEEKLY_DOJOGAMESTOP5(5, 2),// place in top 5 in dojogames 5 times
    // 5 bonus chest for completing + 1/5 of a chest per challenge

    // tier 3 challenges
    // T3_DAILY_DOJOGAMESWIN(1, 3),// win dojo games
    // T3_DAILY_DOJOGAMESTOP5(3, 3); //place top 5 in dojogames 3 times
    // T3_WEEKLY_DOJOGAMESWIN(10, 3), //win 10 dojogames
    // T3_WEEKLY_RANKED1V1STREAK(5, 3), //win 5 ranked in a row
    // T3_WEEKLY_RANKED2V2STREAK(5, 3); // win 5 ranked 2v2 in a row
    // 7 bonus chest for completing

    private int frequency;
    private int value;
    private int tier;
    private ItemStack icon;

    DojoChallenge(int frequency, ItemStack icon, int amount, int tier) {
        this.frequency = frequency;
        this.value = amount;
        this.tier = tier;
        this.icon = icon;
    }

    public ItemStack getIcon() {
        return icon;
    }

    public String getName() {
        return this.name();
    }

    public String getFriendlyName() {
        return GuiItem.getDisplayName(icon);
    }

    public int getValue() {
        return value;
    }

    public int getTier() {
        return tier;
    }

    public int getExpire() {
        return frequency;
    }

    public int getDisplayFrequency() {
        if (value <= 5) {
            return 1;
        } else if (value > 5 && value <= 15) {
            return 3;
        } else if (value > 15 && value < 50) {
            return 5;
        } else
            return 10;
    }

    public static DojoChallenge getChallengeByName(String name) {
        for (DojoChallenge challenge : values()) {
            if (CC.stripColor(challenge.getFriendlyName()).equalsIgnoreCase(name))
                return challenge;
        }
        return null;
    }

    public int getAchievementPointReward() {
        return tier;
    }

    public int getChestReward() {
        if (this.tier == 1) {
            return 1;
        } else if (this.tier == 2) {
            return 2;
        }
        if (this.tier == 3) {
            return 3;
        }
        return 0;
    }
}
