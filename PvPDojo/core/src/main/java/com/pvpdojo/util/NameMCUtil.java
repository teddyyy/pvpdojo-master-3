/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;

public class NameMCUtil {

    public static final String CHECK_URL = "https://api.namemc.com/server/pvpdojo.com/likes?profile=";

    public static boolean hasLiked(UUID uuid) throws IOException {
        return Boolean.valueOf(read(uuid));
    }

    private static String read(UUID uuid) throws IOException {
        final URL url = new URL(CHECK_URL + uuid.toString());
        final HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestMethod("GET");

        return new BufferedReader(new InputStreamReader(con.getInputStream())).readLine();
    }

}
