/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

import com.google.common.base.CaseFormat;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.lang.LocaleMessage;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.session.RelogData;
import com.pvpdojo.session.RelogNPC;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ImageMessage;
import com.pvpdojo.util.bukkit.ImageMessage.ImageChar;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.StringJoiner;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringUtils {

    private static final Pattern LAST_WORD = Pattern.compile("^.*?(\\w+)\\W*$");
    public static final Pattern PIPE = Pattern.compile("\\|");
    public static final Pattern COLON = Pattern.compile(":");
    public static final Pattern COMMA = Pattern.compile(",");
    public static final Pattern MOJANG_UUID = Pattern.compile("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}");
    public static final Pattern UUID = Pattern.compile("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}", Pattern.CASE_INSENSITIVE);
    public static final Pattern PUNCTATION = Pattern.compile("\\p{Punct}");
    public static final Pattern SHORT_CHAR = Pattern.compile("[:!'()./;\\[\\]|{}]");
    private static final LoadingCache<UUID, ImageMessage> headCache;

    static {
        headCache = CacheBuilder.newBuilder()
                                .expireAfterWrite(12, TimeUnit.HOURS)
                                .maximumSize(100)
                                .build(CacheLoader.from(uuid -> {
                                    try {
                                        return new ImageMessage(ImageIO.read(new URL(String.format("https://visage.surgeplay.com/face/%s", uuid.toString()))), 8,
                                                ImageChar.BLOCK.getChar());
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }));
    }

    public static TextComponent createComponent(String msg, String hovermsg, String command) {

        TextComponent tc = new TextComponent();
        tc.setText(msg);
        if (command != null)
            tc.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/" + command));

        tc.setHoverEvent(new HoverEvent(net.md_5.bungee.api.chat.HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(hovermsg).create()));

        return tc;

    }

    public static ImageMessage getHead(String name) throws ExecutionException {
        return headCache.get(PvPDojo.getOfflinePlayer(name).getUniqueId());
    }

    public static ImageMessage getHead(UUID uuid) throws ExecutionException {
        return headCache.get(uuid);
    }

    public static String getDateFromMillis(long millis) {
        return getDateFromMillis(millis, FormatStyle.MEDIUM);
    }

    public static String getDateFromMillis(long millis, FormatStyle format) {
        return DateTimeFormatter.ofLocalizedDateTime(format).withLocale(Locale.GERMANY).format(LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), ZoneId.systemDefault()));
    }

    public static <T> String getStringFromArray(Iterable<T> elements, Function<T, String> converter, String delimiter) {
        StringJoiner joiner = new StringJoiner(delimiter);
        for (T element : elements) {
            joiner.add(converter.apply(element));
        }
        return joiner.toString();
    }

    public static <T> String getStringFromArray(T[] elements, Function<T, String> converter, String delimiter) {
        StringJoiner joiner = new StringJoiner(delimiter);
        for (T element : elements) {
            joiner.add(converter.apply(element));
        }
        return joiner.toString();
    }

    public static <T> List<T> getListFromString(String source, Pattern delimiter, Function<String, T> converter) {
        if (source.isEmpty()) {
            return new ArrayList<>();
        }
        String[] stringElements = delimiter.split(source);
        List<T> elements = new ArrayList<>();
        for (String stringElement : stringElements) {
            elements.add(converter.apply(stringElement));
        }
        return elements;
    }

    public static <T> Set<T> getSetFromString(String source, Pattern delimiter, Function<String, T> converter) {
        if (source.isEmpty()) {
            return new HashSet<>();
        }
        String[] stringElements = delimiter.split(source);
        Set<T> elements = new HashSet<>();
        for (String stringElement : stringElements) {
            elements.add(converter.apply(stringElement));
        }
        return elements;
    }

    public static String[] appendStringArrays(String[]... stringArrays) {
        String[] result = new String[stringArrays[0].length];
        for (int i = 0; i < result.length; i++) {
            StringBuilder builder = new StringBuilder(16 * stringArrays.length);
            for (int j = 0; j < stringArrays.length; j++) {
                builder.append(stringArrays[j][i]);
            }
            result[i] = builder.toString();
        }
        return result;
    }

    public static String removeWhiteSpaces(String string) {
        return string.replaceAll("\\s", "");
    }

    public static String appendAll(String glue, String... input) {
        StringBuilder builder = new StringBuilder(100);
        for (int i = 0; i < input.length; i++) {
            String s = input[i];
            if (i > 0) {
                builder.append(glue);
            }
            builder.append(s);
        }
        return builder.toString();
    }

    public static String getTimeLeft(long end) {
        if (end == -1) {
            return "forever";
        }
        long time = end - System.currentTimeMillis();
        if (time == 0) {
            return "0";
        }
        long seconds = (time / 1000) % 60;
        long minutes = ((time / (1000 * 60)) % 60);
        long hours = ((time / (1000 * 60 * 60)) % 24);
        long days = ((time / (1000 * 60 * 60 * 24)));

        StringBuilder remaining = new StringBuilder();

        boolean spaceNext = false;
        if (days > 0) {
            remaining.append(days).append(" day(s)");
            spaceNext = true;
        }
        if (hours > 0) {
            remaining.append(spaceNext ? " " : "").append(hours).append(" hour(s)");
            spaceNext = true;
        }
        if (minutes > 0) {
            remaining.append(spaceNext ? " " : "").append(minutes).append(" minute(s)");
            spaceNext = true;
        }
        if (seconds > 0) {
            remaining.append(spaceNext ? " " : "").append(seconds).append(" second(s)");
        }
        return remaining.toString();
    }

    public static <T extends Enum<?>> T getEnumFromString(String input, Class<T> en) {
        return Stream.of(en.getEnumConstants()).filter(enumConst -> enumConst.name().equalsIgnoreCase(input) || getEnumName(enumConst).equalsIgnoreCase(input)).findFirst()
                     .orElse(null);
    }

    public static <T extends Enum<?>> T getEnumFromString(String input, Class<T> en, Function<T, String> extraString) {
        return Stream.of(en.getEnumConstants()).filter(
                enumConst -> enumConst.name().equalsIgnoreCase(input) || getEnumName(enumConst).equalsIgnoreCase(input) || extraString.apply(enumConst).equalsIgnoreCase(input))
                     .findFirst().orElse(null);
    }

    public static <T extends Enum<?>> List<String> getEnumNameList(T[] enValues) {
        return Stream.of(enValues).map(StringUtils::getEnumName).collect(Collectors.toList());
    }

    public static String getEnumName(Enum<?> en) {
        return getEnumName(en, false);
    }

    public static String getEnumName(Enum<?> en, boolean allowMultiWords) {
        if (en == null) {
            return "null";
        }
        return getReadableEnumName(en.name(), allowMultiWords);
    }

    public static String getReadableEnumName(String name, boolean allowMultiWords) {
        if (name.contains("_")) {
            String[] words = name.split("_");
            StringBuilder builder = new StringBuilder();
            for (String word : words) {
                builder.append(CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, word)).append(allowMultiWords ? " " : "");
            }
            return builder.substring(0, allowMultiWords ? builder.length() - 1 : builder.length());
        }
        return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, name);
    }

    public static String getReadableSeconds(int totalSecs) {
        int hours = totalSecs / 3600;
        int minutes = (totalSecs % 3600) / 60;
        int seconds = totalSecs % 60;
        if (hours == 0) {
            return String.format("%02d:%02d", minutes, seconds);
        }
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    public static String getReadableDuration(Duration duration) {
        return duration.toString()
                       .substring(2)
                       .replaceAll("(\\d[HMS])(?!$)", "$1 ")
                       .toLowerCase();
    }

    public static double getJaroWinklerDistance(final CharSequence first, final CharSequence second) {
        final double DEFAULT_SCALING_FACTOR = 0.1;

        if (first == null || second == null) {
            throw new IllegalArgumentException("Strings must not be null");
        }

        int[] mtp = matches(first, second);
        double m = mtp[0];
        if (m == 0) {
            return 0D;
        }
        double j = ((m / first.length() + m / second.length() + (m - mtp[1]) / m)) / 3;
        double jw = j < 0.7D ? j : j + Math.min(DEFAULT_SCALING_FACTOR, 1D / mtp[3]) * mtp[2] * (1D - j);
        return Math.round(jw * 100.0D) / 100.0D;
    }

    public static String formatSecondsDiff(int diff) {
        return formatMillisDiff(diff * 1000);
    }

    public static String formatSecondsDiff(int diff, boolean longNames) {
        return formatMillisDiff(diff * 1000, longNames);
    }

    public static String formatMillisDiff(long diff) {
        return formatMillisDiff(diff, true);
    }

    public static String formatMillisDiff(long diff, boolean longNames) {
        return formatMillisDiff(diff, 2, longNames);
    }

    public static String formatMillisDiff(long diff, int accuracy, boolean longNames) {
        Calendar c = new GregorianCalendar();
        c.setTimeInMillis(diff);
        Calendar zero = new GregorianCalendar();
        zero.setTimeInMillis(0);
        return formatDateDiff(zero, c, accuracy, longNames);
    }

    public static String formatDateDiff(long date) {
        return formatDateDiff(date, 2, true);
    }

    public static String formatDateDiff(long date, int accuracy, boolean longNames) {
        Calendar c = new GregorianCalendar();
        c.setTimeInMillis(date);
        Calendar now = new GregorianCalendar();
        return formatDateDiff(now, c, accuracy, longNames);
    }

    public static String formatDateDiff(Calendar fromDate, Calendar toDate, int accuracy, boolean longNames) {
        boolean future = false;
        if (toDate.equals(fromDate)) {
            return "now";
        }
        if (toDate.after(fromDate)) {
            future = true;
        }
        StringBuilder sb = new StringBuilder();
        int[] types = new int[] { Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH, Calendar.HOUR_OF_DAY, Calendar.MINUTE, Calendar.SECOND };
        String[] names = longNames ? new String[] { "year", "years", "month", "months", "day", "days", "hour", "hours", "minute", "minutes", "second", "seconds" } : new String[] {
                "y", "y", "m", "m", "d", "d", "h", "h", "min", "min", "s", "s" };
        int accurate = 0;
        for (int i = 0; i < types.length; i++) {
            if (accurate > accuracy) {
                break;
            }
            int diff = dateDiff(types[i], fromDate, toDate, future);
            if (diff > 0) {
                accurate++;
                sb.append(" ").append(diff).append(longNames ? " " : "").append(names[i * 2 + (diff > 1 ? 1 : 0)]);
            }
        }
        if (sb.length() == 0) {
            return "now";
        }
        return sb.toString().trim();
    }

    static int dateDiff(int type, Calendar fromDate, Calendar toDate, boolean future) {
        int diff = 0;
        long savedDate = fromDate.getTimeInMillis();
        while ((future && !fromDate.after(toDate)) || (!future && !fromDate.before(toDate))) {
            savedDate = fromDate.getTimeInMillis();
            fromDate.add(type, future ? 1 : -1);
            diff++;
        }
        diff--;
        fromDate.setTimeInMillis(savedDate);
        return diff;
    }

    /**
     * Get a proper deathmessage
     *
     * @param event        - PlayerDeathEvent or CombatTagKilledEvent
     * @param livingEntity - Combat logger entity or Player object
     * @param killer       - The killer of the LivingEntity, if there is one
     */
    public static LocaleMessage handleDeathMessage(Event event, LivingEntity livingEntity, Player killer, String abilitiesEntity, String abilitiesKiller, boolean printWeapon) {
        String name;
        String killname = null;
        CC nameColor = CC.RED;
        if (livingEntity instanceof Player) {
            name = ((Player) livingEntity).getNick();
            User pd = User.getUser((Player) livingEntity);
            if (pd.getTeam() != null && pd.getTeam().getColor() != null) {
                nameColor = pd.getTeam().getColor();
            }
            if (abilitiesEntity != null && abilitiesEntity.length() > 0 && !abilitiesEntity.equals("none")) {
                name += "(" + abilitiesEntity + ")";
            }
        } else if (livingEntity.hasMetadata(RelogNPC.RELOG_NPC)) {
            RelogData relogData = (RelogData) livingEntity.getMetadata(RelogNPC.RELOG_NPC).get(0).value();

            if (relogData.getTeam() != null && relogData.getTeam().getColor() != null) {
                nameColor = relogData.getTeam().getColor();
            }
            name = relogData.getName() + " [RelogNPC]";

        } else {
            name = livingEntity.getCustomName();
        }

        // Parse player
        if (killer != null) {
            User kd = User.getUser(killer);
            if (kd.getTeam() != null && kd.getTeam().getColor() != null) {
                killname = "" + kd.getTeam().getColor();
            } else {
                killname = "" + CC.GREEN;
            }
            killname += killer.getNick();
            if (abilitiesKiller != null && abilitiesKiller.length() > 0 && !abilitiesKiller.equals("none")) {
                killname += "(" + abilitiesKiller + ")";
            }
        }

        int distance = killer != null ? (int) killer.getLocation().distance(livingEntity.getLocation()) : 0;

        // Parse entity
        if (killname == null) {
            if (livingEntity.getLastDamageCause() instanceof EntityDamageByEntityEvent && ((EntityDamageByEntityEvent) livingEntity.getLastDamageCause()).getDamager() instanceof LivingEntity) {
                EntityDamageByEntityEvent lastDamageEvent = (EntityDamageByEntityEvent) livingEntity.getLastDamageCause();

                LivingEntity damager = (LivingEntity) lastDamageEvent.getDamager();
                if (lastDamageEvent.getDamager() instanceof Projectile) {
                    if (((Projectile) lastDamageEvent.getDamager()).getShooter() instanceof Entity) {
                        damager = (LivingEntity) ((Projectile) lastDamageEvent.getDamager()).getShooter();
                    }
                }

                distance = (int) damager.getLocation().distance(livingEntity.getLocation());

                if (damager.getCustomName() != null) {
                    killname = CC.GREEN + damager.getCustomName();
                } else {
                    killname = CC.GREEN + getEnumName(damager.getType(), true);
                }

            }
        }

        // Do not show entity weapons
        if (printWeapon && killer == null) {
            printWeapon = false;
        }

        String weapon = killer != null ? getEnumName(killer.getItemInHand().getType(), true) : "air";
        String[] replacements = new String[] { "{victim}", nameColor + name, "{killer}", killname, "{weapon}", weapon, "{distance}", String.valueOf(distance) };

        if (livingEntity.getLastDamageCause() != null) {
            switch (livingEntity.getLastDamageCause().getCause()) {
                case BLOCK_EXPLOSION:
                    return LocaleMessage.of(MessageKeys.DEATH_BLOCK_EXPLOSION, replacements);
                case CONTACT:
                    if (killer != null) {
                        return LocaleMessage.of(MessageKeys.DEATH_CONTACT_ENTITY, replacements);
                    }
                    return LocaleMessage.of(MessageKeys.DEATH_CONTACT, replacements);
                case CUSTOM:
                    // Use the death message from the event if applicable
                    if (event instanceof PlayerDeathEvent) {
                        if (killer != null) {
                            return LocaleMessage.of(nameColor + name + CC.GRAY + " " + ((PlayerDeathEvent) event).getDeathMessage() + " trying to escape " + killname);
                        } else {
                            return LocaleMessage.of(nameColor + name + CC.GRAY + " " + ((PlayerDeathEvent) event).getDeathMessage());
                        }
                    }
                    return LocaleMessage.of(nameColor + name + CC.GRAY + " died for some reason");
                case DROWNING:
                    if (killer != null) {
                        return LocaleMessage.of(MessageKeys.DEATH_DROWNING_ENTITY, replacements);
                    }
                    return LocaleMessage.of(MessageKeys.DEATH_DROWNING, replacements);
                case ENTITY_ATTACK:
                    if (killname != null) {
                        return LocaleMessage.of(printWeapon ? MessageKeys.DEATH_ENTITY_WEAPON : MessageKeys.DEATH_ENTITY, replacements);
                    }
                    return LocaleMessage.of(MessageKeys.DEATH_ENTITY_UNKNOWN, replacements);
                case ENTITY_EXPLOSION:
                    if (killer != null) {
                        return LocaleMessage.of(MessageKeys.DEATH_ENTITY_EXPLOSION, replacements);
                    }
                    return LocaleMessage.of(MessageKeys.DEATH_BLOCK_EXPLOSION, replacements);
                case FALL:
                    if (killer != null) {
                        return LocaleMessage.of(MessageKeys.DEATH_FALL_ENTITY, replacements);
                    }
                    return livingEntity.getFallDistance() > 5 ? LocaleMessage.of(MessageKeys.DEATH_FALL_FAR, replacements) : LocaleMessage.of(MessageKeys.DEATH_FALL, replacements);
                case FALLING_BLOCK:
                    return LocaleMessage.of(MessageKeys.DEATH_FALLING_BLOCK, replacements);
                case FIRE:
                    if (killer != null) {
                        return LocaleMessage.of(MessageKeys.DEATH_FIRE_ENTITY, replacements);
                    }
                    return LocaleMessage.of(MessageKeys.DEATH_FIRE, replacements);
                case FIRE_TICK:
                    if (killer != null) {
                        return LocaleMessage.of(MessageKeys.DEATH_FIRE_TICK_ENTITY, replacements);
                    }
                    return LocaleMessage.of(MessageKeys.DEATH_FIRE_TICK, replacements);
                case LAVA:
                    if (killer != null) {
                        return LocaleMessage.of(MessageKeys.DEATH_LAVA_ENTITY, replacements);
                    }
                    return LocaleMessage.of(MessageKeys.DEATH_LAVA, replacements);
                case LIGHTNING:
                    if (killer != null) {
                        return LocaleMessage.of(MessageKeys.DEATH_LIGHTNING_ENTITY, replacements);
                    }
                    return LocaleMessage.of(MessageKeys.DEATH_LIGHTNING, replacements);
                case MAGIC:
                    if (killer != null) {
                        return LocaleMessage.of(MessageKeys.DEATH_MAGIC_ENTITY, replacements);
                    }
                    return LocaleMessage.of(MessageKeys.DEATH_MAGIC, replacements);
                case PROJECTILE:
                    if (killname != null) {
                        if (distance >= 100) {
                            return LocaleMessage.of(MessageKeys.DEATH_PROJECTILE_FREAK_SNIPE, replacements);
                        } else if (distance >= 60) {
                            return LocaleMessage.of(MessageKeys.DEATH_PROJECTILE_SNIPE, replacements);
                        } else {
                            return LocaleMessage.of(MessageKeys.DEATH_PROJECTILE, replacements);
                        }
                    }
                    return LocaleMessage.of(MessageKeys.DEATH_PROJECTILE_UNKNOWN, replacements);
                case STARVATION:
                    if (killer != null) {
                        return LocaleMessage.of(MessageKeys.DEATH_STARVATION_ENTITY, replacements);
                    }
                    return LocaleMessage.of(MessageKeys.DEATH_STARVATION, replacements);
                case SUFFOCATION:
                    return LocaleMessage.of(MessageKeys.DEATH_SUFFOCATION, replacements);
                case SUICIDE:
                    return LocaleMessage.of(MessageKeys.DEATH_SUICIDE, replacements);
                case THORNS:
                    return LocaleMessage.of(MessageKeys.DEATH_THORNS, replacements);
                case VOID:
                    if (killer != null) {
                        return LocaleMessage.of(MessageKeys.DEATH_VOID_ENTITY, replacements);
                    }
                    return LocaleMessage.of(MessageKeys.DEATH_VOID, replacements);
                case WITHER:
                    return LocaleMessage.of(MessageKeys.DEATH_WITHER, replacements);
            }
        }

        return LocaleMessage.of(MessageKeys.DEATH_UNKNOWN, replacements);
    }

    private static int[] matches(final CharSequence first, final CharSequence second) {
        CharSequence max, min;
        if (first.length() > second.length()) {
            max = first;
            min = second;
        } else {
            max = second;
            min = first;
        }
        int range = Math.max(max.length() / 2 - 1, 0);
        int[] matchIndexes = new int[min.length()];
        Arrays.fill(matchIndexes, -1);
        boolean[] matchFlags = new boolean[max.length()];
        int matches = 0;
        for (int mi = 0; mi < min.length(); mi++) {
            char c1 = min.charAt(mi);
            for (int xi = Math.max(mi - range, 0), xn = Math.min(mi + range + 1, max.length()); xi < xn; xi++) {
                if (!matchFlags[xi] && c1 == max.charAt(xi)) {
                    matchIndexes[mi] = xi;
                    matchFlags[xi] = true;
                    matches++;
                    break;
                }
            }
        }
        char[] ms1 = new char[matches];
        char[] ms2 = new char[matches];
        for (int i = 0, si = 0; i < min.length(); i++) {
            if (matchIndexes[i] != -1) {
                ms1[si] = min.charAt(i);
                si++;
            }
        }
        for (int i = 0, si = 0; i < max.length(); i++) {
            if (matchFlags[i]) {
                ms2[si] = max.charAt(i);
                si++;
            }
        }
        int transpositions = 0;
        for (int mi = 0; mi < ms1.length; mi++) {
            if (ms1[mi] != ms2[mi]) {
                transpositions++;
            }
        }
        int prefix = 0;
        for (int mi = 0; mi < min.length(); mi++) {
            if (first.charAt(mi) == second.charAt(mi)) {
                prefix++;
            } else {
                break;
            }
        }
        return new int[] { matches, transpositions / 2, prefix, max.length() };
    }

    public static boolean stringArrayContains(String string, String[] array) {
        for (String s : array) {
            if (s.equals(string)) {
                return true;
            }
        }
        return false;
    }

    public static String getLS() {
        return CC.STRIKETHROUGH + "                              ";
    }

    public static String getLastWord(String input) {
        return LAST_WORD.matcher(input).replaceAll("$1");
    }

    public static String getRandomHexString(int numchars) {
        StringBuilder sb = new StringBuilder();
        while (sb.length() < numchars) {
            sb.append(Integer.toHexString(PvPDojo.RANDOM.nextInt()));
        }
        return sb.substring(0, numchars);
    }

    public static String toBase64(String source) {
        return Base64.getEncoder().encodeToString(source.getBytes(StandardCharsets.UTF_8));
    }

    public static String fromBase64(String source) {
        return new String(Base64.getDecoder().decode(source.getBytes(StandardCharsets.UTF_8)));
    }

    public static String leftpad(String text, int length) {
        return String.format("%" + length + "." + length + "s", text);
    }

    public static String rightpad(String text, int length) {
        return String.format("%-" + length + "." + length + "s", text);
    }

    public static List<String> wrapText(String text, int length) {
        List<String> output = new ArrayList<>();
        String[] lines = text.split("\\r?\\n");
        StringBuilder currentBuilder = new StringBuilder(length);

        for (String token : lines) {
            if (currentBuilder.length() + token.length() + 1 <= length) {
                currentBuilder.append(token).append("\n");
            } else {
                output.add(currentBuilder.toString());
                currentBuilder = new StringBuilder(length);
                currentBuilder.append(token);
            }
        }

        if (currentBuilder.length() != 0) {
            output.add(currentBuilder.toString());
        }

        return output;
    }

    public static boolean isJSONValid(String test) {
        if (test == null) {
            return false;
        }
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

}
