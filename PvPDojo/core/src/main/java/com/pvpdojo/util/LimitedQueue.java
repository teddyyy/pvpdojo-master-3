/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

import java.util.LinkedList;

public class LimitedQueue<E> extends LinkedList<E> {

    private static final long serialVersionUID = -1757382252035908923L;
    private int limit;

    public LimitedQueue(int limit) {
        this.limit = limit;
    }

    public boolean isFull() {
        return size() >= limit;
    }

    @Override
    public boolean add(E o) {
        while (size() >= limit)
            super.remove();
        return super.add(o);
    }
}
