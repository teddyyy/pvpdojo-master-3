/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

import java.util.concurrent.TimeUnit;

public class TimeUtil {

    public static boolean hasPassed(long past, TimeUnit unit, long amount) {
        return System.currentTimeMillis() - past > unit.toMillis(amount);
    }

}
