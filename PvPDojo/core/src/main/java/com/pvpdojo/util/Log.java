/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

import com.pvpdojo.Discord;
import com.pvpdojo.PvPDojo;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import org.apache.commons.lang.exception.ExceptionUtils;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Log {

    private static final ExecutorService DISCORD_THREAD = Executors.newSingleThreadExecutor();

    public static Logger getLogger() {
        return PvPDojo.get().getLogger();
    }

    public static void log(Level logLevel, String msg) {
        getLogger().log(logLevel, msg);
    }

    public static void info(String msg) {
        getLogger().info(msg);
    }

    public static void fine(String msg) {
        getLogger().fine(msg);
    }

    public static void warn(String msg) {
        getLogger().warning(msg);
    }

    public static void severe(String msg) {
        getLogger().severe(msg);
    }

    public static void exception(String description, Throwable throwable) {
        getLogger().log(Level.SEVERE, description, throwable);
    }

    public static void exception(Throwable throwable) {
        exception(throwable.getMessage(), throwable);
    }

    public static Void discordException(Throwable throwable) {
        DISCORD_THREAD.execute(() -> {
            try {
                String trace = ExceptionUtils.getStackTrace(throwable);
                for (String splitTrace : StringUtils.wrapText(trace, 1960)) {
                    Message newMessage = new MessageBuilder().append(new File(System.getProperty("user.dir")).getName()).appendCodeBlock(splitTrace, "java").build();
                    List<Message> messages = Discord.blamePayback().getHistory().retrievePast(20).complete();

                    try {
                        Optional<Message> first = messages.stream()
                                                          .filter(message -> {
                                                              String raw = message.getContentRaw();
                                                              int lastIndex = raw.lastIndexOf("\n");
                                                              if (lastIndex != -1 && raw.substring(lastIndex + 1).matches("x[0-9]+")) {
                                                                  String sub = raw.substring(0, raw.lastIndexOf("\n"));
                                                                  return sub.equals(newMessage.getContentRaw());
                                                              }
                                                              return message.getContentRaw().equals(newMessage.getContentRaw());
                                                          }).findFirst();
                        if (first.isPresent()) {
                            String raw = first.get().getContentRaw();
                            String lastLine = raw.substring(first.get().getContentRaw().lastIndexOf("\n") + 1);
                            if (lastLine.matches("x[0-9]+")) {
                                int times = Integer.valueOf(lastLine.substring(1)) + 1;
                                first.get().editMessage(raw.substring(0, raw.lastIndexOf("\n")) + "\nx" + times).complete();
                            } else {
                                first.get().editMessage(raw + "\nx2").complete();
                            }
                        } else {
                            Discord.blamePayback().sendMessage(newMessage).complete();
                        }
                    } catch (Throwable thrown) {
                        thrown.printStackTrace();
                    }

                }
            } catch (Throwable thrown) {
                thrown.printStackTrace(); // Do not apply to any logger to prevent overflow
            }
        });
        return null;
    }

}
