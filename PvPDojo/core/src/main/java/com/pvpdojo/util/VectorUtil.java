/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

import com.pvpdojo.PvPDojo;
import org.bukkit.Location;
import org.bukkit.util.Vector;

public class VectorUtil {

    public static Vector getMidpoint(com.sk89q.worldedit.Vector min, com.sk89q.worldedit.Vector max) {
        return toBukkitVector(min).getMidpoint(toBukkitVector(max));
    }

    public static Vector toBukkitVector(com.sk89q.worldedit.Vector vector) {
        return new Vector(vector.getX(), vector.getY(), vector.getZ());
    }

    public static Vector getRandomVelocity() {
        final double power = 0.3D;
        double rix = PvPDojo.RANDOM.nextBoolean() ? -power : power;
        double riz = PvPDojo.RANDOM.nextBoolean() ? -power : power;
        double x = PvPDojo.RANDOM.nextBoolean() ? (rix * (0.25D + (PvPDojo.RANDOM.nextInt(3) / 5D))) : 0.0D;
        double y = 0.6D + (PvPDojo.RANDOM.nextInt(2) / 4.5D);
        double z = PvPDojo.RANDOM.nextBoolean() ? (riz * (0.25D + (PvPDojo.RANDOM.nextInt(3) / 5D))) : 0.0D;

        return new Vector(x, y, z);
    }

    public static Location getPointOnLine(Location loc1, Location loc2, double p) {
        double x1, y1, z1, x2, y2, z2;
        x1 = loc1.getX();
        x2 = loc2.getX();
        y1 = loc1.getY();
        y2 = loc2.getY();
        z1 = loc1.getZ();
        z2 = loc2.getZ();

        double retx = x1 + p * (x2 - x1);
        double rety = y1 + p * (y2 - y1);
        double retz = z1 + p * (z2 - z1);
        return new Location(loc1.getWorld(), retx, rety, retz);
    }

    public static boolean behindLocation(Location loc1, Location loc2, double range) {
        double angle = loc1.getDirection().angle(loc2.getDirection());
        return angle < range;
    }
}
