/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;
import java.util.stream.Stream;

public class StreamUtils {

    public static <T> Predicate<T> negate(Predicate<T> predicate) {
        return predicate.negate();
    }

    public static <T> Predicate<T> and(Predicate<T> predicate, Predicate<T> other) {
        return predicate.and(other);
    }

    public static <T> Predicate<T> or(Predicate<T> predicate, Predicate<T> other) {
        return predicate.or(other);
    }

    public static <T, R> Function<T, Stream<R>> select(Class<R> clazz) {
        return e -> clazz.isInstance(e) ? Stream.of(clazz.cast(e)) : null;
    }

    public static <T, R> Function<T, R> function(ThrowingFunction<T, R> function) {
        return new CheckedFunction<>(function);
    }

    public static <T> ToIntFunction<T> intFunction(ThrowingIntFunction<T> function) {
        return new CheckedIntFunction<>(function);
    }

    public static <T> Stream<T> appendToStream(Stream<? extends T> stream, T element) {
        return Stream.concat(stream, Stream.of(element));
    }

    @FunctionalInterface
    public interface ThrowingFunction<T, R> {
        R apply(T t) throws Exception;
    }

    public interface ThrowingIntFunction<T> {
        int applyAsInt(T t) throws Exception;
    }

    private static final class CheckedIntFunction<T> implements ToIntFunction<T> {

        private final ThrowingIntFunction<T> function;

        CheckedIntFunction(ThrowingIntFunction<T> function) {
            this.function = function;
        }

        @Override
        public int applyAsInt(T t) {
            try {
                return function.applyAsInt(t);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    private static final class CheckedFunction<T, R> implements Function<T, R> {

        private final ThrowingFunction<T, R> function;

        CheckedFunction(ThrowingFunction<T, R> function) {
            this.function = function;
        }

        @Override
        public R apply(T t) {
            try {
                return function.apply(t);
            } catch (Exception exception) {
                throw new RuntimeException(exception);
            }
        }
    }

}
