/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util.bukkit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_7_R4.CraftWorld;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.comphenix.protocol.wrappers.WrappedWatchableObject;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.bukkit.util.TrackedObject;
import com.pvpdojo.util.packetwrapper.AbstractPacket;
import com.pvpdojo.util.packetwrapper.WrapperPlayServerEntityMetadata;
import com.pvpdojo.util.packetwrapper.WrapperPlayServerEntityTeleport;
import com.pvpdojo.util.packetwrapper.WrapperPlayServerSpawnEntityLivingWorkaround;

import net.minecraft.server.v1_7_R4.*;

public class Hologram extends TrackedObject {
    private static final double distance = 0.23;
    private Map<Integer, EntityWitherSkull> skull = new HashMap<>();
    private Map<Integer, EntityHorse> horse = new HashMap<>();
    private List<String> lines = new ArrayList<>();
    private List<Integer> ids = new ArrayList<>();
    private boolean showing = false;
    private int refX, refY, refZ;

    public Hologram(List<String> lines) {
        this.lines = lines;
    }

    public Hologram(String... lines) {
        this.lines.addAll(Arrays.asList(lines));
    }

    public void addLine(String... lines) {
        Collections.addAll(this.lines, lines);
        if (isShowing())
            reset(false, null);
    }

    public void change(List<String> lines) {
        this.lines = lines;
        if (isShowing()) {
            reset(false, null);
        }
    }

    public void move(Location location, Player player) {
        this.location = location;
        location.subtract(0, distance, 0);
        int x = MathHelper.floor(location.getX() * 32) - this.refX, y = MathHelper.floor(location.getY() * 32) - this.refY, z = MathHelper.floor(location.getZ() * 32) - this.refZ;

        if (Math.abs(x) >= 1 || Math.abs(y) >= 1 || Math.abs(z) >= 1) {
            List<Packet> packets = new ArrayList<>(skull.size());
            this.refX += x;
            this.refY += y;
            this.refZ += z;

            boolean tracking = (x >= -128) && (x < 128) && (y >= -128) && (y < 128) && (z >= -128) && (z < 128);
            if (player.isProtocolHack()) {
                for (EntityHorse sk : horse.values()) {
                    if (tracking) {
                        packets.add(new PacketPlayOutRelEntityMove(sk.getId(), (byte) x, (byte) y, (byte) z, false));
                    } else {
                        reset(true, player);
                        break;
                    }
                }
            } else {
                for (EntityWitherSkull sk : skull.values()) {
                    if (tracking) {
                        packets.add(new PacketPlayOutRelEntityMove(sk.getId(), (byte) x, (byte) y, (byte) z, false));
                    } else {
                        reset(true, player);
                        break;
                    }
                }
            }
            packets.forEach(packet -> ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet));
        }

    }

    public void teleport(Location location) {
        this.location = location;
        if (isShowing()) {
            reset(true, null);
        }
    }

    public void show(Location loc, Player player) {
        if (player != null && loc.getWorld() != player.getWorld()) {
            return;
        }
        if (player != null) {
            viewers.add(player);
        }
        Location first = loc.clone().add(0, this.lines.size() / 2D * distance, 0);
        for (int i = 0; i < this.lines.size(); i++) {
            ids.addAll(showLine(first.clone(), this.lines.get(i), player, i));
            first.subtract(0, distance, 0);
        }
        showing = true;
        this.location = loc;
        this.refX = (int) (loc.getX() * 32);
        this.refY = (int) (loc.getY() * 32);
        this.refZ = (int) (loc.getZ() * 32);
    }

    public void show(Location loc, Player player, int ticks) {
        show(loc, player);
        if (ticks != Integer.MAX_VALUE) {
            PvPDojo.schedule(this::destroy).createTask(ticks);
        }
    }

    public void show(Location loc) {
        show(loc, null);
    }

    public boolean isShowing() {
        return showing;
    }

    public List<String> getLines() {
        return lines;
    }

    public void reset(boolean locationChanged, Player player) {
        if (!showing) {
            throw new IllegalStateException("Isn't showing");
        }
        if (horse.size() != lines.size() || locationChanged) {
            for (Player all : viewers) {
                NMSUtils.get(all).removeQueue.addAll(ids);
            }
            showing = false;
            ids.clear();
            horse.clear();
            skull.clear();
        }
        if (player == null)
            show(this.location);
        else
            show(location, player);

    }

    public void destroy() {
        for (Player player : viewers) {
            NMSUtils.get(player).removeQueue.addAll(ids);
        }
        ids.clear();
        showing = false;
        viewers.clear();
        location = null;
        untrack();
    }

    public static List<Hologram> getHolograms() {
        return TRACKED_OBJECTS.stream().filter(obj -> obj instanceof Hologram).map(obj -> (Hologram) obj).collect(Collectors.toList());
    }

    private List<Integer> showLine(Location loc, String text, Player player, int i) {

        if (!showing) {

            WorldServer world = ((CraftWorld) loc.getWorld()).getHandle();
            EntityWitherSkull skull = new EntityWitherSkull(world);
            skull.setLocation(loc.getX(), loc.getY() + 54.56D, loc.getZ(), 0, 0);
            PacketPlayOutSpawnEntity skull_packet = new PacketPlayOutSpawnEntity(skull, 66);
            this.skull.put(i, skull);

            EntityHorse horse = new EntityHorse(world);
            horse.setLocation(loc.getX(), loc.getY() + 54.56, loc.getZ(), 0, 0);
            horse.setAge(-1700000);
            horse.setCustomName(text);
            horse.setCustomNameVisible(true);

            PacketPlayOutSpawnEntityLiving packedt = new PacketPlayOutSpawnEntityLiving(horse);
            this.horse.put(i, horse);

            List<AbstractPacket> protocolHackPacket = getArmorStandSpawnPacket(horse, text);

            /*
             * if (player == null) { for (Player all : loc.getWorld().getPlayers()) { EntityPlayer nmsPlayer = ((CraftPlayer) all).getHandle(); if (!all.spigot().isProtocolHack()) {
             * nmsPlayer.playerConnection.sendPacket(packedt); nmsPlayer.playerConnection.sendPacket(skull_packet); PacketPlayOutAttachEntity pa = new PacketPlayOutAttachEntity(0, horse, skull);
             * nmsPlayer.playerConnection.sendPacket(pa); } else if (viewers.contains(all)) { protocolHackPacket.forEach(protocolPacket -> protocolPacket.sendPacket(all)); } } }
             */
            Collection<Player> receivers = player != null ? Collections.singletonList(player) : viewers;

            for (Player p : receivers) {
                EntityPlayer nmsPlayer = ((CraftPlayer) p).getHandle();
                if (p.isProtocolHack() && viewers.contains(p)) {
                    protocolHackPacket.forEach(protocolHack -> protocolHack.sendPacket(p));
                } else {
                    nmsPlayer.playerConnection.sendPacket(packedt);
                    nmsPlayer.playerConnection.sendPacket(skull_packet);
                    PacketPlayOutAttachEntity pa = new PacketPlayOutAttachEntity(0, horse, skull);
                    nmsPlayer.playerConnection.sendPacket(pa);
                }
            }
            return Arrays.asList(skull.getId(), horse.getId());
        } else {
            horse.get(i).setCustomName(text);
            if (player == null) {
                DataWatcher dw = horse.get(i).getDataWatcher();
                PacketPlayOutEntityMetadata packet = new PacketPlayOutEntityMetadata(horse.get(i).getId(), dw, true);
                AbstractPacket packetProtocolHack = getArmorStandMetadataPacket(horse.get(i));
                for (Player all : viewers) {
                    EntityPlayer nmsPlayer = ((CraftPlayer) all).getHandle();
                    if (all.isProtocolHack() && viewers.contains(all)) {
                        packetProtocolHack.sendPacket(all);
                    } else {
                        nmsPlayer.playerConnection.sendPacket(packet);
                    }
                }
            } else {
                EntityPlayer nmsPlayer = ((CraftPlayer) player).getHandle();
                PacketPlayOutSpawnEntityLiving horse_packet = new PacketPlayOutSpawnEntityLiving(horse.get(i));
                PacketPlayOutSpawnEntity skull_packet = new PacketPlayOutSpawnEntity(skull.get(i), 66);
                PacketPlayOutAttachEntity pa = new PacketPlayOutAttachEntity(0, horse.get(i), skull.get(i));
                List<AbstractPacket> protocolHackPacket = getArmorStandSpawnPacket(horse.get(i), text);

                if (player.isProtocolHack() && viewers.contains(player)) {
                    protocolHackPacket.forEach(protocolPacket -> protocolPacket.sendPacket(player));
                } else {
                    nmsPlayer.playerConnection.sendPacket(horse_packet);
                    nmsPlayer.playerConnection.sendPacket(skull_packet);
                    nmsPlayer.playerConnection.sendPacket(pa);
                }
            }

        }

        return new ArrayList<>();
    }

    public Location getLocation() {
        return location;
    }

    private List<AbstractPacket> getArmorStandSpawnPacket(EntityHorse horse, String text) {
        WrapperPlayServerSpawnEntityLivingWorkaround armorStand = new WrapperPlayServerSpawnEntityLivingWorkaround();

        armorStand.setEntityID(horse.getId());
        armorStand.setX(horse.locX);
        armorStand.setY(horse.locY - 55.04);
        armorStand.setZ(horse.locZ);
        armorStand.setType(30); // Armor Stand ID

        WrappedDataWatcher wdw = new WrappedDataWatcher(); // Create the data watcher
        wdw.setObject(0, (byte) 0x20); // Invisible
        wdw.setObject(2, text); // And set the armor stand name to the hologram text
        wdw.setObject(3, (byte) 1); // Always show nametag
        wdw.setObject(10, (byte) 0x16); // Zero Bounding Box (Marker)

        armorStand.setMetadata(wdw);

        WrapperPlayServerEntityTeleport armorTeleport = new WrapperPlayServerEntityTeleport();
        armorTeleport.setEntityID(horse.getId());
        armorTeleport.setX(horse.locX);
        armorTeleport.setY(horse.locY - 54.74);
        armorTeleport.setZ(horse.locZ);

        return Arrays.asList(armorStand, armorTeleport);
    }

    private AbstractPacket getArmorStandMetadataPacket(EntityHorse horse) {
        WrapperPlayServerEntityMetadata metadata = new WrapperPlayServerEntityMetadata();
        metadata.setEntityId(horse.getId());
        WrappedWatchableObject wwo = new WrappedWatchableObject(2, horse.getCustomName());
        metadata.setEntityMetadata(Collections.singletonList(wwo));
        return metadata;
    }

    @Override
    public void createFor(Player player) {
        show(location, player);
    }

    @Override
    public void destroyFor(Player player) {
        NMSUtils.get(player).removeQueue.addAll(ids);
    }

    @Override
    public Collection<Player> getTargetGroup() {
        return location.getWorld().getPlayers();
    }

}
