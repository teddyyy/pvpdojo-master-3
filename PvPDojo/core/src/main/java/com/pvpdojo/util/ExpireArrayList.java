/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.TimeUnit;

import org.jetbrains.annotations.NotNull;

import com.google.common.base.Preconditions;
import com.google.common.base.Ticker;
import com.google.common.primitives.Longs;

public class ExpireArrayList<T> implements Iterable<T>, Collection<T> {

    private Ticker ticker = Ticker.systemTicker();

    public ExpireArrayList() {

    }

    private class ExpireEntry implements Comparable<ExpireEntry> {

        public final long expireTime;
        public final T expireObject;

        public ExpireEntry(long expireTime, T expireObject) {
            this.expireTime = expireTime;
            this.expireObject = expireObject;
        }

        @Override
        public int compareTo(ExpireEntry o) {
            return Longs.compare(o.expireTime, expireTime);
        }

        @Override
        public String toString() {
            return "ExpireEntry [expireTime=" + expireTime + ", expireObject=" + expireObject + "]";
        }

    }

    private List<T> list = new ArrayList<>();
    private PriorityQueue<ExpireEntry> expireQueue = new PriorityQueue<>();

    public T get(int index) {
        return list.get(index);
    }

    public boolean add(T object, long expireDelay, TimeUnit expireUnit) {
        Preconditions.checkNotNull(object, "expireUnit cannot be NULL");
        Preconditions.checkState(expireDelay > 0, "expireDelay cannot be equal or less than zero.");
        evictExpired();

        ExpireEntry entry = new ExpireEntry(ticker.read() + TimeUnit.NANOSECONDS.convert(expireDelay, expireUnit), object);
        boolean added = list.add(object);
        expireQueue.add(entry);
        return added;
    }

    public boolean contains(Object object) {
        evictExpired();
        return list.contains(object);
    }

    public T first() {
        evictExpired();
        if (expireQueue.peek() != null)
            return expireQueue.peek().expireObject;
        return null;
    }

    public boolean remove(Object object) {
        evictExpired();
        return list.remove(object);
    }

    public int size() {
        evictExpired();
        return list.size();
    }

    @NotNull
    @Override
    public Iterator<T> iterator() {
        evictExpired();
        return list.iterator();
    }

    public void clear() {
        list.clear();
        expireQueue.clear();
    }

    protected void evictExpired() {
        long currentTime = ticker.read();
        while (expireQueue.size() > 0 && expireQueue.peek().expireTime <= currentTime) {
            ExpireEntry entry = expireQueue.poll();
            list.remove(entry.expireObject);
        }
    }

    @Override
    public boolean add(T e) {
        return add(e, 1, TimeUnit.MINUTES);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T element : c) {
            add(element);
        }
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return list.containsAll(c);
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return list.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return list.retainAll(c);
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public <E> E[] toArray(E[] a) {
        return list.toArray(a);
    }
}