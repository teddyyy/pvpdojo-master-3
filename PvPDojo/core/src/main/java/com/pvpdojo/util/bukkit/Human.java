/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util.bukkit;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_7_R4.CraftServer;
import org.bukkit.craftbukkit.v1_7_R4.CraftWorld;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_7_R4.inventory.CraftItemStack;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.bukkit.util.TrackedObject;

import lombok.Getter;
import net.minecraft.server.v1_7_R4.*;
import net.minecraft.util.com.mojang.authlib.GameProfile;

public class Human extends TrackedObject {

    public static final Map<Integer, Human> HUMAN_MAP = new ConcurrentHashMap<>();

    private Player p;
    private EntityPlayer ep;
    private boolean dead, fresh;
    private ItemStack itemInHand;
    @Getter
    private String customName = "";

    private int refX, refY, refZ, refPitch, refYaw, moves;

    public Human(Location l, Player toShow, GameProfile profile) {
        this.ep = new EntityPlayer(((CraftServer) Bukkit.getServer()).getServer(), ((CraftWorld) l.getWorld()).getHandle(), profile,
                new PlayerInteractManager(((CraftWorld) l.getWorld()).getHandle()));
        this.location = l;
        this.p = toShow;
        ep.setPositionRotation(l.getX(), l.getY(), l.getZ(), l.getPitch(), l.getYaw());
    }

    public Human(Location l, Player toShow, GameProfile profile, String customName) {
        this(l,toShow, profile);
        this.customName = customName;
    }

    @Override
    public void track() {
        super.track();
        HUMAN_MAP.put(ep.getId(), this);
    }

    @Override
    public Human untrack() {
        super.untrack();
        remove();
        HUMAN_MAP.remove(ep.getId());
        return this;
    }

    public void spawn() {
        spawn(null);
    }

    public void spawn(Player p) {
        if (p != null) {
            this.p = p;
        }
        String listname = UUID.randomUUID().toString().substring(0, 5);
        sendPacket(PacketPlayOutPlayerInfo.addPlayer(listname, ep.getProfile()), true);

        PacketPlayOutNamedEntitySpawn spawn = new PacketPlayOutNamedEntitySpawn(ep);
        sendPacket(spawn);
        refX = (int) (location.getX() * 32);
        refY = (int) (location.getY() * 32);
        refZ = (int) (location.getZ() * 32);
        refPitch = (int) (location.getPitch() * 32);
        refYaw = (int) (location.getYaw() * 32);
        fresh = true;
        moves = Integer.MAX_VALUE;
        move(location.getYaw(), location.getPitch(), ep.onGround, location);
        fresh = false;

        PvPDojo.schedule(() -> sendPacket(PacketPlayOutPlayerInfo.removePlayer(listname, ep.getProfile()), true)).createTask(5);

        sendPacket(new PacketPlayOutEntityMetadata(ep.getId(), ep.getDataWatcher(), true));
        if (p != null) {
            this.p = null;
        }

    }

    public EntityPlayer getHuman() {
        return ep;
    }

    private int getCompressedAngle(float value) {
        return MathHelper.d(value * 256.0F / 360.0F);
    }

    public void remove() {
        if (p != null) {
            remove(p);
        }
        viewers.forEach(this::remove);
    }

    public void remove(Player player) {
        NMSUtils.get(player).removeQueue.add(ep.getId());
    }

    public void setItemInHand(ItemStack hand) {
        this.itemInHand = hand;
        PacketPlayOutEntityEquipment ps = new PacketPlayOutEntityEquipment(ep.getId(), 0, CraftItemStack.asNMSCopy(hand));
        sendPacket(ps);
    }

    public void updateArmor(ItemStack boots, ItemStack leggins, ItemStack chestplate, ItemStack helmet) {
        PacketPlayOutEntityEquipment eqboots = new PacketPlayOutEntityEquipment(ep.getId(), 1, CraftItemStack.asNMSCopy(boots));
        PacketPlayOutEntityEquipment eqleggins = new PacketPlayOutEntityEquipment(ep.getId(), 2, CraftItemStack.asNMSCopy(leggins));
        PacketPlayOutEntityEquipment eqchestplate = new PacketPlayOutEntityEquipment(ep.getId(), 3, CraftItemStack.asNMSCopy(chestplate));
        PacketPlayOutEntityEquipment eqhelmet = new PacketPlayOutEntityEquipment(ep.getId(), 4, CraftItemStack.asNMSCopy(helmet));

        sendPacket(eqboots);
        sendPacket(eqleggins);
        sendPacket(eqchestplate);
        sendPacket(eqhelmet);
    }

    public void setName(String s) {
        DataWatcher d = ep.getDataWatcher();
        d.watch(10, s);

        PacketPlayOutEntityMetadata packet40 = new PacketPlayOutEntityMetadata(ep.getId(), d, true);
        sendPacket(packet40);
    }

    public void addPotionColor(Color r) {
        int color = r.asBGR();
        DataWatcher dw = ep.getDataWatcher();
        dw.a(7, color);
        PacketPlayOutEntityMetadata packet40 = new PacketPlayOutEntityMetadata(ep.getId(), dw, true);
        sendPacket(packet40);
    }

    public void addPotionColor(int color) {
        DataWatcher dw = new DataWatcher(ep);
        dw.a(7, color);
        PacketPlayOutEntityMetadata packet40 = new PacketPlayOutEntityMetadata(ep.getId(), dw, true);
        sendPacket(packet40);
    }

    public void move(float yaw, float pitch, boolean onGround, Location real) {
        ep.setPositionRotation(real.getX(), real.getY(), real.getZ(), yaw, pitch);
        int a = ep.as.a(real.getX());
        int b = (int) (real.getY() * 32D);
        int c = ep.as.a(real.getZ());

        int x = a - refX;
        int y = b - refY;
        int z = c - refZ;

        int rotYaw = getCompressedAngle(yaw);
        int rotPitch = getCompressedAngle(pitch);

        boolean moveFlag = Math.abs(x) >= 1 || Math.abs(y) >= 1 || Math.abs(z) >= 1;
        boolean rotFlag = Math.abs(rotYaw - refYaw) >= 1 || Math.abs(rotPitch - refPitch) >= 1;

        Packet packet = null;
        if (moves < 20 && (moveFlag || rotFlag) && (x >= -128) && (x < 128) && (y >= -128) && (y < 128) && (z >= -128) && (z < 128)) {
            if (moveFlag && rotFlag || dead) {
                packet = new PacketPlayOutRelEntityMoveLook(ep.getId(), (byte) x, (byte) y, (byte) z, (byte) rotYaw, (byte) rotPitch, onGround);
            } else if (moveFlag) {
                packet = new PacketPlayOutRelEntityMove(ep.getId(), (byte) x, (byte) y, (byte) z, onGround);
            } else if (rotFlag) {
                packet = new PacketPlayOutEntityLook(ep.getId(), (byte) rotYaw, (byte) rotPitch, onGround);
            }
        } else if (moveFlag || rotFlag || fresh) {
            packet = new PacketPlayOutEntityTeleport(ep);
            moves = 0;
        }
        sendPacket(packet);

        if (rotFlag) {
            PacketPlayOutEntityHeadRotation p2 = new PacketPlayOutEntityHeadRotation(ep, (byte) rotYaw);
            sendPacket(p2);
        }

        if (rotFlag) {
            refYaw = rotYaw;
            refPitch = rotPitch;
        }

        if (moveFlag) {
            refX = a;
            refY = b;
            refZ = c;
        }
        this.location = real;
        moves++;
    }

    public void setInvisible() {
        ep.setInvisible(true);
        PacketPlayOutEntityMetadata packet40 = new PacketPlayOutEntityMetadata(ep.getId(), ep.getDataWatcher(), false);
        sendPacket(packet40);
    }

    public void setCrouched(boolean crouched) {
        ep.setSneaking(crouched);
        PacketPlayOutEntityMetadata packet40 = new PacketPlayOutEntityMetadata(ep.getId(), ep.getDataWatcher(), false);
        sendPacket(packet40);
    }

    public void setSprinting(boolean sprint) {
        ep.setSprinting(sprint);
        PacketPlayOutEntityMetadata packet40 = new PacketPlayOutEntityMetadata(ep.getId(), ep.getDataWatcher(), false);
        sendPacket(packet40);
    }

    public void setBlocking(boolean blocking) {
        if (((ep.getDataWatcher().getByte(0) & 1 << 4) != 0) != blocking) {
            ep.e(blocking);
            PacketPlayOutEntityMetadata packet40 = new PacketPlayOutEntityMetadata(ep.getId(), ep.getDataWatcher(), false);
            sendPacket(packet40);
        }
    }

    public void death() {
        PacketPlayOutEntityStatus packet = new PacketPlayOutEntityStatus(ep, (byte) 3);
        this.dead = true;
        sendPacket(packet);
    }

    public void crit() {
        PacketPlayOutAnimation packet18 = new PacketPlayOutAnimation(ep, 4);
        sendPacket(packet18);
    }

    public void magicCrit() {
        PacketPlayOutAnimation packet18 = new PacketPlayOutAnimation(ep, 5);
        sendPacket(packet18);
    }

    public void damage(boolean sound) {
        if (sound) {
            p.playSound(getLocation(), Sound.HURT_FLESH, 1F, (PvPDojo.RANDOM.nextFloat() - PvPDojo.RANDOM.nextFloat()) * 0.2F + 1.0F);
        }
        PacketPlayOutAnimation packet18 = new PacketPlayOutAnimation(ep, 1);
        sendPacket(packet18);
    }

    public void swingArm() {
        PacketPlayOutAnimation packet18 = new PacketPlayOutAnimation(ep, 0);
        sendPacket(packet18);
    }

    public void eatInHand() {
        PacketPlayOutAnimation packet18 = new PacketPlayOutAnimation(ep, 5);
        sendPacket(packet18);
    }

    public void collect(Item item) {
        PacketPlayOutCollect packet = new PacketPlayOutCollect(item.getEntityId(), ep.getId());
        sendPacket(packet);
        item.remove();
    }

    public void sleep() {
        PacketPlayOutBed packet17 = new PacketPlayOutBed(ep, location.getBlockX(), location.getBlockY(), location.getBlockZ());
        sendPacket(packet17);
    }

    public boolean isDead() {
        return dead;
    }

    public double getX() {
        return this.location.getX();
    }

    public double getY() {
        return this.location.getY();
    }

    public double getZ() {
        return this.location.getZ();
    }

    public Location getLocation() {
        return this.location;
    }

    public ItemStack getItemInHand() {
        return itemInHand;
    }

    public void sendPacket(Packet packet) {
        sendPacket(packet, false);
    }

    public void sendPacket(Packet packet, boolean protocolHackOnly) {
        if (p != null) {
            if (protocolHackOnly && p.isProtocolHack()) {
                ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
            } else if (!protocolHackOnly) {
                ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
            }
        } else {
            viewers.forEach(p -> {
                if (protocolHackOnly && p.isProtocolHack()) {
                    ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
                } else if (!protocolHackOnly) {
                    ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
                }
            });
        }
    }

    @Override
    public void createFor(Player player) {
        spawn(player);
    }

    @Override
    public void destroyFor(Player player) {
        remove(player);
    }

    @Override
    public Collection<Player> getTargetGroup() {
        return location.getWorld().getPlayers();
    }

}
