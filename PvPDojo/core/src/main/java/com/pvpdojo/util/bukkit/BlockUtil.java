/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util.bukkit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.BlockIterator;

import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.regions.Region;

public class BlockUtil {

    public static Block getHighestBlock(Location location) {
        return getHighestBlock(location.getWorld(), location.getBlockX(), location.getBlockZ());
    }

    public static Block getHighestBlock(World world, int x, int z) {
        return getHighestBlockMatching(world, x, z, block -> block.getType().isSolid());
    }

    public static Block getHighestBlockMatching(World world, int x, int z, Predicate<Block> matcher) {
        int bukkitHighest = world.getHighestBlockYAt(x, z);
        Location highest = new Location(world, x, bukkitHighest != 0 ? bukkitHighest + 10 : 255, z);
        while (!matcher.test(highest.getBlock()) && highest.getY() > 0) {
            highest.subtract(0, 1, 0);
        }
        return highest.getBlock();
    }

    public static BlockFace getClosestFace(float direction) {
        direction = direction % 360 - 90;

        if (direction < 0)
            direction += 360;

        direction = Math.round(direction / 45);

        switch ((int) direction) {

            case 0:
                return BlockFace.WEST;
            case 1:
                return BlockFace.NORTH_WEST;
            case 2:
                return BlockFace.NORTH;
            case 3:
                return BlockFace.NORTH_EAST;
            case 4:
                return BlockFace.EAST;
            case 5:
                return BlockFace.SOUTH_EAST;
            case 6:
                return BlockFace.SOUTH;
            case 7:
                return BlockFace.SOUTH_WEST;
            default:
                return BlockFace.WEST;

        }
    }

    public static ArrayList<Location> getLocationsAroundPoint(double radius, int increments, Location point) {
        ArrayList<Location> list = new ArrayList<>();
        for (int i = 0; i < increments; i++) {
            float angle = 360F / (i + 1);
            double x = point.getX() + radius * Math.cos(angle);
            double z = point.getZ() + radius * Math.sin(angle);
            list.add(new Location(point.getWorld(), x, point.getY(), z));
        }
        return list;
    }

    public static Location findLocationBetweenPoints(Location start, Location finish) {
        double dif = start.getBlockY() - finish.getBlockY();
        for (int i = 0; i < dif; i++) {
            Block b = start.getWorld().getBlockAt(start.getBlockX(), finish.getBlockY() + i + 2, start.getBlockZ());
            if (!b.getType().equals(Material.AIR)) {
                return b.getLocation().subtract(0, 2, 0);
            }
        }

        return start;
    }

    public static Location getFirstBlockDown(Location start, Material material) {
        for (int i = 0; i < start.getBlockY(); i++) {
            Location loc = new Location(start.getWorld(), start.getBlockX(), start.getBlockY() - i, start.getBlockZ());
            if (!loc.getBlock().getType().equals(Material.AIR)) {
                if (material != null) {
                    if (loc.getBlock().getType().equals(material)) {
                        return loc;
                    } else {
                        continue;
                    }
                }
                return loc;
            }
        }
        return start;
    }

    public static Location getFirstBlockUp(Location start, Material material) {
        for (int i = 0; i < 256; i++) {
            Location loc = new Location(start.getWorld(), start.getBlockX(), start.getBlockY() + i, start.getBlockZ());
            if (!loc.getBlock().getType().equals(Material.AIR)) {
                if (material != null) {
                    if (loc.getBlock().getType().equals(material)) {
                        if (i == 0) {
                            loc.setY(-1); // Special case 0
                        }
                        return loc;
                    } else {
                        continue;
                    }
                }
                if (i == 0) {
                    loc.setY(-1); // Special case 0
                }
                return loc;
            }
        }
        return start;
    }

    public static Location findNearestBlock(Location l, Material material, int radiusX, int radiusY, int radiusZ) {
        for (double x = -radiusX; x < radiusX; x++) {
            for (double y = -radiusY; y < radiusY; y++) {
                for (double z = -radiusZ; z < radiusZ; z++) {
                    Location search = new Location(l.getWorld(), l.getX() + x, l.getY() + y, l.getZ() + z);
                    if (search.getBlock().getType().equals(material)) {
                        return search;
                    }
                }
            }
        }
        return null;
    }

    public static ArrayList<Block> getBlockIterator(Location loc, int yOffset, int maxDistance, boolean checkSee) {
        ArrayList<Block> list = new ArrayList<>();
        Iterator<Block> itr = new BlockIterator(loc, yOffset, maxDistance);
        while (itr.hasNext()) {
            Block block = itr.next();
            if (checkSee) {
                if (!block.getType().equals(Material.AIR)) {
                    break;
                }
            }
            list.add(block);
        }
        return list;
    }

    public static ArrayList<Block> getLineOfSight(Location loc, int distance) {
        return getBlockIterator(loc, 0, distance, true);
    }

    public static ArrayList<Block> getLineOfSight(int distance, LivingEntity entity) {
        return getBlockIterator(entity.getEyeLocation(), 0, distance, true);
    }

    public static Block getTargetBlock(int distance, LivingEntity entity) {
        List<Block> blocks = getLineOfSight(distance, entity);
        return blocks.isEmpty() ? null : blocks.get(blocks.size() - 1);
    }

    public static LivingEntity findClosestEntity(Location loc, List<Entity> nearby) {
        double closest_dist = Double.MAX_VALUE;
        LivingEntity closest = null;
        for (Entity living : nearby) {
            if (living instanceof LivingEntity) {
                Location eloc = living.getLocation();
                double distance = eloc.distanceSquared(loc);
                if (distance < closest_dist) {
                    closest = (LivingEntity) living;
                    closest_dist = distance;
                }
            }
        }
        return closest;
    }

    public static boolean sameBlock(Location loc1, Location loc2, float accuracy) {
        double x1, x2, y1, y2, z1, z2;
        x1 = loc1.getX();
        x2 = loc2.getX();
        y1 = loc1.getY();
        y2 = loc2.getY();
        z1 = loc1.getZ();
        z2 = loc2.getZ();
        if (Math.abs(x1 - x2) < accuracy) {
            if (Math.abs(y1 - y2) < (accuracy + .7)) {
                return Math.abs(z1 - z2) < accuracy;
            }
        }
        return false;
    }

    public static List<Location> circle(Location loc, int r, int h, boolean hollow, boolean sphere, int plusY) {
        List<Location> circleblocks = new ArrayList<>();
        int cx = loc.getBlockX();
        int cy = loc.getBlockY();
        int cz = loc.getBlockZ();
        for (int x = cx - r; x <= cx + r; x++)
            for (int z = cz - r; z <= cz + r; z++)
                for (int y = (sphere ? cy - r : cy); y < (sphere ? cy + r : cy + h); y++) {
                    double dist = (cx - x) * (cx - x) + (cz - z) * (cz - z) + (sphere ? (cy - y) * (cy - y) : 0);
                    if (dist < r * r && !(hollow && dist < (r - 1) * (r - 1))) {
                        Location l = new Location(loc.getWorld(), x, y + plusY, z);
                        circleblocks.add(l);
                    }
                }

        return circleblocks;
    }

    public static List<Block> getNearbyBlocks(Location center, int radius) {
        return circle(center, radius, radius, true, true, 0).stream().map(Location::getBlock).collect(Collectors.toList());
    }

    public static boolean isEmpty(World world, Region region) {
        for (Vector vector : region) {
            if (world.getBlockAt(vector.getBlockX(), vector.getBlockY(), vector.getBlockZ()).getType() != Material.AIR) {
                return false;
            }
        }
        return true;
    }


    /**
     * @param loc
     * @return the block if a player is standing on the edge [X]
     */
    public static Block getAdjacentX(Location loc) {
        int x = (int) loc.getX();
        Block base = loc.getBlock();
        if (x != (int) (loc.getX() - 0.31D) || (x == 0 && loc.getX() < 0.31D && loc.getX() > 0.0D)) {
            base = base.getRelative(-1, 0, 0);
        } else if (x != (int) (loc.getX() + 0.31D) || (x == 0 && loc.getX() > -0.31D && loc.getX() < 0.0D)) {
            base = base.getRelative(1, 0, 0);
        }
        return base;
    }

    /**
     * @param loc
     * @return the block if a player is standing on the edge [Z]
     */
    public static Block getAdjacentZ(Location loc) {
        int z = (int) loc.getZ();
        Block base = loc.getBlock();
        if (z != (int) (loc.getZ() - 0.31D) || (z == 0 && loc.getZ() < 0.31D && loc.getZ() > 0.0D)) {
            base = base.getRelative(0, 0, -1);
        } else if (z != (int) (loc.getZ() + 0.31D) || (z == 0 && loc.getZ() > -0.31D && loc.getZ() < 0.0D)) {
            base = base.getRelative(0, 0, 1);
        }
        return base;
    }

    public static String asString(Block block) {
        Location loc = block.getLocation();
        return "[" + loc.getBlockX() + ", " + loc.getBlockY() + ", " + loc.getBlockZ() + "]";
    }

}
