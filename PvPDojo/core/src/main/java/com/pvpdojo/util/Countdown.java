/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

import java.util.function.IntConsumer;

import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.lang.LanguageManager;
import com.pvpdojo.lang.MessageKeys;

import eu.the5zig.mod.server.api.ModUser;

public class Countdown extends DojoRunnable {

    private int currentCount;
    private int beat = 20;
    private IntConsumer countRunnable;
    private IntConsumer userCountRunnable;
    private Runnable finishRunnable;

    private String name;
    private Iterable<Player> modCounterUsers;

    public Countdown(int countAmount) {
        super(null);
        this.currentCount = countAmount;
        this.runnable = () -> {
            tickModCounter();
            if (currentCount == 0) {
                if (finishRunnable != null) {
                    finishRunnable.run();
                }
                return;
            } else {
                if (countRunnable != null) {
                    countRunnable.accept(currentCount);
                }
                if (userCountRunnable != null && isUserFriendlyNumber(currentCount)) {
                    userCountRunnable.accept(currentCount);
                }
            }
            currentCount--;
        };
    }

    private void tickModCounter() {
        if (modCounterUsers != null) {
            for (Player player : modCounterUsers) {
                ModUser user = ModUser.getUser(player);
                if (user != null) {
                    user.getStatsManager().startCountdown(name, currentCount * 50 * beat);
                }
            }
        }
        modCounterUsers = null;
    }

    public Countdown counter(IntConsumer countRunnable) {
        this.countRunnable = countRunnable;
        return this;
    }

    public Countdown userCounter(IntConsumer userCountRunnable) {
        this.userCountRunnable = userCountRunnable;
        return this;
    }

    public Countdown modCounter(String name, Iterable<Player> users) {
        this.name = name;
        this.modCounterUsers = users;
        return this;
    }

    public Countdown finish(Runnable finishRunnable) {
        this.finishRunnable = finishRunnable;
        return this;
    }

    public Countdown start() {
        if (isRunning()) {
            throw new IllegalStateException("Countdown already started");
        }
        createTimer(0, beat, currentCount + 1);
        return this;
    }

    public Countdown beat(int beat) {
        this.beat = beat;
        return this;
    }

    public Countdown setCurrentCount(int currentCount) {
        this.currentCount = currentCount;
        cancel();
        return this;
    }

    public int getCurrentCount() {
        return currentCount;
    }

    public Runnable getFinish() {
        return finishRunnable;
    }

    public static boolean isUserFriendlyNumber(int count) {
        return (count % 60 == 0 || (count % 15 == 0 && count < 60) || count == 10 || count < 6) && count != 0;
    }

    public static String getUserFriendlyNumber(int count) {
        LanguageManager lang = PvPDojo.LANG;
        if (count < 60) {
            return count + " " + lang.createDynamicKey(count == 1 ? MessageKeys.SECOND : MessageKeys.SECONDS);
        } else {
            return count / 60 + " " + lang.createDynamicKey(count / 60 == 1 ? MessageKeys.MINUTE : MessageKeys.MINUTES)
                    + (count % 60 != 0 ? " " + lang.createDynamicKey(MessageKeys.AND) + " " + count % 60 + " " + lang.createDynamicKey(MessageKeys.SECONDS) : "");
        }
    }

}
