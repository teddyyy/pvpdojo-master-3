/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util.bukkit;

import static com.pvpdojo.util.StringUtils.SHORT_CHAR;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Wool;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionType;

import com.pvpdojo.bukkit.util.NMSUtils;

public class ItemBuilder {

    public static final ItemBuilder GOLDEN_HEAD = new ItemBuilder(Material.GOLDEN_APPLE).name(CC.GOLD + "Golden Head");

    protected ItemStack stack;
    protected ItemMeta meta;

    public ItemBuilder(Material type) {
        stack = new ItemStack(type);
        meta = stack.getItemMeta();
    }

    public ItemBuilder(ItemStack stack) {
        this.stack = stack.clone();
        meta = stack.getItemMeta().clone();
    }

    public ItemBuilder() {
        this(Material.AIR);
    }


    public static class ItemEditor extends ItemBuilder {

        /**
         * This class is not building new items, but editing an existing
         * It is still REQUIRED to call {@link ItemBuilder#build()}
         */
        public ItemEditor(ItemStack stack) {
            this.stack = stack;
            this.meta = stack.getItemMeta();
        }
    }

    public ItemStack build() {
        stack.setItemMeta(meta);
        return stack;
    }

    public ItemBuilder enchant(Enchantment ench, int level) {
        meta.addEnchant(ench, level, true);
        return this;
    }

    public ItemBuilder addEnchantments(Map<Enchantment, Integer> enchantments) {
        for (Entry<Enchantment, Integer> entry : enchantments.entrySet()) {
            meta.addEnchant(entry.getKey(), entry.getValue(), true);
        }
        return this;
    }

    public ItemBuilder removeEnchantment(Enchantment ench) {
        meta.removeEnchant(ench);
        return this;
    }

    public ItemBuilder amount(int amount) {
        stack.setAmount(amount);
        return this;
    }

    public ItemBuilder durability(short durability) {
        stack.setDurability(durability);
        return this;
    }

    public ItemBuilder type(Material type) {
        stack.setType(type);
        return this;
    }

    @SuppressWarnings("deprecation")
    public ItemBuilder data(byte data) {
        stack.setData(new MaterialData(stack.getType(), data));
        return this;
    }

    public ItemBuilder name(String name) {
        meta.setDisplayName(name);
        return this;
    }

    public ItemBuilder lore(boolean append, String... lores) {
        List<String> finalLore = append && meta.hasLore() ? meta.getLore() : new ArrayList<>();
        for (String lore : lores) {
            if (lore != null) {
                finalLore.addAll(wrapLore(lore));
            }
        }
        meta.setLore(finalLore);
        return this;
    }

    public ItemBuilder lore(boolean append, Iterable<String> lores) {
        List<String> finalLore = append && meta.hasLore() ? meta.getLore() : new ArrayList<>();
        for (String lore : lores) {
            finalLore.addAll(wrapLore(lore));
        }
        meta.setLore(finalLore);
        return this;
    }

    public ItemBuilder lore(String... lores) {
        return lore(false, lores);
    }

    public ItemBuilder lore(Iterable<String> lores) {
        return lore(false, lores);
    }

    public ItemBuilder appendLore(String... lore) {
        return lore(true, lore);
    }

    public ItemBuilder appendLore(Iterable<String> lore) {
        return lore(true, lore);
    }

    public ItemBuilder unbreakable() {
        meta.spigot().setUnbreakable(true);
        return this;
    }

    public ItemBuilder hideAttributes() {
        flag(ItemFlag.HIDE_ATTRIBUTES);
        return this;
    }

    public ItemBuilder flag(ItemFlag flag) {
        meta.addItemFlags(flag);
        return this;
    }

    public ItemBuilder glow(boolean glow) {
        return glow ? enchant(NMSUtils.GLOW, 1) : removeEnchantment(NMSUtils.GLOW);
    }

    public static class WoolBuilder extends ItemBuilder {

        protected Wool wool;

        public WoolBuilder(DyeColor color) {
            super(Material.WOOL);
            wool = new Wool(color);
        }

        @SuppressWarnings("deprecation")
        @Override
        public ItemStack build() {
            durability(wool.getData());
            return super.build();
        }

    }

    public static class PotionBuilder extends ItemBuilder {

        protected Potion potion;
        protected int amount;

        public PotionBuilder(PotionType type) {
            amount = 1;
            potion = new Potion(type);
            meta = Bukkit.getItemFactory().getItemMeta(Material.POTION);
        }

        public PotionBuilder() {
            this(PotionType.WATER);
        }

        public PotionBuilder splash() {
            potion.splash();
            return this;
        }

        public PotionBuilder extend() {
            potion.extend();
            return this;
        }

        public PotionBuilder level(int level) {
            potion.setLevel(level);
            return this;
        }

        public PotionBuilder addEffect(PotionEffect potionEffect) {
            ((PotionMeta) meta).addCustomEffect(potionEffect, true);
            return this;
        }

        @Override
        public ItemBuilder amount(int amount) {
            this.amount = amount;
            return this;
        }

        @Override
        public ItemStack build() {
            stack = potion.toItemStack(amount);
            return super.build();
        }

    }

    public static class LeatherArmorBuilder extends ItemBuilder {

        public LeatherArmorBuilder(Material type, Color color) {
            super(type);
            if (!Bukkit.getItemFactory().isApplicable(Bukkit.getItemFactory().getItemMeta(Material.LEATHER_BOOTS), type)) {
                throw new IllegalArgumentException("The provided type is not applicable for a leatherarmor");
            }
            ((LeatherArmorMeta) meta).setColor(color);
        }
    }

    public static class SkullBuilder extends ItemBuilder {

        public SkullBuilder(String name) {
            super(Material.SKULL_ITEM);
            durability((short) 3);
            ((SkullMeta) meta).setOwner(name);
        }

    }

    public static class BookBuilder extends ItemBuilder {

        protected BookMeta bm;

        public BookBuilder() {
            super(Material.WRITTEN_BOOK);
            this.bm = (BookMeta) meta;
        }

        public BookBuilder pages(String... pages) {
            this.bm.setPages(new ArrayList<>());
            this.bm.addPage(pages);
            return this;
        }

        public BookBuilder author(String author) {
            this.bm.setAuthor(author);
            return this;
        }

        public BookBuilder title(String title) {
            this.bm.setTitle(title);
            bm.addPage("");
            return this;
        }

    }

    public static List<String> wrapLore(String text) {
        return wrapLore(text, 42);
    }

    public static List<String> wrapLore(String lore, int length) {
        List<String> output = new ArrayList<>();
        String[] tokens = lore.split(" ");
        String color;
        int line = 0;
        output.add("");

        for (String token : tokens) {

            if (CC.stripColor(SHORT_CHAR.matcher(output.get(line)).replaceAll("")).length() + CC.stripColor(SHORT_CHAR.matcher(token).replaceAll("")).length() + 1 <= length) {
                output.set(line, output.get(line) + token + " ");
            } else {
                color = CC.getLastColors(output.get(line));
                line++;
                output.add(color + token + " ");
            }
        }
        return output;
    }

}
