/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class Throttle {

    private final LimitedQueue<Long> timeStamps;
    private final Duration duration;

    public Throttle(int range, Duration duration) {
        this.timeStamps = new LimitedQueue<>(range);
        this.duration = duration;
    }

    public synchronized boolean isThrottled() {
        Long firstEntry = timeStamps.peek();
        return timeStamps.isFull() && firstEntry != null && !TimeUtil.hasPassed(firstEntry, TimeUnit.MILLISECONDS, duration.toMillis());
    }

    public synchronized boolean throttle() {
        boolean throttled = isThrottled();
        if (!throttled) {
            timeStamps.add(System.currentTimeMillis());
        }
        return throttled;
    }

}
