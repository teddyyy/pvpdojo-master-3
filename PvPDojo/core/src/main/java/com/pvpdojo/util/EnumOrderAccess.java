/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

public interface EnumOrderAccess<T extends Enum> {

    default T[] getValues() {
        return (T[]) getClass().getEnumConstants();
    }

    int ordinal();

    default T next() {
        int next = ordinal() + 1;
        return next >= getValues().length ? getValues()[0] : getValues()[next];
    }

    default T lower() {
        int next = ordinal() - 1;
        return next < 0 ? null : getValues()[next];
    }

}
