/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;

public class FileUtil {

    public static ArrayList<String> getFileLines(File file) {
        ArrayList<String> lines = new ArrayList<>();
        try {
            try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    lines.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

    public static String getAbsolutePath() {
        return new File("").getAbsolutePath();
    }

    public static ArrayList<String> getFileLines(String path) {
        File file = new File(path);
        return getFileLines(file);
    }

    public static File createFileIfMissing(File file) {
        if (file.exists()) {
            return file;
        } else
            try {
                file.createNewFile();
                return file;
            } catch (IOException e) {
                e.printStackTrace();
            }
        return null;
    }

    public static File createFileIfMissing(String path) {
        File file = new File(path);
        return createFileIfMissing(file);
    }

    public static void deleteFolder(String folder) {
        if (Files.exists(Paths.get(folder))) {
            try {
                Files.walk(Paths.get(folder)).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void writeFile(File file, String data) {
        try {
            try (DataOutputStream outstream = new DataOutputStream(new FileOutputStream(file, false))) {
                outstream.write(data.getBytes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void setupSelfDeleteOnShutdown() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                Runtime.getRuntime().exec(new String[] { "sh", new File("../deleteserver.sh").getPath(), new File(".").getName() });
            } catch (IOException e) {
                e.printStackTrace();
            }
            deleteFolder("");
        }));
    }

}
