/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.jetbrains.annotations.NotNull;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multiset;

public class HashBiMultiMap<K, V> implements BiMultiMap<K, V> {

    private final HashMultimap<K, V> delegate = HashMultimap.create();
    private final Map<V, K> inverse = new HashMap<>();

    public HashBiMultiMap() {}

    public static <K, V> HashBiMultiMap<K, V> create() {
        return new HashBiMultiMap<>();
    }

    @Override
    public int size() {
        return delegate.size();
    }

    @Override
    public boolean isEmpty() {
        return delegate.isEmpty();
    }

    @Override
    public boolean containsKey(Object paramObject) {
        return delegate.isEmpty();
    }

    @Override
    public boolean containsValue(Object paramObject) {
        return delegate.containsValue(paramObject);
    }

    @Override
    public boolean containsEntry(Object paramObject1, Object paramObject2) {
        return delegate.containsEntry(paramObject1, paramObject2);
    }

    @Override
    public boolean put(K paramK, V paramV) {
        if (inverse.containsKey(paramV) && inverse.get(paramV) != paramK) {
            throw new IllegalArgumentException("Value is already referenced");
        }
        inverse.put(paramV, paramK);
        return delegate.put(paramK, paramV);
    }

    @Override
    public boolean remove(Object paramObject1, Object paramObject2) {
        boolean removed = delegate.remove(paramObject1, paramObject2);
        if (removed) {
            inverse.remove(paramObject2);
        }
        return removed;
    }

    @Override
    public boolean putAll(K paramK, @NotNull Iterable<? extends V> paramIterable) {
        if (Iterables.any(paramIterable, item -> inverse.containsKey(item) && inverse.get(item) != paramK)) {
            throw new IllegalArgumentException("Value is already referenced");
        }
        boolean putall = delegate.putAll(paramK, paramIterable);
        if (putall) {
            paramIterable.forEach(item -> inverse.put(item, paramK));
        }
        return putall;
    }

    @Override
    public boolean putAll(Multimap<? extends K, ? extends V> paramMultimap) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Collection<V> replaceValues(K paramK, Iterable<? extends V> paramIterable) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Collection<V> removeAll(Object paramObject) {
        Collection<V> values = delegate.removeAll(paramObject);
        values.forEach(item -> inverse.remove(item));
        return values;
    }

    @Override
    public void clear() {
        delegate.clear();
        inverse.clear();
    }

    @Override
    public Collection<V> get(K paramK) {
        return delegate.get(paramK);
    }

    @Override
    public Set<K> keySet() {
        return delegate.keySet();
    }

    @Override
    public Multiset<K> keys() {
        return delegate.keys();
    }

    @Override
    public Collection<V> values() {
        return delegate.values();
    }

    @Override
    public Collection<Entry<K, V>> entries() {
        return delegate.entries();
    }

    @Override
    public Map<K, Collection<V>> asMap() {
        return delegate.asMap();
    }

    @Override
    public Map<V, K> inverse() {
        return inverse;
    }

}