/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

import static com.pvpdojo.util.StringUtils.getStringFromArray;

import java.util.Objects;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.userdata.User;

import co.aikar.locales.MessageKeyProvider;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.chat.ComponentSerializer;

public class BroadcastUtil {

    public static final String TRANSLATABLE = "translatable";
    public static final String SERIALIZED_BASE = "_BaseComponent_";

    public static void broadcastPerm(String perm, String... message) {
        DojoRunnable task;

        if (message.length > 0 && message[0].startsWith(SERIALIZED_BASE)) {
            BaseComponent[] components = ComponentSerializer.parse(message[0].replaceFirst(SERIALIZED_BASE, ""));
            task = PvPDojo.schedule(() -> Bukkit.getOnlinePlayers().stream()
                                                .filter(player -> player.hasPermission(perm))
                                                .map(User::getUser)
                                                .filter(User::isShowSpectators)
                                                .map(User::getPlayer)
                                                .forEach(player -> player.spigot().sendMessage(components)));
        } else {
            task = PvPDojo.schedule(() -> Bukkit.getOnlinePlayers().stream()
                                                .filter(player -> player.hasPermission(perm))
                                                .map(User::getUser)
                                                .filter(User::isShowSpectators)
                                                .map(User::getPlayer)
                                                .forEach(player -> player.sendMessage(message)));
        }
        task.createNonMainThreadTask();
    }

    public static void broadcast(String... message) {

        if (message.length > 0 && message[0].startsWith(SERIALIZED_BASE)) {
            BaseComponent[] components = ComponentSerializer.parse(message[0].replaceFirst(SERIALIZED_BASE, ""));
            PvPDojo.schedule(() -> Bukkit.getOnlinePlayers().forEach(player -> player.spigot().sendMessage(components))).createNonMainThreadTask();
        } else {
            PvPDojo.schedule(() -> {
                for (String msg : message) {
                    broadcast(MessageKeys.GENERIC, "{text}", msg);
                }
            }).createNonMainThreadTask();
        }
    }

    public static void broadcast(MessageKeyProvider key, String... replacements) {
        PvPDojo.schedule(() -> Bukkit.getOnlinePlayers().stream()
                                     .map(User::getUser)
                                     .filter(Objects::nonNull)
                                     .forEach(player -> player.sendMessage(key, replacements))
        ).createNonMainThreadTask();
    }

    public static void broadcastPerm(MessageKeyProvider key, String perm, String... replacements) {
        PvPDojo.schedule(() -> Bukkit.getOnlinePlayers().stream()
                                     .filter(player -> player.hasPermission(perm))
                                     .map(User::getUser)
                                     .filter(Objects::nonNull)
                                     .filter(User::isShowSpectators)
                                     .forEach(user -> user.sendMessage(key, replacements)))
               .createNonMainThreadTask();
    }

    public static void broadcastNearby(Location loc, String message, int distance) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.getWorld().equals(loc.getWorld())) {
                if (loc.distanceSquared(player.getLocation()) < (distance * distance)) {
                    player.sendMessage(message);
                }
            }
        }
    }

    public static void global(ChatChannel channel, String... strings) {
        PvPDojo.schedule(() -> {
            for (String msg : strings) {
                Redis.get().publish(BungeeSync.CHANNEL_CHAT, channel + "|" + "none" + "|" + StringUtils.toBase64(msg));
            }
        }).createNonMainThreadTask();
    }

    public static void global(ChatChannel channel, MessageKeys key, String... replacements) {
        PvPDojo.schedule(() -> {
            Redis.get().publish(BungeeSync.CHANNEL_CHAT, channel + "|" + TRANSLATABLE + "|" + key.name() + "|"
                    + getStringFromArray(replacements, StringUtils::toBase64, ":"));
        }).createNonMainThreadTask();
    }

    public static void global(ChatChannel channel, BaseComponent... baseComponents) {
        String serial = ComponentSerializer.toString(baseComponents);
        global(channel, SERIALIZED_BASE + serial);
    }

}
