/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

public class Holder<T> {

    private T item;

    public Holder() {}

    public Holder(T initialItem) {
        this.item = initialItem;
    }

    public T get() {
        return item;
    }

    public T getValue() {
        return item;
    }

    public void set(T item) {
        this.item = item;
    }

    public void setValue(T item) {
        this.item = item;
    }

    @Override
    public int hashCode() {
        return item.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Holder && ((Holder) obj).get().equals(item);
    }
}
