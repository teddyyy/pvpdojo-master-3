/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Preconditions;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder.FormatRetention;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

/**
 * <p>
 * CompBuilder simplifies creating basic messages by allowing the use of a
 * chainable builder.
 * </p>
 * <pre>
 * new CompBuilder("Hello ").color(ChatColor.RED).
 * append("World").color(ChatColor.BLUE). append("!").bold(true).create();
 * </pre>
 * <p>
 * All methods (excluding {@link #append(String)} and {@link #create()} work on
 * the last part appended to the builder, so in the example above "Hello " would
 * be {@link net.md_5.bungee.api.ChatColor#RED} and "World" would be
 * {@link net.md_5.bungee.api.ChatColor#BLUE} but "!" would be bold and
 * {@link net.md_5.bungee.api.ChatColor#BLUE} because append copies the previous
 * part's formatting
 * </p>
 */
public final class CompBuilder {

    private BaseComponent current;
    private final List<BaseComponent> parts = new ArrayList<>();

    /**
     * Creates a CompBuilder from the other given CompBuilder to clone
     * it.
     *
     * @param original the original for the new CompBuilder.
     */
    public CompBuilder(CompBuilder original) {
        current = original.current.duplicate();
        for (BaseComponent baseComponent : original.parts) {
            parts.add(baseComponent.duplicate());
        }
    }

    /**
     * Creates a CompBuilder with the given text as the first part.
     *
     * @param text the first text element
     */
    public CompBuilder(String text) {
        current = new TextComponent(text);
    }

    /**
     * Creates a CompBuilder with the given component as the first part.
     *
     * @param component the first component element
     */
    public CompBuilder(BaseComponent component) {
        current = component.duplicate();
    }

    /**
     * Appends a component to the builder and makes it the current target for
     * formatting. The component will have all the formatting from previous
     * part.
     *
     * @param component the component to append
     * @return this CompBuilder for chaining
     */
    public CompBuilder append(BaseComponent component) {
        return append(component, FormatRetention.ALL);
    }

    /**
     * Appends a component to the builder and makes it the current target for
     * formatting. You can specify the amount of formatting retained from
     * previous part.
     *
     * @param component the component to append
     * @param retention the formatting to retain
     * @return this CompBuilder for chaining
     */
    public CompBuilder append(BaseComponent component, FormatRetention retention) {
        parts.add(current);

        BaseComponent previous = current;
        current = component.duplicate();
        current.copyFormatting(previous, retention, false);
        return this;
    }

    /**
     * Appends the components to the builder and makes the last element the
     * current target for formatting. The components will have all the
     * formatting from previous part.
     *
     * @param components the components to append
     * @return this CompBuilder for chaining
     */
    public CompBuilder append(BaseComponent[] components) {
        return append(components, FormatRetention.ALL);
    }

    /**
     * Appends the components to the builder and makes the last element the
     * current target for formatting. You can specify the amount of formatting
     * retained from previous part.
     *
     * @param components the components to append
     * @param retention  the formatting to retain
     * @return this CompBuilder for chaining
     */
    public CompBuilder append(BaseComponent[] components, FormatRetention retention) {
        Preconditions.checkArgument(components.length != 0, "No components to append");

        BaseComponent previous = current;
        for (BaseComponent component : components) {
            parts.add(current);

            current = component.duplicate();
            current.copyFormatting(previous, retention, false);
        }

        return this;
    }

    /**
     * Appends the text to the builder and makes it the current target for
     * formatting. The text will have all the formatting from previous part.
     *
     * @param text the text to append
     * @return this CompBuilder for chaining
     */
    public CompBuilder append(String text) {
        return append(text, FormatRetention.ALL);
    }

    /**
     * Appends the text to the builder and makes it the current target for
     * formatting. You can specify the amount of formatting retained from
     * previous part.
     *
     * @param text      the text to append
     * @param retention the formatting to retain
     * @return this CompBuilder for chaining
     */
    public CompBuilder append(String text, FormatRetention retention) {
        parts.add(current);

        BaseComponent old = current;
        current = new TextComponent(text);
        current.copyFormatting(old, retention, false);

        return this;
    }

    /**
     * Allows joining additional components to this builder using the given
     * {@link Joiner} and {@link FormatRetention#ALL}.
     * <p>
     * Simply executes the provided joiner on this instance to facilitate a
     * chain pattern.
     *
     * @param joiner joiner used for operation
     * @return this CompBuilder for chaining
     */
    public CompBuilder append(Joiner joiner) {
        return joiner.join(this, FormatRetention.ALL);
    }

    /**
     * Allows joining additional components to this builder using the given
     * {@link Joiner}.
     * <p>
     * Simply executes the provided joiner on this instance to facilitate a
     * chain pattern.
     *
     * @param joiner    joiner used for operation
     * @param retention the formatting to retain
     * @return this CompBuilder for chaining
     */
    public CompBuilder append(Joiner joiner, FormatRetention retention) {
        return joiner.join(this, retention);
    }

    /**
     * Sets the color of the current part.
     *
     * @param color the new color
     * @return this CompBuilder for chaining
     */
    public CompBuilder color(ChatColor color) {
        current.setColor(color);
        return this;
    }

    /**
     * Sets whether the current part is bold.
     *
     * @param bold whether this part is bold
     * @return this CompBuilder for chaining
     */
    public CompBuilder bold(boolean bold) {
        current.setBold(bold);
        return this;
    }

    /**
     * Sets whether the current part is italic.
     *
     * @param italic whether this part is italic
     * @return this CompBuilder for chaining
     */
    public CompBuilder italic(boolean italic) {
        current.setItalic(italic);
        return this;
    }

    /**
     * Sets whether the current part is underlined.
     *
     * @param underlined whether this part is underlined
     * @return this CompBuilder for chaining
     */
    public CompBuilder underlined(boolean underlined) {
        current.setUnderlined(underlined);
        return this;
    }

    /**
     * Sets whether the current part is strikethrough.
     *
     * @param strikethrough whether this part is strikethrough
     * @return this CompBuilder for chaining
     */
    public CompBuilder strikethrough(boolean strikethrough) {
        current.setStrikethrough(strikethrough);
        return this;
    }

    /**
     * Sets whether the current part is obfuscated.
     *
     * @param obfuscated whether this part is obfuscated
     * @return this CompBuilder for chaining
     */
    public CompBuilder obfuscated(boolean obfuscated) {
        current.setObfuscated(obfuscated);
        return this;
    }

    /**
     * Sets the insertion text for the current part.
     *
     * @param insertion the insertion text
     * @return this CompBuilder for chaining
     */
    public CompBuilder insertion(String insertion) {
        current.setInsertion(insertion);
        return this;
    }

    /**
     * Sets the click event for the current part.
     *
     * @param clickEvent the click event
     * @return this CompBuilder for chaining
     */
    public CompBuilder event(ClickEvent clickEvent) {
        current.setClickEvent(clickEvent);
        return this;
    }

    public CompBuilder command(String command) {
        ClickEvent event = new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/" + command);
        for (BaseComponent component : parts) {
            component.setClickEvent(event);
        }
        return event(event);
    }

    /**
     * Sets the hover event for the current part.
     *
     * @param hoverEvent the hover event
     * @return this CompBuilder for chaining
     */
    public CompBuilder event(HoverEvent hoverEvent) {
        current.setHoverEvent(hoverEvent);
        return this;
    }

    public CompBuilder hoverMessage(String hoverMessage) {
        return event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponent[]{new TextComponent(hoverMessage)}));
    }

    /**
     * Sets the current part back to normal settings. Only text is kept.
     *
     * @return this CompBuilder for chaining
     */
    public CompBuilder reset() {
        return retain(FormatRetention.NONE);
    }

    /**
     * Retains only the specified formatting. Text is not modified.
     *
     * @param retention the formatting to retain
     * @return this CompBuilder for chaining
     */
    public CompBuilder retain(FormatRetention retention) {
        current.retain(retention);
        return this;
    }

    /**
     * Returns the components needed to display the message created by this
     * builder.
     *
     * @return the created components
     */
    public BaseComponent[] create() {
        BaseComponent[] result = parts.toArray(new BaseComponent[parts.size() + 1]);
        result[parts.size()] = current;
        return result;
    }


    /**
     * Functional interface to join additional components to a CompBuilder.
     */
    public interface Joiner {

        /**
         * Joins additional components to the provided {@link CompBuilder}
         * and then returns it to fulfill a chain pattern.
         * <p>
         * Retention may be ignored and is to be understood as an optional
         * recommendation to the Joiner and not as a guarantee to have a
         * previous component in builder unmodified.
         *
         * @param componentBuilder to which to append additional components
         * @param retention        the formatting to possibly retain
         * @return input componentBuilder for chaining
         */
        CompBuilder join(CompBuilder componentBuilder, FormatRetention retention);
    }
}