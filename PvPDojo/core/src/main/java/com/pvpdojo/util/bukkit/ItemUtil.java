/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util.bukkit;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.util.DojoRunnable;
import com.pvpdojo.util.Holder;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import static org.bukkit.Material.WATER;

public class ItemUtil {

    public static final EnumSet<Material> SWORDS = EnumSet.of(Material.WOOD_SWORD, Material.STONE_SWORD, Material.GOLD_SWORD, Material.IRON_SWORD, Material.DIAMOND_SWORD);
    public static final EnumSet<Material> PICKAXES = EnumSet.of(Material.WOOD_PICKAXE, Material.STONE_PICKAXE, Material.GOLD_PICKAXE, Material.IRON_PICKAXE, Material.DIAMOND_PICKAXE);
    public static final EnumSet<Material> LIQUID = EnumSet.of(Material.LAVA, Material.STATIONARY_LAVA, WATER, Material.STATIONARY_WATER);
    public static final EnumSet<Material> TOOLS = EnumSet.of(
            Material.WOOD_SWORD, Material.WOOD_PICKAXE, Material.WOOD_AXE, Material.WOOD_SPADE, Material.WOOD_HOE,
            Material.STONE_SWORD, Material.STONE_PICKAXE, Material.STONE_AXE, Material.STONE_SPADE, Material.STONE_HOE,
            Material.GOLD_SWORD, Material.GOLD_PICKAXE, Material.GOLD_AXE, Material.GOLD_SPADE, Material.GOLD_HOE,
            Material.IRON_SWORD, Material.IRON_PICKAXE, Material.IRON_AXE, Material.IRON_SPADE, Material.IRON_HOE,
            Material.DIAMOND_SWORD, Material.DIAMOND_PICKAXE, Material.DIAMOND_AXE, Material.DIAMOND_SPADE, Material.DIAMOND_HOE
    );

    public static final EnumSet<Material> CLICK_ITEM_POOL = EnumSet.noneOf(Material.class);

    static {
        for (Material material : Material.values()) {
            if (material.isSolid() || material.isEdible() || material.isRecord() || material == Material.SAPLING || material == Material.TORCH || material == Material.REDSTONE_TORCH_ON
                    || material == Material.WOOD_BUTTON || material == Material.STONE_BUTTON || material == Material.VINE || material == Material.TRIPWIRE_HOOK || material == Material.SNOW_BALL
                    || material == Material.LEATHER || material == Material.BRICK || material == Material.CLAY_BALL || material == Material.PAPER
                    || material == Material.WATCH || material == Material.BONE || material == Material.GHAST_TEAR || material == Material.EMERALD || material == Material.NETHER_BRICK
                    || material == Material.QUARTZ || material == Material.FIREWORK || material == Material.RED_ROSE || material == Material.MINECART
                    || material == Material.FEATHER || material == Material.DIAMOND || material == Material.SUGAR || material == Material.SADDLE || material == Material.FERMENTED_SPIDER_EYE
                    || material == Material.BLAZE_POWDER || material == Material.EYE_OF_ENDER || material == Material.ARROW || material == Material.NETHER_STALK || material == Material.WOOL
                    || material == Material.FIREWORK_CHARGE || material == Material.SKULL_ITEM || material == Material.NETHER_STAR || material == Material.LAVA_BUCKET
                    || material == Material.ENDER_PEARL || material == Material.FIREBALL || material == Material.LEASH || material == Material.INK_SACK || material == Material.MONSTER_EGG
                    || material == Material.STAINED_CLAY || material == Material.REDSTONE || material == Material.MAGMA_CREAM || material == Material.SNOW || material == Material.SULPHUR
                    || material == Material.FLINT_AND_STEEL || material == Material.LAVA || material == Material.WATER || material == Material.PACKED_ICE || material == Material.FIRE || material == Material.FLINT
                    || material == Material.STICK || material == Material.STRING
                    || material.name().contains("_AXE")
                    || material.name().contains("_PICKAXE") || material.name().contains("_SPADE") || material.name().contains("_HOE")) {
                CLICK_ITEM_POOL.add(material);
            }
        }
        CLICK_ITEM_POOL.remove(Material.GOLDEN_APPLE);
        CLICK_ITEM_POOL.remove(Material.MUSHROOM_SOUP);
        CLICK_ITEM_POOL.remove(Material.BED_BLOCK);
        CLICK_ITEM_POOL.remove(Material.PISTON_EXTENSION);
        CLICK_ITEM_POOL.remove(Material.PISTON_MOVING_PIECE);
        CLICK_ITEM_POOL.remove(Material.SIGN_POST);
        CLICK_ITEM_POOL.remove(Material.WOODEN_DOOR);
        CLICK_ITEM_POOL.remove(Material.WALL_SIGN);
        CLICK_ITEM_POOL.remove(Material.IRON_DOOR_BLOCK);
        CLICK_ITEM_POOL.remove(Material.GLOWING_REDSTONE_ORE);
        CLICK_ITEM_POOL.remove(Material.CAKE_BLOCK);
        CLICK_ITEM_POOL.remove(Material.BREWING_STAND);
        CLICK_ITEM_POOL.remove(Material.CAULDRON);
        CLICK_ITEM_POOL.remove(Material.REDSTONE_LAMP_ON);
    }

    public static boolean isLiquid(Material material) {
        return LIQUID.contains(material);
    }

    public static boolean isWater(Material material) {
        return material == Material.WATER || material == Material.STATIONARY_WATER;
    }

    public static ItemStack[] cloneContents(ItemStack[] items) {
        return Arrays.stream(items).map(itemStack -> {
            if (itemStack != null) return itemStack.clone();
            return null;
        }).toArray(ItemStack[]::new);
    }

    public static Item throwItem(Entity entity, ItemStack itemToDrop, boolean canPickup, boolean throwFar) {
        Item item = entity.getWorld().dropItem(entity.getLocation().add(0.0D, 1.5D, 0.0D), itemToDrop);

        if (!canPickup) {
            item.setPickupDelay(Integer.MAX_VALUE);
        }

        if (throwFar) {
            item.setVelocity(entity.getLocation().getDirection().multiply(0.9D));
        }

        return item;
    }

    public static void addItemWhenFree(Player player, ItemStack... item) {

        if (item.length == 0) {
            return;
        }

        Holder<DojoRunnable> runnableHolder = new Holder<>();
        List<ItemStack> toAdd = new ArrayList<>();

        runnableHolder.set(PvPDojo.schedule(() -> {
            if (toAdd.isEmpty()) {
                toAdd.addAll(player.getInventory().addItem(item).values());
            } else {
                ItemStack[] addNow = toAdd.toArray(new ItemStack[0]);
                toAdd.clear();
                toAdd.addAll(player.getInventory().addItem(addNow).values());
            }
            if (toAdd.isEmpty()) {
                runnableHolder.get().cancel();
            }
        }));
        runnableHolder.get().createTimer(1, -1);
    }

}
