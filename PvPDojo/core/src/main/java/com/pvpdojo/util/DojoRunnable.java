/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

import co.aikar.taskchain.AsyncQueue;
import com.pvpdojo.PvPDojo;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class DojoRunnable {

    public static final ThreadPoolExecutor EXECUTOR = new ThreadPoolExecutor(0, 50 * Runtime.getRuntime().availableProcessors(),
            180, TimeUnit.SECONDS, new SynchronousQueue<>());

    static {
        EXECUTOR.setRejectedExecutionHandler((run, threadPoolExecutor) -> run.run());
    }

    private BukkitTask run;
    private int run_cycles;
    protected Runnable runnable;

    protected DojoRunnable() {
    }

    public DojoRunnable(Runnable run) {
        this.runnable = run;
    }

    public int createTimer(int delay, final int cycles) {
        return createTimer(0, delay, cycles);
    }

    public int createTimer(int initalDelay, int delay, final int cycles) {
        run_cycles = 0;

        if (cycles == -1) {
            run = Bukkit.getScheduler().runTaskTimer(PvPDojo.get(), runnable, initalDelay, delay);
        } else {
            run = new BukkitRunnable() {
                @Override
                public void run() {
                    if (cycles > 0) {
                        run_cycles++;
                        if (cycles < run_cycles) {
                            cancel();
                            return;
                        }
                    }
                    if (runnable != null) {
                        runnable.run();
                    }
                }
            }.runTaskTimer(PvPDojo.get(), initalDelay, delay);
        }
        return run.getTaskId();
    }

    public int createTimerAsync(int delay, final int cycles) {
        run_cycles = 0;

        run = new BukkitRunnable() {
            @Override
            public void run() {
                if (cycles > 0) {
                    run_cycles++;
                    if (cycles < run_cycles) {
                        cancel();
                        return;
                    }
                }
                if (runnable != null) {
                    runnable.run();
                }
            }
        }.runTaskTimerAsynchronously(PvPDojo.get(), 0, delay);
        return run.getTaskId();
    }

    public int nextTick() {
        return createTask(0);
    }

    public int sync() {
        if (Bukkit.isPrimaryThread()) {
            now();
            return -1;
        }
        return nextTick();
    }

    public int createTask(int delay) {
        if (!PvPDojo.get().isEnabled()) {
            now();
            return 0;
        }
        run = Bukkit.getScheduler().runTaskTimer(PvPDojo.get(), runnable, delay, -1);
        return run.getTaskId();
    }

    public int createTaskAsync(int delay) {
        if (!PvPDojo.get().isEnabled()) {
            now();
            return 0;
        }
        run = Bukkit.getScheduler().runTaskTimerAsynchronously(PvPDojo.get(), runnable, delay, -1);
        return run.getTaskId();
    }

    public void createAsyncTask() {
        if (!PvPDojo.get().isEnabled()) {
            now();
        } else {
            EXECUTOR.execute(runnable);
        }
    }

    public void createNonMainThreadTask() {
        if (Bukkit.isPrimaryThread()) {
            createAsyncTask();
        } else {
            now();
        }
    }

    public void now() {
        try {
            runnable.run();
        } catch (Throwable throwable) {
            Log.exception("DojoRunnable", throwable);
        }
    }

    public boolean isRunning() {
        return run != null && Bukkit.getScheduler().isCurrentlyRunning(run.getTaskId());
    }

    public void cancel() {
        if (run != null) {
            run.cancel();
        }
    }

    public static class DojoAsyncQueue implements AsyncQueue {

        @Override
        public void postAsync(Runnable runnable) {
            EXECUTOR.execute(runnable);
        }

        @Override
        public void shutdown(int timeout, TimeUnit unit) {
            // Handled by onDisable
        }
    }

}
