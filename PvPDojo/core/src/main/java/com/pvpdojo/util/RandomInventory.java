/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Function;

import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;

import lombok.Data;

public class RandomInventory<T> {

    private List<RandomItem> randomItems = new ArrayList<>();
    private Function<RandomItem, T> function;

    public static RandomInventory<ItemStack> randomItemStacks() {
        return new RandomInventory<>(item -> {
            ItemStack stack = new ItemStack(item.getItem());
            stack.setAmount(PvPDojo.RANDOM.nextInt(item.getMaxAmount() - item.getMinAmount() + 1) + item.getMinAmount());
            return stack;
        });
    }

    public RandomInventory(Function<RandomInventory<T>.RandomItem, T> function) {
        this.function = function;
    }

    public T generateRandomItem() {
        if (randomItems.isEmpty()) {
            throw new NoSuchElementException("Inventory is empty");
        }

        int randomIndex = PvPDojo.RANDOM.nextInt(randomItems.stream().mapToInt(RandomItem::getProbability).sum()) + 1;

        int combinedProbabilities = 0;
        for (RandomItem item : randomItems) {
            combinedProbabilities += item.getProbability();
            if (randomIndex <= combinedProbabilities) {
                return function.apply(item);
            }
        }

        throw new IllegalStateException();
    }

    public RandomInventory<T> addItem(T item, int probability) {
        randomItems.add(new RandomItem(item, probability, 1, 1));
        return this;
    }

    public RandomInventory<T> addItem(T item, int probability, int amount) {
        randomItems.add(new RandomItem(item, probability, amount, amount));
        return this;
    }

    public RandomInventory<T> addItem(T item, int probability, int minAmount, int maxAmount) {
        randomItems.add(new RandomItem(item, probability, minAmount, maxAmount));
        return this;
    }

    @Data
    public class RandomItem {

        private final T item;
        private final int probability;
        private final int minAmount;
        private final int maxAmount;

    }

}
