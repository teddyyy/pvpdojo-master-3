/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

import java.util.Map;

import com.google.common.collect.Multimap;

public interface BiMultiMap<K, V> extends Multimap<K, V> {
    Map<V, K> inverse();
}