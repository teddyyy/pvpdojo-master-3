/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

import java.util.ArrayList;

public class MathUtil {

    public static boolean isFloat(String number) {
        try {
            Float.parseFloat(number);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isInteger(String number) {
        try {
            Integer.parseInt(number);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isBoolean(String bool) {
        return bool.equalsIgnoreCase("TRUE") || bool.equalsIgnoreCase("FALSE");
    }

    public static float compileFloatArray(ArrayList<Float> list) {
        float output = 0.0f;
        for (Float f : list) {
            output += f;
        }
        return output;
    }
}
