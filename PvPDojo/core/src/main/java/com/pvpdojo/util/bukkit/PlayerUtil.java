/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util.bukkit;

import static com.pvpdojo.util.StringUtils.appendAll;

import java.util.Collection;
import java.util.Iterator;
import java.util.UUID;
import java.util.function.Consumer;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionType;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.settings.ServerSwitchSettings;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.ItemBuilder.PotionBuilder;

import net.minecraft.server.v1_7_R4.EntityLiving;
import net.minecraft.server.v1_7_R4.MobEffectList;

public class PlayerUtil {

    public static boolean isCrit(LivingEntity living) {
        EntityLiving l = NMSUtils.get(living);
        return (l.fallDistance > 0.0F) && (!l.onGround) && (!l.h_()) && (!l.M()) && (!l.hasEffect(MobEffectList.BLINDNESS)) && (l.vehicle == null);
    }

    public static boolean isRealHit(LivingEntity target) {
        return target.getNoDamageTicks() <= target.getMaximumNoDamageTicks() / 2;
    }

    public static void playDeathEffect(Entity entity) {
        NMSUtils.playEffectPacket(entity, 3);
    }

    public static void sendToServerKick(Player player, ServerType type) {
        sendToServer(player, type.name().toLowerCase());
        PvPDojo.schedule(() -> {
            if (player != null && player.isOnline()) {
                player.kickPlayer(CC.RED + "Took too long to switch server, try rejoining");
            }
        }).createTask(40);
    }

    public static void sendToServer(Player player, String serverName) {
        sendToServer(player, serverName, new ServerSwitchSettings(User.getUser(player)));
    }

    public static void sendToServer(Player player, ServerType type, ServerSwitchSettings settings) {
        sendToServer(player, type.name().toLowerCase(), settings);
    }

    public static void sendToServer(Player p, String serverName, ServerSwitchSettings settings) {
        PvPDojo.schedule(() -> {

            if (p.isOnline()) {
                settings.save(p.getUniqueId());
            }
            Redis.get().publish(BungeeSync.CONNECT, appendAll("|", "" + (p.isOnline() ? BungeeSync.PROXY_REQUEST : BungeeSync.SERVER_REQUEST), p.getUniqueId() + "", serverName));
        }).createNonMainThreadTask();
    }

    public static void sendToServer(UUID uuid, String serverName) {
        PvPDojo.schedule(() -> Redis.get().publish(BungeeSync.CONNECT, BungeeSync.SERVER_REQUEST + "|" + uuid + "|" + serverName)).createNonMainThreadTask();
    }

    public static void sendHere(UUID uuid) {
        sendToServer(uuid, PvPDojo.get().getNetworkPointer());
    }

    public static void refillHealPotions(Player player, int amount) {
        for (int i = 0; i < amount; i++) {
            player.getInventory().addItem(new ItemStack(Material.POTION, 1, (short) 16421));
        }
    }

    public static void refillSpeedPotions(Player player, int i) {
        player.getInventory().setItem(++i, new ItemStack(Material.ENDER_PEARL, 8));
        player.getInventory().setItem(++i, new PotionBuilder(PotionType.SPEED).level(2).build());
        player.getInventory().setItem(17, new PotionBuilder(PotionType.SPEED).level(2).build());
        player.getInventory().setItem(26, new PotionBuilder(PotionType.SPEED).level(2).build());
        player.getInventory().setItem(35, new PotionBuilder(PotionType.SPEED).level(2).build());
    }

    public static void refillSoup(Player player, int amount) {
        for (int i = 0; i < amount; i++) {
            player.getInventory().addItem(new ItemStack(Material.MUSHROOM_SOUP));
        }
    }

    public static void refillRecraft(Player player, int amount) {
        player.getInventory().setItem(14, new ItemBuilder(Material.RED_MUSHROOM).amount(amount).build());
        player.getInventory().setItem(13, new ItemBuilder(Material.BOWL).amount(amount).build());
        player.getInventory().setItem(15, new ItemBuilder(Material.BROWN_MUSHROOM).amount(amount).build());
    }

    public static void refillPotterRecraft(Player player, int amount) {
        player.getInventory().setItem(13, new ItemBuilder(Material.GLASS_BOTTLE).amount(amount).build());
        player.getInventory().setItem(14, new ItemBuilder(Material.SPECKLED_MELON).amount(amount).build());
    }

    public static void addPotterRecraft(Player player, int amount) {
        player.getInventory().addItem(new ItemBuilder(Material.GLASS_BOTTLE).amount(amount).build());
        player.getInventory().addItem(new ItemBuilder(Material.SPECKLED_MELON).amount(amount).build());
    }

    public static void addSpeedPotions(Player player, int amount) {
        if (!player.getInventory().containsAtLeast(new PotionBuilder(PotionType.SPEED).level(2).build(), amount)) {
            if (player.getInventory().addItem(new PotionBuilder(PotionType.SPEED).level(2).build()).isEmpty()) {
                addSpeedPotions(player, amount);
            }
        }
    }

    public static void addEnderPearls(Player player, int amount) {
        if (!player.getInventory().containsAtLeast(new ItemStack(Material.ENDER_PEARL), amount)) {
            player.getInventory().addItem(new ItemStack(Material.ENDER_PEARL));
            addEnderPearls(player, amount);
        }
    }

    public static void addRecraft(Player player, int amount) {
        Inventory inv = player.getInventory();
        inv.addItem(new ItemBuilder(Material.RED_MUSHROOM).amount(amount).build());
        inv.addItem(new ItemBuilder(Material.BOWL).amount(amount).build());
        inv.addItem(new ItemBuilder(Material.BROWN_MUSHROOM).amount(amount).build());
    }

    public static void resetPlayer(Player player) {
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        User user = User.getUser(player);
        user.setAdminMode(false);
        NMSUtils.get(player).getDataWatcher().watch(9, (byte) 0);
        for (PotionEffect effects : player.getActivePotionEffects()) {
            player.removePotionEffect(effects.getType());
        }
        player.getActivePotionEffects().clear();
        player.setWalkSpeed(.2f);
        player.setFireTicks(0);
        player.setFoodLevel(20);
        player.setFallDistance(0);
        player.setMaxHealth(20d);
        player.setHealth(20d);
        if (player.getGameMode().equals(GameMode.CREATIVE)) {
            player.setGameMode(GameMode.SURVIVAL);
        }
    }

    public static void removeVehicleAndPassenger(Player player) {
        if (player.getVehicle() != null) {
            player.getVehicle().eject();
        }
        player.eject();
    }

    public static <T> void spreadPlayerTask(Collection<T> players, Consumer<T> consumer, int initialPlayers, int playersPerTick, int maxTicks) {
        Iterator<T> itr = players.iterator();

        // Enter spread technique
        if (players.size() > initialPlayers) {
            int perTick = playersPerTick;
            int cycles = (int) Math.ceil(players.size() / (double) perTick);

            if (cycles > maxTicks) {
                cycles = maxTicks;
                perTick = (int) Math.ceil(players.size() / (double) maxTicks);
            }

            int finalPerTick = perTick;

            if (cycles > 0) {
                PvPDojo.schedule(() -> {
                    for (int i = 0; i < finalPerTick; i++) {
                        if (itr.hasNext()) {
                            consumer.accept(itr.next());
                        }
                    }
                }).createTimer(0, cycles);
            }
            return;
        }

        // Just do it now
        while (itr.hasNext()) {
            consumer.accept(itr.next());
        }
    }

    /**
     * https://en.wikipedia.org/wiki/Dyscalculia
     *
     * @param player
     * @return if player has dyscalculia
     */
    public static boolean hasDyscalculia(Player player) {
        return hasAutism(player);
    }

    /**
     * https://en.wikipedia.org/wiki/Autism
     *
     * @param player
     * @return if player has autism
     */
    public static boolean hasAutism(Player player) {
        return player.getUniqueId().equals(UUID.fromString("a8ef4b67-76cf-4231-88ab-63431ff3dcea")) || player.getUniqueId().equals(UUID.fromString("9620c50a-5e99-4a44-a930-8ac2e0cf8079"))
                || player.getUniqueId().equals(UUID.fromString("0f926af9-9195-4a83-9029-385403e568d0"));
    }

    public static boolean hasSoltismus(Player player) {
        return player.getUniqueId().equals(UUID.fromString("b67d720a-563c-4e9f-82e3-c6c3961862f6")) || player.getUniqueId().equals(UUID.fromString("a9e41b22-b4b8-4fd1-8a02-6a7e73125544"));
    }

    public static boolean hasRank(CommandSender sender, Rank rank) {
        return hasRank(sender, rank, true);
    }

    public static boolean hasRank(CommandSender sender, Rank rank, boolean sendMessage) {
        if (!(sender instanceof Player)) {
            return true;
        }
        boolean hasPermission = User.getUser((Player) sender).getRank().inheritsRank(rank);
        if (!hasPermission && sendMessage) {
            sender.sendMessage(PvPDojo.WARNING + "Insufficient permissions");
        }
        return hasPermission;
    }

}