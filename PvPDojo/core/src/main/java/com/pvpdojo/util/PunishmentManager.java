/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util;

import static com.pvpdojo.mysql.Tables.PUNISHMENT;
import static com.pvpdojo.util.StringUtils.getLS;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.mysql.SQL;
import com.pvpdojo.mysql.SQLBuilder;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.PlayerUtil;

public class PunishmentManager {

    public static final int SALT = 42;

    public static void punish(CommandSender sender, String name, String reason, long time, PunishmentType type) {
        punish(sender, name, reason, time, type, false);
    }

    public static void punish(CommandSender sender, String name, String reason, long time, PunishmentType type, boolean silent) {
        DojoOfflinePlayer offlinePlayer = PvPDojo.getOfflinePlayer(name);

        // Handle our semi permanent punishments
        boolean softBan = type == PunishmentType.SOFT_BAN;
        if (softBan) {
            type = PunishmentType.BAN;
        }

        if ((type == PunishmentType.BAN && offlinePlayer.isBanned()) || (type == PunishmentType.MUTE && offlinePlayer.isMuted())) {
            sender.sendMessage(PvPDojo.WARNING + offlinePlayer.getName() + " is already punished.");
            return;
        }

        if (!offlinePlayer.hasPlayedBefore()) {
            sender.sendMessage(PvPDojo.WARNING + "User does not exist in the database. Trying to ban them anyway...");
        }

        if (offlinePlayer.getName() == null) {
            sender.sendMessage(PvPDojo.WARNING + "Even mojang could not recognize this user. No action will be taken.");
            return;
        }

        offlinePlayer.getPersistentData().pullUserDataSneaky();

        if (!PlayerUtil.hasRank(sender, offlinePlayer.getPersistentData().getRank().lower())) {
            return;
        }

        UUID uuid = offlinePlayer.getUniqueId();
        String executor = sender.getName();

        try {

            if (type == PunishmentType.MUTE) {
                time = calculateMuteTime(offlinePlayer, reason, time);
            } else if (softBan && PunishmentManager.getPunishments(uuid).stream()
                                                   .filter(pun -> pun.getType() == PunishmentType.BAN)
                                                   .filter(pun -> pun.getOriginalLength() == -1 || pun.getOriginalLength() == Duration.ofDays(30).getSeconds())
                                                   .noneMatch(Punishment::isValid)) {
                // Reduce to 30 days if softban meets first permanent punishment condition
                time = Duration.ofDays(30).getSeconds();
            }

            int id = PunishmentManager.addPunishment(uuid, executor, reason, time, type);

            String remaining = time == -1 ? StringUtils.getTimeLeft(time) : StringUtils.getTimeLeft(System.currentTimeMillis() + time * 1000);
            ChatChannel ch = silent ? ChatChannel.STAFF : ChatChannel.GLOBAL;
            switch (type) {
                case BAN:
                    MessageKeys key = MessageKeys.SERVER_BAN;
                    if (reason.startsWith("[AntiSkid]")) {
                        key = MessageKeys.SERVER_BAN_EYEHAWK;
                    }
                    BroadcastUtil.global(ch, CC.GRAY + getLS());
                    BroadcastUtil.global(ch, key, "{target}", offlinePlayer.getName(), "{reason}", reason, "{period}", remaining);
                    BroadcastUtil.global(ch, CC.GRAY + getLS());

                    String banscreen = "" + CC.RED + CC.STRIKETHROUGH + "-------------\n" + CC.RED + "You have been banned from the server.\n\nReason: " + reason + "\nRemaining time: "
                            + remaining + "\n\n" + CC.YELLOW + "Appeal on pvpdojo.com/discord\nSend this message to DojoBot '" + CC.RED + "!appeal " + Integer.toHexString(id)
                            + CC.YELLOW + "'" + "\n" + CC.RED + CC.STRIKETHROUGH + "-------------";
                    Redis.get().publish(BungeeSync.KICK, uuid.toString() + "|" + StringUtils.toBase64(banscreen));
                    break;
                case MUTE:
                    BroadcastUtil.global(ch, MessageKeys.SERVER_MUTE, "{target}", offlinePlayer.getName(), "{reason}", reason, "{period}", remaining);
                    break;
                default:
                    break;
            }
        } catch (Throwable t) {
            sender.sendMessage(PvPDojo.WARNING + CC.RED + "An error occured while banning player, report this asap");
            Log.exception("Ban Player", t);
        }


    }

    public static long calculateMuteTime(DojoOfflinePlayer target, String reason, long time) {
        long factor = 1 + getPunishments(target.getUniqueId()).stream().filter(Punishment::isValid)
                                                              .filter(pun -> pun.getType() == PunishmentType.MUTE && pun.getOriginalReason().equalsIgnoreCase(reason))
                                                              .count();
        return time * factor * factor;
    }

    public static int addPunishment(UUID uuid, String executor, String reason, long length, PunishmentType type) throws SQLException {
        if (getActivePunishment(uuid, type) != null) {
            throw new IllegalStateException("Player already punished");
        }

        try (SQLBuilder builder = new SQLBuilder()) {
            builder.insert(Tables.PUNISHMENT, "punished, executor, reason, type, date, length")
                   .values("?, ?, ?, ?, NOW(), ?")
                   .createStatement(Statement.RETURN_GENERATED_KEYS)
                   .executeUpdate(uuid.toString(), getUUIDExecutor(executor), reason, type.ordinal(), length);

            builder.getPreparedStatement().getGeneratedKeys().next();
            ResultSet key = builder.getPreparedStatement().getGeneratedKeys();
            key.next();
            return salt(key.getInt(1));
        }
    }

    public static Punishment getActivePunishment(UUID uuid, PunishmentType type) throws SQLException {
        if (type == PunishmentType.KICK) {
            return null;
        }
        try (SQLBuilder sql = new SQLBuilder()) {
            sql.select(Tables.PUNISHMENT, "*")
               .where("valid = 1")
               .and("punished = ?")
               .and("type = ?")
               .and("(IF(update_length != -2, update_length, length) = -1")
               .or("(date + INTERVAL IF(update_length != -2, update_length, length) SECOND) > NOW())")
               .executeQuery(uuid.toString(), type.ordinal());

            if (sql.next()) {
                return buildPunishment(sql.getResultSet());
            } else {
                return null;
            }
        }
    }

    public static void updatePunishment(Punishment updated) {
        if (updated.isUpdated()) {
            String update = "update_length = ?, update_executor = ?, update_reason = ?, valid = ?";
            SQL.updateMultiple(PUNISHMENT, update, "id = ?", updated.updateLength, getUUIDExecutor(updated.updateExecutor), updated.updateReason, updated.valid, updated.id);
        }
    }

    public static Punishment buildPunishment(ResultSet rs) throws SQLException {
        Punishment p = new Punishment();
        p.id = rs.getInt(1);
        p.punished = UUID.fromString(rs.getString(2));
        p.executor = getExecutor(rs.getString(3));
        p.reason = rs.getString(4);
        p.type = PunishmentType.values()[rs.getInt(5)];
        p.timestamp = rs.getTimestamp(6).getTime();
        p.length = rs.getLong(7);
        p.updateLength = rs.getLong(8);
        p.updateExecutor = getExecutor(rs.getString(9));
        p.updateReason = rs.getString(10);
        p.valid = rs.getBoolean(11);
        return p;
    }

    public static String getUUIDExecutor(String raw) {
        if (raw == null || raw.equals("PvPDojo") || raw.equals("EyeHawk") || raw.equals("CONSOLE")) {
            return raw;
        } else {
            return DBCommon.getProfile(raw).getId().toString();
        }
    }

    private static String getExecutor(String raw) {
        if (raw == null || raw.equals("PvPDojo") || raw.equals("EyeHawk") || raw.equals("CONSOLE")) {
            return raw;
        } else {
            return DBCommon.getProfile(UUID.fromString(raw)).getName();
        }
    }

    public static List<Punishment> getAllActivePunishments(PunishmentType type) throws SQLException {
        if (type == PunishmentType.KICK) {
            return Collections.emptyList();
        }
        List<Punishment> active = new ArrayList<>();
        try (SQLBuilder sql = new SQLBuilder()) {
            sql.select(Tables.PUNISHMENT, "*")
               .where("valid = 1")
               .and("type = ?")
               .and("(IF(update_length != -2, update_length, length) = -1")
               .or("(date + INTERVAL IF(update_length != -2, update_length, length) SECOND) > NOW())")
               .executeQuery(type.ordinal());

            while (sql.getResultSet().next()) {
                active.add(buildPunishment(sql.getResultSet()));
            }
        }
        return active;
    }

    public static boolean handleMutedPlayer(Player player) {
        Punishment pun;

        try {
            pun = PunishmentManager.getActivePunishment(player.getUniqueId(), PunishmentType.MUTE);
        } catch (SQLException e) {
            player.sendMessage(PvPDojo.WARNING + "Could not fetch punishment data therefor chat message cannot be processed");
            Log.exception("chat", e);
            return true;
        }

        if (pun != null) {
            player.sendMessage(
                    PvPDojo.WARNING + "You are currently muted for " + CC.RED + CC.UNDERLINE + pun.getReason() + CC.GRAY + " for another " + CC.RED + pun.getRemainingTime());
            return true;
        }
        return false;
    }

    public static List<Punishment> getPunishments(UUID uuid) {
        List<Punishment> punishments = new ArrayList<>();
        try (SQLBuilder sql = new SQLBuilder()) {
            sql.select(Tables.PUNISHMENT, "*").where("punished = ?").orderBy("date", false).executeQuery(uuid.toString());
            while (sql.getResultSet().next()) {
                punishments.add(buildPunishment(sql.getResultSet()));
            }
        } catch (SQLException e) {
            Log.exception("Loading punishments", e);
        }
        return punishments;
    }

    public static Punishment getPunishment(int id) throws SQLException {
        try (SQLBuilder sql = new SQLBuilder()) {
            sql.select(Tables.PUNISHMENT, "*").where("id = ?").executeQuery(unsalt(id));
            if (sql.next()) {
                return buildPunishment(sql.getResultSet());
            } else {
                return null;
            }
        }
    }

    public static class Punishment {

        UUID punished;
        long updateLength = -2;
        long length;
        int id;
        long timestamp;
        boolean valid;
        String executor, reason, updateReason, updateExecutor;
        PunishmentType type;

        public UUID getPunished() {
            return punished;
        }

        public int getId() {
            return salt(id);
        }

        public String getExecutor() {
            return executor;
        }

        public PunishmentType getType() {
            return type;
        }

        public boolean isValid() {
            return valid;
        }

        public long getOriginalLength() {
            return length;
        }

        public String getOriginalReason() {
            return reason;
        }

        public boolean isUpdated() {
            return updateLength != -2 || !valid || updateReason != null;
        }

        public long getLength() {
            return updateLength != -2 ? updateLength : length;
        }

        public long getLengthCompare() {
            return getLength() == -1 ? Long.MAX_VALUE : getLength();
        }

        public String getReason() {
            return updateReason != null ? updateReason : reason;
        }

        public String getRemainingTime() {
            return getLength() == -1 ? StringUtils.getTimeLeft(getLength()) : StringUtils.getTimeLeft(timestamp + getLength() * 1000);
        }

        public long getTimestamp() {
            return timestamp;
        }

        public String getUpdateExecutor() {
            return updateExecutor;
        }

        public void setValid(boolean valid) {
            this.valid = valid;
        }

        public String getUpdateReason() {
            return updateReason;
        }

        public void setUpdateReason(String updateReason) {
            this.updateReason = updateReason;
        }

        public void setUpdateLength(long updateLength) {
            this.updateLength = updateLength;
        }

        public long getUpdateLength() {
            return updateLength;
        }

        public void setUpdateExecutor(String updateExecutor) {
            this.updateExecutor = updateExecutor;
        }

        public boolean isExpired() {
            return getLength() != -1 && System.currentTimeMillis() > getTimestamp() + getLength() * 1000;
        }

        public long getSecondsLeft() {
            return getLength() == -1 ? -1 : (timestamp / 1000 + getLength()) - System.currentTimeMillis() / 1000;
        }
    }

    public enum PunishmentType {
        BAN(Material.IRON_FENCE),
        MUTE(Material.JUKEBOX),
        KICK(Material.ARROW),
        SOFT_BAN(null);

        private Material icon;

        PunishmentType(Material icon) {
            this.icon = icon;
        }

        public Material getIcon() {
            return icon;
        }
    }

    public static int salt(int id) {
        return id * 2 + SALT;
    }

    public static int unsalt(int id) {
        return (id - SALT) / 2;
    }

}
