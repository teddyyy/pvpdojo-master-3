/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.util.bukkit;

import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.userdata.User;
import net.minecraft.server.v1_7_R4.EntityPlayer;
import net.minecraft.server.v1_7_R4.Packet;
import net.minecraft.server.v1_7_R4.PacketPlayOutWorldParticles;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public enum ParticleEffects {

    HUGE_EXPLODE(
            "hugeexplosion",
            0),
    LARGE_EXPLODE(
            "largeexplode",
            1),
    FIREWORK_SPARK(
            "fireworksSpark",
            2),
    AIR_BUBBLE(
            "bubble",
            3),
    SUSPEND(
            "suspend",
            4),
    DEPTH_SUSPEND(
            "depthSuspend",
            5),
    TOWN_AURA(
            "townaura",
            6),
    CRITICAL_HIT(
            "crit",
            7),
    MAGIC_CRITICAL_HIT(
            "magicCrit",
            8),
    MOB_SPELL(
            "mobSpell",
            9),
    MOB_SPELL_AMBIENT(
            "mobSpellAmbient",
            10),
    SPELL(
            "spell",
            11),
    INSTANT_SPELL(
            "instantSpell",
            12),
    BLUE_SPARKLE(
            "witchMagic",
            13),
    NOTE_BLOCK(
            "note",
            14),
    ENDER(
            "portal",
            15),
    ENCHANTMENT_TABLE(
            "enchantmenttable",
            16),
    EXPLODE(
            "explode",
            17),
    FIRE(
            "flame",
            18),
    LAVA_SPARK(
            "lava",
            19),
    FOOTSTEP(
            "footstep",
            20),
    SPLASH(
            "splash",
            21),
    LARGE_SMOKE(
            "largesmoke",
            22),
    CLOUD(
            "cloud",
            23),
    REDSTONE_DUST(
            "reddust",
            24),
    SNOWBALL_HIT(
            "snowballpoof",
            25),
    DRIP_WATER(
            "dripWater",
            26),
    DRIP_LAVA(
            "dripLava",
            27),
    SNOW_DIG(
            "snowshovel",
            28),
    SLIME(
            "slime",
            29),
    HEART(
            "heart",
            30),
    ANGRY_VILLAGER(
            "angryVillager",
            31),
    GREEN_SPARKLE(
            "happyVillager",
            32),
    ICONCRACK(
            "iconcrack",
            33),
    TILECRACK(
            "tilecrack",
            34),
    SMOKE(
            "smoke",
            35);

    private String name;
    private int id;

    ParticleEffects(String name, int id) {
        this.name = name;
        this.id = id;
    }

    /**
     * Gets the name of the Particle Effect
     *
     * @return The particle effect name
     */
    String getName() {
        return name;
    }

    /**
     * Gets the id of the Particle Effect
     *
     * @return The id of the Particle Effect
     */
    int getId() {
        return id;
    }

    /**
     * Send a particle effect to a player
     *
     * @param player   The player to send the effect to
     * @param location The location to send the effect to
     * @param offsetX  The x range of the particle effect
     * @param offsetY  The y range of the particle effect
     * @param offsetZ  The z range of the particle effect
     * @param speed    The speed (or color depending on the effect) of the particle effect
     * @param count    The count of effects
     */
    public void sendToPlayer(Player player, Location location, float offsetX, float offsetY, float offsetZ, float speed, int count) {
        PacketPlayOutWorldParticles packet = createPacket(this, location, offsetX, offsetY, offsetZ, speed, count);
        sendPacket(player, packet);
    }

    /**
     * Send a particle effect to all players
     *
     * @param location The location to send the effect to
     * @param offsetX  The x range of the particle effect
     * @param offsetY  The y range of the particle effect
     * @param offsetZ  The z range of the particle effect
     * @param speed    The speed (or color depending on the effect) of the particle effect
     * @param count    The count of effects
     */
    public void sendToLocation(Location location, float offsetX, float offsetY, float offsetZ, float speed, int count) {
        PacketPlayOutWorldParticles packet = createPacket(this, location, offsetX, offsetY, offsetZ, speed, count);
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.getWorld().equals(location.getWorld()) && player.getLocation().distance(location) < 32) {
                sendPacket(player, packet);
            }
        }
    }

    private static PacketPlayOutWorldParticles createPacket(ParticleEffects effect, Location location, float offsetX, float offsetY, float offsetZ, float speed, int count) {
        if (count <= 0) {
            count = 1;
        }
        // PacketPlayOutWorldParticles packet = new
        // PacketPlayOutWorldParticles(getEnum(effect.name), true,
        // (float)location.getX(), (float)location.getY(),
        // (float)location.getZ(), offsetX, offsetY, offsetZ, speed, count, new
        // int[]{0});
        return new PacketPlayOutWorldParticles(effect.name, (float) location.getX(), (float) location.getY(), (float) location.getZ(), offsetX,
                offsetY, offsetZ, speed, count);
    }

    /*
     * public static EnumParticle getEnum(String particle){ for (EnumParticle e : EnumParticle.values()){ if (e.name().equalsIgnoreCase(particle)){ return e; } } throw new
     * NullPointerException("null particle : " + particle); }
     */

    private static void sendPacket(Player player, Packet packet) {
        if (User.getUser(player).getSettings().isParticles()) {
            EntityPlayer eplayer = NMSUtils.get(player);
            eplayer.playerConnection.sendPacket(packet);
        }
    }
}