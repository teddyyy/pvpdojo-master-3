/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Flags;

@CommandAlias("ping")
public class PingCommand extends DojoCommand {

    @Default
    public void onPing(Player player) {
        player.sendMessage(PvPDojo.PREFIX + "You have a latency of " + CC.GREEN + NMSUtils.getPing(player) + CC.GRAY + " ms");
    }

    @CommandCompletion("@players")
    @CatchUnknown
    public void onPing(Player player, @Flags("other") Player target) {
        player.sendMessage(PvPDojo.PREFIX + CC.BLUE + target.getNick() + CC.GRAY + " has a latency of " + CC.GREEN + NMSUtils.getPing(target) + CC.GRAY + " ms");
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
