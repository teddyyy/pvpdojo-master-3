/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import static com.pvpdojo.util.StringUtils.formatDateDiff;

import java.lang.management.ManagementFactory;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("lag|uptime")
public class LagCommand extends DojoCommand {

    @Default
    public void onCommand(CommandSender sender) {
        sender.sendMessage(CC.GREEN + "Uptime: " + CC.RED + formatDateDiff(ManagementFactory.getRuntimeMXBean().getStartTime()));
        if (sender.hasPermission("Staff")) {
            sender.sendMessage(CC.GREEN + "Maximum memory: " + CC.RED + Runtime.getRuntime().maxMemory() / 1024 / 1024 + "mb");
            sender.sendMessage(CC.GREEN + "Allocated memory: " + CC.RED + Runtime.getRuntime().totalMemory() / 1024 / 1024 + "mb");
            sender.sendMessage(CC.GREEN + "Free memory: " + CC.RED + Runtime.getRuntime().freeMemory() / 1024 / 1024 + "mb");
            int loadedChunks = Bukkit.getWorlds().stream().map(World::getLoadedChunks).mapToInt(chunks -> chunks.length).sum();
            sender.sendMessage(CC.GREEN + "Loaded Chunks: " + CC.RED + loadedChunks);
            sender.sendMessage(CC.GREEN + "Folder: " + CC.RED + PvPDojo.get().getFolder());
        }
        Bukkit.dispatchCommand(sender, "tps");
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
