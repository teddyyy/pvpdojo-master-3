/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import static com.pvpdojo.util.StringUtils.getEnumName;

import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("ranks")
public class RanksCommand extends DojoCommand {

    @Default
    public void onRanks(Player player) {
        for (Rank rank : Rank.values()) {
            player.sendMessage(rank.getPrefix() + "Example" + PvPDojo.POINTER + CC.GRAY + getEnumName(rank, true));
        }
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
