/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;

public class InfoCommand extends DojoCommand {

    // @Override
    // public void onCommand(CommandWrapper wrap) {
    // CommandSender sender = wrap.getSender();
    // if (!wrap.checkArgs(1)) {
    // wrap.argumentException();
    // return;
    // }
    // String name = wrap.getArgs(1);
    // if (name.length() > 17) {
    // sender.sendMessage(CC.RED + "Invalid name, too many characters");
    // return;
    // }
    // sender.sendMessage(CC.GRAY + "looking up " + CC.GREEN + name);
    // }
    //
    // String[] blocked_ip_check = new String[] { "schmockyyy", "ReSuLTStatic", "Gabik21", "BastiGHG" };
    //
    // @Override
    // public String[] getAliases() {
    // return new String[] { "info", "profile" };
    // }

    @Override
    public Rank getRank() {
        return Rank.TRIAL_MODERATOR;
    }
}
