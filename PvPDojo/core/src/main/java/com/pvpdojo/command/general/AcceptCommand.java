/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import java.util.UUID;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Private;

@Private
@CommandAlias("accept")
public class AcceptCommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onAccept(User user, UUID uuid) {
        if (user.getRequests().containsKey(uuid)) {
            user.getRequests().remove(uuid).accept(user);
        } else {
            user.sendMessage(PvPDojo.WARNING + "Request not found or expired");
        }
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
