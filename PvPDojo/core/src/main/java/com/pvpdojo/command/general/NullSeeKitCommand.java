/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;

@CommandAlias("seekit|ckit|showkit")
public class NullSeeKitCommand extends BaseCommand {}
