/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import java.util.Collection;
import java.util.Collections;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Flags;
import co.aikar.commands.annotation.Subcommand;

@CommandAlias("sudo")
public class SudoCommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onSudo(@Flags("other") Player target, String chat) {
        handle(Collections.singleton(target), chat);
    }

    @Subcommand("all")
    public void onSudoAll(String chat) {
        handle(Bukkit.getOnlinePlayers(), chat);
    }

    private void handle(Collection<? extends Player> targets, String chat) {
        if (chat.startsWith("/")) {
            PvPDojo.schedule(() -> targets.stream().map(NMSUtils::get).forEach(ep -> ep.playerConnection.chat(chat, false))).now();
        } else {
            PvPDojo.schedule(() -> targets.stream().map(NMSUtils::get).forEach(ep -> ep.playerConnection.chat(chat, true))).createAsyncTask();
        }
    }

    @Override
    public Rank getRank() {
        return Rank.SERVER_ADMINISTRATOR;
    }
}
