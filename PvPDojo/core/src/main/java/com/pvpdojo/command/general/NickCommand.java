/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.mysql.SQLBuilder;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.NickData;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.TimeUtil;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Optional;
import co.aikar.commands.annotation.Single;
import net.minecraft.util.com.mojang.authlib.GameProfile;
import net.minecraft.util.com.mojang.authlib.properties.Property;

@CommandAlias("nick")
public class NickCommand extends DojoCommand {

    private static final Map<UUID, Long> COOLDOWN = new HashMap<>();

    private static final String NAMES_PATH = "../images/nicklist";
    private static final String SKIN_PATH = "../images/skinlist";

    private static final List<String> NICK_NAMES = new ArrayList<>();
    private static final List<UUID> SKINS = new ArrayList<>();

    static {
        try (Stream<String> stream = Files.lines(Paths.get(NAMES_PATH))) {
            stream.forEach(NICK_NAMES::add);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (Stream<String> stream = Files.lines(Paths.get(SKIN_PATH))) {
            stream.map(UUID::fromString).forEach(SKINS::add);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @CatchUnknown
    @Default
    public void onNick(User user, @Optional @Single String nickName) {
        if (COOLDOWN.containsKey(user.getUUID()) && !TimeUtil.hasPassed(COOLDOWN.get(user.getUUID()), TimeUnit.SECONDS, 5)) {
            user.sendMessage(CC.RED + "Do not spam this command");
            return;
        }
        COOLDOWN.put(user.getUUID(), System.currentTimeMillis());

        if (nickName != null) {
            Player player = user.getPlayer();

            if (hasRank(user.getPlayer(), Rank.MODERATOR_PLUS)) {
                PvPDojo.schedule(() -> {
                    DojoOfflinePlayer nickPlayer = PvPDojo.getOfflinePlayer(nickName);
                    GameProfile nickProfile = nickPlayer.getProfile();
                    if (!nickProfile.isComplete() || nickPlayer.hasPlayedBefore()) {
                        player.sendMessage(PvPDojo.WARNING + "Cannot use this nick name");
                        return;
                    }
                    String skinValue = CraftPlayer.STEVE_VALUE;
                    String skinSignature = CraftPlayer.STEVE_SIGNATURE;
                    if (!nickProfile.getProperties().isEmpty()) {
                        Property prop = nickProfile.getProperties().get("textures").iterator().next();
                        skinValue = prop.getValue();
                        skinSignature = prop.getSignature();
                    }
                    nick(user, new NickData(nickProfile.getId(), nickProfile.getName(), skinValue, skinSignature));
                }).createAsyncTask();
            }
        } else if (user.getPlayer().isNicked() && PvPDojo.SERVER_TYPE != ServerType.HUB && !hasRank(user.getPlayer(), Rank.MODERATOR)) {
            user.sendMessage(PvPDojo.WARNING + "You may only change your nick in hub");
        } else {
            randomNick(user);
        }
    }

    public void randomNick(User user) {
        PvPDojo.schedule(() -> {
            String nickName = NICK_NAMES.get(PvPDojo.RANDOM.nextInt(NICK_NAMES.size()));
            DojoOfflinePlayer nickPlayer = PvPDojo.getOfflinePlayer(nickName);

            if (nickPlayer.hasPlayedBefore()) {
                randomNick(user);
            } else {
                GameProfile skin = DBCommon.getProfile(SKINS.get(PvPDojo.RANDOM.nextInt(SKINS.size())));
                Property prop = skin.getProperties().get("textures").iterator().next();
                String skinValue = prop.getValue();
                String skinSignature = prop.getSignature();

                nick(user, new NickData(skin.getId(), nickName, skinValue, skinSignature));
            }
        }).createAsyncTask();
    }

    public static void nick(User user, NickData data) {
        if (data == null) {
            return;
        }

        Player player = user.getPlayer();

        PvPDojo.newChain().asyncFirst(() -> {
            String previousNick = Redis.get().getValue(BungeeSync.NICK_TO_REAL_NAME + "_" + player.getName());
            if (previousNick != null) {
                Redis.get().removeHValue(BungeeSync.NICK_TO_REAL_NAME, previousNick.toLowerCase());
            }
            Redis.get().setValue(BungeeSync.NICK_TO_REAL_NAME + "_" + user.getPlayer().getName(), data.getName());
            Redis.get().setHValue(BungeeSync.NICK_TO_REAL_NAME, data.getName().toLowerCase(), user.getPlayer().getName());
            Redis.get().setValue(BungeeSync.NICK_FAKE_STATS + user.getPlayer().getUniqueId(), getRandomStatsUUID().toString());
            return data;
        }).sync(() -> {
            if (!player.isOnline()) {
                return;
            }
            if (player.isNicked()) {
                user.getPlayer().resetNameAndSkin();
                user.setSpoofedRank(null);
                user.updateTablist();
                user.updateForEveryOne();
            }
            player.setNameAndSkin(data.getUniqueId(), data.getName(), data.getSkinValue(), data.getSkinSignature());
            user.setSpoofedRank(Rank.DEFAULT);
            user.updateTablist();
            user.updateForEveryOne();
            user.sendMessage(PvPDojo.PREFIX + "You are now nicked as " + CC.BLUE + data.getName());
        }).execute();
    }

    public static UUID getRandomStatsUUID() {
        try (SQLBuilder sql = new SQLBuilder()) {
            sql.select(Tables.HG_STATS, "uuid")
               .where("playtime > ?")
               .orderBy("RAND()", true)
               .limit(1)
               .executeQuery(8 * 60 * 60 * 1000)
               .next();

            return UUID.fromString(sql.getString("uuid"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return UUID.randomUUID();
    }

    @Override
    public Rank getRank() {
        return Rank.SENSEI;
    }

}
