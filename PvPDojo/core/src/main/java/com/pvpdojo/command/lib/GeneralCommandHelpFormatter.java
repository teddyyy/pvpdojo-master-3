/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.lib;

import java.util.List;

import org.jetbrains.annotations.NotNull;

import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.ACFPatterns;
import co.aikar.commands.ACFUtil;
import co.aikar.commands.CommandIssuer;
import co.aikar.commands.CommandManager;
import co.aikar.commands.CommandParameter;
import co.aikar.commands.MessageKeys;
import co.aikar.commands.MessageType;

@SuppressWarnings("rawtypes")
public class GeneralCommandHelpFormatter {
    private final CommandManager manager;

    public GeneralCommandHelpFormatter(CommandManager manager) {
        this.manager = manager;
    }

    public void showAllResults(GeneralCommandHelp commandHelp, List<GeneralHelpEntry> entries) {
        CommandIssuer issuer = commandHelp.getIssuer();
        printHelpHeader(commandHelp, issuer);
        for (GeneralHelpEntry e : entries) {
            printHelpCommand(commandHelp, issuer, e);
        }
        printHelpFooter(commandHelp, issuer);
    }

    public void showSearchResults(GeneralCommandHelp commandHelp, List<GeneralHelpEntry> entries) {
        CommandIssuer issuer = commandHelp.getIssuer();
        printSearchHeader(commandHelp, issuer);
        for (GeneralHelpEntry e : entries) {
            printSearchEntry(commandHelp, issuer, e);
        }
        printSearchFooter(commandHelp, issuer);
    }

    public void showDetailedHelp(GeneralCommandHelp commandHelp, GeneralHelpEntry entry) {
        CommandIssuer issuer = commandHelp.getIssuer();
        // header
        printDetailedHelpHeader(commandHelp, issuer, entry);

        // normal help line
        printDetailedHelpCommand(commandHelp, issuer, entry);

        // additionally detailed help for params
        // for (CommandParameter param : entry.getParameters()) {
        // String description = param.getDescription();
        // if (description != null && !description.isEmpty()) {
        // printDetailedParameter(commandHelp, issuer, entry, param);
        // }
        // }

        // footer
        printDetailedHelpFooter(commandHelp, issuer, entry);
    }

    // ########
    // # help #
    // ########

    public void printHelpHeader(GeneralCommandHelp help, CommandIssuer issuer) {
        issuer.sendMessage(MessageType.HELP, MessageKeys.HELP_HEADER, getHeaderFooterFormatReplacements(help));
    }

    @SuppressWarnings("deprecation")
    public void printHelpCommand(GeneralCommandHelp help, CommandIssuer issuer, GeneralHelpEntry entry) {
        String formatted = this.manager.formatMessage(issuer, MessageType.HELP, MessageKeys.HELP_FORMAT, getEntryFormatReplacements(help, entry));
        for (String msg : ACFPatterns.NEWLINE.split(formatted)) {
            issuer.sendMessageInternal(ACFUtil.rtrim(msg));
        }
    }

    public void printHelpFooter(GeneralCommandHelp help, CommandIssuer issuer) {
        if (help.isLastPage()) {
            return;
        }
        issuer.sendMessage(MessageType.HELP, MessageKeys.HELP_PAGE_INFORMATION, getHeaderFooterFormatReplacements(help));
    }

    // ##########
    // # search #
    // ##########

    public void printSearchHeader(GeneralCommandHelp help, CommandIssuer issuer) {
        issuer.sendMessage(MessageType.HELP, MessageKeys.HELP_SEARCH_HEADER, getHeaderFooterFormatReplacements(help));
    }

    @SuppressWarnings("deprecation")
    public void printSearchEntry(GeneralCommandHelp help, CommandIssuer issuer, GeneralHelpEntry page) {
        String formatted = this.manager.formatMessage(issuer, MessageType.HELP, MessageKeys.HELP_FORMAT, getEntryFormatReplacements(help, page));
        for (String msg : ACFPatterns.NEWLINE.split(formatted)) {
            issuer.sendMessageInternal(ACFUtil.rtrim(msg));
        }
    }

    public void printSearchFooter(GeneralCommandHelp help, CommandIssuer issuer) {
        if (help.isLastPage()) {
            return;
        }
        issuer.sendMessage(MessageType.HELP, MessageKeys.HELP_PAGE_INFORMATION, getHeaderFooterFormatReplacements(help));
    }

    // ############
    // # detailed #
    // ############

    public void printDetailedHelpHeader(GeneralCommandHelp help, CommandIssuer issuer, GeneralHelpEntry entry) {
        issuer.sendMessage(MessageType.HELP, MessageKeys.HELP_DETAILED_HEADER, "{command}", entry.getCommand(), "{commandprefix}", help.getCommandPrefix());
    }

    @SuppressWarnings("deprecation")
    public void printDetailedHelpCommand(GeneralCommandHelp help, CommandIssuer issuer, GeneralHelpEntry entry) {
        String formatted = this.manager.formatMessage(issuer, MessageType.HELP, MessageKeys.HELP_DETAILED_COMMAND_FORMAT, getEntryFormatReplacements(help, entry));
        for (String msg : ACFPatterns.NEWLINE.split(formatted)) {
            issuer.sendMessageInternal(ACFUtil.rtrim(msg));
        }
    }

    @SuppressWarnings("deprecation")
    public void printDetailedParameter(GeneralCommandHelp help, CommandIssuer issuer, GeneralHelpEntry entry, CommandParameter param) {
        String formattedMsg = this.manager.formatMessage(issuer, MessageType.HELP, MessageKeys.HELP_DETAILED_PARAMETER_FORMAT, getParameterFormatReplacements(help, param, entry));
        for (String msg : ACFPatterns.NEWLINE.split(formattedMsg)) {
            issuer.sendMessageInternal(ACFUtil.rtrim(msg));
        }
    }

    public void printDetailedHelpFooter(GeneralCommandHelp help, CommandIssuer issuer, GeneralHelpEntry entry) {
        // default doesn't have a footer
    }

    /**
     * Override this to control replacements
     *
     * @param help
     * @return
     */
    public String[] getHeaderFooterFormatReplacements(GeneralCommandHelp help) {
        return new String[] { "{search}", help.search != null ? String.join(" ", help.search) : "", "{command}", "Help", "{commandprefix}", "", "{rootcommand}", "Help", "{page}",
                "" + help.getPage(), "{totalpages}", "" + help.getTotalPages(), "{results}", "" + help.getTotalResults() };
    }

    /**
     * Override this to control replacements
     *
     * @param help
     * @param entry
     * @return
     */
    public String[] getEntryFormatReplacements(GeneralCommandHelp help, GeneralHelpEntry entry) {
        // {command} {parameters} {separator} {description}
        return new String[] { "{command}", entry.getCommand(), "{commandprefix}", CC.AQUA + help.getCommandPrefix(), "{parameters}", "", "{separator}",
                entry.getDescription().isEmpty() ? "" : "-", "{description}", entry.getDescription() };
    }

    /**
     * Override this to control replacements
     *
     * @param help
     * @param param
     * @param entry
     * @return
     */
    @NotNull
    public String[] getParameterFormatReplacements(GeneralCommandHelp help, CommandParameter param, GeneralHelpEntry entry) {
        // {name} {description}
        return new String[] { "{name}", param.getName(), "{syntax}", ACFUtil.nullDefault(param.getSyntax(), ""), "{description}", ACFUtil.nullDefault(param.getDescription(), ""),
                "{command}", "Help", "{fullcommand}", entry.getCommand(), "{commandprefix}", help.getCommandPrefix() };
    }
}
