/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;

import org.bukkit.command.CommandSender;

import com.pvpdojo.Discord;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.mysql.SQLBuilder;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.StringUtils;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Optional;

@CommandAlias("rank")
public class RankCommand extends DojoCommand {

    @Default
    @CatchUnknown
    @CommandCompletion("@allplayers @ranks")
    public void onRank(CommandSender sender, String target, @Optional Rank rank, @Optional Duration duration) {
        if (rank != null) {
            if (hasRank(sender, Rank.SERVER_ADMINISTRATOR) || (!rank.inheritsRank(Rank.DEVELOPER) && hasRank(sender, Rank.DEVELOPER))) {
                PvPDojo.schedule(() -> {
                    DojoOfflinePlayer offplayer = PvPDojo.getOfflinePlayer(target);
                    if (!offplayer.hasPlayedBefore()) {
                        PvPDojo.LANG.sendMessage(sender, MessageKeys.PLAYER_NEVER_JOINED);
                        return;
                    }

                    if (offplayer.isNicked()) {
                        sender.sendMessage(CC.RED + "You cannot set ranks for nicked players");
                        return;
                    }

                    try (SQLBuilder sql = new SQLBuilder()) {
                        Timestamp expireTime = duration != null ? Timestamp.from(Instant.now().plusSeconds(duration.getSeconds())) : Timestamp.valueOf(LocalDateTime.now().plusYears(100));

                        sql.update(Tables.USERS, "rank", "rankexpire").where("uuid = ?").executeUpdate(rank.name(), expireTime, offplayer.getUniqueId().toString());

                        BungeeSync.updateUser(offplayer.getUniqueId());
                        sender.sendMessage(PvPDojo.PREFIX + CC.GREEN + "Successfully given " + offplayer.getName() + " the " + rank.getPrefix() + rank.name() + CC.GREEN + " rank"
                                + (duration != null ? " for a period of " + StringUtils.getReadableDuration(duration) : ""));

                        if (!PvPDojo.get().isTest()) {
                            Discord.text().sendMessage(sender.getName() + " assigned " + rank + " for " + offplayer.getName()
                                    + (duration != null ? " for a period of " + StringUtils.getReadableDuration(duration) : "")).complete();
                        }
                    } catch (SQLException e) {
                        Log.exception(rank.name() + " failed on " + offplayer.getUniqueId(), e);
                        sender.sendMessage(PvPDojo.WARNING + "Could not set rank: " + e.getMessage());
                    }
                }).createAsyncTask();
            }
        } else {
            viewRank(sender, target);
        }
    }

    public void viewRank(CommandSender sender, String target) {
        PvPDojo.schedule(() -> {
            DojoOfflinePlayer offplayer = PvPDojo.getOfflinePlayer(target);
            PersistentData db = new PersistentData(offplayer.getUniqueId());
            try {
                db.pullUserData();
            } catch (SQLException e) {
                Log.exception(e);
                sender.sendMessage(PvPDojo.WARNING + "Could not pull user data");
                return;
            }
            Rank rank = offplayer.isNicked() ? Rank.DEFAULT : db.getRank();
            sender.sendMessage(PvPDojo.PREFIX + CC.GREEN + offplayer.getName() + "'s " + CC.GRAY + " rank is " + rank.getPrefix() + rank.getName());
        }).createAsyncTask();
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
