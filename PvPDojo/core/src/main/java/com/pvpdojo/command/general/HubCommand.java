/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import org.bukkit.entity.Player;

import com.pvpdojo.ServerType;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Description;

@CommandAlias("hub|lobby")
public class HubCommand extends DojoCommand {

    @Description("Teleports you to the server spawn")
    @Default
    public void onCommand(Player player) {
        ServerType.HUB.connect(player);
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
