/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.BroadcastUtil;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;

@CommandPermission("Staff")
@CommandAlias("alert|broadcast")
public class AlertCommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onAlert(User user, String content) {
        BroadcastUtil.global(ChatChannel.GLOBAL,PvPDojo.PREFIX + CC.GREEN + CC.translateAlternateColorCodes('&', content));
    }

    @Override
    public Rank getRank() {
        return Rank.MODERATOR_PLUS;
    }

}
