/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import com.pvpdojo.Warp;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;

@CommandAlias("warp")
public class WarpCommand extends DojoCommand {

    @Default
    @CatchUnknown
    @CommandCompletion("@warps")
    public void onWarp(User user, Warp warp) {
        user.handleWarp(warp);
    }
    
    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
