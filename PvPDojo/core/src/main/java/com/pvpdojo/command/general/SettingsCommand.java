/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import org.bukkit.entity.Player;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.menus.SettingsMenu;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("settings")
public class SettingsCommand extends DojoCommand {

    @Default
    public void onSettings(Player player) {
        new SettingsMenu(player).open();
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
