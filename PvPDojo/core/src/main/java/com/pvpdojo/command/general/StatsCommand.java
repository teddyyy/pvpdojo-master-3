/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Optional;
import co.aikar.commands.annotation.Single;
import co.aikar.commands.annotation.Subcommand;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.EloRank;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.userdata.stats.PersistentHGStats;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;

import java.sql.SQLException;

import static com.pvpdojo.util.StringUtils.getEnumName;
import static com.pvpdojo.util.StringUtils.getLS;

@CommandAlias("stats")
public class StatsCommand extends DojoCommand {

    @Default
    @CatchUnknown
    @CommandCompletion("@allplayers")
    public void onStats(User user, @Optional @Single String target) {
        PvPDojo.schedule(() -> {
            if (target != null) {
                DojoOfflinePlayer offPlayer = PvPDojo.getOfflinePlayer(target);
                if (offPlayer.hasPlayedBefore()) {
                    sendStats(user, offPlayer);
                } else {
                    user.sendMessage(MessageKeys.PLAYER_NEVER_JOINED);
                }
            } else {
                sendStats(user, PvPDojo.getOfflinePlayer(user.getUUID()));
            }
        }).createAsyncTask();
    }

    @Subcommand("hg")
    @CommandCompletion("@allplayers")
    public void onHGStats(User user, @Optional @Single String target) {
        PvPDojo.schedule(() -> {
            DojoOfflinePlayer off = PvPDojo.getOfflinePlayer(target != null ? target : user.getName());
            if (off.hasPlayedBefore()) {
                PersistentHGStats stats = new PersistentHGStats(off.isNicked() ? off.getNickFakeStats() : off.getUniqueId());
                try {
                    stats.load();
                } catch (SQLException e) {
                    Log.exception(e);
                    user.sendMessage(CC.RED + "An error occurred");
                    return;
                }

                user.sendMessage(CC.GRAY + getLS());
                user.sendMessage(CC.GRAY + "Player" + PvPDojo.POINTER + CC.GOLD + off.getName() + " [HG]");
                user.sendMessage(CC.GRAY + "Kills" + PvPDojo.POINTER + CC.GREEN + stats.getKills());
                user.sendMessage(CC.GRAY + "Deaths" + PvPDojo.POINTER + CC.RED + stats.getDeaths());
                user.sendMessage(CC.GRAY + "K/D" + PvPDojo.POINTER + CC.YELLOW + String.format("%.2f", (double) stats.getKills() / (double) stats.getDeaths()));
                user.sendMessage(CC.GRAY + "Skillpoints" + PvPDojo.POINTER + CC.BLUE + stats.getSkillPoints());
                user.sendMessage(CC.GRAY + "Games Played" + PvPDojo.POINTER + CC.WHITE + stats.getGamesPlayed());
                user.sendMessage(CC.GRAY + "Wins" + PvPDojo.POINTER + CC.DARK_AQUA + stats.getWins());
                user.sendMessage(CC.GRAY + "Bonus Kit Wins" + PvPDojo.POINTER + CC.GOLD + stats.getBonusKitWins());
                user.sendMessage(CC.GRAY + "Kill Record" + PvPDojo.POINTER + CC.AQUA + stats.getMaxKillstreak());
                user.sendMessage(CC.GRAY + "Credits" + PvPDojo.POINTER + CC.DARK_GREEN + stats.getMoney());
                user.sendMessage(CC.GRAY + "Play Time" + PvPDojo.POINTER + CC.GREEN + StringUtils.formatMillisDiff(stats.getPlayTime(), 3, false));
                user.sendMessage(CC.GRAY + getLS());
            } else {
                user.sendMessage(MessageKeys.PLAYER_NEVER_JOINED);
            }
        }).createAsyncTask();
    }

    public void sendStats(User user, DojoOfflinePlayer off) {
        PersistentData data;
        try {
            data = off.isNicked() ? new PersistentData(off.getNickFakeStats()) : off.getPersistentData();
            data.pullUserData();
        } catch (SQLException e) {
            Log.exception(e);
            user.sendMessage(CC.RED + "An error occurred");
            return;
        }

        user.sendMessage(CC.GRAY + getLS());
        user.sendMessage(CC.GRAY + "Player" + PvPDojo.POINTER + CC.GOLD + off.getName());
        user.sendMessage(CC.GRAY + "Kills" + PvPDojo.POINTER + CC.GREEN + data.getKills());
        user.sendMessage(CC.GRAY + "Deaths" + PvPDojo.POINTER + CC.RED + data.getDeaths());
        if (off.isOnlineProxy()) {
            String ks = Redis.get().getValue(BungeeSync.KILLSTREAK + off.getUniqueId());
            user.sendMessage(CC.GRAY + "Current Killstreak" + PvPDojo.POINTER + CC.DARK_AQUA + (ks != null ? ks : 0));
        }
        user.sendMessage(CC.GRAY + "Max Killstreak" + PvPDojo.POINTER + CC.AQUA + data.getMaxKillstreak());
        user.sendMessage(CC.GRAY + "Credits" + PvPDojo.POINTER + CC.DARK_GREEN + data.getMoney());
        EloRank eloRank = EloRank.getRank(data.getElo());
        user.sendMessage(
                CC.GRAY + "Elo" + PvPDojo.POINTER + CC.WHITE + data.getElo() + CC.GRAY + " (" + eloRank.getColor() + getEnumName(eloRank) + CC.GRAY + ")");
        long playTime = off.getUniqueId().equals(user.getUniqueId()) ? user.getStats().getPlayTime() : data.getPlayTime();
        user.sendMessage(CC.GRAY + "Play Time" + PvPDojo.POINTER + CC.GREEN + StringUtils.formatMillisDiff(playTime, 3, false));
        user.sendMessage(CC.GRAY + getLS());
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
