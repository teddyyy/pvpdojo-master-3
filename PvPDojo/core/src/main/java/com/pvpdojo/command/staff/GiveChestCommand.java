/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import org.bukkit.Bukkit;

import com.pvpdojo.DBCommon;
import com.pvpdojo.Discord;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilityinfo.AbilityCollection;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;

@CommandAlias("givechest")
public class GiveChestCommand extends DojoCommand {

    @Default
    @CatchUnknown
    @CommandCompletion("@allplayers @collections")
    public void onGiveChest(User user, String target, AbilityCollection collection, @Default("1") Integer amount) {
        if (collection == AbilityCollection.NONE) {
            return;
        }
        PvPDojo.schedule(() -> {
            DojoOfflinePlayer player = PvPDojo.getOfflinePlayer(target);

            if (!player.hasPlayedBefore()) {
                user.sendMessage(MessageKeys.PLAYER_NEVER_JOINED);
                return;
            }

            PersistentData data = Bukkit.getPlayer(target) != null ? User.getUser(Bukkit.getPlayer(target)).getPersistentData() : new PersistentData(DBCommon.getProfile(target).getId());

            PvPDojo.schedule(() -> {
                for (int i = 0; i < amount; i++) {
                    data.addCollection(collection.getId());
                }
            }).sync();
            user.sendMessage(CC.GREEN + "Given " + collection + " " + amount + " times to " + target);
            Discord.text().sendMessage(user.getName() + " gave " + amount + " " + collection + " to " + target).complete();
        }).createAsyncTask();
    }

    @Override
    public Rank getRank() {
        return Rank.DEVELOPER;
    }
}
