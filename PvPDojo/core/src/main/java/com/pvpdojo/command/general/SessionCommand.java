/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import static com.pvpdojo.util.StreamUtils.negate;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.ActionHandlers;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.menus.SessionMenu;
import com.pvpdojo.menus.SessionsMenu;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Single;
import co.aikar.commands.annotation.Subcommand;

@CommandAlias("session|game")
public class SessionCommand extends DojoCommand {

    @Default
    public void onSession(Player player) {
        PvPDojo.newChain()
               .asyncFirst(() -> DBCommon.getGameSessions(player.getUniqueId()))
               .syncLast(input -> new SessionsMenu(player, input).open())
               .execute();
    }

    @CatchUnknown
    public void onSession(Player player, @Single String sessionId) {
        try {
            new SessionMenu(player, Integer.parseInt(sessionId, 16)).open();
        } catch (NumberFormatException e) {
            showSyntax();
        }
    }

    @Subcommand("player")
    @CommandCompletion("@offlineplayers")
    public void onSessionOther(Player player, @Single String target) {
        PvPDojo.newChain()
               .asyncFirst(() -> PvPDojo.getOfflinePlayer(target))
               .abortIf(negate(OfflinePlayer::hasPlayedBefore), ActionHandlers.MESSAGE, player, PvPDojo.WARNING + "Player never joined the server")
               .async(off -> DBCommon.getGameSessions(off.getUniqueId()))
               .syncLast(input -> new SessionsMenu(player, input).open())
               .execute();
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
