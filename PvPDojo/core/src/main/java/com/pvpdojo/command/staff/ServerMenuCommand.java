/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import org.bukkit.entity.Player;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.menus.ServerMenu;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;

@CommandPermission("staff")
@CommandAlias("servermenu")
public class ServerMenuCommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onServerMenu(Player player) {
        new ServerMenu(player).open();
    }

    @Override
    public Rank getRank() {
        return Rank.TRIAL_MODERATOR;
    }
}
