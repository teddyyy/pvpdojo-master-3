/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.lib;

import org.bukkit.command.CommandSender;

import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.PlayerUtil;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandIssuer;
import co.aikar.commands.CommandManager;
import co.aikar.commands.RegisteredCommand;

public abstract class DojoCommand extends BaseCommand {

    @Override
    public boolean canExecute(CommandIssuer issuer, RegisteredCommand<?> cmd) {
        return PlayerUtil.hasRank(issuer.getIssuer(), getRank());
    }

    public void showSyntax() {
        showSyntax(getCurrentCommandIssuer(), CommandManager.getCurrentCommandOperationContext().getRegisteredCommand());
    }

    public boolean hasRank(CommandSender sender, Rank rank) {
        return PlayerUtil.hasRank(sender, rank);
    }

    public abstract Rank getRank();
}
