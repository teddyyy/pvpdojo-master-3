/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import org.bukkit.Bukkit;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("teleportall|tpall")
public class TeleportAllCommand extends DojoCommand {

    @CatchUnknown
    @Default
    public void onTeleportAll(User user) {
        Bukkit.getOnlinePlayers().forEach(player -> player.teleport(user.getPlayer()));
    }

    @Override
    public Rank getRank() {
        return Rank.DEVELOPER;
    }
}
