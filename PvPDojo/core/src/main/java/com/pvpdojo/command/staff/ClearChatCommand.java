/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import org.bukkit.command.CommandSender;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.BroadcastUtil;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;

@CommandPermission("Staff")
@CommandAlias("clearchat")
public class ClearChatCommand extends DojoCommand {

    private static final String[] EMPTY = new String[101];

    static {
        for (int i = 0; i < EMPTY.length; i++) {
            EMPTY[i] = " ";
        }
    }

    @CatchUnknown
    @Default
    public void onCommand(CommandSender sender) {
        EMPTY[100] = PvPDojo.PREFIX + CC.GREEN + "Chat has been cleared by " + sender.getName();
        BroadcastUtil.global(ChatChannel.GLOBAL, EMPTY);
    }

    @Override
    public Rank getRank() {
        return Rank.TRIAL_MODERATOR;
    }

}
