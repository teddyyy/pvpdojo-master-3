/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import static com.pvpdojo.util.StringUtils.getLS;

import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Conditions;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Flags;

@Conditions("adminmode")
@CommandAlias("playerinfo|pinfo")
public class PlayerInfoCommand extends DojoCommand {

    @Default
    @CatchUnknown
    @CommandCompletion("@players")
    public void onPlayerInfo(Player player, @Flags("other") Player target) {
        User targetUser = User.getUser(target);
        player.sendMessage(CC.GRAY + getLS());
        player.sendMessage(CC.GRAY + "Player" + PvPDojo.POINTER + CC.GOLD + target.getName());
        player.sendMessage(CC.GRAY + "Ping" + PvPDojo.POINTER + CC.GREEN + NMSUtils.getPing(target));
        player.sendMessage(CC.GRAY + "AFK" + PvPDojo.POINTER + (targetUser.isAFK() ? CC.GREEN : CC.RED) + targetUser.isAFK());
        player.sendMessage(CC.GRAY + "Spectator" + PvPDojo.POINTER + (targetUser.isSpectator() ? CC.GREEN : CC.RED) + targetUser.isSpectator());
        player.performCommand("ckit " + target.getNick());
        player.sendMessage(CC.GRAY + getLS());
    }

    @Override
    public Rank getRank() {
        return Rank.TRIAL_MODERATOR;
    }
}
