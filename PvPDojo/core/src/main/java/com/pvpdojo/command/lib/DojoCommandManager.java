/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.lib;

import java.util.Collection;
import java.util.Collections;
import java.util.Locale;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.command.general.AcceptCommand;
import com.pvpdojo.command.general.AchievementsCommand;
import com.pvpdojo.command.general.BalanceCommand;
import com.pvpdojo.command.general.ChallengeCommand;
import com.pvpdojo.command.general.ChatCommand;
import com.pvpdojo.command.general.ClanCommand;
import com.pvpdojo.command.general.FixCommand;
import com.pvpdojo.command.general.HelpCommand;
import com.pvpdojo.command.general.HideCommand;
import com.pvpdojo.command.general.HubCommand;
import com.pvpdojo.command.general.IgnoreCommand;
import com.pvpdojo.command.general.LagCommand;
import com.pvpdojo.command.general.ListCommand;
import com.pvpdojo.command.general.LobbyCommand;
import com.pvpdojo.command.general.MessageCommand;
import com.pvpdojo.command.general.NickCommand;
import com.pvpdojo.command.general.PartyCommand;
import com.pvpdojo.command.general.PingCommand;
import com.pvpdojo.command.general.ProfileCommand;
import com.pvpdojo.command.general.RanksCommand;
import com.pvpdojo.command.general.ReplyCommand;
import com.pvpdojo.command.general.ReportCommand;
import com.pvpdojo.command.general.SessionCommand;
import com.pvpdojo.command.general.SettingsCommand;
import com.pvpdojo.command.general.StatsCommand;
import com.pvpdojo.command.general.TagCommand;
import com.pvpdojo.command.general.TestCommand;
import com.pvpdojo.command.general.UnnickCommand;
import com.pvpdojo.command.general.WarpCommand;
import com.pvpdojo.command.general.WhereAmICommand;
import com.pvpdojo.command.general.WhereCommand;
import com.pvpdojo.command.staff.AdminCommand;
import com.pvpdojo.command.staff.AlertCommand;
import com.pvpdojo.command.staff.AuthCommand;
import com.pvpdojo.command.staff.BanCommand;
import com.pvpdojo.command.staff.BuildCommand;
import com.pvpdojo.command.staff.ChatlogCommand;
import com.pvpdojo.command.staff.CheckCommand;
import com.pvpdojo.command.staff.ClearChatCommand;
import com.pvpdojo.command.staff.FakeAbilitiesCommand;
import com.pvpdojo.command.staff.FreezeCommand;
import com.pvpdojo.command.staff.GiveChestCommand;
import com.pvpdojo.command.staff.GlobalMuteCommand;
import com.pvpdojo.command.staff.InfoCommand;
import com.pvpdojo.command.staff.InvseeCommand;
import com.pvpdojo.command.staff.KickCommand;
import com.pvpdojo.command.staff.MapSetupCommand;
import com.pvpdojo.command.staff.MuteCommand;
import com.pvpdojo.command.staff.NickListCommand;
import com.pvpdojo.command.staff.PardonCommand;
import com.pvpdojo.command.staff.PlayerInfoCommand;
import com.pvpdojo.command.staff.RankCommand;
import com.pvpdojo.command.staff.ReportsMenuCommand;
import com.pvpdojo.command.staff.RestartAllCommand;
import com.pvpdojo.command.staff.RestartCommand;
import com.pvpdojo.command.staff.ServerMenuCommand;
import com.pvpdojo.command.staff.ServerPurchaseCommand;
import com.pvpdojo.command.staff.SetLocationCommand;
import com.pvpdojo.command.staff.StopCommand;
import com.pvpdojo.command.staff.SudoCommand;
import com.pvpdojo.command.staff.TeleportAllCommand;
import com.pvpdojo.command.staff.UnusedAbilityIdCommand;
import com.pvpdojo.command.staff.VanishCommand;
import com.pvpdojo.replay.commands.RecordCommand;
import com.pvpdojo.replay.commands.ReplayCommand;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;

import co.aikar.commands.BukkitCommandCompletionContext;
import co.aikar.commands.BukkitCommandExecutionContext;
import co.aikar.commands.BukkitMessageFormatter;
import co.aikar.commands.BukkitRootCommand;
import co.aikar.commands.CommandCompletions;
import co.aikar.commands.CommandContexts;
import co.aikar.commands.ConditionFailedException;
import co.aikar.commands.MessageType;
import co.aikar.commands.PaperCommandManager;

public class DojoCommandManager extends PaperCommandManager {

    @SuppressWarnings("deprecation")
    public DojoCommandManager(Plugin plugin) {
        super(plugin);
        usePerIssuerLocale(true, true);
        setFormat(MessageType.HELP, new BukkitMessageFormatter(ChatColor.GRAY, ChatColor.GOLD, ChatColor.DARK_GRAY, ChatColor.AQUA, ChatColor.STRIKETHROUGH));
        setFormat(MessageType.SYNTAX, new BukkitMessageFormatter(ChatColor.RED, ChatColor.GRAY, ChatColor.AQUA));
        enableUnstableAPI("help");
    }

    public static DojoCommandManager instance;

    public static DojoCommandManager get() {
        return instance;
    }

    @Override
    public synchronized CommandContexts<BukkitCommandExecutionContext> getCommandContexts() {
        if (this.contexts == null) {
            this.contexts = new DojoCommandContexts(this);
        }
        return this.contexts;
    }

    @Override
    public synchronized CommandCompletions<BukkitCommandCompletionContext> getCommandCompletions() {
        if (this.completions == null) {
            this.completions = new DojoCommandCompletions(this);
        }
        return this.completions;
    }

    public Collection<BukkitRootCommand> getRegisteredCommands() {
        return Collections.unmodifiableCollection(registeredCommands.values());
    }

    public static void initialize() {
        instance = new DojoCommandManager(PvPDojo.get());


        get().getCommandConditions().addCondition("adminmode", c -> {
            if (c.getIssuer().isPlayer()) {
                User user = User.getUser(c.getIssuer().getPlayer());
                if (!user.isAdminMode() && !user.getRank().inheritsRank(Rank.DEVELOPER)) {
                    throw new ConditionFailedException("You must be in admin mode to execute this command");
                }
            }
        });
        get().getCommandConditions().addCondition("noclan", c -> {
            if (c.getIssuer().isPlayer()) {
                if (User.getUser(c.getIssuer().getPlayer()).getClan() != null) {
                    throw new ConditionFailedException("You are already in a clan");
                }
            }
        });
        get().getCommandConditions().addCondition("hasclan", c -> {
            if (c.getIssuer().isPlayer()) {
                if (User.getUser(c.getIssuer().getPlayer()).getClan() == null) {
                    throw new ConditionFailedException("You need to be in a clan");
                }
            }
        });
        get().getCommandConditions().addCondition("clanleader", c -> {
            if (c.getIssuer().isPlayer()) {
                if (!User.getUser(c.getIssuer().getPlayer()).getClan().getLeaders().contains(c.getIssuer().getUniqueId())) {
                    throw new ConditionFailedException("You need to be the clan leader");
                }
            }
        });

        get().getCommandConditions().addCondition("noparty", c -> {
            if (c.getIssuer().isPlayer()) {
                if (User.getUser(c.getIssuer().getPlayer()).getParty() != null) {
                    throw new ConditionFailedException("You are already in a party");
                }
            }
        });
        get().getCommandConditions().addCondition("hasparty", c -> {
            if (c.getIssuer().isPlayer()) {
                if (User.getUser(c.getIssuer().getPlayer()).getParty() == null) {
                    throw new ConditionFailedException("You need to be in a party");
                }
            }
        });
        get().getCommandConditions().addCondition("partyleader", c -> {
            if (c.getIssuer().isPlayer()) {
                if (!User.getUser(c.getIssuer().getPlayer()).getParty().getLeader().equals(c.getIssuer().getUniqueId())) {
                    throw new ConditionFailedException("You need to be the party leader");
                }
            }
        });

        get().setDefaultExceptionHandler((command, registeredCommand, sender, args, t) -> {
            System.err.println("Error occured while executing command " + command.getName());
            return false; // mark as unhandeled, sender will see default message
        });

        get().registerCommand(new ClanCommand());
        get().registerCommand(new ReplyCommand());
        get().registerCommand(new BuildCommand());
        get().registerCommand(new HubCommand());
        get().registerCommand(new VanishCommand());
        get().registerCommand(new BanCommand());
        get().registerCommand(new InvseeCommand());
        get().registerCommand(new KickCommand());
        get().registerCommand(new ChallengeCommand());
        get().registerCommand(new ServerPurchaseCommand());
        get().registerCommand(new LagCommand());
        get().registerCommand(new MessageCommand());
        get().registerCommand(new BalanceCommand());
        get().registerCommand(new AdminCommand());
        get().registerCommand(new RankCommand());
        get().registerCommand(new SetLocationCommand());
        get().registerCommand(new MapSetupCommand());
        get().registerCommand(new FixCommand());
        get().registerCommand(new FreezeCommand());
        get().registerCommand(new InfoCommand());
        get().registerCommand(new PardonCommand());
        get().registerCommand(new AcceptCommand());
        get().registerCommand(new ChatCommand());
        get().registerCommand(new UnnickCommand());
        get().registerCommand(new NickCommand());
        get().registerCommand(new CheckCommand());
        get().registerCommand(new TagCommand());
        get().registerCommand(new ReportCommand());
        get().registerCommand(new StatsCommand());
        get().registerCommand(new RanksCommand());
        get().registerCommand(new ClearChatCommand());
        get().registerCommand(new IgnoreCommand());
        get().registerCommand(new SettingsCommand());
        get().registerCommand(new AlertCommand());
        get().registerCommand(new GlobalMuteCommand());
        get().registerCommand(new RestartAllCommand());
        get().registerCommand(new UnusedAbilityIdCommand());
        get().registerCommand(new MuteCommand());
        get().registerCommand(new PartyCommand());
        get().registerCommand(new ProfileCommand());
        get().registerCommand(new PingCommand());
        get().registerCommand(new ListCommand());
        get().registerCommand(new WarpCommand());
        get().registerCommand(new HelpCommand());
        get().registerCommand(new ReplayCommand());
        get().registerCommand(new RecordCommand());
        get().registerCommand(new ReportsMenuCommand());
        get().registerCommand(new AuthCommand());
        get().registerCommand(new ServerMenuCommand());
        get().registerCommand(new SessionCommand());
        get().registerCommand(new NickListCommand());
        get().registerCommand(new AchievementsCommand());
        if (PvPDojo.SERVER_TYPE != ServerType.HUB) {
            get().registerCommand(new LobbyCommand());
        }
        get().registerCommand(new HideCommand());
        get().registerCommand(new PlayerInfoCommand());
        get().registerCommand(new GiveChestCommand());
        get().registerCommand(new FakeAbilitiesCommand());
        get().registerCommand(new ChatlogCommand());
        get().registerCommand(new TeleportAllCommand());
        get().registerCommand(new SudoCommand());
        get().registerCommand(new WhereAmICommand());
        get().registerCommand(new RestartCommand(), true);
        get().registerCommand(new StopCommand(), true);
        get().registerCommand(new WhereCommand());
        get().registerCommand(new TestCommand());
    }

    public Locale getUUIDLocale(UUID uuid) {
        return issuersLocale.containsKey(uuid) ? issuersLocale.get(uuid) : PvPDojo.LANG.getDefaultLocale();
    }

}
