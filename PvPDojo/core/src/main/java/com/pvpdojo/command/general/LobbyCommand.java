/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import com.pvpdojo.ServerType;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.settings.ServerSwitchSettings;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Log;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("lobby|join")
public class LobbyCommand extends DojoCommand {

    @CatchUnknown
    @Default
    public void onLobby(User user, String uuid) {
        Log.info("default");
        ServerType.HUB.connect(user.getPlayer(), new ServerSwitchSettings(user).addCommand("lobby " + uuid));
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
