/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;

@CommandPermission("Staff")
@CommandAlias("vanish|v")
public class VanishCommand extends DojoCommand {

    @Default
    public void onCommand(User user) {
        user.setVanish(!user.isVanish());
        user.sendMessage(PvPDojo.PREFIX + "Vanish mode: " + (user.isVanish() ? CC.GREEN + "enabled" : CC.RED + "disabled"));
    }

    @Override
    public Rank getRank() {
        return Rank.TRIAL_MODERATOR;
    }
}
