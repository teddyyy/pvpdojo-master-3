/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("restart")
public class RestartCommand extends DojoCommand {

    @CatchUnknown
    @Default
    public void onRestart() {
        BukkitUtil.restart();
    }

    @Override
    public Rank getRank() {
        return Rank.SERVER_ADMINISTRATOR;
    }
}
