/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import static com.pvpdojo.util.StringUtils.getLS;
import static com.pvpdojo.util.StringUtils.getStringFromArray;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import org.bukkit.command.CommandSender;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("list|who")
public class ListCommand extends DojoCommand {

    @Default
    public void onList(CommandSender sender) {
        PvPDojo.schedule(() -> {
            List<String> online = new ArrayList<>(BungeeSync.getOnlinePlayersDisplayName());
            online.sort(Comparator.comparing(self -> Rank.getRankByPrefix(CC.getLastColors(self))));
            PvPDojo.LANG.sendMessage(sender, MessageKeys.PLAYERS_ONLINE, "{players}", "" + online.size());
            sender.sendMessage(CC.GRAY + getLS());
            sender.sendMessage(getStringFromArray(online, Function.identity(), CC.GRAY + ", "));
            sender.sendMessage(CC.GRAY + getLS());
        }).createAsyncTask();
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
