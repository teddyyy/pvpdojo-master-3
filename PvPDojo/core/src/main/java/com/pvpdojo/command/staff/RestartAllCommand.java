/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.command.lib.DojoCommandManager;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Optional;
import co.aikar.commands.annotation.Subcommand;

@CommandPermission("Staff")
@CommandAlias("restartall|reboot")
public class RestartAllCommand extends DojoCommand {

    static {
        DojoCommandManager.get().getCommandCompletions().registerStaticCompletion("restartoptions", "NOW|RANDOM|EMPTY");
    }

    @CatchUnknown
    @Default
    @CommandCompletion("@restartoptions")
    public void onRestartAll(@Optional String time) {
        PvPDojo.schedule(() -> Redis.get().publish(BungeeSync.RESTART, "all" + "|" + (time != null ? time : "0"))).createAsyncTask();
    }

    @Subcommand("this")
    @CommandCompletion("@restartoptions")
    public void onReboot(@Optional String time) {

        if (time != null) {
            boolean now = time.equals("NOW");
            boolean random = time.equalsIgnoreCase("RANDOM");
            boolean empty = time.equalsIgnoreCase("EMPTY");

            BungeeSync.scheduleRestart(now ? 0 : random ? PvPDojo.RANDOM.nextInt(300) : empty ? Integer.MIN_VALUE : Integer.valueOf(time));
        } else {
            BungeeSync.scheduleRestart(10);
        }

    }

    @Subcommand("type")
    @CommandCompletion("@restartoptions")
    public void onRebootType(@Optional String time) {
        PvPDojo.schedule(() -> Redis.get().publish(BungeeSync.RESTART, PvPDojo.SERVER_TYPE.name() + "|" + (time != null ? time : "0"))).createAsyncTask();
    }

    @Override
    public Rank getRank() {
        return Rank.SERVER_ADMINISTRATOR;
    }

}
