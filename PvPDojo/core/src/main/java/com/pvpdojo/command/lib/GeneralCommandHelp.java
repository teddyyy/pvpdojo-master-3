/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.lib;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import co.aikar.commands.BukkitCommandManager;
import co.aikar.commands.BukkitRootCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.CommandIssuer;
import co.aikar.commands.CommandManager;
import co.aikar.commands.MessageKeys;
import co.aikar.commands.MessageType;

@SuppressWarnings("rawtypes")
public class GeneralCommandHelp {

    private CommandManager manager;
    private CommandIssuer issuer;
    private List<GeneralHelpEntry> helpEntries = new ArrayList<>();
    private GeneralHelpEntry exactMatch;
    private String commandPrefix;
    String search;
    private int totalResults;
    private int page;
    private int totalPages;
    private int perPage;
    private boolean lastPage = true;

    @SuppressWarnings("deprecation")
    public GeneralCommandHelp(BukkitCommandManager manager, CommandIssuer issuer) {
        this.issuer = issuer;
        this.manager = manager;
        this.commandPrefix = manager.getCommandPrefix(issuer);
        this.perPage = manager.getDefaultHelpPerPage();

        Set<BukkitRootCommand> seen = new HashSet<>();
        manager.getRegisteredRootCommands().forEach(rootCommand -> {
            BukkitRootCommand bukkitRootCommand = (BukkitRootCommand) rootCommand;
            if (seen.contains(rootCommand) || !bukkitRootCommand.testPermissionSilent(issuer.getIssuer())
                    || (rootCommand.getDefaultRegisteredCommand() != null && rootCommand.getDefaultRegisteredCommand().isPrivate())) {
                return;
            }
            helpEntries.add(new GeneralHelpEntry(this, bukkitRootCommand));
            seen.add(bukkitRootCommand);
        });
        helpEntries.sort(Comparator.comparing(GeneralHelpEntry::getCommand));
    }

    public CommandManager getManager() {
        return manager;
    }

    public CommandIssuer getIssuer() {
        return issuer;
    }

    public String getCommandPrefix() {
        return commandPrefix;
    }

    public boolean isLastPage() {
        return lastPage;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
        if (!isExactMatch()) {
            getHelpEntries().forEach(this::updateSearchScore);
            helpEntries.sort(Comparator.comparing(GeneralHelpEntry::getCommand));
        }
    }

    public boolean isExactMatch() {
        Optional<GeneralHelpEntry> match = helpEntries.stream().filter(entry -> entry.getCommand().equalsIgnoreCase(search)).findFirst();
        match.ifPresent(generalHelpEntry -> exactMatch = generalHelpEntry);
        return match.isPresent();
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public void showHelp() {
        showHelp(issuer);
    }

    public List<GeneralHelpEntry> getHelpEntries() {
        return helpEntries;
    }

    protected void updateSearchScore(GeneralHelpEntry help) {
        if (this.search == null || this.search.isEmpty()) {
            help.setSearchScore(1);
            return;
        }
        int searchScore = 0;
        if (help.getCommand().toLowerCase().contains(search.toLowerCase())) {
            searchScore++;
        }
        help.setSearchScore(searchScore);
    }

    public void showHelp(CommandIssuer issuer) {
        GeneralCommandHelpFormatter formatter = new GeneralCommandHelpFormatter(manager);

        if (exactMatch != null) {
            @SuppressWarnings("deprecation")
            CommandHelp exactHelp = manager.generateCommandHelp(exactMatch.getCommand());
            exactHelp.setPage(page);
            exactHelp.showHelp();
            return;
        }

        List<GeneralHelpEntry> results = getHelpEntries().stream()
                                                         .filter(GeneralHelpEntry::shouldShow)
                                                         .sorted(Comparator.comparingInt(helpEntry -> helpEntry.getSearchScore() * -1))
                                                         .collect(Collectors.toList());
        if (results.isEmpty()) {
            issuer.sendMessage(MessageType.ERROR, MessageKeys.NO_COMMAND_MATCHED_SEARCH, "{search}", search);
            return;
        }
        this.totalResults = results.size();
        int min = (this.page - 1) * this.perPage;
        int max = min + this.perPage;
        this.totalPages = (int) Math.ceil((float) totalResults / (float) this.perPage);
        int i = 0;
        if (min >= totalResults) {
            issuer.sendMessage(MessageType.HELP, MessageKeys.HELP_NO_RESULTS);
            return;
        }

        List<GeneralHelpEntry> printEntries = new ArrayList<>();
        Iterator<GeneralHelpEntry> resultsItr = results.iterator();
        while (resultsItr.hasNext()) {
            GeneralHelpEntry e = resultsItr.next();
            if (i >= max) {
                this.lastPage = false;
                break;
            }
            if (i++ < min) {
                continue;
            }
            printEntries.add(e);
        }

        if (search == null) {
            formatter.showAllResults(this, printEntries);
        } else {
            formatter.showSearchResults(this, printEntries);
        }
    }

}
