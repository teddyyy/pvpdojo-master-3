/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Stream;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.boydti.fawe.object.schematic.Schematic;
import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.map.DojoMap;
import com.pvpdojo.map.MapCuboidRegion;
import com.pvpdojo.map.MapHologram;
import com.pvpdojo.map.MapLoader;
import com.pvpdojo.map.MapNPC;
import com.pvpdojo.map.MapPolygonalRegion;
import com.pvpdojo.map.MapRegion;
import com.pvpdojo.map.MapType;
import com.pvpdojo.map.StateFlagCombo;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.Hologram;
import com.pvpdojo.util.bukkit.Human;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.CuboidSelection;
import com.sk89q.worldedit.bukkit.selections.Polygonal2DSelection;
import com.sk89q.worldedit.bukkit.selections.Selection;
import com.sk89q.worldedit.extent.clipboard.BlockArrayClipboard;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.function.operation.ForwardExtentCopy;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.regions.CuboidRegion;

import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.HelpCommand;
import co.aikar.commands.annotation.Optional;
import co.aikar.commands.annotation.Single;
import co.aikar.commands.annotation.Subcommand;
import net.minecraft.util.com.mojang.authlib.GameProfile;

@CommandPermission("Staff")
@CommandAlias("mapsetup|arenasetup|msetup|asetup")
@SuppressWarnings("deprecation")
public class MapSetupCommand extends DojoCommand {

    private Map<UUID, MapSetup> setupMap = new HashMap<>();

    @HelpCommand
    public void onHelp(CommandHelp help) {
        help.showHelp();
    }

    @Subcommand("create|c")
    @CommandCompletion("@maptypes")
    public void onCreate(Player player, String mapName, MapType mapType, @Optional Integer spawnLocations) {
        if (MapLoader.getMap(mapType, mapName) != null) {
            player.sendMessage(PvPDojo.WARNING + "There is already a map by that name and maptype try /mapsetup edit " + mapName + " " + mapType.name());
            return;
        }
        setupMap.put(player.getUniqueId(), spawnLocations != null ? new MapSetup(mapName, mapType, spawnLocations) : new MapSetup(mapName, mapType));
        player.sendMessage(PvPDojo.PREFIX + CC.GREEN + "Arena setup created with name " + mapName);
    }

    @Subcommand("edit|e")
    public void onEdit(Player player, String mapName, MapType mapType) {
        WorldEditPlugin we = com.pvpdojo.bukkit.util.BukkitUtil.getWorldEdit();
        Selection selection = we.getSelection(player);
        if (selection == null) {
            player.sendMessage(PvPDojo.WARNING + "You must select the exactly matching map first");
            return;
        }
        DojoMap map = MapLoader.getMap(mapType, mapName);
        if (map == null) {
            player.sendMessage(PvPDojo.WARNING + "There is no map by that type and name");
            return;
        }
        MapLoader.removeMap(map);

        Vector min = selection.getNativeMinimumPoint();
        map.shift(min.getBlockX(), min.getBlockY(), min.getBlockZ());
        MapSetup setup = new MapSetup(map);
        setupMap.put(player.getUniqueId(), setup);

        setup.refreshPreview(player);
        player.sendMessage(PvPDojo.PREFIX + CC.GREEN + "Loaded setup into memory");
    }

    @Subcommand("setlocation|sl")
    public void onSetLocation(Player player, int index) {
        setupMap.get(player.getUniqueId()).map.setSpawn(index, player.getLocation());
        player.sendMessage(PvPDojo.PREFIX + CC.GREEN + "Location set");
    }

    @Subcommand("maplocation|ml")
    public void onSetLocation(Player player, @Single String title) {
        setupMap.get(player.getUniqueId()).map.setSpawn(title, player.getLocation());
        player.sendMessage(PvPDojo.PREFIX + CC.GREEN + "Location set with name " + CC.BLUE + title);
    }

    @Subcommand("mapmeta|mm")
    public void onSetMeta(Player player, @Single String key, @Single String value) {
        setupMap.get(player.getUniqueId()).map.getMetaData().put(key, value);
        player.sendMessage(PvPDojo.PREFIX + CC.GREEN + "Meta '" + key + "' set to '" + value + "'");
    }

    @Subcommand("setarena|savearena|save")
    public void onSchematic(Player player) {
        WorldEditPlugin we = (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("GEdit");
        LocalSession session = WorldEdit.getInstance().getSessionManager().get(we.wrapPlayer(player));
        Selection selection = we.getSelection(player);
        if (selection == null) {
            player.sendMessage(PvPDojo.WARNING + "You must select your map with GEdit first");
            return;
        }

        Vector min = selection.getNativeMinimumPoint();
        Vector max = selection.getNativeMaximumPoint();

        MapSetup setup = setupMap.get(player.getUniqueId());

        CuboidRegion region = new CuboidRegion(BukkitUtil.getLocalWorld(player.getWorld()), min, max);

        setup.map.setFirstBound(new Vector());
        setup.map.setSecondBound(max.subtract(min).add(1, 1, 1));
        setup.origin = min;

        PvPDojo.schedule(() -> {
            EditSession edit = session.createEditSession(we.wrapPlayer(player));
            try {
                Clipboard clip = new BlockArrayClipboard(region);
                ForwardExtentCopy copy = new ForwardExtentCopy(edit, region, clip, region.getMinimumPoint());
                copy.setCopyEntities(true);
                copy.setCopyBiomes(true);
                try {
                    Operations.completeLegacy(copy);
                } catch (MaxChangedBlocksException e) {
                    e.printStackTrace();
                }
                Schematic schem = new Schematic(clip);
                schem.save(new File(DojoMap.MAP_SCHEMATICS, setup.map.getName() + "_" + setup.map.getType() + ".schematic"), ClipboardFormat.SCHEMATIC);
            } catch (IOException e) {
                e.printStackTrace();
            }
            player.sendMessage(PvPDojo.PREFIX + CC.GREEN + "Saved schematic");
        }).createAsyncTask();
    }

    @Subcommand("removeholo|rmholo")
    public void removeHolo(Player player, String hologramName) {
        MapSetup setup = setupMap.get(player.getUniqueId());

        if (setup.map.getHolograms().removeIf(holo -> holo.getName().equalsIgnoreCase(hologramName))) {
            player.sendMessage(PvPDojo.PREFIX + CC.GREEN + "You removed hologram: " + hologramName);
            setup.refreshPreview(player);
        } else {
            player.sendMessage(PvPDojo.WARNING + "Hologram not found");
        }

    }

    @Subcommand("teleportholo|tpholo")
    public void teleportHolo(Player player, String hologramName) {
        MapSetup setup = setupMap.get(player.getUniqueId());
        MapHologram h = setup.map.getHolograms().stream().filter(holo -> holo.getName().equalsIgnoreCase(hologramName)).findFirst().orElse(null);
        if (h != null) {
            h.setLocation(player.getEyeLocation());
            setup.refreshPreview(player);
        } else {
            player.sendMessage(PvPDojo.WARNING + "Hologram not found");
        }
    }

    @Subcommand("addline")
    public void addLine(Player player, String hologramName, String text) {
        String line = CC.translateAlternateColorCodes('&', text);
        MapSetup setup = setupMap.get(player.getUniqueId());

        MapHologram h = setup.map.getHolograms().stream().filter(holo -> holo.getName().equalsIgnoreCase(hologramName)).findFirst().orElse(null);
        if (h != null) {
            h.getText().add(line);
            player.sendMessage(PvPDojo.PREFIX + CC.GREEN + "Added line");
        } else {
            h = new MapHologram(hologramName, player.getEyeLocation(), Collections.singletonList(line));
            setup.map.getHolograms().add(h);
            player.sendMessage(PvPDojo.PREFIX + CC.GREEN + "Created new hologram");
        }
        setup.refreshPreview(player);
    }

    @Subcommand("removencp|rmncp")
    public void removeNcp(Player player, String name) {
        MapSetup setup = setupMap.get(player.getUniqueId());

        if (setup.map.getNpcs().removeIf(npc -> npc.getName().equalsIgnoreCase(name))) {
            setup.refreshPreview(player);
            player.sendMessage(PvPDojo.PREFIX + CC.GREEN + "You removed npc: " + name);
        } else {
            player.sendMessage(PvPDojo.WARNING + "Npc not found");
        }
    }

    @Subcommand("addncp")
    public void addNcp(Player player, String name, String npcName, @Optional String skinName) {
        MapSetup setup = setupMap.get(player.getUniqueId());
        if (setup.map.getNpcs().stream().anyMatch(npc -> npc.getName().equalsIgnoreCase(name))) {
            player.sendMessage(PvPDojo.WARNING + "There is already a npc by that name");
            return;
        }
        PvPDojo.newChain().asyncFirst(() -> {
            GameProfile profile = DBCommon.getProfile(skinName != null ? skinName : npcName);
            if (!profile.isComplete()) {
                player.sendMessage(PvPDojo.WARNING + "Npc name must a valid mc account name");
                return null;
            }
            return profile;
        }).abortIfNull().syncLast(profile -> {
            MapNPC npc = skinName != null ? new MapNPC(name, player.getLocation(), npcName, profile.getId()) : new MapNPC(name, player.getLocation(), profile);
            setup.map.getNpcs().add(npc);
            setup.refreshPreview(player);
        }).execute();
    }

    @Subcommand("addregion|addrg")
    public void addRegion(Player player, String regionName, @Optional StateFlagCombo[] flags) {
        MapSetup setup = setupMap.get(player.getUniqueId());
        WorldEditPlugin worldEdit = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("GEdit");
        Selection selection = worldEdit.getSelection(player);
        if (selection == null) {
            player.sendMessage(PvPDojo.WARNING + "You must select your region with GEdit first");
            return;
        }
        if (setup.map.getRegions().stream().anyMatch(rg -> rg.getName().equalsIgnoreCase(regionName))) {
            player.sendMessage(PvPDojo.WARNING + "There is already exists a region by that name");
            return;
        }

        MapRegion mapRegion = selection instanceof CuboidSelection ? new MapCuboidRegion(regionName, selection.getNativeMinimumPoint(),
                selection.getNativeMaximumPoint()) : new MapPolygonalRegion(regionName, ((Polygonal2DSelection) selection).getNativePoints(), selection.getNativeMinimumPoint(),
                selection.getNativeMaximumPoint());

        if (flags != null) {
            for (StateFlagCombo flag : flags) {
                mapRegion.getFlags().put(flag.getFlag().getName(), flag.getState());
            }
        }
        setup.map.getRegions().add(mapRegion);
        player.sendMessage(PvPDojo.PREFIX + CC.GREEN + "Added region " + CC.BLUE + regionName);
    }

    @Subcommand("removeregion|removerg|rmrg")
    public void removeRegion(Player player, String regionName) {
        MapSetup setup = setupMap.get(player.getUniqueId());
        boolean removed = setup.map.getRegions().removeIf(rg -> rg.getName().equalsIgnoreCase(regionName));
        if (removed) {
            player.sendMessage(PvPDojo.PREFIX + CC.GREEN + "Removed region " + CC.BLUE + regionName);
        } else {
            player.sendMessage(PvPDojo.WARNING + "Region not found");
        }
    }

    @Subcommand("info")
    public void onInfo(Player player) {
        MapSetup setup = setupMap.get(player.getUniqueId());
        player.sendMessage(PvPDojo.PREFIX + "Name: " + CC.GREEN + setup.map.getName());
        player.sendMessage(PvPDojo.PREFIX + "Type: " + CC.GREEN + setup.map.getType().name());
        player.sendMessage(PvPDojo.PREFIX + "Size: " + CC.GREEN + (setup.map.getSecondBound() == null ? "undefined" : setup.map.getSecondBound().toString()));
        player.sendMessage(PvPDojo.PREFIX + "Locations: " + setup.map.getLocations().length);
        Stream.of(setup.map.getLocations()).forEach(loc -> player.sendMessage(CC.GRAY
                + (loc == null ? "null" : "x: " + loc.getBlockX() + " y: " + loc.getBlockY() + " z: " + loc.getBlockZ() + " yaw: " + loc.getYaw() + " pitch: " + loc.getPitch())));
        player.sendMessage(PvPDojo.PREFIX + "Regions: " + setup.map.getRegions().size());
        setup.map.getRegions().forEach(rg -> {
            player.sendMessage(CC.GRAY + "Name: " + CC.GREEN + rg.getName());
            player.sendMessage(CC.GRAY + "Flags: " + CC.GREEN + rg.getFlags().size());
            rg.getFlags().forEach((key, value) -> player.sendMessage(CC.GRAY + key + " -> " + CC.GREEN + value));
            player.sendMessage(CC.GRAY + "Bounds: " + CC.GREEN + rg.getFirstBound().toString() + ", " + rg.getSecondBound().toString());
            player.sendMessage("");
        });
        player.sendMessage(PvPDojo.PREFIX + "Holograms: " + setup.map.getHolograms().size());
        setup.map.getHolograms().forEach(holo -> {
            player.sendMessage(CC.GRAY + "Name: " + CC.GREEN + holo.getName());
            player.sendMessage(
                    CC.GREEN + "Location: " + CC.GRAY + "x: " + holo.getLocation().getBlockX() + " y: " + holo.getLocation().getBlockY() + " z: " + holo.getLocation().getBlockZ());
            player.sendMessage(CC.GRAY + "Text: ");
            holo.getText().forEach(player::sendMessage);
            player.sendMessage("");
        });
        player.sendMessage(PvPDojo.PREFIX + "Npcs: " + setup.map.getNpcs().size());
        setup.map.getNpcs().forEach(npc -> {
            player.sendMessage(CC.GRAY + "Name: " + CC.GREEN + npc.getName());
            player.sendMessage(CC.GRAY + "Npc Name: " + CC.GREEN + npc.getNpcName());
            player.sendMessage(
                    CC.GREEN + "Location: " + CC.GRAY + "x: " + npc.getLocation().getBlockX() + " y: " + npc.getLocation().getBlockY() + " z: " + npc.getLocation().getBlockZ());
            player.sendMessage("");
        });
        player.sendMessage(PvPDojo.PREFIX + "Location mappings: " + setup.map.getLocationMap().size());
        setup.map.getLocationMap().forEach((s, location) -> {
            player.sendMessage(CC.GRAY + "Name: " + s);
            player.sendMessage(CC.GRAY + "Location: " + CC.GREEN + "x: " + location.getBlockX() + " y: " + location.getBlockY() + " z: " + location.getBlockZ());
        });
    }

    @Subcommand("finish|f")
    public void onFinish(Player player) {
        MapSetup setup = setupMap.get(player.getUniqueId());
        if (Stream.of(setup.map.getLocations()).anyMatch(Objects::isNull)) {
            player.sendMessage(PvPDojo.WARNING + "Some locations are not set");
            return;
        }
        if (setup.map.getFirstBound() == null || setup.origin == null) {
            player.sendMessage(PvPDojo.WARNING + "Map schematic is not set");
            return;
        }
        setup.clearPreview();
        setup.map.shift(-setup.origin.getBlockX(), -setup.origin.getBlockY(), -setup.origin.getBlockZ());
        setup.map.setFirstBound(setup.map.getFirstBound().add(setup.origin));
        setup.map.setSecondBound(setup.map.getSecondBound().add(setup.origin));
        player.sendMessage(PvPDojo.PREFIX + "Sending to db...");

        MapLoader.addMap(setup.map, thrown -> {
            if (thrown != null) {
                player.sendMessage(PvPDojo.WARNING + "Failed to upload to database: " + thrown.getMessage());
            } else {
                setupMap.remove(player.getUniqueId());
                player.sendMessage(PvPDojo.PREFIX + CC.GREEN + "Successfully set up arena");
            }
        });
    }

    @Override
    public Rank getRank() {
        return Rank.SERVER_ADMINISTRATOR;
    }

    private static class MapSetup {

        private List<Human> npcPreview = new ArrayList<>();
        private List<Hologram> hologramPreview = new ArrayList<>();
        private Vector origin;
        private DojoMap map;

        public MapSetup(DojoMap map) {
            this.map = map;
        }

        public MapSetup(String name, MapType type) {
            map = type.createInstance();
            map.setName(name);
        }

        public MapSetup(String name, MapType type, int spawnLocations) {
            this(name, type);
            map.setLocations(new Location[spawnLocations]);
        }

        public void refreshPreview(Player player) {
            clearPreview();
            map.setWorld(player.getWorld());
            map.getHolograms().forEach(holo -> {
                Hologram h = new Hologram(holo.getText());
                h.show(holo.getLocation(), player);
                hologramPreview.add(h);
            });
            map.getNpcs().forEach(npc -> {
                PvPDojo.newChain().asyncFirst(npc::getProfile).syncLast(profile -> {
                    Human h = new Human(npc.getLocation(), player, profile);
                    h.spawn();
                    npcPreview.add(h);
                }).execute();
            });
        }

        public void clearPreview() {
            npcPreview.forEach(Human::remove);
            hologramPreview.forEach(Hologram::destroy);
            npcPreview.clear();
            hologramPreview.clear();
        }

    }

}
