/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import com.pvpdojo.Discord;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Private;
import co.aikar.commands.annotation.Single;
import co.aikar.commands.annotation.Subcommand;

@Private
@CommandAlias("auth")
public class AuthCommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onAuth(User user, @Single String authKey) {
        if (user.getFastDB().isRequireAuth()) {
            if (authKey.equals(user.getFastDB().getAuthKey())) {
                user.getFastDB().setAuthKey(null);
                user.getFastDB().save(user.getUUID());
                user.getRank().refreshPermissions(user);
                user.sendMessage(PvPDojo.PREFIX + CC.GREEN + "Your permissions have been unlocked");
            } else {
                user.sendMessage(PvPDojo.WARNING + "Wrong key");
            }
        } else {
            user.sendMessage(PvPDojo.WARNING + "Your permissions are already unlocked");
        }
    }

    @Subcommand("resend")
    public void onResend(User user) {
        if (user.getFastDB().isRequireAuth()) {
            PvPDojo.schedule(() -> {
                Discord.get().sendAuth(user.getPlayer(), user.getFastDB().getAuthKey());
            }).createAsyncTask();
        }
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
