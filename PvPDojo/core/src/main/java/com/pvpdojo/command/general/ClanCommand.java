/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.clan.ClanList;
import com.pvpdojo.clan.ClanManager;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;

import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.*;
import co.aikar.commands.annotation.HelpCommand;

@CommandAlias("clan|gang")
public class ClanCommand extends DojoCommand {

    @Description("{@@pvpdojo.cmd-description.clan}")
    @HelpCommand
    public void doHelp(CommandSender user, CommandHelp help) {
        help.showHelp();
        PvPDojo.LANG.sendMessage(user, MessageKeys.CLAN_CHAT_HELP);
    }

    @Description("{@@pvpdojo.cmd-description.clan.info}")
    @Subcommand("info|list|members")
    @CommandCompletion("@clans")
    public void onInfo(User user, @Single @Optional String clanName) {
        PvPDojo.schedule(() -> ClanManager.showInfo(user, clanName != null ? ClanList.getClan(clanName) : user.getClan())).createAsyncTask();
    }

    @Description("{@@pvpdojo.cmd-description.clan.create}")
    @Subcommand("create")
    @Conditions("noclan")
    public void onCreate(User user, @Single String clanName) {
        ClanManager.createClan(user, clanName);
    }

    @Description("{@@pvpdojo.cmd-description.clan.leave}")
    @Subcommand("leave|quit")
    @Conditions("hasclan")
    public void onLeave(User user) {
        ClanManager.leaveClan(user);
    }

    @Conditions("hasclan|clanleader")
    public class ClanLeaderCommands extends DojoCommand {

        @Description("{@@pvpdojo.cmd-description.clan.invite}")
        @Subcommand("invite")
        @CommandCompletion("@players")
        public void onInvite(User user, @Flags("other") Player target) {
            ClanManager.invite(user, target);
        }

        @Description("{@@pvpdojo.cmd-description.clan.demote}")
        @Subcommand("demote")
        @CommandCompletion("@clanmembers")
        public void onDemote(User user, @Single String clanMember) {
            ClanManager.manageClanMember(user, clanMember, ClanManager::demote);
        }

        @Description("{@@pvpdojo.cmd-description.clan.promote}")
        @Subcommand("promote")
        @CommandCompletion("@clanmembers")
        public void onPromote(User user, @Single String clanMember) {
            ClanManager.manageClanMember(user, clanMember, ClanManager::promote);
        }

        @Description("{@@pvpdojo.cmd-description.clan.kick}")
        @Subcommand("kick")
        @CommandCompletion("@clanmembers")
        public void onKick(User user, @Single String clanMember) {
            ClanManager.manageClanMember(user, clanMember, ClanManager::kick);
        }


        @Override
        public Rank getRank() {
            return Rank.DEFAULT;
        }
    }

    @CommandPermission("Staff")
    public class StaffClanCommands extends DojoCommand {

        @Subcommand("delete")
        @CommandCompletion("@clans")
        public void onDelete(User user, @Single String clanName) {
            ClanManager.manageClan(user, clanName, ClanManager::delete);
        }

        @Subcommand("block")
        public void onBlock(User user, @Single String clanName) {
            ClanManager.block(user, clanName);
        }

        @Override
        public Rank getRank() {
            return Rank.TRIAL_MODERATOR;
        }
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
