/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.OptionalInt;

import org.bukkit.command.CommandSender;
import org.eclipse.egit.github.core.Gist;
import org.eclipse.egit.github.core.GistFile;

import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.mysql.SQLBuilder;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.Log;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Single;

@CommandAlias("chatlog|cl")
public class ChatlogCommand extends DojoCommand {

    @CatchUnknown
    @Default
    @CommandCompletion("@offlineplayers")
    public void onChatLog(CommandSender user, @Single String target, @Default("4h") Duration duration) {
        PvPDojo.schedule(() -> {

            DojoOfflinePlayer offlinePlayer = PvPDojo.getOfflinePlayer(target);

            if (!offlinePlayer.hasPlayedBefore()) {
                user.sendMessage(PvPDojo.WARNING + "Player never joined the server");
                return;
            }

            offlinePlayer.getPersistentData().pullUserDataSneaky();

            if (!hasRank(user, offlinePlayer.getPersistentData().getRank())) {
                return;
            }

            try {
                OptionalInt receiverId = DBCommon.getUserId(offlinePlayer.getUniqueId());
                StringBuilder builder = new StringBuilder();

                try (SQLBuilder sql = new SQLBuilder()) {
                    Timestamp timestamp = Timestamp.from(Instant.now().minusSeconds(duration.getSeconds()));

                    sql.select(Tables.CHAT_RECEIVERS, "senderid, message, date")
                       .join(Tables.CHAT_MESSAGES, "chat_log.messageid = chat_receivers.messageid")
                       .where("receiverid = ?")
                       .and("date > ?")
                       .orderBy("date", true)
                       .executeQuery(receiverId.orElseThrow(SQLException::new), timestamp);

                    while (sql.next()) {
                        String name = DBCommon.getLastKnownUserName(DBCommon.getUserById(sql.getInteger("senderid")).orElseThrow(SQLException::new));
                        String message = sql.getString("message");
                        LocalDateTime date = Instant.ofEpochMilli(sql.getTimestamp("date").getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();

                        String formattedDate = date.format(DateTimeFormatter.ofPattern("uuuu-MM-d"));
                        String formattedTime = date.format(DateTimeFormatter.ofPattern("HH:mm:ss"));

                        builder.append("[").append(formattedDate);
                        builder.append(" ").append(formattedTime).append("] ");
                        builder.append(name).append(" > ").append(message);
                        builder.append("\n");
                    }
                }

                if (builder.length() == 0) {
                    user.sendMessage(CC.RED + "Empty.");
                    return;
                }
                user.sendMessage(CC.GREEN + "Uploading to gist...");
                Gist gist = new Gist().setDescription("Chatlog of " + offlinePlayer.getName());
                GistFile file = new GistFile().setContent(builder.substring(0, builder.length() - 1));
                gist.setFiles(Collections.singletonMap("Chatlog#" + PvPDojo.RANDOM.nextInt(1000) + ".log", file));

                try {
                    gist = PvPDojo.GIST_SERVICE.createGist(gist);
                    user.sendMessage(PvPDojo.PREFIX + "Chatlog: " + gist.getHtmlUrl());
                    final Gist fgist = gist;
                    PvPDojo.schedule(() -> {
                        try {
                            PvPDojo.GIST_SERVICE.deleteGist(fgist.getId());
                            user.sendMessage(CC.GREEN + "Deleted requested log");
                        } catch (IOException e) {
                            user.sendMessage(CC.RED + "Failed to delete gist.");
                            e.printStackTrace();
                        }
                    }).createTaskAsync(8 * 20 * 60);

                } catch (IOException e) {
                    user.sendMessage(CC.RED + "Failed upload to gist!");
                    e.printStackTrace();
                }

            } catch (SQLException e) {
                Log.exception(e);
            }

        }).createAsyncTask();
    }

    @Override
    public Rank getRank() {
        return Rank.BUILDER;
    }
}
