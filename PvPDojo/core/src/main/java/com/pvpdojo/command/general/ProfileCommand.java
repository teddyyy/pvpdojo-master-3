/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import static com.pvpdojo.bukkit.util.ActionHandlers.MESSAGE;
import static com.pvpdojo.util.StreamUtils.negate;

import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.menus.ProfileMenu;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Single;

@CommandAlias("profile")
public class ProfileCommand extends DojoCommand {

    @Default
    public void onProfile(Player player) {
        new ProfileMenu(player).open();
    }

    @CommandPermission("staff")
    @CatchUnknown
    @CommandCompletion("@offlineplayers")
    public void onProfile(Player player, @Single String target) {
        PvPDojo.newChain()
               .asyncFirst(() -> PvPDojo.getOfflinePlayer(target))
               .abortIf(negate(DojoOfflinePlayer::hasPlayedBefore), MESSAGE, player, PvPDojo.WARNING + "Player never joined the server")
               .syncLast(other -> new ProfileMenu(player, other).open()).execute();
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
