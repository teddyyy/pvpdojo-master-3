/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.lib;

import static com.pvpdojo.util.StringUtils.MOJANG_UUID;
import static com.pvpdojo.util.StringUtils.getEnumFromString;
import static com.pvpdojo.util.StringUtils.getEnumNameList;

import java.time.Duration;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.pvpdojo.command.staff.BanCommand;
import com.pvpdojo.map.StateFlagCombo;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.StringUtils;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.StateFlag.State;

import co.aikar.commands.ACFBukkitUtil;
import co.aikar.commands.ACFUtil;
import co.aikar.commands.BukkitCommandExecutionContext;
import co.aikar.commands.BukkitCommandIssuer;
import co.aikar.commands.InvalidCommandArgument;
import co.aikar.commands.MessageKeys;
import co.aikar.commands.PaperCommandContexts;
import co.aikar.commands.PaperCommandManager;
import co.aikar.commands.bukkit.contexts.OnlinePlayer;
import co.aikar.commands.contexts.ContextResolver;

public class DojoCommandContexts extends PaperCommandContexts {

    public DojoCommandContexts(PaperCommandManager manager) {
        super(manager);

        registerIssuerOnlyContext(User.class, c -> User.getUser(c.getIssuer().getPlayer()));

        registerContext(Enum.class, (c) -> {
            final String first = c.popFirstArg();
            @SuppressWarnings({ "unchecked", "deprecation" })
            Class<? extends Enum<?>> enumCls = (Class<? extends Enum<?>>) c.getParam().getType();
            Enum<?> match = getEnumFromString(first, enumCls);
            if (match == null) {
                List<String> names = getEnumNameList(enumCls.getEnumConstants());
                throw new InvalidCommandArgument(MessageKeys.PLEASE_SPECIFY_ONE_OF, "{valid}", ACFUtil.join(names));
            }
            return match;
        });

        registerContext(UUID.class, c -> {
            String input = c.popFirstArg();
            if (!StringUtils.UUID.matcher(input).matches()) {
                throw new InvalidCommandArgument();
            }
            return UUID.fromString(input);
        });

        registerContext(Duration.class, c -> {
            String time = c.getFirstArg();
            TimeUnit unit = BanCommand.UNIT_SHORTCUTS.get(time.substring(time.length() - 1));
            if (unit == null) {
                return null;
            }
            try {
                int number = Integer.parseInt(time.substring(0, time.length() - 1));
                c.popFirstArg();
                return Duration.ofSeconds(unit.toSeconds(number));
            } catch (NumberFormatException e) {
                return null;
            }
        });

        ContextResolver<StateFlagCombo, BukkitCommandExecutionContext> stateFlagResolver = c -> {
            String input = c.popFirstArg();
            String[] part = input.split(",");
            if (part.length != 2) {
                throw new InvalidCommandArgument();
            }
            StateFlag flag = (StateFlag) DefaultFlag.fuzzyMatchFlag(part[0]);
            State state = getEnumFromString(part[1], State.class);
            if (flag == null || state == null) {
                throw new InvalidCommandArgument();
            }
            return new StateFlagCombo(flag, state);
        };
        registerContext(StateFlagCombo.class, stateFlagResolver);
        registerContext(StateFlagCombo[].class, c -> {
            StateFlagCombo[] flagCombos = new StateFlagCombo[c.getArgs().size()];
            int idx = 0;
            while (!c.getArgs().isEmpty()) {
                flagCombos[idx++] = stateFlagResolver.getContext(c);
            }
            return flagCombos;
        });

        registerContext(Player[].class, c -> Stream.of((OnlinePlayer[]) getResolver(OnlinePlayer[].class).getContext(c)).map(OnlinePlayer::getPlayer).toArray(Player[]::new));
    }

    @Override
    protected OnlinePlayer getOnlinePlayer(BukkitCommandIssuer issuer, String lookup, boolean allowMissing) throws InvalidCommandArgument {
        if (MOJANG_UUID.matcher(lookup).matches()) {
            OnlinePlayer onlinePlayer = new OnlinePlayer(Bukkit.getPlayer(UUID.fromString(lookup)));
            if (onlinePlayer.getPlayer() == null && !allowMissing) {
                throw new InvalidCommandArgument(false);
            }
            return onlinePlayer;
        }
        Player player = ACFBukkitUtil.findPlayerSmart(issuer, lookup);
        //noinspection Duplicates
        if (player == null) {
            if (allowMissing) {
                return null;
            }
            throw new InvalidCommandArgument(false);
        }
        return new OnlinePlayer(player);
    }

}
