/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import org.bukkit.Bukkit;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.settings.ServerSwitchSettings;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.PlayerUtil;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Single;

@CommandPermission("Staff")
@CommandAlias("check")
public class CheckCommand extends DojoCommand {

    @Default
    @CatchUnknown
    @CommandCompletion("@allplayers")
    public void onCheck(User user, @Single String target) {

        // Player is already on the same server. Great!
        if (Bukkit.getPlayer(target) != null) {
            user.setAdminMode(true);
            user.teleport(Bukkit.getPlayer(target).getLocation());
            return;
        }

        // Search for target on the network
        PvPDojo.schedule(() -> {
            DojoOfflinePlayer player = PvPDojo.getOfflinePlayer(target);

            String serverName = player.getServerName();

            if (serverName.isEmpty()) {
                user.sendMessage(PvPDojo.WARNING + "Could not locate player");
                return;
            }

            ServerSwitchSettings switchSettings = new ServerSwitchSettings(user);
            PlayerUtil.sendToServer(user.getPlayer(), serverName, switchSettings.adminMode(true).teleportTo(player.getUniqueId()));

        }).createAsyncTask();
    }

    @Override
    public Rank getRank() {
        return Rank.HELPER;
    }

}
