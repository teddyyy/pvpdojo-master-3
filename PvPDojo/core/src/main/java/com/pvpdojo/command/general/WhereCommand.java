/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import co.aikar.commands.annotation.*;
import org.bukkit.command.CommandSender;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.CC;

@CommandAlias("where")
public class WhereCommand extends DojoCommand {

    @CatchUnknown
    @Default
    @CommandCompletion("@allplayers")
    public void onWhere(CommandSender sender, @Single String target) {
        PvPDojo.schedule(() -> sender.sendMessage(CC.RED + BungeeSync.findPlayer(PvPDojo.getOfflinePlayer(target).getUniqueId()))).createAsyncTask();
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
