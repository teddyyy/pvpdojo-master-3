/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.lib;

import static com.pvpdojo.util.StringUtils.getEnumNameList;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.Warp;
import com.pvpdojo.abilityinfo.AbilityCollection;
import com.pvpdojo.clan.ClanList;
import com.pvpdojo.map.MapType;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.redis.Report;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.PunishmentManager;
import com.pvpdojo.util.PunishmentManager.PunishmentType;

import co.aikar.commands.PaperCommandCompletions;
import co.aikar.commands.PaperCommandManager;

public class DojoCommandCompletions extends PaperCommandCompletions {

    public DojoCommandCompletions(PaperCommandManager manager) {
        super(manager);

        registerCompletion("players", c -> {
            CommandSender sender = c.getSender();
            Validate.notNull(sender, "Sender cannot be null");

            Player senderPlayer = sender instanceof Player ? (Player) sender : null;

            ArrayList<String> matchedPlayers = new ArrayList<>();
            for (Player player : Bukkit.getOnlinePlayers()) {
                String name = player.getNick();
                if ((senderPlayer == null || senderPlayer.canSee(player))) {
                    matchedPlayers.add(name);
                }
            }

            matchedPlayers.sort(String.CASE_INSENSITIVE_ORDER);
            return matchedPlayers;
        });

        registerCompletion("reporttypes", c -> getEnumNameList(Report.ReportType.values()));
        registerCompletion("chatchannels", c -> getEnumNameList(ChatChannel.values()));
        registerCompletion("ranks", c -> getEnumNameList(Rank.values()));
        registerCompletion("maptypes", c -> getEnumNameList(MapType.values()));
        registerCompletion("warps", c -> getEnumNameList(Warp.values()));
        registerCompletion("collections", c -> getEnumNameList(AbilityCollection.values()));

        registerAsyncCompletion("bannedplayers", c -> {
            if (Bukkit.isPrimaryThread()) {
                return Collections.emptyList();
            }
            try {
                return PunishmentManager.getAllActivePunishments(PunishmentType.BAN).stream()
                                        .map(pun -> DBCommon.getLastKnownUserName(pun.getPunished()))
                                        .sorted(String.CASE_INSENSITIVE_ORDER).collect(Collectors.toList());
            } catch (SQLException e) {
                c.getSender().sendMessage(PvPDojo.WARNING + "Could not get tabcomplete from DB");
                e.printStackTrace();
                return Collections.emptyList();
            }
        });

        registerAsyncCompletion("allplayers", c -> {
            if (Bukkit.isPrimaryThread()) {
                return Collections.emptyList();
            }
            return BungeeSync.getOnlinePlayers().stream()
                             .sorted(String.CASE_INSENSITIVE_ORDER)
                             .collect(Collectors.toList());
        });

        registerAsyncCompletion("clans", c -> {
            if (Bukkit.isPrimaryThread()) {
                return Collections.emptyList();
            }
            return ClanList.getAllClanNames();
        });

        registerAsyncCompletion("clanmembers", c -> {
            if (Bukkit.isPrimaryThread()) {
                return Collections.emptyList();
            }
            User user = User.getUser(c.getPlayer());
            if (user.getClan() == null) {
                return Collections.emptyList();
            }
            return user.getClan().getAllMembers().stream()
                       .map(PvPDojo::getOfflinePlayer)
                       .map(OfflinePlayer::getName)
                       .sorted(String.CASE_INSENSITIVE_ORDER)
                       .collect(Collectors.toList());
        });

        registerAsyncCompletion("partymembers", c -> {
            if (Bukkit.isPrimaryThread()) {
                return Collections.emptyList();
            }
            User user = User.getUser(c.getPlayer());
            if (user.getParty() == null) {
                return Collections.emptyList();
            }
            return user.getParty().getUsers().stream()
                       .map(PvPDojo::getOfflinePlayer)
                       .map(OfflinePlayer::getName)
                       .sorted(String.CASE_INSENSITIVE_ORDER)
                       .collect(Collectors.toList());
        });

        registerAsyncCompletion("offlineplayers", context -> {
            if (Bukkit.isPrimaryThread()) {
                return Collections.emptyList();
            }
            List<String> userNames = DBCommon.getAllLastKnownUserNames();
            userNames.sort(String.CASE_INSENSITIVE_ORDER);
            return userNames;
        });
    }

}
