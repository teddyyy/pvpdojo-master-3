/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("reply|r")
public class ReplyCommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onReply(Player player, String message) {
        PvPDojo.schedule(() -> {
            String target = Redis.get().getValue(BungeeSync.MSG_REPLY + player.getUniqueId().toString());
            if (target != null) {
                PvPDojo.schedule(() -> player.performCommand("msg " + target + " " + message)).sync();
            } else {
                player.sendMessage(PvPDojo.WARNING + "You have no player to reply to");
            }
        }).createAsyncTask();
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
