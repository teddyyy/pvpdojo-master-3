/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import java.time.Duration;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.PunishmentManager;
import com.pvpdojo.util.PunishmentManager.PunishmentType;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

import co.aikar.commands.CommandManager;
import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Optional;
import co.aikar.commands.annotation.Split;

@CommandPermission("Staff")
@CommandAlias("mute|tempmute|tmute")
public class MuteCommand extends DojoCommand {

    @Default
    @CatchUnknown
    @CommandCompletion("@allplayers")
    public void onMute(CommandSender sender, @Split String[] targets, @Optional Duration dur, @Optional String reason) {

        if (reason == null) {
            if (sender instanceof Player) {
                new MuteMenu((Player) sender, targets).open();
            } else {
                showSyntax(getCurrentCommandIssuer(), CommandManager.getCurrentCommandOperationContext().getRegisteredCommand());
            }
            return;
        }

        for (String target : targets) {
            PvPDojo.schedule(() -> PunishmentManager.punish(sender, target, reason, dur != null ? dur.getSeconds() : -1, PunishmentType.MUTE)).createAsyncTask();
        }
    }

    @Override
    public Rank getRank() {
        return Rank.HELPER;
    }

    private class MuteMenu extends SingleInventoryMenu {

        private String[] targets;

        public MuteMenu(Player player, String[] targets) {
            super(player, "Mute Menu", 27);
            this.targets = targets;
            setCloseOnClick(true);
        }

        @Override
        public void redraw() {
            InventoryPage inv = getCurrentPage();

            inv.setItem(4, new ItemBuilder(Material.QUARTZ_BLOCK).name(CC.RED + "Racism").build(), clickType -> confirm("Racism", Duration.ofDays(3)));
            inv.setItem(11, new ItemBuilder(Material.IRON_SWORD).name(CC.RED + "Toxic Chat Behaviour").build(), clickType -> confirm("Toxic Chat Behaviour", Duration.ofHours(1)));
            inv.setItem(13, new ItemBuilder(Material.SPECKLED_MELON).name(CC.RED + "Bad Sportsmanship").build(), clickType -> confirm("Bad Sportsmanship", Duration.ofMinutes(30)));
            inv.setItem(15, new ItemBuilder(Material.EXPLOSIVE_MINECART).name(CC.RED + "Advertising").build(), clickType -> confirm("Advertising", Duration.ofHours(6)));
            inv.setItem(20, new ItemBuilder(Material.COMMAND_MINECART).name(CC.RED + "Spam").build(), clickType -> confirm("Spam", Duration.ofMinutes(30)));
            inv.setItem(22, new ItemBuilder(Material.DEAD_BUSH).name(CC.RED + "Inappropriate Chat Behaviour").build(), clickType -> confirm("Inappropriate Chat Behaviour", Duration.ofDays(1)));
            inv.setItem(24, new ItemBuilder(Material.DIAMOND_SWORD).name(CC.RED + "Encouraging Suicide").build(), clickType -> confirm("Encouraging Suicide", Duration.ofHours(12)));
        }

        private void confirm(String reason, Duration duration) {
            onMute(getPlayer(), targets, duration, reason);
        }

    }

}
