/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import org.bukkit.command.CommandSender;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("whereami|networkid|netid")
public class WhereAmICommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onWhereAmI(CommandSender sender) {
        sender.sendMessage(CC.GREEN + PvPDojo.get().getNetworkPointer());
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
