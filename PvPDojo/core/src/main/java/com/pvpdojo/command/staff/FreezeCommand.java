/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.Hologram;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Flags;

@CommandPermission("Staff")
@CommandAlias("freeze|unfreeze|ss")
public class FreezeCommand extends DojoCommand {

    @Default
    @CatchUnknown
    @CommandCompletion("@players")
    public void onFreeze(CommandSender sender, @Flags("other") Player target) {
        target.setFrozen(!target.isFrozen());
        User user = User.getUser(target);
        user.setFrozenByStaff(target.isFrozen());
        if (target.isFrozen()) {
            target.sendMessage(PvPDojo.WARNING + CC.RED + "You have been frozen by a staff member please follow their instruction");
            Hologram holo = new Hologram(CC.RED + "You have been frozen", CC.RED + "by a staff member.", CC.RED + "Please follow their instructions.", CC.RED + "Do not log out.");
            user.attachHologram(holo, 4, Integer.MAX_VALUE);
            sender.sendMessage(PvPDojo.PREFIX + CC.RED + target.getName() + " has been frozen");
        } else {
            user.detachHologram();
            target.sendMessage(PvPDojo.PREFIX + CC.GREEN + "You have been unfrozen");
            sender.sendMessage(PvPDojo.PREFIX + CC.GREEN + target.getName() + " has been unfrozen");
        }
    }

    @Override
    public Rank getRank() {
        return Rank.TRIAL_MODERATOR;
    }

}
