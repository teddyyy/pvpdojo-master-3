/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import org.bukkit.entity.Player;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.menus.ChallengeMenu;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Description;

@CommandAlias("challenges|challenge")
public class ChallengeCommand extends DojoCommand {

    @Default
    @Description("{@@pvpdojo.cmd-description.challenge}")
    public void onCommand(Player player) {
        ChallengeMenu.getChallengeMenu(player).open();
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
