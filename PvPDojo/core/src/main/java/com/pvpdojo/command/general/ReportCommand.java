/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import static com.pvpdojo.util.StringUtils.getEnumName;

import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import com.pvpdojo.DBCommon;
import com.pvpdojo.Discord;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.gui.InputGUI;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.redis.Report;
import com.pvpdojo.redis.Report.ReportType;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.impl.DojoOfflinePlayerImpl;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.BroadcastUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.CompBuilder;
import com.pvpdojo.util.bukkit.ItemBuilder;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Optional;

@CommandAlias("report|blamestaff")
public class ReportCommand extends DojoCommand {

    @Default
    @CatchUnknown
    @CommandCompletion("@allplayers @reporttypes")
    public void onReport(CommandSender sender, String target, @Optional Report.ReportType reason, @Optional String additional) {
        PvPDojo.schedule(() -> {
            DojoOfflinePlayer targetPlayer = PvPDojo.getOfflinePlayer(target);

            if (!targetPlayer.hasPlayedBefore()) {
                PvPDojo.LANG.sendMessage(sender, MessageKeys.PLAYER_NEVER_JOINED);
                return;
            }
            if (!targetPlayer.isOnlineProxy() && additional == null) {
                PvPDojo.LANG.sendMessage(sender, MessageKeys.OFFLINE_REPORT);
                return;
            }

            if (reason != null) {
                Report report;

                if (sender instanceof Player) {
                    User user = User.getUser((Player) sender);

                    if (!user.getCooldownSettings().canReport()) {
                        user.sendMessage(PvPDojo.WARNING + "You can only report every 30 seconds");
                        return;
                    }

                    user.getCooldownSettings().setLastReport(System.currentTimeMillis());
                    user.getCooldownSettings().save(user.getUniqueId());

                    report = new Report(targetPlayer, new DojoOfflinePlayerImpl(user), reason, additional);
                } else {
                    report = new Report(targetPlayer, new DojoOfflinePlayerImpl(DBCommon.getProfile(PvPDojo.SERVER_UUID)), reason, additional);
                }

                report.setId(BungeeSync.getNextId(BungeeSync.REPORT_ID_COUNTER, 10_000_000));

                // Broadcast staff message
                BroadcastUtil.global(ChatChannel.STAFF, new CompBuilder(CC.RED + "Report " + PvPDojo.POINTER + CC.BLUE + targetPlayer.getName() + CC.GRAY
                        + " has been reported for " + CC.RED + reason + (additional != null ? " (" + additional + ")" : "")).command("reportmenu").create());

                if (!PvPDojo.get().isTest()) {
                    if (sender instanceof ConsoleCommandSender) {
                        Discord.text().sendMessage("Report: " + targetPlayer.getName() + " has been reported for " + reason + (additional != null ? " (" + additional + ")" : ""))
                               .complete();
                    }
                }
                // Push report to Redis
                Redis.get().setHValue(BungeeSync.REPORT, report.getId() + "", DBCommon.GSON.toJson(report));

                // Finally tell sender their report was successful
                PvPDojo.LANG.sendMessage(sender, MessageKeys.REPORT_SUCCESS, "{target}", targetPlayer.getName(), "{reason}", reason.toString(),
                        "{additional}", additional != null ? " (" + additional + ")" : "");
            } else {
                if (sender instanceof Player) {
                    PvPDojo.schedule(() -> new ReportMenu((Player) sender, target).open()).sync();
                }
            }
        }).createAsyncTask();

    }

    private class ReportMenu extends SingleInventoryMenu {

        private String target;

        public ReportMenu(Player player, String target) {
            super(player, CC.RED + "Report Reasoning", 9);
            this.target = target;
            setCloseOnClick(true);
        }

        @Override
        public void redraw() {
            InventoryPage inv = getCurrentPage();

            for (int i = 0; i < Report.ReportType.values().length; i++) {
                Report.ReportType reportType = Report.ReportType.values()[i];
                inv.setItem(i, new ItemBuilder(reportType.getMaterial()).name(CC.RED + getEnumName(reportType)).build(),
                        clickType -> {
                            if (reportType == ReportType.OTHER) {
                                new InputGUI(CC.RED + "Give a custom reason", reason -> getPlayer().performCommand("report " + target + " " + reportType + " " + reason))
                                        .open(getPlayer());
                            } else {
                                getPlayer().performCommand("report " + target + " " + reportType);
                            }
                        });
            }
        }

    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
