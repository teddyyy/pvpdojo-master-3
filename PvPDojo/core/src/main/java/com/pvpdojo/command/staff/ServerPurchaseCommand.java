/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Flags;
import co.aikar.commands.annotation.Private;
import com.pvpdojo.Discord;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.abilityinfo.AbilityCollection;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.mysql.SQL;
import com.pvpdojo.mysql.SQLBuilder;
import com.pvpdojo.mysql.Tables;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.StringUtils;
import com.pvpdojo.util.bukkit.CC;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.UUID;

@Private
@CommandPermission("Staff")
@CommandAlias("server-purchase")
public class ServerPurchaseCommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onCommand(CommandSender sender, @Flags("other") Player target, String item) {

        final String uuid = target.getUniqueId().toString();
        PvPDojo.schedule(() -> {
            switch (item) {
                case "greenbelt":
                    setRank(target.getUniqueId(), Rank.GREENBELT, Duration.ofDays(30));

                    addCollection(uuid, AbilityCollection.BRAVERY);
                    addCollection(uuid, AbilityCollection.DIVINITY);
                    addCollection(uuid, AbilityCollection.MILITARY);
                    break;
                case "variety":
                    addCollection(uuid, AbilityCollection.BRAVERY);
                    addCollection(uuid, AbilityCollection.BRAVERY);

                    addCollection(uuid, AbilityCollection.DIVINITY);
                    addCollection(uuid, AbilityCollection.DIVINITY);

                    addCollection(uuid, AbilityCollection.MILITARY);
                    addCollection(uuid, AbilityCollection.MILITARY);

                    addCollection(uuid, AbilityCollection.SAMURAI);
                    addCollection(uuid, AbilityCollection.SAMURAI);

                    addCollection(uuid, AbilityCollection.SORCERER);
                    addCollection(uuid, AbilityCollection.SORCERER);
                    break;
                case "5military":
                    for (int i = 0; i < 5; i++) {
                        addCollection(uuid, AbilityCollection.MILITARY);
                    }
                    break;
                case "5divinity":
                    for (int i = 0; i < 5; i++) {
                        addCollection(uuid, AbilityCollection.DIVINITY);
                    }
                    break;
                case "5bravery":
                    for (int i = 0; i < 5; i++) {
                        addCollection(uuid, AbilityCollection.BRAVERY);
                    }
                    break;
                case "5samurai":
                    for (int i = 0; i < 5; i++) {
                        addCollection(uuid, AbilityCollection.SAMURAI);
                    }
                    break;
                case "5sorcerer":
                    for (int i = 0; i < 5; i++) {
                        addCollection(uuid, AbilityCollection.SORCERER);
                    }
                    break;
                case "5beam":
                    for (int i = 0; i < 5; i++) {
                        addCollection(uuid, AbilityCollection.BEAM);
                    }
                    break;
                case "5disaster":
                    for (int i = 0; i < 5; i++) {
                        addCollection(uuid, AbilityCollection.DISASTER);
                    }
                    break;
                case "5oldschool":
                    for (int i = 0; i < 5; i++) {
                        addCollection(uuid, AbilityCollection.OLDSCHOOL);
                    }
                    break;
                case "5bender":
                    for (int i = 0; i < 5; i++) {
                        addCollection(uuid, AbilityCollection.BENDER);
                    }
                    break;
                case "customslot":
                    User.getUser(target).getPersistentData().incrementInteger("kitslots", 1);
                    break;
                case "bluebelt":
                    setRank(target.getUniqueId(), Rank.BLUEBELT, Duration.ofDays(30));

                    for (int i = 0; i < 2; i++) {
                        addCollection(uuid, AbilityCollection.BRAVERY);
                    }

                    for (int i = 0; i < 2; i++) {
                        addCollection(uuid, AbilityCollection.DIVINITY);
                    }

                    for (int i = 0; i < 2; i++) {
                        addCollection(uuid, AbilityCollection.MILITARY);
                    }

                    for (int i = 0; i < 1; i++) {
                        addCollection(uuid, AbilityCollection.SAMURAI);
                    }

                    for (int i = 0; i < 1; i++) {
                        addCollection(uuid, AbilityCollection.SORCERER);
                    }
                    break;
                case "blackbelt":
                    setRank(target.getUniqueId(), Rank.BLACKBELT, Duration.ofDays(30));

                    for (int i = 0; i < 4; i++) {
                        addCollection(uuid, AbilityCollection.BRAVERY);
                    }

                    for (int i = 0; i < 4; i++) {
                        addCollection(uuid, AbilityCollection.DIVINITY);
                    }

                    for (int i = 0; i < 3; i++) {
                        addCollection(uuid, AbilityCollection.MILITARY);
                    }

                    for (int i = 0; i < 2; i++) {
                        addCollection(uuid, AbilityCollection.SAMURAI);
                    }

                    for (int i = 0; i < 2; i++) {
                        addCollection(uuid, AbilityCollection.SORCERER);
                    }
                    break;
                case "firstdan":
                    setRank(target.getUniqueId(), Rank.FIRST_DAN, null);
                    for (AbilityCollection collection : AbilityCollection.values()) {
                        if (collection != AbilityCollection.NONE) {
                            for (int i = 0; i < 8; i++) {
                                addCollection(uuid, collection);
                            }
                        }
                    }
                    break;
                default:
                    Discord.blamePayback().sendMessage("An exception occured while processing payment of " + target.getName()).complete();
                    throw new IllegalArgumentException("invalid purchase for " + target.getName());
            }
            target.sendMessage(CC.GREEN + "Buycraft purchase has been processed");
            BungeeSync.updateUser(target.getUniqueId());
        }).createAsyncTask();
    }

    public static void setRank(UUID uuid, Rank rank, Duration duration) {
        try (SQLBuilder sql = new SQLBuilder()) {
            Timestamp expireTime = duration != null ? Timestamp.from(Instant.now().plusSeconds(duration.getSeconds())) : Timestamp.valueOf(LocalDateTime.now().plusYears(100));

            sql.update(Tables.USERS, "rank", "rankexpire").where("uuid = ?").executeUpdate(rank.name(), expireTime, uuid.toString());

            if (!PvPDojo.get().isTest()) {
                Discord.capital().sendMessage("Bought " + rank + " for " + PvPDojo.getOfflinePlayer(uuid).getName()
                        + (duration != null ? " for a period of " + StringUtils.getReadableDuration(duration) : "")).complete();
            }
        } catch (SQLException e) {
            Log.exception(rank.name() + " failed for " + uuid, e);
        }
    }

    public static void addCollection(String uuid, AbilityCollection collection) {
        String abilityuUUID = UUID.randomUUID().toString();
        addCollection(uuid, abilityuUUID, collection.getId());
    }

    public static void addCollection(final String uuid, final String abilityUUID, final int ability_id) {
        try {
            SQL.insert(Tables.ABILITIES, "'" + uuid + "', 0, '" + abilityUUID + "', " + ability_id + ", 1");
        } catch (SQLException e) {
            Log.exception("", e);
        }
    }

    @Override
    public Rank getRank() {
        return Rank.SERVER_ADMINISTRATOR;
    }
}
