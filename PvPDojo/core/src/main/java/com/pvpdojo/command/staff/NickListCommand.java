/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import java.util.HashSet;
import java.util.Map;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("nicklist")
public class NickListCommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onNickList(User sender) {
        PvPDojo.schedule(() -> {
            Map<String, String> nickToRealName = Redis.get().getHAll(BungeeSync.NICK_TO_REAL_NAME);

            // Filter perms
            nickToRealName.values().removeIf(real -> {
                DojoOfflinePlayer off = PvPDojo.getOfflinePlayer(real);
                off.getPersistentData().pullUserDataSneaky();
                return !sender.getRank().inheritsRank(off.getPersistentData().getRank());
            });

            for (String fakeLowerCase : new HashSet<>(nickToRealName.keySet())) {
                nickToRealName.put(PvPDojo.getOfflinePlayer(fakeLowerCase).getName(), nickToRealName.remove(fakeLowerCase));
            }

            if (!nickToRealName.isEmpty()) {
                sendNick(sender, "Nick", "Real");
                nickToRealName.forEach((nick, real) -> sendNick(sender, nick, real));
            } else {
                sender.sendMessage(PvPDojo.PREFIX + "There are no nicked players online");
            }
        }).createAsyncTask();
    }

    public void sendNick(User sender, String nick, String real) {
        sender.sendMessage(CC.YELLOW + nick + CC.GRAY + " - " + CC.BLUE + real);
    }

    @Override
    public Rank getRank() {
        return Rank.TRIAL_MODERATOR;
    }
}
