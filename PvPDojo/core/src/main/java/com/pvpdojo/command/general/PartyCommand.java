/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import static com.pvpdojo.util.StringUtils.createComponent;

import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.ActionHandlers;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.party.Party;
import com.pvpdojo.party.PartyList;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.Request;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Conditions;
import co.aikar.commands.annotation.Flags;
import co.aikar.commands.annotation.HelpCommand;
import co.aikar.commands.annotation.Optional;
import co.aikar.commands.annotation.Subcommand;

@CommandAlias("party")
public class PartyCommand extends DojoCommand {

    @HelpCommand
    public void doHelp(User user, CommandHelp help) {
        help.showHelp();
        user.sendMessage(MessageKeys.PARTY_CHAT_HELP);
    }

    @Subcommand("kick")
    @Conditions("hasparty|partyleader")
    @CommandCompletion("@partymembers")
    public void onKick(User user, String partyMember) {
        PvPDojo.schedule(() -> {
            Party target = user.getParty();
            OfflinePlayer player = PvPDojo.getOfflinePlayer(partyMember);
            if (target.getUsers().contains(player.getUniqueId())) {
                if (target.getLeader().equals(player.getUniqueId())) {
                    PvPDojo.schedule(() -> user.getPlayer().performCommand("party leave")).sync();
                } else {
                    PvPDojo.newChain().async(() -> {
                        target.channelMessage(CC.BLUE + player.getName() + CC.GRAY + " has been kicked out of the party by " + CC.BLUE + user.getName());
                    }).delay(5).asyncFirst(() -> { // Delay by 5 ticks to give the message a chance to pass to kicked user
                        PartyList.removePartyUser(target, player.getUniqueId());
                        return player;
                    }).abortIfNot(OfflinePlayer::isOnline).syncLast(online -> { // Update user if online
                        User targetUser = User.getUser(online.getUniqueId());
                        targetUser.setParty(null);
                    }).execute();
                }
            } else {
                user.sendMessage(MessageKeys.NOT_IN_YOUR_PARTY);
            }
        }).createAsyncTask();
    }

    @Conditions("hasparty")
    @Subcommand("info|list|members")
    public void onInfo(User user) {
        PvPDojo.schedule(() -> {
            Party target = user.getParty();

            if (target != null) {
                user.sendMessage(CC.GRAY + "Leader:");
                user.sendMessage(CC.GRAY + "- " + CC.BLUE + PvPDojo.getOfflinePlayer(target.getLeader()).getName());
                user.sendMessage(CC.GRAY + "Members:");
                target.getUsers().stream().map(PvPDojo::getOfflinePlayer).forEach(off -> user.sendMessage(CC.GRAY + "- " + CC.BLUE + off.getName()));
            }
        }).createAsyncTask();
    }

    @Subcommand("leave|quit")
    @Conditions("hasparty")
    public void onLeave(User user) {
        Party party = user.getParty();
        party.channelMessage(CC.BLUE + user.getName() + CC.GRAY + " left the party");
        user.setParty(null);
        user.sendMessage(MessageKeys.PARTY_LEAVE);

        PvPDojo.schedule(() -> PartyList.removePartyUser(party, user.getUUID())).createAsyncTask();
    }

    @Subcommand("create")
    @Conditions("noparty")
    public void onCreate(User user, @Optional @Flags("other") Player instantInvite) {
        Party party = new Party(UUID.randomUUID());
        party.addUser(user.getUniqueId());
        party.setLeader(user.getUniqueId());
        PvPDojo.newChain().async(() -> PartyList.registerParty(party)).sync(() -> {
            user.setParty(party);
            party.channelMessage(CC.GRAY + "Party has been created");
            if (instantInvite != null) {
                user.getPlayer().performCommand("party invite " + instantInvite.getNick());
            }
        }).execute();
    }

    @Subcommand("invite")
    @CommandCompletion("@players")
    public void onInvite(User user, @Flags("other") Player target) {
        if (user.getParty() == null) {
            user.getPlayer().performCommand("party create " + target.getNick());
            return;
        }
        if (!user.getParty().getLeader().equals(user.getUniqueId())) {
            user.sendMessage(MessageKeys.PARTY_LEADER_REQUIRED);
            return;
        }
        User targetUser = User.getUser(target);
        if (targetUser.getParty() != null && targetUser.getParty().isValid()) {
            user.sendMessage(MessageKeys.ALREADY_IN_PARTY_OTHER);
            return;
        }
        String failReason = targetUser.makeRequest(user.getUUID(), new PartyRequest(user.getParty()));
        if (failReason == null) {
            user.sendMessage(MessageKeys.INVITE_PARTY, "{target}", targetUser.getName());
            targetUser.sendMessage(createComponent(PvPDojo.LANG.formatMessage(targetUser, MessageKeys.INVITED_PARTY, "{inviter}", user.getName()), CC.GREEN + "accept",
                    "accept " + user.getUUID().toString()));
        } else {
            user.sendMessage(MessageKeys.WARNING, "{text}", failReason);
        }
    }

    private class PartyRequest extends Request {

        UUID partyId;

        public PartyRequest(Party party) {
            this.partyId = party.getPartyId();
        }

        @Override
        public void accept(User acceptor) {
            PvPDojo.newChain().asyncFirst(() -> PartyList.getPartyById(partyId)).abortIfNull(ActionHandlers.MESSAGE, acceptor.getPlayer(), PvPDojo.WARNING + "Party not found")
                   .sync(party -> {
                       if (acceptor.getParty() != null) {
                           acceptor.sendMessage(MessageKeys.ALREADY_IN_PARTY);
                           return null;
                       }
                       return party;
                   }).abortIfNull().async(party -> {
                PartyList.addPartyUser(party, acceptor.getUniqueId());
                party.channelMessage(CC.BLUE + acceptor.getName() + CC.GRAY + " joined the party");
                return party;
            }).syncLast(acceptor::setParty).execute();
        }

        @Override
        public void deny(User deniedBy) {}

        @Override
        public int getExpirationSeconds() {
            return 60;
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof PartyRequest;
        }

    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
