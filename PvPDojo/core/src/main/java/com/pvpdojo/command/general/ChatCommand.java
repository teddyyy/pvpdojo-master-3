/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Description;

@CommandAlias("chatchannel|channel|chat|ch|c|cc")
public class ChatCommand extends DojoCommand {

    @Description("{@@pvpdojo.cmd-description.chat}")
    @Default
    @CatchUnknown
    @CommandCompletion("@chatchannels")
    public void onChannelChange(User user, @Default("GLOBAL") ChatChannel chatChannel) {
        if (getExecCommandLabel().equals("cc")) {
            chatChannel = ChatChannel.CLAN;
        }
        if (chatChannel.getCondition().apply(user)) {
            user.setChatChannel(chatChannel);
        } else {
            user.sendMessage(PvPDojo.WARNING + "You have no permission to join that chat channel");
        }
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
