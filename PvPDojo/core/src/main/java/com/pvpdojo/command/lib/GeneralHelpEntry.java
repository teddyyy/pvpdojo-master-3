/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.lib;

import co.aikar.commands.BukkitRootCommand;

public class GeneralHelpEntry {
    private final GeneralCommandHelp commandHelp;
    private final BukkitRootCommand command;
    private int searchScore = 1;

    GeneralHelpEntry(GeneralCommandHelp commandHelp, BukkitRootCommand command) {
        this.commandHelp = commandHelp;
        this.command = command;
    }

    BukkitRootCommand getRegisteredCommand() {
        return this.command;
    }

    public String getCommand() {
        return this.command.getCommandName();
    }

    public String getCommandPrefix() {
        return this.commandHelp.getCommandPrefix();
    }

    public String getDescription() {
        return command.getDefaultRegisteredCommand() != null ? command.getDefaultRegisteredCommand().getHelpText() : command.getDescription();
    }

    public void setSearchScore(int searchScore) {
        this.searchScore = searchScore;
    }

    public boolean shouldShow() {
        return this.searchScore > 0;
    }

    public int getSearchScore() {
        return searchScore;
    }

    public String getSearchTags() {
        return command.getDefaultRegisteredCommand().helpSearchTags;
    }

}
