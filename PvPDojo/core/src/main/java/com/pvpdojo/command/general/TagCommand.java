/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Optional;

@CommandAlias("tag|togglerank")
public class TagCommand extends DojoCommand {

    @CatchUnknown
    @Default
    @CommandCompletion("@ranks")
    public void onTag(User user, @Optional Rank rank) {

        if (user.getPlayer().isNicked() && !hasRank(user.getPlayer(), Rank.MODERATOR)) {
            user.sendMessage(PvPDojo.WARNING + "You cannot use this command while nicked");
            return;
        }

        if (rank == null) {
            user.setSpoofedRank(null);
            user.updateForEveryOne();
            user.updateTablist();
            user.sendMessage(PvPDojo.PREFIX + "You reset your tag");
        } else if (user.getRank().inheritsRank(rank)) {
            user.setSpoofedRank(rank);
            user.updateForEveryOne();
            user.updateTablist();
            user.sendMessage(PvPDojo.PREFIX + "You are now tagged as " + rank.getPrefix() + rank.name());
        } else {
            user.sendMessage(PvPDojo.WARNING + "Provided rank is higher than your actual rank");
        }
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
