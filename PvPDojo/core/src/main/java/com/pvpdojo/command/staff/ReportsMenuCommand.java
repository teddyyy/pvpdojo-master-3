/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import org.bukkit.entity.Player;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.menus.ReportMenu;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;

@CommandPermission("staff")
@CommandAlias("reports|reportmenu")
public class ReportsMenuCommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onReportMenu(Player player) {
        new ReportMenu(player).open();
    }

    @Override
    public Rank getRank() {
        return Rank.HELPER;
    }
}
