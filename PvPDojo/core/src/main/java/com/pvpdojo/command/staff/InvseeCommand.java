/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Conditions;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Flags;

@Conditions("adminmode")
@CommandPermission("Staff")
@CommandAlias("invsee|inv")
public class InvseeCommand extends DojoCommand {

    public static final Set<UUID> VIEWING = new HashSet<>();

    @Default
    @CatchUnknown
    @CommandCompletion("@players")
    public void onCommand(Player player, @Flags("other") Player target) {
        player.openInventory(target.getInventory());
        player.sendMessage(PvPDojo.PREFIX + "Viewing inventory of " + CC.BLUE + target.getName());
        VIEWING.add(player.getUniqueId());
    }

    @Override
    public Rank getRank() {
        return Rank.HELPER;
    }
}
