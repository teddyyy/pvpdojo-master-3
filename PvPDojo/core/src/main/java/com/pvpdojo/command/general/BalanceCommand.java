/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import java.sql.SQLException;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.userdata.stats.PersistentHGStats;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.Log;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Description;
import co.aikar.commands.annotation.Subcommand;

@CommandAlias("balance|bal|money|coins|credits")
public class BalanceCommand extends DojoCommand {

    @Description("{@@pvpdojo.cmd-description.balance}")
    @Default
    @CatchUnknown
    public void onCommand(User user) {
        user.sendMessage(MessageKeys.BALANCE, "{money}", String.valueOf(user.getPersistentData().getMoney()));
    }

    @Subcommand("hg")
    public void onHGBalance(User user) {
        PvPDojo.schedule(() -> {
            PersistentHGStats stats = new PersistentHGStats(user.getUniqueId());
            try {
                stats.load();
            } catch (SQLException e) {
                Log.exception(e);
                user.sendMessage(CC.RED + "An error occurred");
                return;
            }
            user.sendMessage(MessageKeys.BALANCE, "{money}", String.valueOf(stats.getMoney()));
        }).createAsyncTask();
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
