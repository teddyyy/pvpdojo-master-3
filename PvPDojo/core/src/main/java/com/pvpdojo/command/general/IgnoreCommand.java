/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Single;
import co.aikar.commands.annotation.Subcommand;

@CommandAlias("ignore")
public class IgnoreCommand extends DojoCommand {

    @Default
    @CatchUnknown
    @CommandCompletion("@allplayers")
    public void onIgnore(User user, @Single String target) {
        PvPDojo.schedule(() -> {
            DojoOfflinePlayer player = PvPDojo.getOfflinePlayer(target);
            if (player.hasPlayedBefore()) {
                if (user.getSettings().getIgnored().contains(player.getUniqueId())) {
                    user.getSettings().getIgnored().remove(player.getUniqueId());
                    user.sendMessage(PvPDojo.PREFIX + "You are no longer ignoring " + CC.GREEN + player.getName() + CC.GRAY + "'s requests/msgs");
                } else {
                    user.getSettings().getIgnored().add(player.getUniqueId());
                    user.sendMessage(PvPDojo.PREFIX + "You are now ignoring " + CC.GREEN + player.getName() + CC.GRAY + "'s requests/msgs");
                }
                user.getSettings().save(user.getUUID());
            } else {
                user.sendMessage(MessageKeys.PLAYER_NEVER_JOINED);
            }
        }).createAsyncTask();
    }

    @Subcommand("list")
    public void onIgnoreList(User user) {
        PvPDojo.schedule(() -> user.getSettings().getIgnored().stream()
                                   .map(PvPDojo::getOfflinePlayer)
                                   .forEach(ignored -> user.sendMessage(CC.RED + "- " + CC.BLUE + ignored.getName()))
        ).createAsyncTask();
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
