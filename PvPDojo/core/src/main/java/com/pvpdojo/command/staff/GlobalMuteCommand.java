/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.BroadcastUtil;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;

@CommandPermission("Staff")
@CommandAlias("globalmute|togglechat|tc")
public class GlobalMuteCommand extends DojoCommand {

    @Default
    public void onGlobalMute(Player player) {
        PvPDojo.schedule(() -> {
            BungeeSync.setGlobalMute(!BungeeSync.isGlobalMute());
            BroadcastUtil.global(ChatChannel.GLOBAL,PvPDojo.PREFIX + CC.BLUE + "Global mute " + CC.GRAY + " has been " + (BungeeSync.isGlobalMute() ? CC.GREEN + "enabled" : CC.RED + "disabled"));
        }).createAsyncTask();
    }

    @Override
    public Rank getRank() {
        return Rank.TRIAL_MODERATOR;
    }

}
