/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;

import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("fix")
public class FixCommand extends DojoCommand {

    @Default
    public void onCommand(User user) {
        NMSUtils.get(user.getPlayer()).retrack();
        user.sendOverlay(MessageKeys.FIX);
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
