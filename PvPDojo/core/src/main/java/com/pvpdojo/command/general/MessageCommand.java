/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import static com.pvpdojo.util.StringUtils.appendAll;

import java.sql.SQLException;
import java.util.Arrays;

import com.pvpdojo.DBCommon;
import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.PersistentData;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.PunishmentManager;
import com.pvpdojo.util.StringUtils;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import org.bukkit.ChatColor;

@CommandAlias("message|whisper|tell|msg|pm|w|m")
public class MessageCommand extends DojoCommand {

    @Default
    @CatchUnknown
    @CommandCompletion("@allplayers")
    public void onMessage(User user, String target, String words) {
        PvPDojo.schedule(() -> {

            if (BungeeSync.isGlobalMute() && !user.getRank().inheritsRank(Rank.TRIAL_MODERATOR)) {
                user.sendMessage(PvPDojo.WARNING + "The global chat is currently muted");
                return;
            }

            if (BungeeSync.getOnlinePlayers().stream().noneMatch(online -> online.equalsIgnoreCase(target))) {
                user.sendMessage(MessageKeys.COULD_NOT_FIND_PLAYER);
                return;
            }
            DojoOfflinePlayer offlinePlayer = PvPDojo.getOfflinePlayer(target);
            PersistentData receiverData = new PersistentData(offlinePlayer.getUniqueId());
            try {
                receiverData.pullUserData();
            } catch (SQLException e) {
                Log.exception(e);
            }
            if (!user.getSettings().getPrivateMessages().test(user, receiverData)) {
                user.sendMessage(PvPDojo.WARNING + "You have your pm's disabled! Enable them in /settings");
                return;
            }
            if (PunishmentManager.handleMutedPlayer(user.getPlayer())) {
                return;
            }

            // Chat Log
            DBCommon.logChatMessage(user.getUUID(), Arrays.asList(user.getUUID(), offlinePlayer.getUniqueId()), "[PM] -> [" + offlinePlayer.getName() + "] " + words);

            Redis.get().setValue(BungeeSync.MSG_REPLY + user.getUniqueId().toString(), target, 600);
            Redis.get().publish(BungeeSync.MSG, appendAll("|", "" + BungeeSync.REQUEST, user.getUniqueId().toString(), user.getSpoofedRank().getPrefix()
                    + user.getName(), target, StringUtils.toBase64(ChatColor.GREEN + words)));
        }).createAsyncTask();
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
