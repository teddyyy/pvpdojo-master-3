/*
 * Copyright (c) 2019 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.pvpdojo.bukkit.util.NMSUtils;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import net.minecraft.server.v1_7_R4.EntityPlayer;
import net.minecraft.server.v1_7_R4.EntityTrackerEntry;
import net.minecraft.server.v1_7_R4.WorldServer;

@CommandAlias("test")
public class TestCommand extends DojoCommand
{
    @Default
    @CatchUnknown
    public void onTest(CommandSender sender, int i) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            EntityPlayer ep = NMSUtils.get(player);
            EntityTrackerEntry entry = (EntityTrackerEntry) ((WorldServer)ep.getWorld()).getTracker().trackedEntities.get(ep.getId());
            entry.c = i;
            sender.sendMessage(String.valueOf(i));
        }
    }

    @Override
    public Rank getRank() {
        return Rank.OWNER;
    }
}
