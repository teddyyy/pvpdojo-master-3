/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.menus.PunishmentsMenu;
import com.pvpdojo.userdata.DojoOfflinePlayer;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.PunishmentManager;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Single;

@CommandPermission("Staff")
@CommandAlias("unban|pardon|uban")
public class PardonCommand extends DojoCommand {

    @Default
    @CatchUnknown
    @CommandCompletion("@bannedplayers")
    public void onCommand(CommandSender sender, @Single String target) {
        PvPDojo.schedule(() -> {
            DojoOfflinePlayer offplayer = PvPDojo.getOfflinePlayer(target);
            if (!offplayer.isBanned()) {
                sender.sendMessage(PvPDojo.WARNING + CC.RED + offplayer.getName() + " is not banned");
                return;
            }

            if (sender instanceof Player) {
                PvPDojo.schedule(() -> new PunishmentsMenu((Player) sender, offplayer).open()).sync();
            } else {
                offplayer.getCurrentBan().setValid(false);
                offplayer.getCurrentBan().setUpdateExecutor(sender.getName());
                offplayer.getCurrentBan().setUpdateReason("Legacy unban");
                PunishmentManager.updatePunishment(offplayer.getCurrentBan());
                sender.sendMessage(PvPDojo.PREFIX + CC.GREEN + offplayer.getName() + " has been unbanned");
            }
        }).createAsyncTask();
    }

    @Override
    public Rank getRank() {
        return Rank.TRIAL_MODERATOR;
    }
}
