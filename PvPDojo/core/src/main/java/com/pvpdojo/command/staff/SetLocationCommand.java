/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Private;

@Private // Legacy
@CommandPermission("Staff")
@CommandAlias("setlocation|sl")
public class SetLocationCommand extends DojoCommand {

    @CatchUnknown
    public void onCommand(Player player, String name) {
        Location loc = player.getLocation();
        PvPDojo.get().getConfig().set(name + ".x", loc.getX());
        PvPDojo.get().getConfig().set(name + ".y", loc.getY());
        PvPDojo.get().getConfig().set(name + ".z", loc.getZ());
        PvPDojo.get().getConfig().set(name + ".yaw", loc.getYaw());
        PvPDojo.get().getConfig().set(name + ".pitch", loc.getPitch());
        PvPDojo.get().saveConfig();
        BukkitUtil.reloadLocations();
        player.sendMessage(PvPDojo.PREFIX + "Successfully set location " + CC.GREEN + name);
    }

    @Override
    public Rank getRank() {
        return Rank.SERVER_ADMINISTRATOR;
    }

}
