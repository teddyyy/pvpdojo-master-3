/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import org.bukkit.Bukkit;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("hide")
public class HideCommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onHide(User user) {
        user.setShowSpectators(!user.isShowSpectators());
        if (user.isShowSpectators()) {
            user.sendMessage(MessageKeys.SPEC_SHOWN);
        } else {
            user.sendMessage(MessageKeys.SPEC_HIDDEN);
        }
        Bukkit.getOnlinePlayers().stream().map(User::getUser).forEach(online -> online.updateUserFor(user));
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
