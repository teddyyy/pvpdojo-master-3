/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;

@CommandPermission("Staff")
@CommandAlias("build|b")
public class BuildCommand extends DojoCommand {

    @Default
    public void onBuild(User user) {
        user.setBuild(!user.canBuild());
        user.sendMessage(PvPDojo.PREFIX + "Build mode: " + (user.canBuild() ? CC.GREEN + "enabled" : CC.RED + "disabled"));
    }

    @Override
    public Rank getRank() {
        return Rank.MODERATOR_PLUS;
    }

}
