/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.bukkit.events.AdminModeEvent;
import com.pvpdojo.bukkit.util.BukkitUtil;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.gui.HotbarGUI;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.userdata.AdminModeHotbar;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import lombok.Data;

@CommandPermission("Staff")
@CommandAlias("admin|staff")
public class AdminCommand extends DojoCommand {

    public static final Map<UUID, AdminModeData> DATA_SAVE = new HashMap<>();


    @Default
    public void onCommand(User user) {
        user.setAdminMode(!user.isAdminMode());
    }

    @Override
    public Rank getRank() {
        return Rank.HELPER;
    }

    public static void enableAdminMode(Player p) {

        Bukkit.getPluginManager().callEvent(new AdminModeEvent(p, true));

        User pd = User.getUser(p);

        DATA_SAVE.put(p.getUniqueId(), new AdminModeData(p.getInventory().getContents(), p.getInventory().getArmorContents(), pd.getHotbarGui(), pd.getChatChannel()));

        p.getInventory().setArmorContents(null);
        p.getInventory().clear();

        pd.setChatChannel(ChatChannel.STAFF);

        pd.setSpectator(false);
        p.setAllowFlight(true);
        p.setFlying(true);

        pd.setHotbarGui(AdminModeHotbar.get());

        pd.setVanish(true);
        p.setGameMode(GameMode.CREATIVE);

        p.sendMessage(PvPDojo.PREFIX + "You are now in " + CC.RED + "ADMIN" + CC.GRAY + " mode");

        BukkitUtil.getWorldEdit().getSession(p).setToolControl(true);

    }

    public static void disableAdminMode(Player p) {

        User pd = User.getUser(p);

        if (pd.canBuild()) {
            p.performCommand("build");
        }

        p.setAllowFlight(false);
        p.setGameMode(GameMode.SURVIVAL);

        AdminModeData data = DATA_SAVE.remove(p.getUniqueId());

        pd.setHotbarGui(data.getHotbarGUI());
        p.getInventory().setContents(data.getInventory());
        p.getInventory().setArmorContents(data.getArmor());

        pd.setVanish(false);
        pd.setChatChannel(data.getChatChannel());

        p.sendMessage(PvPDojo.PREFIX + "You are now in " + CC.GREEN + "PLAYER" + CC.GRAY + " mode");

        BukkitUtil.getWorldEdit().getSession(p).setToolControl(false);

        Bukkit.getPluginManager().callEvent(new AdminModeEvent(p, false));

    }

    @Data
    public static class AdminModeData {
        private final ItemStack[] inventory, armor;
        private final HotbarGUI hotbarGUI;
        private final ChatChannel chatChannel;
    }

}
