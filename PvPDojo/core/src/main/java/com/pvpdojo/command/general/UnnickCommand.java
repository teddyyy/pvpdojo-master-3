/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.Redis;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("unnick|unick")
public class UnnickCommand extends DojoCommand {

    @Default
    @CatchUnknown
    public void onUnnick(User user) {
        if (user.getPlayer().isNicked()) {
            if (PvPDojo.SERVER_TYPE != ServerType.HUB && !hasRank(user.getPlayer(), Rank.MODERATOR)) {
                user.sendMessage(PvPDojo.WARNING + "You may only use this command in hub");
            } else {
                String nick = user.getPlayer().getNick();
                user.getPlayer().resetNameAndSkin();
                user.setSpoofedRank(null);
                user.updateTablist();
                user.updateForEveryOne();
                PvPDojo.schedule(() -> {
                    Redis.get().removeHValue(BungeeSync.NICK_TO_REAL_NAME, nick.toLowerCase());
                    Redis.get().delete(BungeeSync.NICK_TO_REAL_NAME + "_" + user.getPlayer().getName());
                }).createAsyncTask();
                user.sendMessage(PvPDojo.PREFIX + "Your nick has been removed");
            }
        } else {
            user.sendMessage(PvPDojo.WARNING + "You are not nicked");
        }
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
