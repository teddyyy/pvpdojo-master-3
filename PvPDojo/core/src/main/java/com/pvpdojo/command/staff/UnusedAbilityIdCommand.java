/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import java.util.stream.IntStream;

import org.bukkit.entity.Player;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;

@CommandPermission("Admin")
@CommandAlias("unusedabilityid")
public class UnusedAbilityIdCommand extends DojoCommand {

    @Default
    public void onUnusedAbilityId(Player player) {
        IntStream.range(0, 200).filter(id -> AbilityLoader.getAbilityData(id) == null).limit(10).forEach(id -> player.sendMessage("" + id));
    }

    @Override
    public Rank getRank() {
        return Rank.SERVER_ADMINISTRATOR;
    }

}
