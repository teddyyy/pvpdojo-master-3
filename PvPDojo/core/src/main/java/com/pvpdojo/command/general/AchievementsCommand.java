/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.ServerType;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.menus.AchievementMenu;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Description;

@CommandAlias("achievements|achiev|ach|a")
public class AchievementsCommand extends DojoCommand {

    @Description("{@@pvpdojo.cmd-description.achievements}")
    @Default
    @CatchUnknown
    public void onAchievements(Player player) {
        new AchievementMenu(player).open();
    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }
}
