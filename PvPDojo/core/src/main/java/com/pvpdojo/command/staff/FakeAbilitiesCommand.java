/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.dataloader.AbilityLoader;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.CC;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Optional;

@CommandAlias("fakeabilities")
public class FakeAbilitiesCommand extends DojoCommand {

    @CatchUnknown
    @Default
    public void onFakeAbilities(User user, @Optional Integer id) {
        if (id == null) {
            user.setFakeAbilities(!user.hasFakeAbilities());
            user.sendMessage(CC.GRAY + "Fake Abilities" + PvPDojo.POINTER + (user.hasFakeAbilities() ? CC.GREEN + "enabled" : CC.RED + "disabled"));
        } else {
            user.sendMessage(AbilityLoader.getAbilityData(id).getDisplayName());
        }
    }

    @Override
    public Rank getRank() {
        return Rank.YOUTUBER;
    }
}
