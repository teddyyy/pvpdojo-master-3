/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.general;

import org.bukkit.command.CommandSender;

import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.command.lib.DojoCommandManager;
import com.pvpdojo.command.lib.GeneralCommandHelp;
import com.pvpdojo.userdata.Rank;

import co.aikar.commands.ACFUtil;
import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Description;
import co.aikar.commands.annotation.Optional;

@CommandAlias("help|h|?")
public class HelpCommand extends DojoCommand {

    @Description("Shows the help menu")
    @Default
    @CatchUnknown
    public void onHelp(CommandSender sender, @Optional String command, @Optional Integer page) {

        int pageNumber = command != null && ACFUtil.isInteger(command) ? Integer.valueOf(command) : page != null ? page : 1;
        String search = command != null && !ACFUtil.isInteger(command) ? command : null;

        GeneralCommandHelp help = new GeneralCommandHelp(DojoCommandManager.get(), getCurrentCommandIssuer());

        if (search != null) {
            help.setSearch(search);
        }

        help.setPage(pageNumber);

        help.showHelp();

    }

    @Override
    public Rank getRank() {
        return Rank.DEFAULT;
    }

}
