/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import static com.pvpdojo.util.StringUtils.formatMillisDiff;

import java.time.Duration;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.gui.ConfirmInputGUI;
import com.pvpdojo.menu.InventoryPage;
import com.pvpdojo.menu.SingleInventoryMenu;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.PunishmentManager;
import com.pvpdojo.util.PunishmentManager.PunishmentType;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

import co.aikar.commands.CommandManager;
import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Optional;
import co.aikar.commands.annotation.Split;

@CommandPermission("Staff")
@CommandAlias("ban|tempban|tban")
public class BanCommand extends DojoCommand {

    public static Map<String, TimeUnit> UNIT_SHORTCUTS = new HashMap<>();

    static {
        UNIT_SHORTCUTS.put("s", TimeUnit.SECONDS);
        UNIT_SHORTCUTS.put("m", TimeUnit.MINUTES);
        UNIT_SHORTCUTS.put("h", TimeUnit.HOURS);
        UNIT_SHORTCUTS.put("d", TimeUnit.DAYS);
    }

    @Default
    @CatchUnknown
    @CommandCompletion("@allplayers")
    public void onBan(CommandSender sender, @Split String[] targets, @Optional Duration dur, @Optional String reason) {
        if (reason == null) {
            if (sender instanceof Player) {
                new BanMenu((Player) sender, targets).open();
            } else {
                showSyntax(getCurrentCommandIssuer(), CommandManager.getCurrentCommandOperationContext().getRegisteredCommand());
            }
            return;
        }

        if (sender instanceof Player && User.getUser((Player) sender).getRank() == Rank.HELPER) {
            if (dur == null || dur.toMillis() > Duration.ofHours(2).toMillis()) {
                sender.sendMessage(PvPDojo.WARNING + "You can only ban player for a maximum of 2 hours");
                return;
            }
        }

        long time = dur != null ? dur.getSeconds() : -1;
        boolean softban = reason.equalsIgnoreCase("cheating") || reason.equalsIgnoreCase("[AntiSkid] Cheat Detection");

        for (String target : targets) {
            PvPDojo.schedule(() -> PunishmentManager.punish(sender, target, reason, time, softban ? PunishmentType.SOFT_BAN : PunishmentType.BAN)).createAsyncTask();
        }
    }

    private class BanMenu extends SingleInventoryMenu {

        private String[] targets;

        public BanMenu(Player player, String[] targets) {
            super(player, "Ban Menu", 27);
            this.targets = targets;
        }

        @Override
        public void redraw() {
            InventoryPage inv = getCurrentPage();

            inv.setItem(2, new ItemBuilder(Material.LADDER).name(CC.RED + "Racism").build(), clickType -> confirm("Racism", Duration.ofDays(30)));
            inv.setItem(6, new ItemBuilder(Material.FEATHER).name(CC.RED + "Combat Logging").build(), clickType -> confirm("Combat Logging", Duration.ofHours(2)));
            inv.setItem(9, new ItemBuilder(Material.VINE).name(CC.RED + "Report Abuse").build(), clickType -> confirm("Report Abuse", Duration.ofHours(3)));
            inv.setItem(11, new ItemBuilder(Material.IRON_SWORD).name(CC.RED + "Cheating").build(), clickType -> confirm("Cheating", null));
            inv.setItem(13, new ItemBuilder(Material.SPECKLED_MELON).name(CC.RED + "Elo Manipulation").lore("", CC.YELLOW + "This action will NOT reset ELO").build(), clickType -> confirm("Elo Manipulation", Duration.ofDays(7)));
            inv.setItem(15, new ItemBuilder(Material.EXPLOSIVE_MINECART).name(CC.RED + "Advertising").build(), clickType -> confirm("Advertising", null));
            inv.setItem(17, new ItemBuilder(Material.WOOD_DOOR).name(CC.RED + "Inappropriate Skin").build(), clickType -> confirm("Inappropriate Skin (Appeal on Skin Change)", Duration.ofDays(30)));
            inv.setItem(18, new ItemBuilder(Material.LONG_GRASS).data((byte) 2).name(CC.RED + "Teaming in Classic").build(), clickType -> confirm("Teaming in Classic", Duration.ofHours(3)));
            inv.setItem(20, new ItemBuilder(Material.COMMAND_MINECART).name(CC.RED + "DDoS Threats").build(), clickType -> confirm("DDoS Threats", null));
            inv.setItem(22, new ItemBuilder(Material.ENDER_PORTAL_FRAME).name(CC.RED + "Bug Abuse").build(), clickType -> confirm("Bug Abuse", Duration.ofDays(30)));
            inv.setItem(24, new ItemBuilder(Material.WOOD_HOE).name(CC.RED + "Selling sth. for IRL money").build(), clickType -> confirm("Offering exchanges involving real money", null));
            inv.setItem(26, new ItemBuilder(Material.IRON_DOOR).name(CC.RED + "Inappropriate Name").build(), clickType -> confirm("Inappropriate Name (Appeal on Name Change)", Duration.ofDays(30)));

            inv.fillBlank();
        }

        private void confirm(String reason, Duration duration) {
            new ConfirmInputGUI(getPlayer(), "Ban " + Arrays.toString(targets) + " for " + reason + "(" + (duration != null ? formatMillisDiff(duration.getSeconds() * 1000) : "permanent" + ")"), () -> onBan(getPlayer(), targets, duration, reason)).open();
        }

    }

    @Override
    public Rank getRank() {
        return Rank.HELPER;
    }
}