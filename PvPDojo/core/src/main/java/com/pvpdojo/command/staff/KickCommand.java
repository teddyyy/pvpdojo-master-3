/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo.command.staff;

import java.sql.SQLException;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.command.lib.DojoCommand;
import com.pvpdojo.lang.MessageKeys;
import com.pvpdojo.redis.ChatChannel;
import com.pvpdojo.userdata.Rank;
import com.pvpdojo.util.BroadcastUtil;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.Log;
import com.pvpdojo.util.PunishmentManager;
import com.pvpdojo.util.PunishmentManager.PunishmentType;

import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Flags;

@CommandPermission("Staff")
@CommandAlias("kick")
public class KickCommand extends DojoCommand {

    @Default
    @CatchUnknown
    @CommandCompletion("@players")
    public void onCommand(CommandSender sender, @Flags("other") Player[] targets, String reason) {
        for (Player target : targets) {
            target.kickPlayer(CC.RED + "Kicked from the server by a staff member\nReason: " + reason);
            PvPDojo.schedule(() -> {
                try {
                    PunishmentManager.addPunishment(target.getUniqueId(), sender.getName(), reason, 0, PunishmentType.KICK);
                    BroadcastUtil.global(ChatChannel.GLOBAL, MessageKeys.SERVER_KICK, "{target}", target.getName(), "{reason}", reason);
                } catch (SQLException e) {
                    sender.sendMessage(PvPDojo.WARNING + "Could not log punishment: " + e.getMessage());
                    Log.exception("Log kick", e);
                }
            }).createAsyncTask();
        }
    }

    @Override
    public Rank getRank() {
        return Rank.HELPER;
    }
}