/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo;

import static com.pvpdojo.util.StringUtils.getEnumName;

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.SpawnEgg;

import com.pvpdojo.redis.BungeeSync;
import com.pvpdojo.redis.ServerInfo;
import com.pvpdojo.session.SessionType;
import com.pvpdojo.session.network.SessionInfo;
import com.pvpdojo.util.bukkit.CC;
import com.pvpdojo.util.bukkit.ItemBuilder;

public enum Gamemode {

    FFA(new ItemBuilder(Material.ENDER_PEARL).name(CC.DARK_AQUA + "FFA").build()),
    DEATHMATCH(new ItemBuilder(Material.IRON_CHESTPLATE).name(CC.RED + "Deathmatch").build()),
    HG(new ItemBuilder(Material.MUSHROOM_SOUP).name(CC.GOLD + "HG").build()),
    CHALLENGES(new ItemBuilder(Material.LAVA_BUCKET).name(CC.YELLOW + "Challenges").build()),
    CLASSIC(new ItemBuilder(Material.IRON_SWORD).name(CC.DARK_AQUA + "Classic").build()),
    CTM(new ItemBuilder(new SpawnEgg(EntityType.MUSHROOM_COW).toItemStack(1)).name(CC.RED + "CTM").build()),
    LMS(new ItemBuilder(Material.DIAMOND_SWORD).name(CC.GREEN + "LMS").build());

    private ItemStack icon;
    private int playerCount;

    Gamemode(ItemStack icon) {
        this.icon = icon;
    }

    public String getName() {
        return getEnumName(this);
    }

    public ItemStack getIcon() {
        return icon.clone();
    }

    public int getPlayerCount() {
        return playerCount;
    }

    public void setPlayerCount(int playerCount) {
        this.playerCount = playerCount;
    }

    public static void acceptServerInfos(List<ServerInfo> servers) {
        servers.removeIf(serverInfo -> BungeeSync.OFFLINE.test(serverInfo.getPlayers()));

        Stream.of(values()).forEach(gm -> {
            // Special impl for HG player count due to queue system
            if (gm == Gamemode.HG) {
                gm.playerCount = servers.stream()
                                        .filter(gm.getServerFilter())
                                        .map(ServerInfo::getSessionInfo)
                                        .filter(Objects::nonNull)
                                        .mapToInt(SessionInfo::getSessionPlayerCount)
                                        .sum();
            } else {
                gm.playerCount = servers.stream()
                                        .filter(gm.getServerFilter())
                                        .mapToInt(ServerInfo::getPlayers)
                                        .sum();
            }
        });

    }

    private Predicate<ServerInfo> getServerFilter() {

        switch (this) {
            case HG:
                return info -> info.getName().startsWith("game") && info.getSessionInfo() != null && info.getSessionInfo().getSessionType() == SessionType.HG;
            case CTM:
                return info -> info.getName().startsWith("game") && info.getSessionInfo() != null && info.getSessionInfo().getSessionType() == SessionType.C_T_M;
            default:
                return info -> info.getName().startsWith(getName().toLowerCase());
        }
    }

}
