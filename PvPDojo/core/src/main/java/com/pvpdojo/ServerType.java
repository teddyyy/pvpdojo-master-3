/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package com.pvpdojo;

import org.bukkit.entity.Player;

import com.pvpdojo.settings.ServerSwitchSettings;
import com.pvpdojo.userdata.User;
import com.pvpdojo.util.bukkit.PlayerUtil;

public enum ServerType {
    NONE("none"),
    HUB("Hub"),
    FFA("FFA"),
    CLASSIC("Classic"),
    DEATHMATCH("Deathmatch"),
    CHALLENGES("Challenges"),
    GAME("Game"),
    FALLBACK("Fallback");

    private String name;

    ServerType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void connect(Player player) {
        connect(player, new ServerSwitchSettings(User.getUser(player)));
    }

    public void connect(Player player, ServerSwitchSettings settings) {
        if (player.isOnline() && PvPDojo.SERVER_TYPE != this) {
            PlayerUtil.sendToServer(player, this, settings);
        }
    }
}
