/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package eu.the5zig.mod.server.util.protocol;

import java.io.IOException;

import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_7_R4.PacketDataSerializer;
import net.minecraft.server.v1_7_R4.PacketPlayOutCustomPayload;
import net.minecraft.util.io.netty.buffer.Unpooled;

public class BufferUtilsImpl implements IBufferUtils {

    @Override
    public IPacketBuffer createBuffer() {
        return new PacketBuffer();
    }

    @Override
    public IPacketBuffer createBuffer(Protocol.PayloadType payloadType) {
        return new PacketBuffer(payloadType);
    }

    private class PacketBuffer implements IPacketBuffer {

        private PacketDataSerializer packetDataSerializer;

        public PacketBuffer() {
            this.packetDataSerializer = new PacketDataSerializer(Unpooled.buffer());
        }

        public PacketBuffer(Protocol.PayloadType payloadType) {
            this();
            writeInt(payloadType.ordinal());
        }

        @Override
        public void writeString(String string) {
            try {
                packetDataSerializer.a(string);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void writeInt(int i) {
            packetDataSerializer.writeInt(i);
        }

        @Override
        public void writeLong(long l) {
            packetDataSerializer.writeLong(l);
        }

        @Override
        public void send(Player player, String channel) {
            PacketPlayOutCustomPayload packet = new PacketPlayOutCustomPayload(channel, packetDataSerializer);
            ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
        }
    }

}
