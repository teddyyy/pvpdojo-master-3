/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package eu.the5zig.mod.server.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.google.common.base.Charsets;

import net.minecraft.util.io.netty.buffer.ByteBuf;
import net.minecraft.util.io.netty.buffer.ByteBufOutputStream;
import net.minecraft.util.io.netty.buffer.Unpooled;
import net.minecraft.util.io.netty.handler.codec.base64.Base64;
import net.minecraft.util.org.apache.commons.io.IOUtils;

public class Utils {

    public static String getBase64(BufferedImage bufferedImage) throws IOException {
        // Converting Image byte array into Base64 String
        ByteBuf localByteBuf1 = Unpooled.buffer();
        ImageIO.write(bufferedImage, "PNG", new ByteBufOutputStream(localByteBuf1));
        ByteBuf localByteBuf2 = Base64.encode(localByteBuf1);
        return localByteBuf2.toString(Charsets.UTF_8);
    }

    /**
     * Checks the size of a BufferedImage.
     *
     * @param image   The BufferedImage.
     * @param maxSize The maximum allowed size of the Image.
     */
    public static void checkImageSize(BufferedImage image, long maxSize) {
        ByteArrayOutputStream tmp = new ByteArrayOutputStream();
        try {
            ImageIO.write(image, "png", tmp);
            if (tmp.size() > maxSize) {
                throw new IllegalStateException("Image size may not be larger than " + maxSize + "!");
            }
        } catch (IOException ignored) {
        } finally {
            IOUtils.closeQuietly(tmp);
        }
    }

}