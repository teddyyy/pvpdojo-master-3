/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package eu.the5zig.mod.server.listener;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.google.gson.JsonObject;
import com.pvpdojo.bukkit.util.NMSUtils;

import eu.the5zig.mod.server.api.events.The5zigModUserJoinEvent;
import net.minecraft.server.v1_7_R4.PacketPlayOutCustomPayload;
import net.minecraft.util.io.netty.buffer.ByteBuf;
import net.minecraft.util.io.netty.buffer.Unpooled;
import net.minecraft.util.io.netty.handler.codec.EncoderException;

public class The5zigJoinListener implements Listener {

    public static final Map<Permission, Boolean> SETTINGS = new HashMap<>();

    static {
        for (Permission permission : Permission.values()) {
            SETTINGS.put(permission, permission.isDefaultEnabled());
        }
        SETTINGS.put(Permission.IMPROVED_LAVA, true);
        SETTINGS.put(Permission.CROSSHAIR_SYNC, true);
        SETTINGS.put(Permission.TAGS, false);
    }

    @EventHandler
    public void onModUserJoin(The5zigModUserJoinEvent e) {
        e.getModUser().getStatsManager().setDisplayName("PvPDojo");
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        NMSUtils.get(e.getPlayer()).playerConnection.sendPacket(new PacketPlayOutCustomPayload("LMC", getBytesToSend(SETTINGS)));
        NMSUtils.get(e.getPlayer()).playerConnection.sendPacket(new PacketPlayOutCustomPayload("DAMAGEINDICATOR", new byte[] { 0 }));
    }

    public byte[] getBytesToSend(Map<Permission, Boolean> permissions) {
        // Creating a json object we will put the permissions in
        JsonObject object = new JsonObject();

        // Adding the permissions to the json object
        for (Map.Entry<Permission, Boolean> permissionEntry : permissions.entrySet()) {
            object.addProperty(permissionEntry.getKey().name(), permissionEntry.getValue());
        }

        // Returning the byte array
        return getBytesToSend("PERMISSIONS", object.toString());
    }

    public byte[] getBytesToSend(String messageKey, String messageContents) {
        // Getting an empty buffer
        ByteBuf byteBuf = Unpooled.buffer();

        // Writing the message-key to the buffer
        writeString(byteBuf, messageKey);

        // Writing the contents to the buffer
        writeString(byteBuf, messageContents);

        // Copying the buffer's bytes to the byte array
        byte[] bytes = new byte[byteBuf.readableBytes()];
        byteBuf.readBytes(bytes);

        // Returning the byte array
        return bytes;
    }

    private void writeString(ByteBuf buf, String string) {
        byte[] abyte = string.getBytes(Charset.forName("UTF-8"));

        if (abyte.length > Short.MAX_VALUE) {
            throw new EncoderException("String too big (was " + string.length() + " bytes encoded, max " + Short.MAX_VALUE + ")");
        } else {
            writeVarIntToBuffer(buf, abyte.length);
            buf.writeBytes(abyte);
        }
    }

    private void writeVarIntToBuffer(ByteBuf buf, int input) {
        while ((input & -128) != 0) {
            buf.writeByte(input & 127 | 128);
            input >>>= 7;
        }

        buf.writeByte(input);
    }

    public enum Permission {

        // Permissions that are disabled by default
        IMPROVED_LAVA("Improved Lava", false),
        CROSSHAIR_SYNC("Crosshair sync", false),
        REFILL_FIX("Refill fix", false),

        // GUI permissions
        GUI_ALL("LabyMod GUI", true),
        GUI_POTION_EFFECTS("Potion Effects", true),
        GUI_ARMOR_HUD("Armor HUD", true),
        GUI_ITEM_HUD("Item HUD", true),

        // Permissions that are enabled by default
        BLOCKBUILD("Blockbuild", true),
        TAGS("Tags", true),
        CHAT("Chat features", true),
        ANIMATIONS("Animations", true),
        SATURATION_BAR("Saturation bar", true);

        private String displayName;
        private boolean defaultEnabled;

        /**
         * @param displayName    the permission's display-name
         * @param defaultEnabled whether or not this permission is enabled/activated by default
         */
        Permission(String displayName, boolean defaultEnabled) {
            this.displayName = displayName;
            this.defaultEnabled = defaultEnabled;
        }

        public String getDisplayName() {
            return displayName;
        }

        public boolean isDefaultEnabled() {
            return defaultEnabled;
        }
    }

}
