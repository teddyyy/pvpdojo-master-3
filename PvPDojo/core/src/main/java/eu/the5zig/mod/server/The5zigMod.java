/*
 * Copyright (c) 2018 Gabriel Bolonkowski (Gabik21) - All Rights Reserved
 */

package eu.the5zig.mod.server;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;

import com.pvpdojo.PvPDojo;
import com.pvpdojo.util.Log;

import eu.the5zig.mod.server.api.ImageRegistry;
import eu.the5zig.mod.server.api.UserManager;
import eu.the5zig.mod.server.backend.ClientMessageListener;
import eu.the5zig.mod.server.backend.ImageRegistryImpl;
import eu.the5zig.mod.server.backend.UserManagerImpl;
import eu.the5zig.mod.server.listener.The5zigJoinListener;
import eu.the5zig.mod.server.util.protocol.BufferUtilsImpl;
import eu.the5zig.mod.server.util.protocol.Protocol;

public class The5zigMod {

    public static final String CHANNEL = "5zig";
    public static final String CHANNEL_REGISTER = "5zig_REG";
    public static final int VERSION = 4;

    private static final The5zigMod instance = new The5zigMod();
    private UserManager userManager;
    private Protocol protocol;
    private ImageRegistry imageRegistry;

    /**
     * Returns the current instance of the Server API.
     *
     * @return The Server API instance.
     */
    public static The5zigMod getInstance() {
        return instance;
    }

    public void onEnable() {

        if (!setupHooks())
            return;

        userManager = new UserManagerImpl(this);
        imageRegistry = new ImageRegistryImpl();

        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents((Listener) userManager, PvPDojo.get());
        pluginManager.registerEvents(new The5zigJoinListener(), PvPDojo.get());

        Bukkit.getMessenger().registerOutgoingPluginChannel(PvPDojo.get(), CHANNEL);
        Bukkit.getMessenger().registerIncomingPluginChannel(PvPDojo.get(), CHANNEL, new ClientMessageListener(this));

        try {
            imageRegistry.register("test", ImageIO.read(new File("../images/dojoicon.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Player player : Bukkit.getOnlinePlayers()) {
            protocol.requestRegister(player);
        }

        Log.info("The 5zig Mod Server API has been enabled!");
    }

    /**
     * Setup version independent ProtocolUtils
     */
    private boolean setupHooks() {
        // org.bukkit.craftbukkit.version
        Log.info("Trying to setup Hooks for CraftBukkit ");
        try {
            this.protocol = new Protocol(new BufferUtilsImpl());
            Log.info("Successfully hooked into CraftBukkit");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            Log.severe("Could not find support for this CraftBukkit version");
            Log.info("Please visit 5zig.eu for an updated Version.");
            return false;
        }
    }

    /**
     * Gets the UserManager. The Manager class stores all connected 5zig Mod users in a list
     *
     * @return The User Manager class.
     */
    public UserManager getUserManager() {
        return userManager;
    }

    public Protocol getProtocolUtils() {
        return protocol;
    }

    /**
     * Gets the ImageRegistry. This class is used to register all images that should be sent to the player.
     *
     * @return The Image Registry class.
     */
    public ImageRegistry getImageRegistry() {
        return imageRegistry;
    }

}
