name=Smoke Grenade
id=61
rarity=BLUE
collection=MILITARY
points=3
description=Click your item to throw a grenade that will create blinding smoke
executor=SmokeGrenadeAbility
icon=GHAST_TEAR:0
ability_type=active
enabled=true

base-stat-cooldown=40
base-stat-mana_use=100
base-stat-ability_uses=1.0
base-stat-duration=20
base-stat-radius=5

upgrade-1-name=Cooldown Reduction
upgrade-2-name=Increase Duration
upgrade-3-name=Increase Radius

upgrade-1-stat-cooldown=-10.0
upgrade-2-stat-duration=5
upgrade-3-stat-radius=2.0


upgrade-1-points=1
upgrade-2-points=1
upgrade-3-points=1

upgrade-1-description=Reduces the cooldown by ~cooldown seconds
upgrade-2-description=Increases duration by ~duration
upgrade-3-description=Increases radius by ~radius
