name=Nuker
id=158
rarity=DARK_RED
collection=DISASTER
points=12
description=desc
executor=NukerAbility
icon=TNT:0
ability_type=active
enabled=true

base-stat-cooldown=90.0
base-stat-mana_use=100.0
base-stat-ability_uses=1
base-stat-radius=3.0
base-stat-ability_damage=100

upgrade-1-name=Cooldown Reduction
upgrade-2-name=Damage
upgrade-3-name=Radius
upgrade-4-name=Double Uses

upgrade-1-stat-cooldown=-10.0
upgrade-2-stat-radius=3.0
upgrade-3-stat-ability_damage=50
upgrade-4-stat-ability_uses=1

upgrade-1-points=0.5
upgrade-2-points=1
upgrade-3-points=2
upgrade-4-points=1

upgrade-1-description=reduces the cooldown by ~cooldown seconds
upgrade-2-description=increase nuker radius by ~radius blocks
upgrade-3-description=increases damage by ~ability_damagedamage hearts
upgrade-4-description=increases uses
