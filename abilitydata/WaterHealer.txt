name=Water Healer
id=91
rarity=BLUE
collection=BENDER
points=8
description=...
executor=WaterHealerAbility
icon=INK_SACK:4
ability_type=active
enabled=true

base-stat-cooldown=60.0
base-stat-ability_uses=1.0
base-stat-mana_use=50.0
base-stat-heal=150

upgrade-1-name=Cooldown reduction
upgrade-2-name=Duration upgrade

upgrade-1-stat-cooldown=-10
upgrade-2-stat-heal=50

upgrade-1-points=0.5
upgrade-2-points=1.5

upgrade-1-description=reduce cooldown by ~cooldown seconds
upgrade-2-description=increases heal by 2 more hearts.