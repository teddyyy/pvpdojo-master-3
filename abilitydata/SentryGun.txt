name=SentryGun
id=13
rarity=PURPLE
collection=MILITARY
points=8
description=When you activate the ability, a barrage of arrows is fired, each arrow deals ~arrow_damage damage (hearts) 
executor=SentryGunAbility
icon=ARROW:0
ability_type=active
enabled=true

base-stat-cooldown=55.0
base-stat-arrow_damage=12.5
base-stat-mana_use=80.0
base-stat-ability_uses=1.0
base-stat-amount=20.0
base-stat-speed=2.0
base-stat-fire_rate=0

upgrade-1-name=Mana Reduction
upgrade-2-name=Cooldown Reduction
upgrade-3-name=Increased Velocity
upgrade-4-name=More Arrows
upgrade-5-name=Faster Fire Rate
upgrade-6-name=Increased Damage


upgrade-1-stat-mana_use=-50.0
upgrade-2-stat-cooldown=-15.0
upgrade-3-stat-speed=2.0
upgrade-4-stat-amount=20.0
upgrade-5-stat-fire_rate=3.0
upgrade-6-stat-arrow_damage=6.25
upgrade-6-stat-mana_use=80.0


upgrade-1-points=1
upgrade-2-points=1
upgrade-3-points=1
upgrade-4-points=1
upgrade-5-points=1.5
upgrade-6-points=1.5

upgrade-1-description=reduces the mana use by ~mana_use mana
upgrade-2-description=reduces the cooldown by ~cooldown seconds
upgrade-3-description=arrows fly twice as fast
upgrade-4-description=increases total number of, arrows by ~amount arrows 
upgrade-5-description=fire rate of arrows doubled
upgrade-6-description=damage per arrow increased, by ~arrow_damage damage (hearts)