name=Avatar
id=187
rarity=LEGENDARY
collection=BENDER
points=15
description=...
executor=AvatarAbility
icon=BEACON:0
ability_type=active
enabled=true

base-stat-mana_use=150.0
base-stat-cooldown=80.0
base-stat-ability_uses=1.0
base-stat-sides=10

upgrade-1-name=Cooldown Reduction
upgrade-2-name=Better Flying

upgrade-1-stat-cooldown=-20.0
upgrade-2-stat-sides=10

upgrade-1-points=1.5
upgrade-2-points=1.0

upgrade-1-description=Reduces the cooldown by ~cooldown seconds
upgrade-2-description=...
