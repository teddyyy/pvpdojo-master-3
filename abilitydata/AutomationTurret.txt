name=Automation Turret
id=62
rarity=LEGENDARY
collection=MILITARY
points=18
description=Click your item to spawn a turret that will fire automatically at enemies or click it to take control
executor=AutomationTurretAbility
icon=ENDER_PEARL:0
ability_type=active
enabled=true

base-stat-cooldown=140
base-stat-mana_use=50
base-stat-ability_uses=1.0
base-stat-bulllets=90
base-stat-missles=10

upgrade-1-name=Cooldown Reduction
upgrade-2-name=Increase bullets
upgrade-3-name=Increase missles

upgrade-1-stat-cooldown=-15.0
upgrade-2-stat-bulllets=15
upgrade-3-stat-missles=5


upgrade-1-points=1
upgrade-2-points=1
upgrade-3-points=1

upgrade-1-description=Reduces the cooldown by ~cooldown seconds
upgrade-2-description=Increases bullets by ~bullets
upgrade-3-description=Increases missles by ~missles
