name=Pillar
id=11
rarity=BLUE
collection=SAMURAI
points=5
description=When activated creates a pillar, with you on top of it
executor=PillarAbility
icon=QUARTZ_BLOCK:0
ability_type=active
enabled=true

base-stat-mana_use=50.0
base-stat-cooldown=35.0
base-stat-ability_uses=1.0
base-stat-height=5.0
base-stat-speed=0.0

upgrade-1-name=Cooldown Reduction
upgrade-1-points=1
upgrade-1-stat-cooldown=-10.0
upgrade-1-description=description

upgrade-2-name=Increased Height
upgrade-2-stat-height=5.0
upgrade-2-points=1
upgrade-2-description=description

upgrade-3-name=Increased Speed
upgrade-3-points=1
upgrade-3-description=description
upgrade-3-stat-speed=5.0
