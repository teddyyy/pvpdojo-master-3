name=Craft Aura
id=103
rarity=PURPLE
collection=DIVINITY
points=6
description=Recrafting creates an explosion damaging, players in a ~radius block radius.
executor=CraftAuraAbility
icon=RED_MUSHROOM:0
ability_type=passive
enabled=true

base-stat-cooldown=20.0
base-stat-ability_uses=1.0
base-stat-mana_use=0.0
base-stat-ability_damage=200.0
base-stat-radius=3.0

upgrade-1-name=Cooldown Reduction
upgrade-2-name=Radius
upgrade-3-name=Damage

upgrade-1-stat-cooldown=-10.0
upgrade-2-stat-radius=3.0
upgrade-3-stat-ability_damage=75.0

upgrade-1-points=0.5
upgrade-2-points=1.0
upgrade-3-points=1.0

upgrade-1-description=Reduces cooldown by ~cooldown seconds
upgrade-2-description=Increases the radius by ~radius blocks
upgrade-3-description=Increases the damage by ~ability_damage
