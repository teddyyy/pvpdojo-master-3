name=Astronaut
id=31
rarity=BLUE
collection=SAMURAI
points=2
description=Massively increased jump height, for ~duration seconds
executor=AstronautAbility
icon=GLASS:0
ability_type=active
enabled=true

base-stat-cooldown=30.0
base-stat-duration=5.0
base-stat-mana_use=30.0
base-stat-ability_uses=1.0
base-stat-jump_power=5

upgrade-1-name=Cooldown Reduction
upgrade-2-name=Jump Power Increase
upgrade-3-name=Duration Increase

upgrade-1-stat-cooldown=-10
upgrade-2-stat-jump_power=5
upgrade-3-stat-duration=6
upgrade-3-stat-mana_use=20

upgrade-1-points=.5
upgrade-2-points=.5
upgrade-3-points=.5

upgrade-1-description=reduces the cooldown by ~cooldown seconds
upgrade-2-description=double the effective jump power
upgrade-3-description=increases the duration by ~duration seconds
