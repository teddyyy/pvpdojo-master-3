name=Zen
id=29
rarity=PURPLE
collection=SAMURAI
points=6
description=After 2 seconds teleports you to, the nearest enemy within ~range blocks  
executor=ZenAbility
icon=EMERALD_BLOCK:0
ability_type=active
enabled=true

base-stat-cooldown=75
base-stat-invincibility=0
base-stat-ability_uses=1.0
base-stat-mana_use=75.0
base-stat-range=25

upgrade-1-name=Mana Reduction
upgrade-2-name=Cooldown Reduction
upgrade-3-name=Range Increase
upgrade-4-name=Invincibility 

upgrade-1-stat-mana_use=-50
upgrade-2-stat-cooldown=-20
upgrade-3-stat-range=20
upgrade-4-stat-invincibility=2.0
upgrade-4-stat-mana_use=50

upgrade-1-points=1
upgrade-2-points=1
upgrade-3-points=1
upgrade-4-points=1.5

upgrade-1-description=reduces the mana use by ~mana_use mana
upgrade-2-description=reduces the cooldown by ~cooldown seconds
upgrade-3-description=increases range by ~range blocks
upgrade-4-description=become invincible while channeling Zen, (may not attack while channeling)