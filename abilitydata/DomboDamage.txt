name=DomboDamage
id=41
rarity=DARK_RED
collection=BRAVERY
points=9
description=When you hit the enemy ~combo_hits times, without being hit once. Your attack, damage is increased by ~bonus_damage damage (hearts), Once you are hit the bonus damage is lost.
executor=DomboDamageAbility
icon=DIAMOND_SWORD:0
ability_type=passive
enabled=true

base-stat-combo_hits=5
base-stat-combo_poison=0
base-stat-bonus_damage=12.5

upgrade-1-name=Combo Hits
upgrade-2-name=Poison
upgrade-3-name=Bonus Damage

upgrade-1-stat-combo_hits=-1
upgrade-2-stat-combo_poison=12.5
upgrade-3-stat-bonus_damage=12.5

upgrade-1-points=1
upgrade-2-points=1
upgrade-3-points=2

upgrade-1-description=reduces consecutive hits needed by ~combo_hits hit
upgrade-2-description=enemy is poisoned when you combo them
upgrade-3-description=damage increased by ~bonus_damage damage (hearts)
