name=Barricade
id=3
rarity=BLUE
collection=SAMURAI
points=4
description=When activated creates a ~width x 4.0 wall, that's impenetrable to players and abilities   
executor=BarricadeAbility
icon=BRICK:0
ability_type=active
enabled=true


base-stat-cooldown=40.0
base-stat-ability_uses=1.0
base-stat-mana_use=65.0
base-stat-duration=5.0
base-stat-width=5.0

upgrade-1-name=Cooldown Reduction
upgrade-2-name=Width Increase
upgrade-3-name=Duration Increase

upgrade-1-stat-cooldown=-15.0
upgrade-2-stat-width=4.0
upgrade-3-stat-duration=5.0

upgrade-1-points=.5
upgrade-2-points=.5
upgrade-3-points=1

upgrade-1-description=reduces the cooldown by ~cooldown seconds
upgrade-2-description=increases the width of the wall by ~width blocks
upgrade-3-description=increases the duration by ~duration seconds