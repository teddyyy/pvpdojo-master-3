name=Racer
id=59
rarity=PURPLE
collection=SUMMONER
points=7
description=Click your item to spawn a minecart that allows to drive around the map
executor=RacerAbility
icon=MINECART:0
ability_type=active
enabled=true

base-stat-cooldown=100
base-stat-mana_use=50
base-stat-ability_uses=1.0
base-stat-duration=25

upgrade-1-name=Cooldown Reduction
upgrade-2-name=Ability Duration

upgrade-1-stat-cooldown=-15.0
upgrade-2-stat-duration=5

upgrade-1-points=1
upgrade-2-points=1

upgrade-1-description=Reduces the cooldown by ~cooldown seconds
upgrade-2-description=Increases duration by ~duration

