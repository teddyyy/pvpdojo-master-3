name=Ray Gun
id=113
rarity=RED
collection=BEAM
points=8
description=Click your item to shoot a, ray/particle beam which damages, the target it hits
executor=RayGunAbility
icon=REDSTONE_TORCH_ON:0
ability_type=active
enabled=true


base-stat-cooldown=75
base-stat-ability_damage=75.0
base-stat-mana_use=100
base-stat-ability_uses=1.0
base-stat-radius=2.0
base-stat-damage=150.0


upgrade-1-name=Cooldown Reduction
upgrade-2-name=Radius
upgrade-3-name=Damage

upgrade-1-stat-cooldown=-15.0
upgrade-2-stat-radius=2.0
upgrade-3-stat-damage=50.0


upgrade-1-points=1
upgrade-2-points=1
upgrade-3-points=1


upgrade-1-description=reduces the cooldown by ~cooldown seconds
upgrade-2-description=increases the radius by ~radius blocks
upgrade-3-description=increases the damage by ~damage (hearts)
