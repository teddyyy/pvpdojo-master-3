name=Icelord
id=101
rarity=GOLD
collection=SORCERER
points=15
description=Starts with 3 attacks, the first one shoots a projectile to, freeze up your target, the second one shoots a beam, to blind out your target, the last one shoots a beam, to incage the target it hits, in a box
executor=IcelordAbility
icon=PACKED_ICE:0
ability_type=active
enabled=true


base-stat-cooldown=110.0
base-stat-ability_damage=300.0
base-stat-mana_use=115
base-stat-ability_uses=3.0
base-stat-freeze_duration=3.0
base-stat-blindness_damage=125.0
base-stat-blindness_duration=5.0
base-stat-ice_duration=5.0


upgrade-1-name=Cooldown Reduction
upgrade-2-name=Freeze
upgrade-3-name=Blindness
upgrade-4-name=Ice

upgrade-1-stat-cooldown=-15.0
upgrade-2-stat-freeze_duration=2.0
upgrade-3-stat-blindness_duration=3.0
upgrade-4-stat-ice_duration=5.0


upgrade-1-points=1
upgrade-2-points=1
upgrade-3-points=0.5
upgrade-4-points=1


upgrade-1-description=reduces the cooldown by ~cooldown seconds
upgrade-2-description=increases freeze duration by ~freeze_duration seconds
upgrade-3-description=increases blindness duration by ~blindness_duration seconds
upgrade-4-description=increases Ice case duration by ~ice_duration seconds
